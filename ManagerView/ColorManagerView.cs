﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ColorManagerView
    {
        private ColorManager mColorMan;

        public ColorManagerView()
        {
            mColorMan = new ColorManager();
            Mapper.CreateMap<ARGNSColor, ColorView>();
            Mapper.CreateMap<ColorView, ARGNSColor>();
        }

        public ColorView GetColorByCode(CompanyView pCv, string pColorCode)
        {
            ColorView mReturn = null;
            try
            {
                mReturn = Mapper.Map<ColorView>(mColorMan.GetColorByCode(Mapper.Map<CompanyConn>(pCv), pColorCode));
                mReturn.U_PicFile = GetImageName(mReturn.U_Pic);

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ColorView> GetColorListSearch(CompanyView pCc, string pColorCode = "", string pColorName = "")
        {
            List<ColorView> ret = null;
            try
            {
                ret=Mapper.Map<List<ColorView>>(mColorMan.GetColorListSearch(Mapper.Map<CompanyConn>(pCc), pColorCode, pColorName));
               
                return ret;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string getColorRGB(string U_Argb)
        {
            string ret = string.Empty;
            if (!string.IsNullOrEmpty(U_Argb))
            {
                Color color = Color.FromArgb(Convert.ToInt32(U_Argb.ToString()));
                ret = ("rgb(" + color.R + ", " + color.G + ", " + color.B + ")");
            }
            return ret;
        }

        private string GetImageName(string pPath)
        {
            string ret = string.Empty;
            string[] mImageName;
            try
            {
                if (!string.IsNullOrEmpty(pPath))
                {
                    mImageName = pPath.Split(new Char[] { '\\' });
                    ret = mImageName[mImageName.Length - 1];

                }

                return ret;
            }
            catch (Exception)
            {
                return string.Empty;
                throw;
            }
        }

        public string Add(CompanyView pCv, ColorView pColorView)
        {
            try
            {
                return mColorMan.Add(Mapper.Map<CompanyConn>(pCv), Mapper.Map<ARGNSColor>(pColorView));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(CompanyView pCv, ColorView pColorView)
        {
            try
            {
                return mColorMan.Update(Mapper.Map<CompanyConn>(pCv), Mapper.Map<ARGNSColor>(pColorView));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
