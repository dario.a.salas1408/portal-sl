﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ProjectManagerView
    {
        private ProjectManager mProjectManager;
        private UserSapManager mUserSapManager;
        private BusinessPartnerManager mBusinessPartnerManager;

        public ProjectManagerView()
        {
            mProjectManager = new ProjectManager();
            mUserSapManager = new UserSapManager();
            mBusinessPartnerManager = new BusinessPartnerManager();
        }

        public ProjectView GetProject(CompanyView pCv, string pProjectCode, string pModelId)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ARGNSProject, ProjectView>(); cfg.CreateMap<ARGNSCrPathActivities, ARGNSCrPathActivitiesView>(); cfg.CreateMap<ActivitiesSAP, ActivitiesView>(); cfg.CreateMap<ActivitiesSAPCombo, ActivitiesSAPComboView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                ARGNSProject mProject = mProjectManager.GetProject(mapper.Map<CompanyConn>(pCv), pProjectCode, pModelId);
                mProject.ImageName = GetImageName(mProject.U_Pic);
                return mapper.Map<ProjectView>(mProject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSRoutingLine> GetWorkflowLines(CompanyView pCv, string pWorkflow)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => {cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mProjectManager.GetWorkflowLines(mapper.Map<CompanyConn>(pCv), pWorkflow);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCriticalPathDefaultBP(CompanyView pCv)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

                return mProjectManager.GetCriticalPathDefaultBP(mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public List<UserSAP> GetUserSAPList(CompanyView pCv, string pCode, string pName)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mUserSapManager.GetUserSAPSearchByCode(mapper.Map<CompanyConn>(pCv), pCode, pName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeList(CompanyView pCv, string pCode, string pName, short? pDepartment)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mUserSapManager.GetEmployeeSAPSearchByCode(mapper.Map<CompanyConn>(pCv), pCode, pName, pDepartment, false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BusinessPartnerView> GetBPList(CompanyView pCv, string pCode, string pName)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

                return mapper.Map<List<BusinessPartnerView>>(mBusinessPartnerManager.GetBusinessPartnerListSearch(mapper.Map<CompanyConn>(pCv), pCode, pName));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateActivities(CompanyView pCv, List<ActivitiesView> pListActivities)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); cfg.CreateMap<ActivitiesView, ActivitiesSAP>(); cfg.CreateMap<ActivitiesSAPComboView, ActivitiesSAPCombo>(); }).CreateMapper();

                return mProjectManager.CreateActivities(mapper.Map<CompanyConn>(pCv), mapper.Map<List<ActivitiesSAP>>(pListActivities));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public string Update(CompanyView pCv,ProjectView pProject)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); cfg.CreateMap<ProjectView, ARGNSProject>(); cfg.CreateMap<ARGNSCrPathActivitiesView, ARGNSCrPathActivities>(); cfg.CreateMap<ActivitiesView, ActivitiesSAP>(); }).CreateMapper();

                return mProjectManager.Update(mapper.Map<CompanyConn>(pCv), mapper.Map<ARGNSProject>(pProject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult Add(CompanyView pCv, ProjectView pProject)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); cfg.CreateMap<ProjectView, ARGNSProject>(); cfg.CreateMap<ARGNSCrPathActivitiesView, ARGNSCrPathActivities>(); cfg.CreateMap<ActivitiesView, ActivitiesSAP>(); }).CreateMapper();

                return mProjectManager.Add(mapper.Map<CompanyConn>(pCv), mapper.Map<ARGNSProject>(pProject));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetImageName(string pPath)
        {
            string ret = string.Empty;
            string[] mImageName;
            try
            {
                if (!string.IsNullOrEmpty(pPath))
                {
                    mImageName = pPath.Split(new Char[] { '\\' });
                    ret = mImageName[mImageName.Length - 1];

                }

                return ret;
            }
            catch (Exception)
            {
                return string.Empty;
                throw;
            }
        }
    }
}
