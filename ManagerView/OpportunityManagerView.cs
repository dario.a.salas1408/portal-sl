﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class OpportunityManagerView
    {
        private OpportunityManager mOpportunitynManager;

        public OpportunityManagerView()
        {
            mOpportunitynManager = new OpportunityManager();

            Mapper.CreateMap<OpportunitiesSAP, OpportunitiesView>();
            Mapper.CreateMap<OpportunitiesSAPLine, OpportunitiesLineView>();
            Mapper.CreateMap<OpportunitiesView, OpportunitiesSAP>();
            Mapper.CreateMap<OpportunitiesLineView, OpportunitiesSAPLine>();
            Mapper.CreateMap<OpportunityStage, OpportunityStageView>();

            //Activities List
            Mapper.CreateMap<ActivitiesSAP, ActivitiesView>();
            Mapper.CreateMap<ActivitiesView, ActivitiesSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<ActivitiesSAPComboView, ActivitiesSAPCombo>();
            Mapper.CreateMap<ActivitiesSAPCombo, ActivitiesSAPComboView>();
        }

        public OpportunityStageView GetStageById(CompanyView pCompanyParam, int pId)
        {
            try
            {
                return Mapper.Map<OpportunityStageView>(mOpportunitynManager.GetStageById(Mapper.Map<CompanyConn>(pCompanyParam), pId));
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManagerView -> GetStageById:" + ex.InnerException);
                throw;
            }
        }

        public OpportunitiesLineView GetOpportunityLine(CompanyView pCompanyParam)
        {
            try
            {
                return Mapper.Map<OpportunitiesLineView>(mOpportunitynManager.GetOpportunityLine(Mapper.Map<CompanyConn>(pCompanyParam)));
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManagerView -> GetOpportunityLine:" + ex.InnerException);
                throw;
            }
        }

        public OpportunitiesView GetOportunityById(CompanyView pCompanyParam, int pId)
        {           
            try
            {             
                return Mapper.Map<OpportunitiesView>(mOpportunitynManager.GetOportunityById(Mapper.Map<CompanyConn>(pCompanyParam), pId));             

            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManagerView -> GetOportunityById:" + ex.InnerException);
                throw;
            }
           
        }

        public string AddOportunity(OpportunitiesView pObject, CompanyView pCompanyParam)
        {
            try
            {
                return mOpportunitynManager.AddOportunity(Mapper.Map<OpportunitiesSAP>(pObject), Mapper.Map<CompanyConn>(pCompanyParam));
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManagerView -> AddOportunity:" + ex.InnerException);
                throw;
            }
        }

        public string UpdateOportunity(OpportunitiesView pObject, CompanyView pCompanyParam)
        {
            try
            {
                return mOpportunitynManager.UpdateOportunity(Mapper.Map<OpportunitiesSAP>(pObject), Mapper.Map<CompanyConn>(pCompanyParam));
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManagerView -> UpdateOportunity:" + ex.InnerException);
                throw;
            }
        }

        public List<OpportunitiesView> GetOpportunityListSearch(CompanyView pCompanyParam, string pCodeBP = "", int? pSalesEmp = null, string pOpportunityName = "", int? pOwner = null, DateTime? pDate = null, string pStatus = "")
        {
            try
            {
                return Mapper.Map<List<OpportunitiesView>>(mOpportunitynManager.GetOpportunityListSearch(Mapper.Map<CompanyConn>(pCompanyParam), pCodeBP, pSalesEmp, pOpportunityName, pOwner, pDate, pStatus));
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManagerView -> GetOpportunityListSearch:" + ex.InnerException);
                throw;
            }
        }


    }
}
