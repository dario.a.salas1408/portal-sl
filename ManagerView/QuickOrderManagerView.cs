﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Manager;
using ARGNS.View;
using ARGNS.Model.Implementations;
using ARGNS.Model.Interfaces;
using ARGNS.Model;
using AutoMapper;
using System.Web.UI.WebControls;
using ARGNS.Util;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.PDM;

namespace ARGNS.ManagerView
{

    public class QuickOrderManagerView
    {
        QuickOrderManager mQuickOrderManager;
        UserSapManager mUserSapManager;
        ItemMasterManagerView mItemMasterManagerView;

        public QuickOrderManagerView()
        {
            mQuickOrderManager = new QuickOrderManager();
            mUserSapManager = new UserSapManager();
            mItemMasterManagerView = new ItemMasterManagerView();

            Mapper.CreateMap<QuickOrder, QuickOrderView>();
            Mapper.CreateMap<QuickOrderView, QuickOrder>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pIdUser"></param>
        /// <param name="pCompanyId"></param>
        /// <param name="pCustomerCode"></param>
        /// <param name="pGetPrices"></param>
        /// <returns></returns>
        public List<QuickOrderView> GetQuickOrders(CompanyView pCc, int pIdUser,
            int pCompanyId, string pCustomerCode, bool pOnlyActiveItems, bool pGetPrices = false)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

                List<QuickOrderView> mListQuickOrder = Mapper.Map<List<QuickOrderView>>(mQuickOrderManager.GetQuickOrders(pIdUser, pCompanyId));
                if (pGetPrices && mListQuickOrder.Count > 0)
                {
                    JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetQuickOrderListBy(pCc, "Y", "N",
                        null, "", pIdUser, "", "", pCustomerCode, -1, pOnlyActiveItems, true, false, "",
                        mListQuickOrder.Select(c => c.ItemCode).ToList(), 0, 0, null);

                    foreach (QuickOrderView mQuickOrderAux in mListQuickOrder)
                    {
                        ItemMasterSAP mItemMasterAux = mJsonObjectResult.ItemMasterSAPList.Where(c => c.ItemCode == mQuickOrderAux.ItemCode).FirstOrDefault();
                        UnitOfMeasure mDefaultUOM = mItemMasterAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                        mQuickOrderAux.ItemName = mItemMasterAux.ItemName;
                        mQuickOrderAux.ItemPrices = (mItemMasterAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault());
                        mQuickOrderAux.UnitOfMeasureList = mItemMasterAux.UnitOfMeasureList;
                        mQuickOrderAux.Stock = mapper.Map<StockModelView>(mItemMasterAux.Stock);
                    }

                    return mListQuickOrder;
                }
                else
                {
                    return mListQuickOrder;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManagerView -> GetQuickOrders:" + ex.InnerException);
                throw ex;
            }
        }

        public QuickOrderView GetQuickOrderLine(int pIdUser, string pItemCode, int pCompanyId)
        {
            try
            {
                return Mapper.Map<QuickOrderView>(mQuickOrderManager.GetQuickOrderLine(pIdUser, pItemCode, pCompanyId));
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManagerView -> GetQuickOrderLine:" + ex.InnerException);
                throw ex;
            }
        }


        public bool Add(QuickOrderView pQuickOrder)
        {
            try
            {
                return mQuickOrderManager.Add(Mapper.Map<QuickOrder>(pQuickOrder));
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManagerView -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(QuickOrderView pQuickOrder)
        {
            try
            {
                return mQuickOrderManager.Update(Mapper.Map<QuickOrder>(pQuickOrder));
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManagerView -> Update:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Delete(QuickOrderView pQuickOrder)
        {
            try
            {
                return mQuickOrderManager.Delete(Mapper.Map<QuickOrder>(pQuickOrder));
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManagerView -> Delete:" + ex.InnerException);
                throw ex;
            }
        }

        public bool DeleteCart(int pIdUser, int pCompanyId)
        {
            try
            {
                return mQuickOrderManager.DeleteCart(pIdUser, pCompanyId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManagerView -> DeleteCart:" + ex.InnerException);
                throw ex;
            }
        }

    }

}
