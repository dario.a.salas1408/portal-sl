﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class LogManagerView
    {
        LogManager mLogManager;

        public LogManagerView()
        {
            mLogManager = new LogManager();
        }

        public List<Log> GetListLogByUser(int pIdUser)
        {
            try
            {
                return mLogManager.GetListLogByUser(pIdUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
