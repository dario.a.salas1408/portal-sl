﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ShipmentManagerView
    {
        private ShipmentManager mShipmentManager;

        public ShipmentManagerView()
        {
            mShipmentManager = new ShipmentManager();
        }

        public List<ARGNSContainerView> GetShipmentListSearch(CompanyView pCv, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            try
            {
                Mapper.CreateMap<ARGNSContainer, ARGNSContainerView>();
                Mapper.CreateMap<ARGNSContainerView, ARGNSContainer>();

                return Mapper.Map<List<ARGNSContainerView>>(mShipmentManager.GetShipmentListSearch(Mapper.Map<CompanyConn>(pCv), txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSContainerView> GetMyShipmentListSearchBP(CompanyView pCv, string pBP, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            try
            {
                Mapper.CreateMap<ARGNSContainer, ARGNSContainerView>();
                Mapper.CreateMap<ARGNSContainerView, ARGNSContainer>();

                return Mapper.Map<List<ARGNSContainerView>>(mShipmentManager.GetMyShipmentListSearchBP(Mapper.Map<CompanyConn>(pCv), pBP, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public ARGNSContainerView GetShipmentByCode(CompanyView pCv, int pShipmentID)
        {
            try
            {
                Mapper.CreateMap<ARGNSContainer, ARGNSContainerView>();
                Mapper.CreateMap<ARGNSContainerView, ARGNSContainer>();
                Mapper.CreateMap<ARGNSContainerLineView, ARGNSContainerLine>();
                Mapper.CreateMap<ARGNSContainerLine, ARGNSContainerLineView>();
                Mapper.CreateMap<ARGNSContainerStatusView, ARGNSContainerStatus>();
                Mapper.CreateMap<ARGNSContainerStatus, ARGNSContainerStatusView>();
                Mapper.CreateMap<ARGNSContainerPort, ARGNSContainerPortView>();
                Mapper.CreateMap<ARGNSContainerPortView, ARGNSContainerPort>();
                Mapper.CreateMap<ARGNSContainerPackageView, ARGNSContainerPackage>();
                Mapper.CreateMap<ARGNSContainerPackage, ARGNSContainerPackageView>();
                Mapper.CreateMap<ARGNSContainerPackageLine, ARGNSContainerPackageLineView>();
                Mapper.CreateMap<ARGNSContainerPackageLineView, ARGNSContainerPackageLine>();
                Mapper.CreateMap<Freight, FreightView>();
                Mapper.CreateMap<FreightView, Freight>();
                Mapper.CreateMap<DraftLine, DraftLineView>();
                Mapper.CreateMap<DraftLineView, DraftLine>();
                Mapper.CreateMap<GLAccountSAP, GLAccountView>();
                Mapper.CreateMap<GLAccountView, GLAccountSAP>(); 

                return Mapper.Map<ARGNSContainerView>(mShipmentManager.GetShipmentByCode(Mapper.Map<CompanyConn>(pCv), pShipmentID));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSContainerPackageView> GetContainerPackageList(CompanyView pCv, int pShipmentID)
        {
            try
            {
                Mapper.CreateMap<ARGNSContainerPackageView, ARGNSContainerPackage>();
                Mapper.CreateMap<ARGNSContainerPackage, ARGNSContainerPackageView>();
                Mapper.CreateMap<ARGNSContainerPackageLine, ARGNSContainerPackageLineView>();
                Mapper.CreateMap<ARGNSContainerPackageLineView, ARGNSContainerPackageLine>();

                return Mapper.Map<List<ARGNSContainerPackageView>>(mShipmentManager.GetContainerPackageList(Mapper.Map<CompanyConn>(pCv), pShipmentID));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSContainerSetupView> GetContainerSetupList(CompanyView pCv)
        {
            try
            {
                Mapper.CreateMap<ARGNSContainerSetupView, ARGNSContainerSetup>();
                Mapper.CreateMap<ARGNSContainerSetup, ARGNSContainerSetupView>();

                return Mapper.Map<List<ARGNSContainerSetupView>>(mShipmentManager.GetContainerSetupList(Mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(CompanyView pCv, ARGNSContainerView pShipment)
        {
            try
            {
                Mapper.CreateMap<ARGNSContainer, ARGNSContainerView>();
                Mapper.CreateMap<ARGNSContainerView, ARGNSContainer>();

                return mShipmentManager.Update(Mapper.Map<CompanyConn>(pCv), Mapper.Map<ARGNSContainer>(pShipment));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(CompanyView pCv, ARGNSContainerView pShipment)
        {
            try
            {
                Mapper.CreateMap<ARGNSContainer, ARGNSContainerView>();
                Mapper.CreateMap<ARGNSContainerView, ARGNSContainer>();

                return mShipmentManager.Add(Mapper.Map<CompanyConn>(pCv), Mapper.Map<ARGNSContainer>(pShipment));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ARGNSPSlipPackageView> GetPSlipPackageList(CompanyView pCv, int pContainerID, int pShipmentID)
        {
            try
            {
                Mapper.CreateMap<ARGNSPSlipPackageView, ARGNSPSlipPackage>();
                Mapper.CreateMap<ARGNSPSlipPackage, ARGNSPSlipPackageView>();
                Mapper.CreateMap<ARGNSPSlipPackageLine, ARGNSPSlipPackageLineView>();
                Mapper.CreateMap<ARGNSPSlipPackageLineView, ARGNSPSlipPackageLine>();

                return Mapper.Map<List<ARGNSPSlipPackageView>>(mShipmentManager.GetPSlipPackageList(Mapper.Map<CompanyConn>(pCv), pContainerID, pShipmentID));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSPSlipPackageTypeView> GetPSlipPackageTypeList(CompanyView pCv)
        {
            try
            {
                Mapper.CreateMap<ARGNSPSlipPackageTypeView, ARGNSPSlipPackageType>();
                Mapper.CreateMap<ARGNSPSlipPackageType, ARGNSPSlipPackageTypeView>();

                return Mapper.Map<List<ARGNSPSlipPackageTypeView>>(mShipmentManager.GetPSlipPackageTypeList(Mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DraftView> GetDocumentWhitOpenLines(CompanyView pCv, string pObjType)
        {
            try
            {
                Mapper.CreateMap<DraftView, Draft>();
                Mapper.CreateMap<Draft, DraftView>();

                return Mapper.Map<List<DraftView>>(mShipmentManager.GetDocumentWhitOpenLines(Mapper.Map<CompanyConn>(pCv), pObjType));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DraftLineView> GetDocumentOpenLines(CompanyView pCv, string pObjType, List<int> pDocEntryList)
        {
            try
            {
                Mapper.CreateMap<DraftLineView, DraftLine>();
                Mapper.CreateMap<DraftLine, DraftLineView>();

                return Mapper.Map<List<DraftLineView>>(mShipmentManager.GetDocumentOpenLines(Mapper.Map<CompanyConn>(pCv), pObjType, pDocEntryList));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DraftView> GetShipmentDocument(CompanyView pCv, string pShipmentID, string pObjType)
        {
            try
            {
                Mapper.CreateMap<DraftView, Draft>();
                Mapper.CreateMap<Draft, DraftView>();

                return Mapper.Map<List<DraftView>>(mShipmentManager.GetShipmentDocument(Mapper.Map<CompanyConn>(pCv), pShipmentID, pObjType));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ShipmentMembersView GetShipmentMembersObject(CompanyView pCv)
        {
            try
            {
                Mapper.CreateMap<ShipmentMembersView, ShipmentMembers>();
                Mapper.CreateMap<ShipmentMembers, ShipmentMembersView>();
                Mapper.CreateMap<ARGNSContainerStatus, ARGNSContainerStatusView>();
                Mapper.CreateMap<ARGNSContainerStatusView, ARGNSContainerStatus>();
                Mapper.CreateMap<ARGNSContainerPort, ARGNSContainerPortView>();
                Mapper.CreateMap<ARGNSContainerPortView, ARGNSContainerPort>();
                Mapper.CreateMap<Freight, FreightView>();
                Mapper.CreateMap<FreightView, Freight>();

                return Mapper.Map<ShipmentMembersView>(mShipmentManager.GetShipmentMembersObject(Mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
