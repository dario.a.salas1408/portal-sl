﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.DiverSO;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using static ARGNS.Util.Enums;

namespace ARGNS.ManagerView
{
    public class ItemMasterManagerView
    {
        private ItemMasterManager mItemMasterManager;
        private UserSapManager mUserSapManager;

        public ItemMasterManagerView()
        {
            mItemMasterManager = new ItemMasterManager();
            mUserSapManager = new UserSapManager();
            Mapper.CreateMap<ItemMasterSAP, ItemMasterView>();
            Mapper.CreateMap<ItemMasterView, ItemMasterSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<StockModelView, StockModel>();
            Mapper.CreateMap<StockModel, StockModelView>();
            Mapper.CreateMap<GLAccountView, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, GLAccountView>();
            Mapper.CreateMap<UnitOfMeasure, UnitOfMeasureView>();
            Mapper.CreateMap<InventoryStatus, InventoryStatusView>();
            Mapper.CreateMap<InventoryStatusView, InventoryStatus>();
            Mapper.CreateMap<AvailableToPromise, AvailableToPromiseView>();
            Mapper.CreateMap<AvailableToPromiseView, AvailableToPromise>();
        }


        public DiverSOResultItems GetDiverSOItems(string pItemCode, string pCardCode, DiverSOItemMethod pType)
        {
            try
            {
                return Mapper.Map<DiverSOResultItems>(
                    mItemMasterManager.GetDiverSOItems(pItemCode, pCardCode, pType));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetDiverSOItems :" + ex.Message);
                Logger.WriteError("ItemMasterManagerView -> GetDiverSOItems :" + ex.InnerException.Message);
                throw ex;
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pPurchaseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <returns></returns>
        public short GetItemGroup(CompanyView pCc,
            string pPurchaseItem, string pSellItem,
            string pInventoryItem, string pItemCode,
            int pUserId, bool pOnlyActiveItems,
            List<WarehousePortal> pWarehousePortalList)
        {
            short mResult;
            try
            {
                List<ItemMasterView> ret = Mapper.Map<List<ItemMasterView>>(
                    mItemMasterManager.GetOITMListBy(
                        Mapper.Map<CompanyConn>(pCc),
                        pCc.CheckApparelInstallation, pPurchaseItem, pSellItem,
                        pInventoryItem, "N", "", null, "", pUserId, pItemCode, "",
                        pOnlyActiveItems, pWarehousePortalList));

                mResult = ret.Where(c => c.ItemCode == pItemCode).Single().ItmsGrpCod ?? 0;
            }
            catch (Exception)
            {
                mResult = 0;
            }
            return mResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pModCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetOITMListBy(CompanyView pCc, string pPrchseItem,
            string pSellItem, string pInventoryItem,
            string pItemsWithStock,
            string pItemGroup,
            List<UDF_ARGNS> pMappedUdf,
            string pModCode, int pUserId, string pItemCode,
            string pItemName, bool pOnlyActiveItems,
            List<WarehousePortal> pWarehousePortalList,
            bool pCkCatalogueNum = false,
            string pBPCode = "", string pBPCatalogCode = "",
            int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                if (!pItemsWithStock.Equals("Y"))
                {
                    return mItemMasterManager.GetOITMListBy(
                        Mapper.Map<CompanyConn>(pCc),
                        pCc.CheckApparelInstallation, pPrchseItem, pSellItem,
                        pInventoryItem, pItemsWithStock, pItemGroup,
                        pMappedUdf,
                        pModCode, pUserId, pItemCode,
                        pItemName, pOnlyActiveItems, pWarehousePortalList,
                        pCkCatalogueNum, pBPCode, pBPCatalogCode,
                        pStart, pLength, pOrderColumn);
                }
                else
                {
                    JsonObjectResult mItemList = mItemMasterManager.GetOITMListBy(
                        Mapper.Map<CompanyConn>(pCc),
                        pCc.CheckApparelInstallation, pPrchseItem, pSellItem,
                        pInventoryItem, pItemsWithStock, pItemGroup, pMappedUdf,
                        pModCode, pUserId, pItemCode,
                        pItemName, pOnlyActiveItems, pWarehousePortalList,
                        pCkCatalogueNum, pBPCode, pBPCatalogCode,
                        pStart, pLength, pOrderColumn);

                    string[] mFormula = pCc.CompanyStock.FirstOrDefault().Formula.Split(' ');

                    foreach (ItemMasterSAP mItemAux in mItemList.ItemMasterSAPList)
                    {
                        if (mItemAux.Stock != null)
                        {
                            mItemAux.Stock.Available = Util.Miscellaneous.GetAvailable(mFormula, Mapper.Map<StockModel>(mItemAux.Stock));
                        }
                    }

                    if (mItemList.Others["TotalRecords"] != null)
                    {
                        mItemList.Others["TotalRecords"] = mItemList.ItemMasterSAPList.Where(w => w.Stock.Available > 0).Count().ToString();
                    }
                    else
                    {
                        mItemList.Others.Add("TotalRecords", mItemList.ItemMasterSAPList.Where(w => w.Stock.Available > 0).Count().ToString());
                    }

                    mItemList.ItemMasterSAPList = mItemList.ItemMasterSAPList
                        .Where(w => w.Stock.Available > 0)
                        .Skip(pStart)
                        .Take(pLength).ToList();

                    return mItemList;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetOITMListBy :" + ex.Message);
                Logger.WriteError("ItemMasterManagerView -> GetOITMListBy :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pListItems"></param>
        /// <param name="pItemType"></param>
        /// <returns></returns>
        public List<ItemMasterView> GetOITMListByList(CompanyView pCc, List<string> pListItems, string pItemType)
        {
            try
            {
                return Mapper.Map<List<ItemMasterView>>(
                    mItemMasterManager.GetOITMListByList(
                        Mapper.Map<CompanyConn>(pCc), pListItems, pItemType));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetOITMListByList :" + ex.Message);
                Logger.WriteError("ItemMasterManagerView -> GetOITMListByList :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="PrchseItem"></param>
        /// <param name="SellItem"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pModCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="uCardCode"></param>
        /// <param name="pItemGroupCode"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pNoItemsWithZeroStockCk"></param>
        /// <param name="pSupplierCardCode"></param>
        /// <param name="pItemList"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetQuickOrderListBy(CompanyView pCc, string PrchseItem,
            string SellItem, List<UDF_ARGNS> pMappedUdf, string pModCode,
            int pUserId, string pItemCode, string pItemName, string uCardCode,
            short pItemGroupCode, bool pOnlyActiveItems, bool isSO,
            bool pNoItemsWithZeroStockCk = false,
            string pSupplierCardCode = "",
            List<string> pItemList = null, int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                Mapper.CreateMap<StockModel, StockModelView>();
                Mapper.CreateMap<StockModelView, StockModel>();

                UsersSetting mUserSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserId);

                JsonObjectResult mItemList = mItemMasterManager
                    .GetQuickOrderListBy(Mapper.Map<CompanyConn>(pCc),
                    pCc.CheckApparelInstallation, PrchseItem, SellItem,
                    pMappedUdf, pModCode, pUserId, pItemCode, pItemName,
                    uCardCode, pItemGroupCode, mUserSetting.CostPriceList, pOnlyActiveItems, isSO,
                    pNoItemsWithZeroStockCk, pSupplierCardCode,
                    pItemList, pStart, pLength, pOrderColumn);

                string[] mFormula = pCc.CompanyStock.FirstOrDefault().Formula.Split(' ');
                foreach (ItemMasterSAP mItemAux in mItemList.ItemMasterSAPList)
                {
                    if (mItemAux.Stock != null)
                    {
                        mItemAux.Stock.Available = Util.Miscellaneous.GetAvailable(
                            mFormula, Mapper.Map<StockModel>(mItemAux.Stock));
                    }
                }

                return mItemList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetQuickOrderListBy :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="PrchseItem"></param>
        /// <param name="SellItem"></param>
        /// <param name="pCodeBar"></param>
        /// <returns></returns>
        public ItemMasterView GeItemByCodeBar(CompanyView pCc, string PrchseItem, string SellItem, string pCodeBar)
        {
            try
            {
                return Mapper.Map<ItemMasterView>(
                    mItemMasterManager.GeItemByCodeBar(
                        Mapper.Map<CompanyConn>(pCc), PrchseItem, SellItem, pCodeBar));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GeItemByCodeBar :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public ItemMasterView GeItemByCode(CompanyView pCc, string pItemCode, int idUser, string pBarCode = null, short? pListNum = null)
        {
            try
            {
                ItemMasterView mItemMasterView = Mapper.Map<ItemMasterView>(
                    mItemMasterManager.GeItemByCode(Mapper.Map<CompanyConn>(pCc), pItemCode, idUser, pBarCode));

                if (mItemMasterView.ListPriceListSAP.Count > 0)
                {
                    mItemMasterView.PriceItemByListPrice = mItemMasterManager.GetPriceItemByPriceList(Mapper.Map<CompanyConn>(pCc), pItemCode, (pListNum != null ? pListNum.Value : mItemMasterView.ListPriceListSAP.FirstOrDefault().ListNum));
                }

                return mItemMasterView;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GeItemByCode :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="ListItems"></param>
        /// <param name="CardCode"></param>
        /// <param name="pSession"></param>
        /// <param name="pOrderItems"></param>
        /// <returns></returns>
        public List<ItemMasterView> GetItemsPrice(CompanyView pCc, List<ItemMasterView> ListItems, string CardCode, string pSession, bool pOrderItems = true, string ano = "")
        {
            try
            {
                return Mapper.Map<List<ItemMasterView>>(mItemMasterManager.GetItemsPrice(Mapper.Map<CompanyConn>(pCc), Mapper.Map<List<ItemMasterSAP>>(ListItems), CardCode, pSession, pOrderItems, ano));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetItemsPrice :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public List<StockModelView> GetItemStock(CompanyView pCv, string[] pItemCode)
        {
            try
            {
                Mapper.CreateMap<StockModel, StockModelView>();
                Mapper.CreateMap<StockModelView, StockModel>();
                Mapper.CreateMap<WarehousePortal, WarehousePortalView>();
                Mapper.CreateMap<WarehousePortalView, WarehousePortal>();

                List<StockModelView> mReturn = Mapper.Map<List<StockModelView>>(
                    mItemMasterManager.GetItemStock(Mapper.Map<CompanyConn>(pCv),
                    pItemCode,
                    Mapper.Map<List<WarehousePortal>>(
                        pCv.CompanyStock.FirstOrDefault().WarehousePortalList)));

                string[] mFormula = pCv.CompanyStock.FirstOrDefault().Formula.Split(' ');

                foreach (StockModelView item in mReturn)
                {
                    item.Available = Util.Miscellaneous.GetAvailable(mFormula,
                        Mapper.Map<StockModel>(item));
                }

                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetItemStock :" + ex.InnerException.Message);
                return new List<StockModelView>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pItemsList"></param>
        /// <returns></returns>
        public List<StockModelView> GetItemWithStock(CompanyView pCv, List<string> pItemsList = null)
        {
            try
            {
                Mapper.CreateMap<WarehousePortal, WarehousePortalView>();
                Mapper.CreateMap<WarehousePortalView, WarehousePortal>();
                List<StockModelView> mReturn = Mapper.Map<List<StockModelView>>(mItemMasterManager.GetItemStock(Mapper.Map<CompanyConn>(pCv), pItemsList.ToArray(), Mapper.Map<List<WarehousePortal>>(pCv.CompanyStock.FirstOrDefault().WarehousePortalList)));
                string[] mFormula = pCv.CompanyStock.FirstOrDefault().Formula.Split(' ');

                foreach (StockModelView item in mReturn)
                {
                    item.Available = Util.Miscellaneous.GetAvailable(mFormula, Mapper.Map<StockModel>(item));
                }

                return mReturn.Where(c => c.Available > 0).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetItemWithStock :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public List<GLAccountView> GetGLAccountList(CompanyView pCc)
        {
            try
            {
                return Mapper.Map<List<GLAccountView>>(mItemMasterManager.GetGLAccountList(Mapper.Map<CompanyConn>(pCc)));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManagerView -> GetGLAccountList :" + ex.InnerException.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pPriceList"></param>
        /// <returns></returns>
        public decimal GetPriceItemByPriceList(CompanyView pCc, string pItemCode, short pPriceList)
        {
            try
            {
                return mItemMasterManager.GetPriceItemByPriceList(Mapper.Map<CompanyConn>(pCc), pItemCode, pPriceList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public List<UnitOfMeasureView> GeUnitOfMeasureByItem(CompanyView pCc, string pItemCode)
        {
            try
            {
                List<UnitOfMeasureView> mUOMView = Mapper.Map<List<UnitOfMeasureView>>(mItemMasterManager.GeItemByCode(Mapper.Map<CompanyConn>(pCc), pItemCode, pCc.IdUserConected).UnitOfMeasureList);

                return mUOMView;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public List<UDF_ARGNS> GetItemMasterUDF(CompanyView pCc, int pUserId)
        {
            try
            {
                return mItemMasterManager.GetItemMasterUDF(Mapper.Map<CompanyConn>(pCc), pUserId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public List<ItemGroupSAP> GetItemGroupList(CompanyView pCc)
        {
            try
            {
                return mItemMasterManager.GetItemGroupList(Mapper.Map<CompanyConn>(pCc));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCodeFrom"></param>
        /// <param name="pItemCodeTo"></param>
        /// <param name="pVendorFrom"></param>
        /// <param name="pVendorTo"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pSelectedWH"></param>
        /// <returns></returns>
        public JsonObjectResult GetInventoryStatus(CompanyView pCc,
            string pItemCodeFrom, string pItemCodeTo,
            string pVendorFrom, string pVendorTo,
            string pItemGroup, string[] pSelectedWH, int pStart, int pLength,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                return mItemMasterManager.GetInventoryStatus(Mapper.Map<CompanyConn>(pCc),
                    pItemCodeFrom, pItemCodeTo, pVendorFrom,
                    pVendorTo, pItemGroup, pSelectedWH,
                    pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        public List<AvailableToPromiseView> GetATPByWarehouse(CompanyView pCc, string pItemCode, string pWarehouse)
        {
            try
            {
                List<AvailableToPromiseView> mItemATPList = Mapper.Map<List<AvailableToPromiseView>>(mItemMasterManager.GetATPByWarehouse(Mapper.Map<CompanyConn>(pCc), pItemCode, pWarehouse));

                return mItemATPList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<WarehouseView> GetAvailableWarehousesByCompany(CompanyView pCv)
        {
            Mapper.CreateMap<Warehouse, WarehouseView>();
            Mapper.CreateMap<WarehouseView, Warehouse>();
            List<WarehouseView> mReturn = Mapper.Map<List<WarehouseView>>(mItemMasterManager.GetAvailableWarehousesByCompany(Mapper.Map<CompanyConn>(pCv), Mapper.Map<List<WarehousePortal>>(pCv.CompanyStock.FirstOrDefault().WarehousePortalList)));

            return mReturn;
        }

        public List<ItemMasterView> GeUOMByItemList(CompanyView pCv, List<ItemMasterView> pListItem, string pSellItem, string pPrchseItem)
        {
            Mapper.CreateMap<ItemMasterSAP, ItemMasterView>();
            Mapper.CreateMap<ItemMasterView, ItemMasterSAP>();
            List<ItemMasterView> mReturnList = Mapper.Map<List<ItemMasterView>>(mItemMasterManager.GeUOMByItemList(Mapper.Map<CompanyConn>(pCv), Mapper.Map<List<ItemMasterSAP>>(pListItem), pSellItem, pPrchseItem));

            return mReturnList;
        }
    }
}


