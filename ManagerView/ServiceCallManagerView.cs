﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ServiceCallManagerView
    {
        private ServiceCallManager mServManager;
        private UserSapManager mUserSapManager;
        private ActivitiesManagerView mActivityManager;
        public ServiceCallManagerView()
        {
            mServManager = new ServiceCallManager();
            mUserSapManager = new UserSapManager();
            mActivityManager = new ActivitiesManagerView();
            Mapper.CreateMap<ServiceCallSAP, ServiceCallView>();
            Mapper.CreateMap<ServiceCallView, ServiceCallSAP>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<EquipmentCardSAP, EquipmentCardView>();
            Mapper.CreateMap<ServiceCallActivities, ServiceCallActivitiesView>();
            Mapper.CreateMap<AttachmentSAP, AttachmentView>();
            Mapper.CreateMap<AttachmentView, AttachmentSAP>();
            Mapper.CreateMap<ServiceCallActivitiesView, ServiceCallActivities>().ForMember(c => c.Activity, x => x.MapFrom(model => model.Activity));

        }

        public ServiceCallView GetServiceCallById(CompanyView pCompanyParam, int pId)
        {
            ServiceCallView ret = null;
            try
            {
                ret = Mapper.Map<ServiceCallView>(mServManager.GetServiceCallById(Mapper.Map<CompanyConn>(pCompanyParam), pId));
                ret.ActivitiesList.ForEach(c => c.Activity = Mapper.Map<ActivitiesView>(mActivityManager.GetActivity(c.ClgID.Value, pCompanyParam)));
                ret.ComboList = mServManager.GetServiceCallComboList(Mapper.Map<CompanyConn>(pCompanyParam));


            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManagerView -> GetServiceCallById:" + ex.InnerException);
                throw;
            }
            return ret;
        }

        public JsonObjectResult AddServiceCall(CompanyView pCompanyParam, ServiceCallView pObject, short? userId = null)
        {
            try
            {
                return mServManager.AddServiceCall(Mapper.Map<CompanyConn>(pCompanyParam), Mapper.Map<ServiceCallSAP>(pObject));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManagerView -> AddServiceCall:" + ex.InnerException);
                throw;
            }
        }

        public string UpdateServiceCall(CompanyView pCompanyParam, ServiceCallView pObject, short? userId = null)
        {
            try
            {
                
                return mServManager.UpdateServiceCall(Mapper.Map<CompanyConn>(pCompanyParam), Mapper.Map<ServiceCallSAP>(pObject));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManagerView -> UpdateServiceCall:" + ex.InnerException);
                throw;
            }
        }

        public List<ServiceCallView> GetServiceCallListSearchByUser(CompanyView pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null)
        {
            try
            {
                UserSAP user = new UserSAP();
                UsersSetting userSetting = mUserSapManager.GetCompany(pCompanyParam.IdCompany, (int)pUser);
                user = mUserSapManager.GetUserSAPSearchByCode(Mapper.Map<CompanyConn>(pCompanyParam), userSetting.UserCodeSAP, "").Single();
                return Mapper.Map<List<ServiceCallView>>(mServManager.GetServiceCallListSearch(Mapper.Map<CompanyConn>(pCompanyParam), pCodeBP, pPriority, pSubject, pRefNumber, pDate, pStatus, user.USERID));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManagerView -> GetServiceCallListSearch:" + ex.InnerException);
                throw;
            }
        }

        public List<ServiceCallView> GetServiceCallListSearch(CompanyView pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null)
        {
            try
            {
                return Mapper.Map<List<ServiceCallView>>(mServManager.GetServiceCallListSearch(Mapper.Map<CompanyConn>(pCompanyParam), pCodeBP, pPriority, pSubject, pRefNumber, pDate, pStatus, pUser));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManagerView -> GetServiceCallListSearch:" + ex.InnerException);
                throw;
            }
        }

        public List<EquipmentCardView> GetEquipmentCardListSearch(CompanyView pCompanyParam, string pItemCode = "", string pItemName = "")
        {
            try
            {
                return Mapper.Map<List<EquipmentCardView>>(mServManager.GetEquipmentCardListSearch(Mapper.Map<CompanyConn>(pCompanyParam), pItemCode, pItemName));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManagerView -> GetServiceCallListSearch:" + ex.InnerException);
                throw;
            }
        }

        public List<ServiceStatus> GetServiceCallStatusList(CompanyView pCompanyParam)
        {
            try
            {
                return mServManager.GetServiceCallStatusList(Mapper.Map<CompanyConn>(pCompanyParam));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManagerView -> GetServiceCallStatusList:" + ex.InnerException);
                throw;
            }
        }

    }
}
