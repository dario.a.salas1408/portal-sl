﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ScaleMasterManagerView
    {

         private ScaleMasterManager mScaleMan;

         public ScaleMasterManagerView()
        {
            mScaleMan = new ScaleMasterManager();
            Mapper.CreateMap<ARGNSScale, ScaleView>();
        }

         public List<ScaleView> GetColorListSearch(CompanyView pCc, string pScaleCode = "", string pScaleName = "")
        {           
            try
            {
                return Mapper.Map<List<ScaleView>>(mScaleMan.GetColorListSearch(Mapper.Map<CompanyConn>(pCc), pScaleCode, pScaleName));             

            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ARGNSSize> GetScaleDescriptionListSearch(CompanyView pCc,string pScaleCode = "")
        {
            try
            {
                return Mapper.Map<List<ARGNSSize>>(mScaleMan.GetScaleDescriptionListSearch(Mapper.Map<CompanyConn>(pCc), pScaleCode));             

            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ARGNSSize> GetSizeByScale(CompanyView pCc, string pScaleCode = "",string pModeCode = "")
        {
            try
            {
                return Mapper.Map<List<ARGNSSize>>(mScaleMan.GetSizeByScale(Mapper.Map<CompanyConn>(pCc), pScaleCode, pModeCode));

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
