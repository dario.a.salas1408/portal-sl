﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class SaleQuotationManagerView
    {
        SaleQuotationManager mSaleQuotationManager;
        UserSapManager mUserSapManager;
        GlobalDocumentManager mGlobalDoc;
        public SaleQuotationManagerView()
        {
            mSaleQuotationManager = new SaleQuotationManager();
            mUserSapManager = new UserSapManager();
            mGlobalDoc = new GlobalDocumentManager();
            Mapper.CreateMap<SalesQuotationSAP, SaleQuotationView>();
            Mapper.CreateMap<SaleQuotationView, SalesQuotationSAP>();
            Mapper.CreateMap<SaleQuotationView, SalesQuotationSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<SalesQuotationSAPLine, SaleQuotationLineView>();
            Mapper.CreateMap<SaleQuotationLineView, SalesQuotationSAPLine>();
            Mapper.CreateMap<SaleQuotationLineView, SalesQuotationSAPLine>();
            Mapper.CreateMap<SalesQuotationSAPLine, SaleQuotationLineView>();
            Mapper.CreateMap<FreightView, Freight>();
            Mapper.CreateMap<Freight, FreightView>();
        }

        public SaleQuotationView GetSaleQuotation(int Id, int pUserId, CompanyView pCv)
        {
            try
            {
                EmployeeSAP mESAP = new EmployeeSAP();
                SalesQuotationSAP mSQ = mSaleQuotationManager.GetSaleQuotation(Mapper.Map<CompanyConn>(pCv), Id, pUserId);
                SaleQuotationView mSQV = Mapper.Map<SaleQuotationView>(mSQ);
                mESAP = mSQ.Employee;
                if (mESAP != null)
                    mSQV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                //if (Id != 0)
                //{
                //    EmployeeSAP mESAP = mUserSapManager.GetEmployeeById(Mapper.Map<CompanyConn>(pCv), mSQ.OwnerCode);
                //    if (mESAP != null)
                //        mSQV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                //}
                return mSQV;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult GetSalesQuotationListSearch(CompanyView pCc, string pCodeVendor = "", 
            DateTime? pDate = null, int? pDocNum = null, 
            string pDocStatus = "", string pOwnerCode = "", 
            string pSECode = "", int pStart = 0, int pLength = 0, 
            OrderColumn pOrderColumn = null)
        {
            try
            {
                return mSaleQuotationManager.GetSalesQuotationListSearch(Mapper.Map<CompanyConn>(pCc), pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
        public List<SaleQuotationLineView> GetSalesQuotationLinesSearch(CompanyView pCc, string[] pDocuments)
        {
            try
            {

                List<SaleQuotationLineView> mReturn = Mapper.Map<List<SaleQuotationLineView>>(mSaleQuotationManager.GetSalesQuotationLinesSearch(Mapper.Map<CompanyConn>(pCc), pDocuments));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSQ"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SaleQuotationView pSQ, CompanyView pCv)
        {
            try
            {
                return mSaleQuotationManager.Add(
                    Mapper.Map<SalesQuotationSAP>(pSQ), 
                    Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesQuotationManagerView -> Add: " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSQ"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public string Update(SaleQuotationView pSQ, CompanyView pCv)
        {
            try
            {
                return mSaleQuotationManager.Update(Mapper.Map<SalesQuotationSAP>(pSQ), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetEmployeeSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SalesQuotationSAP GetSQInternalObjects(CompanyView pCv, SaleQuotationView pSQ)
        {
            try
            {
                return mSaleQuotationManager.GetSQInternalObjects(Mapper.Map<CompanyConn>(pCv), Mapper.Map<SalesQuotationSAP>(pSQ));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FreightView> GetFreights(CompanyView pCv, int pDocEntry)
        {
            try
            {
                return Mapper.Map<List<FreightView>>(mSaleQuotationManager.GetFreights(Mapper.Map<CompanyConn>(pCv), pDocEntry));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RatesSystem> GetListRates(CompanyView pCv, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mSaleQuotationManager.GetListRates(Mapper.Map<CompanyConn>(pCv), mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyView pCv)
        {
            try
            {
                return mGlobalDoc.GetTaxCode(Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
