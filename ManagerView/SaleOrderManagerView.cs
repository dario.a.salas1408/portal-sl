﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.DiverSO;
using ARGNS.Model.Implementations.DiverSO.Catalog;
using ARGNS.Model.Implementations.DiverSO.SalesDocProcess;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace ARGNS.ManagerView
{
    public class SaleOrderManagerView
    {
        SaleOrderManager mSaleOrderManager;
        UserSapManager mUserSapManager;
        GlobalDocumentManager mGlobalDoc;
        public SaleOrderManagerView()
        {
            mSaleOrderManager = new SaleOrderManager();
            mUserSapManager = new UserSapManager();
            mGlobalDoc = new GlobalDocumentManager();
            Mapper.CreateMap<SaleOrderSAP, SaleOrderView>();
            Mapper.CreateMap<SaleOrderView, SaleOrderSAP>();
            Mapper.CreateMap<SaleOrderView, SaleOrderSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<SaleOrderSAPLine, SaleOrderLineView>();
            Mapper.CreateMap<SaleOrderLineView, SaleOrderSAPLine>();
            Mapper.CreateMap<SaleOrderLineView, SaleOrderSAPLine>();
            Mapper.CreateMap<SaleOrderSAPLine, SaleOrderLineView>();
            Mapper.CreateMap<FreightView, Freight>();
            Mapper.CreateMap<Freight, FreightView>();
            Mapper.CreateMap<AttachmentView, AttachmentSAP>();
            Mapper.CreateMap<AttachmentSAP, AttachmentView>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pUserId"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public SaleOrderView GetSaleOrder(int pId, int pUserId, CompanyView pCv, bool pShowOpenQuantity)
        {
            try
            {
                EmployeeSAP mESAP = new EmployeeSAP();
                SaleOrderSAP mSO = mSaleOrderManager.GetSaleOrder(
                    Mapper.Map<CompanyConn>(pCv),
                    pId, pUserId, pShowOpenQuantity);

                SaleOrderView mSOV = Mapper.Map<SaleOrderView>(mSO);
                mESAP = mSO.Employee;
                if (mESAP != null)
                    mSOV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                return mSOV;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SOManagerView -> GetSaleOrderById :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCv"></param>
        /// <param name="pFrom"></param>
        /// <param name="pUrlFrom"></param>
        /// <param name="pQuickOrderId"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SaleOrderView pSO, CompanyView pCv, string pFrom, string pUrlFrom, string pQuickOrderId = "")
        {
            try
            {
                return mSaleOrderManager.Add(Mapper.Map<SaleOrderSAP>(pSO), Mapper.Map<CompanyConn>(pCv), pFrom, pUrlFrom, pQuickOrderId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesOrderManagerView -> Add: " + ex.Message); 
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public string Update(SaleOrderView pSO, CompanyView pCv)
        {
            try
            {
                return mSaleOrderManager.Update(Mapper.Map<SaleOrderSAP>(pSO), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesOrderListSearch(CompanyView pCc,
            bool pShowOpenQuantityStatus, string pCodeVendor = "", 
            DateTime? pDate = null, int? pDocNum = null, 
            string pDocStatus = "", string pOwnerCode = "", 
            string pSECode = "", int pStart = 0, int pLength = 0, 
            OrderColumn pOrderColumn = null)
        {
            try
            {
                return mSaleOrderManager.GetSalesOrderListSearch(
                    Mapper.Map<CompanyConn>(pCc), pShowOpenQuantityStatus, 
                    pCodeVendor, pDate, 
                    pDocNum, pDocStatus, pOwnerCode, pSECode, 
                    pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManagerView -> GetSalesOrderListSearch: " + ex.Message);
                Logger.WriteError("SaleOrderManagerView -> GetSalesOrderListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pSO"></param>
        /// <returns></returns>
        public SaleOrderSAP GetSOInternalObjects(CompanyView pCv, SaleOrderView pSO)
        {
            try
            {
                return mSaleOrderManager.GetSOInternalObjects(Mapper.Map<CompanyConn>(pCv), Mapper.Map<SaleOrderSAP>(pSO));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pDocEntry"></param>
        /// <returns></returns>
        public List<FreightView> GetFreights(CompanyView pCv, int pDocEntry)
        {
            try
            {
                return Mapper.Map<List<FreightView>>(mSaleOrderManager.GetFreights(Mapper.Map<CompanyConn>(pCv), pDocEntry));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="mListCurrency"></param>
        /// <param name="DocDate"></param>
        /// <param name="pLocalCurrency"></param>
        /// <param name="pSystemCurrency"></param>
        /// <returns></returns>
        public List<RatesSystem> GetListRates(CompanyView pCv, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mSaleOrderManager.GetListRates(Mapper.Map<CompanyConn>(pCv), mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyView pCv)
        {
            try
            {
                return mGlobalDoc.GetTaxCode(Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetEmployeeSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DiverSOSalesDocProcessResult GetPromotionItemsByOrder(SaleOrderView pSalesOrder)
        {
            try
            {
                return mSaleOrderManager.GetPromotionItemsByOrder(Mapper.Map<SaleOrderSAP>(pSalesOrder));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DiverSOCatalogResult GetDSOCatalogs(string pCatalogCode, string pCatalogName, string pCardCode, string pSlpCode, DateTime pDate, bool pIncludeItems, string pItemCode, string pItemName, string pItemGroup, int pStart = 0, int pLength = 0)
        {
            try
            {
                return mSaleOrderManager.GetDSOCatalogs(pCatalogCode, pCatalogName, pCardCode, pSlpCode, pDate, pIncludeItems, pItemCode, pItemName, pItemGroup, pStart, pLength);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
    }
}
