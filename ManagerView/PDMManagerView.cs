﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PDMManagerView
    {
        PDMManager mPDMManager;
        UserSapManager mUserSapManager;
        ItemMasterManager mItemMasterManager;
        public PDMManagerView()
        {
            mPDMManager = new PDMManager();
            mUserSapManager = new UserSapManager();
            mItemMasterManager = new ItemMasterManager();

            Mapper.CreateMap<ARGNSModel, ARGNSModelView>();
            Mapper.CreateMap<ARGNSModelView, ARGNSModel>();
            Mapper.CreateMap<ARGNSModelView, ARGNSModel>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<ARGNSModelFile, ARGNSModelFileView>();
            Mapper.CreateMap<ARGNSCrPath, ARGNSCrPathView>();
            Mapper.CreateMap<ARGNSModelPom, ARGNSModelPomView>();
            Mapper.CreateMap<ARGNSCrPath, ARGNSCrPathView>();
            Mapper.CreateMap<ARGNSCrPathActivities, ARGNSCrPathActivitiesView>();
            Mapper.CreateMap<SkuModel, SkuModelView>();
            Mapper.CreateMap<ARGNSModelImg, ARGNSModelImgView>();
            Mapper.CreateMap<ModelDesc, ModelDescView>();
            Mapper.CreateMap<ARGNSModelHistory, ARGNSModelHistoryView>();
            Mapper.CreateMap<SAPLogs, SAPLogsView>();
            Mapper.CreateMap<ARGNSSegmentation, ARGNSSegmentationView>();
            Mapper.CreateMap<ARGNSSegmentationView, ARGNSSegmentation>();
            Mapper.CreateMap<ActivitiesSAP, ActivitiesView>();
            Mapper.CreateMap<ActivitiesView, ActivitiesSAP>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="pModCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public ARGNSModelView GetModel(string Id, string pModCode, int pUserId, CompanyView pCv)
        {
            try
            {
                ARGNSModel mModel = mPDMManager.GetModel(Mapper.Map<CompanyConn>(pCv), Id, pModCode, pUserId);
                if (mModel.MappedUdf != null)
                {
                    foreach (UDF_ARGNS mUDF in mModel.MappedUdf)
                    {
                        if (!string.IsNullOrEmpty(mUDF.RTable))
                        {
                            mUDF.ListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(mUserSapManager.GetQueryResult(Mapper.Map<CompanyConn>(pCv), (pCv.ServerType == 9 ? "SELECT \"Code\" AS \"FldValue\", \"Name\" AS \"Descr\" FROM \"" + pCv.CompanyDB + "\".\"@" + mUDF.RTable + "\"" : "Select Code as FldValue, Name as Descr From [@" + mUDF.RTable + "]")));
                        }
                    }
                }
                return Mapper.Map<ARGNSModelView>(mModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public MatrixModelView GetMatrixModelView(CompanyView pCv, string Id,
            List<ARGNS.Model.Implementations.Portal.WarehousePortal> pWarehousePortalList)
        {
            MatrixModelView ret = null;
            try
            {
                ret = new MatrixModelView();
                List<ARGNSModelImg> pImgList = new List<ARGNSModelImg>();
                ret.SkuModelList = Mapper.Map<List<SkuModelView>>(
                    mPDMManager.GetSkuModelList(Mapper.Map<CompanyConn>(pCv), Id, ref pImgList));
                ret.ModelImagesList = Mapper.Map<List<ARGNSModelImgView>>(pImgList);

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManagerView --> GetMatrixModelView" + ex.Message);
                return new MatrixModelView();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pCode"></param>
        /// <param name="pU_ModCode"></param>
        /// <returns></returns>
        public List<ARGNSModelFile> GetModelFileList(CompanyView pCv, string pCode, string pU_ModCode)
        {
            try
            {
                return mPDMManager.GetModelFileList(Mapper.Map<CompanyConn>(pCv), pCode, pU_ModCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModel"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public string UpdateModel(ARGNSModelView pModel, CompanyView pCv)
        {
            try
            {
                return mPDMManager.UpdateModel(Mapper.Map<ARGNSModel>(pModel), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModel"></param>
        /// <param name="pCv"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        public JsonObjectResult AddModel(ARGNSModelView pModel, CompanyView pCv, string pFrom)
        {
            try
            {
                return mPDMManager.AddModel(Mapper.Map<ARGNSModel>(pModel), Mapper.Map<CompanyConn>(pCv), pFrom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <returns></returns>
        public List<ARGNSModelView> GetPDMListSearch(CompanyView pCc, ref PDMSAPCombo mPDMSAPCombo,
            string pCodeModel = "", string pNameModel = "",
            string pSeasonModel = "", string pGroupModel = "",
            string pBrand = "", string pCollection = "",
            string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            try
            {
                List<ARGNSModelView> mReturn = Mapper.Map<List<ARGNSModelView>>(
                    mPDMManager.GetPDMListSearch(Mapper.Map<CompanyConn>(pCc),
                        ref mPDMSAPCombo, pCodeModel, pNameModel,
                        pSeasonModel, pGroupModel, pBrand, pCollection,
                        pSubCollection, pStart, pLength));

                mReturn.ForEach(c => c.ImageName = GetImageName(c.U_Pic));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <param name="pCatalogCode"></param>
        /// <returns></returns>
        public JsonObjectResult GetPDMListDesc(CompanyView pCc,
            List<WarehousePortal> pWarehousePortalList,
            bool pHasPermissionToViewStock, int pStart, int pLength,
            string pCodeModel = "", string pNameModel = "", string pSeasonModel = "",
            string pGroupModel = "", string pBrand = "", string pCollection = "",
            string pSubCollection = "", string pCatalogCode = "")
        {
            try
            {

                JsonObjectResult mJsonObjectResult = mPDMManager
                    .GetPDMListDesc(Mapper.Map<CompanyConn>(pCc),
                        pStart, pLength,
                        pCodeModel, pNameModel, pSeasonModel, pGroupModel, pBrand,
                        pCollection, pSubCollection, pCatalogCode);

                if(mJsonObjectResult.ModelDescViewList != null)
                {
                    mJsonObjectResult.ModelDescViewList.ForEach(c => c.ImageName = GetImageName(c.ModelImage));
                }

                

                if (pHasPermissionToViewStock)
                {
                    string[] mFormula = pCc.CompanyStock.FirstOrDefault().Formula.Split(' ');

                    foreach (var item in mJsonObjectResult.ModelDescViewList)
                    {
                        item.HasPermissionToViewStock = pHasPermissionToViewStock;

                        StockModel stockModel = mItemMasterManager.GetStockByModel(Mapper.Map<CompanyConn>(pCc),
                            item.ModelCode, pWarehousePortalList);

                        item.Stock = Util.Miscellaneous.GetAvailable(mFormula,
                                Mapper.Map<StockModel>(stockModel), false);
                        item.StockPrepack = Util.Miscellaneous.GetAvailable(mFormula,
                                Mapper.Map<StockModel>(stockModel), true);
                    }
                }
                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("GetPDMListDesc --> GetPDMListDesc: " + ex.Message);
                return new JsonObjectResult();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public ModelDescView GetModelDesc(CompanyView pCc, string pModCode)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ModelDesc, ModelDescView>(); }).CreateMapper();

                return mapper.Map<ModelDescView>(mPDMManager.GetModelDesc(Mapper.Map<CompanyConn>(pCc), pModCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetImageName(string pPath)
        {
            string ret = string.Empty;
            string[] mImageName;
            try
            {
                if (!string.IsNullOrEmpty(pPath))
                {
                    mImageName = pPath.Split(new Char[] { '\\' });
                    ret = mImageName[mImageName.Length - 1];

                }

                return ret;
            }
            catch (Exception)
            {
                return string.Empty;
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public List<ARGNSCrPathView> GetModelProjectList(CompanyView pCc, string pModCode)
        {
            try
            {
                List<ARGNSCrPathView> mProjectList = Mapper.Map<List<ARGNSCrPathView>>(mPDMManager.GetModelProjectList(Mapper.Map<CompanyConn>(pCc), pModCode));
                return mProjectList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public List<ARGNSModelPomView> GetModelPomList(CompanyView pCc, string pModCode)
        {
            try
            {
                List<ARGNSModelPomView> mModelPomList = Mapper.Map<List<ARGNSModelPomView>>(mPDMManager.GetModelPomList(Mapper.Map<CompanyConn>(pCc), pModCode));
                return mModelPomList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSModelPomTemplate> GetModelPomTemplateList(CompanyView pCc, List<string> pSclcodeList, List<string> pPomCodeList)
        {
            try
            {
                return mPDMManager.GetModelPomTemplateList(Mapper.Map<CompanyConn>(pCc), pSclcodeList, pPomCodeList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSPomTemplateLines> GetModelPomTemplateLines(CompanyView pCc, string pPOMCode)
        {
            try
            {
                return mPDMManager.GetModelPomTemplateLines(Mapper.Map<CompanyConn>(pCc), pPOMCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSCrPathView> GetCriticalPathList(CompanyView pCc, string pCollCode = "", string pSubCollCode = "", string pSeasonCode = "", string pModelCode = "", string pProjectCode = "", string pVendorCode = "", string pCustomerCode = "")
        {
            List<ARGNSCrPathView> ret = null;
            try
            {
                ret = Mapper.Map<List<ARGNSCrPathView>>(mPDMManager.GetCriticalPathListSearch(Mapper.Map<CompanyConn>(pCc), pCollCode, pSubCollCode, pSeasonCode, pModelCode, pProjectCode, pVendorCode, pCustomerCode));


                foreach (ARGNSCrPathView item in ret)
                {

                    item.ImageName = GetImageName(item.ModelPicture);

                    foreach (ARGNSCrPathActivitiesView mAct in item.ActivitiesList)
                    {

                        if (string.IsNullOrEmpty(mAct.U_ActSAP))//No Planeadas
                        {
                            mAct.LabelType = "warning";
                        }
                        else if (mAct.Closed)//Cerradas
                        {
                            mAct.LabelType = "success";
                        }
                        else if (mAct.U_CDate.HasValue && mAct.U_CDate.Value < DateTime.Today) //Vencidas
                        {
                            mAct.LabelType = "danger";
                        }
                        else //en Progreso
                        {
                            mAct.LabelType = "primary";
                        }
                    }

                }


                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSModelHistoryView> GetModelHistoryList(CompanyView pCc, string pModCode)
        {
            try
            {
                return Mapper.Map<List<ARGNSModelHistoryView>>(mPDMManager.GetModelHistoryList(Mapper.Map<CompanyConn>(pCc), pModCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SAPLogsView> GetSAPModelLogs(CompanyView pCompanyParam, string pModelCode = "", string pLogInstList = "")
        {
            try
            {
                return Mapper.Map<List<SAPLogsView>>(mPDMManager.GetSAPModelLogs(Mapper.Map<CompanyConn>(pCompanyParam), pModelCode, pLogInstList));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ARGNSSegmentationView GetSegmentationById(CompanyView pCompanyParam, string pCode)
        {
            try
            {
                return Mapper.Map<ARGNSSegmentationView>(mPDMManager.GetSegmentationById(Mapper.Map<CompanyConn>(pCompanyParam), pCode));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCatalogCode"></param>
        /// <param name="pCatalogName"></param>
        /// <param name="pSalesEmployee"></param>
        /// <returns></returns>
        public List<ARGNSCatalog> GetCatalogListSearch(CompanyView pCc, string pCatalogCode = "", string pCatalogName = "", int? pSalesEmployee = null)
        {
            try
            {
                List<ARGNSCatalog> mReturn = mPDMManager.GetCatalogListSearch(Mapper.Map<CompanyConn>(pCc), pCatalogCode, pCatalogName, pSalesEmployee);
                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManagerView --> GetCatalogListSearch: " + ex.Message);
                return new List<ARGNSCatalog>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public MatrixModelView GetModelFromCatalog(CompanyView pCv, string Id,
            List<ARGNS.Model.Implementations.Portal.WarehousePortal> pWarehousePortalList,
            bool pHasPermissionToViewStock)
        {
            MatrixModelView ret = null;
            List<ARGNSModelImg> pImgList = new List<ARGNSModelImg>();

            try
            {
                IMapper mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<ARGNSPrepack, ARGNSPrepackView>();
                    cfg.CreateMap<ARGNSPrepackLines, ARGNSPrepackLinesView>();
                    cfg.CreateMap<ARGNSSizeRun, ARGNSSizeRunView>();
                    cfg.CreateMap<ARGNSSizeRunLine, ARGNSSizeRunLineView>();
                })
                    .CreateMapper();

                ret = new MatrixModelView();

                ret.SkuModelList = Mapper.Map<List<SkuModelView>>(
                    mPDMManager.GetSkuModelList(Mapper.Map<CompanyConn>(pCv),
                    Id, ref pImgList));

                ret.ModelImagesList = Mapper.Map<List<ARGNSModelImgView>>(pImgList);
                if (ret.SkuModelList.Count > 0)
                {
                    ret.SizeRunList = mapper.Map<List<ARGNSSizeRunView>>(mPDMManager.GetSizeRunsByScale(Mapper.Map<CompanyConn>(pCv), ret.SkuModelList.FirstOrDefault().U_ARGNS_SCL));
                }

                string[] itemCodeList = ret.SkuModelList.Select(s => s.ItemCode).ToArray();

                List<StockModelView> mReturn = Mapper.Map<List<StockModelView>>(
                    mItemMasterManager.GetItemStock(Mapper.Map<CompanyConn>(pCv),
                    itemCodeList,
                    Mapper.Map<List<WarehousePortal>>(pCv.CompanyStock.FirstOrDefault().WarehousePortalList)));

                if (pHasPermissionToViewStock)
                {
                    foreach (var item in mReturn)
                    {
                        string[] mFormula = pCv.CompanyStock.FirstOrDefault().Formula.Split(' ');
                        ret.SkuModelList.Where(w => w.ItemCode == item.ItemCode)
                            .FirstOrDefault().HasStock = Util.Miscellaneous.GetAvailable(mFormula,
                            Mapper.Map<StockModel>(item)) > 0;
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManagerView --> GetModelFromCatalog" + ex.Message);
                return new MatrixModelView();
            }
        }
    }
}
