﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PaymentMeanManagerView
    {
        private PaymentMeanManager paymentMeanManager;

        public PaymentMeanManagerView()
        {
            paymentMeanManager = new PaymentMeanManager();
        }

        public List<PaymentMean> GetPaymentMeans(CompanyView companyView, string branchCode)
        {
            try
            {
                var paymentMeans = paymentMeanManager.GetPaymentMeans(Mapper.Map<CompanyConn>(companyView), branchCode);

                return paymentMeans;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<PaymentMeanType> GetPaymentMeanType(CompanyView companyView)
        {
            try
            {
                var paymentMeanType = paymentMeanManager.GetPaymentMeanType(Mapper.Map<CompanyConn>(companyView));

                return paymentMeanType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<PaymentMeanOrder> GetForDraft(CompanyView companyParam, int code)
        {
            try
            {
                return paymentMeanManager.GetForDraft(Mapper.Map<CompanyConn>(companyParam), code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
