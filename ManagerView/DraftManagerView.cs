﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class DraftManagerView
    {
        private DraftManager mDManager;
        private UserSapManager mUserSapManager;

        public DraftManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<DraftPortalView, DraftPortal>();
            Mapper.CreateMap<DraftPortal, DraftPortalView>();
            Mapper.CreateMap<DraftView, Draft>();
            Mapper.CreateMap<Draft, DraftView>();
            Mapper.CreateMap<DraftLineView, DraftLine>();
            Mapper.CreateMap<DraftLine, DraftLineView>();
            Mapper.CreateMap<PurchaseRequestView, PurchaseRequestSAP>();
            Mapper.CreateMap<PurchaseRequestSAP, PurchaseRequestView>();
            Mapper.CreateMap<DocumentConfirmationLineView, DocumentConfirmationLines>();
            Mapper.CreateMap<DocumentConfirmationLines, DocumentConfirmationLineView>();
            Mapper.CreateMap<StageView, StageSAP>();
            Mapper.CreateMap<StageSAP, StageView>();
            mDManager = new DraftManager();
            mUserSapManager = new UserSapManager();
        }

        public JsonObjectResult GetPurchaseUserDraftListSearch(CompanyView pCc, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);

            UserSAP user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

            return mDManager.GetPurchaseUserDraftListSearch(Mapper.Map<CompanyConn>(pCc), user.USERID, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
        }

        public JsonObjectResult GetPurchaseCustomerDraftListSearch(CompanyView pCc, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            return mDManager.GetPurchaseCustomerDraftListSearch(Mapper.Map<CompanyConn>(pCc), pBP, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
        }

        public JsonObjectResult GetPurchaseSalesEmployeeDraftListSearch(CompanyView pCc, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            return mDManager.GetPurchaseSalesEmployeeDraftListSearch(Mapper.Map<CompanyConn>(pCc), pSalesEmployee, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
        }

        public JsonObjectResult GetSalesUserDraftListSearch(CompanyView pCc, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);

                UserSAP user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

                return mDManager.GetSalesUserDraftListSearch(Mapper.Map<CompanyConn>(pCc), user.USERID, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult GetSalesCustomerDraftListSearch(CompanyView pCc, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            return mDManager.GetSalesCustomerDraftListSearch(Mapper.Map<CompanyConn>(pCc), pBP, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
        }

        public JsonObjectResult GetSalesSalesEmployeeDraftListSearch(CompanyView pCc, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            return mDManager.GetSalesSalesEmployeeDraftListSearch(Mapper.Map<CompanyConn>(pCc), pSalesEmployee, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
        }

        public DraftView GetDraftById(int pCode, CompanyView pCv)
        {
            try
            {
                DraftView mDraft = Mapper.Map<DraftView>(mDManager.GetDraftById(Mapper.Map<CompanyConn>(pCv), pCode));

                return mDraft;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool CreateDocument(CompanyView pCv, int DraftDocEntry)
        {
            try
            {
                return mDManager.CreateDocument(Mapper.Map<CompanyConn>(pCv), pCv.ResultConnection, DraftDocEntry);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DocumentConfirmationLineView> GetApprovalListByDocumentId(CompanyView pCv, int pDocEntry, string pObjType)
        {
            try
            {
                return Mapper.Map<List<DocumentConfirmationLineView>>(mDManager.GetApprovalListByDocumentId(Mapper.Map<CompanyConn>(pCv), pDocEntry, pObjType));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StageView> GetStageList(CompanyView pCv)
        {
            try
            {
                return Mapper.Map<List<StageView>>(mDManager.GetStageList(Mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
