﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PurchaseQuotationManagerView
    {
        PurchaseQuotationManager mPurchaseQuotationManager;
        UserSapManager mUserSapManager;
        GlobalDocumentManager mGlobalDoc;

        public PurchaseQuotationManagerView()
        {
            mPurchaseQuotationManager = new PurchaseQuotationManager();
            mUserSapManager = new UserSapManager();
            mGlobalDoc = new GlobalDocumentManager();

            Mapper.CreateMap<PurchaseQuotationSAP, PurchaseQuotationView>();
            Mapper.CreateMap<PurchaseQuotationView, PurchaseQuotationSAP>();
            Mapper.CreateMap<PurchaseQuotationView, PurchaseQuotationSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<PurchaseQuotationSAPLine, PurchaseQuotationLineView>();
            Mapper.CreateMap<PurchaseQuotationLineView, PurchaseQuotationSAPLine>();
            Mapper.CreateMap<PurchaseQuotationLineView, PurchaseQuotationSAPLine>();
            Mapper.CreateMap<PurchaseQuotationSAPLine, PurchaseQuotationLineView>();
            Mapper.CreateMap<PurchaseQuotationSAP, PurchaseQuotationView>();
            Mapper.CreateMap<FreightView, Freight>();
            Mapper.CreateMap<Freight, FreightView>();
            Mapper.CreateMap<AttachmentView, AttachmentSAP>();
            Mapper.CreateMap<AttachmentSAP, AttachmentView>();
        }

        public PurchaseQuotationView GetPurchaseQuotation(int Id, int pUserId, CompanyView pCv)
        {
            try
            {
                EmployeeSAP mESAP = new EmployeeSAP();
                PurchaseQuotationSAP mPQ = mPurchaseQuotationManager.GetPurchaseQuotation(Mapper.Map<CompanyConn>(pCv), Id, pUserId);
                PurchaseQuotationView mPQV = Mapper.Map<PurchaseQuotationView>(mPQ);
                mESAP = mPQ.Employee;
                //if (Id != 0)
                //{
                //    EmployeeSAP mESAP = mUserSapManager.GetEmployeeById(Mapper.Map<CompanyConn>(pCv), mPQ.OwnerCode);
                //    if (mESAP != null)
                //        mPQV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                //}

                if (mESAP != null)
                    mPQV.OwnerName = mESAP.firstName + " " + mESAP.lastName;

                return mPQV;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(PurchaseQuotationView pPQ, CompanyView pCv)
        {
            try
            {
                return mPurchaseQuotationManager.Add(Mapper.Map<PurchaseQuotationSAP>(pPQ), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(PurchaseQuotationView pPQ, CompanyView pCv)
        {
            try
            {
                return mPurchaseQuotationManager.Update(Mapper.Map<PurchaseQuotationSAP>(pPQ), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult GetPurchaseQuotationListSearch(CompanyView pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                JsonObjectResult mReturn = mPurchaseQuotationManager.GetPurchaseQuotationListSearch(Mapper.Map<CompanyConn>(pCc), pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PurchaseQuotationLineView> GetPurchaseQuotationLinesSearch(CompanyView pCc, string[] pDocuments)
        {
            try
            {

                List<PurchaseQuotationLineView> mReturn = Mapper.Map<List<PurchaseQuotationLineView>>(mPurchaseQuotationManager.GetPurchaseQuotationLinesSearch(Mapper.Map<CompanyConn>(pCc), pDocuments));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public PurchaseQuotationSAP GetPQInternalObjects(CompanyView pCv, PurchaseQuotationView pPQ)
        {
            try
            {
                return mPurchaseQuotationManager.GetPQInternalObjects(Mapper.Map<CompanyConn>(pCv), Mapper.Map<PurchaseQuotationSAP>(pPQ));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetEmployeeSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FreightView> GetFreights(CompanyView pCv, int pDocEntry)
        {
            try
            {
                return Mapper.Map<List<FreightView>>(mPurchaseQuotationManager.GetFreights(Mapper.Map<CompanyConn>(pCv), pDocEntry));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RatesSystem> GetListRates(CompanyView pCv, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mPurchaseQuotationManager.GetListRates(Mapper.Map<CompanyConn>(pCv), mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyView pCv)
        {
            try
            {
                return mGlobalDoc.GetTaxCode(Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
