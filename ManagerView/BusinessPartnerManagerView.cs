﻿using System;
using System.Collections.Generic;
using ARGNS.Manager;
using AutoMapper;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using ARGNS.Util;
using ARGNS.Model.Implementations.View;

namespace ARGNS.ManagerView
{
    public class BusinessPartnerManagerView
    {
        BusinessPartnerManager mBusinessPartnerManager;

        public BusinessPartnerManagerView()
        {
            mBusinessPartnerManager = new BusinessPartnerManager();
            Mapper.CreateMap<BusinessPartnerSAP, BusinessPartnerView>();
            Mapper.CreateMap<BusinessPartnerView, BusinessPartnerSAP>();
            Mapper.CreateMap<CustomerAgingReportLineView, CustomerAgingReportLine>();
            Mapper.CreateMap<CustomerAgingReportLine, CustomerAgingReportLineView>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="pCv"></param>
        /// <param name="pUserId"></param>
        /// <param name="LocalCurrency"></param>
        /// <returns></returns>
        public BusinessPartnerView GetBusinessPartner(string Id, CompanyView pCv, int pUserId, string LocalCurrency = "")
        {
            try
            {
                return Mapper.Map<BusinessPartnerView>(mBusinessPartnerManager.GetBusinessPartner(Mapper.Map<CompanyConn>(pCv), Id, pUserId, LocalCurrency));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<BusinessPartnerView> GetBusinessPartners(CompanyView pCv)
        {
            try
            {

                return Mapper.Map<List<BusinessPartnerView>>(mBusinessPartnerManager.GetBusinessPartners(Mapper.Map<CompanyConn>(pCv)));

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public List<BusinessPartnerView> GetBusinessPartners(CompanyView pCv, Enums.BpType pType)
        {
            try
            {
                return Mapper.Map<List<BusinessPartnerView>>(mBusinessPartnerManager.GetBusinessPartners(Mapper.Map<CompanyConn>(pCv), pType));
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBp"></param>
        /// <param name="pCv"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public string Add(BusinessPartnerView pBp, CompanyView pCv, string idUser)
        {
            try
            {
                return mBusinessPartnerManager.Add(Mapper.Map<BusinessPartnerSAP>(pBp), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBp"></param>
        /// <param name="pCv"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public string Update(BusinessPartnerView pBp, CompanyView pCv, string idUser)
        {
            try
            {
                return mBusinessPartnerManager.Update(Mapper.Map<BusinessPartnerSAP>(pBp), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pTypeBP"></param>
        /// <returns></returns>
        public List<BusinessPartnerView> GetBusinessPartnerListSearch(CompanyView pCc, string pCodeBP = "", string pNameBP = "", string pTypeBP = "")
        {
            try
            {
                List<BusinessPartnerView> mReturn = Mapper.Map<List<BusinessPartnerView>>(mBusinessPartnerManager.GetBusinessPartnerListSearch(Mapper.Map<CompanyConn>(pCc), pCodeBP, pNameBP, pTypeBP));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCountry"></param>
        /// <returns></returns>
        public List<StateSAP> GetStateList(CompanyView pCc, string pCountry)
        {
            try
            {
                List<StateSAP> mReturn = mBusinessPartnerManager.GetStateList(Mapper.Map<CompanyConn>(pCc), pCountry);

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="mBPListToFilter"></param>
        /// <returns></returns>
        public JsonObjectResult GetCustomerAgingReport(CompanyView pCc, List<string> mBPListToFilter)
        {
            return mBusinessPartnerManager.GetCustomerAgingReport(Mapper.Map<CompanyConn>(pCc), mBPListToFilter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pType"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pFilterUser"></param>
        /// <param name="pSECode"></param>
        /// <param name="pBPGroupId"></param>
        /// <param name="pGetLeads"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetBusinessPartnerListByUser(CompanyView pCv, Enums.BpType pType, 
            bool pOnlyActiveItems, 
            string pCodeBP = "", string pNameBP = "", bool pFilterUser = false, 
            int? pSECode = null, int? pBPGroupId = null, bool pGetLeads = false, 
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null, bool pFilterByBP = false, string licTracNum = "")
        {
            try
            {
                return mBusinessPartnerManager.GetBusinessPartnerListByUser(
                    Mapper.Map<CompanyConn>(pCv), pType, pOnlyActiveItems, pCodeBP, 
                    pNameBP, pFilterUser, pSECode, pBPGroupId, pGetLeads, pStart, pLength, pOrderColumn, pFilterByBP, licTracNum);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManagerView ----> GetBusinessPartnerListByUser: " + ex.Message);
                throw ex;
            }
        }

        public BusinessPartnerView GetBusinessPartnerMinimalData(CompanyView pCv, string pCardCode)
        {
            try
            {
                return Mapper.Map<BusinessPartnerView>(mBusinessPartnerManager.GetBusinessPartnerMinimalData(Mapper.Map<CompanyConn>(pCv), pCardCode));
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManagerView -> GetBusinessPartnerMinimalData:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
