﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class SalesInvoiceManagerView
    {
        SalesInvoiceManager mSalesInvoiceManager;
        UserSapManager mUserSapManager;

        public SalesInvoiceManagerView()
        {
            mSalesInvoiceManager = new SalesInvoiceManager();
            mUserSapManager = new UserSapManager();

            Mapper.CreateMap<SalesInvoiceSAP, SalesInvoiceView>();
            Mapper.CreateMap<SalesInvoiceView, SalesInvoiceSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<SalesInvoiceLineSAP, SalesInvoiceLineView>();
            Mapper.CreateMap<SalesInvoiceLineView, SalesInvoiceLineSAP>();


        }

        public SalesInvoiceView GetSalesInvoice(int Id, int pUserId, CompanyView pCv)
        {
            try
            {
                EmployeeSAP mESAP = new EmployeeSAP();
                SalesInvoiceSAP mSI = mSalesInvoiceManager.GetSalesInvoice(Mapper.Map<CompanyConn>(pCv), Id, pUserId);
                SalesInvoiceView mSIV = Mapper.Map<SalesInvoiceView>(mSI);
                mESAP = mSI.Employee;
                //if (Id != 0)
                //{
                //    EmployeeSAP mESAP = mUserSapManager.GetEmployeeById(Mapper.Map<CompanyConn>(pCv), mSI.OwnerCode);
                //    if (mESAP != null)
                //        mSIV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                //}
                if (mESAP != null)
                    mSIV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                return mSIV;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonObjectResult GetSalesInvoiceListSearch(CompanyView pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mSalesInvoiceManager.GetSalesInvoiceListSearch(Mapper.Map<CompanyConn>(pCc), pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetEmployeeSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
