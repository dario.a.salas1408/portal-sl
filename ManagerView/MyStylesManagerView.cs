﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace ARGNS.ManagerView
{
	public class MyStylesManagerView
    {
        private MyStylesManager mMSManager;
        private UserSapManager mUserSapManager;
        public MyStylesManagerView()
        {
            mMSManager = new MyStylesManager();
            mUserSapManager = new UserSapManager();
            Mapper.CreateMap<ARGNSModel, ARGNSModelView>();
            Mapper.CreateMap<ARGNSModelView, ARGNSModel>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<ARGNSModelImg, ARGNSModelImgView>();
            Mapper.CreateMap<ARGNSCostSheet, ARGNSCostSheetView>();

            Mapper.CreateMap<ARGNSCrPath, ARGNSCrPathView>();
            Mapper.CreateMap<ARGNSSegmentation, ARGNSSegmentationView>();
            Mapper.CreateMap<ARGNSSegmentationView, ARGNSSegmentation>();
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pUserSign"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <returns></returns>
        public JsonObjectResult GetUserPDMListSearch(CompanyView pCc,
            int pUserSign,
            ref PDMSAPCombo mPDMSAPCombo,
            string pCodeModel = "",
            string pNameModel = "",
            string pSeasonModel = "",
            string pGroupModel = "",
            string pBrand = "",
            string pCollection = "",
            string pSubCollection = "",
            int pStart = 0,
            int pLength = 0)
        {
            try
            {
                //TODO CHANGE THAT
                UserSAP user = new UserSAP();

                UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);
                user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                if (user != null)
                {
                    mJsonObjectResult = mMSManager.GetUserPDMListSearch(
                        Mapper.Map<CompanyConn>(pCc), user.USER_CODE, ref mPDMSAPCombo,
                        pCodeModel, pNameModel, pSeasonModel,
                        pGroupModel, pBrand,
                        pCollection, pSubCollection, pStart, pLength);

                    mJsonObjectResult.ARGNSModelList.ForEach(c => c.ImageName = GetImageName(c.U_Pic));
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pBPCode"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <returns></returns>
        public List<ARGNSModelView> GetBPPDMListSearch(CompanyView pCc, string pBPCode, 
			ref PDMSAPCombo mPDMSAPCombo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "", int pStart = 0, int pLength = 0)
        {
            try
            {
                List<ARGNSModelView> mReturn = Mapper.Map<List<ARGNSModelView>>
					(mMSManager.GetBPPDMListSearch(Mapper.Map<CompanyConn>(pCc), pBPCode,
					ref mPDMSAPCombo, pCodeModel, pNameModel, pSeasonModel, pGroupModel, 
					pBrand, pCollection, pSubCollection, pStart, pLength));

                mReturn.ForEach(c => c.ImageName = GetImageName(c.U_Pic));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ARGNSCostSheetView> GetUserCostSheetListSearch(CompanyView pCc, int pUserSign, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            try
            {
                UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);
                UserSAP user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

                List<ARGNSCostSheetView> mReturn = Mapper.Map<List<ARGNSCostSheetView>>(mMSManager.GetUserCostSheetListSearch(Mapper.Map<CompanyConn>(pCc), user.USER_CODE, txtStyle, txtCodeCostSheet, txtDescription));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ARGNSCostSheetView> GetBPCostSheetListSearch(CompanyView pCc, string pBPCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            try
            {
                List<ARGNSCostSheetView> mReturn = Mapper.Map<List<ARGNSCostSheetView>>(mMSManager.GetBPCostSheetListSearch(Mapper.Map<CompanyConn>(pCc), pBPCode, txtStyle, txtCodeCostSheet, txtDescription));
                
                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GetImageName(string pPath)
        {
            string ret = string.Empty;
            string[] mImageName;
            try
            {
                if (!string.IsNullOrEmpty(pPath))
                {
                    mImageName = pPath.Split(new Char[] { '\\' });
                    ret = mImageName[mImageName.Length - 1];

                }

                return ret;
            }
            catch (Exception)
            {
                return string.Empty;
                throw;
            }

        }

        //public PDMSAPCombo GetPDMSAPCombo(CompanyView pCc)
        //{
        //    try
        //    {
        //        PDMSAPCombo mComboReturn = mMSManager.GetPDMSAPCombo(Mapper.Map<CompanyConn>(pCc));
        //        return mComboReturn;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
