﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class AreaKeyManagerView
    {
        AreaKeyManager mAreaKeyManager;

        public AreaKeyManagerView()
        {
            mAreaKeyManager = new AreaKeyManager();
        }


        public bool GetAreasAccess(Enums.Areas pArea)
        {
            try
            {
              
                return mAreaKeyManager.GetAreasAccess(pArea);
            }
            catch (Exception)
            {
                return false;
            }

        }

        public List<AreaKeyView> GetAreasKeyList()
        {
            Mapper.CreateMap<AreaKey, AreaKeyView>();
            Mapper.CreateMap<AreaKeyView, AreaKey>();

            return Mapper.Map<List<AreaKeyView>>(mAreaKeyManager.GetAreasKeyList());

        }
    }
}
