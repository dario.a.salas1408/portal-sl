﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ProjectSAPManagerView
    {
        private ProjectSAPManager mProjectSAPManager;

        public ProjectSAPManagerView()
        {
            mProjectSAPManager = new ProjectSAPManager();
        }

        public List<ProjectSAPView> GetProjectsSAPList(CompanyView pCv, string pProjectCode, string pProjectName)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ProjectSAP, ProjectSAPView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();
                List<ProjectSAP> mProjectList = mProjectSAPManager.GetProjectsSAPList(mapper.Map<CompanyConn>(pCv), pProjectCode, pProjectName);

                return mapper.Map<List<ProjectSAPView>>(mProjectList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
