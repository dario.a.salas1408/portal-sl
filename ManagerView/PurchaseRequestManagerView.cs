﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PurchaseRequestManagerView
    {
        PurchaseRequestManager mPurchaseRequestManager;
        UserSapManager mUserSapManager;
        public PurchaseRequestManagerView()
        {
            mPurchaseRequestManager = new PurchaseRequestManager();
            mUserSapManager = new UserSapManager();
            Mapper.CreateMap<PurchaseRequestSAP, PurchaseRequestView>();
            Mapper.CreateMap<PurchaseRequestView, PurchaseRequestSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<PurchaseRequestLineSAP, PurchaseRequestLineView>();
            Mapper.CreateMap<PurchaseRequestLineView, PurchaseRequestLineSAP>();
        }

        public PurchaseRequestView GetPurchaseRequest(int pCode, int pUserId, CompanyView pCv)
        {
            try
            {
                PurchaseRequestSAP mPR = mPurchaseRequestManager.GetPurchaseRequest(Mapper.Map<CompanyConn>(pCv), pCode, pUserId);

                return Mapper.Map<PurchaseRequestView>(mPR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pRequest"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDepartment"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseRequestListSearch(CompanyView pCc, string pRequest = "", 
            DateTime? pDate = null, int? pDocNum = null, int? pDepartment = null, 
            string pDocStatus = "", string pOwnerCode = "", string pSECode = "", 
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mPurchaseRequestManager.GetPurchaseRequestListSearch(Mapper.Map<CompanyConn>(pCc), 
                    pRequest, pDate, pDocNum, pDepartment, 
                    pDocStatus, pOwnerCode, pSECode, pStart, 
                    pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
        public List<PurchaseRequestLineView> GetPurchaseRequestLinesSearch(CompanyView pCc, string[] pDocuments)
        {
            try
            {

                List<PurchaseRequestLineView> mReturn = Mapper.Map<List<PurchaseRequestLineView>>(mPurchaseRequestManager.GetPurchaseRequestLinesSearch(Mapper.Map<CompanyConn>(pCc), pDocuments));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<UserSAP> GetUserSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetUserSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetEmployeeSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PurchaseRequestCombo GetPurchaseRequestCombo(CompanyView pCv)
        {
            try
            {
                return mPurchaseRequestManager.GetPurchaseRequestCombo(Mapper.Map<CompanyConn>(pCv));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(PurchaseRequestView pPR, CompanyView pCv)
        {
            try
            {
                return mPurchaseRequestManager.Add(Mapper.Map<PurchaseRequestSAP>(pPR), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(PurchaseRequestView pPR, CompanyView pCv)
        {
            try
            {
                return mPurchaseRequestManager.Update(Mapper.Map<PurchaseRequestSAP>(pPR), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RatesSystem> GetListRates(CompanyView pCv, List<CurrencySAP> mListCurrency, DateTime DocDate)
        {
            try
            {
                return mPurchaseRequestManager.GetListRates(Mapper.Map<CompanyConn>(pCv), mListCurrency, DocDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FreightView> GetFreights(CompanyView pCv, int pDocEntry)
        {
            try
            {
                return Mapper.Map<List<FreightView>>(mPurchaseRequestManager.GetFreights(Mapper.Map<CompanyConn>(pCv), pDocEntry));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
