﻿using ARGNS.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class InfoPortalManagerView
    {
        InfoPortalManager mInfoPortalManager;

        public InfoPortalManagerView()
        {
            mInfoPortalManager = new InfoPortalManager();
        }

        public System.DateTime? GetLasUpdate()
        {
            return mInfoPortalManager.GetLasUpdate();
        }

        public bool IsDbCreated()
        {
            return mInfoPortalManager.IsDbCreated();
        }
    }
}
