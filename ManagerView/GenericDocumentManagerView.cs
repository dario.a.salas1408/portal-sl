﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;

namespace ARGNS.ManagerView
{
    public class GenericDocumentManagerView
    {
        public JsonObjectResult GetDocuments(CompanyView pCc, string pDocType, 
            bool pShowOpenQuantity,
            DateTime? pDocDate = null, bool pBPDocCB = false, 
            string pBP = "", int pStart = 0, int pLength = 0, 
            OrderColumn pOrderColumn = null)
        {
            try
            {
                switch (pDocType)
                {
                    case "13":
                        SalesInvoiceManager mSalesInvoiceManager = new SalesInvoiceManager();
                        return mSalesInvoiceManager.GetSalesInvoiceListSearch(Mapper.Map<CompanyConn>(pCc), (pBPDocCB == true ? pBP : ""), pDocDate, null, "", "", "",pStart, pLength, pOrderColumn);
                    case "17":
                        SaleOrderManager mSaleOrderManager = new SaleOrderManager();
                        return mSaleOrderManager.GetSalesOrderListSearch(
                            Mapper.Map<CompanyConn>(pCc), pShowOpenQuantity,
                            (pBPDocCB == true ? pBP : ""), pDocDate, null, "", "", "", pStart, pLength, pOrderColumn);
                    case "23":
                        SaleQuotationManager mSaleQuotationManager = new SaleQuotationManager();
                        return mSaleQuotationManager.GetSalesQuotationListSearch(Mapper.Map<CompanyConn>(pCc), (pBPDocCB == true ? pBP : ""), pDocDate, null, "", "", "", pStart, pLength, pOrderColumn);
                    default:
                        return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
