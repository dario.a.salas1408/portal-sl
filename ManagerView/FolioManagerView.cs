﻿using ARGNS.Manager;
using ARGNS.Util;
using System;

namespace ARGNS.ManagerView
{
    public class FolioManagerView
    {
        public int New(string pFolioType)
        {
            int ret = 0;
            try
            {
                FolioManager folioManager = new FolioManager();
                ret = folioManager.New(pFolioType);
            }
            catch (Exception ex)
            {
                Logger.WriteError(ex.Message);
            }
            return ret;

        }
    }
}
