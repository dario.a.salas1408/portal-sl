﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class MaterialManagerView
    {
        private MaterialManager mMaterialManager;

        public MaterialManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();

            mMaterialManager = new MaterialManager();
        }

        //public List<ARGNSCSMaterial> GetMaterialsByCode(CompanyView pCc, string Code)
        //{
        //    return mMaterialManager.GetMaterialsByCode(Mapper.Map<CompanyConn>(pCc), Code);
        //}

    }
}
