﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace ARGNS.ManagerView
{
    public class LocalDraftsManagerView
    {
        LocalDraftsHeaderManager mDraftsHeaderManager;

        public LocalDraftsManagerView()
        {
            mDraftsHeaderManager = new LocalDraftsHeaderManager();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SaleOrderView pSO, CompanyView pCv, string pUrlFrom, string pFrom)
        {
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                mJsonObjectResult =  mDraftsHeaderManager.Add(Mapper.Map<SaleOrderSAP>(pSO), Mapper.Map<CompanyConn>(pCv));

                if (mJsonObjectResult.ServiceAnswer == Enums.ServiceResult.Ok.ToDescriptionString())
                {
                    //If code is null, the order entry in approval process.
                    if (!string.IsNullOrEmpty(mJsonObjectResult.Code))
                    {
                            mJsonObjectResult.RedirectUrl = "/Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=Update&IdPO={Code}&fromController={From}";
                            mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{Code}", mJsonObjectResult.Code);
                            mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{From}", pFrom);
                            mJsonObjectResult.AddUpdateMsg = string.Format("Local Draft {0} successfully created", mJsonObjectResult.Code);
                    }
                    else
                    {
                        mJsonObjectResult.RedirectUrl = pUrlFrom;
                    }
                }

                return mJsonObjectResult; 
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftsHeaderManagerView--> Add: " + ex.InnerException.ToString());
                return new JsonObjectResult();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCv"></param>
        /// <param name="pUrlFrom"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        public string Update(SaleOrderView pSO, CompanyView pCv, string pUrlFrom, string pFrom)
        {
            try
            {
                var mapper = new MapperConfiguration(
                      cfg => {
                          cfg.CreateMap<SaleOrderView, LocalDrafts>();
                          cfg.CreateMap<SaleOrderLineView, LocalDraftsLine>();
                          cfg.CreateMap<CompanyView, CompanyConn>();
                          cfg.CreateMap<CompanyStockView, CompanyStock>();
                      })
                      .CreateMapper();

                string result = mDraftsHeaderManager.Update(
                    mapper.Map<LocalDrafts>(pSO),
                    mapper.Map<CompanyConn>(pCv));

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftsHeaderManagerView--> Add: " + ex.Message.ToString());
                Logger.WriteError("DraftsHeaderManagerView--> Add: " + ex.InnerException.ToString());
                return "Error: " + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="draftId"></param>
        /// <returns></returns>
        public bool Delete(int draftId)
        {
            return mDraftsHeaderManager.Delete(draftId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult ListAllDrafts(CompanyView pCc, string pCodeVendor = "", DateTime? pDate = null, 
            int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", 
            string pSECode = "", int pStart = 0, int pLength = 0, 
            OrderColumn pOrderColumn = null)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                mJsonObjectResult = mDraftsHeaderManager.ListAllDrafts(Mapper.Map<CompanyConn>(pCc), 
                    pCodeVendor, pDate, pDocNum, pDocStatus, 
                    pOwnerCode, pSECode, pStart, 
                    pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftsHeaderManagerView--> ListAllDrafts: " + ex.Message.ToString());
                Logger.WriteError("DraftsHeaderManagerView--> ListAllDrafts: " + ex.InnerException.ToString());
            }
            return mJsonObjectResult; 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
        public List<SaleQuotationLineView> ListLineDocuments(CompanyView pCc, string[] pDocuments)
        {
            try
            {
                var mapper = new MapperConfiguration(
                    cfg => { cfg.CreateMap<LocalDraftsLine, SaleQuotationLineView>(); })
                    .CreateMapper();

                List<SaleQuotationLineView> mReturn = mapper.Map<
                    List<SaleQuotationLineView>>
                    (mDraftsHeaderManager.ListLineDocuments(
                        Mapper.Map<CompanyConn>(pCc), pDocuments));

                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftsHeaderManagerView--> ListLineDocuments" + ex.Message);
                Logger.WriteError("DraftsHeaderManagerView--> ListLineDocuments" + ex.InnerException);
                return new List<SaleQuotationLineView>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPO"></param>
        /// <param name="pUserId"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public SaleOrderView GetDraftDocument(int idPO, int pUserId, CompanyView pCv)
        {
            var mapper = new MapperConfiguration(
                  cfg => {
                      cfg.CreateMap<LocalDrafts, SaleOrderView>();
                      cfg.CreateMap<LocalDraftsLine, SaleOrderLineView>();
                  })
                  .CreateMapper();

            SaleOrderView mReturn = mapper.Map<
                    SaleOrderView>
                    (mDraftsHeaderManager.GetDraftDocument(idPO));

            return mReturn; 
        }
    }
}
