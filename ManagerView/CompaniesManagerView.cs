﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.ManagerView
{
    public class CompaniesManagerView
    {
        CompaniesManager mCompaniesManager;
        CompaniesStockManager mCompaniesStockManager;
        UserSapManager mUserSapManager;

        public CompaniesManagerView()
        {
            mCompaniesManager = new CompaniesManager();
            mCompaniesStockManager = new CompaniesStockManager();
            mUserSapManager = new UserSapManager();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<UserView, User>();
            Mapper.CreateMap<UsersSettingView, UsersSetting>();
            Mapper.CreateMap<UsersSetting, UsersSettingView>();

            Mapper.CreateMap<CompanyStock, CompanyStockView>();
            Mapper.CreateMap<CompanyStockView, CompanyStock>();
            Mapper.CreateMap<CRPageMap, CRPageMapView>();
            Mapper.CreateMap<CRPageMapView, CRPageMap>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CompanyView> GetCompanies()
        {
            try
            {
                return Mapper.Map<List<CompanyView>>(mCompaniesManager.GetCompanies());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CompanyView> GetAllCompaniesActives()
        {
            try
            {
                return Mapper.Map<List<CompanyView>>(mCompaniesManager.GetAllCompaniesActives());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public CompanyView GetCompany(int IdCompany)
        {
            try
            {
                CompanyConn mm = mCompaniesManager.GetCompany(IdCompany);

                return Mapper.Map<CompanyView>(mm);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Add(CompanyView pCompany, UserView pUser)
        {
            try
            {
                return mCompaniesManager.Add(Mapper.Map<CompanyConn>(pCompany), Mapper.Map<User>(pUser));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool Update(CompanyView pCompany)
        {
            try
            {
                return mCompaniesManager.Update(Mapper.Map<CompanyConn>(pCompany));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool Delete(CompanyView pCompany)
        {
            try
            {
                return mCompaniesManager.Delete(Mapper.Map<CompanyConn>(pCompany));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public List<CompanyView> GetCompaniesByUser(UserView pUser)
        {
            try
            {
                return Mapper.Map<List<CompanyView>>(mCompaniesManager.GetCompaniesByUser(Mapper.Map<User>(pUser)));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public CompanyView SignIn(string CompanyId, UserView user)
        {
            try
            {
                CompanyView mCompanyView = GetCompany(Convert.ToInt32(CompanyId));
                UsersSetting userSetting = mUserSapManager.GetCompany(Convert.ToInt32(CompanyId), user.IdUser);
                if (mCompanyView.UseCompanyUser == true && userSetting != null)
                {
                    mCompanyView.UserName = userSetting.UserCodeSAP;
                    mCompanyView.Password = userSetting.UserPasswordSAP;
                }
                //Seteo el business partner asociado y el sales employee
                user.BPCode = userSetting.IdBp;
                user.SECode = userSetting.IdSE;
                user.BPGroupId = userSetting.BPGroupId;
                
                //Seteo el UserGroup de este usuario
                mCompanyView.UserGroupId = userSetting.UserGroupId;
                //------------------------------------------------------

                //mCompanyView.CompanyStock =  Mapper.Map<CompanyStockView>(mCompaniesStockManager.GetCompanyStock(mCompanyView.IdCompany));

                if (!mCompanyView.OnDemand)
                {
                    CompanyConn mCompanyConn = mCompaniesManager.SignIn(Mapper.Map<CompanyConn>(mCompanyView));
                    mCompanyView.ResultConnection = mCompanyConn.DSSessionId;
                    mCompanyView.SLRouteId = mCompanyConn.SLRouteId;
                    mCompanyView.SLSessionId = mCompanyConn.SLSessionId;
                    mCompanyView.DSSessionId = mCompanyConn.DSSessionId;
                }
                CompanySAPConfig mCompanySAPConfig = mCompaniesManager.GetCompanySAPConfig(Mapper.Map<CompanyConn>(mCompanyView));
                mCompanyView.CheckApparelInstallation = mCompanySAPConfig.CheckApparelInstallation;
                mCompanyView.CompanySAPConfig = mCompanySAPConfig;

                if (mCompanyView.CompanyStock.Count == 0)
                {
                    mCompanyView.CompanyStock.Add(new CompanyStockView { ValidateStock = false, Formula = "A + B - C" });
                }
                else
                {
                    Mapper.CreateMap<WarehousePortal, WarehousePortalView>();
                    Mapper.CreateMap<WarehousePortalView, WarehousePortal>();
                    mCompanyView.CompanyStock.FirstOrDefault().WarehousePortalList = Mapper.Map<List<WarehousePortalView>>(mCompaniesStockManager.GetWarehousePortalList(mCompanyView.IdCompany));
                }

                return mCompanyView;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mCompanyView"></param>
        /// <returns></returns>
        public CompanyView ConnectCompany(CompanyView mCompanyView)
        {
            try
            {
                CompanyView mCompanyViewReturn = mCompanyView;


                CompanyConn mCompanyConn = mCompaniesManager.SignIn(Mapper.Map<CompanyConn>(mCompanyView));
                mCompanyViewReturn.ResultConnection = mCompanyConn.DSSessionId;
                mCompanyViewReturn.SLRouteId = mCompanyConn.SLRouteId;
                mCompanyViewReturn.SLSessionId = mCompanyConn.SLSessionId;
                mCompanyConn.DSSessionId = mCompanyConn.DSSessionId;


                return mCompanyViewReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<UsersSettingView> GetUserSettings()
        {
            try
            {
                List<UsersSetting> userSettingList = mCompaniesManager.GetUserSettings();
                return Mapper.Map<List<UsersSettingView>>(userSettingList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <param name="idUSer"></param>
        /// <returns></returns>
        public bool ValidateConnection(int CompanyId, int idUSer)
        {
            try
            {
                CompanyView mCompanyView = GetCompany(CompanyId);
                UsersSetting userSetting = mCompaniesManager.GetUserSettingsByCompanyAndUser(CompanyId, idUSer);
                mCompanyView.UserName = userSetting.UserCodeSAP;
                mCompanyView.Password = userSetting.UserPasswordSAP;

                mCompanyView.ResultConnection = mCompaniesManager.SignIn(Mapper.Map<CompanyConn>(mCompanyView)).DSSessionId;
                if (mCompanyView.ResultConnection.Split(':')[0] == "Error")
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pServer"></param>
        /// <param name="pUser"></param>
        /// <param name="pPassword"></param>
        /// <param name="pCompanyDB"></param>
        /// <returns></returns>
        public bool ValidateSQLConnection(string pServer, string pUser, string pPassword, string pCompanyDB)
        {
            try
            {
                return mCompaniesManager.ValidateSQLConnection(pServer, pUser, pPassword, pCompanyDB);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool ValidateSAPConnection(CompanyView pCompany)
        {
            try
            {
                pCompany.ResultConnection = mCompaniesManager.SignIn(Mapper.Map<CompanyConn>(pCompany)).DSSessionId;
                if (pCompany.ResultConnection.Split(':')[0] == "Error")
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region QRConfig
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public QRConfigView GetQRConfig(int IdCompany)
        {
            try
            {
                Mapper.CreateMap<QRConfig, QRConfigView>();
                Mapper.CreateMap<QRConfigView, QRConfig>();
                return Mapper.Map<QRConfigView>(mCompaniesManager.GetQRConfig(IdCompany));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pQRConfigView"></param>
        /// <returns></returns>
        public bool AddQRConfig(QRConfigView pQRConfigView)
        {
            try
            {
                Mapper.CreateMap<QRConfig, QRConfigView>();
                Mapper.CreateMap<QRConfigView, QRConfig>();
                return mCompaniesManager.AddQRConfig(Mapper.Map<QRConfig>(pQRConfigView));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pQRConfigView"></param>
        /// <returns></returns>
        public bool UpdateQRConfig(QRConfigView pQRConfigView)
        {
            try
            {
                Mapper.CreateMap<QRConfig, QRConfigView>();
                Mapper.CreateMap<QRConfigView, QRConfig>();
                return mCompaniesManager.UpdateQRConfig(Mapper.Map<QRConfig>(pQRConfigView));
            }
            catch (Exception ex)
            {
                throw ex;                
            }
        }

        #endregion
    }
}
