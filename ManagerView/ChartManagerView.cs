﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ChartManagerView
    {
        ChartManager mChartManager;
        public ChartManagerView()
        {
            mChartManager = new ChartManager();
        }

        public List<PortalChartView> GetPortalChartList(int pIdCompany)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<PortalChart, PortalChartView>(); cfg.CreateMap<PortalChartColumn, PortalChartColumnView>(); cfg.CreateMap<PortalChartType, PortalChartTypeView>();
                    cfg.CreateMap<PortalChartUserGroup, PortalChartUserGroupView>(); cfg.CreateMap<PortalChartParam, PortalChartParamView>(); cfg.CreateMap<UserGroup, UserGroupView>();
                }).CreateMapper();


                List<PortalChart> mPortalChartList = mChartManager.GetPortalChartList(pIdCompany);

                return mapper.Map<List<PortalChartView>>(mPortalChartList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalChartView> GetPortalChartListSearch(string pPortalPage, int pIdCompany, UserGroupView pUserGroupId)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<PortalChart, PortalChartView>(); cfg.CreateMap<PortalChartColumn, PortalChartColumnView>(); cfg.CreateMap<PortalChartType, PortalChartTypeView>();
                    cfg.CreateMap<PortalChartUserGroup, PortalChartUserGroupView>(); cfg.CreateMap<PortalChartParam, PortalChartParamView>(); cfg.CreateMap<UserGroupView, UserGroup>();
                }).CreateMapper();

                List<PortalChart> mPortalChartList = mChartManager.GetPortalChartListSearch(pPortalPage, pIdCompany, mapper.Map<UserGroup>(pUserGroupId));

                return mapper.Map<List<PortalChartView>>(mPortalChartList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PortalChartView GetPortalChartById(CompanyView pCv, int pChartId)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<PortalChart, PortalChartView>(); cfg.CreateMap<PortalChartColumn, PortalChartColumnView>(); cfg.CreateMap<PortalChartType, PortalChartTypeView>();
                    cfg.CreateMap<PortalChartUserGroup, PortalChartUserGroupView>(); cfg.CreateMap<PortalChartParam, PortalChartParamView>(); cfg.CreateMap<UserGroup, UserGroupView>();
                    cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>();
                }).CreateMapper();


                PortalChart mPortalChart = mChartManager.GetPortalChartById(mapper.Map<CompanyConn>(pCv), pChartId);

                return mapper.Map<PortalChartView>(mPortalChart);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalChartTypeView> GetPortalChartTypeList(CompanyView pCv)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<PortalChartType, PortalChartTypeView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mapper.Map<List<PortalChartTypeView>>(mChartManager.GetPortalChartTypeList(mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalChartUserGroupView> GetPortalChartUserGroupList(CompanyView pCv, int IdChart)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<PortalChartUserGroup, PortalChartUserGroupView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mapper.Map<List<PortalChartUserGroupView>>(mChartManager.GetPortalChartUserGroupList(mapper.Map<CompanyConn>(pCv), IdChart));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult Update(PortalChartView pPortalChartView)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<PortalChartView, PortalChart>(); cfg.CreateMap<PortalChartColumnView, PortalChartColumn>(); cfg.CreateMap<PortalChartTypeView, PortalChartType>();
                    cfg.CreateMap<PortalChartUserGroupView, PortalChartUserGroup>(); cfg.CreateMap<PortalChartParamView, PortalChartParam>();
                }).CreateMapper();

                return mChartManager.Update(mapper.Map<PortalChart>(pPortalChartView));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalChartParamsTypeView> GetPortalChartParamsTypeList(CompanyView pCv)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<PortalChartParamsType, PortalChartParamsTypeView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mapper.Map<List<PortalChartParamsTypeView>>(mChartManager.GetPortalChartParamsTypeList(mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalChartParamsValueTypeView> GetPortalChartParamsValueTypeList(CompanyView pCv)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<PortalChartParamsValueType, PortalChartParamsValueTypeView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mapper.Map<List<PortalChartParamsValueTypeView>>(mChartManager.GetPortalChartParamsValueTypeList(mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult Add(PortalChartView pPortalChartView)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<PortalChartView, PortalChart>(); cfg.CreateMap<PortalChartColumnView, PortalChartColumn>(); cfg.CreateMap<PortalChartTypeView, PortalChartType>();
                    cfg.CreateMap<PortalChartUserGroupView, PortalChartUserGroup>(); cfg.CreateMap<PortalChartParamView, PortalChartParam>();
                }).CreateMapper();

                return mChartManager.Add(mapper.Map<PortalChart>(pPortalChartView));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int pIdChartDelete)
        {
            try
            {
                return mChartManager.Delete(pIdChartDelete);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
