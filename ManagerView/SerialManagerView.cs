﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.SAP;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ARGNS.Util.Enums;

namespace ARGNS.ManagerView
{
    public class SerialManagerView
    {
        private SerialManager serialManager;
        public SerialManagerView()
        {
            serialManager = new SerialManager();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
        }

        public SerialSAP GetSerialsByDocument(CompanyView pCompanyParam, int docNum, int line, string whsCode, DocumentsType docType, string itemCode, int idUser, int anio)
        {
            try
            {
                var serialSAP = serialManager.GetSerialsByDocument(Mapper.Map<CompanyConn>(pCompanyParam), docNum, line, whsCode, docType, itemCode, idUser, anio);

                return serialSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SerialManagerView -> GetSerialsByDocument:" + ex.InnerException);
                throw;
            }
        }

    }
}
