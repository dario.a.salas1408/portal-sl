﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class UserBranchManagerView
    {
        private UserBranchManager userBranchManager;

        public UserBranchManagerView()
        {
            userBranchManager = new UserBranchManager();
            Mapper.CreateMap<UserBranch, UserBranchView>();
            Mapper.CreateMap<UserBranchView, UserBranch>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
        }

        public UserBranchView GetUserBranch(int Id)
        {
            try
            {
                UserBranch userBranch = userBranchManager.GetUserBranch(Id);

                return Mapper.Map<UserBranchView>(userBranch);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<UserBranchView> GetUserBranchList(int idUser, int idCompany)
        {
            try
            {
                List<UserBranch> ListUserBranch = userBranchManager.GetUserBranchList(idUser, idCompany);

                return Mapper.Map<List<UserBranchView>>(ListUserBranch);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool Add(UserBranchView userBranchView)
        {
            try
            {

                return userBranchManager.Add(Mapper.Map<UserBranch>(userBranchView));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Delete(int idUserBranch)
        {
            try
            {
                return userBranchManager.Delete(idUserBranch);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<BranchesSAP> GetBranchesByUser(CompanyView pCc, string userSAP)
        {
            try
            {
                return userBranchManager.GetBranchesByUser(Mapper.Map<CompanyConn>(pCc), userSAP);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
