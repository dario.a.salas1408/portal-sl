﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class OperationManagerView
    {
        private OperationManager mOperationManager;

        public OperationManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();

            mOperationManager = new OperationManager();
        }

        //public List<ARGNSCSOperation> GetOperationsByCode(CompanyView pCc, string Code)
        //{
        //    return mOperationManager.GetOperationsByCode(Mapper.Map<CompanyConn>(pCc), Code);
        //}
    }
}
