﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class WarehouseManagerView
    {
        WarehouseManager mWarehouseManager;

        public WarehouseManagerView()
        {
            mWarehouseManager = new WarehouseManager();
            Mapper.CreateMap<Warehouse, WarehouseView>();
        }

        public List<WarehouseView> GetListWarehouse(CompanyView pCc)
        {
            try
            {
                return Mapper.Map<List<WarehouseView>>(mWarehouseManager.GetListWarehouse(Mapper.Map<CompanyConn>(pCc)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
