﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class SchemaManagerView
    {
        private SchemaManager mSchemaManager;

        public SchemaManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();

            mSchemaManager = new SchemaManager();
        }

        //public List<ARGNSCSSchema> GetSchemasByCode(CompanyView pCc, string Code)
        //{
        //    return mSchemaManager.GetSchemasByCode(Mapper.Map<CompanyConn>(pCc), Code);
        //}
    }
}
