﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class TileManagerView
    {
        TileManager mTileManager;
        public TileManagerView()
        {
            mTileManager = new TileManager();
        }

        public List<PortalTileView> GetPortalTileList(int pIdCompany)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PortalTile, PortalTileView>(); cfg.CreateMap<PortalTileType, PortalTileTypeView>();
                    cfg.CreateMap<PortalTileUserGroup, PortalTileUserGroupView>(); cfg.CreateMap<PortalTileParam, PortalTileParamView>(); cfg.CreateMap<UserGroup, UserGroupView>();
                }).CreateMapper();


                List<PortalTile> mPortalTileList = mTileManager.GetPortalTileList(pIdCompany);

                return mapper.Map<List<PortalTileView>>(mPortalTileList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalTileView> GetPortalTileListSearch(int pIdCompany, UserGroupView pUserGroupId)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PortalTile, PortalTileView>(); cfg.CreateMap<PortalTileType, PortalTileTypeView>();
                    cfg.CreateMap<PortalTileUserGroup, PortalTileUserGroupView>(); cfg.CreateMap<PortalTileParam, PortalTileParamsView>(); cfg.CreateMap<UserGroupView, UserGroup>();
                }).CreateMapper();

                List<PortalTile> mPortalTileList = mTileManager.GetPortalTileListSearch(pIdCompany, mapper.Map<UserGroup>(pUserGroupId));

                return mapper.Map<List<PortalTileView>>(mPortalTileList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PortalTileView GetPortalTileById(CompanyView pCv, int pTileId)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PortalTile, PortalTileView>(); cfg.CreateMap<PortalTileType, PortalTileTypeView>();
                    cfg.CreateMap<PortalTileUserGroup, PortalTileUserGroupView>(); cfg.CreateMap<PortalTileParam, PortalTileParamView>(); cfg.CreateMap<UserGroup, UserGroupView>();
                    cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>();
                }).CreateMapper();


                PortalTile mPortalTile = mTileManager.GetPortalTileById(mapper.Map<CompanyConn>(pCv), pTileId);

                return mapper.Map<PortalTileView>(mPortalTile);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalTileTypeView> GetPortalTileTypeList(CompanyView pCv)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<PortalTile, PortalTileView>(); cfg.CreateMap<PortalTileType, PortalTileTypeView>(); cfg.CreateMap<PortalTileParam, PortalTileParamView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mapper.Map<List<PortalTileTypeView>>(mTileManager.GetPortalTileTypeList(mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PortalTileUserGroupView> GetPortalTileUserGroupList(CompanyView pCv, int IdTile)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<PortalTileUserGroup, PortalTileUserGroupView>(); cfg.CreateMap<CompanyView, CompanyConn>(); cfg.CreateMap<CompanyStockView, CompanyStock>(); }).CreateMapper();

                return mapper.Map<List<PortalTileUserGroupView>>(mTileManager.GetPortalTileUserGroupList(mapper.Map<CompanyConn>(pCv), IdTile));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult Update(PortalTileView pPortalTileView)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PortalTileView, PortalTile>(); cfg.CreateMap<PortalTileTypeView, PortalTileType>();
                    cfg.CreateMap<PortalTileUserGroupView, PortalTileUserGroup>(); cfg.CreateMap<PortalTileParamView, PortalTileParam>(); cfg.CreateMap<UserGroupView, UserGroup>();
                }).CreateMapper();

                return mTileManager.Update(mapper.Map<PortalTile>(pPortalTileView));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult Add(PortalTileView pPortalTileView)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<PortalTileView, PortalTile>(); cfg.CreateMap<PortalTileTypeView, PortalTileType>();
                    cfg.CreateMap<PortalTileUserGroupView, PortalTileUserGroup>(); cfg.CreateMap<PortalTileParamView, PortalTileParam>(); cfg.CreateMap<UserGroupView, UserGroup>();
                }).CreateMapper();

                return mTileManager.Add(mapper.Map<PortalTile>(pPortalTileView));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int pIdTileDelete)
        {
            try
            {
                return mTileManager.Delete(pIdTileDelete);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
