﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PurchaseOrderManagerView
    {
        PurchaseOrderManager mPurchaseOrderManager;
        UserSapManager mUserSapManager;
        GlobalDocumentManager mGlobalDoc;
        public PurchaseOrderManagerView()
        {
            mPurchaseOrderManager = new PurchaseOrderManager();
            mUserSapManager = new UserSapManager();
            mGlobalDoc = new GlobalDocumentManager();

            Mapper.CreateMap<PurchaseOrderSAP, PurchaseOrderView>();
            Mapper.CreateMap<PurchaseOrderView, PurchaseOrderSAP>();
            Mapper.CreateMap<PurchaseOrderView, PurchaseOrderSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<PurchaseOrderSAPLine, PurchaseOrderLineView>();
            Mapper.CreateMap<PurchaseOrderLineView, PurchaseOrderSAPLine>();
            Mapper.CreateMap<PurchaseOrderLineView, PurchaseOrderSAPLine>();
            Mapper.CreateMap<PurchaseOrderSAPLine, PurchaseOrderLineView>();
            Mapper.CreateMap<FreightView, Freight>();
            Mapper.CreateMap<Freight, FreightView>();
            Mapper.CreateMap<AttachmentView, AttachmentSAP>();
            Mapper.CreateMap<AttachmentSAP, AttachmentView>();

        }

        public PurchaseOrderView GetPurchaseOrder(int Id,int pUserId, CompanyView pCv)
        {
            try
            {
                EmployeeSAP mESAP = new EmployeeSAP();
                PurchaseOrderSAP mPO = mPurchaseOrderManager.GetPurchaseOrder(Mapper.Map<CompanyConn>(pCv), Id,pUserId);
                PurchaseOrderView mPOV = Mapper.Map<PurchaseOrderView>(mPO);
                mESAP = mPO.Employee;
                //if (Id != 0)
                //{
                //    EmployeeSAP mESAP = mUserSapManager.GetEmployeeById(Mapper.Map<CompanyConn>(pCv), mPO.OwnerCode);
                //    if(mESAP != null)
                //        mPOV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                //}

                if (mESAP != null)
                        mPOV.OwnerName = mESAP.firstName + " " + mESAP.lastName;

                return mPOV;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult GetPurchaseOrderListSearch(CompanyView pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                JsonObjectResult mReturn = mPurchaseOrderManager.GetPurchaseOrderListSearch(Mapper.Map<CompanyConn>(pCc), pCodeVendor, pDate, pDocNum,pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(PurchaseOrderView pPO, CompanyView pCv)
        {
            try
            {
                return mPurchaseOrderManager.Add(Mapper.Map<PurchaseOrderSAP>(pPO), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(PurchaseOrderView pPO, CompanyView pCv)
        {
            try
            {
                return mPurchaseOrderManager.Update(Mapper.Map<PurchaseOrderSAP>(pPO), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PurchaseOrderSAP GetPOInternalObjects(CompanyView pCv, PurchaseOrderView pPO)
        {
            try
            {
                return mPurchaseOrderManager.GetPOInternalObjects(Mapper.Map<CompanyConn>(pCv), Mapper.Map<PurchaseOrderSAP>(pPO));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FreightView> GetFreights(CompanyView pCv, int pDocEntry)
        {
            try
            {
                return Mapper.Map<List<FreightView>>(mPurchaseOrderManager.GetFreights(Mapper.Map<CompanyConn>(pCv), pDocEntry));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyView pCv)
        {
            try
            {
                return mGlobalDoc.GetTaxCode(Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<RatesSystem> GetListRates(CompanyView pCv, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mPurchaseOrderManager.GetListRates(Mapper.Map<CompanyConn>(pCv), mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetEmployeeSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
