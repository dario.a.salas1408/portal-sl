﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class CompaniesStockManagerView
    {
        CompaniesStockManager mCompaniesStockManager;
        CompaniesManager mCompaniesManager;

        public CompaniesStockManagerView()
        {
            mCompaniesStockManager = new CompaniesStockManager();
            mCompaniesManager = new CompaniesManager();

            Mapper.CreateMap<CompanyStock, CompanyStockView>();
            Mapper.CreateMap<CompanyStockView, CompanyStock>();

            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            
        }

        public CompanyStockView GetCompanyStock(int pIdCompany)
        {
            try
            {
                CompanyStock mCompanyConn = mCompaniesStockManager.GetCompanyStock(pIdCompany);

                CompanyStockView mReturn = Mapper.Map<CompanyStockView>(mCompanyConn);

                //mReturn.CompanyConn = Mapper.Map<CompanyView>(mCompanyConn);

                if (mReturn == null)
                {
                    mReturn = new CompanyStockView();
                    mReturn.Formula = "B + A - C";
                }
                else
                {
                    mReturn.ModeAdd = false;
                }
                mReturn.CompanyConn = Mapper.Map<CompanyView>(mCompaniesManager.GetCompany(pIdCompany));
                mReturn.IdCompany = pIdCompany;
             
                return mReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<WarehousePortalView> GetWarehousePortalList(int pIdCompany)
        {
            try
            {
                Mapper.CreateMap<WarehousePortal, WarehousePortalView>();
                Mapper.CreateMap<WarehousePortalView, WarehousePortal>();
                
                List<WarehousePortalView> mWarehousePortalList = Mapper.Map<List<WarehousePortalView>>(mCompaniesStockManager.GetWarehousePortalList(pIdCompany));

               
                return mWarehousePortalList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool Add(CompanyStockView pCompany)
        {
            try
            {
                bool mResultAdd = mCompaniesStockManager.Add(Mapper.Map<CompanyStock>(pCompany));
                if(mResultAdd == true && pCompany.WarehousePortalList.Count > 0)
                {
                    mCompaniesStockManager.AddWarehousePortalList(Mapper.Map<List<WarehousePortal>>(pCompany.WarehousePortalList));
                }

                return mResultAdd;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Update(CompanyStockView pCompany)
        {
            try
            {
                bool mResultUpdate = mCompaniesStockManager.Update(Mapper.Map<CompanyStock>(pCompany));
                if (mResultUpdate == true)
                {
                    mCompaniesStockManager.UpdateWarehousePortalList(Mapper.Map<List<WarehousePortal>>(pCompany.WarehousePortalList), pCompany.IdCompany);
                }

                return mResultUpdate;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
