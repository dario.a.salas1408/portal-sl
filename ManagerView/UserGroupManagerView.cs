﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class UserGroupManagerView
    {
        private UserGroupManager mUserGroupManager;

        public UserGroupManagerView()
        {
            mUserGroupManager = new UserGroupManager();
            Mapper.CreateMap<UserGroup, UserGroupView>();
            Mapper.CreateMap<UserGroupView, UserGroup>();
        }

        public UserGroupView GetUserGroup(int Id)
        {
            try
            {
                UserGroup mUserGroup = mUserGroupManager.GetUserGroup(Id);

                return Mapper.Map<UserGroupView>(mUserGroup);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<UserGroupView> GetUserGroupList(int IdCompany)
        {
            try
            {
                List<UserGroup> ListUser = mUserGroupManager.GetUserGroupList(IdCompany);

                return Mapper.Map<List<UserGroupView>>(ListUser);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool Add(UserGroupView mUserGroup)
        {
            try
            {

                return mUserGroupManager.Add(Mapper.Map<UserGroup>(mUserGroup));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Update(UserGroupView mUserGroup)
        {
            try
            {
                return mUserGroupManager.Update(Mapper.Map<UserGroup>(mUserGroup));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
       
    }
}
