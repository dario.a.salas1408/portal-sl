﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class ApprovalManagerView
    {
        
        private UserSapManager mUserSapManager;
        private ApprovalManager mApprovalManager;
        public ApprovalManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<DocumentConfirmationLineView, DocumentConfirmationLines>();
            Mapper.CreateMap<DocumentConfirmationLines, DocumentConfirmationLineView>();
            Mapper.CreateMap<StageView, StageSAP>();
            Mapper.CreateMap<StageSAP, StageView>();

            mApprovalManager = new ApprovalManager();
            mUserSapManager = new UserSapManager();
        }

        public List<DocumentConfirmationLineView> GetApprovalListSearch(CompanyView pCc, int pUserSign, 
			DateTime? pDocDate = null, string pDocStatus = "", string pObjectId ="")
        {
            UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);

            UserSAP user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

            return Mapper.Map<List<DocumentConfirmationLineView>>(mApprovalManager.GetApprovalListSearch(
				Mapper.Map<CompanyConn>(pCc), user.USERID,pDocDate,pDocStatus, pObjectId));
        }

        public List<DocumentConfirmationLineView> GetPurchaseApprovalByOriginator(CompanyView pCc, int pUserSign, DateTime? pDocDate = null, 
			string pDocStatus = "", string pObjectId = "")
        {
            UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);

            UserSAP user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

            return Mapper.Map<List<DocumentConfirmationLineView>>(mApprovalManager.GetPurchaseApprovalByOriginator(Mapper.Map<CompanyConn>(pCc), 
				user.USERID, pDocDate, pDocStatus, pObjectId));
        }

        public List<DocumentConfirmationLineView> GetSalesApprovalListSearch(CompanyView pCc, int pUserSign, 
			DateTime? pDocDate = null, string pDocStatus = "", string pObjectId="")
        {
            UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);

            UserSAP user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

            return Mapper.Map<List<DocumentConfirmationLineView>>(
				mApprovalManager.GetSalesApprovalListSearch(Mapper.Map<CompanyConn>(pCc), 
				user.USERID, pDocDate, pDocStatus, pObjectId));
        }

        public List<DocumentConfirmationLineView> GetSalesApprovalByOriginator(CompanyView pCc, int pUserSign, DateTime? pDocDate = null, 
			string pDocStatus = "", string pObjectId ="")
        {
            UsersSetting userSetting = mUserSapManager.GetCompany(pCc.IdCompany, pUserSign);

            UserSAP user = mUserSapManager.GetUserSAPSearchByName(Mapper.Map<CompanyConn>(pCc), userSetting.UserNameSAP);

            return Mapper.Map<List<DocumentConfirmationLineView>>(mApprovalManager.GetSalesApprovalByOriginator(Mapper.Map<CompanyConn>(pCc), 
				user.USERID, pDocDate, pDocStatus, pObjectId));
        }

        public UsersSetting GetUserPortal(CompanyView pCv, int pUserSign)
        {
            try
            {
                return mUserSapManager.GetCompany(pCv.IdCompany, pUserSign);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveApprovalResponse(CompanyView pCv, int owwdCode, string remark, UsersSetting userPortal, string approvalCode)
        {
            try
            {
                return mApprovalManager.SaveApprovalResponse(Mapper.Map<CompanyConn>(pCv), owwdCode, remark, userPortal.UserCodeSAP, userPortal.UserPasswordSAP, approvalCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DocumentConfirmationLineView> GetApprovalListByID(CompanyView pCv,int pWddCode)
        {
            try
            {
                return Mapper.Map<List<DocumentConfirmationLineView>>(mApprovalManager.GetApprovalListByID(Mapper.Map<CompanyConn>(pCv),pWddCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public List<StageView> GetStageList(CompanyView pCv)
        {
            try
            {
                return Mapper.Map<List<StageView>>(mApprovalManager.GetStageList(Mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

    }
}
