﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using AutoMapper;

namespace ARGNS.ManagerView
{
    public class ActivitiesManagerView
    {
        ActivitiesManager mActivitiesManager;
        UserSapManager mUserSapManager;

        public ActivitiesManagerView()
        {
            mActivitiesManager = new ActivitiesManager();
            mUserSapManager = new UserSapManager();
            Mapper.CreateMap<ActivitiesSAP, ActivitiesView>();
            Mapper.CreateMap<ActivitiesView, ActivitiesSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<ActivitiesSAPComboView, ActivitiesSAPCombo>();
            Mapper.CreateMap<ActivitiesSAPCombo, ActivitiesSAPComboView>();

        }

        public ActivitiesView GetActivity(int Id, CompanyView pCv)
        {
            ActivitiesView ret = null;
            UserSapManager mUserMan = null;

            try
            {
                ret = Mapper.Map<ActivitiesView>(mActivitiesManager.GetActivity(Mapper.Map<CompanyConn>(pCv), Id, pCv.CheckApparelInstallation));

                mUserMan = new UserSapManager();
                List<ActivityUser> mActUserList = new List<ActivityUser>();

                if (ret.AttendEmpl != null)
                {
                    ret.ActUser = ret.AttendEmpl;
                    List<EmployeeSAP> listEmployee = mUserMan.GetEmployeeList(Mapper.Map<CompanyConn>(pCv));
                    foreach (EmployeeSAP item in listEmployee)
                    {
                        ActivityUser mUser = new ActivityUser();
                        mUser.Code = item.empID;
                        mUser.Name = item.lastName + " " + item.firstName;
                        mActUserList.Add(mUser);
                    }
                }
                else
                {
                    ret.ActUser = ret.AttendUser;
                    List<UserSAP> listUsers = mUserMan.GetListUsers(Mapper.Map<CompanyConn>(pCv));
                    foreach (UserSAP item in listUsers)
                    {
                        ActivityUser mUser = new ActivityUser();
                        mUser.Code = item.USERID;
                        mUser.Name = item.U_NAME;
                        mActUserList.Add(mUser);
                    }
                }

                ret.ActivityCombo.ActivityUserList = mActUserList;
                return ret;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ActivityUser> GetActivityUserList(CompanyView pCv, int pType)
        {
            List<ActivityUser> mActUserList = null;
            UserSapManager mUserMan = null;
            try
            {
                mActUserList = new List<ActivityUser>();
                mUserMan = new UserSapManager();

                if (pType == 0)
                {
                    List<UserSAP> listUsers = mUserMan.GetListUsers(Mapper.Map<CompanyConn>(pCv));
                    foreach (UserSAP item in listUsers)
                    {
                        ActivityUser mUser = new ActivityUser();
                        mUser.Code = item.USERID;
                        mUser.Name = item.U_NAME;
                        mActUserList.Add(mUser);
                    }

                }
                else
                {
                    List<EmployeeSAP> listEmployee = mUserMan.GetEmployeeList(Mapper.Map<CompanyConn>(pCv));
                    foreach (EmployeeSAP item in listEmployee)
                    {
                        ActivityUser mUser = new ActivityUser();
                        mUser.Code = item.empID;
                        mUser.Name = item.lastName + " " + item.firstName;
                        mActUserList.Add(mUser);
                    }
                }

                return mActUserList;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public List<ActivitiesView> GetActivities(CompanyView pCv)
        {
            try
            {
                return Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetActivities(Mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ActivitiesView> GetActivitiesSeach(CompanyView pCv, int UserType, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesView> ret = null;
            try
            {
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetActivitiesSearch(Mapper.Map<CompanyConn>(pCv), UserType, pModel, pProject, txtStatus, txtRemarks));
                ret.ForEach(c => c.ImageName = GetImageName(c.ModelImage));

                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ActivityStatusSAP> GetActivityStatusList(CompanyView pCv)
        {
            try
            {
                List<ActivityStatusSAP> ret = mActivitiesManager.GetActivityStatusList(Mapper.Map<CompanyConn>(pCv));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GetImageName(string pPath)
        {
            string ret = string.Empty;
            string[] mImageName;
            try
            {
                if (!string.IsNullOrEmpty(pPath))
                {
                    mImageName = pPath.Split(new Char[] { '\\' });
                    ret = mImageName[mImageName.Length - 1];

                }

                return ret;
            }
            catch (Exception)
            {
                return string.Empty;
                throw;
            }

        }

        public string Add(ActivitiesView pActivities, CompanyView pCv, string idUser)
        {
            try
            {
                if (pActivities.UserType == 0)
                {
                    pActivities.AttendUser = (short)pActivities.ActUser;
                }
                else
                {
                    pActivities.AttendEmpl = (short)pActivities.ActUser;
                }
                return mActivitiesManager.Add(Mapper.Map<ActivitiesSAP>(pActivities), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(ActivitiesView pActivities, CompanyView pCv, string idUser)
        {
            try
            {

                if (pActivities.UserType == 0)
                {
                    pActivities.AttendUser = (short)pActivities.ActUser;
                }
                else 
                {
                    pActivities.AttendEmpl = (short)pActivities.ActUser;
                }

                return mActivitiesManager.Update(Mapper.Map<ActivitiesSAP>(pActivities), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSalesOpportunitiesActivity(List<ActivitiesView> pActivitiesList,int? pOpportId,short? pLine, CompanyView pCv)
        {
            try
            {
                mActivitiesManager.UpdateSalesOpportunitiesActivity(Mapper.Map<List<ActivitiesSAP>>(pActivitiesList), pOpportId, pLine, Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ActivitiesView> GetCRMActivitiesSeach(CompanyView pCv, int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode)
        {
            List<ActivitiesView> ret = null;
            try
            {
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetCRMActivitiesSearch(Mapper.Map<CompanyConn>(pCv), UserType, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks, txtBPCode));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActivitiesSAPComboView GetActivityCombo(CompanyView pCv)
        {
            try
            {
                return Mapper.Map<ActivitiesSAPComboView>(mActivitiesManager.GetActivityCombo(Mapper.Map<CompanyConn>(pCv)));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ActivitiesView> GetBPCRMActivitiesSeach(CompanyView pCv, int UserType, string pBPCode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesView> ret = null;
            try
            {
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetBPCRMActivitiesSearch(Mapper.Map<CompanyConn>(pCv), UserType, pBPCode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ActivitiesView> GetUserCRMActivitiesSeach(CompanyView pCv, int UserType, int pUserSign, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesView> ret = null;
            try
            {
                UserSAP user = new UserSAP();
                UsersSetting userSetting = mUserSapManager.GetCompany(pCv.IdCompany, pUserSign);
                user = mUserSapManager.GetUserSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), userSetting.UserCodeSAP).FirstOrDefault();
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetUserCRMActivitiesSearch(Mapper.Map<CompanyConn>(pCv), UserType, user.USERID, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ActivitiesView> GetSalesEmployeeCRMActivitiesSeach(CompanyView pCv, int UserType, int pSECode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesView> ret = null;
            try
            {
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetSalesEmployeeCRMActivitiesSeach(Mapper.Map<CompanyConn>(pCv), UserType, pSECode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ActivitiesView> GetCRMActivitiesListFromTo(CompanyView pCv, DateTime? txtStartDate, DateTime? txtEndDate, int pUserSign)
        {
            List<ActivitiesView> ret = null;
            try
            {
                UserSAP user = new UserSAP();
                UsersSetting userSetting = mUserSapManager.GetCompany(pCv.IdCompany, pUserSign);
                user = mUserSapManager.GetUserSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), userSetting.UserCodeSAP).FirstOrDefault();
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetCRMActivitiesListFromTo(Mapper.Map<CompanyConn>(pCv), txtStartDate, txtEndDate, user.USERID));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ActivitiesView> GetBPMyActivitiesSeach(CompanyView pCv, int UserType, string pBPCode, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesView> ret = null;
            try
            {
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetBPMyActivitiesSearch(Mapper.Map<CompanyConn>(pCv), UserType, pBPCode, txtModel, txtProject, txtStatus, txtRemarks));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ActivitiesView> GetUserMyActivitiesSearch(CompanyView pCv, int UserType, int pUserId, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesView> ret = null;
            try
            {
                UserSAP user = new UserSAP();
                UsersSetting userSetting = mUserSapManager.GetCompany(pCv.IdCompany, pUserId);
                user = mUserSapManager.GetUserSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), userSetting.UserCodeSAP).FirstOrDefault();
                ret = Mapper.Map<List<ActivitiesView>>(mActivitiesManager.GetUserMyActivitiesSearch(Mapper.Map<CompanyConn>(pCv), UserType, user.USERID, txtModel, txtProject, txtStatus, txtRemarks));
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
