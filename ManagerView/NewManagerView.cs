﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace ARGNS.ManagerView
{
    public class NewManagerView
    {
        NewManager mNewManager;
        public NewManagerView()
        {
            mNewManager = new NewManager();
            Mapper.CreateMap<News, NewView>();
            Mapper.CreateMap<NewView, News>();
        }

        public List<NewView> GetNews()
        {
            try
            {
                return Mapper.Map<List<NewView>>(mNewManager.GetNews());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public NewView GetNew(int Id)
        {
            try
            {
                return  Mapper.Map<NewView>(mNewManager.GetNew(Id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add(NewView pNews)
        {
            try
            {
                return mNewManager.Add(Mapper.Map<News>(pNews));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Update(NewView pNews)
        {
            try
            {
                return mNewManager.Update(Mapper.Map<News>(pNews));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Delete(NewView pNews)
        {
            try
            {
                return mNewManager.Delete(Mapper.Map<News>(pNews));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
