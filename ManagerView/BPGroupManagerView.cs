﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class BPGroupManagerView
    {
        private BPGroupManager mBPGroupManager;

        public BPGroupManagerView()
        {
            mBPGroupManager = new BPGroupManager();
        }

        public BPGroupView GetBPGroup(int Id)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<BPGroup, BPGroupView>(); cfg.CreateMap<BPGroupLine, BPGroupLineView>(); }).CreateMapper();

                BPGroup mBPGroup = mBPGroupManager.GetBPGroup(Id);

                return mapper.Map<BPGroupView>(mBPGroup);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<BPGroupView> GetBPGroupList(int IdCompany)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<BPGroup, BPGroupView>(); cfg.CreateMap<BPGroupLine, BPGroupLineView>(); }).CreateMapper();

                List<BPGroup> ListBPGroup = mBPGroupManager.GetBPGroupList(IdCompany);

                return mapper.Map<List<BPGroupView>>(ListBPGroup);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public bool Add(BPGroupView mBPGroupView)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<BPGroupView, BPGroup>(); cfg.CreateMap<BPGroupLineView, BPGroupLine>(); }).CreateMapper();

                return mBPGroupManager.Add(mapper.Map<BPGroup>(mBPGroupView));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Update(BPGroupView mBPGroupView)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<BPGroupView, BPGroup>(); cfg.CreateMap<BPGroupLineView, BPGroupLine>(); }).CreateMapper();

                return mBPGroupManager.Update(mapper.Map<BPGroup>(mBPGroupView));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
       
    }
}
