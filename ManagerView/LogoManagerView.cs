﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class LogoManagerView
    {
         private LogoManager mLogoManager;

         public LogoManagerView()
         {
             mLogoManager = new LogoManager();
             Mapper.CreateMap<ARGNSModelLogo, ARGNSModelLogoView>();
             Mapper.CreateMap<ARGNSModelLogoView, ARGNSModelLogo>();
             Mapper.CreateMap<LogoView, ARGNSLogo>();
             Mapper.CreateMap<ARGNSLogo, LogoView>();
             
         }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public List<ARGNSModelLogoView> GetLogoList(CompanyView pCc, string pCode)
        {           
            try
            {
                return Mapper.Map<List<ARGNSModelLogoView>>(mLogoManager.GetLogoList(Mapper.Map<CompanyConn>(pCc), pCode));             

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public LogoView GetLogoById(CompanyView pCc, string pCode)
        {           
            try
            {
                return Mapper.Map<LogoView>(mLogoManager.GetLogoById(Mapper.Map<CompanyConn>(pCc), pCode));             

            }
            catch (Exception)
            {
                throw;
            }
        }
        
    }
}
