﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class DesignManagerView
    {
        DesignManager mDesignManager;
        public DesignManagerView()
        {
            mDesignManager = new DesignManager();
            Mapper.CreateMap<ARGNSModel, ARGNSModelView>();
            Mapper.CreateMap<ARGNSModelView, ARGNSModel>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<ARGNSSegmentation, ARGNSSegmentationView>();
            Mapper.CreateMap<ARGNSSegmentationView, ARGNSSegmentation>();
        }

        public ARGNSModelView GetDesignById(CompanyView pCv, string pCode)
        {
            try
            {
                ARGNSModel mModel = mDesignManager.GetDesignById(Mapper.Map<CompanyConn>(pCv), pCode);

                return Mapper.Map<ARGNSModelView>(mModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSModelView> GetDesignListSearch(CompanyView pCc, ref PDMSAPCombo mPDMSAPCombo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "", int pStart = 0, int pLength = 0)
        {
            try
            {
                List<ARGNSModelView> mReturn = Mapper.Map<List<ARGNSModelView>>(
					mDesignManager.GetDesignListSearch(Mapper.Map<CompanyConn>(pCc), ref mPDMSAPCombo, 
					pCodeModel, pNameModel, pSeasonModel, pGroupModel, 
					pBrand, pCollection, pSubCollection, pStart, pLength));

                mReturn.ForEach(c => c.ImageName = GetImageName(c.U_Pic));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string UpdateDesign(ARGNSModelView pDesign, CompanyView pCv)
        {
            try
            {
                return mDesignManager.UpdateDesign(Mapper.Map<ARGNSModel>(pDesign), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string AddDesign(ARGNSModelView pDesign, CompanyView pCv)
        {
            try
            {
                return mDesignManager.AddDesign(Mapper.Map<ARGNSModel>(pDesign), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetImageName(string pPath)
        {
            string ret = string.Empty;
            string[] mImageName;
            try
            {
                if (!string.IsNullOrEmpty(pPath))
                {
                    mImageName = pPath.Split(new Char[] { '\\' });
                    ret = mImageName[mImageName.Length - 1];

                }

                return ret;
            }
            catch (Exception)
            {
                return string.Empty;
                throw;
            }

        }
    }
}
