﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.MaterialDetails;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class MaterialDetailManagerView
    {
        MaterialDetailManager mMaterialDetailManager;
        PDMManagerView mPDMManagerView;

        public MaterialDetailManagerView()
        {
            mMaterialDetailManager = new MaterialDetailManager();
            mPDMManagerView = new PDMManagerView();
        }

        public MaterialDetailView GetMaterialDetails(CompanyView pCv, string pModCode, int pUserId)
        {
            try
            {
                Mapper.CreateMap<ARGNSMaterialDetail, MaterialDetailView>();
                Mapper.CreateMap<MaterialDetailView, ARGNSMaterialDetail>();

                ARGNSMaterialDetail mModel = mMaterialDetailManager.GetMaterialDetails(Mapper.Map<CompanyConn>(pCv), pModCode, pUserId);
                mModel.U_Pic = mPDMManagerView.GetImageName(mModel.U_Pic);

                return Mapper.Map<MaterialDetailView>(mModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSModelColor> GetColor(CompanyView pCv, string pModCode)
        {
            try
            {
                List<ARGNSModelColor> mListColor = mMaterialDetailManager.GetColor(Mapper.Map<CompanyConn>(pCv), pModCode);

                return mListColor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(CompanyView pCv, MaterialDetailView pMaterialDetail)
        {
            try
            {
                Mapper.CreateMap<ARGNSMaterialDetail, MaterialDetailView>();
                Mapper.CreateMap<MaterialDetailView, ARGNSMaterialDetail>();

                return mMaterialDetailManager.Update(Mapper.Map<CompanyConn>(pCv), Mapper.Map<ARGNSMaterialDetail>(pMaterialDetail));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(CompanyView pCv, MaterialDetailView pMaterialDetail)
        {
            try
            {
                Mapper.CreateMap<ARGNSMaterialDetail, MaterialDetailView>();
                Mapper.CreateMap<MaterialDetailView, ARGNSMaterialDetail>();

                return mMaterialDetailManager.Add(Mapper.Map<CompanyConn>(pCv), Mapper.Map<ARGNSMaterialDetail>(pMaterialDetail));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
