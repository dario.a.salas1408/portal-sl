﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class TechPackManagerView
    {
        TechPackManager mTechPackManager;

        public TechPackManagerView()
        {
            mTechPackManager = new TechPackManager();
        }

        public ARGNSTechPackView GetTechPack(CompanyView pCv, int Id)
        {
            ARGNSTechPackView ret = null;
            try
            {
                Mapper.CreateMap<ARGNSTechPack, ARGNSTechPackView>();
                Mapper.CreateMap<ARGNSTechPackView, ARGNSTechPack>();
                Mapper.CreateMap<ARGNSTechPackLine, ARGNSTechPackLineView>();
                Mapper.CreateMap<ARGNSTechPackLineView, ARGNSTechPackLine>();
                Mapper.CreateMap<ARGNSTechPackSection, ARGNSTechPackSectionView>();
                Mapper.CreateMap<ARGNSTechPackSectionView, ARGNSTechPackSection>();
                ret = Mapper.Map<ARGNSTechPackView>(mTechPackManager.GetTechPack(Mapper.Map<CompanyConn>(pCv), Id));
                
                return ret;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateTechPack(ARGNSTechPackView pTechPack, CompanyView pCv)
        {
            try
            {
                return mTechPackManager.UpdateTechPack(Mapper.Map<ARGNSTechPack>(pTechPack), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
