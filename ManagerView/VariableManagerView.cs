﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class VariableManagerView
    {
           private VariableManager mVariableMan;

           public VariableManagerView()
        {
            mVariableMan = new VariableManager();
            Mapper.CreateMap<ARGNSVariable, VariableView>();
        }

           public List<VariableView> GetVariableListSearch(CompanyView pCc,ref List<ARGNSSegmentation> pSegmentationList, string pVarCode = "", string pVarName = "", string pVarSegmentation = "")
        {           
            try
            {
                return Mapper.Map<List<VariableView>>(mVariableMan.GetVariableListSearch(Mapper.Map<CompanyConn>(pCc), ref pSegmentationList, pVarCode, pVarName, pVarSegmentation));             

            }
            catch (Exception)
            {
                throw;
            }
        }

           public List<ARGNSSegmentation> GetSegmentationList(CompanyView pCc)
           {
               try
               {
                   return Mapper.Map<List<ARGNSSegmentation>>(mVariableMan.GetSegmentationList(Mapper.Map<CompanyConn>(pCc)));

               }
               catch (Exception)
               {
                   throw;
               }
           }
    }
}
