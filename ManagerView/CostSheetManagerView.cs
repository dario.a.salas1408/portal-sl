﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.ManagerView
{
    public class CostSheetManagerView
    {
        private CostSheetManager mCostSheetManager;
        private UserManagerView mUserManagerView;

        public CostSheetManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<ARGNSCostSheetView, ARGNSCostSheet>();
            Mapper.CreateMap<ARGNSCostSheet, ARGNSCostSheetView>();
            mCostSheetManager = new CostSheetManager();
            mUserManagerView = new UserManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        public List<ARGNSCostSheet> GetListCostSheet(CompanyView pCc, string ItemCode)
        {
            return mCostSheetManager.GetListCostSheet(Mapper.Map<CompanyConn>(pCc), ItemCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="txtStyle"></param>
        /// <param name="txtCodeCostSheet"></param>
        /// <param name="txtDescription"></param>
        /// <returns></returns>
        public List<ARGNSCostSheetView> GetCostSheetListSearch(CompanyView pCv,string txtStyle = "",string txtCodeCostSheet = "", string txtDescription = "")
        {
            try
            {
                return Mapper.Map<List<ARGNSCostSheetView>>(mCostSheetManager.GetCostSheetListSearch(Mapper.Map<CompanyConn>(pCv), txtStyle, txtCodeCostSheet, txtDescription));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="Code"></param>
        /// <param name="pModelCode"></param>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public ARGNSCostSheetView GetListCostSheetByCode(CompanyView pCv, string Code, string pModelCode, int pUserId)
        {
            ARGNSCostSheetView mARGNSCostSheetView = new ARGNSCostSheetView();
            mARGNSCostSheetView = Mapper.Map<ARGNSCostSheetView>(
                mCostSheetManager.GetListCostSheetByCode(
                    Mapper.Map<CompanyConn>(pCv), Code, pModelCode, pUserId));

            if (mARGNSCostSheetView.mCostSheetUDF != null)
            {
                mARGNSCostSheetView.mCostSheetUDF.ListUDFOITM = mUserManagerView.GetUserUDFSearch(
                    pCv, pUserId, pCv.IdCompany, "OITM");
            }
            
            return mARGNSCostSheetView;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pCode"></param>
        /// <param name="pName"></param>
        /// <returns></returns>
        public List<ARGNSSchema> GetCSSchemaList(CompanyView pCv, string pCode, string pName)
        {
            try
            {
                return mCostSheetManager.GetCSSchemaList(Mapper.Map<CompanyConn>(pCv), pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManagerView -> GetCSSchemaList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public List<ARGNSSchemaLine> GetCSSchemaLineList(CompanyView pCv, string pCode)
        {
            try
            {
                return mCostSheetManager.GetCSSchemaLineList(Mapper.Map<CompanyConn>(pCv), pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManagerView -> GetCSSchemaLineList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pCode"></param>
        /// <param name="pName"></param>
        /// <returns></returns>
        public List<ARGNSOperTemplate> GetCSOperationTemplateList(CompanyView pCv, string pCode, string pName)
        {
            try
            {
                return mCostSheetManager.GetCSOperationTemplateList(Mapper.Map<CompanyConn>(pCv), pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManagerView -> GetCSOperationTemplateList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSOperTemplateLine> GetCSOperationTemplateLineList(CompanyView pCv, string pCode)
        {
            try
            {
                return mCostSheetManager.GetCSOperationTemplateLineList(Mapper.Map<CompanyConn>(pCv), pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManagerView -> GetCSOperationTemplateLineList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSPatternTemplate> GetCSPatternList(CompanyView pCv, string pCode, string pName)
        {
            try
            {
                return mCostSheetManager.GetCSPatternList(Mapper.Map<CompanyConn>(pCv), pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManagerView -> GetCSPatternList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSPatternTemplateLine> GetCSPatternLineList(CompanyView pCv, string pCode)
        {
            try
            {
                return mCostSheetManager.GetCSPatternLineList(Mapper.Map<CompanyConn>(pCv), pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManagerView -> GetCSPatternLineList :" + ex.Message);
                throw ex;
            }
        }

        public string Update(ARGNSCostSheetView pCostSheet, CompanyView pCv)
        {
            try
            {
                return mCostSheetManager.Update(Mapper.Map<ARGNSCostSheet>(pCostSheet), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult AddCostSheet(ARGNSCostSheetView pCostSheet, CompanyView pCv)
        {
            try
            {
                return mCostSheetManager.AddCostSheet(Mapper.Map<ARGNSCostSheet>(pCostSheet), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RatesSystem> GetListRates(CompanyView pCv, List<CurrencySAP> mListCurrency, DateTime DocDate)
        {
            try
            {
                return mCostSheetManager.GetListRates(Mapper.Map<CompanyConn>(pCv), mListCurrency, DocDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
