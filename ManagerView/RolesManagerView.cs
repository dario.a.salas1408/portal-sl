﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.ManagerView
{
    public class RolesManagerView
    {
        RolesManager mRolesManager;

        public RolesManagerView()
        {
            mRolesManager = new RolesManager();
            Mapper.CreateMap<Role, RoleView>();
            Mapper.CreateMap<RoleView, Role>();
            Mapper.CreateMap<Page, PageView>();
            Mapper.CreateMap<PageView, Page>();
            Mapper.CreateMap<RolPageAction, RolPageActionView>();
            Mapper.CreateMap<RoleView, RolPageAction>();

        }

        public List<ListItem> GetRoles()
        {
            try
            {
                return mRolesManager.GetRoles().Select(c => new ListItem { Value = c.IdRol.ToString(), Text = c.Description }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RoleView GetRol(int IdRol)
        {
            try
            {
                Role mDbRol = mRolesManager.GetRol(IdRol);

                return Mapper.Map<RoleView>(mDbRol);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
