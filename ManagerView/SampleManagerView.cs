﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class SampleManagerView
    {
        private SampleManager mSampleManager;

        public SampleManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<ARGNSModelSample, ARGNSModelSampleView>();
            Mapper.CreateMap<ARGNSModelSampleView, ARGNSModelSample>();

            mSampleManager = new SampleManager();
        }

        public List<ARGNSModelSample> GetListSample(CompanyView pCc, string ItemCode)
        {
            return mSampleManager.GetListSample(Mapper.Map<CompanyConn>(pCc), ItemCode);
        }        

        public ARGNSModelSampleView GetSample(CompanyView pCc, string pSampleCode, string pModCode)
        {
            return Mapper.Map<ARGNSModelSampleView>(mSampleManager.GetSample(Mapper.Map<CompanyConn>(pCc), pSampleCode, pModCode));
        }

        public string AddSample(ARGNSModelSampleView pObject, CompanyView pCompanyParam)
        {
            return mSampleManager.AddSample(Mapper.Map<ARGNSModelSample>(pObject), Mapper.Map<CompanyConn>(pCompanyParam));
        }

        public string UpdateSample(ARGNSModelSampleView pObject, CompanyView pCompanyParam)
        {
            return mSampleManager.UpdateSample(Mapper.Map<ARGNSModelSample>(pObject), Mapper.Map<CompanyConn>(pCompanyParam));
        }

        public ARGNSModelSampleView GetSampleByCode(CompanyView pCc, string pSampleCode, string pModCode)
        {
            return Mapper.Map<ARGNSModelSampleView>(mSampleManager.GetSampleByCode(Mapper.Map<CompanyConn>(pCc), pSampleCode, pModCode));
        }
    }
}
