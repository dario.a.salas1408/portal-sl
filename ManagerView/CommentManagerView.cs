﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class CommentManagerView
    {
        private CommentManager mCommentManager;

        public CommentManagerView()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            mCommentManager = new CommentManager();
        }

        public List<ARGNSModelComment> GetListComments(CompanyView pCc, string ItemCode)
        {

            return mCommentManager.GetListComments(Mapper.Map<CompanyConn>(pCc), ItemCode);
        }

        public string AddComment(ARGNSModelComment pModel, CompanyView pCv)
        {
            try
            {
                return mCommentManager.AddComment(Mapper.Map<ARGNSModelComment>(pModel), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdateComment(ARGNSModelComment pModel, CompanyView pCv, string idUser)
        {
            try
            {
                return mCommentManager.UpdateComment(Mapper.Map<ARGNSModelComment>(pModel), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
