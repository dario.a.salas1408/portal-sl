﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class RangePlanManagerView
    {
        RangePlanManager mRangePlanManager;
        public RangePlanManagerView()
        {
            mRangePlanManager = new RangePlanManager();
            Mapper.CreateMap<ARGNSRangePlan, ARGNSRangePlanView>();
            Mapper.CreateMap<ARGNSRangePlanView, ARGNSRangePlan>();
            Mapper.CreateMap<ARGNSRangePlanDetailView, ARGNSRangePlanDetail>();
            Mapper.CreateMap<ARGNSRangePlanDetail, ARGNSRangePlanDetailView>();
        }

        public List<ARGNSRangePlanView> GetRangePlanListSearch(CompanyView pCc, ref RangePlanCombo mRangePlanCombo, string pCode = "", string pSeason = "", string pCollection = "", string pEmployee = "")
        {
            try
            {
                List<ARGNSRangePlanView> mReturn = Mapper.Map<List<ARGNSRangePlanView>>(mRangePlanManager.GetRangePlanListSearch(Mapper.Map<CompanyConn>(pCc), ref mRangePlanCombo, pCode, pSeason, pCollection, pEmployee));

                return mReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ARGNSRangePlanView GetRangePlanById(CompanyView pCv, string pCode)
        {
            try
            {
                ARGNSRangePlan mRangePlan = mRangePlanManager.GetRangePlanById(Mapper.Map<CompanyConn>(pCv), pCode);

                return Mapper.Map<ARGNSRangePlanView>(mRangePlan);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RangePlanCombo GetRangePlanCombo(CompanyView pCv)
        {
            try
            {
                return mRangePlanManager.GetRangePlanCombo(Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(ARGNSRangePlanView pRangePlan, CompanyView pCv)
        {
            try
            {
                return mRangePlanManager.Update(Mapper.Map<ARGNSRangePlan>(pRangePlan), Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
