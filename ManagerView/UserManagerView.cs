﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Manager;
using ARGNS.View;
using ARGNS.Model.Implementations;
using ARGNS.Model.Interfaces;
using ARGNS.Model;
using AutoMapper;
using System.Web.UI.WebControls;
using ARGNS.Util;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;


namespace ARGNS.ManagerView
{
    public class UserManagerView
    {
        UserManager mUserManager;
        UserSapManager mUserSapManager;
        CompaniesManagerView mCompaniesManagerView;
        AreaKeyManager mAreaKeyManager;
        PageManager mPageManager;
        Security mSecurity;
        RolesManagerView mRolesManagerView;
        RolesManagerView mRolesManagerViewAux;

        public UserManagerView()
        {
            mUserManager = new UserManager();
            mUserSapManager = new UserSapManager();
            mCompaniesManagerView = new CompaniesManagerView();
            mSecurity = new Security();
            mPageManager = new PageManager();
            mRolesManagerView = new RolesManagerView();
            mAreaKeyManager = new AreaKeyManager();
            mRolesManagerViewAux = new RolesManagerView();

            Mapper.CreateMap<User, UserView>();
            Mapper.CreateMap<UserView, User>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<UsersSetting, UsersSettingView>();
            Mapper.CreateMap<UsersSettingView, UsersSetting>();
            Mapper.CreateMap<Page, PageView>();
            Mapper.CreateMap<PageView, Page>();
            Mapper.CreateMap<AreaKeyView, AreaKey>();
            Mapper.CreateMap<AreaKey, AreaKeyView>();
            Mapper.CreateMap<Actions, ActionsView>();
            Mapper.CreateMap<ActionsView, Actions>();

            Mapper.CreateMap<CompanyStock, CompanyStockView>();
            Mapper.CreateMap<CompanyStockView, CompanyStock>();

        }

        public string GetUser()
        {
            try
            {
                UserManager ttt = new UserManager();

                ttt.GetUsuario();

                return "";
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public UserView GetUser(CompanyView pCv, int Id)
        {
            try
            {
                User mDBUser = mUserManager.GetUsuario(Id);

                UserView mViewRet = Mapper.Map<UserView>(mDBUser);

                mViewRet.AccessUser = mDBUser.UserPageActions.Select(c => c.Page.IdAreaKeys.ToString() + "-" + c.IdPage.ToString() + "-" + c.IdAction.ToString()).ToArray();

                mViewRet.ListArea = Mapper.Map<List<AreaKeyView>>(mAreaKeyManager.GetAreasAllowed());

                if (mDBUser.Roles.Description.ToUpper() == Enums.Rols.Administrator.ToDescriptionString())
                {
                    mViewRet.ListArea = mViewRet.ListArea.Where(c => c.Area.ToUpper() == Enums.Areas.WEBPORTAL.ToDescriptionString().ToUpper()).ToList();
                    mViewRet.ListRoles = mRolesManagerView.GetRoles().Where(c => c.Text.ToUpper() == Enums.Rols.Administrator.ToDescriptionString()).ToList();
                    mViewRet.IsAdmin = true;
                }
                else
                {
                    mViewRet.ListRoles = mRolesManagerView.GetRoles().Where(c => c.Text.ToUpper() != Enums.Rols.Administrator.ToDescriptionString()).ToList();
                }

                if (mDBUser.Roles.Description.ToUpper() == Enums.Rols.BusinessPartner.ToDescriptionString())
                {
                    mViewRet.IsCustomer = true;
                }
                if (mDBUser.Roles.Description.ToUpper() == Enums.Rols.SalesEmployee.ToDescriptionString())
                {
                    mViewRet.IsSalesEmployee = true;
                }
                mViewRet.Companies = mDBUser.UsersSettings.Select(c => Mapper.Map<CompanyView>(c.CompanyConn)).ToList();

                return mViewRet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonObjectResult GetUsers(int pStart, int pLength, string pUserName)
        {
            try
            {
                return  mUserManager.GetUsuarios(pStart, pLength, pUserName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetUsers:" + ex.Message);
                Logger.WriteError("UserManagerView -> GetUsers:" + ex.InnerException);
                throw; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUserName"></param>
        /// <param name="pPsw"></param>
        /// <returns></returns>
        public UserView Signin(string pUserName, string pPsw)
        {
            try
            {

                User mDbUser = mUserManager.Signin(pUserName, pPsw);

                UserView mUserView = Mapper.Map<UserView>(mDbUser);

                if (mDbUser == null)
                { return mUserView; }

                mUserView.AccessUser = mDbUser.UserPageActions.Select(c => c.Page.IdAreaKeys.ToString() + "-" + c.IdPage.ToString() + "-" + c.IdAction.ToString()).ToArray();

                mUserView.ListUserPageAction = mDbUser.UserPageActions.Select(c => new UserPageActionView { IdAction = c.IdAction, IdAreaKey = c.Page.IdAreaKeys, IdPage = c.IdPage, IdUser = c.IdUser, InternalKey = c.Action.InternalKey, PageInternalKey = c.Page.InternalKey }).ToList();

                mUserView.IsValidUser = IsInvalidUser(mDbUser);

                if (mUserView.IsValidUser)
                {

                    mUserView.Companies = mDbUser.UsersSettings.Where(c => c.CompanyConn.Active == true).Select(c => Mapper.Map<CompanyView>(c.CompanyConn)).OrderBy(c => c.VisualOrder).ToList();

                    mUserView.IsAdmin = (mDbUser.Roles.Description.ToUpper() == Enums.Rols.Administrator.ToDescriptionString().ToUpper()) ? true : false;

                    mUserView.IsCustomer = (mDbUser.Roles.Description.ToUpper() == Enums.Rols.BusinessPartner.ToDescriptionString().ToUpper()) ? true : false;

                    mUserView.IsSalesEmployee = (mDbUser.Roles.Description.ToUpper() == Enums.Rols.SalesEmployee.ToDescriptionString().ToUpper()) ? true : false;

                    mUserView.IsUser = (mDbUser.Roles.Description.ToUpper() == Enums.Rols.User.ToDescriptionString().ToUpper()) ? true : false;

                    List<Page> ListPage = mPageManager.GetPageByArea(Enums.Areas.WEBPORTAL.ToDescriptionString());

                    if (mUserView.Companies.Count == 0)
                    {
                        bool mUseSap = true;

                        mUserView.WorkOffline = true;
                        mUserView.Companies.Add(new CompanyView { CompanyDB = "Disconnected" });

                        foreach (Page item in ListPage)
                        {
                            if (mSecurity.CkeckPage(item.KeyAdd, item.IdPage, out mUseSap))
                            {
                                if (!mUseSap)
                                {
                                    foreach (string itemAccess in mUserView.AccessUser)
                                    {
                                        string[] mPageAccess = itemAccess.Split('-');

                                        if (mPageAccess[1] == item.IdPage.ToString())
                                        {
                                            mUserView.ListPage.Add(Mapper.Map<PageView>(item));
                                            break;
                                        }

                                    }

                                }
                            }
                        }

                    }
                    else
                    {
                        bool mUseSap = true;

                        List<AreaKey> ListAreas = mDbUser.UserAreas.Where(c => c.AreaKey.Area != Enums.Areas.WEBPORTAL.ToDescriptionString()).Select(c => c.AreaKey).ToList();

                        foreach (AreaKey item in ListAreas)
                        {
                            string mKeyadd = mDbUser.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).Select(c => c.KeyAdd).FirstOrDefault();

                            if (mSecurity.CkeckUser(mKeyadd, item.Area, mUserView.IdUser))
                            {
                                mUserView.ListArea.Add(Mapper.Map<AreaKeyView>(item));
                            }
                        }

                        foreach (Page item in ListPage)
                        {
                            if (mSecurity.CkeckPage(item.KeyAdd, item.IdPage, out mUseSap))
                            {
                                foreach (string itemAccess in mUserView.AccessUser)
                                {
                                    string[] mPageAccess = itemAccess.Split('-');

                                    if (mPageAccess[1] == item.IdPage.ToString())
                                    {
                                        mUserView.ListPage.Add(Mapper.Map<PageView>(item));
                                        break;
                                    }

                                }
                            }
                        }
                    }
                }

                return mUserView;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Add(UserView pUser)
        {
            try
            {
                User mUserAdd = new User();
                Int32 mIdAreaKey = 0;

                mUserAdd = Mapper.Map<User>(pUser);

                foreach (string item in pUser.AccessUser)
                {
                    string[] values = item.Split('-');

                    if (values.Length == 3)
                    {
                        mUserAdd.UserPageActions.Add(new UserPageAction { IdUser = pUser.IdUser, IdPage = Convert.ToInt32(values[1]), IdAction = Convert.ToInt32(values[2]) });

                        if (mIdAreaKey == 0 || (mIdAreaKey != Convert.ToInt32(values[0])))
                        {
                            AreaKeyManager mAreaKeyManagerAux = new AreaKeyManager();

                            AreaKey mAreaKey = mAreaKeyManagerAux.GetAreaById(Convert.ToInt32(values[0]));

                            mUserAdd.UserAreas.Add(new UserArea { AreaKey = mAreaKey });

                            mIdAreaKey = Convert.ToInt32(values[0]);

                            mAreaKeyManagerAux = null;
                        }

                    }

                }

                return mUserManager.Add(mUserAdd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool ValidateExistingUser(string userName)
        {
            try
            {
                return mUserManager.ValidateExistingUser(userName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> ValidateExistingUser: " + ex.Message);
                return false; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Update(UserView pUser)
        {
            try
            {
                User mUserUpdate = new User();
                Int32 mIdAreaKey = 0;

                mUserUpdate = Mapper.Map<User>(pUser);

                foreach (string item in pUser.AccessUser)
                {
                    string[] values = item.Split('-');

                    if (values.Length == 3)
                    {
                        mUserUpdate.UserPageActions.Add(new UserPageAction { IdUser = pUser.IdUser, IdPage = Convert.ToInt32(values[1]), IdAction = Convert.ToInt32(values[2]) });

                        if (mIdAreaKey == 0 || (mIdAreaKey != Convert.ToInt32(values[0])))
                        {
                            AreaKeyManager mAreaKeyManagerAux = new AreaKeyManager();

                            AreaKey mAreaKey = mAreaKeyManagerAux.GetAreaById(Convert.ToInt32(values[0]));

                            mUserUpdate.UserAreas.Add(new UserArea { AreaKey = mAreaKey });

                            mIdAreaKey = Convert.ToInt32(values[0]);

                            mAreaKeyManagerAux = null;
                        }
                    }
                }

                return mUserManager.Update(mUserUpdate);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Delete(UserView pUser)
        {
            try
            {
                return mUserManager.Delete(Mapper.Map<User>(pUser));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Duplicate(UserView pUser)
        {
            try
            {
                return mUserManager.Duplicate(pUser.IdUser, pUser.Name, pUser.Password);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> Duplicate: " + ex.Message);
                Logger.WriteError("UserManagerView -> Duplicate: " + ex.InnerException);
                return false; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public UserView EmptyViewUser(CompanyView pCv)
        {
            try
            {

                UserView mViewRet = new UserView();

                mViewRet.ListArea = Mapper.Map<List<AreaKeyView>>(mAreaKeyManager.GetAreasAllowed());

                mViewRet.ListRoles = mRolesManagerView.GetRoles().Where(c => c.Text.ToUpper() != Enums.Rols.Administrator.ToDescriptionString()).ToList();

                RoleView mRoleView = mRolesManagerViewAux.GetRol(Convert.ToInt32(mViewRet.ListRoles.FirstOrDefault().Value));

                mViewRet.AccessUser = mRoleView.RolPageActions.Select(c => c.Page.IdAreaKeys.ToString() + "-" + c.IdPage.ToString() + "-" + c.IdAction.ToString()).ToArray();

                return mViewRet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <param name="IdCompany"></param>
        /// <param name="UserNameSAP"></param>
        /// <param name="UserPasswordSAP"></param>
        /// <param name="UserCodeSAP"></param>
        /// <param name="BPCode"></param>
        /// <param name="SECode"></param>
        /// <param name="DftDistRule"></param>
        /// <param name="DftWhs"></param>
        /// <param name="pSalesTaxCodeDef"></param>
        /// <param name="pPurchaseTaxCodeDef"></param>
        /// <param name="pUserGroupId"></param>
        /// <param name="pCostPriceList"></param>
        /// <param name="pBPGroupId"></param>
        /// <returns></returns>
        public bool AddCompanyToUser(int IdUser, int IdCompany, string UserNameSAP, string UserPasswordSAP, string UserCodeSAP, string BPCode, int? SECode, string DftDistRule, string DftWhs, string pSalesTaxCodeDef, string pPurchaseTaxCodeDef, int? pUserGroupId, int? pCostPriceList, int? pBPGroupId)
        {
            try
            {
                return mUserManager.AddCompanyToUser(IdUser, IdCompany, UserNameSAP, UserPasswordSAP, UserCodeSAP, BPCode, SECode, DftDistRule, DftWhs,  pSalesTaxCodeDef,  pPurchaseTaxCodeDef, pUserGroupId, pCostPriceList, pBPGroupId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public bool DeleteCompanyToUser(int IdUser, int IdCompany)
        {
            try
            {
                return mUserManager.DeleteCompanyToUser(IdUser, IdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> DeleteCompanyToUser: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDbUser"></param>
        /// <returns></returns>
        private bool IsInvalidUser(User pDbUser)
        {
            List<string> mKeys = pDbUser.UserAreas.Select(c => c.KeyAdd).ToList();

            int mCountKey = mKeys.Count();

            int mCountDisKey = (from c in mKeys select c).Distinct().Count();

            if (mCountKey != mCountDisKey)
            { return false; }

            bool mValida = false;

            foreach (string item in mKeys)
            {
                mValida = mSecurity.CkeckUserintegrity(item, pDbUser.IdUser);

                if (!mValida)
                { break; }
            }

            return mValida;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<ListItem> GetUserSap(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetListUsers(Mapper.Map<CompanyConn>(pCv)).Select(c => new ListItem { Value = c.USER_CODE.ToString(), Text = c.U_NAME }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetUserSap: " + ex.Message);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<UserSAP> GetUserSapList(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetListUsers(Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetUserSapList: " + ex.Message);
                return new List<UserSAP>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<ListItem> GetBusinessPartnerList(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetBusinessPartnerList(Mapper.Map<CompanyConn>(pCv)).Select(c => new ListItem { Value = c.CardCode.ToString(), Text = c.CardName }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetBusinessPartnerList: " + ex.Message);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<ListItem> GetSalesEmployeeList(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetAllSalesEmployeeList(Mapper.Map<CompanyConn>(pCv)).Select(c => new ListItem { Value = c.SlpCode.ToString(), Text = c.SlpName }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetSalesEmployeeList: " + ex.Message);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pSEName"></param>
        /// <returns></returns>
        public List<ListItem> GetSalesEmployeeSearchByName(CompanyView pCv, string pSEName)
        {
            try
            {
                return mUserSapManager.GetSalesEmployeeSearchByName(Mapper.Map<CompanyConn>(pCv), pSEName).Select(c => new ListItem { Value = c.SlpCode.ToString(), Text = c.SlpName }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetSalesEmployeeSearchByName: " + ex.Message);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<ListItem> GetSalesTaxCode(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetSalesTaxCode(Mapper.Map<CompanyConn>(pCv)).Select(c => new ListItem { Value = c.Code.ToString(), Text = c.Name }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetSalesTaxCode: " + ex.Message);
                return new List<ListItem>();
            }
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<ListItem> GetWarehouseList(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetWarehouseList(Mapper.Map<CompanyConn>(pCv)).Select(c => new ListItem { Value = c.WhsCode.ToString(), Text = c.WhsName }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetWarehouseList: " + ex.Message);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pWhsCode"></param>
        /// <param name="pWhsName"></param>
        /// <returns></returns>
        public List<WarehouseView> GetWarehouseListSearch(CompanyView pCv, string pWhsCode, string pWhsName)
        {
            Mapper.CreateMap<Warehouse, WarehouseView>();
            Mapper.CreateMap<WarehouseView, Warehouse>();

            try
            {
                return Mapper.Map<List<WarehouseView>>(mUserSapManager.GetWarehouseListSearch(Mapper.Map<CompanyConn>(pCv), pWhsCode, pWhsName));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetWarehouseListSearch: " + ex.Message);
                return new List<WarehouseView>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<ListItem> GetDistributionRuleList(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetDistributionRuleList(Mapper.Map<CompanyConn>(pCv)).Select(c => new ListItem { Value = c.OcrCode.ToString(), Text = c.OcrName }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetDistributionRuleList: " + ex.Message);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool UpdateMyAccount(UserView pUser)
        {
            try
            {
                User mUserUpdate = new User();
                mUserUpdate = Mapper.Map<User>(pUser);
                return mUserManager.UpdateMyAccount(mUserUpdate);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public List<ListItem> GetEmployeeList(CompanyView pCc)
        {
            try
            {

                return mUserSapManager.GetAllEmployeeList(Mapper.Map<CompanyConn>(pCc)).Select(c => new ListItem { Value = c.empID.ToString(), Text = c.getName }).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetEmployeeList :" + ex.InnerException);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public List<UserUDFView> GetUserUDF(int IdUser)
        {
            try
            {
                Mapper.CreateMap<UserUDFView, UserUDF>();
                Mapper.CreateMap<UserUDF, UserUDFView>();
                return Mapper.Map<List<UserUDFView>>(mUserSapManager.GetUserUDF(IdUser).ToList());

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetUserUDF :" + ex.InnerException);
                return new List<UserUDFView>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pIdUser"></param>
        /// <param name="pIdCompany"></param>
        /// <param name="pDocument"></param>
        /// <returns></returns>
        public List<UDF_ARGNS> GetUserUDFSearch(CompanyView pCc, int pIdUser, int pIdCompany, string pDocument)
        {
            try
            {
                Mapper.CreateMap<UDF_ARGNS, UserUDF>();
                Mapper.CreateMap<UserUDF, UDF_ARGNS>();
                List<UDF_ARGNS> mUDFList = Mapper.Map<List<UDF_ARGNS>>(mUserSapManager.GetUserUDFSearch(pIdUser, pIdCompany, pDocument).ToList());
                List<UFD1_SAP> mUFD1List = mUserSapManager.GetUDF1ByTableID(Mapper.Map<CompanyConn>(pCc), pDocument).ToList();
                foreach (UDF_ARGNS mUDF in mUDFList)
                {
                    if (!string.IsNullOrEmpty(mUDF.RTable))
                    {
                        mUDF.ListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(mUserSapManager.GetQueryResult(Mapper.Map<CompanyConn>(pCc), (pCc.ServerType == 9 ? "SELECT \"Code\" AS \"FldValue\", \"Name\" AS \"Descr\" FROM \"" + pCc.CompanyDB + "\".\"@" + mUDF.RTable + "\"" : "Select Code as FldValue, Name as Descr From [@" + mUDF.RTable + "]") ));
                    }
                    else
                    {
                        mUDF.ListUFD1 = mUFD1List.Where(j => j.FieldID == mUDF.FieldID).ToList();
                    }
                }

                return mUDFList;

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetUserUDFSearch :" + ex.InnerException);
                return new List<UDF_ARGNS>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<SAPObjectView> GetSAPObjects()
        {
            try
            {
                Mapper.CreateMap<SAPObject, SAPObjectView>();
                Mapper.CreateMap<SAPObjectView, SAPObject>();
                return Mapper.Map<List<SAPObjectView>>(mUserManager.GetSAPObjects());
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetSAPObjects:" + ex.InnerException);
                return new List<SAPObjectView>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pTableID"></param>
        /// <returns></returns>
        public List<UDF_SAPView> GetUDFByTableID(CompanyView pCc, string pTableID)
        {
            try
            {
                Mapper.CreateMap<UDF_SAP, UDF_SAPView>();
                Mapper.CreateMap<UDF_SAPView, UDF_SAP>();
                return Mapper.Map<List<UDF_SAPView>>(mUserSapManager.GetUDFByTableID(Mapper.Map<CompanyConn>(pCc), pTableID));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetUDFByTableID:" + ex.InnerException);
                return new List<UDF_SAPView>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUserID"></param>
        /// <param name="pCompanyId"></param>
        /// <param name="pTableID"></param>
        /// <param name="pUDFName"></param>
        /// <param name="pFieldID"></param>
        /// <param name="pTypeID"></param>
        /// <param name="pUDFDesc"></param>
        /// <param name="pRTable"></param>
        /// <returns></returns>
        public bool AddUDF(int pUserID, int pCompanyId, string pTableID, string pUDFName, int pFieldID, string pTypeID, string pUDFDesc, string pRTable)
        {
            try
            {
                return mUserManager.AddUDF(pUserID, pCompanyId, pTableID, pUDFName, pFieldID, pTypeID, pUDFDesc, pRTable);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> AddUDF:" + ex.InnerException);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <param name="IdUDF"></param>
        /// <returns></returns>
        public bool DeleteUDF(int IdUser, int IdUDF)
        {
            try
            {
                return mUserManager.DeleteUDF(IdUser, IdUDF);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> DeleteUDF:" + ex.InnerException);
                return false; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public List<AlertView> GetAlertsByUser(CompanyView pCv, int pUser)
        {
            try
            {
                Mapper.CreateMap<AlertSAP, AlertView>();
                Mapper.CreateMap<AlertView, AlertSAP>();
                UserSAP user = new UserSAP();
                UsersSetting userSetting = mUserSapManager.GetCompany(pCv.IdCompany, pUser);
                user = mUserSapManager.GetUserSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), userSetting.UserCodeSAP).FirstOrDefault();
                return Mapper.Map<List<AlertView>>(mUserSapManager.GetAlertsByUser(Mapper.Map<CompanyConn>(pCv), user.USERID));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetAlertsByUser:" + ex.InnerException);
                return new List<AlertView>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pAlertId"></param>
        /// <returns></returns>
        public AlertView GetAlertById(CompanyView pCv, int pAlertId)
        {
            try
            {
                Mapper.CreateMap<AlertSAP, AlertView>();
                Mapper.CreateMap<AlertView, AlertSAP>();
                return Mapper.Map<AlertView>(mUserSapManager.GetAlertById(Mapper.Map<CompanyConn>(pCv), pAlertId));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetAlertById:" + ex.InnerException);
                return new AlertView();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="mQuery"></param>
        /// <returns></returns>
        public string GetQueryResult(CompanyView pCv, string mQuery)
        {
            try
            {
                return mUserSapManager.GetQueryResult(Mapper.Map<CompanyConn>(pCv), mQuery);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetQueryResult :" + ex.InnerException);
                return "";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pQuery"></param>
        /// <returns></returns>
        public JsonObjectResult GetSAPQueryResult(CompanyView pCv, string pQuery)
        {
            try
            {
                return mUserSapManager.GetSAPQueryResult(Mapper.Map<CompanyConn>(pCv), pQuery);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetSAPQueryResult :" + ex.InnerException);
                return new JsonObjectResult();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public List<ListItem> GetAllPriceListSAP(CompanyView pCv)
        {
            try
            {
                return mUserSapManager.GetAllPriceListSAP(Mapper.Map<CompanyConn>(pCv)).Select(c => new ListItem { Value = c.ListNum.ToString(), Text = c.ListName }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManagerView -> GetAllPriceListSAP :" + ex.InnerException);
                return new List<ListItem>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCv"></param>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public UserSAP GetUserSAPByPortalUser(CompanyView pCv, int pUser)
        {
            UserSAP mUser = new UserSAP();
            UsersSetting userSetting = mUserSapManager.GetCompany(pCv.IdCompany, pUser);
            mUser = mUserSapManager.GetUserSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), userSetting.UserCodeSAP).FirstOrDefault();
            return mUser;
        }
    }
}
