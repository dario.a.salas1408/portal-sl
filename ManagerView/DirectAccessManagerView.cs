﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class DirectAccessManagerView
    {
        private DirectAccessManager mDirectAccessManager;

        public DirectAccessManagerView()
        {
            mDirectAccessManager = new DirectAccessManager();
        }

        public List<DirectAccessView> GetDirectAccessList()
        {
            Mapper.CreateMap<DirectAccess, DirectAccessView>();
            Mapper.CreateMap<DirectAccessView, DirectAccess>();

            return Mapper.Map<List<DirectAccessView>>(mDirectAccessManager.GetDirectAccessList());
        }

        public List<UserDirectAccessView> GetUserDirectAccessList(int pIdUSer, int pIdCompany)
        {
            Mapper.CreateMap<UserDirectAccess, UserDirectAccessView>();
            Mapper.CreateMap<UserDirectAccessView, UserDirectAccess>();

            return Mapper.Map<List<UserDirectAccessView>>(mDirectAccessManager.GetUserDirectAccessList(pIdUSer, pIdCompany));
        }

        public List<ActionsView> GetActionList()
        {
            Mapper.CreateMap<Actions, ActionsView>();
            Mapper.CreateMap<ActionsView, Actions>();

            return Mapper.Map<List<ActionsView>>(mDirectAccessManager.GetActionList());
        }

        public bool UpdateMyDirectAccesses(List<int> pListDirectAccesses, List<int> pListDirectAccessesCustom, int pIdUser, int pIdCompany)
        {
            try
            {
                return mDirectAccessManager.UpdateMyDirectAccesses(pListDirectAccesses, pListDirectAccessesCustom, pIdUser, pIdCompany);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
