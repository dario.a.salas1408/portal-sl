﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PDCManagerView
    {
        PDCManager mPDCManager;
        public PDCManagerView()
        {
            mPDCManager = new PDCManager();
        }

        public ARGNSPDCView GetPDCById(CompanyView pCv, string pPDCCode, ref PDCSAPCombo combo)
        {
            try
            {
                Mapper.CreateMap<CompanyConn, CompanyView>();
                Mapper.CreateMap<CompanyView, CompanyConn>();
                Mapper.CreateMap<ARGNSPDC, ARGNSPDCView>();
                Mapper.CreateMap<ARGNSPDCView, ARGNSPDC>();

                ARGNSPDC mPDC = mPDCManager.GetPDCByID(Mapper.Map<CompanyConn>(pCv), pPDCCode, ref combo);

                return Mapper.Map<ARGNSPDCView>(mPDC);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSPDCView> GetPDCList(CompanyView pCv, ref PDCSAPCombo combo, DateTime? pPD, string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "")
        {
            try
            {
                Mapper.CreateMap<CompanyConn, CompanyView>();
                Mapper.CreateMap<CompanyView, CompanyConn>();
                Mapper.CreateMap<ARGNSPDC, ARGNSPDCView>();
                Mapper.CreateMap<ARGNSPDCView, ARGNSPDC>();

                return Mapper.Map<List<ARGNSPDCView>>(mPDCManager.GetPDCList(Mapper.Map<CompanyConn>(pCv), ref combo, pPD, pCode, pEmployee, pShift, pVendor));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PDCSAPCombo GetPDCCombo(CompanyView pCv)
        {
            try
            {
                Mapper.CreateMap<CompanyConn, CompanyView>();
                Mapper.CreateMap<CompanyView, CompanyConn>();

                return mPDCManager.GetCombo(Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ARGNSPDCLine GetPDCLineCodeBar(CompanyView pCv, string CodeBar)
        {
            try
            {
                Mapper.CreateMap<CompanyConn, CompanyView>();
                Mapper.CreateMap<CompanyView, CompanyConn>();

                return mPDCManager.GetPDCLineCodeBar(Mapper.Map<CompanyConn>(pCv), CodeBar);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSPDCLine> GetPDCLinesCutTick(CompanyView pCv, string CutTick)
        {
            try
            {
                Mapper.CreateMap<CompanyConn, CompanyView>();
                Mapper.CreateMap<CompanyView, CompanyConn>();
                return mPDCManager.GetPDCLinesCutTick(Mapper.Map<CompanyConn>(pCv), CutTick).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(CompanyView pCv, ARGNSPDCView model)
        {
            try
            {
                return mPDCManager.Add(Mapper.Map<CompanyConn>(pCv), Mapper.Map<ARGNSPDC>(model));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
