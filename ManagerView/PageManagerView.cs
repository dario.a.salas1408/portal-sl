﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PageManagerView
    {
        PageManager mPageManager;

        public PageManagerView()
        {
            mPageManager = new PageManager();
        }

        public List<PageView> GetPageList()
        {
            Mapper.CreateMap<Page, PageView>();
            Mapper.CreateMap<PageView, Page>();

            return Mapper.Map<List<PageView>>(mPageManager.GetPageList());

        }
    }
}
