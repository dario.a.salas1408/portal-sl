﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class PurchaseInvoiceManagerView
    {
        PurchaseInvoiceManager mPurchaseInvoiceManager;
        UserSapManager mUserSapManager;

        public PurchaseInvoiceManagerView()
        {
            mPurchaseInvoiceManager = new PurchaseInvoiceManager();
            mUserSapManager = new UserSapManager();

            Mapper.CreateMap<PurchaseInvoiceSAP, PurchaseInvoiceView>();
            Mapper.CreateMap<PurchaseInvoiceView, PurchaseInvoiceSAP>();
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<PurchaseInvoiceLineSAP, PurchaseInvoiceLineView>();
            Mapper.CreateMap<PurchaseInvoiceLineView, PurchaseInvoiceLineSAP>();


        }

        public PurchaseInvoiceView GetPurchaseInvoice(int Id, int pUserId, CompanyView pCv)
        {
            try
            {
                EmployeeSAP mESAP;
                PurchaseInvoiceSAP mPI = mPurchaseInvoiceManager.GetPurchaseInvoice(Mapper.Map<CompanyConn>(pCv), Id, pUserId);
                PurchaseInvoiceView mPIV = Mapper.Map<PurchaseInvoiceView>(mPI);
                mESAP = mPI.Employee;
                //if (Id != 0)
                //{
                //    EmployeeSAP mESAP = mUserSapManager.GetEmployeeById(Mapper.Map<CompanyConn>(pCv), mPI.OwnerCode);
                //    if (mESAP != null)
                //        mPIV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                //}
                if (mESAP != null)
                    mPIV.OwnerName = mESAP.firstName + " " + mESAP.lastName;
                return mPIV;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonObjectResult GetPurchaseInvoiceListSearch(CompanyView pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mPurchaseInvoiceManager.GetPurchaseInvoiceListSearch(Mapper.Map<CompanyConn>(pCc), pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyView pCv, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapManager.GetEmployeeSAPSearchByCode(Mapper.Map<CompanyConn>(pCv), pRequesterCode, pRequesterName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
