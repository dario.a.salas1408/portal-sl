﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class QueryManagerManagerView
    {
        QueryManagerManager mQueryManagerManager;

        public QueryManagerManagerView()
        {
            mQueryManagerManager = new QueryManagerManager();
        }

        public List<QueryManagerItemView> GetListQueryByCompany(int pIdCompany)
        {
            try
            {
                Mapper.CreateMap<QueryManagerItem, QueryManagerItemView>();
                Mapper.CreateMap<QueryManagerItemView, QueryManagerItem>();
                return Mapper.Map<List<QueryManagerItemView>>(mQueryManagerManager.GetListQueryByCompany(pIdCompany));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QueryManagerItemView GetQuery(int pIdQuery)
        {
            try
            {
                Mapper.CreateMap<QueryManagerItem, QueryManagerItemView>();
                Mapper.CreateMap<QueryManagerItemView, QueryManagerItem>();
                return Mapper.Map<QueryManagerItemView>(mQueryManagerManager.GetQuery(pIdQuery));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add(QueryManagerItemView pQueryManagerItemModel)
        {
            try
            {
                Mapper.CreateMap<QueryManagerItem, QueryManagerItemView>();
                Mapper.CreateMap<QueryManagerItemView, QueryManagerItem>();
                return mQueryManagerManager.Add(Mapper.Map<QueryManagerItem>(pQueryManagerItemModel));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Update(QueryManagerItemView pQueryManagerItemModel)
        {
            try
            {
                Mapper.CreateMap<QueryManagerItem, QueryManagerItemView>();
                Mapper.CreateMap<QueryManagerItemView, QueryManagerItem>();
                return mQueryManagerManager.Update(Mapper.Map<QueryManagerItem>(pQueryManagerItemModel));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Delete(QueryManagerItemView pQueryManagerItemModel)
        {
            try
            {
                Mapper.CreateMap<QueryManagerItem, QueryManagerItemView>();
                Mapper.CreateMap<QueryManagerItemView, QueryManagerItem>();
                return mQueryManagerManager.Delete(Mapper.Map<QueryManagerItem>(pQueryManagerItemModel));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string GetQueryResult(CompanyView pCompanyParam, string pQueryIdentifier, Dictionary<string, string> pParams)
        {
            try
            {
                Mapper.CreateMap<CompanyConn, CompanyView>();
                Mapper.CreateMap<CompanyView, CompanyConn>();

                return mQueryManagerManager.GetQueryResult(Mapper.Map<CompanyConn>(pCompanyParam), pQueryIdentifier, pParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<QuerySAPView> GetSAPQueryListSearch(CompanyView pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<QuerySAP, QuerySAPView>();
                Mapper.CreateMap<QuerySAPView, QuerySAP>();
                Mapper.CreateMap<QueryCategorySAP, QueryCategorySAPView>();
                Mapper.CreateMap<QueryCategorySAPView, QueryCategorySAP>();
                return Mapper.Map<List<QuerySAPView>>(mQueryManagerManager.GetSAPQueryListSearch(Mapper.Map<CompanyConn>(pCompanyParam)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
