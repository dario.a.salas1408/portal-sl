﻿using ARGNS.Manager;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using ARGNS.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.ManagerView
{
    public class CRPageMapManagerView
    {
        CRPageMapManager mCRPageMapManager;

        public CRPageMapManagerView()
        {
            mCRPageMapManager = new CRPageMapManager();
            Mapper.CreateMap<CRPageMap, CRPageMapView>();
            Mapper.CreateMap<CRPageMapView, CRPageMap>();
            Mapper.CreateMap<Page, PageView>();
            Mapper.CreateMap<PageView, Page>();
        }

        public List<CRPageMapView> GetCRPageMap(int pIdCompany)
        {
            try
            {
                List<CRPageMap> mCRPageMap = mCRPageMapManager.GetCRPageMap(pIdCompany);

                return Mapper.Map<List<CRPageMapView>>(mCRPageMap);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<PageView> getPagesToAdd(string[] pPagesAdded)
        {
            try
            {
                List<Page> mPagesToAdd = mCRPageMapManager.getPagesToAdd(pPagesAdded);

                return Mapper.Map<List<PageView>>(mPagesToAdd);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Update(List<CRPageMapView> listCRPageMap, int IdCompany)
        {
            try
            {
                return mCRPageMapManager.Update(Mapper.Map<List<CRPageMap>>(listCRPageMap), IdCompany);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Delete(int pIdICRPageMap)
        {
            try
            {
                return mCRPageMapManager.Delete(pIdICRPageMap);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<string> HasAPageCRMapping(Enums.Pages page, CompanyView pCv)
        {
            try
            {
                return mCRPageMapManager.HasAPageCRMapping(page, Mapper.Map<CompanyConn>(pCv));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
