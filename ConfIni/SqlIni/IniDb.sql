﻿    CREATE TABLE [dbo].[Actions] (
        [IdAction] [int] NOT NULL IDENTITY,
        [Description] [nvarchar](50),
        [Active] [bit] NOT NULL,
        [InternalKey] [nvarchar](max),
        [KeyResource] [nvarchar](150),
        CONSTRAINT [PK_dbo.Actions] PRIMARY KEY ([IdAction])
    )
    CREATE TABLE [dbo].[Pages] (
        [IdPage] [int] NOT NULL IDENTITY,
        [IdAreaKeys] [int] NOT NULL,
        [Description] [nvarchar](50),
        [Controller] [nvarchar](150),
        [Action] [nvarchar](150),
        [KeyResource] [nvarchar](150),
        [KeyAdd] [nvarchar](500),
        [Active] [bit] NOT NULL,
        [InternalKey] [nvarchar](max),
        [CR] [bit] NOT NULL,
        CONSTRAINT [PK_dbo.Pages] PRIMARY KEY ([IdPage])
    )
    CREATE INDEX [IX_IdAreaKeys] ON [dbo].[Pages]([IdAreaKeys])
    CREATE TABLE [dbo].[AreaKeys] (
        [IdAreaKeys] [int] NOT NULL IDENTITY,
        [Area] [nvarchar](20) NOT NULL,
        [KeyNumber] [nvarchar](500) NOT NULL,
        [Controller] [nvarchar](max),
        [Action] [nvarchar](150),
        [KeyResource] [nvarchar](150),
        CONSTRAINT [PK_dbo.AreaKeys] PRIMARY KEY ([IdAreaKeys])
    )
    CREATE TABLE [dbo].[UserAreas] (
        [IdAreaKeys] [int] NOT NULL,
        [IdUser] [int] NOT NULL,
        [KeyAdd] [nvarchar](500),
        CONSTRAINT [PK_dbo.UserAreas] PRIMARY KEY ([IdAreaKeys], [IdUser])
    )
    CREATE INDEX [IX_IdAreaKeys] ON [dbo].[UserAreas]([IdAreaKeys])
    CREATE INDEX [IX_IdUser] ON [dbo].[UserAreas]([IdUser])
    CREATE TABLE [dbo].[Users] (
        [IdUser] [int] NOT NULL IDENTITY,
        [IdRol] [int],
        [Name] [nvarchar](100),
        [Password] [nvarchar](max),
        [UserIdSAP] [smallint] NOT NULL,
        [UserPasswordSAP] [nvarchar](max),
        [Active] [bit] NOT NULL,
        CONSTRAINT [PK_dbo.Users] PRIMARY KEY ([IdUser])
    )
    CREATE INDEX [IX_IdRol] ON [dbo].[Users]([IdRol])
    CREATE TABLE [dbo].[Authorizations] (
        [Id] [int] NOT NULL IDENTITY,
        [Name] [nvarchar](100),
        [User_IdUser] [int],
        CONSTRAINT [PK_dbo.Authorizations] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_User_IdUser] ON [dbo].[Authorizations]([User_IdUser])
    CREATE TABLE [dbo].[QuickOrders] (
        [IdQuickOrder] [int] NOT NULL IDENTITY,
        [IdUser] [int] NOT NULL,
		[IdCompany] [int] NOT NULL,
        [IdBp] [nvarchar](max),
        [ItemCode] [nvarchar](128) NOT NULL,
        [Quantity] [int] NOT NULL,
        [WarehouseCode] [nvarchar](10),
        CONSTRAINT [PK_dbo.QuickOrders] PRIMARY KEY ([IdQuickOrder], [ItemCode])
    )
    CREATE INDEX [IX_IdUser] ON [dbo].[QuickOrders]([IdUser])
	CREATE INDEX [IX_IdCompany] ON [dbo].[QuickOrders]([IdCompany])
    CREATE TABLE [dbo].[Roles] (
        [IdRol] [int] NOT NULL IDENTITY,
        [Description] [nvarchar](50),
        [Active] [bit] NOT NULL,
        CONSTRAINT [PK_dbo.Roles] PRIMARY KEY ([IdRol])
    )
    CREATE TABLE [dbo].[RolPageAction] (
        [IdRol] [int] NOT NULL,
        [IdPage] [int] NOT NULL,
        [IdAction] [int] NOT NULL,
        CONSTRAINT [PK_dbo.RolPageAction] PRIMARY KEY ([IdRol], [IdPage], [IdAction])
    )
    CREATE INDEX [IX_IdRol] ON [dbo].[RolPageAction]([IdRol])
    CREATE INDEX [IX_IdPage] ON [dbo].[RolPageAction]([IdPage])
    CREATE INDEX [IX_IdAction] ON [dbo].[RolPageAction]([IdAction])
    CREATE TABLE [dbo].[UserPageAction] (
        [IdUser] [int] NOT NULL,
        [IdPage] [int] NOT NULL,
        [IdAction] [int] NOT NULL,
        CONSTRAINT [PK_dbo.UserPageAction] PRIMARY KEY ([IdUser], [IdPage], [IdAction])
    )
    CREATE INDEX [IX_IdUser] ON [dbo].[UserPageAction]([IdUser])
    CREATE INDEX [IX_IdPage] ON [dbo].[UserPageAction]([IdPage])
    CREATE INDEX [IX_IdAction] ON [dbo].[UserPageAction]([IdAction])
    CREATE TABLE [dbo].[UsersSettings] (
        [IdUser] [int] NOT NULL,
        [IdCompany] [int] NOT NULL,
        [IdBp] [nvarchar](max),
        [IdSE] [int],
        [UserNameSAP] [nvarchar](50),
        [UserPasswordSAP] [nvarchar](50),
        [UserCodeSAP] [nvarchar](max),
        [DftDistRule] [nvarchar](max),
        [DftWhs] [nvarchar](max),
        [SalesTaxCodeDef] [nvarchar](max),
        [PurchaseTaxCodeDef] [nvarchar](max),
        [UserGroupId] [int],
        [CostPriceList] [int],
		[BPGroupId] [int],
        CONSTRAINT [PK_dbo.UsersSettings] PRIMARY KEY ([IdUser], [IdCompany])
    )
    CREATE INDEX [IX_IdUser] ON [dbo].[UsersSettings]([IdUser])
    CREATE INDEX [IX_IdCompany] ON [dbo].[UsersSettings]([IdCompany])
    CREATE TABLE [dbo].[CompanyConns] (
        [IdCompany] [int] NOT NULL IDENTITY,
        [ServerType] [int] NOT NULL,
        [Server] [nvarchar](100),
        [CompanyDB] [nvarchar](100),
        [UserName] [nvarchar](50),
        [Password] [nvarchar](254),
        [DbUserName] [nvarchar](50),
        [DbPassword] [nvarchar](254),
        [UseTrusted] [bit] NOT NULL,
        [UseCompanyUser] [bit] NOT NULL,
        [ItemInMultipleLines] [bit] NOT NULL,
        [UseHandheld] [bit] NOT NULL,
        [CheckBasketID] [bit] NOT NULL,
        [OnDemand] [bit] NOT NULL,
        [Active] [bit] NOT NULL,
        [LicenseServer] [nvarchar](max),
        [PortNumber] [int] NOT NULL,
        [SAPLanguaje] [nvarchar](10),
        [CodeType] [int],
        [UrlHana] [nvarchar](max),
        [UrlHanaGetQueryResult] [nvarchar](max),
        [VisualOrder] [smallint],
        CONSTRAINT [PK_dbo.CompanyConns] PRIMARY KEY ([IdCompany])
    )
    CREATE TABLE [dbo].[CompanyStock] (
        [IdCompanyStock] [int] NOT NULL IDENTITY,
        [IdCompany] [int] NOT NULL,
        [ValidateStock] [bit] NOT NULL,
        [Formula] [nvarchar](200) NOT NULL,
        CONSTRAINT [PK_dbo.CompanyStock] PRIMARY KEY ([IdCompanyStock])
    )
    CREATE INDEX [IX_IdCompany] ON [dbo].[CompanyStock]([IdCompany])
    CREATE TABLE [dbo].[CRPageMaps] (
        [IdICRPageMap] [int] NOT NULL IDENTITY,
        [IdPage] [int] NOT NULL,
        [IdCompany] [int] NOT NULL,
        [CRName] [nvarchar](max),
        CONSTRAINT [PK_dbo.CRPageMaps] PRIMARY KEY ([IdICRPageMap])
    )
    CREATE INDEX [IX_IdPage] ON [dbo].[CRPageMaps]([IdPage])
    CREATE INDEX [IX_IdCompany] ON [dbo].[CRPageMaps]([IdCompany])
    CREATE TABLE [dbo].[Alerts] (
        [Id] [int] NOT NULL IDENTITY,
        [Name] [nvarchar](100),
        [Description] [nvarchar](254),
        CONSTRAINT [PK_dbo.Alerts] PRIMARY KEY ([Id])
    )
    CREATE TABLE [dbo].[BPGroupLines] (
        [Id] [int] NOT NULL IDENTITY,
        [CardCode] [nvarchar](max),
		[CardName] [nvarchar](max),
        [BPGroup_Id] [int],
        CONSTRAINT [PK_dbo.BPGroupLines] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_BPGroup_Id] ON [dbo].[BPGroupLines]([BPGroup_Id])
    CREATE TABLE [dbo].[BPGroups] (
        [Id] [int] NOT NULL IDENTITY,
        [IdCompany] [int] NOT NULL,
        [Name] [nvarchar](100),
        CONSTRAINT [PK_dbo.BPGroups] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_IdCompany] ON [dbo].[BPGroups]([IdCompany])
    CREATE TABLE [dbo].[DirectAccesses] (
        [IdDirectAccess] [int] NOT NULL IDENTITY,
        [Page] [int] NOT NULL,
        [Action] [int] NOT NULL,
        [Area] [int] NOT NULL,
        [Url] [nvarchar](max),
        [UrlName] [nvarchar](max),
		[TranslationKey] [nvarchar](50),
        CONSTRAINT [PK_dbo.DirectAccesses] PRIMARY KEY ([IdDirectAccess])
    )
    CREATE TABLE [dbo].[InfoPortals] (
        [Id] [int] NOT NULL IDENTITY,
        [vsPortal] [nvarchar](254),
        [vsDB] [nvarchar](254),
        [IniDB] [datetime] NOT NULL,
        [LastUpdateDB] [datetime] NOT NULL,
        CONSTRAINT [PK_dbo.InfoPortals] PRIMARY KEY ([Id])
    )
    CREATE TABLE [dbo].[Logs] (
        [IdLog] [int] NOT NULL IDENTITY,
        [IdUser] [int] NOT NULL,
        [strClave] [nvarchar](250),
        [Tipo] [nvarchar](100),
        [strLog] [text],
        [dtLog] [datetime] NOT NULL,
        CONSTRAINT [PK_dbo.Logs] PRIMARY KEY ([IdLog])
    )
    CREATE INDEX [IX_IdUser] ON [dbo].[Logs]([IdUser])
    CREATE TABLE [dbo].[News] (
        [Id] [int] NOT NULL IDENTITY,
        [Head] [nvarchar](100),
        [Description] [nvarchar](max),
        [Url] [nvarchar](max),
        [ImageName] [nvarchar](max),
        [Fecha] [datetime] NOT NULL,
        [Active] [bit] NOT NULL,
        CONSTRAINT [PK_dbo.News] PRIMARY KEY ([Id])
    )
    CREATE TABLE [dbo].[PortalChartColumns] (
        [IdPortalColumn] [int] NOT NULL IDENTITY,
        [IdChart] [int] NOT NULL,
        [MappedFieldName] [nvarchar](max),
        CONSTRAINT [PK_dbo.PortalChartColumns] PRIMARY KEY ([IdPortalColumn], [IdChart])
    )
    CREATE INDEX [IX_IdChart] ON [dbo].[PortalChartColumns]([IdChart])
    CREATE TABLE [dbo].[PortalCharts] (
        [IdChart] [int] NOT NULL IDENTITY,
        [ChartName] [nvarchar](max),
        [IdSAPQuery] [int],
        [NameSAPQuery] [nvarchar](max),
        [IdChartType] [int] NOT NULL,
        [PortalPage] [nvarchar](max),
        [IdCompany] [int] NOT NULL,
        [MenuChart] [bit] NOT NULL,
        CONSTRAINT [PK_dbo.PortalCharts] PRIMARY KEY ([IdChart])
    )
    CREATE INDEX [IX_IdChartType] ON [dbo].[PortalCharts]([IdChartType])
    CREATE INDEX [IX_IdCompany] ON [dbo].[PortalCharts]([IdCompany])
    CREATE TABLE [dbo].[PortalChartParams] (
        [Id] [int] NOT NULL IDENTITY,
        [ParamName] [nvarchar](max),
		[ParamNamePortal] [nvarchar](max),
        [ParamValue] [nvarchar](max),
        [ParamFixedValue] [nvarchar](max),
		[ParamValueType] [nvarchar](20),
        [PortalChart_IdChart] [int],
        CONSTRAINT [PK_dbo.PortalChartParams] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_PortalChart_IdChart] ON [dbo].[PortalChartParams]([PortalChart_IdChart])
    CREATE TABLE [dbo].[PortalChartTypes] (
        [IdChartType] [int] NOT NULL IDENTITY,
        [ChartName] [nvarchar](max),
        [ChartColumnNumber] [int] NOT NULL,
        CONSTRAINT [PK_dbo.PortalChartTypes] PRIMARY KEY ([IdChartType])
    )
    CREATE TABLE [dbo].[PortalChartUserGroups] (
        [Id] [int] NOT NULL IDENTITY,
        [PortalChart_IdChart] [int],
        [UserGroup_Id] [int],
        CONSTRAINT [PK_dbo.PortalChartUserGroups] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_PortalChart_IdChart] ON [dbo].[PortalChartUserGroups]([PortalChart_IdChart])
    CREATE INDEX [IX_UserGroup_Id] ON [dbo].[PortalChartUserGroups]([UserGroup_Id])
	CREATE TABLE [dbo].[PortalChartParamsType] (
        [Id] [int] NOT NULL IDENTITY,
        [ParamTypeKey] [nvarchar](20) NOT NULL,
        [ParamTypeDesc] [nvarchar](50),
        CONSTRAINT [PK_dbo.PortalChartParamsType] PRIMARY KEY ([Id], [ParamTypeKey])
    )
    CREATE TABLE [dbo].[PortalChartParamsValueType] (
        [Id] [int] NOT NULL IDENTITY,
        [ParamValueTypeKey] [nvarchar](20) NOT NULL,
        [ParamValueTypeDesc] [nvarchar](50),
        CONSTRAINT [PK_dbo.PortalChartParamsValueType] PRIMARY KEY ([Id], [ParamValueTypeKey])
    )
    CREATE TABLE [dbo].[UserGroups] (
        [Id] [int] NOT NULL IDENTITY,
        [IdCompany] [int] NOT NULL,
        [Name] [nvarchar](100),
        [CSSName] [nvarchar](100),
        [JSName] [varchar](100),
        CONSTRAINT [PK_dbo.UserGroups] PRIMARY KEY ([Id])
    )
    CREATE TABLE [dbo].[PortalTileParams] (
        [Id] [int] NOT NULL IDENTITY,
        [ParamName] [nvarchar](max),
        [ParamValue] [nvarchar](max),
        [ParamFixedValue] [nvarchar](max),
        [PortalTile_IdTile] [int],
        CONSTRAINT [PK_dbo.PortalTileParams] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_PortalTile_IdTile] ON [dbo].[PortalTileParams]([PortalTile_IdTile])
    CREATE TABLE [dbo].[PortalTiles] (
        [IdTile] [int] NOT NULL IDENTITY,
        [TileName] [nvarchar](max),
        [IdSAPQuery] [int],
        [NameSAPQuery] [nvarchar](max),
        [IdTileType] [int] NOT NULL,
        [UrlPage] [nvarchar](max),
        [HtmlText] [nvarchar](max),
        [IdCompany] [int] NOT NULL,
        [VisualOrder] [smallint],
        CONSTRAINT [PK_dbo.PortalTiles] PRIMARY KEY ([IdTile])
    )
    CREATE INDEX [IX_IdTileType] ON [dbo].[PortalTiles]([IdTileType])
    CREATE INDEX [IX_IdCompany] ON [dbo].[PortalTiles]([IdCompany])
    CREATE TABLE [dbo].[PortalTileTypes] (
        [IdTileType] [int] NOT NULL IDENTITY,
        [TypeName] [nvarchar](max),
        CONSTRAINT [PK_dbo.PortalTileTypes] PRIMARY KEY ([IdTileType])
    )
    CREATE TABLE [dbo].[PortalTileUserGroups] (
        [Id] [int] NOT NULL IDENTITY,
        [UserGroup_Id] [int],
        [PortalTile_IdTile] [int],
        [UserGroup_Id1] [int],
        CONSTRAINT [PK_dbo.PortalTileUserGroups] PRIMARY KEY ([Id])
    )
    CREATE INDEX [IX_PortalTile_IdTile] ON [dbo].[PortalTileUserGroups]([PortalTile_IdTile])
    CREATE INDEX [IX_UserGroup_Id1] ON [dbo].[PortalTileUserGroups]([UserGroup_Id1])
    CREATE TABLE [dbo].[QRConfigs] (
        [IdQRConfig] [int] NOT NULL IDENTITY,
        [IdCompany] [int] NOT NULL,
        [Separator] [nvarchar](max),
        [ItemCodePosition] [int] NOT NULL,
        [DscriptionPosition] [int],
        [WhsCodePosition] [int],
        [OcrCodePosition] [int],
        [GLAccountPosition] [int],
        [FreeTxtPosition] [int],
        [QuantityPosition] [int],
        [CurrencyPosition] [int],
        [PricePosition] [int],
        [SerialPosition] [int],
        [BatchPosition] [int],
        [UomCodePosition] [int],
        CONSTRAINT [PK_dbo.QRConfigs] PRIMARY KEY ([IdQRConfig])
    )
    CREATE TABLE [dbo].[QueryManagerItems] (
        [IdQuery] [int] NOT NULL IDENTITY,
        [IdCompany] [int] NOT NULL,
        [QueryIdentifier] [nvarchar](max),
        [SelectField] [nvarchar](max),
        [FromField] [nvarchar](max),
        [WhereField] [nvarchar](max),
        [OrderByField] [nvarchar](max),
        [GroupByField] [nvarchar](max),
        [Active] [bit] NOT NULL,
        CONSTRAINT [PK_dbo.QueryManagerItems] PRIMARY KEY ([IdQuery])
    )
    CREATE INDEX [IX_IdCompany] ON [dbo].[QueryManagerItems]([IdCompany])
    CREATE TABLE [dbo].[SAPObjects] (
        [IdObject] [int] NOT NULL IDENTITY,
        [ObjectName] [nvarchar](100),
        [ObjectTable] [nvarchar](50),
        [Visible] [bit] NOT NULL,
        CONSTRAINT [PK_dbo.SAPObjects] PRIMARY KEY ([IdObject])
    )
    CREATE TABLE [dbo].[UserDirectAccesses] (
        [IdUser] [int] NOT NULL,
        [IdCompany] [int] NOT NULL,
        [IdDirectAccess] [int] NOT NULL,
        [IdTile] [int] NOT NULL,
        CONSTRAINT [PK_dbo.UserDirectAccesses] PRIMARY KEY ([IdUser], [IdCompany], [IdDirectAccess], [IdTile])
    )
    CREATE INDEX [IX_IdUser] ON [dbo].[UserDirectAccesses]([IdUser])
    CREATE INDEX [IX_IdCompany] ON [dbo].[UserDirectAccesses]([IdCompany])
--Branches-------------------------------------------------------------------------------------------------------
	CREATE TABLE [dbo].[UserBranches](
	[IdUserBranches] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[IdCompany] [int] NULL,
	[IdBranchSAP] [int] NOT NULL,
	[BranchName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_User_Branches] PRIMARY KEY CLUSTERED 
(
	[IdUserBranches] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
ALTER TABLE [dbo].[UserBranches]  WITH CHECK ADD  CONSTRAINT [FK_UserBranches_CompanyConns] FOREIGN KEY([IdCompany])
REFERENCES [dbo].[CompanyConns] ([IdCompany])
ALTER TABLE [dbo].[UserBranches] CHECK CONSTRAINT [FK_UserBranches_CompanyConns]
ALTER TABLE [dbo].[UserBranches]  WITH CHECK ADD  CONSTRAINT [FK_UserBranches_Users] FOREIGN KEY([IdUser])
REFERENCES [dbo].[Users] ([IdUser])
ALTER TABLE [dbo].[UserBranches] CHECK CONSTRAINT [FK_UserBranches_Users]
--Branches-------------------------------------------------------------------------------------------------------
    CREATE TABLE [dbo].[UserUDFs] (
        [IdUDF] [int] NOT NULL IDENTITY,
        [IdUser] [int] NOT NULL,
        [IdCompany] [int] NOT NULL,
        [Document] [nvarchar](100),
        [UDFName] [nvarchar](100),
        [UDFDesc] [nvarchar](100),
        [FieldID] [smallint] NOT NULL,
        [TypeID] [nvarchar](1),
        [RTable] [nvarchar](20),
        CONSTRAINT [PK_dbo.UserUDFs] PRIMARY KEY ([IdUDF])
    )
    CREATE INDEX [IX_IdUser] ON [dbo].[UserUDFs]([IdUser])
    CREATE INDEX [IX_IdCompany] ON [dbo].[UserUDFs]([IdCompany])
    CREATE TABLE [dbo].[WarehousePortals] (
        [IdCompany] [int] NOT NULL,
        [WhsCode] [nvarchar](128) NOT NULL,
        CONSTRAINT [PK_dbo.WarehousePortals] PRIMARY KEY ([IdCompany], [WhsCode])
    )
    CREATE INDEX [IX_IdCompany] ON [dbo].[WarehousePortals]([IdCompany])
    CREATE TABLE [dbo].[PageActions] (
        [IdAction] [int] NOT NULL,
        [IdPage] [int] NOT NULL,
        CONSTRAINT [PK_dbo.PageActions] PRIMARY KEY ([IdAction], [IdPage])
    )
    CREATE INDEX [IX_IdAction] ON [dbo].[PageActions]([IdAction])
    CREATE INDEX [IX_IdPage] ON [dbo].[PageActions]([IdPage])
    ALTER TABLE [dbo].[Pages] ADD CONSTRAINT [FK_dbo.Pages_dbo.AreaKeys_IdAreaKeys] FOREIGN KEY ([IdAreaKeys]) REFERENCES [dbo].[AreaKeys] ([IdAreaKeys]) ON DELETE CASCADE
    ALTER TABLE [dbo].[UserAreas] ADD CONSTRAINT [FK_dbo.UserAreas_dbo.AreaKeys_IdAreaKeys] FOREIGN KEY ([IdAreaKeys]) REFERENCES [dbo].[AreaKeys] ([IdAreaKeys]) ON DELETE CASCADE
    ALTER TABLE [dbo].[UserAreas] ADD CONSTRAINT [FK_dbo.UserAreas_dbo.Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([IdUser]) ON DELETE CASCADE
    ALTER TABLE [dbo].[Users] ADD CONSTRAINT [FK_dbo.Users_dbo.Roles_IdRol] FOREIGN KEY ([IdRol]) REFERENCES [dbo].[Roles] ([IdRol])
    ALTER TABLE [dbo].[Authorizations] ADD CONSTRAINT [FK_dbo.Authorizations_dbo.Users_User_IdUser] FOREIGN KEY ([User_IdUser]) REFERENCES [dbo].[Users] ([IdUser])
    ALTER TABLE [dbo].[QuickOrders] ADD CONSTRAINT [FK_dbo.QuickOrders_dbo.Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([IdUser]) ON DELETE CASCADE
	ALTER TABLE [dbo].[QuickOrders] ADD CONSTRAINT [FK_dbo.QuickOrders_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[RolPageAction] ADD CONSTRAINT [FK_dbo.RolPageAction_dbo.Roles_IdRol] FOREIGN KEY ([IdRol]) REFERENCES [dbo].[Roles] ([IdRol])
    ALTER TABLE [dbo].[RolPageAction] ADD CONSTRAINT [FK_dbo.RolPageAction_dbo.Pages_IdPage] FOREIGN KEY ([IdPage]) REFERENCES [dbo].[Pages] ([IdPage])
    ALTER TABLE [dbo].[RolPageAction] ADD CONSTRAINT [FK_dbo.RolPageAction_dbo.Actions_IdAction] FOREIGN KEY ([IdAction]) REFERENCES [dbo].[Actions] ([IdAction])
    ALTER TABLE [dbo].[UserPageAction] ADD CONSTRAINT [FK_dbo.UserPageAction_dbo.Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([IdUser])
    ALTER TABLE [dbo].[UserPageAction] ADD CONSTRAINT [FK_dbo.UserPageAction_dbo.Pages_IdPage] FOREIGN KEY ([IdPage]) REFERENCES [dbo].[Pages] ([IdPage])
    ALTER TABLE [dbo].[UserPageAction] ADD CONSTRAINT [FK_dbo.UserPageAction_dbo.Actions_IdAction] FOREIGN KEY ([IdAction]) REFERENCES [dbo].[Actions] ([IdAction])
    ALTER TABLE [dbo].[UsersSettings] ADD CONSTRAINT [FK_dbo.UsersSettings_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany])
    ALTER TABLE [dbo].[UsersSettings] ADD CONSTRAINT [FK_dbo.UsersSettings_dbo.Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([IdUser])
    ALTER TABLE [dbo].[CompanyStock] ADD CONSTRAINT [FK_dbo.CompanyStock_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany])
    ALTER TABLE [dbo].[CRPageMaps] ADD CONSTRAINT [FK_dbo.CRPageMaps_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany])
    ALTER TABLE [dbo].[CRPageMaps] ADD CONSTRAINT [FK_dbo.CRPageMaps_dbo.Pages_IdPage] FOREIGN KEY ([IdPage]) REFERENCES [dbo].[Pages] ([IdPage])
    ALTER TABLE [dbo].[BPGroupLines] ADD CONSTRAINT [FK_dbo.BPGroupLines_dbo.BPGroups_BPGroup_Id] FOREIGN KEY ([BPGroup_Id]) REFERENCES [dbo].[BPGroups] ([Id])
    ALTER TABLE [dbo].[BPGroups] ADD CONSTRAINT [FK_dbo.BPGroups_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[Logs] ADD CONSTRAINT [FK_dbo.Logs_dbo.Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([IdUser]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PortalChartColumns] ADD CONSTRAINT [FK_dbo.PortalChartColumns_dbo.PortalCharts_IdChart] FOREIGN KEY ([IdChart]) REFERENCES [dbo].[PortalCharts] ([IdChart]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PortalCharts] ADD CONSTRAINT [FK_dbo.PortalCharts_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PortalCharts] ADD CONSTRAINT [FK_dbo.PortalCharts_dbo.PortalChartTypes_IdChartType] FOREIGN KEY ([IdChartType]) REFERENCES [dbo].[PortalChartTypes] ([IdChartType]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PortalChartParams] ADD CONSTRAINT [FK_dbo.PortalChartParams_dbo.PortalCharts_PortalChart_IdChart] FOREIGN KEY ([PortalChart_IdChart]) REFERENCES [dbo].[PortalCharts] ([IdChart])
    ALTER TABLE [dbo].[PortalChartUserGroups] ADD CONSTRAINT [FK_dbo.PortalChartUserGroups_dbo.UserGroups_UserGroup_Id] FOREIGN KEY ([UserGroup_Id]) REFERENCES [dbo].[UserGroups] ([Id])
    ALTER TABLE [dbo].[PortalChartUserGroups] ADD CONSTRAINT [FK_dbo.PortalChartUserGroups_dbo.PortalCharts_PortalChart_IdChart] FOREIGN KEY ([PortalChart_IdChart]) REFERENCES [dbo].[PortalCharts] ([IdChart])
    ALTER TABLE [dbo].[PortalTileParams] ADD CONSTRAINT [FK_dbo.PortalTileParams_dbo.PortalTiles_PortalTile_IdTile] FOREIGN KEY ([PortalTile_IdTile]) REFERENCES [dbo].[PortalTiles] ([IdTile])
    ALTER TABLE [dbo].[PortalTiles] ADD CONSTRAINT [FK_dbo.PortalTiles_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PortalTiles] ADD CONSTRAINT [FK_dbo.PortalTiles_dbo.PortalTileTypes_IdTileType] FOREIGN KEY ([IdTileType]) REFERENCES [dbo].[PortalTileTypes] ([IdTileType]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PortalTileUserGroups] ADD CONSTRAINT [FK_dbo.PortalTileUserGroups_dbo.PortalTiles_PortalTile_IdTile] FOREIGN KEY ([PortalTile_IdTile]) REFERENCES [dbo].[PortalTiles] ([IdTile])
    ALTER TABLE [dbo].[PortalTileUserGroups] ADD CONSTRAINT [FK_dbo.PortalTileUserGroups_dbo.UserGroups_UserGroup_Id1] FOREIGN KEY ([UserGroup_Id1]) REFERENCES [dbo].[UserGroups] ([Id])
    ALTER TABLE [dbo].[QueryManagerItems] ADD CONSTRAINT [FK_dbo.QueryManagerItems_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[UserDirectAccesses] ADD CONSTRAINT [FK_dbo.UserDirectAccesses_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[UserDirectAccesses] ADD CONSTRAINT [FK_dbo.UserDirectAccesses_dbo.Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([IdUser]) ON DELETE CASCADE
    ALTER TABLE [dbo].[UserUDFs] ADD CONSTRAINT [FK_dbo.UserUDFs_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[UserUDFs] ADD CONSTRAINT [FK_dbo.UserUDFs_dbo.Users_IdUser] FOREIGN KEY ([IdUser]) REFERENCES [dbo].[Users] ([IdUser]) ON DELETE CASCADE
    ALTER TABLE [dbo].[WarehousePortals] ADD CONSTRAINT [FK_dbo.WarehousePortals_dbo.CompanyConns_IdCompany] FOREIGN KEY ([IdCompany]) REFERENCES [dbo].[CompanyConns] ([IdCompany]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PageActions] ADD CONSTRAINT [FK_dbo.PageActions_dbo.Actions_IdAction] FOREIGN KEY ([IdAction]) REFERENCES [dbo].[Actions] ([IdAction]) ON DELETE CASCADE
    ALTER TABLE [dbo].[PageActions] ADD CONSTRAINT [FK_dbo.PageActions_dbo.Pages_IdPage] FOREIGN KEY ([IdPage]) REFERENCES [dbo].[Pages] ([IdPage]) ON DELETE CASCADE

	INSERT INTO [dbo].[PortalChartTypes]([ChartName],[ChartColumnNumber]) VALUES ('ColumnChart',2),('PieChart',2),('Grid',0),('RadarChart',2)
	INSERT INTO [dbo].[PortalTileTypes]([TypeName]) VALUES ('Query'),('HTML'),('News')
	INSERT INTO [dbo].[PortalChartParamsType]([ParamTypeKey],[ParamTypeDesc]) VALUES ('LoggedBP','LBP'),('LoggedSE','LSE'),('LoggedUser','LUser'),('FixedValue','FixedValue'),('EnteredByUser','VEBU')
	INSERT INTO [dbo].[PortalChartParamsValueType]([ParamValueTypeKey],[ParamValueTypeDesc]) VALUES ('string','String'),('int','Integer'),('double','Decimal'),('date','Date')

	INSERT [dbo].[InfoPortals]([vsPortal],[vsDB],[IniDB],[LastUpdateDB])
	VALUES ('1.2.5.9','',(SELECT GETDATE()),(SELECT GETDATE()));