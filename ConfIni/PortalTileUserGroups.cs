//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ARGNS.ConfIni
{
    using System;
    using System.Collections.Generic;
    
    public partial class PortalTileUserGroups
    {
        public int Id { get; set; }
        public Nullable<int> UserGroup_Id { get; set; }
        public Nullable<int> PortalTile_IdTile { get; set; }
        public Nullable<int> UserGroup_Id1 { get; set; }
    
        public virtual PortalTiles PortalTiles { get; set; }
        public virtual UserGroups UserGroups { get; set; }
    }
}
