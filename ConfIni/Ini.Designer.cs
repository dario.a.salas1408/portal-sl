﻿namespace ConfIni
{
    partial class IniForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabIniArg = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnIni = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtCRM = new System.Windows.Forms.TextBox();
            this.ckCRM = new System.Windows.Forms.CheckBox();
            this.txtSERVCALL = new System.Windows.Forms.TextBox();
            this.ckSERVCALL = new System.Windows.Forms.CheckBox();
            this.txtWMS = new System.Windows.Forms.TextBox();
            this.ckWMS = new System.Windows.Forms.CheckBox();
            this.txtMD = new System.Windows.Forms.TextBox();
            this.ckMD = new System.Windows.Forms.CheckBox();
            this.txtSA = new System.Windows.Forms.TextBox();
            this.ckSA = new System.Windows.Forms.CheckBox();
            this.txtPU = new System.Windows.Forms.TextBox();
            this.ckPU = new System.Windows.Forms.CheckBox();
            this.txtPLM = new System.Windows.Forms.TextBox();
            this.ckPLM = new System.Windows.Forms.CheckBox();
            this.txtWP = new System.Windows.Forms.TextBox();
            this.ckWP = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPSW = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnOkFeacture = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtCRMFe = new System.Windows.Forms.TextBox();
            this.txtSERVCALLFe = new System.Windows.Forms.TextBox();
            this.txtWMSFe = new System.Windows.Forms.TextBox();
            this.txtMDFe = new System.Windows.Forms.TextBox();
            this.txtSAFe = new System.Windows.Forms.TextBox();
            this.txtPUFe = new System.Windows.Forms.TextBox();
            this.txtPLMFe = new System.Windows.Forms.TextBox();
            this.txtWPFe = new System.Windows.Forms.TextBox();
            this.ckCRMFe = new System.Windows.Forms.CheckBox();
            this.ckSERVCALLFe = new System.Windows.Forms.CheckBox();
            this.ckWMSFe = new System.Windows.Forms.CheckBox();
            this.ckMDFe = new System.Windows.Forms.CheckBox();
            this.ckSAFe = new System.Windows.Forms.CheckBox();
            this.ckPUFe = new System.Windows.Forms.CheckBox();
            this.ckPLMFe = new System.Windows.Forms.CheckBox();
            this.ckWPFE = new System.Windows.Forms.CheckBox();
            this.txtCRMFeCount = new System.Windows.Forms.TextBox();
            this.txtSERVCALLFeCount = new System.Windows.Forms.TextBox();
            this.txtWMSFeCount = new System.Windows.Forms.TextBox();
            this.txtMDFeCount = new System.Windows.Forms.TextBox();
            this.txtSAFeCount = new System.Windows.Forms.TextBox();
            this.txtPUFeCount = new System.Windows.Forms.TextBox();
            this.txtPLMFeCount = new System.Windows.Forms.TextBox();
            this.txtWPFeCount = new System.Windows.Forms.TextBox();
            this.tabIniArg.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabIniArg
            // 
            this.tabIniArg.Controls.Add(this.tabPage1);
            this.tabIniArg.Controls.Add(this.tabPage2);
            this.tabIniArg.Location = new System.Drawing.Point(12, 12);
            this.tabIniArg.Name = "tabIniArg";
            this.tabIniArg.SelectedIndex = 0;
            this.tabIniArg.Size = new System.Drawing.Size(328, 506);
            this.tabIniArg.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnIni);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(320, 480);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "IniDB";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnIni
            // 
            this.btnIni.Location = new System.Drawing.Point(13, 425);
            this.btnIni.Name = "btnIni";
            this.btnIni.Size = new System.Drawing.Size(295, 46);
            this.btnIni.TabIndex = 8;
            this.btnIni.Text = "Inicializar Base";
            this.btnIni.UseVisualStyleBackColor = true;
            this.btnIni.Click += new System.EventHandler(this.btnIni_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtCRM);
            this.groupBox2.Controls.Add(this.ckCRM);
            this.groupBox2.Controls.Add(this.txtSERVCALL);
            this.groupBox2.Controls.Add(this.ckSERVCALL);
            this.groupBox2.Controls.Add(this.txtWMS);
            this.groupBox2.Controls.Add(this.ckWMS);
            this.groupBox2.Controls.Add(this.txtMD);
            this.groupBox2.Controls.Add(this.ckMD);
            this.groupBox2.Controls.Add(this.txtSA);
            this.groupBox2.Controls.Add(this.ckSA);
            this.groupBox2.Controls.Add(this.txtPU);
            this.groupBox2.Controls.Add(this.ckPU);
            this.groupBox2.Controls.Add(this.txtPLM);
            this.groupBox2.Controls.Add(this.ckPLM);
            this.groupBox2.Controls.Add(this.txtWP);
            this.groupBox2.Controls.Add(this.ckWP);
            this.groupBox2.Location = new System.Drawing.Point(13, 126);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(295, 293);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Productos";
            // 
            // txtCRM
            // 
            this.txtCRM.Location = new System.Drawing.Point(123, 230);
            this.txtCRM.Name = "txtCRM";
            this.txtCRM.Size = new System.Drawing.Size(163, 20);
            this.txtCRM.TabIndex = 16;
            // 
            // ckCRM
            // 
            this.ckCRM.AutoSize = true;
            this.ckCRM.Location = new System.Drawing.Point(9, 231);
            this.ckCRM.Name = "ckCRM";
            this.ckCRM.Size = new System.Drawing.Size(50, 17);
            this.ckCRM.TabIndex = 15;
            this.ckCRM.Text = "CRM";
            this.ckCRM.UseVisualStyleBackColor = true;
            // 
            // txtSERVCALL
            // 
            this.txtSERVCALL.Location = new System.Drawing.Point(123, 198);
            this.txtSERVCALL.Name = "txtSERVCALL";
            this.txtSERVCALL.Size = new System.Drawing.Size(163, 20);
            this.txtSERVCALL.TabIndex = 14;
            // 
            // ckSERVCALL
            // 
            this.ckSERVCALL.AutoSize = true;
            this.ckSERVCALL.Location = new System.Drawing.Point(9, 199);
            this.ckSERVCALL.Name = "ckSERVCALL";
            this.ckSERVCALL.Size = new System.Drawing.Size(108, 17);
            this.ckSERVCALL.TabIndex = 13;
            this.ckSERVCALL.Text = "SERVICE CALLS";
            this.ckSERVCALL.UseVisualStyleBackColor = true;
            // 
            // txtWMS
            // 
            this.txtWMS.Location = new System.Drawing.Point(123, 262);
            this.txtWMS.Name = "txtWMS";
            this.txtWMS.Size = new System.Drawing.Size(163, 20);
            this.txtWMS.TabIndex = 18;
            // 
            // ckWMS
            // 
            this.ckWMS.AutoSize = true;
            this.ckWMS.Location = new System.Drawing.Point(9, 264);
            this.ckWMS.Name = "ckWMS";
            this.ckWMS.Size = new System.Drawing.Size(53, 17);
            this.ckWMS.TabIndex = 17;
            this.ckWMS.Text = "WMS";
            this.ckWMS.UseVisualStyleBackColor = true;
            // 
            // txtMD
            // 
            this.txtMD.Location = new System.Drawing.Point(123, 164);
            this.txtMD.Name = "txtMD";
            this.txtMD.Size = new System.Drawing.Size(163, 20);
            this.txtMD.TabIndex = 12;
            // 
            // ckMD
            // 
            this.ckMD.AutoSize = true;
            this.ckMD.Location = new System.Drawing.Point(9, 166);
            this.ckMD.Name = "ckMD";
            this.ckMD.Size = new System.Drawing.Size(103, 17);
            this.ckMD.TabIndex = 11;
            this.ckMD.Text = "MASTER DATA";
            this.ckMD.UseVisualStyleBackColor = true;
            // 
            // txtSA
            // 
            this.txtSA.Location = new System.Drawing.Point(123, 131);
            this.txtSA.Name = "txtSA";
            this.txtSA.Size = new System.Drawing.Size(163, 20);
            this.txtSA.TabIndex = 10;
            // 
            // ckSA
            // 
            this.ckSA.AutoSize = true;
            this.ckSA.Location = new System.Drawing.Point(9, 133);
            this.ckSA.Name = "ckSA";
            this.ckSA.Size = new System.Drawing.Size(60, 17);
            this.ckSA.TabIndex = 9;
            this.ckSA.Text = "SALES";
            this.ckSA.UseVisualStyleBackColor = true;
            // 
            // txtPU
            // 
            this.txtPU.Location = new System.Drawing.Point(123, 97);
            this.txtPU.Name = "txtPU";
            this.txtPU.Size = new System.Drawing.Size(163, 20);
            this.txtPU.TabIndex = 8;
            // 
            // ckPU
            // 
            this.ckPU.AutoSize = true;
            this.ckPU.Location = new System.Drawing.Point(9, 99);
            this.ckPU.Name = "ckPU";
            this.ckPU.Size = new System.Drawing.Size(85, 17);
            this.ckPU.TabIndex = 7;
            this.ckPU.Text = "PURCHASE";
            this.ckPU.UseVisualStyleBackColor = true;
            // 
            // txtPLM
            // 
            this.txtPLM.Location = new System.Drawing.Point(123, 65);
            this.txtPLM.Name = "txtPLM";
            this.txtPLM.Size = new System.Drawing.Size(163, 20);
            this.txtPLM.TabIndex = 6;
            // 
            // ckPLM
            // 
            this.ckPLM.AutoSize = true;
            this.ckPLM.Location = new System.Drawing.Point(9, 67);
            this.ckPLM.Name = "ckPLM";
            this.ckPLM.Size = new System.Drawing.Size(48, 17);
            this.ckPLM.TabIndex = 5;
            this.ckPLM.Text = "PLM";
            this.ckPLM.UseVisualStyleBackColor = true;
            // 
            // txtWP
            // 
            this.txtWP.Location = new System.Drawing.Point(123, 32);
            this.txtWP.Name = "txtWP";
            this.txtWP.Size = new System.Drawing.Size(163, 20);
            this.txtWP.TabIndex = 4;
            // 
            // ckWP
            // 
            this.ckWP.AutoSize = true;
            this.ckWP.Checked = true;
            this.ckWP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckWP.Enabled = false;
            this.ckWP.Location = new System.Drawing.Point(9, 34);
            this.ckWP.Name = "ckWP";
            this.ckWP.Size = new System.Drawing.Size(94, 17);
            this.ckWP.TabIndex = 0;
            this.ckWP.Text = "WEBPORTAL";
            this.ckWP.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPSW);
            this.groupBox1.Controls.Add(this.txtUser);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(295, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seguridad";
            // 
            // txtPSW
            // 
            this.txtPSW.Location = new System.Drawing.Point(101, 51);
            this.txtPSW.Name = "txtPSW";
            this.txtPSW.Size = new System.Drawing.Size(191, 20);
            this.txtPSW.TabIndex = 3;
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(101, 25);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(191, 20);
            this.txtUser.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Contraseña:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuario:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnOkFeacture);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(320, 480);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Features";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnOkFeacture
            // 
            this.btnOkFeacture.Location = new System.Drawing.Point(10, 310);
            this.btnOkFeacture.Name = "btnOkFeacture";
            this.btnOkFeacture.Size = new System.Drawing.Size(295, 46);
            this.btnOkFeacture.TabIndex = 1;
            this.btnOkFeacture.Text = "Ok";
            this.btnOkFeacture.UseVisualStyleBackColor = true;
            this.btnOkFeacture.Click += new System.EventHandler(this.btnOkFeacture_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtCRMFeCount);
            this.groupBox3.Controls.Add(this.txtSERVCALLFeCount);
            this.groupBox3.Controls.Add(this.txtWMSFeCount);
            this.groupBox3.Controls.Add(this.txtMDFeCount);
            this.groupBox3.Controls.Add(this.txtSAFeCount);
            this.groupBox3.Controls.Add(this.txtPUFeCount);
            this.groupBox3.Controls.Add(this.txtPLMFeCount);
            this.groupBox3.Controls.Add(this.txtWPFeCount);
            this.groupBox3.Controls.Add(this.txtCRMFe);
            this.groupBox3.Controls.Add(this.txtSERVCALLFe);
            this.groupBox3.Controls.Add(this.txtWMSFe);
            this.groupBox3.Controls.Add(this.txtMDFe);
            this.groupBox3.Controls.Add(this.txtSAFe);
            this.groupBox3.Controls.Add(this.txtPUFe);
            this.groupBox3.Controls.Add(this.txtPLMFe);
            this.groupBox3.Controls.Add(this.txtWPFe);
            this.groupBox3.Controls.Add(this.ckCRMFe);
            this.groupBox3.Controls.Add(this.ckSERVCALLFe);
            this.groupBox3.Controls.Add(this.ckWMSFe);
            this.groupBox3.Controls.Add(this.ckMDFe);
            this.groupBox3.Controls.Add(this.ckSAFe);
            this.groupBox3.Controls.Add(this.ckPUFe);
            this.groupBox3.Controls.Add(this.ckPLMFe);
            this.groupBox3.Controls.Add(this.ckWPFE);
            this.groupBox3.Location = new System.Drawing.Point(10, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(295, 293);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Productos";
            // 
            // txtCRMFe
            // 
            this.txtCRMFe.Location = new System.Drawing.Point(222, 222);
            this.txtCRMFe.Name = "txtCRMFe";
            this.txtCRMFe.ReadOnly = true;
            this.txtCRMFe.Size = new System.Drawing.Size(56, 20);
            this.txtCRMFe.TabIndex = 32;
            // 
            // txtSERVCALLFe
            // 
            this.txtSERVCALLFe.Location = new System.Drawing.Point(222, 190);
            this.txtSERVCALLFe.Name = "txtSERVCALLFe";
            this.txtSERVCALLFe.ReadOnly = true;
            this.txtSERVCALLFe.Size = new System.Drawing.Size(56, 20);
            this.txtSERVCALLFe.TabIndex = 31;
            // 
            // txtWMSFe
            // 
            this.txtWMSFe.Location = new System.Drawing.Point(222, 254);
            this.txtWMSFe.Name = "txtWMSFe";
            this.txtWMSFe.ReadOnly = true;
            this.txtWMSFe.Size = new System.Drawing.Size(56, 20);
            this.txtWMSFe.TabIndex = 33;
            // 
            // txtMDFe
            // 
            this.txtMDFe.Location = new System.Drawing.Point(222, 156);
            this.txtMDFe.Name = "txtMDFe";
            this.txtMDFe.ReadOnly = true;
            this.txtMDFe.Size = new System.Drawing.Size(56, 20);
            this.txtMDFe.TabIndex = 30;
            // 
            // txtSAFe
            // 
            this.txtSAFe.Location = new System.Drawing.Point(222, 123);
            this.txtSAFe.Name = "txtSAFe";
            this.txtSAFe.ReadOnly = true;
            this.txtSAFe.Size = new System.Drawing.Size(56, 20);
            this.txtSAFe.TabIndex = 29;
            // 
            // txtPUFe
            // 
            this.txtPUFe.Location = new System.Drawing.Point(222, 89);
            this.txtPUFe.Name = "txtPUFe";
            this.txtPUFe.ReadOnly = true;
            this.txtPUFe.Size = new System.Drawing.Size(56, 20);
            this.txtPUFe.TabIndex = 28;
            // 
            // txtPLMFe
            // 
            this.txtPLMFe.Location = new System.Drawing.Point(222, 57);
            this.txtPLMFe.Name = "txtPLMFe";
            this.txtPLMFe.ReadOnly = true;
            this.txtPLMFe.Size = new System.Drawing.Size(56, 20);
            this.txtPLMFe.TabIndex = 27;
            // 
            // txtWPFe
            // 
            this.txtWPFe.Location = new System.Drawing.Point(222, 24);
            this.txtWPFe.Name = "txtWPFe";
            this.txtWPFe.ReadOnly = true;
            this.txtWPFe.Size = new System.Drawing.Size(56, 20);
            this.txtWPFe.TabIndex = 26;
            // 
            // ckCRMFe
            // 
            this.ckCRMFe.AutoSize = true;
            this.ckCRMFe.Location = new System.Drawing.Point(15, 224);
            this.ckCRMFe.Name = "ckCRMFe";
            this.ckCRMFe.Size = new System.Drawing.Size(50, 17);
            this.ckCRMFe.TabIndex = 24;
            this.ckCRMFe.Text = "CRM";
            this.ckCRMFe.UseVisualStyleBackColor = true;
            // 
            // ckSERVCALLFe
            // 
            this.ckSERVCALLFe.AutoSize = true;
            this.ckSERVCALLFe.Location = new System.Drawing.Point(15, 192);
            this.ckSERVCALLFe.Name = "ckSERVCALLFe";
            this.ckSERVCALLFe.Size = new System.Drawing.Size(108, 17);
            this.ckSERVCALLFe.TabIndex = 23;
            this.ckSERVCALLFe.Text = "SERVICE CALLS";
            this.ckSERVCALLFe.UseVisualStyleBackColor = true;
            // 
            // ckWMSFe
            // 
            this.ckWMSFe.AutoSize = true;
            this.ckWMSFe.Location = new System.Drawing.Point(15, 257);
            this.ckWMSFe.Name = "ckWMSFe";
            this.ckWMSFe.Size = new System.Drawing.Size(53, 17);
            this.ckWMSFe.TabIndex = 25;
            this.ckWMSFe.Text = "WMS";
            this.ckWMSFe.UseVisualStyleBackColor = true;
            // 
            // ckMDFe
            // 
            this.ckMDFe.AutoSize = true;
            this.ckMDFe.Location = new System.Drawing.Point(15, 159);
            this.ckMDFe.Name = "ckMDFe";
            this.ckMDFe.Size = new System.Drawing.Size(103, 17);
            this.ckMDFe.TabIndex = 22;
            this.ckMDFe.Text = "MASTER DATA";
            this.ckMDFe.UseVisualStyleBackColor = true;
            // 
            // ckSAFe
            // 
            this.ckSAFe.AutoSize = true;
            this.ckSAFe.Location = new System.Drawing.Point(15, 126);
            this.ckSAFe.Name = "ckSAFe";
            this.ckSAFe.Size = new System.Drawing.Size(60, 17);
            this.ckSAFe.TabIndex = 21;
            this.ckSAFe.Text = "SALES";
            this.ckSAFe.UseVisualStyleBackColor = true;
            // 
            // ckPUFe
            // 
            this.ckPUFe.AutoSize = true;
            this.ckPUFe.Location = new System.Drawing.Point(15, 92);
            this.ckPUFe.Name = "ckPUFe";
            this.ckPUFe.Size = new System.Drawing.Size(85, 17);
            this.ckPUFe.TabIndex = 20;
            this.ckPUFe.Text = "PURCHASE";
            this.ckPUFe.UseVisualStyleBackColor = true;
            // 
            // ckPLMFe
            // 
            this.ckPLMFe.AutoSize = true;
            this.ckPLMFe.Location = new System.Drawing.Point(15, 60);
            this.ckPLMFe.Name = "ckPLMFe";
            this.ckPLMFe.Size = new System.Drawing.Size(48, 17);
            this.ckPLMFe.TabIndex = 19;
            this.ckPLMFe.Text = "PLM";
            this.ckPLMFe.UseVisualStyleBackColor = true;
            // 
            // ckWPFE
            // 
            this.ckWPFE.AutoSize = true;
            this.ckWPFE.Checked = true;
            this.ckWPFE.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckWPFE.Enabled = false;
            this.ckWPFE.Location = new System.Drawing.Point(15, 27);
            this.ckWPFE.Name = "ckWPFE";
            this.ckWPFE.Size = new System.Drawing.Size(94, 17);
            this.ckWPFE.TabIndex = 18;
            this.ckWPFE.Text = "WEBPORTAL";
            this.ckWPFE.UseVisualStyleBackColor = true;
            // 
            // txtCRMFeCount
            // 
            this.txtCRMFeCount.Location = new System.Drawing.Point(137, 222);
            this.txtCRMFeCount.Name = "txtCRMFeCount";
            this.txtCRMFeCount.ReadOnly = true;
            this.txtCRMFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtCRMFeCount.TabIndex = 40;
            // 
            // txtSERVCALLFeCount
            // 
            this.txtSERVCALLFeCount.Location = new System.Drawing.Point(137, 190);
            this.txtSERVCALLFeCount.Name = "txtSERVCALLFeCount";
            this.txtSERVCALLFeCount.ReadOnly = true;
            this.txtSERVCALLFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtSERVCALLFeCount.TabIndex = 39;
            // 
            // txtWMSFeCount
            // 
            this.txtWMSFeCount.Location = new System.Drawing.Point(137, 254);
            this.txtWMSFeCount.Name = "txtWMSFeCount";
            this.txtWMSFeCount.ReadOnly = true;
            this.txtWMSFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtWMSFeCount.TabIndex = 41;
            // 
            // txtMDFeCount
            // 
            this.txtMDFeCount.Location = new System.Drawing.Point(137, 156);
            this.txtMDFeCount.Name = "txtMDFeCount";
            this.txtMDFeCount.ReadOnly = true;
            this.txtMDFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtMDFeCount.TabIndex = 38;
            // 
            // txtSAFeCount
            // 
            this.txtSAFeCount.Location = new System.Drawing.Point(137, 123);
            this.txtSAFeCount.Name = "txtSAFeCount";
            this.txtSAFeCount.ReadOnly = true;
            this.txtSAFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtSAFeCount.TabIndex = 37;
            // 
            // txtPUFeCount
            // 
            this.txtPUFeCount.Location = new System.Drawing.Point(137, 89);
            this.txtPUFeCount.Name = "txtPUFeCount";
            this.txtPUFeCount.ReadOnly = true;
            this.txtPUFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtPUFeCount.TabIndex = 36;
            // 
            // txtPLMFeCount
            // 
            this.txtPLMFeCount.Location = new System.Drawing.Point(137, 57);
            this.txtPLMFeCount.Name = "txtPLMFeCount";
            this.txtPLMFeCount.ReadOnly = true;
            this.txtPLMFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtPLMFeCount.TabIndex = 35;
            // 
            // txtWPFeCount
            // 
            this.txtWPFeCount.Location = new System.Drawing.Point(137, 24);
            this.txtWPFeCount.Name = "txtWPFeCount";
            this.txtWPFeCount.ReadOnly = true;
            this.txtWPFeCount.Size = new System.Drawing.Size(56, 20);
            this.txtWPFeCount.TabIndex = 34;
            // 
            // IniForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 530);
            this.Controls.Add(this.tabIniArg);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IniForm";
            this.Text = "Inicializador Argentis  Web Portal";
            this.Load += new System.EventHandler(this.IniForm_Load);
            this.tabIniArg.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabIniArg;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnIni;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtCRM;
        private System.Windows.Forms.CheckBox ckCRM;
        private System.Windows.Forms.TextBox txtSERVCALL;
        private System.Windows.Forms.CheckBox ckSERVCALL;
        private System.Windows.Forms.TextBox txtWMS;
        private System.Windows.Forms.CheckBox ckWMS;
        private System.Windows.Forms.TextBox txtMD;
        private System.Windows.Forms.CheckBox ckMD;
        private System.Windows.Forms.TextBox txtSA;
        private System.Windows.Forms.CheckBox ckSA;
        private System.Windows.Forms.TextBox txtPU;
        private System.Windows.Forms.CheckBox ckPU;
        private System.Windows.Forms.TextBox txtPLM;
        private System.Windows.Forms.CheckBox ckPLM;
        private System.Windows.Forms.TextBox txtWP;
        private System.Windows.Forms.CheckBox ckWP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPSW;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOkFeacture;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtCRMFe;
        private System.Windows.Forms.TextBox txtSERVCALLFe;
        private System.Windows.Forms.TextBox txtWMSFe;
        private System.Windows.Forms.TextBox txtMDFe;
        private System.Windows.Forms.TextBox txtSAFe;
        private System.Windows.Forms.TextBox txtPUFe;
        private System.Windows.Forms.TextBox txtPLMFe;
        private System.Windows.Forms.TextBox txtWPFe;
        private System.Windows.Forms.CheckBox ckCRMFe;
        private System.Windows.Forms.CheckBox ckSERVCALLFe;
        private System.Windows.Forms.CheckBox ckWMSFe;
        private System.Windows.Forms.CheckBox ckMDFe;
        private System.Windows.Forms.CheckBox ckSAFe;
        private System.Windows.Forms.CheckBox ckPUFe;
        private System.Windows.Forms.CheckBox ckPLMFe;
        private System.Windows.Forms.CheckBox ckWPFE;
        private System.Windows.Forms.TextBox txtCRMFeCount;
        private System.Windows.Forms.TextBox txtSERVCALLFeCount;
        private System.Windows.Forms.TextBox txtWMSFeCount;
        private System.Windows.Forms.TextBox txtMDFeCount;
        private System.Windows.Forms.TextBox txtSAFeCount;
        private System.Windows.Forms.TextBox txtPUFeCount;
        private System.Windows.Forms.TextBox txtPLMFeCount;
        private System.Windows.Forms.TextBox txtWPFeCount;
    }
}