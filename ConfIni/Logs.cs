//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ARGNS.ConfIni
{
    using System;
    using System.Collections.Generic;
    
    public partial class Logs
    {
        public int IdLog { get; set; }
        public int IdUser { get; set; }
        public string strClave { get; set; }
        public string Tipo { get; set; }
        public string strLog { get; set; }
        public System.DateTime dtLog { get; set; }
    
        public virtual Users Users { get; set; }
    }
}
