//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ARGNS.ConfIni
{
    using System;
    using System.Collections.Generic;
    
    public partial class PortalChartParamsType
    {
        public int Id { get; set; }
        public string ParamTypeKey { get; set; }
        public string ParamTypeDesc { get; set; }
    }
}
