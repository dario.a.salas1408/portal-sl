﻿using ARGNS.ConfIni;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfIni
{
    public partial class IniForm : Form
    {
        WPortal mDataBase;
        Security mSecurity;
        public IniForm()
        {
            InitializeComponent();
        }

        private void btnIni_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Validate())
                {
                    return;
                }

                mSecurity = new Security();
                using (mDataBase = new WPortal())
                {
                    #region CreateBase

                    string sqlConnectionString = mDataBase.Database.Connection.ConnectionString;

                    string mUrlSql = AppDomain.CurrentDomain.BaseDirectory + @"\SqlIni\IniDb.sql";

                    string script = File.ReadAllText(mUrlSql);

                    SqlConnection sqlConnection1 = new SqlConnection(mDataBase.Database.Connection.ConnectionString);
                    SqlCommand cmd = new SqlCommand();
                    SqlDataReader reader;

                    cmd.CommandText = script;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = sqlConnection1;

                    sqlConnection1.Open();

                    reader = cmd.ExecuteReader();

                    sqlConnection1.Close();

                    #endregion

                    #region "Creamos Los Roles"

                    mDataBase.Roles.Add(new Roles { Active = true, Description = "Administrator" });

                    mDataBase.SaveChanges();

                    mDataBase.Roles.Add(new Roles { Active = true, Description = "User" });

                    mDataBase.SaveChanges();

                    mDataBase.Roles.Add(new Roles { Active = true, Description = "Business Partner" });

                    mDataBase.SaveChanges();

                    mDataBase.Roles.Add(new Roles { Active = true, Description = "Sales Employee" });

                    mDataBase.SaveChanges();
                    #endregion

                    #region "Creamos Administrador"

                    string RolAdm = Enums.Rols.Administrator.ToDescriptionString();

                    int IdRol = mDataBase.Roles.Where(c => c.Description.ToUpper() == RolAdm.ToUpper()).Select(c => c.IdRol).FirstOrDefault();

                    mDataBase.Users.Add(new Users { Active = true, IdRol = IdRol, Name = txtUser.Text.ToUpper(), Password = txtPSW.Text, UserIdSAP = 0 });

                    mDataBase.SaveChanges();


                    #endregion

                    #region "Creamos Areas"

                    if (ckWP.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "WEBPORTAL", Action = "", Controller = "", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("WEBPORTAL", Convert.ToInt32(txtWP.Text)), "Argentis"), KeyResource = "Home" });
                        mDataBase.SaveChanges();
                    }

                    if (ckPLM.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "PLM", Action = "Index", Controller = "PLM/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("PLM", Convert.ToInt32(txtPLM.Text)), "Argentis"), KeyResource = "PLM" });
                        mDataBase.SaveChanges();
                    }

                    if (ckSA.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "SALES", Action = "Index", Controller = "Sales/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("SALES", Convert.ToInt32(txtSA.Text)), "Argentis"), KeyResource = "Sales" });
                        mDataBase.SaveChanges();
                    }

                    if (ckPU.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "PURCHASE", Action = "Index", Controller = "Purchase/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("PURCHASE", Convert.ToInt32(txtPU.Text)), "Argentis"), KeyResource = "Purchases" });
                        mDataBase.SaveChanges();
                    }

                    if (ckWMS.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "WMS", Action = "Index", Controller = "", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("WMS", Convert.ToInt32(txtWMS.Text)), "Argentis"), KeyResource = "" });
                        mDataBase.SaveChanges();
                    }

                    if (ckMD.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "MASTERDATA", Action = "Index", Controller = "MasterData/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("MASTERDATA", Convert.ToInt32(txtMD.Text)), "Argentis"), KeyResource = "MasterData" });
                        mDataBase.SaveChanges();
                    }

                    if (ckSERVCALL.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "SERVICECALL", Action = "Index", Controller = "ServiceCall/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("SERVICECALL", Convert.ToInt32(txtSERVCALL.Text)), "Argentis"), KeyResource = "ServiceCall" });
                        mDataBase.SaveChanges();
                    }

                    if (ckCRM.Checked == true)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "CRM", Action = "Index", Controller = "CRM/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("CRM", Convert.ToInt32(txtCRM.Text)), "Argentis"), KeyResource = "CRM" });
                        mDataBase.SaveChanges();
                    }

                    #endregion

                    #region "Asigno al Admin a WebPortal"

                    string mAreaWP = Enums.Areas.WEBPORTAL.ToDescriptionString();

                    int IdAreaWP = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                    int IdUserAdm = mDataBase.Users.FirstOrDefault().IdUser;

                    mDataBase.UserAreas.Add(new UserAreas { IdAreaKeys = IdAreaWP, IdUser = IdUserAdm, KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyUser("WEBPORTAL", IdUserAdm), "Argentis") });

                    mDataBase.SaveChanges();

                    #endregion

                    #region"Add Actions"

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "List", InternalKey = "KeyList", KeyResource = "" });
                    mDataBase.SaveChanges();

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "Add", InternalKey = "KeyAdd", KeyResource = "" });
                    mDataBase.SaveChanges();

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "Delete", InternalKey = "KeyDelete", KeyResource = "" });
                    mDataBase.SaveChanges();

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "Update", InternalKey = "KeyUpdate", KeyResource = "" });
                    mDataBase.SaveChanges();

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "View", InternalKey = "KeyView", KeyResource = "" });
                    mDataBase.SaveChanges();

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "Add to Company", InternalKey = "KeyAsigCompany", KeyResource = "" });
                    mDataBase.SaveChanges();

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "Execute Query", InternalKey = "KeyExecuteQuery", KeyResource = "" });
                    mDataBase.SaveChanges();

                    mDataBase.Actions.Add(new Actions { Active = true, Description = "Show Stock", InternalKey = "StockMKTDoc", KeyResource = "" });
                    mDataBase.SaveChanges();

                    #endregion

                    #region "Add Paginas"
                    int IdAreaKey = 0;
                    int IdActionKey = 0;
                    Pages mPageUpdate = null;
                    Pages mPage = null;
                    DirectAccesses mDirectAccess = null;
                    string mAction = "";

                    if (ckWP.Checked == true)
                    {
                        #region "WEBPORTAL"

                        mAreaWP = Enums.Areas.WEBPORTAL.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPageUpdate = new Pages();
                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Companies", Description = "ABM Companies", IdAreaKeys = IdAreaKey, InternalKey = "WP-ABMCompanies", KeyAdd = "", KeyResource = "Companies", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(false, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Companies"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion


                        mPage = new Pages { Action = "Index", Active = true, Controller = "Users", Description = "ABM Users", IdAreaKeys = IdAreaKey, InternalKey = "WP-ABMUsers", KeyAdd = "", KeyResource = "Users", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(false, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Users"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.AsigCompany.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "Index", Active = true, Controller = "News", Description = "ABM News", IdAreaKeys = IdAreaKey, InternalKey = "WPABMNews", KeyAdd = "", KeyResource = "News", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(false, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page News"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        mPage = new Pages { Action = "Index", Active = true, Controller = "QueryManager", Description = "ABM Query Manager", IdAreaKeys = IdAreaKey, InternalKey = "WP-ABMQuery", KeyAdd = "", KeyResource = "QueryManager", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(false, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Query Manager"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.ExecuteQuery.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "Index", Active = true, Controller = "Chart", Description = "ABM Charts", IdAreaKeys = IdAreaKey, InternalKey = "WP-ABMChart", KeyAdd = "", KeyResource = "Charts", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(false, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Query Manager"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();
                        
                        #endregion

                        #endregion
                    }

                    if (ckPLM.Checked == true)
                    {
                        #region "PLM"

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "PDMGrid", Active = true, Controller = "MyStyles", Description = "My Style Grid", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyStyleGrid", KeyAdd = "", KeyResource = "MyStyleGrid", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Style Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Styles Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/MyStyles/PDMGrid", UrlName = "My Styles Grid Home", TranslationKey = "MSGH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=MyStyleGrid", UrlName = "My Styles - Add Style from Grid", TranslationKey = "MSASFG" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MyStyles", Description = "My Style List", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyStyleList", KeyAdd = "", KeyResource = "MyStyleList" };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Style List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Styles List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/MyStyles", UrlName = "My Styles List Home", TranslationKey = "MSLH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=MyStyleList", UrlName = "My Styles - Add Style from List", TranslationKey = "MSASFL" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "PDMGrid", Active = true, Controller = "PDM", Description = "Style Grid", IdAreaKeys = IdAreaKey, InternalKey = "PLM-StyleGrid", KeyAdd = "", KeyResource = "StyleGrid", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Style Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Styles Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/PDMGrid", UrlName = "Styles Grid Home", TranslationKey = "SGH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=Grid", UrlName = "Add Style from Grid", TranslationKey = "ASFG" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PDM", Description = "Style List", IdAreaKeys = IdAreaKey, InternalKey = "PLM-StylList", KeyAdd = "", KeyResource = "SL" };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Style List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Styles List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM", UrlName = "Styles List Home", TranslationKey = "SLH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=List", UrlName = "Add Style from List", TranslationKey = "ASFL" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "MyCostSheet", Active = true, Controller = "MyStyles", Description = "My Cost Sheet", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyCostSheet", KeyAdd = "", KeyResource = "MyCostSheet", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/MyStyles/MyCostSheet", UrlName = "My Cost Sheet Home", TranslationKey = "MCSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CostSheet/_CostSheet?ActionMyCostSheet=Add&Code=0&ModeCodeDb=0&fromController=MyCostSheet", UrlName = "Add Cost Sheet from My Cost Sheet", TranslationKey = "ACSFMCS" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "CostSheet", Description = "Cost Sheet", IdAreaKeys = IdAreaKey, InternalKey = "PLM-CostSheet", KeyAdd = "", KeyResource = "CostSheet", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CostSheet", UrlName = "Cost Sheet Home", TranslationKey = "CSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CostSheet/_CostSheet?ActionMyCostSheet=Add&Code=0&ModeCodeDb=0&fromController=CostSheet", UrlName = "Add Cost Sheet", TranslationKey = "ACS" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "ColorMaster", Description = "Color Master", IdAreaKeys = IdAreaKey, InternalKey = "PLM-ColorMaster", KeyAdd = "", KeyResource = "ColorMaster", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Color Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Color Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/ColorMaster", UrlName = "Color Master Home", TranslationKey = "CMH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/ColorMaster/ActionColorMaster?pActionColorMaster=Add&pCode=0", UrlName = "Add Color", TranslationKey = "AC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion


                        mPage = new Pages() { Action = "Index", Active = true, Controller = "ScaleMaster", Description = "Scale Master", IdAreaKeys = IdAreaKey, InternalKey = "PLM-ScaleMaster", KeyAdd = "", KeyResource = "ScaleMaster", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Scale Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Scale Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/ScaleMaster", UrlName = "Scale Master Home", TranslationKey = "SMH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "VarMaster", Description = "Var Master", IdAreaKeys = IdAreaKey, InternalKey = "PLM-VarMaster", KeyAdd = "", KeyResource = "VarMaster", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Var Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Var Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/VarMaster", UrlName = "Variable Master Home", TranslationKey = "VMH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Activity", Description = "Activity", IdAreaKeys = IdAreaKey, InternalKey = "PLM-Activity", KeyAdd = "", KeyResource = "Activity", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Activity"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Activity"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity", UrlName = "Activity Home", TranslationKey = "AH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity/ActivityAction?pAction=Add&pCode=0&pFrom=Activities", UrlName = "Add Activity", TranslationKey = "AA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "IndexMyActivity", Active = true, Controller = "Activity", Description = "My Activities", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyActivities", KeyAdd = "", KeyResource = "PLMMyActivities", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Activity"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity/IndexMyActivity", UrlName = "My Activity Home", TranslationKey = "MAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity/ActivityAction?pAction=Add&pCode=0&pFrom=MyActivities", UrlName = "My Activity - Add Activity", TranslationKey = "MYAA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PDM", Description = "PDM", IdAreaKeys = IdAreaKey, InternalKey = "PLM-PDM", KeyAdd = "", KeyResource = "ProductDataManagement", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page PDM"

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "RangePlan", Description = "Range Plan", IdAreaKeys = IdAreaKey, InternalKey = "PLM-RangePlan", KeyAdd = "", KeyResource = "RangePlan", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page RangePlan"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Range Plan"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/RangePlan", UrlName = "Range Plan Home", TranslationKey = "RPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "CriticalPath", Description = "Critical Path", IdAreaKeys = IdAreaKey, InternalKey = "PLM-CPath", KeyAdd = "", KeyResource = "CriticalPath", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Critical Path"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Critical Path"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CriticalPath", UrlName = "Critical Path Home", TranslationKey = "CPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PDC", Description = "Product Data Collection", IdAreaKeys = IdAreaKey, InternalKey = "PLM-PDC", KeyAdd = "", KeyResource = "PDC", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page PDC"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page PDC"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDC", UrlName = "PDC Home", TranslationKey = "PDCH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDC/PDCAction?PDCAction=Add&Code=0", UrlName = "Add PDC", TranslationKey = "APDC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MaterialDetail", Description = "Material Detail", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MaterialDetail", KeyAdd = "", KeyResource = "MatDet", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Material Detail"

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Shipment", Description = "Shipment", IdAreaKeys = IdAreaKey, InternalKey = "PLM-Shipment", KeyAdd = "", KeyResource = "Shipment", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment", UrlName = "Shipment Home", TranslationKey = "SH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment/_Shipment?ActionShipment=Add&pShipmentID=0&pFromController=Shipment", UrlName = "Add Shipment", TranslationKey = "AS" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "MyShipmentIndex", Active = true, Controller = "Shipment", Description = "My Shipment", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyShipment", KeyAdd = "", KeyResource = "MyShipment", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page My Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment/MyShipmentIndex", UrlName = "My Shipment Home", TranslationKey = "MSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment/_Shipment?ActionShipment=Add&pShipmentID=0&pFromController=MyShipment", UrlName = "My Shipment - Add Shipment", TranslationKey = "MSAD" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "TechPackAction", Active = true, Controller = "TechPack", Description = "Tech Pack", IdAreaKeys = IdAreaKey, InternalKey = "PLM-TechPack", KeyAdd = "", KeyResource = "PLMTechPack", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Tech Pack"

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #endregion
                    }

                    if (ckPU.Checked == true)
                    {
                        #region"PURCHASE"

                        mAreaWP = Enums.Areas.Purchase.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();


                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseOrder", Description = "Purchase Order", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseOrder", KeyAdd = "", KeyResource = "POS", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseOrder", UrlName = "Purchase Order Home", TranslationKey = "POH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=PurchaseOrder", UrlName = "Add Purchase Order", TranslationKey = "APO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseQuotation", Description = "Purchase Quotation", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseQuotation", KeyAdd = "", KeyResource = "PQS", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseQuotation", UrlName = "Purchase Quotation Home", TranslationKey = "PQH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=PurchaseQuotation", UrlName = "Add Purchase Quotation", TranslationKey = "APQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseRequest", Description = "Purchase Request", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseRequest", KeyAdd = "", KeyResource = "PRS", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Request"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Request"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseRequest", UrlName = "Purchase Request Home", TranslationKey = "PRH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseRequest/ActionPurchaseRequest?ActionPurchaseRequest=Add&IdPR=0&fromController=PurchaseRequest", UrlName = "Add Purchase Request", TranslationKey = "APR" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Draft", Description = "My Documents", IdAreaKeys = IdAreaKey, InternalKey = "PU-Draft", KeyAdd = "", KeyResource = "MD", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/Draft", UrlName = "My Documents Home", TranslationKey = "MDPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Purchase Order", TranslationKey = "MDAPO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Purchase Quotation", TranslationKey = "MDAPQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseRequest/ActionPurchaseRequest?ActionPurchaseRequest=Add&IdPR=0&fromController=MyDocuments", UrlName = "My Documents - Add Purchase Request", TranslationKey = "MDAPR" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Approval", Description = "Purchase Approval", IdAreaKeys = IdAreaKey, InternalKey = "PU-Approval", KeyAdd = "", KeyResource = "MAS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/Approval", UrlName = "Purchase Approval Home", TranslationKey = "PAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseInvoice", Description = "Purchase Invoice", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseInvoice", KeyAdd = "", KeyResource = "PIS", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseInvoice", UrlName = "Purchase Invoice Home", TranslationKey = "PIH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        #endregion
                    }

                    if (ckSA.Checked == true)
                    {
                        #region"SALES"

                        mAreaWP = Enums.Areas.Sales.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesOrder", Description = "Sales Order", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesOrder", KeyAdd = "", KeyResource = "SOS", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.StockMKTDoc.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Sales Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesOrder", UrlName = "Sales Order Home", TranslationKey = "SOH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=SalesOrder", UrlName = "Add Sales Order", TranslationKey = "ASO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesQuotation", Description = "Sales Quotation", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesQuotation", KeyAdd = "", KeyResource = "SQS", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesQuotation", UrlName = "Sales Quotation Home", TranslationKey = "SQH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=SalesQuotation", UrlName = "Add Sales Quotation", TranslationKey = "ASQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesDraft", Description = "My Documents", IdAreaKeys = IdAreaKey, InternalKey = "SO-Draft", KeyAdd = "", KeyResource = "MD", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesDraft", UrlName = "My Documents Home", TranslationKey = "MDSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Sales Order", TranslationKey = "MDASO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Sales Quotation", TranslationKey = "MDASQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesApproval", Description = "Sales Approval", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesApproval", KeyAdd = "", KeyResource = "SA", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesApproval", UrlName = "Sales Approval Home", TranslationKey = "SAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesInvoice", Description = "Sales Invoice", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesInvoice", KeyAdd = "", KeyResource = "SIS", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesInvoice", UrlName = "Sales Invoice Home", TranslationKey = "SIH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion


                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Opportunities", Description = "Opportunities", IdAreaKeys = IdAreaKey, InternalKey = "SO-Opportunities", KeyAdd = "", KeyResource = "Oportunity", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Opportunities"


                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Opportunities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/Opportunities", UrlName = "Opportunities Home", TranslationKey = "OH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/Opportunities/Action?pAction=Add&pCode=0", UrlName = "Add Opportunities", TranslationKey = "AO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MyOpportunities", Description = "My Opportunities", IdAreaKeys = IdAreaKey, InternalKey = "SO-MyOpportunities", KeyAdd = "", KeyResource = "MyOportunity", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Opportunities"


                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Opportunities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/MyOpportunities", UrlName = "My Opportunities Home", TranslationKey = "MOH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/Opportunities/Action?pAction=Add&pCode=0&pFrom=MyOpportunity", UrlName = "My Opportunities - Add Opportunities", TranslationKey = "MOAO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "QuickOrder", Description = "Quick Order", IdAreaKeys = IdAreaKey, InternalKey = "SO-QuickOrder", KeyAdd = "", KeyResource = "SOQuickOrder", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Quick Order"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Quick Order"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/QuickOrder", UrlName = "Quick Order Home", TranslationKey = "QOH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion


                        #endregion
                    }

                    if (ckMD.Checked == true)
                    {
                        #region "MASTER DATA"

                        mAreaWP = Enums.Areas.MasterData.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "BusinessPartner", Description = "ABM BP", IdAreaKeys = IdAreaKey, InternalKey = "MD-BP", KeyAdd = "", KeyResource = "BP", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Bp"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page BP"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/BusinessPartner", UrlName = "BP Home", TranslationKey = "BPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/BusinessPartner/ActionBusinessPartner?ActionBusinessPartner=Add&CardCodeBP=0", UrlName = "Add BP", TranslationKey = "ABP" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MyBusinessPartner", Description = "My Business Partner", IdAreaKeys = IdAreaKey, InternalKey = "MD-MyBP", KeyAdd = "", KeyResource = "MYBP", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My BP"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Purchase Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/MyBusinessPartner", UrlName = "My Business Partner", TranslationKey = "MBP" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/BusinessPartner/ActionBusinessPartner?ActionBusinessPartner=Add&CardCodeBP=0&fromController=MyBusinessPartner", UrlName = "My BP - Add BP", TranslationKey = "MBPABP" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "Index", Active = true, Controller = "CustomerAging", Description = "Customer Aging Report", IdAreaKeys = IdAreaKey, InternalKey = "MD-CustomerAging", KeyAdd = "", KeyResource = "CAR", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Customer Aging Report"

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Customer Aging Report"

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/CustomerAging", UrlName = "Customer Aging Report Home", TranslationKey = "CARH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "Index", Active = true, Controller = "MasterData", Description = "Item Master", IdAreaKeys = IdAreaKey, InternalKey = "MD-ItemMaster", KeyAdd = "", KeyResource = "IM", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Item Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Item Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/MasterData", UrlName = "Item Master Home", TranslationKey = "IMH" };


                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey,
                            Page = mPage.IdPage, Url = "MasterData/InventoryStatus",
                            UrlName = "Inventory Status", TranslationKey = "ISR" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        #endregion
                    }

                    if (ckSERVCALL.Checked == true)
                    {
                        #region "SERVICE CALL"

                        mAreaWP = Enums.Areas.ServiceCall.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "ServiceCall", Description = "Service Call", IdAreaKeys = IdAreaKey, InternalKey = "ServCall", KeyAdd = "", KeyResource = "ServiceCall", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();


                        #region "Acciones Page Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/ServiceCall", UrlName = "Service Calls Home", TranslationKey = "SCH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/ServiceCall/Action?pAction=Add&pCode=0", UrlName = "Add Service Calls", TranslationKey = "ASC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.ServiceCall.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "MyServiceCall", Description = "My Service Call", IdAreaKeys = IdAreaKey, InternalKey = "MyServCall", KeyAdd = "", KeyResource = "MyServiceCall", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();


                        #region "Acciones Page My Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/MyServiceCall", UrlName = "My Service Calls Home", TranslationKey = "MSCH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/ServiceCall/Action?pAction=Add&pCode=0&pFrom=MyService", UrlName = "My Service Calls - Add Service Call", TranslationKey = "MSCASC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion


                        #endregion
                    }

                    if (ckCRM.Checked == true)
                    {
                        #region "CRM"

                        mAreaWP = Enums.Areas.CRM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "CRM", Description = "CRMActivity", IdAreaKeys = IdAreaKey, InternalKey = "CRM-Activities", KeyAdd = "", KeyResource = "CRMActivities", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page CRMActivities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page CRM Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMActivity", UrlName = "CRM Activities Home", TranslationKey = "CRMAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMActivity/ActivityAction?pAction=Add&pCode=0&pFrom=List", UrlName = "CRM Activities - Add Activity", TranslationKey = "CRMAA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.CRM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "CRMActivity", Description = "My Activities", IdAreaKeys = IdAreaKey, InternalKey = "CRM-MyActivities", KeyAdd = "", KeyResource = "CRMMyActivities", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();


                        #region "Acciones Page CRM My Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page CRM My Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMMyActivity", UrlName = "CRM My Activities Home", TranslationKey = "CRMMAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMActivity/ActivityAction?pAction=Add&pCode=0&pFrom=MyList", UrlName = "CRM My Activities - Add Activity", TranslationKey = "CRMMAAA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion


                        #endregion
                    }


                    #region"WMS"
                    #endregion

                    #endregion

                    #region "Asigno todos los Permisos al Admin"
                    string mRol = Enums.Rols.Administrator.ToDescriptionString().ToUpper();
                    int IdRolAdm = mDataBase.Roles.Where(c => c.Description.ToUpper() == mRol).FirstOrDefault().IdRol;
                    IdUserAdm = mDataBase.Users.FirstOrDefault().IdUser;
                    List<Pages> List = mDataBase.Pages.ToList();

                    foreach (Pages item in List)
                    {
                        foreach (Actions itemAc in item.Actions)
                        {
                            mDataBase.RolPageAction.Add(new RolPageAction { IdRol = IdRolAdm, IdAction = itemAc.IdAction, IdPage = item.IdPage });
                            mDataBase.SaveChanges();

                            mDataBase.UserPageAction.Add(new UserPageAction { IdUser = IdUserAdm, IdAction = itemAc.IdAction, IdPage = item.IdPage });
                            mDataBase.SaveChanges();
                        }
                    }

                    #endregion

                    #region "Creamos Los SAPObject"

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Order", ObjectTable = "OPOR", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Order Lines", ObjectTable = "POR1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Quotation", ObjectTable = "OPQT", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Quotation Lines", ObjectTable = "PQT1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Request", ObjectTable = "OPRQ", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Request Lines", ObjectTable = "PRQ1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Invoice", ObjectTable = "OPCH", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Purchase Invoice Lines", ObjectTable = "PCH1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Sales Order", ObjectTable = "ORDR", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Sales Order Lines", ObjectTable = "RDR1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Sales Quotation", ObjectTable = "OQUT", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Sales Quotation Lines", ObjectTable = "QUT1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Sales Invoice", ObjectTable = "OINV", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Sales Invoice Lines", ObjectTable = "INV1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "ARGNS Model", ObjectTable = "@ARGNS_MODEL", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Item Master", ObjectTable = "OITM", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Fabric", ObjectTable = "@ARGNS_MD_FABRIC", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Accessories", ObjectTable = "@ARGNS_MD_ACCESS", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Care Instructions", ObjectTable = "@ARGNS_MD_CARE_INST", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Labelling", ObjectTable = "@ARGNS_MD_LABELLING", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Packaging", ObjectTable = "@ARGNS_MD_PACKAGING", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Footware Material", ObjectTable = "@ARGNS_MD_FOOTWEAR", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Footware Detail", ObjectTable = "@ARGNS_MD_FDETAILS", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Footware Packaging", ObjectTable = "@ARGNS_MD_FPACKAGING", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Footware Pictogram", ObjectTable = "@ARGNS_MD_FPICTOGRAM", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Footware Size Range", ObjectTable = "@ARGNS_MD_FSRANGE", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Material Detail Footware Accessories", ObjectTable = "@ARGNS_MD_FACCESS", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Business Partner - Addresses", ObjectTable = "CRD1", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Serials", ObjectTable = "OSRN", Visible = true });

                    mDataBase.SaveChanges();

                    mDataBase.SAPObjects.Add(new SAPObjects { ObjectName = "Business Partner", ObjectTable = "OCRD", Visible = true });

                    mDataBase.SaveChanges();
                    #endregion
                }
                MessageBox.Show("La Base se inicio correctamente!!!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

                LoadForm();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Surgio Un error verifique los Datos!!!! - "  + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool Validate()
        {
            try
            {
                if (txtPSW.Text == "" || txtUser.Text == "")
                {
                    MessageBox.Show("Ingrese el Usuario y assword.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

                if (ckWP.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtWP.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }

                }

                if (ckPLM.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtPLM.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckSA.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtSA.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckPU.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtPU.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckWMS.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtWMS.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckCRM.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtCRM.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Surgio Un error verifique los Datos!!!!  - " + ex.Message.ToString() ,
					"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void IniForm_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void btnOkFeacture_Click(object sender, EventArgs e)
        {
            try
            {
                int IdAreaKey = 0;
                int IdActionKey = 0;
                Pages mPageUpdate = null;
                Pages mPage = null;
                string mAction = "";
                string mAreaWP = string.Empty;
                bool mExistArea = false;
                AreaKeys mAreaKeys = null;
                DirectAccesses mDirectAccess = null;

                using (mDataBase = new WPortal())
                {
                    #region "PLM"

                    mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                    mAreaKeys = mDataBase.AreaKeys.Where(c => c.Area == mAreaWP).FirstOrDefault();

                    mExistArea = (mAreaKeys != null ? true : false);

                    if (ckPLMFe.Checked == true && mExistArea == false)
                    {

                        if (txtPLMFeCount.Text == "")
                        {

                        }

                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "PLM", Action = "Index", Controller = "PLM/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("PLM", Convert.ToInt32(txtPLMFeCount.Text)), "Argentis"), KeyResource = "PLM" });
                        mDataBase.SaveChanges();

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "PDMGrid", Active = true, Controller = "MyStyles", Description = "My Style Grid", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyStyleGrid", KeyAdd = "", KeyResource = "MyStyleGrid", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Style Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Styles Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/MyStyles/PDMGrid", UrlName = "My Styles Grid Home", TranslationKey = "MSGH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=MyStyleGrid", UrlName = "My Styles - Add Style from Grid", TranslationKey = "MSASFG" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MyStyles", Description = "My Style List", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyStyleList", KeyAdd = "", KeyResource = "MyStyleList" };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Style List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Styles List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/MyStyles", UrlName = "My Styles List Home", TranslationKey = "MSLH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=MyStyleList", UrlName = "My Styles - Add Style from List", TranslationKey = "MSASFL" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "PDMGrid", Active = true, Controller = "PDM", Description = "Style Grid", IdAreaKeys = IdAreaKey, InternalKey = "PLM-StyleGrid", KeyAdd = "", KeyResource = "StyleGrid", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Style Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Styles Grid"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/PDMGrid", UrlName = "Styles Grid Home", TranslationKey = "SGH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=Grid", UrlName = "Add Style from Grid", TranslationKey = "ASFG" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PDM", Description = "Style List", IdAreaKeys = IdAreaKey, InternalKey = "PLM-StylList", KeyAdd = "", KeyResource = "SL" };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Style List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Styles List"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM", UrlName = "Styles List Home", TranslationKey = "SLH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDM/ActionPDM?ActionPDM=AddModel&ModelCode=0&pU_ModCode=0&From=List", UrlName = "Add Style from List", TranslationKey = "ASFL" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "MyCostSheet", Active = true, Controller = "MyStyles", Description = "My Cost Sheet", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyCostSheet", KeyAdd = "", KeyResource = "MyCostSheet", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/MyStyles/MyCostSheet", UrlName = "My Cost Sheet Home", TranslationKey = "MCSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CostSheet/_CostSheet?ActionMyCostSheet=Add&Code=0&ModeCodeDb=0&fromController=MyCostSheet", UrlName = "Add Cost Sheet from My Cost Sheet", TranslationKey = "ACSFMCS" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.PLM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "CostSheet", Description = "Cost Sheet", IdAreaKeys = IdAreaKey, InternalKey = "PLM-CostSheet", KeyAdd = "", KeyResource = "CostSheet", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Cost Sheet"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CostSheet", UrlName = "Cost Sheet Home", TranslationKey = "CSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CostSheet/_CostSheet?ActionMyCostSheet=Add&Code=0&ModeCodeDb=0&fromController=CostSheet", UrlName = "Add Cost Sheet", TranslationKey = "ACS" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "ColorMaster", Description = "Color Master", IdAreaKeys = IdAreaKey, InternalKey = "PLM-ColorMaster", KeyAdd = "", KeyResource = "ColorMaster", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Color Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Color Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/ColorMaster", UrlName = "Color Master Home", TranslationKey = "CMH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/ColorMaster/ActionColorMaster?pActionColorMaster=Add&pCode=0", UrlName = "Add Color", TranslationKey = "AC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "ScaleMaster", Description = "Scale Master", IdAreaKeys = IdAreaKey, InternalKey = "PLM-ScaleMaster", KeyAdd = "", KeyResource = "ScaleMaster", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Scale Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Scale Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/ScaleMaster", UrlName = "Scale Master Home", TranslationKey = "SMH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "VarMaster", Description = "Var Master", IdAreaKeys = IdAreaKey, InternalKey = "PLM-VarMaster", KeyAdd = "", KeyResource = "VarMaster", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Var Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Var Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/VarMaster", UrlName = "Variable Master Home", TranslationKey = "VMH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Activity", Description = "Activity", IdAreaKeys = IdAreaKey, InternalKey = "PLM-Activity", KeyAdd = "", KeyResource = "Activity", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Activity"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Activity"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity", UrlName = "Activity Home", TranslationKey = "AH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity/ActivityAction?pAction=Add&pCode=0&pFrom=Activities", UrlName = "Add Activity", TranslationKey = "AA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "IndexMyActivity", Active = true, Controller = "Activity", Description = "My Activities", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyActivities", KeyAdd = "", KeyResource = "PLMMyActivities", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Activity"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity/IndexMyActivity", UrlName = "My Activity Home", TranslationKey = "MAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Activity/ActivityAction?pAction=Add&pCode=0&pFrom=MyActivities", UrlName = "My Activity - Add Activity", TranslationKey = "MYAA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PDM", Description = "PDM", IdAreaKeys = IdAreaKey, InternalKey = "PLM-PDM", KeyAdd = "", KeyResource = "ProductDataManagement", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page PDM"

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "RangePlan", Description = "Range Plan", IdAreaKeys = IdAreaKey, InternalKey = "PLM-RangePlan", KeyAdd = "", KeyResource = "RangePlan", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page RangePlan"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Range Plan"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/RangePlan", UrlName = "Range Plan Home", TranslationKey = "RPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "CriticalPath", Description = "Critical Path", IdAreaKeys = IdAreaKey, InternalKey = "PLM-CPath", KeyAdd = "", KeyResource = "CriticalPath", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Critical Path"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Critical Path"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/CriticalPath", UrlName = "Critical Path Home", TranslationKey = "CPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PDC", Description = "Product Data Collection", IdAreaKeys = IdAreaKey, InternalKey = "PLM-PDC", KeyAdd = "", KeyResource = "PDC", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page PDC"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page PDC"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDC", UrlName = "PDC Home", TranslationKey = "PDCH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/PDC/PDCAction?PDCAction=Add&Code=0", UrlName = "Add PDC", TranslationKey = "APDC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MaterialDetail", Description = "Material Detail", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MaterialDetail", KeyAdd = "", KeyResource = "MatDet", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Material Detail"

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Shipment", Description = "Shipment", IdAreaKeys = IdAreaKey, InternalKey = "PLM-Shipment", KeyAdd = "", KeyResource = "Shipment", CR = true };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment", UrlName = "Shipment Home", TranslationKey = "SH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment/_Shipment?ActionShipment=Add&pShipmentID=0&pFromController=Shipment", UrlName = "Add Shipment", TranslationKey = "AS" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "MyShipmentIndex", Active = true, Controller = "Shipment", Description = "My Shipment", IdAreaKeys = IdAreaKey, InternalKey = "PLM-MyShipment", KeyAdd = "", KeyResource = "MyShipment", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page My Shipment"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment/MyShipmentIndex", UrlName = "My Shipment Home", TranslationKey = "MSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "PLM/Shipment/_Shipment?ActionShipment=Add&pShipmentID=0&pFromController=MyShipment", UrlName = "My Shipment - Add Shipment", TranslationKey = "MSAD" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "TechPackAction", Active = true, Controller = "TechPack", Description = "Tech Pack", IdAreaKeys = IdAreaKey, InternalKey = "PLM-TechPack", KeyAdd = "", KeyResource = "PLMTechPack", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Tech Pack"

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion
                    }
                    else
                    {
                        if (ckPLMFe.Checked == false && mExistArea == true)
                        {
                            List<DirectAccesses> mDirectAccessesList = mDataBase.DirectAccesses.Where(c => c.Area == mAreaKeys.IdAreaKeys).ToList();
                            List<int> mDirectAccessesIdList = mDirectAccessesList.Select(j => j.IdDirectAccess).ToList();
                            List<UserDirectAccesses> mUserDirectAccessesList = mDataBase.UserDirectAccesses.Where(c => mDirectAccessesIdList.Contains(c.IdDirectAccess)).ToList();
                            mDataBase.UserDirectAccesses.RemoveRange(mUserDirectAccessesList);
                            mDataBase.SaveChanges();
                            mDataBase.DirectAccesses.RemoveRange(mDirectAccessesList);
                            mDataBase.SaveChanges();

                            ICollection<UserAreas> cUserAreas = mDataBase.UserAreas.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.UserAreas.RemoveRange(cUserAreas);
                            mDataBase.SaveChanges();

                            foreach (Pages itemPage in mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList())
                            {

                                ICollection<UserPageAction> cUserPageAction = mDataBase.UserPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.UserPageAction.RemoveRange(cUserPageAction);
                                mDataBase.SaveChanges();

                                ICollection<RolPageAction> cRolPageAction = mDataBase.RolPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.RolPageAction.RemoveRange(cRolPageAction);
                                mDataBase.SaveChanges();

                            }

                            ICollection<Pages> cPages = mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.Pages.RemoveRange(cPages);
                            mDataBase.SaveChanges();

                            mDataBase.AreaKeys.Remove(mAreaKeys);
                            mDataBase.SaveChanges();

                        }
                    }

                    #endregion

                    #region"PURCHASE"

                    mAreaWP = Enums.Areas.Purchase.ToDescriptionString();

                    mAreaKeys = mDataBase.AreaKeys.Where(c => c.Area == mAreaWP).FirstOrDefault();

                    mExistArea = (mAreaKeys != null ? true : false);

                    if (ckPUFe.Checked == true && mExistArea == false)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "PURCHASE", Action = "Index", Controller = "Purchase/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("PURCHASE", Convert.ToInt32(txtPUFeCount.Text)), "Argentis"), KeyResource = "Purchases" });
                        mDataBase.SaveChanges();

                        mAreaWP = Enums.Areas.Purchase.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseOrder", Description = "Purchase Order", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseOrder", KeyAdd = "", KeyResource = "POS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseOrder", UrlName = "Purchase Order Home", TranslationKey = "POH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=PurchaseOrder", UrlName = "Add Purchase Order", TranslationKey = "APO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseQuotation", Description = "Purchase Quotation", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseQuotation", KeyAdd = "", KeyResource = "PQS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseQuotation", UrlName = "Purchase Quotation Home", TranslationKey = "PQH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=PurchaseQuotation", UrlName = "Add Purchase Quotation", TranslationKey = "APQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseRequest", Description = "Purchase Request", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseRequest", KeyAdd = "", KeyResource = "PRS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Request"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Request"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseRequest", UrlName = "Purchase Request Home", TranslationKey = "PRH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseRequest/ActionPurchaseRequest?ActionPurchaseRequest=Add&IdPR=0&fromController=PurchaseRequest", UrlName = "Add Purchase Request", TranslationKey = "APR" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Draft", Description = "My Documents", IdAreaKeys = IdAreaKey, InternalKey = "PU-Draft", KeyAdd = "", KeyResource = "MD", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/Draft", UrlName = "My Documents Home", TranslationKey = "MDPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Purchase Order", TranslationKey = "MDAPO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Purchase Quotation", TranslationKey = "MDAPQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseRequest/ActionPurchaseRequest?ActionPurchaseRequest=Add&IdPR=0&fromController=MyDocuments", UrlName = "My Documents - Add Purchase Request", TranslationKey = "MDAPR" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Approval", Description = "Purchase Approval", IdAreaKeys = IdAreaKey, InternalKey = "PU-Approval", KeyAdd = "", KeyResource = "MAS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/Approval", UrlName = "Purchase Approval Home", TranslationKey = "PAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "PurchaseInvoice", Description = "Purchase Invoice", IdAreaKeys = IdAreaKey, InternalKey = "PU-PurchaseInvoice", KeyAdd = "", KeyResource = "PIS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Purchase Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Purchase Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Purchase/PurchaseInvoice", UrlName = "Purchase Invoice Home", TranslationKey = "PIH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                    }
                    else
                    {
                        if (ckPUFe.Checked == false && mExistArea == true)
                        {
                            List<DirectAccesses> mDirectAccessesList = mDataBase.DirectAccesses.Where(c => c.Area == mAreaKeys.IdAreaKeys).ToList();
                            List<int> mDirectAccessesIdList = mDirectAccessesList.Select(j => j.IdDirectAccess).ToList();
                            List<UserDirectAccesses> mUserDirectAccessesList = mDataBase.UserDirectAccesses.Where(c => mDirectAccessesIdList.Contains(c.IdDirectAccess)).ToList();
                            mDataBase.UserDirectAccesses.RemoveRange(mUserDirectAccessesList);
                            mDataBase.SaveChanges();
                            mDataBase.DirectAccesses.RemoveRange(mDirectAccessesList);
                            mDataBase.SaveChanges();

                            ICollection<UserAreas> cUserAreas = mDataBase.UserAreas.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.UserAreas.RemoveRange(cUserAreas);
                            mDataBase.SaveChanges();

                            foreach (Pages itemPage in mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList())
                            {

                                ICollection<UserPageAction> cUserPageAction = mDataBase.UserPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.UserPageAction.RemoveRange(cUserPageAction);
                                mDataBase.SaveChanges();

                                ICollection<RolPageAction> cRolPageAction = mDataBase.RolPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.RolPageAction.RemoveRange(cRolPageAction);
                                mDataBase.SaveChanges();

                            }

                            ICollection<Pages> cPages = mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.Pages.RemoveRange(cPages);
                            mDataBase.SaveChanges();

                            mDataBase.AreaKeys.Remove(mAreaKeys);
                            mDataBase.SaveChanges();

                        }
                    }

                    #endregion

                    #region"SALES"
                    mAreaWP = Enums.Areas.Sales.ToDescriptionString();

                    mAreaKeys = mDataBase.AreaKeys.Where(c => c.Area == mAreaWP).FirstOrDefault();

                    mExistArea = (mAreaKeys != null ? true : false);

                    if (ckSAFe.Checked == true && mExistArea == false)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "SALES", Action = "Index", Controller = "Sales/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("SALES", Convert.ToInt32(txtSAFeCount.Text)), "Argentis"), KeyResource = "Sales" });
                        mDataBase.SaveChanges();

                        mAreaWP = Enums.Areas.Sales.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesOrder", Description = "Sales Order", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesOrder", KeyAdd = "", KeyResource = "SOS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesOrder", UrlName = "Sales Order Home", TranslationKey = "SOH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=SalesOrder", UrlName = "Add Sales Order", TranslationKey = "ASO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion


                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesQuotation", Description = "Sales Quotation", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesQuotation", KeyAdd = "", KeyResource = "SQS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Quotation"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesQuotation", UrlName = "Sales Quotation Home", TranslationKey = "SQH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=SalesQuotation", UrlName = "Add Sales Quotation", TranslationKey = "ASQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesDraft", Description = "My Documents", IdAreaKeys = IdAreaKey, InternalKey = "SO-Draft", KeyAdd = "", KeyResource = "MD", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesDraft", UrlName = "My Documents Home", TranslationKey = "MDSH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Sales Order", TranslationKey = "MDASO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Add&IdPO=0&fromController=MyDocuments", UrlName = "My Documents - Add Sales Quotation", TranslationKey = "MDASQ" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesApproval", Description = "Sales Approval", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesApproval", KeyAdd = "", KeyResource = "SA", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Approval"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesApproval", UrlName = "Sales Approval Home", TranslationKey = "SAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "SalesInvoice", Description = "Sales Invoice", IdAreaKeys = IdAreaKey, InternalKey = "SO-SalesInvoice", KeyAdd = "", KeyResource = "SIS", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Sales Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Sales Invoice"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/SalesInvoice", UrlName = "Sales Invoice Home", TranslationKey = "SIH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "Opportunities", Description = "Opportunities", IdAreaKeys = IdAreaKey, InternalKey = "SO-Opportunities", KeyAdd = "", KeyResource = "Oportunity", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Opportunities"


                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Opportunities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/Opportunities", UrlName = "Opportunities Home", TranslationKey = "OH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/Opportunities/Action?pAction=Add&pCode=0", UrlName = "Add Opportunities", TranslationKey = "AO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MyOpportunities", Description = "My Opportunities", IdAreaKeys = IdAreaKey, InternalKey = "SO-MyOpportunities", KeyAdd = "", KeyResource = "MyOportunity", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Opportunities"


                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Opportunities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/MyOpportunities", UrlName = "My Opportunities Home", TranslationKey = "MOH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/Opportunities/Action?pAction=Add&pCode=0&pFrom=MyOpportunity", UrlName = "My Opportunities - Add Opportunities", TranslationKey = "MOAO" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "QuickOrder", Description = "Quick Order", IdAreaKeys = IdAreaKey, InternalKey = "SO-QuickOrder", KeyAdd = "", KeyResource = "SOQuickOrder", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Quick Order"

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Quick Order"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "Sales/QuickOrder", UrlName = "Quick Order Home", TranslationKey = "QOH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion
                    }
                    else
                    {
                        if (ckSAFe.Checked == false && mExistArea == true)
                        {
                            List<DirectAccesses> mDirectAccessesList = mDataBase.DirectAccesses.Where(c => c.Area == mAreaKeys.IdAreaKeys).ToList();
                            List<int> mDirectAccessesIdList = mDirectAccessesList.Select(j => j.IdDirectAccess).ToList();
                            List<UserDirectAccesses> mUserDirectAccessesList = mDataBase.UserDirectAccesses.Where(c => mDirectAccessesIdList.Contains(c.IdDirectAccess)).ToList();
                            mDataBase.UserDirectAccesses.RemoveRange(mUserDirectAccessesList);
                            mDataBase.SaveChanges();
                            mDataBase.DirectAccesses.RemoveRange(mDirectAccessesList);
                            mDataBase.SaveChanges();

                            ICollection<UserAreas> cUserAreas = mDataBase.UserAreas.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.UserAreas.RemoveRange(cUserAreas);
                            mDataBase.SaveChanges();

                            foreach (Pages itemPage in mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList())
                            {

                                ICollection<UserPageAction> cUserPageAction = mDataBase.UserPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.UserPageAction.RemoveRange(cUserPageAction);
                                mDataBase.SaveChanges();

                                ICollection<RolPageAction> cRolPageAction = mDataBase.RolPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.RolPageAction.RemoveRange(cRolPageAction);
                                mDataBase.SaveChanges();

                            }

                            ICollection<Pages> cPages = mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.Pages.RemoveRange(cPages);
                            mDataBase.SaveChanges();

                            mDataBase.AreaKeys.Remove(mAreaKeys);
                            mDataBase.SaveChanges();

                        }
                    }

                    #endregion

                    #region "MASTER DATA"

                    mAreaWP = Enums.Areas.MasterData.ToDescriptionString();

                    mAreaKeys = mDataBase.AreaKeys.Where(c => c.Area == mAreaWP).FirstOrDefault();

                    mExistArea = (mAreaKeys != null ? true : false);

                    if (ckMDFe.Checked == true && mExistArea == false)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "MASTERDATA", Action = "Index", Controller = "MasterData/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("MASTERDATA", Convert.ToInt32(txtMDFeCount.Text)), "Argentis"), KeyResource = "MasterData" });
                        mDataBase.SaveChanges();

                        mAreaWP = Enums.Areas.MasterData.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "BusinessPartner", Description = "ABM BP", IdAreaKeys = IdAreaKey, InternalKey = "MD-BP", KeyAdd = "", KeyResource = "BP", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Bp"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Delete.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page BP"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/BusinessPartner", UrlName = "BP Home", TranslationKey = "BPH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/BusinessPartner/ActionBusinessPartner?ActionBusinessPartner=Add&CardCodeBP=0", UrlName = "Add BP", TranslationKey = "ABP" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages() { Action = "Index", Active = true, Controller = "MyBusinessPartner", Description = "My Business Partner", IdAreaKeys = IdAreaKey, InternalKey = "MD-MyBP", KeyAdd = "", KeyResource = "MYBP", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My BP"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Purchase Draft"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/MyBusinessPartner", UrlName = "My Business Partner", TranslationKey = "MBP" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/BusinessPartner/ActionBusinessPartner?ActionBusinessPartner=Add&CardCodeBP=0&fromController=MyBusinessPartner", UrlName = "My BP - Add BP", TranslationKey = "MBPABP" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "Index", Active = true, Controller = "CustomerAging", Description = "Customer Aging Report", IdAreaKeys = IdAreaKey, InternalKey = "MD-CustomerAging", KeyAdd = "", KeyResource = "CAR", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Customer Aging Report"

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Customer Aging Report"

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/CustomerAging", UrlName = "Customer Aging Report Home", TranslationKey = "CARH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mPage = new Pages { Action = "Index", Active = true, Controller = "MasterData", Description = "Item Master", IdAreaKeys = IdAreaKey, InternalKey = "MD-ItemMaster", KeyAdd = "", KeyResource = "IM", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Item Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        #endregion

                        #region "Direct Access Page Item Master"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "MasterData/MasterData", UrlName = "Item Master Home", TranslationKey = "IMH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion
                    }
                    else
                    {
                        if (ckMDFe.Checked == false && mExistArea == true)
                        {
                            List<DirectAccesses> mDirectAccessesList = mDataBase.DirectAccesses.Where(c => c.Area == mAreaKeys.IdAreaKeys).ToList();
                            List<int> mDirectAccessesIdList = mDirectAccessesList.Select(j => j.IdDirectAccess).ToList();
                            List<UserDirectAccesses> mUserDirectAccessesList = mDataBase.UserDirectAccesses.Where(c => mDirectAccessesIdList.Contains(c.IdDirectAccess)).ToList();
                            mDataBase.UserDirectAccesses.RemoveRange(mUserDirectAccessesList);
                            mDataBase.SaveChanges();
                            mDataBase.DirectAccesses.RemoveRange(mDirectAccessesList);
                            mDataBase.SaveChanges();

                            ICollection<UserAreas> cUserAreas = mDataBase.UserAreas.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.UserAreas.RemoveRange(cUserAreas);
                            mDataBase.SaveChanges();

                            foreach (Pages itemPage in mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList())
                            {

                                ICollection<UserPageAction> cUserPageAction = mDataBase.UserPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.UserPageAction.RemoveRange(cUserPageAction);
                                mDataBase.SaveChanges();

                                ICollection<RolPageAction> cRolPageAction = mDataBase.RolPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.RolPageAction.RemoveRange(cRolPageAction);
                                mDataBase.SaveChanges();

                            }

                            ICollection<Pages> cPages = mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.Pages.RemoveRange(cPages);
                            mDataBase.SaveChanges();

                            mDataBase.AreaKeys.Remove(mAreaKeys);
                            mDataBase.SaveChanges();

                        }
                    }

                    #endregion

                    #region "SERVICE CALL"

                    mAreaWP = Enums.Areas.ServiceCall.ToDescriptionString();

                    mAreaKeys = mDataBase.AreaKeys.Where(c => c.Area == mAreaWP).FirstOrDefault();

                    mExistArea = (mAreaKeys != null ? true : false);

                    if (ckSERVCALLFe.Checked == true && mExistArea == false)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "SERVICECALL", Action = "Index", Controller = "ServiceCall/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("SERVICECALL", Convert.ToInt32(txtSERVCALLFeCount.Text)), "Argentis"), KeyResource = "ServiceCall" });
                        mDataBase.SaveChanges();

                        mAreaWP = Enums.Areas.ServiceCall.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "ServiceCall", Description = "Service Call", IdAreaKeys = IdAreaKey, InternalKey = "ServCall", KeyAdd = "", KeyResource = "ServiceCall", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/ServiceCall", UrlName = "Service Calls Home", TranslationKey = "SCH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/ServiceCall/Action?pAction=Add&pCode=0", UrlName = "Add Service Calls", TranslationKey = "ASC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.ServiceCall.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "MyServiceCall", Description = "My Service Call", IdAreaKeys = IdAreaKey, InternalKey = "MyServCall", KeyAdd = "", KeyResource = "MyServiceCall", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page My Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page My Service Calls"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/MyServiceCall", UrlName = "My Service Calls Home", TranslationKey = "MSCH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "ServiceCall/ServiceCall/Action?pAction=Add&pCode=0&pFrom=MyService", UrlName = "My Service Calls - Add Service Call", TranslationKey = "MSCASC" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion
                    }
                    else
                    {
                        if (ckSERVCALLFe.Checked == false && mExistArea == true)
                        {
                            List<DirectAccesses> mDirectAccessesList = mDataBase.DirectAccesses.Where(c => c.Area == mAreaKeys.IdAreaKeys).ToList();
                            List<int> mDirectAccessesIdList = mDirectAccessesList.Select(j => j.IdDirectAccess).ToList();
                            List<UserDirectAccesses> mUserDirectAccessesList = mDataBase.UserDirectAccesses.Where(c => mDirectAccessesIdList.Contains(c.IdDirectAccess)).ToList();
                            mDataBase.UserDirectAccesses.RemoveRange(mUserDirectAccessesList);
                            mDataBase.SaveChanges();
                            mDataBase.DirectAccesses.RemoveRange(mDirectAccessesList);
                            mDataBase.SaveChanges();

                            ICollection<UserAreas> cUserAreas = mDataBase.UserAreas.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.UserAreas.RemoveRange(cUserAreas);
                            mDataBase.SaveChanges();

                            foreach (Pages itemPage in mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList())
                            {

                                ICollection<UserPageAction> cUserPageAction = mDataBase.UserPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.UserPageAction.RemoveRange(cUserPageAction);
                                mDataBase.SaveChanges();

                                ICollection<RolPageAction> cRolPageAction = mDataBase.RolPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.RolPageAction.RemoveRange(cRolPageAction);
                                mDataBase.SaveChanges();

                            }

                            ICollection<Pages> cPages = mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.Pages.RemoveRange(cPages);
                            mDataBase.SaveChanges();

                            mDataBase.AreaKeys.Remove(mAreaKeys);
                            mDataBase.SaveChanges();

                        }
                    }


                    #endregion

                    #region "CRM"

                    mAreaWP = Enums.Areas.CRM.ToDescriptionString();

                    mAreaKeys = mDataBase.AreaKeys.Where(c => c.Area == mAreaWP).FirstOrDefault();

                    mExistArea = (mAreaKeys != null ? true : false);

                    if (ckCRMFe.Checked == true && mExistArea == false)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "CRM", Action = "Index", Controller = "CRM/Home", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("CRM", Convert.ToInt32(txtCRMFeCount.Text)), "Argentis"), KeyResource = "CRM" });
                        mDataBase.SaveChanges();

                        mAreaWP = Enums.Areas.CRM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "CRM", Description = "CRMActivity", IdAreaKeys = IdAreaKey, InternalKey = "CRM-Activities", KeyAdd = "", KeyResource = "CRMActivities", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page CRMActivities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page CRM Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMActivity", UrlName = "CRM Activities Home", TranslationKey = "CRMAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMActivity/ActivityAction?pAction=Add&pCode=0&pFrom=List", UrlName = "CRM Activities - Add Activity", TranslationKey = "CRMAA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                        mAreaWP = Enums.Areas.CRM.ToDescriptionString();

                        IdAreaKey = mDataBase.AreaKeys.Where(c => c.Area.ToUpper() == mAreaWP.ToUpper()).Select(c => c.IdAreaKeys).FirstOrDefault();

                        mPage = new Pages { Action = "Index", Active = true, Controller = "CRMActivity", Description = "My Activities", IdAreaKeys = IdAreaKey, InternalKey = "CRM-MyActivities", KeyAdd = "", KeyResource = "CRMMyActivities", CR = false };

                        mDataBase.Pages.Add(mPage);

                        mDataBase.SaveChanges();

                        mPageUpdate = mDataBase.Pages.Where(c => c.IdPage == mPage.IdPage).FirstOrDefault();

                        mPageUpdate.KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyPage(true, mPage.IdPage), "Argentis");

                        mDataBase.SaveChanges();

                        #region "Acciones Page CRM My Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Update.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.View.ToDescriptionString().ToUpper();

                        mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).FirstOrDefault().Pages.Add(mPageUpdate);

                        mDataBase.SaveChanges();


                        #endregion

                        #region "Direct Access Page CRM My Activities"

                        mAction = Enums.Actions.List.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMMyActivity", UrlName = "CRM My Activities Home", TranslationKey = "CRMMAH" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        mAction = Enums.Actions.Add.ToDescriptionString().ToUpper();

                        IdActionKey = mDataBase.Actions.Where(c => c.InternalKey.ToUpper() == mAction).Select(c => c.IdAction).FirstOrDefault();

                        mDirectAccess = new DirectAccesses() { Action = IdActionKey, Area = IdAreaKey, Page = mPage.IdPage, Url = "CRM/CRMActivity/ActivityAction?pAction=Add&pCode=0&pFrom=MyList", UrlName = "CRM My Activities - Add Activity", TranslationKey = "CRMMAAA" };

                        mDataBase.DirectAccesses.Add(mDirectAccess);

                        mDataBase.SaveChanges();

                        #endregion

                    }
                    else
                    {
                        if (ckCRMFe.Checked == false && mExistArea == true)
                        {
                            List<DirectAccesses> mDirectAccessesList = mDataBase.DirectAccesses.Where(c => c.Area == mAreaKeys.IdAreaKeys).ToList();
                            List<int> mDirectAccessesIdList = mDirectAccessesList.Select(j => j.IdDirectAccess).ToList();
                            List<UserDirectAccesses> mUserDirectAccessesList = mDataBase.UserDirectAccesses.Where(c => mDirectAccessesIdList.Contains(c.IdDirectAccess)).ToList();
                            mDataBase.UserDirectAccesses.RemoveRange(mUserDirectAccessesList);
                            mDataBase.SaveChanges();
                            mDataBase.DirectAccesses.RemoveRange(mDirectAccessesList);
                            mDataBase.SaveChanges();

                            ICollection<UserAreas> cUserAreas = mDataBase.UserAreas.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.UserAreas.RemoveRange(cUserAreas);
                            mDataBase.SaveChanges();

                            foreach (Pages itemPage in mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList())
                            {

                                ICollection<UserPageAction> cUserPageAction = mDataBase.UserPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.UserPageAction.RemoveRange(cUserPageAction);
                                mDataBase.SaveChanges();

                                ICollection<RolPageAction> cRolPageAction = mDataBase.RolPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.RolPageAction.RemoveRange(cRolPageAction);
                                mDataBase.SaveChanges();

                            }

                            ICollection<Pages> cPages = mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.Pages.RemoveRange(cPages);
                            mDataBase.SaveChanges();

                            mDataBase.AreaKeys.Remove(mAreaKeys);
                            mDataBase.SaveChanges();

                        }
                    }


                    #endregion

                    #region"WMS"
                    mAreaWP = Enums.Areas.WMS.ToDescriptionString();

                    mAreaKeys = mDataBase.AreaKeys.Where(c => c.Area == mAreaWP).FirstOrDefault();

                    mExistArea = (mAreaKeys != null ? true : false);

                    if (ckWMSFe.Checked == true && mExistArea == false)
                    {
                        mDataBase.AreaKeys.Add(new AreaKeys { Area = "WMS", Action = "Index", Controller = "", KeyNumber = mSecurity.Encrypt(mSecurity.GetKey("WMS", Convert.ToInt32(txtWMSFeCount.Text)), "Argentis"), KeyResource = "" });
                        mDataBase.SaveChanges();
                    }
                    else
                    {
                        if (ckWMSFe.Checked == false && mExistArea == true)
                        {

                            ICollection<UserAreas> cUserAreas = mDataBase.UserAreas.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.UserAreas.RemoveRange(cUserAreas);
                            mDataBase.SaveChanges();

                            foreach (Pages itemPage in mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList())
                            {

                                ICollection<UserPageAction> cUserPageAction = mDataBase.UserPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.UserPageAction.RemoveRange(cUserPageAction);
                                mDataBase.SaveChanges();

                                ICollection<RolPageAction> cRolPageAction = mDataBase.RolPageAction.Where(c => c.IdPage == itemPage.IdPage).ToList();
                                mDataBase.RolPageAction.RemoveRange(cRolPageAction);
                                mDataBase.SaveChanges();

                            }

                            ICollection<Pages> cPages = mDataBase.Pages.Where(c => c.IdAreaKeys == mAreaKeys.IdAreaKeys).ToList();
                            mDataBase.Pages.RemoveRange(cPages);
                            mDataBase.SaveChanges();

                            mDataBase.AreaKeys.Remove(mAreaKeys);
                            mDataBase.SaveChanges();

                        }
                    }
                    #endregion

                    MessageBox.Show("La operacion se realizo correctamente!!!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    if (!ckSAFe.Checked)
                    {
                        txtSAFe.Text = "";
                        txtSAFeCount.Text = "";
                    }

                    if (!ckPLMFe.Checked)
                    {
                        txtPLMFe.Text = "";
                        txtPLMFeCount.Text = "";
                    }

                    if (!ckPUFe.Checked)
                    {
                        txtPUFe.Text = "";
                        txtPUFeCount.Text = "";
                    }

                    if (!ckMDFe.Checked)
                    {
                        txtMDFe.Text = "";
                        txtMDFeCount.Text = "";
                    }

                    if (!ckSERVCALLFe.Checked)
                    {
                        txtSERVCALLFe.Text = "";
                        txtSERVCALLFeCount.Text = "";
                    }

                    if (!ckCRMFe.Checked)
                    {
                        txtCRMFe.Text = "";
                        txtCRMFeCount.Text = "";
                    }

                    if (!ckWMSFe.Checked)
                    {
                        txtWMSFe.Text = "";
                        txtWMSFeCount.Text = "";
                    }

                    LoadForm();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Surgio Un error verifique los Datos!!!! - " + 
					ex.Message, "Error", MessageBoxButtons.OK, 
					MessageBoxIcon.Error);
            }
        }

        private void LoadForm()
        {
            tabIniArg.SelectedIndex = 1;

            int mCountPro = 0;

            try
            {

                foreach (TabPage itemTab in tabIniArg.TabPages)
                {
                    if (itemTab.Text == "Features")
                    {
                        itemTab.Enabled = true;
                    }
                }

                mSecurity = new Security();

                using (mDataBase = new WPortal())
                {
                    foreach (AreaKeys item in mDataBase.AreaKeys.ToList())
                    {
                        switch (item.Area)
                        {
                            case "WEBPORTAL":

                                ckWPFE.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtWPFe.Text = mCountPro.ToString();

                                txtWPFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                            case "PLM":
                                ckPLMFe.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtPLMFe.Text = mCountPro.ToString();

                                txtPLMFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                            case "SALES":
                                ckSAFe.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtSAFe.Text = mCountPro.ToString();

                                txtSAFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                            case "PURCHASE":
                                ckPUFe.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtPUFe.Text = mCountPro.ToString();

                                txtPUFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                            case "WMS":
                                ckWMSFe.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtWMSFe.Text = mCountPro.ToString();

                                txtWMSFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                            case "MASTERDATA":
                                ckMDFe.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtMDFe.Text = mCountPro.ToString();

                                txtMDFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                            case "SERVICECALL":
                                ckSERVCALLFe.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtSERVCALLFe.Text = mCountPro.ToString();

                                txtSERVCALLFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                            case "CRM":
                                ckCRMFe.Checked = true;

                                mCountPro = 0;

                                foreach (UserAreas itemUserAreas in mDataBase.UserAreas.Where(c => c.IdAreaKeys == item.IdAreaKeys).ToList())
                                {
                                    mCountPro = mCountPro + mSecurity.GetCountLCAssigned(itemUserAreas.KeyAdd);
                                }

                                txtCRMFe.Text = mCountPro.ToString();

                                txtCRMFeCount.Text = mSecurity.GetCountProductLc(item.KeyNumber).ToString();

                                break;
                        }
                    }

                    foreach (TabPage itemTab in tabIniArg.TabPages)
                    {
                        if (itemTab.Text == "IniDB")
                        {
                            itemTab.Enabled = false;
                        }
                    }

                    if (!ckPLMFe.Checked)
                    {
                        txtPLMFeCount.ReadOnly = false;
                    }

                    if (!ckSAFe.Checked)
                    {
                        txtSAFeCount.ReadOnly = false;
                    }

                    if (!ckPUFe.Checked)
                    {
                        txtPUFeCount.ReadOnly = false;
                    }

                    if (!ckWMSFe.Checked)
                    {
                        txtWMSFeCount.ReadOnly = false;
                    }

                    if (!ckMDFe.Checked)
                    {
                        txtMDFeCount.ReadOnly = false;
                    }

                    if (!ckSERVCALLFe.Checked)
                    {
                        txtSERVCALLFeCount.ReadOnly = false;
                    }

                    if (!ckCRMFe.Checked)
                    {
                        txtCRMFeCount.ReadOnly = false;
                    }

                }
            }
            catch (Exception)
            {
                tabIniArg.SelectedIndex = 0;

                foreach (TabPage itemTab in tabIniArg.TabPages)
                {
                    if (itemTab.Text == "Features")
                    {
                        itemTab.Enabled = false;
                    }
                }

            }
        }

        private bool ValidateFeacture()
        {
            try
            {

                if (ckPLMFe.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtPLMFeCount.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckSAFe.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtSAFeCount.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckPUFe.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtPUFeCount.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckWMSFe.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtWMSFeCount.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                if (ckCRMFe.Checked == true)
                {
                    try
                    {
                        if (Convert.ToInt32(txtCRMFeCount.Text) < 0)
                        {
                            MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Ingrese una cantidad de Licencias Correcta.!!!!", "Validacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Surgio Un error verifique los Datos!!!! - " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }



    }
}
