﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ScaleView
    {
        public ScaleView()
        {
            SizeList = new List<ARGNSSize>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string Canceled { get; set; }
        public string Object { get; set; }
        public Nullable<int> LogInst { get; set; }
        public Nullable<int> UserSign { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<short> CreateTime { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public Nullable<short> UpdateTime { get; set; }
        public string DataSource { get; set; }
        public string U_SclCode { get; set; }
        public string U_SclDesc { get; set; }
        public string U_Active { get; set; }
        public Nullable<short> U_VOrder { get; set; }
        public List<ARGNSSize> SizeList { get; set; }
    }
}
