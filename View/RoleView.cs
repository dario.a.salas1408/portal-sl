﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class RoleView
    {

        public RoleView()
        {
            ListPage = new List<PageView>();
            RolPageActions = new List<RolPageActionView>();
        }

        public int IdRol { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }

        public List<PageView> ListPage { get; set; }

        public List<RolPageActionView> RolPageActions { get; set; }

    }
}
