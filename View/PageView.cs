﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PageView
    {
        public PageView()
        {
            Actions = new List<ActionsView>();
            ActionsAllowed = new List<ActionsView>();
        }

        public int IdPage { get; set; }
        public int IdAreaKeys { get; set; }
        public string Description { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string KeyResource { get; set; }
        public string KeyAdd { get; set; }
        public bool Active { get; set; }
        public string translated { get; set; }
        public string InternalKey { get; set; }
        public List<ActionsView> Actions { get; set; }
        public List<ActionsView> ActionsAllowed { get; set; }

    }
}
