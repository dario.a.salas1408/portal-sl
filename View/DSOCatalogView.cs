﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class DSOCatalogView
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime DueDate { get; set; }
        public string Image { get; set; }
    }
}
