﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class UsersSettingView
    {
        //int Id { get; set; }
        public CompanyView CompanyConn { get; set; }
        public UserView User { get; set; }
        public string IdBp { get; set; }
        public int? IdSE { get; set; }
        public int IdUser { get; set; }
        public int IdCompany { get; set; }
        public string UserNameSAP { get; set; }
        public string UserCodeSAP { get; set; }
        public string DftDistRule { get; set; }
        public string DftWhs { get; set; }
        public string SalesTaxCodeDef { get; set; }
        public string PurchaseTaxCodeDef { get; set; }
        public int? UserGroupId { get; set; }
    }
}
