﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class AlertView
    {
        public int AlertCode { get; set; }

        public short? UserSign { get; set; }

        public DateTime? RecDate { get; set; }

        public string Subject { get; set; }

        public string UserName { get; set; }
        public string UserText { get; set; }
        public string Type { get; set; }

        public List<AlertColumnSAP> AlertColumns { get; set; }

        public List<AlertRowDataSAP> AlertRowData { get; set; }
    }
}
