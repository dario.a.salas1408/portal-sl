﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class POMViewSamples
    {
        public POMViewSamples()
        {
            ListPOM = new List<ARGNSModelPomView>();
        }

        public string Scale { get; set; }
        public string Size { get; set; }
        public List<ARGNSModelPomView> ListPOM { get; set; }
    }
}
