﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalChartColumnView
    {
        public int IdPortalColumn { get; set; }

        public int IdChart { get; set; }

        public string MappedFieldName { get; set; }
    }
}
