﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class VariableView
    {
        public string Code { get; set; }
        public string Name { get; set; }  
        public string U_VarCode { get; set; }
        public string U_VarDesc { get; set; }
        public string U_Active { get; set; }
        public string U_ATGrp { get; set; }
    }
}
