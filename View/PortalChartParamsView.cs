﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class PortalChartParamsView
    {
        public PortalChartParamsView()
        {

        }

        public PortalChartParamsView(List<PortalChartParamsTypeView> pPortalChartParamsTypeList, List<PortalChartParamsValueTypeView> mPortalChartParamsValueTypeList)
        {
            PortalChartParamsTypeList = pPortalChartParamsTypeList;
            PortalChartParamsValueTypeList = mPortalChartParamsValueTypeList;
        }

        public PortalChartView PortalChart { get; set; }
        public CompanyView Company { get; set; }
        public List<AreaKeyView> ListAreas { get; set; }
        public List<UserGroupView> ListUserGroup { get; set; }
        public List<PortalChartUserGroupView> ListPortalChartUserGroup { get; set; }
        public List<PortalChartParamsTypeView> PortalChartParamsTypeList { get; set; } 
        public List<PortalChartParamsValueTypeView> PortalChartParamsValueTypeList { get; set; }
    }
}
