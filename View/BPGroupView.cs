﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class BPGroupView
    {
        public BPGroupView()
        {
            BPGroupLines = new List<BPGroupLineView>();
        }

        public int Id { get; set; }

        public int IdCompany { get; set; }

        public string Name { get; set; }

        public List<BPGroupLineView> BPGroupLines { get; set; }
        
    }
}
