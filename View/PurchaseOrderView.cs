﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class PurchaseOrderView
    {
        public PurchaseOrderView()
        {
            ListItem = new List<ItemMasterView>();
            ListBPVendor = new List<BusinessPartnerView>();
            ListSystemRates = new List<RatesSystem>();
            Lines = new List<PurchaseOrderLineView>();
            ListPQ = new List<PurchaseQuotationView>();
            ModelList = new List<ModelDescView>();
            MatrixModel = new MatrixModelView();
            SAPDocSettings = new DocumentSettingsSAP();
            ListFreight = new List<FreightView>();
            Employee = new EmployeeSAP();
            DistrRuleSAP = new List<DistrRuleSAP>();
            Warehouse = new List<Warehouse>();
            GLAccountSAP = new List<GLAccountSAP>();
            AttachmentList = new List<AttachmentView>();

        }

        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public string DocType { get; set; }
        public System.DateTime DocDate { get; set; }
        public System.DateTime DocDueDate { get; set; }
        public System.DateTime TaxDate { get; set; }
        public System.DateTime? CancelDate { get; set; }
        public System.DateTime? ReqDate { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string Project { get; set; }
        public string RevisionPo { get; set; }
        public string SummryType { get; set; }
        public string NumAtCard { get; set; }
        public int SlpCode { get; set; }
        public int OwnerCode { get; set; }
        public string DocCur { get; set; }
        public decimal DocRate { get; set; }
        public int CntctCode { get; set; }
        public short GroupNum { get; set; }
        public string PeyMethod { get; set; }
        public decimal DiscPrcnt { get; set; }
        public short TrnspCode { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public bool Draft { get; set; }
        public string ObjType { get; set; }
        public string DocStatus { get; set; }
        public string Comments { get; set; }
        public string JrnlMemo { get; set; }
        public string FatherType { get; set; }
        public string ShipToCode { get; set; }
        public string CurSource { get; set; }
        public decimal? TotalExpns { get; set; }
        //Document Lines
        public List<PurchaseOrderLineView> Lines { get; set; }
        public DocumentSAPCombo ListDocumentSAPCombo { get; set; }
        public List<ItemMasterView> ListItem { get; set; }
        public List<BusinessPartnerView> ListBPVendor { get; set; }
        public string LocalCurrency { get; set; }
        public string SystemCurrency { get; set; }
        public string RateCurrency { get; set; }
        public List<RatesSystem> ListSystemRates { get; set; }
        public decimal? DocTotal { get; set; }
        public string Confirmed { get; set; }
        public int? BPLId { get; set; }
        public string BPLName { get; set; }
        public string DescConfirmed
        {
            get
            {
                switch (Confirmed)
                {
                    case "Y":
                        return "Yes";

                    case "N":
                        return "No";

                    default:
                        return Confirmed;

                }
            }
        }
        public string OwnerName { get; set; }
        public CompanyAddress companyAddress { get; set; }
        public DocumentAddress POAddress { get; set; }
        public List<PurchaseQuotationView> ListPQ { get; set; }
        public List<ModelDescView> ModelList { get; set; }
        public MatrixModelView MatrixModel { get; set; }
        public DocumentSettingsSAP SAPDocSettings { get; set; }
        public List<FreightView> ListFreight { get; set; }
        public EmployeeSAP Employee { get; set; }
        public List<DistrRuleSAP> DistrRuleSAP { get; set; }
        public List<Warehouse> Warehouse { get; set; }
        public List<GLAccountSAP> GLAccountSAP { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }
        public string PageKey { get; set; }
        public List<AttachmentView> AttachmentList { get; set; }

        //Added to JSON View
        public string DocDateJSONView
        {
            get { return this.DocDate.ToShortDateString(); }
        }
        public string DocDueDateJSONView
        {
            get { return this.DocDueDate.ToShortDateString(); }
        }

        public OpenQuantityStatus OpenQuantityStatus { get; set; }
    }
}
