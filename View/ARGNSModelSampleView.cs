﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class ARGNSModelSampleView
    {
        public ARGNSModelSampleView()
        {
            ListSampleVal = new List<ARGNS_SampleVal>();
            ListUser = new List<UserSAP>();
            ListPOM = new List<ARGNSModelPomTemplate>();
            ListScale = new List<ARGNSScale>();
            ListStatus = new List<ListItem>() { new ListItem { Value = "O", Text = "Open", Selected = true }, new ListItem { Value = "C", Text = "Closed", Selected = false }, new ListItem { Value = "A", Text = "Cancel", Selected = false } };
            ListSizes = new List<ARGNSSize>();
            ListModelPomTab = new List<ARGNSModelPomView>();
            ListLine = new List<ARGNSModelSampleLine>();
            ListCustomer = new List<BusinessPartnerView>();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ModCode { get; set; }
        public string U_Type { get; set; }
        public string U_SampCode { get; set; }
        public string U_Status { get; set; }
        public string U_SclCode { get; set; }
        public string U_POM { get; set; }
        public DateTime U_Date { get; set; }
        public string U_User { get; set; }
        public DateTime U_ApproDate { get; set; }
        public string U_Active { get; set; }
        public string U_Comments { get; set; }
        public string U_BPCustomer { get; set; }
        public string U_BPSupp { get; set; }
        public string U_Picture { get; set; }
        public List<ARGNS_SampleVal> ListSampleVal { get; set; }
        public List<UserSAP> ListUser { get; set; }
        public List<ARGNSModelPomTemplate> ListPOM { get; set; }
        public List<ARGNSScale> ListScale { get; set; }
        public List<ListItem> ListStatus { get; set; }
        public List<ARGNSSize> ListSizes { get; set; }
        public List<ARGNSModelPomView> ListModelPomTab { get; set; }
        public List<ARGNSModelSampleLine> ListLine { get; set; }
        public List<BusinessPartnerView> ListCustomer { get; set; }

    }
}
