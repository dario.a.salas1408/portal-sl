﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSModelFileView
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string U_ModCode { get; set; }
        public string U_File { get; set; }
        public string U_Path { get; set; }
    }
}
