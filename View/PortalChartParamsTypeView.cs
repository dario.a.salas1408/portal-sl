﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalChartParamsTypeView
    {
        public int Id { get; set; }
        
        public string ParamTypeKey { get; set; }

        public string ParamTypeDesc { get; set; }

        public string ParamTypeDescTranslated { get; set; }

        
    }
}
