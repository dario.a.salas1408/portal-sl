﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSAvailableItemView
    {
        public string U_BaseEntry { get; set; }

        public string U_BaseDocNo { get; set; }

        public short? U_BaseLine { get; set; }

        public string U_ItemCode { get; set; }

        public string U_ItemName { get; set; }

        public decimal? Quantity { get; set; }

        public int LineId { get; set; }
    }

}
