﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Model.Interfaces;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.SAP;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.View;

namespace ARGNS.View
{
    public class BusinessPartnerView : IBusinessPartnerSAP
    {
        public BusinessPartnerView()
        {
            ListContact = new List<ContacPerson>();
            BPCombo = new BusinessPartnerCombo();
            PropertiesList = new List<BPPropertiesSAP>();
            CurrencyList = new List<CurrencySAP>();
            ShipTypeList = new List<ShippingType>();
            PriceList = new List<PriceListSAP>();
            EmployeeList = new List<SalesEmployeeSAP>();
            CountryList = new List<CountrySAP>();
            CountyList = new List<StateSAP>();
            PaymentMethodList = new List<PaymentMethod>();
            MappedUdf = new List<UDF_ARGNS>();
        }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string CardType { get; set; }
        public string Address { get; set; }
        public virtual List<BPAddressesSAP> Addresses { get; set; }
        public string ZipCode { get; set; }
        public string MailAddres { get; set; }
        public CompanyView company { get; set; }
        public List<ContacPerson> ListContact { get; set; }
        public string RateCurrency { get; set; }
        public string Currency { get; set; }
        public string ShipToDef { get; set; }
        public string BillToDef { get; set; }
        public short? ListNum { get; set; }

        public short? ShipType { get; set; }

        public string Notes { get; set; }

        public int? SlpCode { get; set; }

        public string QryGroup1 { get; set; }

        public string QryGroup2 { get; set; }

        public string QryGroup3 { get; set; }

        public string QryGroup4 { get; set; }

        public string QryGroup5 { get; set; }

        public string QryGroup6 { get; set; }

        public string QryGroup7 { get; set; }

        public string QryGroup8 { get; set; }

        public string QryGroup9 { get; set; }

        public string QryGroup10 { get; set; }

        public string QryGroup11 { get; set; }

        public string QryGroup12 { get; set; }

        public string QryGroup13 { get; set; }

        public string QryGroup14 { get; set; }

        public string QryGroup15 { get; set; }

        public string QryGroup16 { get; set; }

        public string QryGroup17 { get; set; }

        public string QryGroup18 { get; set; }

        public string QryGroup19 { get; set; }

        public string QryGroup20 { get; set; }

        public string QryGroup21 { get; set; }

        public string QryGroup22 { get; set; }

        public string QryGroup23 { get; set; }

        public string QryGroup24 { get; set; }

        public string QryGroup25 { get; set; }

        public string QryGroup26 { get; set; }

        public string QryGroup27 { get; set; }

        public string QryGroup28 { get; set; }

        public string QryGroup29 { get; set; }

        public string QryGroup30 { get; set; }

        public string QryGroup31 { get; set; }

        public string QryGroup32 { get; set; }

        public string QryGroup33 { get; set; }

        public string QryGroup34 { get; set; }

        public string QryGroup35 { get; set; }

        public string QryGroup36 { get; set; }

        public string QryGroup37 { get; set; }

        public string QryGroup38 { get; set; }

        public string QryGroup39 { get; set; }

        public string QryGroup40 { get; set; }

        public string QryGroup41 { get; set; }

        public string QryGroup42 { get; set; }

        public string QryGroup43 { get; set; }

        public string QryGroup44 { get; set; }

        public string QryGroup45 { get; set; }

        public string QryGroup46 { get; set; }

        public string QryGroup47 { get; set; }

        public string QryGroup48 { get; set; }

        public string QryGroup49 { get; set; }

        public string QryGroup50 { get; set; }

        public string QryGroup51 { get; set; }

        public string QryGroup52 { get; set; }

        public string QryGroup53 { get; set; }

        public string QryGroup54 { get; set; }

        public string QryGroup55 { get; set; }

        public string QryGroup56 { get; set; }

        public string QryGroup57 { get; set; }

        public string QryGroup58 { get; set; }

        public string QryGroup59 { get; set; }

        public string QryGroup60 { get; set; }

        public string QryGroup61 { get; set; }

        public string QryGroup62 { get; set; }

        public string QryGroup63 { get; set; }

        public string QryGroup64 { get; set; }

        public decimal? Balance { get; set; }

        public decimal? DNotesBal { get; set; }

        public decimal? OrdersBal { get; set; }

        public int? OprCount { get; set; }

        public string LicTradNum { get; set; }

        public BusinessPartnerCombo BPCombo { get; set; }
        public string ErrorResponse { get; set; }
        public List<BPPropertiesSAP> PropertiesList { get; set; }
        public List<CurrencySAP> CurrencyList { get; set; }
        public List<ShippingType> ShipTypeList { get; set; }
        public List<PriceListSAP> PriceList { get; set; }
        public List<SalesEmployeeSAP> EmployeeList { get; set; }
        public List<CountrySAP> CountryList { get; set; }
        public List<StateSAP> CountyList { get; set; }
        public List<PaymentMethod> PaymentMethodList { get; set; }
        public List<UFD1_SAP> ListFiscIdValues { get; set; }

        public string CompanyCountry { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Cellular { get; set; }
        public string E_Mail { get; set; }
        public string IntrntSite { get; set; }
        public int GroupNum { get; set; }
        public string PymCode { get; set; }
        public string VatStatus { get; set; }

        public decimal? Discount { get; set; }

        public string U_B1SYS_FiscIdType { get; set; }

        public string BalanceJSONView
        {
            get { return (this.Balance.HasValue ? this.Balance.Value.ToString() : "0"); }
        }

        //Agregados para Portal
        public bool BPGroupSelected { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }

    }
}
