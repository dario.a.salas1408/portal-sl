﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class RolPageActionView
    {
        public RolPageActionView()
        {
            Page = new PageView();
        }

        public int IdRol { get; set; }

        public int IdPage { get; set; }

        public int IdAction { get; set; }

        public PageView Page { get; set; }

    }
}
