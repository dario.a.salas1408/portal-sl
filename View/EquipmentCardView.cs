﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class EquipmentCardView
    {
        public int insID { get; set; }
        public string customer { get; set; }
        public string custmrName { get; set; }
        public int? contactCod { get; set; }
        public string manufSN { get; set; }
        public string internalSN { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public short? itemGroup { get; set; }  
    }
}
