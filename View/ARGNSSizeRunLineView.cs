﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSSizeRunLineView
    {
        public string Code { get; set; }

        public int LineId { get; set; }

        public string U_SizeRunCode { get; set; }

        public string U_SizeCode { get; set; }

        public short? U_Percent { get; set; }

        public short? U_Qty { get; set; }
    }
}
