﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PurchaseOrderItemsFormView
    {
        public PurchaseOrderItemsFormView()
        {

        }

        public PurchaseOrderItemsFormView(List<PurchaseOrderLineView> pLines, string pDocCur, UsersSettingView pUsersSetting, string pPageKey)
        {
            this.Lines = pLines;
            this.DocCur = pDocCur;
            this.UsersSetting = pUsersSetting;
            this.PageKey = pPageKey;
        }

        public List<PurchaseOrderLineView> Lines { get; set; }

        public string DocCur { get; set; }

        public UsersSettingView UsersSetting { get; set; }

        public bool CkCatalogNum { get; set; }

        public string PageKey { get; set; }
        public string LawsSet { get; set; }
    }
}
