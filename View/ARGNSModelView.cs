﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSModelView
    {
        public ARGNSModelView()
        {
            ModelColorList = new List<ARGNSModelColor>();
            ModelSizeList = new List<ARGNSModelSize>();
            ModelVariableList = new List<ARGNSModelVar>();
            ModelImageList = new List<ARGNSModelImg>();
            ModelStockList = new List<StockModel>();
            ListPDMSAPCombo = new PDMSAPCombo();
            ModelFileList = new List<ARGNSModelFile>();
            ListComment = new List<ARGNSModelComment>();
            ModelProjectList = new List<ARGNSCrPathView>();
            ListCostSheet = new List<ARGNSCostSheet>();
            ModelPomList = new List<ARGNSModelPom>();
            ModelCSList = new List<ARGNSModelCostSheet>();
            ModelSegmentation = new ARGNSSegmentationView();
            U_RMaterial = "N";
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ATGrp { get; set; }
        public string U_ModCode { get; set; }
        public string U_ModDesc { get; set; }
        public System.DateTime? U_SSDate { get; set; }
        public System.DateTime? U_SCDate { get; set; }
        public string U_Year { get; set; }
        public string U_COO { get; set; }
        public string U_Pic { get; set; }
        public string U_Active { get; set; }
        public short U_SapGrp { get; set; }
        public short U_PList { get; set; }
        public string U_Division { get; set; }
        public string U_Season { get; set; }
        public string U_FrgnDesc { get; set; }
        public string U_ModGrp { get; set; }
        public string U_StyleTyp { get; set; }
        public string U_SclCode { get; set; }
        public string U_LineCode { get; set; }
        public string U_Vendor { get; set; }
        public string U_MainWhs { get; set; }
        public string U_Comments { get; set; }
        public string U_Owner { get; set; }
        public string U_Approved { get; set; }
        public string U_PicR { get; set; }
        public decimal U_Price { get; set; }
        public string U_WhsNewI { get; set; }
        public string U_CollCode { get; set; }
        public string U_AmbCode { get; set; }
        public string U_CompCode { get; set; }
        public string U_Brand { get; set; }
        public string U_ChartCod { get; set; }
        public string U_Designer { get; set; }
        public string U_Customer { get; set; }
        public string U_GrpSCod { get; set; }
        public string U_Currency { get; set; }
        public string U_Status { get; set; }
        public string U_RMaterial { get; set; }
        public string U_DocNumb { get; set; }
        public List<ARGNSModelColor> ModelColorList { get; set; }
        public List<ARGNSModelSize> ModelSizeList { get; set; }
        public List<ARGNSModelVar> ModelVariableList { get; set; }
        public List<PriceListSAP> GetPriceLiList { get; set; }
        public List<StockModel> ModelStockList { get; set; }
        public List<ARGNSModelImg> ModelImageList { get; set; }
        public List<ARGNSModelFile> ModelFileList { get; set; }
        public List<ARGNSCrPathView> ModelProjectList { get; set; }
        public List<ARGNSModelPom> ModelPomList { get; set; }
        public PDMSAPCombo ListPDMSAPCombo { get; set; }
        public AdministrationApparel administrationApparel { get; set; }
        public AdministrationSAP administrationSAP { get; set; }
        public ARGNSSegmentationView ModelSegmentation { get; set; }

        public string ImageName { get; set; }

        public string[] ColorTableItems { get; set; }
        public string[] ScaleTableItems { get; set; }
        public string[] VariableTableItems { get; set; }
        public List<ARGNSModelComment> ListComment { get; set; }
        public List<ARGNSCostSheet> ListCostSheet { get; set; }

        public string U_SclPOM { get; set; }
        public string U_CodePOM { get; set; }
        public string PageKey { get; set; }

        public List<ARGNSModelFitt> ModelFittList { get; set; }
        public List<ARGNSModelInst> ModelInstList { get; set; }
        public List<ARGNSModelBom> ModelBomList { get; set; }
        public List<ARGNSModelWFLOW> ModelWFLOWList { get; set; }
        public List<ARGNSModelLogos> ModelLogosList { get; set; }
        public List<ARGNSModelCostSheet> ModelCSList { get; set; }
        
        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}
