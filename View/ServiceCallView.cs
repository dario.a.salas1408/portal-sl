﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ServiceCallView
    {

        public ServiceCallView()
        {
            ComboList = new ServiceCallComboList();
            ContactPersonList = new List<ContacPerson>();
            ActivitiesList = new List<ServiceCallActivitiesView>();
            AttachmentList = new List<AttachmentView>();
        }

        public int callID { get; set; }
        public string subject { get; set; }
        public string customer { get; set; }
        public string custmrName { get; set; }
        public int? contctCode { get; set; }
        public string manufSN { get; set; }
        public string internalSN { get; set; }
        public int? contractID { get; set; }
        public DateTime? cntrctDate { get; set; }
        public DateTime? resolDate { get; set; }
        public short? resolTime { get; set; }
        public string free_1 { get; set; }
        public DateTime? free_2 { get; set; }
        public short? origin { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public short? itemGroup { get; set; }
        public short? status { get; set; }
        public string priority { get; set; }
        public short? callType { get; set; }
        public short? problemTyp { get; set; }
        public short? assignee { get; set; }
        public string descrption { get; set; }
        public string objType { get; set; }
        public int? logInstanc { get; set; }
        public short? userSign { get; set; }
        public DateTime? createDate { get; set; }
        public short? createTime { get; set; }
        public DateTime? closeDate { get; set; }
        public short? closeTime { get; set; }
        public short? userSign2 { get; set; }
        public DateTime? updateDate { get; set; }
        public int? SCL1Count { get; set; }
        public int? SCL2Count { get; set; }
        public string isEntitled { get; set; }
        public int? insID { get; set; }
        public int? technician { get; set; }
        public string resolution { get; set; }
        public int? Scl1NxtLn { get; set; }
        public int? Scl2NxtLn { get; set; }
        public int? Scl3NxtLn { get; set; }
        public int? Scl4NxtLn { get; set; }
        public int? Scl5NxtLn { get; set; }
        public string isQueue { get; set; }
        public string Queue { get; set; }
        public DateTime? resolOnDat { get; set; }
        public short? resolOnTim { get; set; }
        public DateTime? respByDate { get; set; }
        public short? respByTime { get; set; }
        public DateTime? respOnDate { get; set; }
        public short? respOnTime { get; set; }
        public short? respAssign { get; set; }
        public DateTime? AssignDate { get; set; }
        public short? AssignTime { get; set; }
        public short? UpdateTime { get; set; }
        public short? responder { get; set; }
        public string Transfered { get; set; }
        public short Instance { get; set; }
        public int DocNum { get; set; }
        public short? Series { get; set; }
        public string Handwrtten { get; set; }
        public string PIndicator { get; set; }
        public DateTime? StartDate { get; set; }
        public int? StartTime { get; set; }
        public DateTime? EndDate { get; set; }
        public int? EndTime { get; set; }
        public decimal? Duration { get; set; }
        public string DurType { get; set; }
        public string Reminder { get; set; }
        public decimal? RemQty { get; set; }
        public string RemType { get; set; }
        public DateTime? RemDate { get; set; }
        public string RemSent { get; set; }
        public short? RemTime { get; set; }
        public short? Location { get; set; }
        public string AddrName { get; set; }
        public string AddrType { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Room { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string DisplInCal { get; set; }
        public string SupplCode { get; set; }
        public string Attachment { get; set; }
        public int? AtcEntry { get; set; }
        public string NumAtCard { get; set; }
        public short? ProSubType { get; set; }
        public ServiceCallComboList ComboList { get; set; }
        public List<ContacPerson> ContactPersonList { get; set; }
        public string CustomerPhone { get; set; }
        public List<ServiceCallActivitiesView> ActivitiesList { get; set; }
        public List<AttachmentView> AttachmentList { get; set; }
        public string Action { get; set; }
        public string FromPage { get; set; }

        //Agregados

        public string PortalBPCode { get; set; }
        public string PortalBPName { get; set; }
    }
}
