﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ShipmentMembersView
    {
        public List<ARGNSContainerStatusView> ListStatus { get; set; }
        public List<ARGNSContainerPortView> ListPort { get; set; }
        public List<FreightView> ListFreight { get; set; }
    }
}
