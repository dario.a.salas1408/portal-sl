﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PurchaseRequestView
    {
        public PurchaseRequestView()
        {
            Lines = new List<PurchaseRequestLineView>();
            ListItem = new List<ItemMasterView>();
            ListReqType = new List<ReqTypeSAP>();
            ModelList = new List<ModelDescView>();
            MatrixModel = new MatrixModelView();
            SAPDocSettings = new DocumentSettingsSAP();
        }

        public int DocEntry { get; set; }

        public int DocNum { get; set; }

        public string DocType { get; set; }

        public int? ReqType { get; set; }

        public string Requester { get; set; }

        public string ReqName { get; set; }

        public short? Branch { get; set; }

        public short? Department { get; set; }

        public string Email { get; set; }

        public string Notify { get; set; }

        public DateTime? DocDate { get; set; }

        public DateTime? DocDueDate { get; set; }

        public DateTime? TaxDate { get; set; }

        public DateTime? ReqDate { get; set; }

        public string Comments { get; set; }

        public string DocStatus { get; set; }

        public string CardCode { get; set; }

        public string CardName { get; set; }

        public string Address { get; set; }

        public string NumAtCard { get; set; }

        public decimal? DiscPrcnt { get; set; }

        public string DocCur { get; set; }

        public decimal? DocRate { get; set; }

        public decimal? DocTotal { get; set; }

        public List<PurchaseRequestLineView> Lines { get; set; }

        public List<ItemMasterView> ListItem { get; set; }

        public List<ReqTypeSAP> ListReqType { get; set; }

        public string ShipToCode { get; set; }

        public string FatherType { get; set; }

        public string JrnlMemo { get; set; }

        public string ObjType { get; set; }

        public string Address2 { get; set; }

        public short? TrnspCode { get; set; }

        public string PeyMethod { get; set; }

        public short? GroupNum { get; set; }

        public int? CntctCode { get; set; }

        public int? OwnerCode { get; set; }

        public int? SlpCode { get; set; }

        public string SummryType { get; set; }

        public string RevisionPo { get; set; }
        public decimal? TotalExpns { get; set; }

        public string Project { get; set; }

        public DateTime? CancelDate { get; set; }
        public bool Draft { get; set; }
        public string Confirmed { get; set; }
        public string DescConfirmed
        {
            get
            {
                switch (Confirmed)
                {
                    case "Y":
                        return "Yes";

                    case "N":
                        return "No";

                    default:
                        return Confirmed;

                }
            }
        }

        public List<ModelDescView> ModelList { get; set; }
        public MatrixModelView MatrixModel { get; set; }
        public DocumentSettingsSAP SAPDocSettings { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }
        public string PageKey { get; set; }
        public PurchaseRequestCombo PurchaseRequestCombo { get; set; }

        //Added to JSON View
        public string ReqDateJSONView
        {
            get { return this.ReqDate != null ? this.ReqDate.Value.ToShortDateString() : ""; }
        }
        public string DocDateJSONView
        {
            get { return this.DocDate != null ? this.DocDate.Value.ToShortDateString() : ""; }
        }
        public string DocDueDateJSONView
        {
            get { return this.DocDueDate != null ? this.DocDueDate.Value.ToShortDateString() : ""; }
        }

        public OpenQuantityStatus OpenQuantityStatus { get; set; }
    }
}
