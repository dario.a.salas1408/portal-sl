﻿using System;
using System.Collections.Generic;

namespace ARGNS.View
{
	public class SalesOrderItemsFormView
    {
        public SalesOrderItemsFormView()
        {
                
        }

        public SalesOrderItemsFormView(List<SaleOrderLineView> pLines, string pDocCur, 
			UsersSettingView pUsersSetting, DateTime? pReqDate, 
			List<FreightView> pFreightList, 
            bool hasPermissionToViewOpenQuantity)
        {
            this.Lines = pLines;
            this.DocCur = pDocCur;
            this.UsersSetting = pUsersSetting;
            this.ReqDate = pReqDate;
            this.FreightList = pFreightList;
            this.ShowOpenQuantity = hasPermissionToViewOpenQuantity; 
        }

        public List<SaleOrderLineView> Lines { get; set; }
        public string DocCur { get; set; }
        public UsersSettingView UsersSetting { get; set; }
        public DateTime? ReqDate { get; set; }
        public List<FreightView> FreightList { get; set; }
        public bool CkCatalogNum { get; set; }
        public string LawsSet { get; set; }
        public bool ShowOpenQuantity { get; set; }
        
    }
}
