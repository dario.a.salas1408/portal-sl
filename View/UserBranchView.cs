﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class UserBranchView
    {
        public int IdUserBranches { get; set; }
        public int IdUser { get; set; }
        public int IdBranchSAP { get; set; }
        public int IdCompany { get; set; }
        public string BranchName { get; set; }
        public CompanyView CompanyConn { get; set; }
    }
}
