﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class UserDirectAccessView
    {
        public int IdUser { get; set; }
        
        public int IdCompany { get; set; }
        
        public int IdDirectAccess { get; set; }
        
        //Agregados para Tiles
        public int IdTile { get; set; }
        public short? VisualOrder { get; set; }
    }
}
