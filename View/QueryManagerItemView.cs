﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class QueryManagerItemView
    {
        public int IdQuery { get; set; }

        public int IdCompany { get; set; }

        public string QueryIdentifier { get; set; }

        public string SelectField { get; set; }

        public string FromField { get; set; }

        public string WhereField { get; set; }

        public string OrderByField { get; set; }

        public string GroupByField { get; set; }

        public bool Active { get; set; }
    }
}
