﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class MatrixModelView
    {
        public MatrixModelView()
        {
            SkuModelList = new List<SkuModelView>();
            ModelImagesList = new List<ARGNSModelImgView>();
            ModelPrepackList = new List<ARGNSPrepackView>();
            SizeRunList = new List<ARGNSSizeRunView>();
        }

        public List<SkuModelView> SkuModelList { get; set; }
        public List<ARGNSModelImgView> ModelImagesList { get; set; }
        public List<ARGNSPrepackView> ModelPrepackList { get; set; }
        public List<ARGNSSizeRunView> SizeRunList { get; set; }
        public string ModelCode { get; set; }

    }
}
