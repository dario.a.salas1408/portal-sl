﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class SAPObjectView
    {
        public SAPObjectView()
        {       
        }

        public SAPObjectView(int pIdObject, string pObjectName, string pObjectTable, bool pVisible)
        {
            this.IdObject = pIdObject;
            this.ObjectName = pObjectName;
            this.ObjectTable = pObjectTable;
            this.Visible = pVisible;
        }
        public int IdObject { get; set; }

        public string ObjectName { get; set; }

        public string ObjectTable { get; set; }

        public bool Visible { get; set; }
    }
}
