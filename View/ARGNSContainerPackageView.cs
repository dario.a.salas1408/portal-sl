﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSContainerPackageView
    {
        public ARGNSContainerPackageView()
        {
            this.Lines = new List<ARGNSContainerPackageLineView>();        
        }
        
        public int DocEntry { get; set; }

        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public short? U_CntnerNum { get; set; }

        public string U_CntnerTyp { get; set; }

        public decimal? U_Weight { get; set; }

        public short? U_WeightUnit { get; set; }

        //Lines
        public List<ARGNSContainerPackageLineView> Lines { get; set; }

        //Others
        public bool IsNew { get; set; }
        
    }
}
