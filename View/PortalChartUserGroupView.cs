﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalChartUserGroupView
    {
        public int Id { get; set; }

        public int UserGroup_Id { get; set; }

        public int PortalChart_IdChart { get; set; }
    }
}
