﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class WarehouseView
    {
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
    }
}
