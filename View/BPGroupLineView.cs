﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class BPGroupLineView
    {
        public int Id { get; set; }

        public string CardCode { get; set; }
        public string CardName { get; set; }

        public BPGroupView BPGroup { get; set; }
    }
}
