﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class StageView
    {
        public int WstCode { get; set; }

        public string Name { get; set; }
    }
}
