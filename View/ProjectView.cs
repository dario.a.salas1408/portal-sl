﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class ProjectView
    {
        public ProjectView()
        {
            this.StatusList = new List<ListItem>() {
                new ListItem { Value = "1", Text = "In Progress"},
                new ListItem { Value = "2", Text = "Closed" },
                new ListItem { Value = "3", Text = "Cancelled"}};
            this.PlanList = new List<ListItem>() {
                new ListItem { Value = "1", Text = "Forward Plan"},
                new ListItem { Value = "2", Text = "Backward Plan" }};
        }

        public string Code { get; set; }

        public string Name { get; set; }

        public int DocEntry { get; set; }

        public string Canceled { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        public string DataSource { get; set; }

        public string U_ProyCode { get; set; }

        public string U_Desc { get; set; }

        public string U_Model { get; set; }

        public string U_Workflow { get; set; }

        public string U_Status { get; set; }

        public string U_Calendar { get; set; }

        public string U_Planning { get; set; }

        public DateTime? U_SDate { get; set; }

        public DateTime? U_CDate { get; set; }

        public string U_SalesO { get; set; }

        public string U_DocType { get; set; }

        //Agregados

        public List<ARGNSCrPathActivitiesView> ActivitiesList { get; set; }
        public List<ARGNSRouting> RoutingList { get; set; }
        public List<ListItem> StatusList { get; set; }
        public List<ListItem> PlanList { get; set; }
        public List<Department> DepartmentList { get; set; }
        public List<RolSAP> RolSAPList { get; set; }
        public string PageKey { get; set; }
        public string ModelID { get; set; }
        public string U_Pic { get; set; }
        public string u_ModelDesc { get; set; }
        public string ImageName { get; set; }

    }
}
