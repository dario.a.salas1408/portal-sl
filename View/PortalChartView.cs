﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalChartView
    {
        public PortalChartView()
        {
            this.PortalChartColumns = new List<PortalChartColumnView>();
        }

        public int IdChart { get; set; }

        public string ChartName { get; set; }

        public int? IdSAPQuery { get; set; }

        public string NameSAPQuery { get; set; }

        public int IdChartType { get; set; }

        public string PortalPage { get; set; }

        public int IdCompany { get; set; }
        public bool MenuChart { get; set; }

        public List<PortalChartTypeView> ListPortalChartType { get; set; }
        public List<PortalChartColumnView> PortalChartColumns { get; set; }
        public List<PortalChartUserGroupView> PortalChartUserGroups { get; set; }
        public List<PortalChartParamView> PortalChartParamList { get; set; }
    }
}
