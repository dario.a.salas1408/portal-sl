﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View.AdvanceSearchViews
{
    public class AdvanceSearchMyPurchaseDocumentView
    {
        private string mDocDate;
        private string mDocStatus;
        private string mReqDate;
        private string mDocType;
        private string mApprovalStatus;
        private string mCodeVendor;
        private string mDocDateFrom;
        private string mDocDateTo;
        private string mDocNum;
        public string pDocDate { get { if (this.mDocDate == null) return ""; else return this.mDocDate; } set { this.mDocDate = value; } }
        public string pDocStatus { get { if (this.mDocStatus == null) return ""; else return this.mDocStatus; } set { this.mDocStatus = value; } }
        public string pReqDate { get { if (this.mReqDate == null) return ""; else return this.mReqDate; } set { this.mReqDate = value; } }
        public string pDocDateFrom { get { if (this.mDocDateFrom == null) return ""; else return this.mDocDateFrom; } set { this.mDocDateFrom = value; } }
        public string pDocDateTo { get { if (this.mDocDateTo == null) return ""; else return this.mDocDateTo; } set { this.mDocDateTo = value; } }
        public string pDocType { get { if (this.mDocType == null) return ""; else return this.mDocType; } set { this.mDocType = value; } }
        public string pApprovalStatus { get { if (this.mApprovalStatus == null) return ""; else return this.mApprovalStatus; } set { this.mApprovalStatus = value; } }
        public string pCodeVendor { get { if (this.mCodeVendor == null) return ""; else return this.mCodeVendor; } set { this.mCodeVendor = value; } }
        public string pDocNum { get { if (this.mDocNum == null) return ""; else return this.mDocNum; } set { this.mDocNum = value; } }
    }
}
