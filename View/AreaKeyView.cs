﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class AreaKeyView
    {
        public AreaKeyView()
        { Pages = new List<PageView>(); }

        public int IdAreaKeys { get; set; }
        public string Area { get; set; }
        public string KeyNumber { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string KeyResource { get; set; }
        public string translated { get; set; }
        public List<PageView> Pages { get; set; }
    }
}
