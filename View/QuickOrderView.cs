﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Model;
using ARGNS.Model.Implementations;
using AutoMapper;
using System.Web.UI.WebControls;
using ARGNS.Model.Implementations.SAP;

namespace ARGNS.View
{
    public class QuickOrderView
    {
        public QuickOrderView()
        {
            ItemPrices = new ItemPrices();
        }

        public int IdQuickOrder { get; set; }

        public int IdUser { get; set; }

        public int? IdCompany { get; set; }

        public string IdBp { get; set; }

        public string ItemCode { get; set; }

        public string ItemName { get; set; }

        public int Quantity { get; set; }

        public ItemPrices ItemPrices { get; set; }
        public List<UnitOfMeasure> UnitOfMeasureList { get; set; }

        public decimal? OnHand { get; set; }
        public StockModelView Stock { get; set; }
        public string WarehouseCode { get; set; }


    }

    
}
