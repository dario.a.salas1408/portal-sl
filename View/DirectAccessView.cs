﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class DirectAccessView
    {
        public int IdDirectAccess { get; set; }
        public int Page { get; set; }
        public int Action { get; set; }
        public int Area { get; set; }
        public string Url { get; set; }
        public string UrlName { get; set; }
        public string TranslationKey { get; set; }

        //Agregados para Tiles
        public int IdTile { get; set; }

        //Agregados para View
        public string TranslatedName { get; set; }
    }
}
