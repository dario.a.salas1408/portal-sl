﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PurchaseQuotationLineView
    {
        public PurchaseQuotationLineView()
        {
            ListCurrency = new List<CurrencySAP>();
        }

        public int DocEntry { get; set; }
        public int LineNum { get; set; }
        public int? TargetType { get; set; }
        public int? TrgetEntry { get; set; }
        public string BaseRef { get; set; }
        public int? BaseType { get; set; }
        public int? BaseEntry { get; set; }
        public int? BaseLine { get; set; }
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public string SubCatNum { get; set; }
        public string AcctCode { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal PriceBefDi { get; set; }
        public string Currency { get; set; }
        public string WhsCode { get; set; }
        public string TaxCode { get; set; }
        public string Project { get; set; }
        public string OcrCode { get; set; }
        public string FreeTxt { get; set; }
        public Nullable<System.DateTime> ShipDate { get; set; }
        public List<CurrencySAP> ListCurrency { get; set; }
        public double RateGl { get; set; }
        public double RateLine { get; set; }
        public Nullable<System.DateTime> PQTReqDate { get; set; }
        public string UomCode { get; set; }
        public string LineStatus { get; set; }
        public decimal? PQTReqQty { get; set; }

        //Agregados
        public GLAccountView GLAccount { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }

        public decimal? VatPrcnt { get; set; }
        public decimal? PriceAfVAT { get; set; }
        public decimal? VatSum { get; set; }
        public decimal? GTotal { get; set; }
        public decimal? DiscPrcnt { get; set; }
        public string CalcTax { get; set; }

        public List<UnitOfMeasure> UnitOfMeasureList { get; set; }
        public List<ItemPrices> ItemPrices { get; set; }
        public string TaxCodeAP { get; set; }
    }
}
