﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class CompanyStockView
    {
        public CompanyStockView()
        {
            CompanyConn = new CompanyView();
            ModeAdd = true;
            WarehouseList = new List<WarehouseView>();
            WarehousePortalList = new List<WarehousePortalView>();
        }
        public int IdCompanyStock { get; set; }
        public int IdCompany { get; set; }
        public bool ValidateStock { get; set; }
        public string Formula { get; set; }
        public CompanyView CompanyConn { get; set; }
        public bool ModeAdd { get; set; }
        public string Server { get; set; }
        public string CompanyDB { get; set; }
        public List<WarehouseView> WarehouseList { get; set; }
        public List<WarehousePortalView> WarehousePortalList { get; set; }

    }
}
