﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class ItemMasterView : IItemMasterSAP
    {

        public ItemMasterView()
        {
            ListCurrency = new List<CurrencySAP>();
            ListItemGroup = new List<ItemGroup>();
            ListPriceListSAP = new List<PriceListSAP>();
            ListAttachmentSAP = new List<AttachmentSAP>();
            ItemPrices = new List<ItemPrices>();
            UnitOfMeasureList = new List<UnitOfMeasure>();
            SerialsSAP = new List<Serial>();
            hasStock = false;
            drItemType = new List<ListItem>() { 
                new ListItem { Value = "I", Text = "Items"}, 
                new ListItem { Value = "L", Text = "Labor" },
                new ListItem { Value = "T", Text = "Travel"}};
        }

        public string ItemCode { get; set; }

        public string ItemName { get; set; }

        public string PrchseItem { get; set; }

        public string SellItem { get; set; }

        public string InvntItem { get; set; }

        public decimal? LastPurPrc { get; set; }

        public string LastPurCur { get; set; }

        public DateTime? LastPurDat { get; set; }

        public List<ItemPrices> ItemPrices { get; set; }

        public List<CurrencySAP> ListCurrency { get; set; }

        public bool hasStock { get; set; }

        public string ManBtchNum { get; set; }

        public string ManSerNum { get; set; }

        public short? ItmsGrpCod { get; set; }

        public string CardCode { get; set; }

        public decimal? OnHand { get; set; }

        public string PicturName { get; set; }

        public string UserText { get; set; }
        public List<ListItem> drItemType { get; set; }

        public string ItemType { get; set; }

        public List<ItemGroup> ListItemGroup { get; set; }

        public List<PriceListSAP> ListPriceListSAP { get; set; }

        public List<AttachmentSAP> ListAttachmentSAP { get; set; }

        public int PriceSelect { get; set; }

        public int? AtcEntry { get; set; }

        public decimal PriceItemByListPrice { get; set; }

        public string DfltWH { get; set; }

        public int UgpEntry { get; set; }

        public List<UnitOfMeasure> UnitOfMeasureList { get; set; }

        public List<UDF_ARGNS> MappedUdf { get; set; }
        public StockModelView Stock { get; set; }
        public decimal? CostPrice { get; set; }
        public string CostPriceCurrency { get; set; }
        public int? CostPriceList { get; set; }
        public string Substitute { get; set; }
        public string ItmsGrpNam { get; set; }
        public string CardName { get; set; }
        public string TaxCodeAR { get; set; }
        public string TaxCodeAP { get; set; }
        public string UomGroupName { get; set; }
        public string validFor { get; set; }
        public DateTime? validFrom { get; set; }
        public DateTime? validTo { get; set; }
        public string frozenFor { get; set; }
        public DateTime? frozenFrom { get; set; }
        public DateTime? frozenTo { get; set; }

        //Added To CodeBars
        public int? UomEntry { get; set; }

        public int PriceUnit { get; set; }

        public int? PUoMEntry { get; set; }
        public int? SUoMEntry { get; set; }
        public List<Serial> SerialsSAP { get; set; }
        public string CodeBars { get; set; }

    }
}
