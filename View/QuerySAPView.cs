﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class QuerySAPView
    {
        public int IntrnalKey { get; set; }

        public int QCategory { get; set; }

        public string QName { get; set; }

        public string QString { get; set; }

        public string QType { get; set; }

        public string ColumnSize { get; set; }

        public int? DBType { get; set; }

        public QueryCategorySAPView CategorySAP { get; set; }
    }
}
