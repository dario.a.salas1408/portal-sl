﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class FreightView
    {
        public FreightView()
        {
            ListDistrRule = new List<DistrRuleSAP>();
            ListTax = new List<SalesTaxCodesSAP>();
            ListDistrMethod = new List<DistrMethodView>();
            ListProject = new List<ProjectSAP>();
            ListDistrMethod.Add(new DistrMethodView("N", "None"));
            ListDistrMethod.Add(new DistrMethodView("Q", "Quantity"));
            ListDistrMethod.Add(new DistrMethodView("V", "Volume"));
            ListDistrMethod.Add(new DistrMethodView("W", "Weight"));
            ListDistrMethod.Add(new DistrMethodView("E", "Equally"));
            ListDistrMethod.Add(new DistrMethodView("T", "Row Total"));
        }
        public int DocEntry { get; set; }

        public int? ExpnsCode { get; set; }

        public string ExpnsName { get; set; }

        public decimal? LineTotal { get; set; }

        public decimal? TotalFrgn { get; set; }

        public decimal? TotalSumSy { get; set; }

        public string Comments { get; set; }

        public string ObjType { get; set; }

        public string DistrbMthd { get; set; }

        public string TaxStatus { get; set; }

        public string VatGroup { get; set; }

        public decimal? VatPrcnt { get; set; }

        public decimal? VatSum { get; set; }

        public decimal? VatSumFrgn { get; set; }

        public decimal? VatSumSy { get; set; }

        public decimal? DedVatSum { get; set; }

        public decimal? DedVatSumF { get; set; }

        public decimal? DedVatSumS { get; set; }

        public string IsAcquistn { get; set; }

        public string TaxCode { get; set; }

        public string TaxType { get; set; }

        public string WTLiable { get; set; }

        public decimal? VatApplied { get; set; }

        public decimal? VatAppldFC { get; set; }

        public decimal? VatAppldSC { get; set; }

        public decimal? EquVatPer { get; set; }

        public decimal? EquVatSum { get; set; }

        public decimal? EquVatSumF { get; set; }

        public decimal? EquVatSumS { get; set; }

        public decimal? LineVat { get; set; }

        public decimal? LineVatF { get; set; }

        public decimal? LineVatS { get; set; }

        public string BaseMethod { get; set; }

        public string Stock { get; set; }

        public string LstPchPrce { get; set; }

        public string AnalysRpt { get; set; }

        public int? BaseAbsEnt { get; set; }

        public int? BaseType { get; set; }

        public int? BaseRef { get; set; }

        public int? BaseLnNum { get; set; }

        public int LineNum { get; set; }

        public string Status { get; set; }

        public int? TrgType { get; set; }

        public int? TrgAbsEnt { get; set; }

        public decimal? StDstr { get; set; }

        public decimal? StDstrSC { get; set; }

        public decimal? StDstrFC { get; set; }

        public string FixCurr { get; set; }

        public decimal? VatDscntPr { get; set; }

        public string OcrCode { get; set; }

        public string TaxDistMtd { get; set; }

        public string OcrCode2 { get; set; }

        public string OcrCode3 { get; set; }

        public string OcrCode4 { get; set; }

        public string OcrCode5 { get; set; }

        public string Project { get; set; }

        public string VatGrpSrc { get; set; }

        public decimal? DrawnTotal { get; set; }

        public decimal? DrawnFC { get; set; }

        public decimal? DrawnSC { get; set; }

        public List<DistrRuleSAP> ListDistrRule { get; set; }
        public List<DistrMethodView> ListDistrMethod { get; set; }
        public List<SalesTaxCodesSAP> ListTax { get; set; }
        public List<ProjectSAP> ListProject { get; set; }
    }
}
