﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class DraftPortalView
    {
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public string DocType { get; set; }

        public string DocStatus { get; set; }

        public string ObjType { get; set; }

        public DateTime? DocDate { get; set; }

        public DateTime? DocDueDate { get; set; }

        public string CardCode { get; set; }

        public string CardName { get; set; }

        public string Address { get; set; }

        public string NumAtCard { get; set; }

        public decimal? VatSum { get; set; }

        public decimal? DiscPrcnt { get; set; }

        public decimal? DiscSum { get; set; }

        public string DocCur { get; set; }

        public decimal? DocRate { get; set; }

        public decimal? DocTotal { get; set; }

        public string Ref1 { get; set; }

        public string Ref2 { get; set; }

        public string Comments { get; set; }

        public string JrnlMemo { get; set; }

        public int? SlpCode { get; set; }

        public short? TrnspCode { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? TaxDate { get; set; }

        public short? UserSign { get; set; }

        public string WddStatus { get; set; }

        public int? draftKey { get; set; }

        public decimal? TotalExpns { get; set; }

        public DateTime? ReqDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public int? OwnerCode { get; set; }

        public string Requester { get; set; }

        public string ReqName { get; set; }

        public short? Branch { get; set; }

        public short? Department { get; set; }

        public string Email { get; set; }

        public string Notify { get; set; }

        //campos extras a la draft
        public string PODocStatus { get; set; }
        public string PQDocStatus { get; set; }
        public string PRDocStatus { get; set; }
        public string SODocStatus { get; set; }
        public string SQDocStatus { get; set; }
        public string OWDDStatus { get; set; }
        public string OWDDObjType { get; set; }
        public string DocTotalFormatted { get; set; }
        //Added by Jose
        public bool ShowOpenQuantity { get; set; }
        public OpenQuantityStatus OpenQuantityStatus { get; set; }

        public int DraftOrigin { get; set; }

        //Added to JSON View
        public string DocDateJSONView
        {
            get { return (this.DocDate.HasValue ? this.DocDate.Value.ToShortDateString() : ""); }
        }
        public string ReqDateJSONView
        {
            get { return (this.ReqDate.HasValue ? this.ReqDate.Value.ToShortDateString() : ""); }
        }
        public string OWDDStatusJSONView
        {
            get { return (!string.IsNullOrEmpty(this.OWDDStatus) ? this.OWDDStatus : ""); }
        }
        
    }
}
