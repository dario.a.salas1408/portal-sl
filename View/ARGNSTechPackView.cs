﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSTechPackView
    {
        public string Code { get; set; }

        public string ModelCode { get; set; }

        public string ModelDesc { get; set; }

        public List<ARGNSTechPackLineView> Lines { get; set; }

        public List<ARGNSTechPackSectionView> Sections { get; set; }
    }
}
