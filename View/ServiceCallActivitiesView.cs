﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ServiceCallActivitiesView
    {
        public ServiceCallActivitiesView()
        {
            Activity = new ActivitiesView();
        }
        public int SrvcCallId { get; set; }
        public short Line { get; set; }
        public int? ClgID { get; set; }       
        public int? VisOrder { get; set; }
        public ActivitiesView Activity { get; set; }
    }
}
