﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSContainerPackageLineView
    {
        public int DocEntry { get; set; }

        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public short? U_CntnerNum { get; set; }

        public string U_ItemCode { get; set; }

        public decimal? U_Quantity { get; set; }

        public int? U_UomEntry { get; set; }

        public string U_unitMsr { get; set; }

        public decimal? U_NumPerMsr { get; set; }

        public string U_BaseEntry { get; set; }

        public string U_BaseRef { get; set; }

        public string U_BaseLine { get; set; }

        //Lines
        public DraftLineView DocumentLine { get; set; }

        //Others
        public bool IsNew { get; set; }
        public string U_ItemName { get; set; }

        public string U_ARGNS_MOD { get; set; }

        public string U_ARGNS_COL { get; set; }

        public string U_ARGNS_SIZE { get; set; }
    }
}
