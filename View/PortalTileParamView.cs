﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalTileParamView
    {
        public int Id { get; set; }

        public string ParamName { get; set; }

        public string ParamValue { get; set; }

        public string ParamFixedValue { get; set; }

        public virtual PortalTileView PortalTile { get; set; }

        //Agregados 
        public bool IsNew { get; set; }
    }
}
