﻿namespace ARGNS.View
{
    public class InventoryStatusSearchView
    {
        public string pItemCodeFrom { get; set; }
        public string pItemCodeTo { get; set; }
        public string pVendorFrom { get; set; }
        public string pVendorTo { get; set; }
        public string pItemGroup { get; set; }
        public string[] pSelectedWH { get; set; }
        public int pStart { get; set; }
        public int pLength { get; set; }
    }
}
