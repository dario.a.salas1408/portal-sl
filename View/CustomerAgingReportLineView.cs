﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class CustomerAgingReportLineView
    {
        public string CustNum { get; set; }
        public string CustName { get; set; }
        public decimal? DebitAmt { get; set; }
        public decimal? CreditAmt { get; set; }
        public string TransType { get; set; }
        public string Reference { get; set; }
        public string Currency { get; set; }
        public DateTime PostingDate { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DocDate { get; set; }
        public decimal? from0to30Days { get; set; }
        public decimal? from31to60Days { get; set; }
        public decimal? from61to90days { get; set; }
        public decimal? from90to120Days { get; set; }
        public decimal? from120PlusDays { get; set; }
        public int? FolNumFrom { get; set; }
        public int? FolNumTo { get; set; }
        public int? SourceID { get; set; }
        public string DocSubType { get; set; }
    }
}
