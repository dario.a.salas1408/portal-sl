﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalChartParamView
    {
        public int Id { get; set; }

        public int PortalChart_IdChart { get; set; }

        public string ParamName { get; set; }
        public string ParamNamePortal { get; set; }        

        public string ParamValue { get; set; }

        public string ParamFixedValue { get; set; }

        public string ParamValueType { get; set; }

        public virtual PortalChartView PortalChart { get; set; }

        //Agregados 
        public bool IsNew { get; set; }
    }
}
