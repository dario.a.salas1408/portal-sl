﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class AvailableToPromiseView
    {
        public string Document { get; set; }
        public string DocumentName { get; set; }
        public int? DocEntry { get; set; }
        public int? LineNum { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public decimal? Ordered { get; set; }
        public decimal? Committed { get; set; }
        public decimal? OnHand { get; set; }
        public string UomCode { get; set; }
        public string UomName { get; set; }
        public decimal? BaseQty { get; set; }

        /*Solo para la vista*/
        public DateTime? DeliveryDateOrder {
            get
            {
                if (this.DeliveryDate == null)
                    return this.OrderDate;
                else
                    return this.DeliveryDate;
            }
        }
    }
}
