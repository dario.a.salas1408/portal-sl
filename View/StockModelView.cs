﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class StockModelView
    {
        public string ItemCode { get; set; }
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
        public decimal? OnHand { get; set; }
        public decimal? IsCommited { get; set; }
        public decimal? OnOrder { get; set; }
        public string U_ARGNS_SIZE { get; set; }
        public short? U_ARGNS_SIZEVO { get; set; }
        public string U_ARGNS_COL { get; set; }
        public string U_ARGNS_VAR { get; set; }
        public decimal? Total { get; set; }
        public decimal Available { get; set; }  
    }
}
