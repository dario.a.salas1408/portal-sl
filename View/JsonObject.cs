﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class JsonObject
    {
        public string Code { get; set; }
        public string Qty { get; set; }
    }
}
