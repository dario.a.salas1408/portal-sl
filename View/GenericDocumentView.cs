﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class GenericDocumentView
    {
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public string DocType { get; set; }
        public System.DateTime DocDate { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }

        //Added to JSON View
        public string DocDateJSONView
        {
            get { return this.DocDate.ToShortDateString(); }
        }
    }
}
