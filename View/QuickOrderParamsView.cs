﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class QuickOrderParamsView
    {
        public bool TilesSearchCk { get; set; }
        public bool DoubleTilesSearchCk { get; set; }
        public string ColumnRadio { get; set; }
        public bool FirstDoubleSearch { get; set; }
        public List<ItemMasterView> ListItemMaster { get; set; }
        public List<UDF_ARGNS> ListOITMUDF { get; set; }
        public List<QuickOrderView> ListQuickOrder { get; set; }
        public List<ItemGroupSAP> ListItemGroup { get; set; }
        public List<WarehouseView> ListWarehouse { get; set; }
        public int SalesOrderId { get; set; }
        public int TileToModify { get; set; }
        public int TotalQuantity { get; set; }

        //Added to take params from url
        public string UDF_Code { get; set; }
        public string UDF_Name { get; set; }
        public string ItemGroup { get; set; }
        public string TxtCodeBP { get; set; }
        public bool TileSearch { get; set; }
        public bool DoubleSearch { get; set; }
        public bool ZeroStock { get; set; }
        public bool ExecuteSearch { get; set; }
        public Dictionary<string, string> UDFListValues { get; set; }

    }
}
