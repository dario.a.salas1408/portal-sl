﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ItemToPackageView
    {
        public List<ARGNSAvailableItemView> ItemsToAdd { get; set; }

        public int PackageNum { get; set; }

        public int ContainerNum { get; set; }

        public string pPageKey { get; set; }

    }
}
