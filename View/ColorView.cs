﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ColorView
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ColCode { get; set; }
        public string U_ColDesc { get; set; }
        public string U_Shade { get; set; }
        public string U_NRFCode { get; set; }
        public string U_Active { get; set; }
        public string U_Argb { get; set; }
        public string U_Pic { get; set; }
        public string U_PicFile { get; set; }
        public string U_Attrib1 { get; set; }
        public string U_Attrib2 { get; set; }

        //Agregados en la view
        public int Alpha { get; set; }
        public int Red { get; set; }
        public int Green { get; set; }
        public int Blue { get; set; }

        public string U_ColorHex
        {
            get
            {
                string ret = string.Empty;
                if (!string.IsNullOrEmpty(U_Argb))
                {
                    Color color = Color.FromArgb(Convert.ToInt32(this.U_Argb.ToString()));

                    ret = string.Format("{0:X2}{1:X2}{2:X2}", color.R, color.G, color.B);
                }

                return ret;
            }
        }

        public string getColorRGB()
        {
            string ret = string.Empty;
            if (!string.IsNullOrEmpty(U_Argb))
            {
                Color color = Color.FromArgb(Convert.ToInt32(this.U_Argb.ToString()));
                ret = ("rgb(" + color.R + ", " + color.G + ", " + color.B + ")");
            }

            return ret;
        }

        public int getColorARGB(int pA, int pR, int pG, int pB)
        {
            Color color = Color.FromArgb(pA, pR, pG, pB);
            return color.ToArgb();
        }
    }
}
