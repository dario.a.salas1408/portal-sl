﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class SalesQuotationItemsFormView
    {
        public SalesQuotationItemsFormView()
        {

        }

        public SalesQuotationItemsFormView(List<SaleQuotationLineView> pLines, string pDocCur, UsersSettingView pUsersSetting, List<FreightView> pFreightList)
        {
            this.Lines = pLines;
            this.DocCur = pDocCur;
            this.UsersSetting = pUsersSetting;
            this.FreightList = pFreightList;
        }

        public List<SaleQuotationLineView> Lines { get; set; }

        public string DocCur { get; set; }

        public UsersSettingView UsersSetting { get; set; }

        public List<FreightView> FreightList { get; set; }

        public string LawsSet { get; set; }
    }
}
