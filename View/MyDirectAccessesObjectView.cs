﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class MyDirectAccessesObjectView
    {
        public List<DirectAccessView> ListDirectAccessView { get; set; }
        public List<UserDirectAccessView> ListUserDirectAccessView { get; set; }
        public CompanyView CompanyConnect { get; set; }
        public List<PageView> ListPage { get; set; }
        public List<ActionsView> ListAction { get; set; }
        public int IdUser { get; set; }
    }
}
