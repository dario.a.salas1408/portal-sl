﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSPrepackView
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public int DocEntry { get; set; }

        public string Canceled { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        public string DataSource { get; set; }

        public string U_PpCode { get; set; }

        public string U_PpType { get; set; }

        public string U_PpDesc { get; set; }

        public string U_SclCode { get; set; }

        public string U_BPCode { get; set; }

        public string U_NRFCode { get; set; }

        public string U_Active { get; set; }

        public string U_AFSeg { get; set; }

        public string U_ModGrp { get; set; }

        public string U_ModCode { get; set; }

        public string U_Shipto { get; set; }

        public string U_Sruncode { get; set; }

        public string U_Comb { get; set; }

        public string U_PriceList { get; set; }

        public string U_WhsCode { get; set; }

        //Agregados

        public List<ARGNSPrepackLinesView> PrepackLines { get; set; }
    }
}
