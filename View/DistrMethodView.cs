﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class DistrMethodView
    {
        public DistrMethodView(string Code, string Name)
        {
            this.Code = Code;
            this.Name = Name;
        }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
