﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class GLAccountView
    {
        public string AcctCode { get; set; }

        public string AcctName { get; set; }

        public string FormatCode { get; set; }
    }
}
