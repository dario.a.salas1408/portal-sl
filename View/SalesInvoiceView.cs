﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class SalesInvoiceView
    {
        public SalesInvoiceView()
	    {
            Lines = new List<SalesInvoiceLineView>();
	    }

        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public string DocType { get; set; }

        public string DocStatus { get; set; }

        public string ObjType { get; set; }

        public DateTime? DocDate { get; set; }

        public DateTime? DocDueDate { get; set; }

        public string CardCode { get; set; }

        public string CardName { get; set; }

        public string Address { get; set; }

        public string NumAtCard { get; set; }

        public decimal? VatSum { get; set; }

        public decimal? DiscPrcnt { get; set; }

        public decimal? DiscSum { get; set; }

        public string DocCur { get; set; }

        public decimal? DocRate { get; set; }

        public decimal? DocTotal { get; set; }
        
        public decimal? PaidToDate { get; set; }

        public string Ref1 { get; set; }

        public string Ref2 { get; set; }

        public string Comments { get; set; }

        public string JrnlMemo { get; set; }

        public int? SlpCode { get; set; }

        public short? TrnspCode { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? TaxDate { get; set; }

        public short? UserSign { get; set; }

        public decimal? TotalExpns { get; set; }

        public DateTime? ReqDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public int? OwnerCode { get; set; }

        public string Requester { get; set; }

        public string ReqName { get; set; }

        public short? Branch { get; set; }

        public short? Department { get; set; }

        public string Email { get; set; }

        public string Notify { get; set; }

        public Nullable<short> GroupNum { get; set; }
        public string PeyMethod { get; set; }

        public decimal? DpmAmnt { get; set; }

        public decimal? RoundDif { get; set; }

        public string Confirmed { get; set; }

        public int? BPLId { get; set; }
        public string BPLName { get; set; }

        public string DescConfirmed
        {
            get
            {
                switch (Confirmed)
                {
                    case "Y":
                        return "Yes";

                    case "N":
                        return "No";

                    default:
                        return Confirmed;

                }
            }
        }

        //Agregados

        public List<SalesInvoiceLineView> Lines { get; set; }
        public DocumentSAPCombo ListDocumentSAPCombo { get; set; }
        public DocumentAddress SIAddress { get; set; }
        public string OwnerName { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }

        //Added to JSON View
        public string DocDateJSONView
        {
            get { return this.DocDate != null ? this.DocDate.Value.ToShortDateString() : ""; }
        }
        public string DocDueDateJSONView
        {
            get { return this.DocDueDate != null ? this.DocDueDate.Value.ToShortDateString() : ""; }
        }
        public OpenQuantityStatus OpenQuantityStatus { get; set; }
    }
}
