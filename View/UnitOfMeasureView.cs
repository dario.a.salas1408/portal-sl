﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class UnitOfMeasureView
    {
        public int UomEntry { get; set; }
        public string UomCode { get; set; }
        public string UomName { get; set; }
        public int LineNum { get; set; }
        public decimal? BaseQty { get; set; }
        public decimal? AltQty { get; set; }
    }
}
