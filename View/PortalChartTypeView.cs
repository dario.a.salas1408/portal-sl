﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalChartTypeView
    {
        public int IdChartType { get; set; }
        
        public string ChartName { get; set; }
        
        public int ChartColumnNumber { get; set; }
    }
}
