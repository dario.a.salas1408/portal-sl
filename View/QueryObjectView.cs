﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class QueryObjectView
    {
        public string pQueryIdentifier { get; set; }

        public Dictionary<string, string> pQueryParams { get; set; }
    }
}
