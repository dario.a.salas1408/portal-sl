﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class SaleOrderLineView
    {
        public SaleOrderLineView()
        { 
            ListCurrency = new List<CurrencySAP>();
            Freight = new List<SaleOrderSAPLineFreight>();
            hasStock = false;
        }

        public SaleOrderLineView(int pLineNum, string pItemCode, string pDscription, decimal pPrice, decimal pQuantity)
        {
            ListCurrency = new List<CurrencySAP>();
            Freight = new List<SaleOrderSAPLineFreight>();
            hasStock = false;
            ItemCode = pItemCode;
            Dscription = pDscription;
            Price = pPrice;
            Quantity = pQuantity;
            PriceBefDi = pPrice;
            LineNum = pLineNum;
            Serials = new List<Serial>();
        }


        public int DocEntry { get; set; }
        public int LineNum { get; set; }
        public int? TargetType { get; set; }
        public int? TrgetEntry { get; set; }
        public string BaseRef { get; set; }
        public int? BaseType { get; set; }
        public int? BaseEntry { get; set; }
        public int? BaseLine { get; set; }
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public string SubCatNum { get; set; }
        public string AcctCode { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal PriceBefDi { get; set; }
        public decimal PriceAfDisc { get; set; }
        public string Currency { get; set; }
        public string WhsCode { get; set; }
        public string TaxCode { get; set; }
        public string Project { get; set; }
        public string OcrCode { get; set; }
        public string FreeTxt { get; set; }
        public Nullable<System.DateTime> ShipDate { get; set; }
        public List<CurrencySAP> ListCurrency { get; set; }
        public double RateGl { get; set; }
        public double RateLine { get; set; }
        public bool hasStock { get; set; }
        public string UomCode { get; set; }
        public int? UomEntry { get; set; }
        public int UgpEntry { get; set; }
        public string SerialBatch { get; set; }
        public string LineStatus { get; set; }
        //Agregados
        public GLAccountView GLAccount { get; set; }       
        //public string TaxCode { get; set; }     
        public decimal? VatPrcnt { get; set; }       
        public decimal? PriceAfVAT { get; set; }       
        public decimal? VatSum { get; set; }       
        //public decimal? VatSum { get; set; }       
        public decimal? GTotal { get; set; }
        public decimal? DiscPrcnt { get; set; }
        public string SpecPrice { get; set; }
        public string OcrCode2 { get; set; }
        public string CalcTax { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }
        public List<SaleOrderSAPLineFreight> Freight { get; set; }
        public List<UnitOfMeasure> UnitOfMeasureList { get; set; }
        public List<ItemPrices> ItemPrices { get; set; }
        public string Substitute { get; set; }
        public string TaxCodeAR { get; set; }
        public string UomGroupName { get; set; }
        public OpenQuantityStatus OpenQuantityStatus { get; set; }
        public short PaymentTerm { get; set; }
        public decimal OpenQty { get; set; }
        public List<Serial> Serials { get; set; }
        public string ManBtchNum { get; set; }
        public string ManSerNum { get; set; }
        public bool isWarranty { get; set; }
        public string fromWarrantyItem { get; set; }
        public string anoSelected { get; set; }
        public string TreeType { get; set; }
    }
}
