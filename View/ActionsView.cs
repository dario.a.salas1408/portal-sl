﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ActionsView
    {
        public int IdAction { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }

        public string InternalKey { get; set; }
    }
}
