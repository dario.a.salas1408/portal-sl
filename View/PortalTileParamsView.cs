﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class PortalTileParamsView
    {
        public PortalTileParamsView()
        {

        }

        public PortalTileParamsView(List<ListItem> pParamValueList)
        {
            ParamValueList = pParamValueList;
        }

        public PortalTileView PortalTile { get; set; }
        public CompanyView Company { get; set; }
        public List<UserGroupView> ListUserGroup { get; set; }
        public List<PortalTileUserGroupView> ListPortalTileUserGroup { get; set; }
        public List<ListItem> ParamValueList { get; set; }
    }
}
