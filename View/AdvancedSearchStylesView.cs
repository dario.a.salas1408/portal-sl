﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class AdvancedSearchStylesView
	{
		public string CodeModel { get; set; }
		public string NameModel { get; set; }
		public string SeasonModel { get; set; }
		public string GroupModel { get; set; }
		public string BrandModel { get; set; }
		public string CollectionModel { get; set; }
		public string SubCollectionModel { get; set; }
		public int Start { get; set; }
		public int Length { get; set; }
	}
}
