﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class HomeObjectView
    {
        public List<NewView> ListNews { get; set; }
        public List<AlertView> ListAlerts { get; set; }
        public List<UserDirectAccessView> ListUserDirectAccessView { get; set; }
        public List<DirectAccessView> ListDirectAccessView { get; set; }
        public List<PortalTileView> ListTileView { get; set; }
        public List<PortalTileTypeView> ListTileTypeView { get; set; }
        public List<AreaKeyView> ListAreaKeyView { get; set; }
        public List<PageView> ListPageView { get; set; }
    }
}
