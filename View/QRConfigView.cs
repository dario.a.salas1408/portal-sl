﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class QRConfigView
    {
        public int IdCompany { get; set; }
        public int IdQRConfig { get; set; }
        public string Separator { get; set; }
        public int? ItemCodePosition { get; set; }
        public int? DscriptionPosition { get; set; }
        public int? WhsCodePosition { get; set; }
        public int? OcrCodePosition { get; set; }
        public int? GLAccountPosition { get; set; }
        public int? FreeTxtPosition { get; set; }
        public int? QuantityPosition { get; set; }
        public int? CurrencyPosition { get; set; }
        public int? PricePosition { get; set; }
        public int? SerialPosition { get; set; }
        public int? BatchPosition { get; set; }
        public int? UomCodePosition { get; set; }
    }
}
