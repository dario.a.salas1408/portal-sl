﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class UserPageActionView
    {
        public int IdUser { get; set; }

        public int IdPage { get; set; }

        public int IdAction { get; set; }

        public int IdAreaKey { get; set; }

        public string InternalKey { get; set; }

        public string PageInternalKey { get; set; }
    }
}
