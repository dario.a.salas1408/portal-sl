﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSCrPathView
    {

        public ARGNSCrPathView()
        {
            ActivitiesList = new List<ARGNSCrPathActivitiesView>();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ProyCode { get; set; }
        public string U_Desc { get; set; }
        public string U_Model { get; set; }
        public string U_Status { get; set; }
        public string U_Planning { get; set; }
        public List<ARGNSCrPathActivitiesView> ActivitiesList { get; set; }
        public string ImageName { get; set; }

        //Descripciones
        public string SeasonDesc { get; set; }
        public string CollectionDesc { get; set; }
        public string ModelPicture { get; set; }
        public string ModelDesc { get; set; }
        

        //Sales Order Info
        public string SalesOrderNum { get; set; }
        public string SalesCardCode { get; set; }
        public string SalesCardName { get; set; }
        public DateTime? SalesPostingDate { get; set; }
        public DateTime? SalesDelivDate { get; set; }
        public decimal? SalesQty { get; set; }
    }
}
