﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ServiceCallComboView
    {

        public ServiceCallComboView()
        {
            ServicePriorityList = new List<ServicePriority>();
            ServiceOriginList = new List<ServiceOrigin>();
            ServiceProblemSubTypeList = new List<ServiceProblemSubType>();
            ServiceProblemTypeList = new List<ServiceProblemType>();
            CallTypeList = new List<CallType>();
            ServiceStatusList = new List<ServiceStatus>();
            TechnicianList = new List<EmployeeSAP>();
            HandledByList = new List<UserSAP>();
        }


        public List<ServicePriority> ServicePriorityList { get; set; }
        public List<ServiceOrigin> ServiceOriginList { get; set; }
        public List<ServiceProblemSubType> ServiceProblemSubTypeList { get; set; }
        public List<ServiceProblemType> ServiceProblemTypeList { get; set; }
        public List<CallType> CallTypeList { get; set; }
        public List<ServiceStatus> ServiceStatusList { get; set; }
        public List<EmployeeSAP> TechnicianList { get; set; }
        public List<UserSAP> HandledByList { get; set; }

        
    }
}
