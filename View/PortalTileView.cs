﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalTileView
    {
        public PortalTileView()
        {
            ListPortalTileType = new List<PortalTileTypeView>();
        }

        public int IdTile { get; set; }

        public string TileName { get; set; }

        public int? IdSAPQuery { get; set; }

        public string NameSAPQuery { get; set; }

        public int IdTileType { get; set; }

        public string UrlPage { get; set; }
        public string UrlName { get; set; }

        public int IdCompany { get; set; }

        public string HtmlText { get; set; }

        public short? VisualOrder { get; set; }

        public List<PortalTileTypeView> ListPortalTileType { get; set; }
        public List<PortalTileUserGroupView> PortalTileUserGroups { get; set; }
        public List<PortalTileParamView> PortalTileParams { get; set; }

        //Agregados para la view
        public string QueryResult { get; set; }
    }
}
