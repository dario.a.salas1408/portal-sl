﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class CompanyView
    {

        public CompanyView()
        {
            ServerTypeList = new List<ListItem>() { new ListItem { Value = "1", Text = "MSSQL", Selected = true }, new ListItem { Value = "4", Text = "MSSQL 2005" }, new ListItem { Value = "6", Text = "MSSQL 2008" }, new ListItem { Value = "7", Text = "MSSQL 2012" }, new ListItem { Value = "8", Text = "MSSQL 2014" }, new ListItem { Value = "10", Text = "MSSQL 2016" }, new ListItem { Value = "9", Text = "HANA" } };
            ListUserGroup = new List<ListItem>();
            ListBusinessPartnerSap = new List<ListItem>();
            ListSalesEmployeeSap = new List<ListItem>();
            LanguajeList = new List<ListItem>() {
                new ListItem { Value = "cs-CZ", Text = "cs-CZ"},
                new ListItem { Value = "de-DE", Text = "de-DE" },
                new ListItem { Value = "da-DK", Text = "da-DK" },
                new ListItem { Value = "el-GR", Text = "el-GR" },
                new ListItem { Value = "en-CY", Text = "en-CY" },
                new ListItem { Value = "en-GB", Text = "en-GB" },
                new ListItem { Value = "en-US", Text = "en-US", Selected = true },
                new ListItem { Value = "en-SG", Text = "en-SG" },
                new ListItem { Value = "es-AR", Text = "es-AR" },
                new ListItem { Value = "es-CO", Text = "es-CO" },
                new ListItem { Value = "es-ES", Text = "es-ES" },
                new ListItem { Value = "es-PA", Text = "es-PA" },
                new ListItem { Value = "fi-FI", Text = "fi-FI" },
                new ListItem { Value = "fr-FR", Text = "fr-FR" },
                new ListItem { Value = "he-IL", Text = "he-IL" },
                new ListItem { Value = "hu-HU", Text = "hu-HU" },
                new ListItem { Value = "it-IT", Text = "it-IT" },
                new ListItem { Value = "ja-JP", Text = "ja-JP" },
                new ListItem { Value = "ko-KR", Text = "ko-KR" },
                new ListItem { Value = "nl-NL", Text = "nl-NL" },
                new ListItem { Value = "no-NO", Text = "no-NO" },
                new ListItem { Value = "pl-PL", Text = "pl-PL" },
                new ListItem { Value = "pt-BR", Text = "pt-BR" },
                new ListItem { Value = "pt-PT", Text = "pt-PT" },
                new ListItem { Value = "ru-RU", Text = "ru-RU" },
                new ListItem { Value = "sk-SK", Text = "sk-SK" },
                new ListItem { Value = "sr-YU", Text = "sr-YU" },
                new ListItem { Value = "sv-SE", Text = "sv-SE" },
                new ListItem { Value = "xx-XX", Text = "xx-XX" },
                new ListItem { Value = "zh-CN", Text = "zh-CN" },
                new ListItem { Value = "zh-TW", Text = "zh-TW" }
            };
            CompanyStock = new List<CompanyStockView>();
        }

        public int IdCompany { get; set; }
        public int ServerType { get; set; }
        public string Server { get; set; }
        public string CompanyDB { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DbUserName { get; set; }
        public string DbPassword { get; set; }
        public bool UseTrusted { get; set; }
        public bool Active { get; set; }
        public bool UseCompanyUser { get; set; }
        public bool ItemInMultipleLines { get; set; }
        public bool UseHandheld { get; set; }
        public bool CheckBasketID { get; set; }
        public bool OnDemand { get; set; }
        public string ResultConnection { get; set; }
        public List<ListItem> ServerTypeList { get; set; }
        public UsersSettingView userSettings { get; set; }
        public bool CheckApparelInstallation { get; set; }
        public string LicenseServer { get; set; }
        public string SAPLanguaje { get; set; }
        public int PortNumber { get; set; }
        public List<CompanyStockView> CompanyStock { get; set; }
        public List<ListItem> LanguajeList { get; set; }
        public string SLSessionId { get; set; }
        public string SLRouteId { get; set; }
        public string DSSessionId { get; set; }
        public int? CodeType { get; set; }
        public string UrlHana { get; set; }
        public string UrlHanaGetQueryResult { get; set; }
        public string UrlSL { get; set; }
        public int IdUserConected { get; set; }
        public int? UserGroupId { get; set; }
        public short? VisualOrder { get; set; }
        public List<ListItem> ListUserGroup { get; set; }
        public List<ListItem> ListBusinessPartnerSap { get; set; }
        public List<ListItem> ListSalesEmployeeSap { get; set; }
        public CompanySAPConfig CompanySAPConfig { get; set; }
        [NotMapped]
        public int? IdBranchSelect { get; set; }

        public string FormatNumberWithDecimals(ARGNS.Util.Enums.Decimals pDecimalType, decimal pNumber)
        {
            short? mDecimalQty;
            string mResult = string.Empty;
            try
            {
                switch (pDecimalType)
                {
                    case ARGNS.Util.Enums.Decimals.Amount:
                        mDecimalQty = this.CompanySAPConfig.CompanySettingsSAP.SumDec;
                        break;
                    case ARGNS.Util.Enums.Decimals.DecimalInQuery:
                        mDecimalQty = this.CompanySAPConfig.CompanySettingsSAP.QueryDec;
                        break;
                    case ARGNS.Util.Enums.Decimals.Percent:
                        mDecimalQty = this.CompanySAPConfig.CompanySettingsSAP.PercentDec;
                        break;
                    case ARGNS.Util.Enums.Decimals.Price:
                        mDecimalQty = this.CompanySAPConfig.CompanySettingsSAP.PriceDec;
                        break;
                    case ARGNS.Util.Enums.Decimals.Quantity:
                        mDecimalQty = this.CompanySAPConfig.CompanySettingsSAP.QtyDec;
                        break;
                    case ARGNS.Util.Enums.Decimals.Rate:
                        mDecimalQty = this.CompanySAPConfig.CompanySettingsSAP.RateDec;
                        break;
                    case ARGNS.Util.Enums.Decimals.Unit:
                        mDecimalQty = this.CompanySAPConfig.CompanySettingsSAP.MeasureDec;
                        break;
                    default:
                        mDecimalQty = 2;
                        break;
                }
                mResult = String.Format(CultureInfo.InvariantCulture, String.Format("{{0:0.{0}}}", new string('0', (mDecimalQty != null ? mDecimalQty.Value : 2))), pNumber);

                return mResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
