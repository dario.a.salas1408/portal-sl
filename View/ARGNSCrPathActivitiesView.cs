﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSCrPathActivitiesView 
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string Object { get; set; }
        public int? LogInst { get; set; }
        public string U_PhaseId { get; set; }
        public string U_Desc { get; set; }
        public string U_Depart { get; set; }
        public string U_Role { get; set; }
        public string U_Manager { get; set; }
        public string U_User { get; set; }
        public string U_Bp { get; set; }
        public decimal? U_DelivDay { get; set; }
        public DateTime? U_SDate { get; set; }
        public DateTime? U_CDate { get; set; }
        public string U_ActSAP { get; set; }
        public string U_Priority { get; set; }
        public string U_Status { get; set; }
        public string U_Comment { get; set; }
        public string U_CreateAc { get; set; }
        public string U_Updated { get; set; }
        public DateTime? U_PlSDate { get; set; }
        public DateTime? U_PlCDate { get; set; }
        public decimal? U_LTime { get; set; }
        public decimal? U_PlanLead { get; set; }
        public short? U_JobId { get; set; }
        public string U_JobNum { get; set; }

        public bool Closed { get; set; }     
        public string LabelType { get; set; }
        public ActivitiesView ActivitySAP  { get; set;}
    }
}
