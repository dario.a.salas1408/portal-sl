﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class QueryCategorySAPView
    {
        public int CategoryId { get; set; }

        public string CatName { get; set; }

        public string PermMask { get; set; }

        public string DataSource { get; set; }

        public short? UserSign { get; set; }
    }
}
