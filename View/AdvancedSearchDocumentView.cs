﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class AdvancedSearchDocumentView
    {
        private string mCodeVendor, mRequester, mDate, mNro, mDocStatus, mOwnerCode, mSECode, mDocumentType, mDepartment, mCardCode, mCardName, mBPType, mPageKey, mItemCode, mItemName, mBPCatalogCode, mItemGroupCode, mSupplierCardCode, mColumnRadio, mPrchseItem, mSellItem, mInventoryItem, licTracNum;
        private bool mBPDocCheckBox, mClearTable, mCkCatalogueNum, mNoItemsWithZeroStockCk, mTilesSearchCk, mDoubleTilesSearchCk, mFirstDoubleSearch;
        private int mStart, mLength, mTileToModify, mIdCompany;
        private List<UDF_ARGNS> mMappedUdf;
        private List<BusinessPartnerView> mBPList;

        public string pCardCode { get { if (this.mCardCode == null) return ""; else return this.mCardCode; } set { this.mCardCode = value; } }
        public string pCardName { get { if (this.mCardName == null) return ""; else return this.mCardName; } set { this.mCardName = value; } }
        public string pBPType { get { if (this.mBPType == null) return ""; else return this.mBPType; } set { this.mBPType = value; } }
        public string pCodeVendor { get { if (this.mCodeVendor == null) return ""; else return this.mCodeVendor; } set { this.mCodeVendor = value; } }
        public string pRequester { get { if (this.mRequester == null) return ""; else return this.mRequester; } set { this.mRequester = value; } }
        public string pDocumentType { get { if (this.mDocumentType == null) return ""; else return this.mDocumentType; } set { this.mDocumentType = value; } }
        public string pDate { get { if (this.mDate == null) return ""; else return this.mDate; } set { this.mDate = value; } }
        public string pNro { get { if (this.mNro == null) return ""; else return this.mNro; } set { this.mNro = value; } }
        public string pDocStatus { get { if (this.mDocStatus == null) return ""; else return this.mDocStatus; } set { this.mDocStatus = value; } }
        public string pOwnerCode { get { if (this.mOwnerCode == null) return ""; else return this.mOwnerCode; } set { this.mOwnerCode = value; } }
        public string pSECode { get { if (this.mSECode == null) return ""; else return this.mSECode; } set { this.mSECode = value; } }
        public string pDepartment { get { if (this.mDepartment == null) return ""; else return this.mDepartment; } set { this.mDepartment = value; } }
        public string pPageKey { get { if (this.mPageKey == null) return ""; else return this.mPageKey; } set { this.mPageKey = value; } }
        public string pItemCode { get { if (this.mItemCode == null) return ""; else return this.mItemCode; } set { this.mItemCode = value; } }
        public string pItemName { get { if (this.mItemName == null) return ""; else return this.mItemName; } set { this.mItemName = value; } }
        public string pPrchseItem { get { if (this.mPrchseItem == null) return ""; else return this.mPrchseItem; } set { this.mPrchseItem = value; } }
        public string pSellItem { get { if (this.mSellItem == null) return ""; else return this.mSellItem; } set { this.mSellItem = value; } }
        public string pInventoryItem { get { if (this.mInventoryItem == null) return ""; else return this.mInventoryItem; } set { this.mInventoryItem = value; } }
        public string pItemWithStock { get; set; }
        public string pItemGroup { get; set; }
        public string pBPCatalogCode { get { if (this.mBPCatalogCode == null) return ""; else return this.mBPCatalogCode; } set { this.mBPCatalogCode = value; } }
        public List<UDF_ARGNS> pMappedUdf { get { return this.mMappedUdf; } set { this.mMappedUdf = value; } }
        public List<BusinessPartnerView> pBPList { get { return this.mBPList; } set { this.mBPList = value; } }
        public bool pBPDocCheckBox { get { return this.mBPDocCheckBox; } set { this.mBPDocCheckBox = value; } }
        public bool pClearTable { get { return this.mClearTable; } set { this.mClearTable = value; } }
        public bool pCkCatalogueNum { get { return this.mCkCatalogueNum; } set { this.mCkCatalogueNum = value; } }
        public string pItemGroupCode { get { if (this.mItemGroupCode == null) return ""; else return this.mItemGroupCode; } set { this.mItemGroupCode = value; } }
        public string pSupplierCardCode { get { if (this.mSupplierCardCode == null) return ""; else return this.mSupplierCardCode; } set { this.mSupplierCardCode = value; } }
        public bool pNoItemsWithZeroStockCk { get { return this.mNoItemsWithZeroStockCk; } set { this.mNoItemsWithZeroStockCk = value; } }
        public bool pTilesSearchCk { get { return this.mTilesSearchCk; } set { this.mTilesSearchCk = value; } }
        public bool pDoubleTilesSearchCk { get { return this.mDoubleTilesSearchCk; } set { this.mDoubleTilesSearchCk = value; } }
        public bool pFirstDoubleSearch { get { return this.mFirstDoubleSearch; } set { this.mFirstDoubleSearch = value; } }
        public string pColumnRadio { get { if (this.mColumnRadio == null) return ""; else return this.mColumnRadio; } set { this.mColumnRadio = value; } }
        public int pStart { get { return this.mStart; } set { this.mStart = value; } }
        public int pLength { get { return this.mLength; } set { this.mLength = value; } }
        public int pTileToModify { get { return this.mTileToModify; } set { this.mTileToModify = value; } }
        public int pIdCompany { get { return this.mIdCompany; } set { this.mIdCompany = value; } }
        public string LicTracNum { get { if (this.licTracNum == null) return ""; else return this.licTracNum; } set { this.licTracNum = value; } }

    }
}
