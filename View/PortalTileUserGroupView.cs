﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalTileUserGroupView
    {
        public int Id { get; set; }

        public int? UserGroup_Id { get; set; }
    }
}
