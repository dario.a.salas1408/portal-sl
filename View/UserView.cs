﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Model;
using ARGNS.Model.Implementations;
using AutoMapper;
using System.Web.UI.WebControls;

namespace ARGNS.View
{

    public class UserView
    {
        public UserView()
        {
            CompanyConnect = new CompanyView();
            ListUserSap = new List<ListItem>();
            ListBusinessPartnerSap = new List<ListItem>();
            ListSalesEmployeeSap = new List<ListItem>();
            ListPage = new List<PageView>();
            WorkOffline = false;
            IsValidUser = true;
            ListArea = new List<AreaKeyView>();
            ListRoles = new List<ListItem>();
            AccessUser = new string[1];
            AccessUser[0] = "";
            IsAdmin = false;
            ListUserPageAction = new List<UserPageActionView>();
            ListDistributionRule = new List<ListItem>();
            ListWarehouse = new List<ListItem>();
            ListSalesTaxCode = new List<ListItem>();
            ListPurchaseTaxCode = new List<ListItem>();
            ListUserGroup = new List<ListItem>();
            ListCostPriceList = new List<ListItem>();
            ListBPGroup = new List<ListItem>();
        }

        public int IdUser { get; set; }
        public string Name { get; set; }
        public List<Authorization> Authorization { get; set; }
        public string Password { get; set; }
        public short UserIdSAP { get; set; }
        public string UserNameSAP { get; set; }
        public string UserPasswordSAP { get; set; }
        public List<CompanyView> Companies { get; set; }
        public bool Active { get; set; }
        public CompanyView CompanyConnect { get; set; }
        public List<ListItem> ListUserSap { get; set; }
        public List<ListItem> ListBusinessPartnerSap { get; set; }
        public List<ListItem> ListSalesEmployeeSap { get; set; }
        public List<ListItem> ListDistributionRule { get; set; }
        public List<ListItem> ListWarehouse { get; set; }
        public List<ListItem> ListSalesTaxCode { get; set; }
        public List<ListItem> ListPurchaseTaxCode { get; set; }
        public List<ListItem> ListUserGroup { get; set; }
        public List<ListItem> ListCostPriceList { get; set; }
        public List<ListItem> ListBPGroup { get; set; }
        public int IdSelector { get; set; }
        public bool WorkOffline { get; set; }
        public List<PageView> ListPage { get; set; }
        public bool IsValidUser { get; set; }
        public List<AreaKeyView> ListArea { get; set; }
        public List<ListItem> ListRoles { get; set; }
        public int IdRol { get; set; }
        public string[] AccessUser { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsCustomer { get; set; }
        public bool IsSalesEmployee { get; set; }
        public bool IsUser { get; set; }
        public List<UserPageActionView> ListUserPageAction { get; set; }
        public string UserCodeSAP { get; set; }
        public int? SECode { get; set; }
        public string BPCode { get; set; }
        public string DftWhs { get; set; }
        public string DftDistRule { get; set; }
        public string SalesTaxCodeDef { get; set; }
        public string PurchaseTaxCodeDef { get; set; }
        public int? CostPriceList { get; set; }
        public int? BPGroupId { get; set; }
        public int? UserGroupId { get; set; }
        public bool UseBranche { get; set; }

        //Solo utilizados a nivel de UserView
        public string NewPassword { get; set; }
        public string RepNewPassword { get; set; }
        public UserGroupView UserGroupObj { get; set; }
        


    }
}
