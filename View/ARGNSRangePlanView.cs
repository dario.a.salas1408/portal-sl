﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class ARGNSRangePlanView
    {
        public ARGNSRangePlanView()
        {
            this.RangePlanCombo = new RangePlanCombo();
            this.Lines = new List<ARGNSRangePlanDetailView>();
        }

        public string Code { get; set; }

        public string Name { get; set; }

        public int DocEntry { get; set; }

        public string Canceled { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        public string DataSource { get; set; }

        public string U_RangeCode { get; set; }

        public string U_RangeDesc { get; set; }

        public string U_Coll { get; set; }

        public string U_Season { get; set; }

        public DateTime? U_SDate { get; set; }

        public string U_RPEmpl { get; set; }

        public string U_Active { get; set; }

        public DateTime? U_DTo { get; set; }

        public string U_PrjCode { get; set; }

        public RangePlanCombo RangePlanCombo { get; set; }

        public List<ARGNSRangePlanDetailView> Lines { get; set; }
    }
}
