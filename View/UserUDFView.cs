﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class UserUDFView
    {
        public int IdUDF { get; set; }

        public int IdUser { get; set; }

        public int IdCompany { get; set; }

        public string Document { get; set; }

        public string UDFName { get; set; }

        public string UDFDesc { get; set; }

        public short FieldID { get; set; }

        public string TypeID { get; set; }
    }
}
