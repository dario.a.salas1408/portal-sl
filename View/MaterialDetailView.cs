﻿using ARGNS.Model.Implementations.PDM.MaterialDetails;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class MaterialDetailView
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public int DocEntry { get; set; }

        public string U_ModCode { get; set; }

        public string U_Description { get; set; }

        //Material Details Lines
        public List<ARGNSMDFabric> ListFabric { get; set; }
        public List<ARGNSMDAccess> ListAccessories { get; set; }
        public List<ARGNSMDCareInst> ListCareInstructions { get; set; }
        public List<ARGNSMDLabelling> ListLabelling { get; set; }
        public List<ARGNSMDPackaging> ListPackaging { get; set; }
        public List<ARGNSMDFootwearMaterial> ListFootwearMaterial { get; set; }
        public List<ARGNSMDFootwearDetails> ListFootwearDetail { get; set; }
        public List<ARGNSMDFootwearPackaging> ListFootwearPackaging { get; set; }
        public List<ARGNSMDFootwearPictogram> ListFootwearPictogram { get; set; }
        public List<ARGNSMDFootwearSRange> ListFootwearSizeRange { get; set; }
        public List<ARGNSMDFootwearAccess> ListFootwearAccessories { get; set; }

        //Agregado para mostrar la imagen
        public string U_Pic { get; set; }
        public MaterialDetailsUDF mMaterialDetailsUDF { get; set; }
    }
}
