﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace ARGNS.View
{
    public class LogoView
    {
        public LogoView()
        {
            ListType = new List<ListItem>();
            ListType.Add(new ListItem("Embroidery", "Embroidery"));
            ListType.Add(new ListItem("Transfer", "Transfer"));
            ListType.Add(new ListItem("Screen Print", "Screen Print"));
            ListType.Add(new ListItem("Tax Tab", "Tax Tab"));
            ListType.Add(new ListItem("Sew on Badge", "Sew on Badge"));
        }

        public string Code { get; set; } 
        public string Name { get; set; } 
        public int DocEntry { get; set; } 
        public string Canceled { get; set; } 
        public string Object { get; set; }        
        public int? LogInst { get; set; } 
        public int? UserSign { get; set; } 
        public string Transfered { get; set; } 
        public DateTime? CreateDate { get; set; } 
        public int CreateTime { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int UpdateTime { get; set; } 
        public string DataSource { get; set; }
        public string U_CodeLogo { get; set; }        
        public string U_Desc { get; set; }        
        public string U_LType { get; set; } 
        public DateTime? U_LDate { get; set; } 
        public string U_Approved { get; set; } 
        public string U_LImagen { get; set; } 
        public string U_LArchive { get; set; }        
        public string U_StichCount { get; set; } 
        public string U_HoopSize { get; set; } 
        public string U_LWidth { get; set; } 
        public string U_LHeight { get; set; }
        public string U_Comments { get; set; }

        //Agregados en el view
        public List<ListItem> ListType { get; set; }
    }
}
