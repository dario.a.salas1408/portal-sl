﻿using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class CRPageMapView
    {
        public CRPageMapView()
        {
            CompanyConn = new CompanyConn();
            FromDB = "S";
        }
        public int IdICRPageMap { get; set; }
        public int IdCompany { get; set; }
        public int IdPage { get; set; }
        public CompanyConn CompanyConn { get; set; }
        public Page Page { get; set; }
        public string CRName { get; set; }
        public string FromDB { get; set; }
    }
}
