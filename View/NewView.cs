﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class NewView : INews
    {
        public int Id { get; set; }
        public string Head { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string ImageName { get; set; }
        public DateTime Fecha { get; set; }
        public bool Active { get; set; }

        //Agregados View
        public bool ImgUploadChange { get; set; }
    }
}
