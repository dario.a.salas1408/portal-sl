﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class UDF_SAPView
    {
        public string TableID { get; set; }

        public short FieldID { get; set; }

        public string AliasID { get; set; }

        public string Descr { get; set; }

        public string TypeID { get; set; }

        public string EditType { get; set; }

        public short? SizeID { get; set; }

        public short? EditSize { get; set; }

        public string Dflt { get; set; }

        public string NotNull { get; set; }

        public string Sys { get; set; }

        public string RTable { get; set; }
    }
}
