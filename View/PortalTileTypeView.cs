﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.View
{
    public class PortalTileTypeView
    {
        public PortalTileTypeView()
        {
            PortalTiles = new List<PortalTileView>();
        }
        
        public int IdTileType { get; set; }

        public string TypeName { get; set; }

        public virtual List<PortalTileView> PortalTiles { get; set; }
    }
}
