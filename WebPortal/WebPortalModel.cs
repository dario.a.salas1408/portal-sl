namespace ARGNS.Model
{
    using ARGNS.Model.Implementations;
    using Implementations.Portal;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class WebPortalModel : DbContext
    {

        public WebPortalModel()
            : base("name=DbWebPortal")
        {
        }

        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Alert> Alerts { get; set; }
        public virtual DbSet<CompanyConn> Companies { get; set; }
        public virtual DbSet<Authorization> Authorizations { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }
        public virtual DbSet<UsersSetting> UsersSettings { get; set; }
        public virtual DbSet<AreaKey> AreaKeys { get; set; }
        public virtual DbSet<Actions> Actions { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserPageAction> UserPageActions { get; set; }
        public virtual DbSet<RolPageAction> RolPageActions { get; set; }
        public virtual DbSet<UserArea> UserAreas { get; set; }
        public virtual DbSet<CompanyStock> CompanyStock { get; set; }
        public virtual DbSet<CRPageMap> CRPageMap { get; set; }
        public virtual DbSet<QRConfig> QRConfig { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<UserUDF> UserUDFs { get; set; }
        public virtual DbSet<SAPObject> SAPObjects { get; set; }
        public virtual DbSet<QueryManagerItem> QueryManagerItems { get; set; }
        public virtual DbSet<InfoPortal> InfoPortals { get; set; }
        public virtual DbSet<QuickOrder> QuickOrder { get; set; }
        public virtual DbSet<PortalChart> PortalCharts { get; set; }
        public virtual DbSet<PortalChartType> PortalChartTypes { get; set; }
        public virtual DbSet<PortalChartColumn> PortalChartColumns { get; set; }
        public virtual DbSet<WarehousePortal> WarehousesPortal { get; set; }
        public virtual DbSet<DirectAccess> DirectAccesses { get; set; }
        public virtual DbSet<UserDirectAccess> UserDirectAccesses { get; set; }
        public virtual DbSet<PortalChartUserGroup> PortalChartUserGroups { get; set; }
        public virtual DbSet<PortalChartParam> PortalChartParams { get; set; }
        public virtual DbSet<PortalTile> PortalTiles { get; set; }
        public virtual DbSet<PortalTileParam> PortalTileParams { get; set; }
        public virtual DbSet<PortalTileType> PortalTileTypes { get; set; }
        public virtual DbSet<PortalTileUserGroup> PortalTileUserGroups { get; set; }
        public virtual DbSet<BPGroup> BPGroups { get; set; }
        public virtual DbSet<BPGroupLine> BPGroupLines { get; set; }
        public virtual DbSet<PortalChartParamsType> PortalChartParamsTypes { get; set; }
        public virtual DbSet<PortalChartParamsValueType> PortalChartParamsValueTypes { get; set; }
        public virtual DbSet<LocalDrafts> LocalDrafts { get; set; }
        public virtual DbSet<LocalDraftsLine> LocalDraftsLines { get; set; }
        public virtual DbSet<Folio> Folios { get; set; }
        public virtual DbSet<UserBranch> UserBranches { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PortalChart>()
                .HasMany(e => e.PortalChartUserGroups)
                .WithOptional(e => e.PortalChart)
                .HasForeignKey(e => e.PortalChart_IdChart);

            modelBuilder.Entity<UserGroup>()
                .Property(e => e.JSName)
                .IsUnicode(false);

            modelBuilder.Entity<UserGroup>()
                .HasMany(e => e.PortalChartUserGroups)
                .WithOptional(e => e.UserGroup)
                .HasForeignKey(e => e.UserGroup_Id);

            modelBuilder.Entity<Actions>()
                .Property(e => e.Description);

            modelBuilder.Entity<LocalDrafts>()
                .HasMany(e => e.Lines)
                .WithRequired(e => e.Header)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Actions>()
                .HasMany(e => e.UserPageActions)
                .WithRequired(e => e.Action)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Actions>()
               .HasMany(e => e.Pages)
               .WithMany(e => e.Actions)
               .Map(m => m.ToTable("PageActions").MapLeftKey("IdAction").MapRightKey("IdPage"));

            modelBuilder.Entity<CompanyConn>()
                .HasMany(e => e.UsersSettings)
                .WithRequired(e => e.CompanyConn)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UsersSettings)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyConn>()
                .HasMany(e => e.CRPageMaps)
                .WithRequired(e => e.CompanyConn)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Page>()
                .HasMany(e => e.CRPageMaps)
                .WithRequired(e => e.Page)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserPageActions)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Authorization)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_IdUser);                

            modelBuilder.Entity<AreaKey>()
                .Property(e => e.Area);

            modelBuilder.Entity<AreaKey>()
                .Property(e => e.KeyNumber);

            modelBuilder.Entity<Page>()
               .Property(e => e.Description);

            modelBuilder.Entity<Page>()
                .HasMany(e => e.RolPageActions)
                .WithRequired(e => e.Page)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Page>()
                .HasMany(e => e.UserPageActions)
                .WithRequired(e => e.Page)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Role>()
                .Property(e => e.Description);


            modelBuilder.Entity<Role>()
                .HasMany(e => e.RolPageActions)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyConn>()
               .HasMany(e => e.CompanyStock)
               .WithRequired(e => e.CompanyConns)
               .WillCascadeOnDelete(false);

           

        }
    }


}