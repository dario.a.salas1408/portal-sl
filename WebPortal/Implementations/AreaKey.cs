﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ARGNS.Model.Implementations
{
	public partial class AreaKey
    {
         public AreaKey()
        {
            UserAreas = new HashSet<UserArea>();
            Pages = new HashSet<Page>();
        }

        [Key]
        public int IdAreaKeys { get; set; }
        [Required]
        [StringLength(20)]
        public string Area { get; set; }
        [Required]
        [StringLength(500)]
        public string KeyNumber { get; set; }
        public string Controller { get; set; }
        [StringLength(150)]
        public string Action { get; set; }
        [StringLength(150)]
        public string KeyResource { get; set; }
        [StringLength(500)]
        public virtual ICollection<UserArea> UserAreas { get; set; }
        public virtual ICollection<Page> Pages { get; set; }

    }
}