﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{
	public class QueryManagerItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdQuery { get; set; }

        public int IdCompany { get; set; }

        public string QueryIdentifier { get; set; }

        public string SelectField { get; set; }

        public string FromField { get; set; }

        public string WhereField { get; set; }

        public string OrderByField { get; set; }

        public string GroupByField { get; set; }

        public bool Active { get; set; }

        public virtual CompanyConn Company { get; set; }
    }
}
