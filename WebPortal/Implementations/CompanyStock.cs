using ARGNS.Model.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{


	[Table("CompanyStock")]
    public partial class CompanyStock : ICompanyStock
    {
       
        [Key]
        public int IdCompanyStock { get; set; }

        public int IdCompany { get; set; }

        public bool ValidateStock { get; set; }

        [Required]
        [StringLength(200)]
        public string Formula { get; set; }

        public virtual CompanyConn CompanyConns { get; set; }
    }
}
