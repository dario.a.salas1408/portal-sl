﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ARGNS.Model.Implementations
{
	public partial class Role
    {
        public Role()
        {
            RolPageActions = new HashSet<RolPageAction>();
            Users = new HashSet<User>();
        }

        [Key]
        public int IdRol { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<RolPageAction> RolPageActions { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
