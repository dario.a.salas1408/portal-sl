﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;

namespace ARGNS.Model.Implementations
{
	public class User : IUser
    {
        public User()
        {
            //UserGroup = new List<UserGroup>();
            Authorization = new List<Authorization>();
            UsersSettings = new HashSet<UsersSetting>();
            UserPageActions = new HashSet<UserPageAction>();
            UserAreas = new HashSet<UserArea>();
        }

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdUser { get; set; }
        public int? IdRol { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        //public List<UserGroup> UserGroup { get; set; }
        public List<Authorization> Authorization { get; set; }
        public string Password { get; set; }
        public short UserIdSAP { get; set; }
        public string UserPasswordSAP { get; set; }
        public virtual ICollection<UsersSetting> UsersSettings { get; set; }
        public bool Active { get; set; }
        public virtual ICollection<UserPageAction> UserPageActions { get; set; }
        public virtual Role Roles { get; set; }
        public virtual ICollection<UserArea> UserAreas { get; set; }
        public virtual ICollection<QuickOrder> QuickOrders { get; set; }
    }
}
