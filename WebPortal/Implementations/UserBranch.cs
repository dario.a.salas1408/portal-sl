﻿using ARGNS.Model.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{
    public class UserBranch : IUserBranch
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdUserBranches { get; set; }
        public int IdUser { get; set; }
        public int IdCompany { get; set; }
        public int IdBranchSAP { get; set; }
        [StringLength(100)]
        public string BranchName { get; set; }
        public virtual User User { get; set; }
        public virtual CompanyConn CompanyConn { get; set; }
    }
}
