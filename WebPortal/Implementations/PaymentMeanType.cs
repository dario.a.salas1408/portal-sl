﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations
{
    public class PaymentMeanType: IPaymentMeanType
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string U_type { get; set; }
    }
}
