using ARGNS.Model.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{


	public partial class UserArea : IUserArea
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdAreaKeys { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdUser { get; set; }

        [StringLength(500)]
        public string KeyAdd { get; set; }

        public virtual AreaKey AreaKey { get; set; }

        public virtual User User { get; set; }
    }
}
