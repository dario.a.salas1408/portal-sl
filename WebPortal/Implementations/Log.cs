﻿using ARGNS.Model.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{
	public class Log : ILog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLog { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdUser { get; set; }
        [StringLength(250)]
        public string strClave { get; set; }
        [StringLength(100)]
        public string Tipo { get; set; }
        [Column(TypeName = "Text")]
        public string strLog { get; set; }
        public DateTime dtLog { get; set; }
        public virtual User User { get; set; }
    }
}
