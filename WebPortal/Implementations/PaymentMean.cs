﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations
{
    public class PaymentMean : IPaymentMean
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string U_BranchCode { get; set; }
        public decimal? U_AmountRule { get; set; }
        public decimal? U_Surcharge { get; set; }
        public string U_Type { get; set; }
        public int? U_Quotas { get; set; }
    }
}
