﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ActivityStatusSAP
    {
        public ActivityStatusSAP()
        {

        }

        public ActivityStatusSAP(int statusID, string name)
        {
            this.statusID = statusID;
            this.name = name;
        }

        public int statusID { get; set; }

        public string name { get; set; }

        public string descriptio { get; set; }

        public string Locked { get; set; }
    }
}
