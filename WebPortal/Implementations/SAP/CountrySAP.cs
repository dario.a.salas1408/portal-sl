﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class CountrySAP : ICountrySAP
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
