﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class StageSAP : IStageSAP
    {
        public int WstCode { get; set; }

        public string Name { get; set; }
    }
}
