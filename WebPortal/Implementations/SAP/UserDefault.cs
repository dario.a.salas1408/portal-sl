﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class UserDefault : IUserDefault
    {
        public string Code { get; set; }
        public string DfltsGroup { get; set; }
        public string Warehouse { get; set; }
        public string USER_CODE { get; set; }
    }
}
