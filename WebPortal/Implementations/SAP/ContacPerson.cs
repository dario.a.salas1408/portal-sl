﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ContacPerson : IContacPerson
    {
        public int CrtctCode { get; set; }     
        public string CardCode { get; set; }
        public string Name { get; set; }

        //Agregue este campo porque esta mal mapeado
        public int CntctCode { get; set; }
        
    }
}
