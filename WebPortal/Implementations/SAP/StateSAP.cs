﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class StateSAP : IStateSAP
    {
        public string Code { get; set; }

        public string Country { get; set; }

        public string Name { get; set; }
    }
}
