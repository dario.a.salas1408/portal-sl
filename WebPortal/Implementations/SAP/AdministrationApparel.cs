﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class AdministrationApparel : IAdministrationApparel
    {
        public string Code { get; set; }

        public string U_SBODec { get; set; }
        public short? U_BOMDSPL { get; set; }
    }
}
