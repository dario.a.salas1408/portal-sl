﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
    public class Warehouse : IWarehouse
    {
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
        public int? BPLid { get; set; }
    }
}
