﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class PurchaseRequestCombo : IPurchaseRequestCombo
    {
        public PurchaseRequestCombo() 
        {
            DepartmentList = new List<Department>();
            CurrencyList = new List<CurrencySAP>();
            BranchList = new List<Branch>();
            RatesList = new List<RatesSystem>();

        }

        public List<Department> DepartmentList { get; set; }
        public List<CurrencySAP> CurrencyList { get; set; }
        public List<Branch> BranchList { get; set; }
        public List<RatesSystem> RatesList { get; set; }
    }
}
