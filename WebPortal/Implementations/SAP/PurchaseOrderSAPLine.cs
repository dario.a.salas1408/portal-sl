﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class PurchaseOrderSAPLine : IDocumentLine, IPurchaseOrderSAPLine
    {
        public int DocEntry { get; set; }
        public int LineNum { get; set; }
        public int? TargetType { get; set; }
        public int? TrgetEntry { get; set; }
        public string BaseRef { get; set; }
        public int? BaseType { get; set; }
        public int? BaseEntry { get; set; }
        public int? BaseLine { get; set; }
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public string SubCatNum { get; set; }
        public string AcctCode { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<decimal> PriceBefDi { get; set; }
        public string Currency { get; set; }
        public string WhsCode { get; set; }
        public string TaxCode { get; set; }
        public string Project { get; set; }
        public string OcrCode { get; set; }
        public string FreeTxt { get; set; }
        public Nullable<System.DateTime> ShipDate { get; set; }
        public string UomCode { get; set; }     
        public decimal? VatPrcnt { get; set; }     
        public decimal? PriceAfVAT { get; set; }       
        public decimal? VatSum { get; set; }      
        public decimal? GTotal { get; set; }       
        public decimal? DiscPrcnt { get; set; }
        public string LineStatus { get; set; }

        //Agregados
        public GLAccountSAP GLAccount { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }
        public List<UnitOfMeasure> UnitOfMeasureList { get; set; }
    }
}
