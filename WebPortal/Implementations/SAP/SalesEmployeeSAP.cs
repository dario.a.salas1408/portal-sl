﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class SalesEmployeeSAP : ISalesEmployeeSAP
    {
        public int SlpCode { get; set; }

        public string SlpName { get; set; }

    }
}
