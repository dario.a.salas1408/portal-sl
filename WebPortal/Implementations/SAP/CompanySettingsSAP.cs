﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class CompanySettingsSAP
    {
        public int Code { get; set; }
        public string TimeFormat { get; set; }
        public string DateFormat { get; set; }
        public string DateSep { get; set; }
        public short? SumDec { get; set; } //Amounts
        public short? PriceDec { get; set; } //Prices
        public short? RateDec { get; set; } //Rates
        public short? QtyDec { get; set; } //Quantities
        public short? PercentDec { get; set; } //Percents
        public short? MeasureDec { get; set; } //Units
        public short? QueryDec { get; set; } //Decimals in query
        public string DecSep { get; set; } //Decimal Separator
        public string CompnyName { get; set; }
        public string Country { get; set; }
        public string DfltWhs { get; set; }
    }
}
