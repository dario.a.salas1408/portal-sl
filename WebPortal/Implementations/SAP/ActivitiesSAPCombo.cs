﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ActivitiesSAPCombo : IActivitiesSAPCombo
    {

        public ActivitiesSAPCombo()
        {
            ActivityTypeList = new List<ActivityTypeSAP>();
            ActivityPriorityList = new List<ActivityPriority>();
            ActivityActionList = new List<ActivityAction>();
            ActivityUserTypeList = new List<ActivityUserType>();
            ActivityUserList = new List<ActivityUser>();
            ActivityStatusList = new List<ActivityStatusSAP>();
        }

        public List<ActivityTypeSAP> ActivityTypeList { get; set; }
        public List<ActivityPriority> ActivityPriorityList { get; set; }
        public List<ActivityAction> ActivityActionList { get; set; }
        public List<ActivityUserType> ActivityUserTypeList { get; set; }
        public List<ActivityUser> ActivityUserList { get; set; }
        public List<ActivityStatusSAP> ActivityStatusList { get; set; }

    }
}
