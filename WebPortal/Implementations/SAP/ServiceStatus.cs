﻿namespace ARGNS.Model.Implementations.SAP
{
	public class ServiceStatus
    {
        public ServiceStatus()
        {

        }

        public ServiceStatus(short pStatusID, string pName)
        {
            statusID = pStatusID;
            Name = pName;
        }

        public short statusID { get; set; }      
        public string Name { get; set; }       
        public string Descriptio { get; set; } 
        public string Locked { get; set; }
    }
}
