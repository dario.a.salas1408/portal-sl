﻿using System;

namespace ARGNS.Model.Implementations.SAP
{
	public class SAPLogs
    {
        public int Id { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ChangedField { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

    }
}
