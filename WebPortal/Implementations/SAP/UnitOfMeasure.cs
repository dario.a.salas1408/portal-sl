﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class UnitOfMeasure : IUnitOfMeasure
    {
       public  int UomEntry { get; set; }
       public string UomCode { get; set; }
       public string UomName { get; set; }
       public int LineNum { get; set; }
       public decimal? BaseQty { get; set; }
       public decimal? AltQty { get; set; }
    }
}
