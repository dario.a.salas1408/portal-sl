﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class EmployeeSAP : IEmployeeSAP
    {
        public EmployeeSAP()
        {
                
        }

        public EmployeeSAP(int empID, string lastName, string firstName)
        {
            this.empID = empID;
            this.lastName = lastName;
            this.firstName = firstName;
        }

        public int empID { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string completeName { get; set; }
        public string getName { 
            get { return this.firstName + " " +  this.lastName; } 
        }
    }
}
