﻿namespace ARGNS.Model.Implementations.SAP
{
	public class RolSAP
    {
        public int typeID { get; set; }

        public string name { get; set; }

        public string descriptio { get; set; }

        public string locked { get; set; }
    }
}
