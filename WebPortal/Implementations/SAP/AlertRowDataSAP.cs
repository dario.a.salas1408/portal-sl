﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class AlertRowDataSAP
    {
        public int Code { get; set; }

        public int Location { get; set; }

        public int Line { get; set; }

        public string Value { get; set; }

        public string ObjType { get; set; }

        public string KeyStr { get; set; }
    }
}
