﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.SAP
{
	public class SaleOrderSAP : IDocument, ISaleOrderSAP
    {
        public SaleOrderSAP()
        {
            Lines = new List<SaleOrderSAPLine>();
            ListSystemRates = new List<RatesSystem>();
            SAPDocSettings = new DocumentSettingsSAP();
            ListFreight = new List<Freight>();
            Employee = new EmployeeSAP();
            DistrRuleSAP = new List<DistrRuleSAP>();
            Warehouse = new List<Warehouse>();
            GLAccountSAP = new List<GLAccountSAP>();
            AttachmentList = new List<AttachmentSAP>();
            SOAddress = new DocumentAddress();
            PaymentMeanOrder = new List<PaymentMeanOrder>();
        }
        public int DocEntry { get; set; }
        public int DocNum { get; set; }
        public string DocType { get; set; }
        public Nullable<System.DateTime> DocDate { get; set; }
        public Nullable<System.DateTime> DocDueDate { get; set; }
        public Nullable<System.DateTime> TaxDate { get; set; }
        public Nullable<System.DateTime> CancelDate { get; set; }
        public Nullable<System.DateTime> ReqDate { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string Project { get; set; }
        public string RevisionPo { get; set; }
        public string SummryType { get; set; }
        public string NumAtCard { get; set; }
        public Nullable<int> SlpCode { get; set; }
        public Nullable<int> OwnerCode { get; set; }
        public string DocCur { get; set; }
        public Nullable<decimal> DocRate { get; set; }
        public Nullable<int> CntctCode { get; set; }
        public Nullable<short> GroupNum { get; set; }
        public string PeyMethod { get; set; }
        public Nullable<decimal> DiscPrcnt { get; set; }
        public Nullable<short> TrnspCode { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public bool Draft { get; set; }
        public string ObjType { get; set; }
        public string DocStatus { get; set; }
        public string Comments { get; set; }
        public string JrnlMemo { get; set; }
        public string FatherType { get; set; }
        public string ShipToCode { get; set; }
        public string PayToCode { get; set; }
        public int? AtcEntry { get; set; }
        public decimal? TotalExpns { get; set; }
        public List<SaleOrderSAPLine> Lines { get; set; }
        public DocumentSAPCombo ListDocumentSAPCombo { get; set; }
        public string LocalCurrency { get; set; }
        public string SystemCurrency { get; set; }
        public string RateCurrency { get; set; }
        public List<RatesSystem> ListSystemRates { get; set; }
        public decimal? DocTotal { get; set; }
        public string Confirmed { get; set; }
        public CompanyAddress companyAddress { get; set; }
        public DocumentAddress SOAddress { get; set; }
        public DocumentSettingsSAP SAPDocSettings { get; set; }
        public List<Freight> ListFreight { get; set; }
        public EmployeeSAP Employee { get; set; }
        public List<DistrRuleSAP> DistrRuleSAP { get; set; }
        public List<Warehouse> Warehouse { get; set; }
        public List<GLAccountSAP> GLAccountSAP { get; set; }
        public decimal? VatSum { get; set; }
        public int? BPLId { get; set; }
        public string BPLName { get; set; }

        public List<UDF_ARGNS> MappedUdf { get; set; }
        public List<AttachmentSAP> AttachmentList { get; set; }

        public List<PaymentMeanOrder> PaymentMeanOrder { get; set; }

        [NotMapped]
        public DateTime LastUpdatedTime { get; set; }

        public OpenQuantityStatus OpenQuantityStatus { get; set; }
    }
}
