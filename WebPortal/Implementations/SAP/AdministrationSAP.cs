﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class AdministrationSAP : IAdministrationSAP
    {
        public int Code { get; set; }
        public string TimeFormat { get; set; }
        public string DateFormat { get; set; }
        public string DateSep { get; set; }
        public short? SumDec { get; set; }
        public short? PriceDec { get; set; }
        public short? RateDec { get; set; }
        public short? QtyDec { get; set; }
        public short? PercentDec { get; set; }
        public short? MeasureDec { get; set; }
        public short? QueryDec { get; set; }
        public string DecSep { get; set; }        
        public string getDateFormat() {
            switch (this.DateFormat)
            {
                case "0":
                    return "DD/MM/YY";
                case "1":
                    return "DD/MM/CCYY";
                case "2":
                    return "MM/DD/YY";
                case "3":
                    return "MM/DD/CCYY";
                case "4":
                    return "CCYY/MM/DD";
                case "5":
                    return "DD/Month/YYYY";
                case "6":
                    return "YY/MM/DD";
                default:
                    return "";
            }
        }
        public string getTimeFormat()
        {
            switch (this.DateFormat)
            {
                case "0":
                    return "24H";
                case "1":
                    return "12H";
                default:
                    return "";
            }
        }
    }
}
