﻿namespace ARGNS.Model.Implementations.SAP
{
	public class ServiceProblemType
    {       
        public short prblmTypID { get; set; }        
        public string Name { get; set; }       
        public string Descriptio { get; set; }
    }
}
