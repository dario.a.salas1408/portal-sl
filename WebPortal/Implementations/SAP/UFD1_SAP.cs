﻿namespace ARGNS.Model.Implementations.SAP
{
	public class UFD1_SAP
    {
        public string TableID { get; set; }

        public short FieldID { get; set; }

        public short IndexID { get; set; }

        public string FldValue { get; set; }

        public string Descr { get; set; }
    }
}
