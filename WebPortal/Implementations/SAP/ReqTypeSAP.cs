﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class ReqTypeSAP : IReqTypeSAP
    {
        public int Code { get; set; }

        public string Name { get; set; }

        public ReqTypeSAP(int pCode, string pName)
        {
            this.Code = pCode;
            this.Name = pName;
        }

    }
}
