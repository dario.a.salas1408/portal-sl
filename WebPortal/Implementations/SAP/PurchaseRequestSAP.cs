﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class PurchaseRequestSAP : IPurchaseRequestSAP, IDocument
    {
        public PurchaseRequestSAP()
        {
            Lines = new List<PurchaseRequestLineSAP>();
            SAPDocSettings = new DocumentSettingsSAP();
            Employee = new EmployeeSAP();
            DistrRuleSAP = new List<DistrRuleSAP>();
            Warehouse = new List<Warehouse>();
            GLAccountSAP = new List<GLAccountSAP>();
            ListDocumentSAPCombo = new DocumentSAPCombo();

        }

        public int DocEntry { get; set; }

        public int DocNum { get; set; }

        public string DocType { get; set; }

        public int? ReqType { get; set; }

        public string Requester { get; set; }

        public string ReqName { get; set; }

        public short? Branch { get; set; }

        public short? Department { get; set; }

        public string Email { get; set; }

        public string Notify { get; set; }

        public DateTime? DocDate { get; set; }

        public DateTime? DocDueDate { get; set; }

        public DateTime? TaxDate { get; set; }

        public DateTime? ReqDate { get; set; }

        public string Comments { get; set; }

        public string DocStatus { get; set; }

        public string CardCode { get; set; }

        public string CardName { get; set; }

        public string Address { get; set; }

        public string NumAtCard { get; set; }

        public decimal? DiscPrcnt { get; set; }

        public string DocCur { get; set; }

        public decimal? DocRate { get; set; }

        public decimal? DocTotal { get; set; }

        public List<PurchaseRequestLineSAP> Lines { get; set; }

        public string ShipToCode { get; set; }

        public string FatherType { get; set; }

        public string JrnlMemo { get; set; }

        public decimal? TotalExpns { get; set; }

        public string ObjType { get; set; }

        public string Address2 { get; set; }

        public short? TrnspCode { get; set; }

        public string PeyMethod { get; set; }

        public short? GroupNum { get; set; }

        public int? CntctCode { get; set; }

        public int? OwnerCode { get; set; }

        public int? SlpCode { get; set; }

        public string SummryType { get; set; }

        public string RevisionPo { get; set; }

        public string Project { get; set; }

        public DateTime? CancelDate { get; set; }

        public bool Draft { get; set; }

        public string Confirmed { get; set; }

        public DocumentSettingsSAP SAPDocSettings { get; set; }
        public string LocalCurrency { get; set; }
        public string SystemCurrency { get; set; }
        public string RateCurrency { get; set; }
        public EmployeeSAP Employee { get; set; }
        public List<DistrRuleSAP> DistrRuleSAP { get; set; }
        public List<Warehouse> Warehouse { get; set; }
        public List<GLAccountSAP> GLAccountSAP { get; set; }
        public DocumentSAPCombo ListDocumentSAPCombo { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }

    }
}
