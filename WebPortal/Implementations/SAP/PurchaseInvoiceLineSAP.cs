﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class PurchaseInvoiceLineSAP : IPurchaseInvoiceLineSAP
    {
        public int DocEntry { get; set; }

        public int LineNum { get; set; }

        public int? TargetType { get; set; }

        public int? TrgetEntry { get; set; }

        public string BaseRef { get; set; }

        public int? BaseType { get; set; }

        public int? BaseEntry { get; set; }

        public int? BaseLine { get; set; }

        public string LineStatus { get; set; }

        public string ItemCode { get; set; }

        public string Dscription { get; set; }

        public decimal? Quantity { get; set; }

        public DateTime? ShipDate { get; set; }

        public decimal? OpenQty { get; set; }

        public decimal? Price { get; set; }

        public string Currency { get; set; }

        public decimal? Rate { get; set; }

        public decimal? DiscPrcnt { get; set; }

        public decimal? LineTotal { get; set; }

        public string WhsCode { get; set; }

        public int? SlpCode { get; set; }

        public decimal? PriceBefDi { get; set; }

        public string OcrCode { get; set; }

        public string FreeTxt { get; set; }

        public string LineVendor { get; set; }

        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}
