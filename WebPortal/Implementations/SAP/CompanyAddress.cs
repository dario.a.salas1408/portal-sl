﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class CompanyAddress : ICompanyAddress
    {
        public string Street { get; set; }

        public string Block { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public string County { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public int Code { get; set; }

        public string AddrType { get; set; }

        public string StreetNo { get; set; }

        public string Building { get; set; }

        public string GlblLocNum { get; set; }
    }
}
