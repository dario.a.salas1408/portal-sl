﻿namespace ARGNS.Model.Implementations.SAP
{
	public class ServiceOrigin
    {       
        public short originID { get; set; }       
        public string Name { get; set; }       
        public string Descriptio { get; set; }        
        public string Locked { get; set; }
    }
}
