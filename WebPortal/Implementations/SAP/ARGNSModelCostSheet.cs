﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ARGNSModelCostSheet
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string Object { get; set; }
        public string LogInst { get; set; }
        public string U_CSCode { get; set; }
        public string U_Desc { get; set; }
        public string U_Selected { get; set; }
    }
}
