﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.SAP
{
	public class DocumentConfirmationLines : IDocumentConfirmationLines
    {
        public DocumentConfirmationLines() 
        {
            ApproveDesicionList = new List<ApproveDesicion>();
            ApproveDesicion ad = new ApproveDesicion("W", "Pending");
            ApproveDesicionList.Add(ad);
            ad = new ApproveDesicion("Y", "Approved");
            ApproveDesicionList.Add(ad);
            ad = new ApproveDesicion("N", "Not Approved");
            ApproveDesicionList.Add(ad);
        }

        public int WddCode { get; set; }

        public int StepCode { get; set; }

        public int UserID { get; set; }

        public string Status { get; set; }

        public string Remarks { get; set; }

        public short? UserSign { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        public List<ApproveDesicion> ApproveDesicionList { get; set; }

        //Agregados para poder mostrar en el listado de My Approvals
        public DateTime? TaxDate { get; set; }
        public DateTime? ReqDate { get; set; }
        public string RequestorRemark { get; set; }
        public string ObjType { get; set; }
        public int ODRFDocEntry { get; set; }
        public decimal? DocTotal { get; set;}
        public string DocumentComments { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
		public string  DraftType { get; set; }

	}
}
