﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class DocumentAddress : IDocumentAddress
    {
        public DocumentAddress()
        {
            this.MappedUdfShipTo = new List<UDF_ARGNS>();
            this.MappedUdfBillTo = new List<UDF_ARGNS>();
        }

        public int DocEntry { get; set; }
        public string StreetS { get; set; }
        public string BlockS { get; set; }
        public string BuildingS { get; set; }
        public string CityS { get; set; }
        public string ZipCodeS { get; set; }
        public string CountyS { get; set; }
        public string StateS { get; set; }
        public string CountryS { get; set; }
        public string AddrTypeS { get; set; }
        public string StreetNoS { get; set; }
        public string StreetB { get; set; }
        public string BlockB { get; set; }
        public string BuildingB { get; set; }
        public string CityB { get; set; }
        public string ZipCodeB { get; set; }
        public string CountyB { get; set; }
        public string StateB { get; set; }
        public string CountryB { get; set; }
        public string AddrTypeB { get; set; }
        public string StreetNoB { get; set; }
        public string GlbLocNumS { get; set; }
        public string GlbLocNumB { get; set; }

        //Added by Portal
        public List<UDF_ARGNS> MappedUdfShipTo { get; set; }
        public List<UDF_ARGNS> MappedUdfBillTo { get; set; }

    }
}
