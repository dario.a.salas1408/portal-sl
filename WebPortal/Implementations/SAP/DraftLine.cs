﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class DraftLine : IDocumentLine
    {
        public int DocEntry { get; set; }

        public int LineNum { get; set; }

        public string ItemCode { get; set; }

        public string Dscription { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? Price { get; set; }

        public string LineVendor { get; set; }

        public DateTime? PQTReqDate { get; set; }

        public decimal? DiscPrcnt { get; set; }

        public decimal? PriceBefDi { get; set; }

        public string Currency { get; set; }

        public DateTime? ShipDate { get; set; }

        public string FreeTxt { get; set; }

        public string OcrCode { get; set; }

        public string Project { get; set; }

        public string TaxCode { get; set; }
                     
        public string WhsCode { get; set; }

        public string AcctCode { get; set; }

        public string SubCatNum { get; set; }

        public decimal? DelivrdQty { get; set; }

        public string ShipToCode { get; set; }

        //Agregados
        public GLAccountSAP GLAccount { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public int DocNum { get; set; }
        public string ItemName { get; set; }
        public string U_ARGNS_MOD { get; set; }
        public string U_ARGNS_COL { get; set; }
        public string U_ARGNS_SIZE { get; set; }
        public string U_ARGNS_SEASON { get; set; }
    }
}
