﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class SalesTaxCodesSAP : ISalesTaxCodesSAP
    {
        public string Code { get; set; }

        public string Name { get; set; }
        public decimal? Rate { get; set; }
    }
}
