﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class DistrRuleSAP : IDistrRuleSAP
    {
        public string OcrCode { get; set; }

        public string OcrName { get; set; }

        public string Locked { get; set; }

        public string Active { get; set; }
    }
}
