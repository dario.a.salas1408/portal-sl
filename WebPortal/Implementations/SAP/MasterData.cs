﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class MasterData
    {
       public List<MasterDataRows> Rows { get; set; }
    }
}
