﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class PaymentTerm : IPaymentTerm
    {
        public short GroupNum { get; set; }

        public string PymntGroup { get; set; }
        
    }
}
