﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class CallType
    {
        public short callTypeID { get; set; }      
        public string Name { get; set; }      
        public string Descriptio { get; set; }
    }
}
