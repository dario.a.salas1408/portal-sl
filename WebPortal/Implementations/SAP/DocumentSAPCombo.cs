﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class DocumentSAPCombo : IDocumentSAPCombo
    {
        public DocumentSAPCombo()
        {
            ListBuyer = new List<Buyer>();
            ListPaymentMethod = new List<PaymentMethod>() { new PaymentMethod()};
            ListCurrency = new List<CurrencySAP>();
            ListPaymentTerm = new List<PaymentTerm>();
            ListShippingType = new List<ShippingType>();
            ListTaxt = new List<Taxt>();
            ListCountry = new List<CountrySAP>();
        }

        public List<Buyer> ListBuyer {get;set;}
        public List<PaymentMethod> ListPaymentMethod { get; set; }
        public List<CurrencySAP> ListCurrency { get; set; }
        public List<PaymentTerm> ListPaymentTerm { get; set; }
        public List<ShippingType> ListShippingType { get; set; }
        public List<Taxt> ListTaxt { get; set; }
        public List<TypeCurrency> ListTypeCurrency { get; set; }
        public List<CountrySAP> ListCountry { get; set; }
        
    }
}
