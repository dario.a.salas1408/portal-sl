﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class DocumentSettingsSAP : IDocumentSettingsSAP
    {
        public int Version { get; set; }

        public string EnblExpns { get; set; }

        public string LawsSet { get; set; }
    }
}
