﻿using ARGNS.Model.Interfaces;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class SerialSAP : ISerialSAP
    {
        public List<Serial> SerialsSAP { get; set; }
        public List<Serial> SerialsInDocument { get; set; }
    }
}
