﻿namespace ARGNS.Model.Implementations.SAP
{
	public class ServiceCallActivities
    {

        public ServiceCallActivities()
        {            
            Activity = new ActivitiesSAP();
        }        
        public int SrvcCallId { get; set; }      
        public short Line { get; set; }
        public int? ClgID { get; set; }          
        public int? VisOrder { get; set; }
        public ActivitiesSAP Activity { get; set; }
    }
}
