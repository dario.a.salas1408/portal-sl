﻿using System.Collections.Generic;

namespace ARGNS.Model.Implementations.SAP
{
	public class ServiceCallComboList
    {
        public ServiceCallComboList()
        {
            ServicePriorityList = new List<ServicePriority>();

            ServiceOriginList = new List<ServiceOrigin>();
            ServiceOriginList.Add(new ServiceOrigin());

            ServiceProblemSubTypeList = new List<ServiceProblemSubType>();
            ServiceProblemSubTypeList.Add(new ServiceProblemSubType());

            ServiceProblemTypeList = new List<ServiceProblemType>();
            ServiceProblemTypeList.Add(new ServiceProblemType());

            CallTypeList = new List<CallType>();
            CallTypeList.Add(new CallType() );

            ItemGroupList = new List<ItemGroupSAP>();
            ItemGroupList.Add(new ItemGroupSAP());

            TechnicianList = new List<EmployeeSAP>();
            TechnicianList.Add(new EmployeeSAP());
            ServiceStatusList = new List<ServiceStatus>();           
            HandledByList = new List<UserSAP>();
           
        }


        public List<ServicePriority> ServicePriorityList { get; set; }
        public List<ServiceOrigin> ServiceOriginList { get; set; }
        public List<ServiceProblemSubType> ServiceProblemSubTypeList { get; set; }
        public List<ServiceProblemType> ServiceProblemTypeList { get; set; }
        public List<CallType> CallTypeList { get; set; }
        public List<ServiceStatus> ServiceStatusList { get; set; }
        public List<EmployeeSAP> TechnicianList { get; set; }
        public List<UserSAP> HandledByList { get; set; }
        public List<ItemGroupSAP> ItemGroupList { get; set; }
    }
}
