﻿namespace ARGNS.Model.Implementations.SAP
{
	public class SaleOrderSAPLineFreight
    {
        public int DocEntry { get; set; }

        public int LineNum { get; set; }

        public int GroupNum { get; set; }

        public int? ExpnsCode { get; set; }

        public decimal? LineTotal { get; set; }

        public decimal? TotalFrgn { get; set; }

        public decimal? TotalSumSy { get; set; }
    }
}
