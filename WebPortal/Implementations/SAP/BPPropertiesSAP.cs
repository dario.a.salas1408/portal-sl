﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class BPPropertiesSAP : IBPPropertiesSAP
    {
        public short GroupCode { get; set; }

        public string GroupName { get; set; }

        public short? UserSign { get; set; }

        public string Filler { get; set; }
    }
}
