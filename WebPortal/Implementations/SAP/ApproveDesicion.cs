﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ApproveDesicion
    {
        public ApproveDesicion() 
        {
        
        }
        
        public ApproveDesicion(string Code, string Name)
        {
            this.Code = Code;
            this.Name = Name;
        }

        public string Code { get; set; }
        public string Name { get; set; }
    }
}
