﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.SAP
{
	public class SalesInvoiceSAP : ISalesInvoiceSAP
    {
        public SalesInvoiceSAP()
        {
            Lines = new List<SalesInvoiceLineSAP>();
            Employee = new EmployeeSAP();
            DistrRuleSAP = new List<DistrRuleSAP>();
            Warehouse = new List<Warehouse>();
            GLAccountSAP = new List<GLAccountSAP>();
        }

        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public string DocType { get; set; }

        public string DocStatus { get; set; }

        public string ObjType { get; set; }

        public DateTime? DocDate { get; set; }

        public DateTime? DocDueDate { get; set; }

        public string CardCode { get; set; }

        public string CardName { get; set; }

        public string Address { get; set; }

        public string NumAtCard { get; set; }

        public decimal? VatSum { get; set; }

        public decimal? DiscPrcnt { get; set; }

        public decimal? DiscSum { get; set; }

        public string DocCur { get; set; }

        public decimal? DocRate { get; set; }

        public decimal? DocTotal { get; set; }

        public decimal? PaidToDate { get; set; }

        public string Ref1 { get; set; }

        public string Ref2 { get; set; }

        public string Comments { get; set; }

        public string JrnlMemo { get; set; }

        public int? SlpCode { get; set; }

        public short? TrnspCode { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? TaxDate { get; set; }

        public short? UserSign { get; set; }

        public decimal? TotalExpns { get; set; }

        public DateTime? ReqDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public int? OwnerCode { get; set; }

        public string Requester { get; set; }

        public string ReqName { get; set; }

        public short? Branch { get; set; }

        public short? Department { get; set; }

        public string Email { get; set; }

        public string Notify { get; set; }

        public Nullable<short> GroupNum { get; set; }
        public string PeyMethod { get; set; }
        public decimal? DpmAmnt { get; set; }
        public decimal? RoundDif { get; set; }
        public string Confirmed { get; set; }
        public int? BPLId { get; set; }
        public string BPLName { get; set; }

        //Agregados
        public List<SalesInvoiceLineSAP> Lines { get; set; }
        public DocumentSAPCombo ListDocumentSAPCombo { get; set; }
        public DocumentAddress SIAddress { get; set; }
        public EmployeeSAP Employee { get; set; }
        public List<DistrRuleSAP> DistrRuleSAP { get; set; }
        public List<Warehouse> Warehouse { get; set; }
        public List<GLAccountSAP> GLAccountSAP { get; set; }
        public string LocalCurrency { get; set; }
        public string SystemCurrency { get; set; }
        public string RateCurrency { get; set; }
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}
