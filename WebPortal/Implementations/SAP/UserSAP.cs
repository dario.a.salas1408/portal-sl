﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class UserSAP : IUserSAP
    {
        public short USERID { get; set; }
        public string PASSWORD { get; set; }
        public string USER_CODE { get; set; }
        public string U_NAME { get; set; }       
    }
}
