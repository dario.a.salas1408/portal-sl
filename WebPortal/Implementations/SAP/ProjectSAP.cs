﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ProjectSAP : IProjectSAP
    {
        public string PrjCode { get; set; }

        public string PrjName { get; set; }
    }
}
