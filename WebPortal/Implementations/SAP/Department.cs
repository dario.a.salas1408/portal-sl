﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class Department : IDepartment
    {
        public short Code { get; set; }

        public string Name { get; set; }

        public string Remarks { get; set; }

        public short? UserSign { get; set; }

        public string Father { get; set; }
    }
}
