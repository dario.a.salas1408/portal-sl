﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class GLAccountSAP : IGLAccount
    {
        public string AcctCode { get; set; }

        public string AcctName { get; set; }

        public string FormatCode { get; set; }
    }
}
