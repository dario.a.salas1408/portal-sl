﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class CurrencySAP : ICurrency
    {
        public string CurrCode {get;set;}

        public string CurrName { get; set; }
        
    }
}
