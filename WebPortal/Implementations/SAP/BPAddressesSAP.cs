﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class BPAddressesSAP : IBPAddressesSAP
    {
        public string Address { get; set; }

        public string CardCode { get; set; }

        public string Street { get; set; }

        public string Block { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public int? LineNum { get; set; }

        public string Building { get; set; }

        public string AdresType { get; set; }

        public string StreetNo { get; set; }

        public string GlblLocNum { get; set; }

        public string LicTradNum { get; set; }

        public string TaxCode { get; set; }

        //Added by Portal
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}
