﻿namespace ARGNS.Model.Implementations.SAP
{
	public class ServicePriority
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
