﻿namespace ARGNS.Model.Implementations.SAP
{
	public class ServiceProblemSubType
    {
        public short ProSubTyId { get; set; }
        public string Name { get; set; }
        public string Descriptio { get; set; }
    }
}
