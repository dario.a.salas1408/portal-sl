﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class TypeCurrency : ITypeCurrency
    {
        public string CurSource {get;set;}

        public string Description { get; set; }
        
    }
}
