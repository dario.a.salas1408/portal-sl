﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ActivityPriority : IActivityPriority
    {
        public ActivityPriority(string Code, string Name)
        {
            this.Code = Code;
            this.Name = Name;
        }

        public ActivityPriority()
        {

        }

        public string Code{get;set;}
        public string Name { get; set; }
       
    }
}
