﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ItemGroupSAP
    {
        public ItemGroupSAP()
        {

        }

        public ItemGroupSAP(short pItmsGrpCod, string pItmsGrpNam)
        {
            this.ItmsGrpCod = pItmsGrpCod;
            this.ItmsGrpNam = pItmsGrpNam;
        }
        
        public short ItmsGrpCod { get; set; }       
        public string ItmsGrpNam { get; set; }
    }
}
