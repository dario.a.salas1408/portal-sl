﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ActivityUserType : IActivityUserType
    {
        public ActivityUserType()
        {

        }

        public ActivityUserType(int Code, string Name)
        {
            this.Code = Code;
            this.Name = Name;
        }

        public int Code { get; set; }
        public string Name { get; set; }
    }
}
