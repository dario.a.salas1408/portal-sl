﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class AlertColumnSAP
    {
        public int Code { get; set; }

        public int Location { get; set; }

        public string ColName { get; set; }

        public string Link { get; set; }
    }
}
