﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
   public  class ActivitiesSAP : IActivitiesSAP
   {
       public ActivitiesSAP()
       {
           ActivityCombo = new ActivitiesSAPCombo();
       }

        public int ClgCode { get; set; }
        public string CardCode { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> Recontact { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }        
        public string Closed { get; set; }
        public string inactive { get; set; }
        public Nullable<short> AttendUser { get; set; }
        public string Action { get; set; }
        public  string Details { get; set; }
        public Nullable<short> CntctType { get; set; }
        public string Priority { get; set; }
        public Nullable<short> CntctSbjct { get; set; }
        public ActivitiesSAPCombo ActivityCombo { get; set; }
        public string U_ARGNS_PRDCODE { get; set; }
        public string U_ARGNS_PROYECT { get; set; }
        public string CardName { get; set; }   
        public string ModelName { get; set; }
        public string ActionName { get; set; }
        public string ModelImage { get; set; }
        public string AssignedUserName { get; set; }
        public Nullable<int> AttendEmpl { get; set; }
        public int? OprId { get; set; }
        public short? OprLine { get; set; }
        public int? status { get; set; }
        public int? BeginTime { get; set; }
        public int? ENDTime { get; set; }
        public decimal? Duration { get; set; }
        public int UserType { get; set; }

        //Added to apparel
        public string U_ARGNS_WORKF2 { get; set; }
        public short? U_ARGNS_JOBID { get; set; }
        public int ProjectLineId { get; set; }
    }
}
