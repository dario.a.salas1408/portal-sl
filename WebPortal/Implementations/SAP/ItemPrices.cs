﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{
    public class ItemPrices : IItemPrices
    {
        public string ItemCode { get; set; }

        public short? PriceList { get; set; }

        public decimal? Price { get; set; }
        
        public string Currency { get; set; }

        public bool HasUOMPrice { get; set; }

        public bool HasCurrencyPrice { get; set; }

        public string UOMCode { get; set; }

        public decimal? Factor { get; set; }
    }
}
