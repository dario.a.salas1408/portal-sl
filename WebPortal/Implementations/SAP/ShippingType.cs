﻿using ARGNS.Model.Interfaces.SAP;

namespace ARGNS.Model.Implementations.SAP
{
	public class ShippingType : IShippingType
    {
        public int TrnspCode {get;set;}

        public string TrnspName { get; set; }
        
    }
}
