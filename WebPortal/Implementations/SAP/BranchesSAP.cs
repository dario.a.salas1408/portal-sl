﻿using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.SAP
{

    public class BranchesSAP : IBranchesSAP
    {
        public int BPLId { get; set; }
        public string BPLName { get; set; }
    }
}
