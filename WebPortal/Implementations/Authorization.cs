﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;

namespace ARGNS.Model.Implementations
{
	public class Authorization : IAuthorization
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        /// <summary>
        /// Level es el que indica si va a a ser a nivel de menu o formulario etc.
        /// </summary>
        public char Level { get; set; }

        public virtual User User { get; set; }

        public int? User_IdUser { get; set; }
        
    }
}
