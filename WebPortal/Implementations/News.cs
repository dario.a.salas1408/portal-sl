﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;

namespace ARGNS.Model.Implementations
{
	public class News : INews
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(100)]
        public string Head { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public string ImageName { get; set; }

        public DateTime Fecha { get; set; }

        public bool Active { get; set; }

        //Agregados View
        [NotMapped]
        public bool ImgUploadChange { get; set; }

    }
}
