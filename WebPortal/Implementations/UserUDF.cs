﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace ARGNS.Model.Implementations
{
	public class UserUDF
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdUDF { get; set; }

        public int IdUser { get; set; }

        public int IdCompany { get; set; }

        [StringLength(100)]
        public string Document { get; set; }

        [StringLength(100)]
        public string UDFName { get; set; }

        [StringLength(100)]
        public string UDFDesc { get; set; }

        public short FieldID { get; set; }

        [StringLength(1)]
        public string TypeID { get; set; }

        [StringLength(20)]
        public string RTable { get; set; }

        public virtual User User { get; set; }

        public virtual CompanyConn Company { get; set; }
    }
}
