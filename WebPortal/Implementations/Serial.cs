﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations
{
    public class Serial : ISerial
    {
        public string DistNumber { get; set; }
        public int SysNumber { get; set; }
        public int DocLine { get; set; }
        public int AbsEntry { get; set; }
        public decimal? AllocQty { get; set; }
        public string whsCode { get; set; }
        public List<UDF_ARGNS> UDFs { get; set; }

    }
}
