﻿using ARGNS.Model.Interfaces;

namespace ARGNS.Model.Implementations
{
	public class RatesSystem : IRatesSystem
    {
        public RatesSystem()
        {
            System = false;
            Local = false;
        }

        public string Currency { get; set; }
        public double Rate { get; set; }
        public bool Local { get; set; }
        public bool System { get; set; }
    }
}
