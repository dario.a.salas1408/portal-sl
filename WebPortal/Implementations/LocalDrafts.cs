﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations
{
    public class LocalDrafts : ILocalDrafts
    {
        public LocalDrafts()
        {

        }

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocNum { get; set; }
        public string ObjectCode { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string NumAtCard { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? DocDueDate { get; set; }
        public string Comments { get; set; }
        public int UserId { get; set; }
        public int CompanyId { get; set; }
        public string DocStatus { get; set; }
        public string ObjType { get; set; }
        public virtual ICollection<LocalDraftsLine> Lines { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public short? GroupNum { get; set; }
        public int DraftOrigin { get; set; }
    }
}
