﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;
using ARGNS.Model.Implementations.Portal;

namespace ARGNS.Model.Implementations
{
	public class UserGroup : IUserGroup
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int IdCompany { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string CSSName { get; set; }

        [StringLength(100)]
        public string JSName { get; set; }

        public virtual ICollection<PortalChartUserGroup> PortalChartUserGroups { get; set; }
    }
}
