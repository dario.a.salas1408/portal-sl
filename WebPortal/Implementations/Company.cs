﻿using ARGNS.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace ARGNS.Model.Implementations
{

	public class CompanyConn : ICompany
    {
        public CompanyConn()
        {
            UsersSettings = new HashSet<UsersSetting>();
            CRPageMaps = new HashSet<CRPageMap>();
            CompanyStock = new HashSet<CompanyStock>();
        }

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCompany { get; set; }
        public int ServerType { get; set; }
        [StringLength(100)]
        public string Server { get; set; }
        [StringLength(100)]
        public string CompanyDB { get; set; }
        [StringLength(50)]
        public string UserName { get; set; }
        [StringLength(254)]
        public string Password { get; set; }
        [StringLength(50)]
        public string DbUserName { get; set; }
        [StringLength(254)]
        public string DbPassword { get; set; }
        public bool UseTrusted { get; set; }
        public bool UseCompanyUser { get; set; }
        public bool ItemInMultipleLines { get; set; }
        public bool UseHandheld { get; set; }
        public bool CheckBasketID { get; set; }
        public bool OnDemand { get; set; }
        public virtual ICollection<UsersSetting> UsersSettings { get; set; }
        public virtual ICollection<CRPageMap> CRPageMaps { get; set; }
        public bool Active { get; set; }
        public virtual ICollection<CompanyStock> CompanyStock { get; set; }
        public string LicenseServer { get; set; }
        public int PortNumber { get; set; }
        [StringLength(10)]
        public string SAPLanguaje { get; set; }
        [NotMapped]
        public string SLSessionId { get; set; }
        [NotMapped]
        public string SLRouteId { get; set; }
        [NotMapped]
        public string DSSessionId { get; set; }
        public int? CodeType { get; set; }
        public string UrlHana { get; set; }
        public string UrlHanaGetQueryResult { get; set; }
        public string UrlSL { get; set; }
        [NotMapped]
        public int IdUserConected { get; set; }
        public short? VisualOrder { get; set; }
        [NotMapped]
        public CompanySAPConfig CompanySAPConfig { get; set; }
        [NotMapped]
        public int? IdBranchSelect { get; set; }
    }

}
