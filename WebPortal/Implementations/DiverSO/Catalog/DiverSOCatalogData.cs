﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.Catalog
{
    public class DiverSOCatalogData
    {
        public int Count { get; set; }
        public List<DiverSOCatalogValue> Data { get; set; }
    }
}
