﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.Catalog
{

    public class DiverSOCatalogRequest
    {
        public DiverSOCatalogRequest(string pMethod, string pCardCode, string pCatalogCode, string pCatalogName, string pSlpCode, string pDate, bool pIncludeItems, string pItemCode, string pItemName, string pItemGroup, int pSkip, int pTake)
        {
            method = pMethod;
            @params = new DiverSOCatalogRequestParams(pCardCode, pCatalogCode, pCatalogName, pSlpCode, pDate, pIncludeItems, pItemCode, pItemName, pItemGroup, pSkip, pTake);
        }

        public string method { get; set; }
        public DiverSOCatalogRequestParams @params { get; set; }

    }

    public class DiverSOCatalogRequestParams
    {
        public DiverSOCatalogRequestParams(string pCardCode, string pCatalogCode, string pCatalogName, string pSlpCode, string pDate, bool pIncludeItems, string pItemCode, string pItemName, string pItemGroup, int pSkip, int pTake)
        {
            cardCode = pCardCode;
            code = pCatalogCode;
            name = pCatalogName;
            slpCode = pSlpCode;
            validOnDate = pDate;
            includeItems = pIncludeItems.ToString();
            itemCode = pItemCode;
            itemName = pItemName;
            itemGroup = pItemGroup;
            skip = pSkip;
            take = pTake;
        }

        public string cardCode { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string slpCode { get; set; }
        public string validOnDate { get; set; }
        public string includeItems { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public string itemGroup { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
    }

   

}
