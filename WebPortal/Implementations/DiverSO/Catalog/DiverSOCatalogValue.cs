﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.Catalog
{
    public class DiverSOCatalogValue
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime DueDate { get; set; }
        public string Image { get; set; }
        public List<ItemMasterDSO> Items { get; set; }
    }
}
