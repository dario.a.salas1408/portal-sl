﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.Catalog
{
    public class DiverSOCatalogResult : IDiverSOResult
    {
        public string error { get; set; }
        public bool succeed { get; set; }
        public DiverSOCatalogData value { get; set; }
    }
}
