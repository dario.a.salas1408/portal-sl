﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations
{
    public class ItemMasterDSO
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Grupo { get; set; }
        public string Rubros { get; set; }
        public string Marca { get; set; }
        public string Familia { get; set; }
        public string Proveedor { get; set; }
        public decimal OnHand { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }
    }
}
