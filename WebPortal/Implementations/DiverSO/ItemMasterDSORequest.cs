﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations
{
    public class ItemMasterDSORequest
    {
        public ItemMasterDSORequest(string pMethod,string pItemCode,string pCardCode)
        {
            this.method = pMethod;
            this.@params = new ItemsParameters(pItemCode, pCardCode);
        }

        public string method { get; set; }
        public ItemsParameters @params { get; set; }

    }

    public class ItemsParameters
    {
        public ItemsParameters(string pItemCode, string pCardCode)
        {
            this.itemCode = pItemCode;
            this.cardCode = pCardCode;
        }
        public string itemCode { get; set; }
        public string cardCode { get; set; }
    }


}



