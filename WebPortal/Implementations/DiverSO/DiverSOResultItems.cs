﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO
{
    public class DiverSOResultItems : IDiverSOResult
    {
        public string error{ get; set;}
        public bool succeed {get; set; }
        public List<ItemMasterDSO> value { get; set; }
        public int TotalRecords { get; set; }
    }
}
