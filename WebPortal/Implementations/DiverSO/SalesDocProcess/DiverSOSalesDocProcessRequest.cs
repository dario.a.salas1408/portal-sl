﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.SalesDocProcess
{

    public class DiverSOSalesDocProcessRequest
    {
        public DiverSOSalesDocProcessRequest(string pMethod, string pCardCode, string pOrigin, string pDocDate, List<DiverSOSalesDocProcessRequestDocLines> pSalesDocLines)
        {
            method = pMethod;
            @params = new DiverSOSalesDocProcessRequestParams(pCardCode, pOrigin, pDocDate, pSalesDocLines);
        }

        public string method { get; set; }
        public DiverSOSalesDocProcessRequestParams @params { get; set; }

    }

    public class DiverSOSalesDocProcessRequestParams
    {
        public DiverSOSalesDocProcessRequestParams(string pCardCode, string pOrigin, string pDocDate, List<DiverSOSalesDocProcessRequestDocLines> pSalesDocLines)
        {
            doc = new DiverSOSalesDocProcessRequestDoc(pCardCode, pOrigin, pDocDate, pSalesDocLines);
        }

        public DiverSOSalesDocProcessRequestDoc doc { get; set; }
    }

   

}
