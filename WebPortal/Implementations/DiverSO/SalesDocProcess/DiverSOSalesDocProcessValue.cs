﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.SalesDocProcess
{
    public class DiverSOSalesDocProcessValue
    {
        public List<DiverSOPromotions> Promotions { get; set; }
    }
}
