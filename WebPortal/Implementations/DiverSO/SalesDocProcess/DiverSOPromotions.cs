﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.SalesDocProcess
{
    public class DiverSOPromotions
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal DiscountTotal { get; set; }
        public string Notes { get; set; }
        public short BaseLineNum { get; set; }

    }
}
