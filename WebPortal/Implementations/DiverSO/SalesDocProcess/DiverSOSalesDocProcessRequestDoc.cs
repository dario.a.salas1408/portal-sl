﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.SalesDocProcess
{
    public class DiverSOSalesDocProcessRequestDoc
    {
        public DiverSOSalesDocProcessRequestDoc(string pCardCode, string pOrigin, string pDocDate, List<DiverSOSalesDocProcessRequestDocLines> pSalesDocLines)
        {
            CardCode = pCardCode;
            Origin = pOrigin;
            DocDate = pDocDate;
            Lines = pSalesDocLines;
        }

        public string CardCode { get; set; }
        public string Origin { get; set; }
        public string DocDate { get; set; }
        public List<DiverSOSalesDocProcessRequestDocLines> Lines { get; set; }

    }
}
