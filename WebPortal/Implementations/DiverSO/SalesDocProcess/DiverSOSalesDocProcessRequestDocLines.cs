﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.SalesDocProcess
{
    public class DiverSOSalesDocProcessRequestDocLines
    {
        public DiverSOSalesDocProcessRequestDocLines(short pLineNum, string pItemCode, decimal pQuantity, decimal pPrice, decimal pLineTotal)
        {
            LineNum = pLineNum;
            ItemCode = pItemCode;
            Quantity = pQuantity;
            Price = pPrice;
            LineTotal = pLineTotal;
        }


        public short LineNum { get; set; }
        public string ItemCode { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal LineTotal { get; set; }

    }
}
