﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.DiverSO.SalesDocProcess
{
    public class DiverSOSalesDocProcessResult : IDiverSOResult
    {
        public string error { get; set; }
        public bool succeed { get; set; }
        public DiverSOSalesDocProcessValue value { get; set; }
    }
}
