﻿using ARGNS.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace ARGNS.Model.Implementations
{
	public partial class Actions : IActions
    {
        public Actions()
        {
            RolPageActions = new HashSet<RolPageAction>();
            UserPageActions = new HashSet<UserPageAction>();
            Pages = new HashSet<Page>();
        }

        [Key]
        public int IdAction { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public bool Active { get; set; }

        public string InternalKey { get; set; }
        [StringLength(150)]
        public string KeyResource { get; set; }

        public virtual ICollection<RolPageAction> RolPageActions { get; set; }

        public virtual ICollection<UserPageAction> UserPageActions { get; set; }

        public virtual ICollection<Page> Pages { get; set; }
    }
}

