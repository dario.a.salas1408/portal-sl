﻿using ARGNS.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations
{
    public class PaymentMeanOrder : IPaymentMeanOrder
    {
        public int DocEntry { get; set; }
        public int U_DocType { get; set; }
        public int U_DocEntryOrder { get; set; }
        public int U_DocNumOrder { get; set; }
        public string U_PaymentCode { get; set; }
        public decimal U_Amount { get; set; }
        public int? U_Quotas { get; set; }
        public Int16 U_CreditCard { get; set; }
        public string U_Voucher { get; set; }
        public DateTime? U_Date { get; set; }
        public DateTime? U_DueDate { get; set; }
        public string U_Account { get; set; }
        public string U_Reference { get; set; }
        public decimal? U_AmountSurcharger { get; set; }
        public string U_CUIT { get; set; }
        public decimal U_Neto { get; set; }
        public int? U_Contract { get; set; }
    }
}
