﻿using ARGNS.Model.Implementations.SAP;
using System.Collections;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations
{
	public class CompanySAPConfig
    {
        public bool CheckApparelInstallation { get; set; }

        public DocumentSettingsSAP DocumentSettingsSAP { get; set; }

        public CompanySettingsSAP CompanySettingsSAP { get; set; }

        public IEnumerable<BranchesSAP> Branches { get; set; } 
    }
}
