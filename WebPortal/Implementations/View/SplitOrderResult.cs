﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.View
{
    public class SplitOrderResult
    {
        public string Code { get; set; }
        public string OrderType { get; set; }
        public string ErrorMsg { get; set; }
    }
}
