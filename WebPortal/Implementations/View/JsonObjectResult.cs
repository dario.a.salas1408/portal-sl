﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.View
{
	public class JsonObjectResult
    {
        public JsonObjectResult()
        {
            Others = new Dictionary<string, string>();
            SplitOrders = new List<SplitOrderResult>();
        }
        
        public string ServiceAnswer { get; set; }
        public string Code { get; set; }
        public string RedirectUrl { get; set; }
        public string ErrorMsg { get; set; }
        public string UserMsg { get; set; }
        public string AddUpdateMsg { get; set; }
        public Dictionary<string, string> Others { get; set; }
        public List<PurchaseOrderSAP> PurchaseOrderSAPList { get; set; }
        public List<PurchaseQuotationSAP> PurchaseQuotationSAPList { get; set; }
        public List<PurchaseRequestSAP> PurchaseRequestSAPList { get; set; }
        public List<PurchaseInvoiceSAP> PurchaseInvoiceSAPList { get; set; }
        public List<SaleOrderSAP> SalesOrderSAPList { get; set; }
        public List<SalesQuotationSAP> SalesQuotationSAPList { get; set; }
        public List<SalesInvoiceSAP> SalesInvoiceSAPList { get; set; }
        public List<DraftPortal> DraftPortalList { get; set; }
        public List<ItemMasterSAP> ItemMasterSAPList { get; set; }
        public List<BusinessPartnerSAP> BusinessPartnerSAPList { get; set; }
        public List<InventoryStatus> InventoryStatusList { get; set; }
        public List<PDM.ModelDesc> ModelDescViewList { get; set; }
        public List<LocalDrafts> LocalDraftsList { get; set; }
        public List<User> UserList { get; set; }
        public List<ARGNSModel> ARGNSModelList { get; set; }

        public int SplitOrderFolioNumber { get; set; }
        public List<SplitOrderResult> SplitOrders { get; set; }
    }
}
