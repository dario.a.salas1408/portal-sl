﻿namespace ARGNS.Model.Implementations.View
{
	public class InventoryStatus
    {
        public string ItmsGrpNam { get; set; }
        public short? ItmsGrpCod { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public decimal? OnHand { get; set; }
        public decimal? IsCommited { get; set; }
        public decimal? OnOrder { get; set; }
        public decimal? Available { get; set; }
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string Phone1 { get; set; }
    }
}
