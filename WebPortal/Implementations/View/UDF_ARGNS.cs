﻿using ARGNS.Model.Implementations.SAP;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.View
{
	public class UDF_ARGNS
    {
        public UDF_ARGNS(string UDFName, string Value, short FieldID = 0)
        {
            this.UDFName = UDFName;
            this.Value = Value;
            this.FieldID = FieldID;
            this.ListUFD1 = new List<UFD1_SAP>();
        }

        public UDF_ARGNS()
        {
            this.ListUFD1 = new List<UFD1_SAP>();
        }

        public UDF_ARGNS(UDF_ARGNS mUDF)
        {
            this.UDFName = mUDF.UDFName;
            this.Value = mUDF.Value;
            this.FieldID = mUDF.FieldID;
            this.TypeID = mUDF.TypeID;
            this.ListUFD1 = mUDF.ListUFD1;
            this.UDFDesc = mUDF.UDFDesc;
            this.RTable = mUDF.RTable;
        }

        public string UDFName { get; set; }
        public string Value { get; set; }
        public short FieldID { get; set; }
        public string TypeID { get; set; }
        public List<UFD1_SAP> ListUFD1 { get; set; }
        public string UDFDesc { get; set; }
        public string RTable { get; set; }
        public string Document { get; set; }
    }
}
