﻿using System.Collections.Generic;

namespace ARGNS.Model.Implementations.View
{
	public class CostSheetUDF
    {
        public List<UDF_ARGNS> ListUDFMaterial { get; set; }
        public List<UDF_ARGNS> ListUDFOperation { get; set; }
        public List<UDF_ARGNS> ListUDFSchema { get; set; }
        public List<UDF_ARGNS> ListUDFPattern { get; set; }
        public List<UDF_ARGNS> ListUDFOITM { get; set; }
    }
}
