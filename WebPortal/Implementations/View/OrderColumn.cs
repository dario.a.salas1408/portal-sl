﻿namespace ARGNS.Model.Implementations.View
{
	public class OrderColumn
    {
        public string Data { get; set; }
        public bool IsOrdered { get; set; }
        public string Name { get; set; }
        public bool Orderable { get; set; }
        public int OrderNumber { get; set; }
        public bool Searchable { get; set; }
        public OrderDirection SortDirection { get; set; }

        public enum OrderDirection
        {
            Ascendant = 0,
            Descendant = 1
        }
    }
}
