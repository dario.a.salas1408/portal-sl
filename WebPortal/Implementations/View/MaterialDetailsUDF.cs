﻿using System.Collections.Generic;

namespace ARGNS.Model.Implementations.View
{
	public class MaterialDetailsUDF
    {
        public List<UDF_ARGNS> ListUDFFabric { get; set; }
        public List<UDF_ARGNS> ListUDFAccessories { get; set; }
        public List<UDF_ARGNS> ListUDFCareInstructions { get; set; }
        public List<UDF_ARGNS> ListUDFLabelling { get; set; }
        public List<UDF_ARGNS> ListUDFPackaging { get; set; }
        public List<UDF_ARGNS> ListUDFFootwearMaterial { get; set; }
        public List<UDF_ARGNS> ListUDFFootwearDetail { get; set; }
        public List<UDF_ARGNS> ListUDFFootwearPackaging { get; set; }
        public List<UDF_ARGNS> ListUDFFootwearPictogram { get; set; }
        public List<UDF_ARGNS> ListUDFFootwearSizeRange { get; set; }
        public List<UDF_ARGNS> ListUDFFootwearAccessories { get; set; }
    }
}
