﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.View;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.View
{
	public class Freight : IFreight
    {
        public Freight()
        {
            ListDistrRule = new List<DistrRuleSAP>();
            ListTax = new List<SalesTaxCodesSAP>();
            ListProject = new List<ProjectSAP>();
        }

        public Freight(int? ExpnsCode, string ExpnsName)
        {
            this.ExpnsCode = ExpnsCode;
            this.ExpnsName = ExpnsName;
            ListDistrRule = new List<DistrRuleSAP>();
            ListTax = new List<SalesTaxCodesSAP>();
            ListProject = new List<ProjectSAP>();
        }

        public int DocEntry { get; set; }
        public int? ExpnsCode { get; set; }
        public string ExpnsName { get; set; }
        public decimal? LineTotal { get; set; }
        public decimal? TotalFrgn { get; set; }
        public string Comments { get; set; }
        public string ObjType { get; set; }
        public string DistrbMthd { get; set; }
        public decimal? VatSum { get; set; }
        public string TaxCode { get; set; }       
        public int LineNum { get; set; }
        public string Status { get; set; }
        public string OcrCode { get; set; }
        public string TaxDistMtd { get; set; }
        public string Project { get; set; }
        public int? BaseAbsEnt { get; set; }
        public int? BaseType { get; set; }
        public int? BaseRef { get; set; }
        public int? BaseLnNum { get; set; }
        public List<DistrRuleSAP> ListDistrRule { get; set; }
        public List<SalesTaxCodesSAP> ListTax { get; set; }
        public List<ProjectSAP> ListProject { get; set; }
    }
}
