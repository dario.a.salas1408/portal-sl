﻿using ARGNS.Model.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{
	public class InfoPortal : IInfoPortal
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(254)]
        public string vsPortal { get; set; }
        [StringLength(254)]
        public string vsDB { get; set; }
        public System.DateTime IniDB { get; set; }
        public System.DateTime LastUpdateDB { get; set; }
    }
}
