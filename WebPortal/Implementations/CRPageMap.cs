﻿using ARGNS.Model.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{
	public partial class CRPageMap : ICRPageMap
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdICRPageMap { get; set; }
        [ForeignKey("Page")]
        public int IdPage { get; set; }
        [ForeignKey("CompanyConn")]
        public int IdCompany { get; set; }
        public virtual CompanyConn CompanyConn { get; set; }
        public virtual Page Page { get; set; }
        public string CRName { get; set; }
    }
}
