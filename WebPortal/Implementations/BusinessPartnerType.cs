﻿using ARGNS.Model.Interfaces;

namespace ARGNS.Model.Implementations
{
	public class BusinessPartnerType : IBusinessPartnerType
    {

        public string Code { get; set; }
        public string Name { get; set; }

    }
}
