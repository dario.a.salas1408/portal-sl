﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.Portal
{
    public class UserDirectAccess
    {
        public UserDirectAccess()
        {

        }

        public UserDirectAccess(int pIdDirectAccess, int pIdUser, int pIdCompany, int pIdTile)
        {
            this.IdDirectAccess = pIdDirectAccess;
            this.IdUser = pIdUser;
            this.IdCompany = pIdCompany;
            this.IdTile = pIdTile;
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdUser { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdCompany { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdDirectAccess { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdTile { get; set; }

        public virtual User User { get; set; }

        public virtual CompanyConn CompanyConn { get; set; }
    }
}
