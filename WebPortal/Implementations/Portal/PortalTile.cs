﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.Portal
{
    public class PortalTile
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTile { get; set; }

        [Column(Order = 1)]
        public string TileName { get; set; }

        [Column(Order = 2)]
        public int? IdSAPQuery { get; set; }

        [Column(Order = 3)]
        public string NameSAPQuery { get; set; }

        [Column(Order = 4)]
        public int IdTileType { get; set; }

        [Column(Order = 5)]
        public string UrlPage { get; set; }

        [Column(Order = 6)]
        public string HtmlText { get; set; }

        [Column(Order = 7)]
        public int IdCompany { get; set; }

        [Column(Order = 8)]
        public short? VisualOrder { get; set; }

        public virtual PortalTileType PortalTileType { get; set; }

        public virtual ICollection<PortalTileUserGroup> PortalTileUserGroups { get; set; }

        public virtual ICollection<PortalTileParam> PortalTileParams { get; set; }

        public virtual CompanyConn CompanyConns { get; set; }
    }
}
