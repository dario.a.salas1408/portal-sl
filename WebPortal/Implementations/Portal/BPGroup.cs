﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;

namespace ARGNS.Model.Implementations
{
    public class BPGroup
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int IdCompany { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public virtual ICollection<BPGroupLine> BPGroupLines { get; set; }

        public virtual CompanyConn CompanyConns { get; set; }
    }
}
