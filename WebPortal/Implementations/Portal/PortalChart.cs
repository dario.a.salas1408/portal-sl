﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.Portal
{
    public class PortalChart
    {
        public PortalChart()
        {
            PortalChartColumns = new HashSet<PortalChartColumn>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChart { get; set; }

        [Column(Order = 1)]
        public string ChartName { get; set; }

        [Column(Order = 2)]
        public int? IdSAPQuery { get; set; }

        [Column(Order = 3)]
        public string NameSAPQuery { get; set; }

        [Column(Order = 4)]
        public int IdChartType { get; set; }

        [Column(Order = 5)]
        public string PortalPage { get; set; }

        [Column(Order = 6)]
        public int IdCompany { get; set; }

        [Column(Order = 7)]
        public bool MenuChart { get; set; }

        public virtual PortalChartType PortalChartType { get; set; }

        public virtual ICollection<PortalChartColumn> PortalChartColumns { get; set; }

        public virtual ICollection<PortalChartUserGroup> PortalChartUserGroups { get; set; }

        public virtual ICollection<PortalChartParam> PortalChartParamList { get; set; }

        public virtual CompanyConn CompanyConns { get; set; }
    }
}
