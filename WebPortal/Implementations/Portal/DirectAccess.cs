﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.Portal
{
    public class DirectAccess
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdDirectAccess { get; set; }

        [Column(Order = 1)]
        public int Page { get; set; }

        [Column(Order = 2)]
        public int Action { get; set; }

        [Column(Order = 3)]
        public int Area { get; set; }

        [Column(Order = 5)]
        public string Url { get; set; }

        [Column(Order = 6)]
        public string UrlName { get; set; }

        [Column(Order = 7)]
        [StringLength(50)]
        public string TranslationKey { get; set; }
    }
}
