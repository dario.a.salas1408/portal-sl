﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.Portal
{
    [Table("PortalChartParamsValueType")]
    public partial class PortalChartParamsValueType
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string ParamValueTypeKey { get; set; }

        [StringLength(50)]
        public string ParamValueTypeDesc { get; set; }
    }
}
