﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.Portal
{
    public class PortalTileUserGroup
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? UserGroup_Id { get; set; }

        public virtual PortalTile PortalTile { get; set; }

        public virtual UserGroup UserGroup { get; set; }
    }
}
