﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.Portal
{
    public class PortalTileType
    {
        public PortalTileType()
        {
            PortalTiles = new HashSet<PortalTile>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTileType { get; set; }

        [Column(Order = 1)]
        public string TypeName { get; set; }

        public virtual ICollection<PortalTile> PortalTiles { get; set; }
    }
}
