﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.Portal
{
    public class PortalTileParam
    {
        public PortalTileParam()
        {
            IsNew = false;
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string ParamName { get; set; }

        public string ParamValue { get; set; }

        public string ParamFixedValue { get; set; }

        public virtual PortalTile PortalTile { get; set; }

        //Agregados 
        [NotMapped]
        public bool IsNew { get; set; }
    }
}
