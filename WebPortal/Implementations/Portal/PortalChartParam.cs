﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.Portal
{
    public class PortalChartParam
    {
        public PortalChartParam()
        {
            IsNew = false;
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [NotMapped]
        public int PortalChart_IdChart { get; set; }

        public string ParamName { get; set; }

        public string ParamNamePortal { get; set; }

        public string ParamValue { get; set; }

        public string ParamFixedValue { get; set; }

        [StringLength(20)]
        public string ParamValueType { get; set; }

        public virtual PortalChart PortalChart { get; set; }

        //Agregados 
        [NotMapped]
        public bool IsNew { get; set; }
    }
}
