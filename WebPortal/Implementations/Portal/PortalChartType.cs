﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.Portal
{
    public class PortalChartType
    {
        public PortalChartType()
        {
            PortalCharts = new HashSet<PortalChart>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdChartType { get; set; }

        [Column(Order = 1)]
        public string ChartName { get; set; }

        [Column(Order = 2)]
        public int ChartColumnNumber { get; set; }

        public virtual ICollection<PortalChart> PortalCharts { get; set; }
    }
}
