﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.Portal
{
    public class WarehousePortal
    {
        [Key]
        [Column(Order = 0)]
        public int IdCompany { get; set; }

        [Key]
        [Column(Order = 1)]
        public string WhsCode { get; set; }

        public virtual CompanyConn CompanyConn { get; set; }
    }
}
