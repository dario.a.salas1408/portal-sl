﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations.Portal
{
    [Table("PortalChartParamsType")]
    public partial class PortalChartParamsType
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string ParamTypeKey { get; set; }

        [StringLength(50)]
        public string ParamTypeDesc { get; set; }
    }
}
