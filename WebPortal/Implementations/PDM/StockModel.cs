﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class StockModel : IStockModel
    {
        public StockModel()
        {

        }

        public StockModel(string pItemCode, decimal? pOnHand, decimal? pOnOrder, decimal? pIsCommited)
        {
            this.ItemCode = pItemCode;
            this.OnHand = pOnHand;
            this.OnOrder = pOnOrder;
            this.IsCommited = pIsCommited;
        }

        public string ItemCode {get;set;}
        public string WhsCode {get;set;}
        public string WhsName { get; set; }
        public decimal? OnHand {get;set;}
        public decimal? IsCommited { get; set; }
        public decimal? OnOrder { get; set; }
        public string U_ARGNS_SIZE {get;set;}
        public short? U_ARGNS_SIZEVO { get; set; }
        public string U_ARGNS_COL {get;set;}
        public string U_ARGNS_VAR {get;set;}
        public string U_SizeCode { get; set; }
        public decimal? Total { get; set; }
        public decimal Available { get; set; }
        public decimal? OnHandPrePack { get; set; }
        public decimal? TotalPrepack { get; set; }
        public string U_ARGNS_ITYPE { get; set; }
    }

}
