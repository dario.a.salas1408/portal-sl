﻿using ARGNS.Model.Interfaces.PDM;

namespace ARGNS.Model.Implementations.PDM
{
    public class SkuModel : ISkuModel
    {
        public string ItemCode { get; set; }
        public string U_ARGNS_SIZE { get; set; }
        public string U_ARGNS_COL { get; set; }
        public string U_ARGNS_VAR { get; set; }
        public string ColorDesc { get; set; }
        public string VarDesc { get; set; }
        public string U_ARGNS_ITYPE { get; set; }
        public short? U_ARGNS_SIZEVO { get; set; }
        public string U_ARGNS_SCL { get; set; }
        //Agregados
        public string U_PpDesc { get; set; }
        public string U_PpSizeRun { get; set; }
        public bool HasStock { get; set; }
    }
}
