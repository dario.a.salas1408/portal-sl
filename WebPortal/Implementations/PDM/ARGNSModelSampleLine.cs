﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelSampleLine : IARGNSModelSampleLine
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string Object { get; set; }
        public int LogInst { get; set; }
        public string U_SclCode { get; set; }
        public string U_POM { get; set; }
        public string U_Desc { get; set; }
        public string U_SizeCode { get; set; }
        public string U_SizeDesc { get; set; }
        public decimal U_Target { get; set; }
        public decimal U_Actual { get; set; }
        public string U_Revision { get; set; }
    }
}
