﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSContainer
    {
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public int? Period { get; set; }

        public short? Instance { get; set; }

        public int? Series { get; set; }
        
        public string Handwrtten { get; set; }
        
        public string Canceled { get; set; }
        
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }
        
        public string Transfered { get; set; }
        
        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }
        
        public string DataSource { get; set; }
        
        public string RequestStatus { get; set; }
        
        public string Creator { get; set; }
        
        public string Remark { get; set; }
        
        public string U_Shipment { get; set; }
        
        public string U_Broker { get; set; }
        
        public string U_Status { get; set; }
        
        public string U_ShipVia { get; set; }

        public DateTime? U_ESD { get; set; }

        public DateTime? U_EDA { get; set; }

        public DateTime? U_EDW { get; set; }

        public DateTime? U_ASD { get; set; }

        public DateTime? U_ADA { get; set; }

        public DateTime? U_ADW { get; set; }
        
        public decimal? U_Quantity { get; set; }
        
        public decimal? U_Amount { get; set; }
        
        public decimal? U_Weight { get; set; }
        
        public decimal? U_Volume { get; set; }
        
        public decimal? U_Carton { get; set; }
        
        public string U_POE { get; set; }

        public string U_UserTxt1 { get; set; }

        public string U_UserTxt2 { get; set; }

        public string U_UserTxt3 { get; set; }

        public string U_UserTxt4 { get; set; }

        public string U_ObjType { get; set; }

        //Others
        public List<ARGNSContainerLine> Lines { get; set; }
        public List<ARGNSContainerStatus> ListStatus { get; set; }
        public List<ARGNSContainerPort> ListPort { get; set; }
        public List<ARGNSContainerPackage> ListPackage { get; set; }
        public List<ARGNSPSlipPackage> ListPackaginSlip { get; set; }
        public List<Freight> ListFreight { get; set; }
    }
}
