﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelFitt
    {
        public string Code {get; set;}
        public int LineId {get; set;}
        public string Object {get; set;}
        public int? LogInst {get; set;}
        public string U_ModCode {get; set;}
        public string U_FittCod {get; set;}
        public string U_Fitting { get; set; }
    }
}
