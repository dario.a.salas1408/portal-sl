﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSColor : IARGNSColor
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ColCode { get; set; }
        public string U_ColDesc { get; set; }
        public string U_Shade { get; set; }
        public string U_NRFCode { get; set; }
        public string U_Active { get; set; }
        public string U_Argb { get; set; }
        public string U_Pic { get; set; }
        public string U_Attrib1 { get; set; }
        public string U_Attrib2 { get; set; }
    }
}
