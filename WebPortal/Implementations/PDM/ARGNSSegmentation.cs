﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSegmentation : IARGNSSegmentation
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_UseMod { get; set; }      
        public string U_UseCol { get; set; }     
        public string U_UseScl { get; set; }       
        public string U_UseVar { get; set; }        
        public string U_Active { get; set; }
        public short? U_ModLen { get; set; }
        public short? U_ColLen { get; set; }
        public short? U_SizeLen { get; set; }
        public short? U_VarLen { get; set; }
    }
}
