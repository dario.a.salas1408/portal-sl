﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSPDC
    {
        public ARGNSPDC()
        {
            this.Lines = new List<ARGNSPDCLine>();
        }

        public string Code { get; set; }
        public string DocEntry { get; set; }
        public DateTime? U_Date { get; set; }
        public int? U_empID { get; set; }
        public string U_ShiftCode { get; set; }
        public string U_MinPres { get; set; }
        public string U_UnprodMins { get; set; }
        public string U_Vendor { get; set; }
        public string U_Branch { get; set; }
        public List<ARGNSPDCLine> Lines {get; set;}
    }
}
