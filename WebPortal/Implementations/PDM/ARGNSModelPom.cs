﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelPom : IARGNSModelPom
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string U_Desc { get; set; }
        public string U_SizeCode { get; set; }
        public string U_SizeDesc { get; set; }
        public decimal? U_Value { get; set; }
        public decimal? U_TolPosit { get; set; }
        public decimal? U_TolNeg { get; set; }
        public string U_QAPoint { get; set; }
        public string U_POM { get; set; }
        public string Object { get; set; }
        public string LogInst { get; set; }
        public string U_SclPom { get; set; }
        public string U_PomCode { get; set; }
        public bool IsNew { get; set; }
    }
}
