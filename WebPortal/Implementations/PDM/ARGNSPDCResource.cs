﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
   public  class ARGNSPDCResource
    {
        public string U_WorkCePrCode { get; set; }
        public string U_ResCode { get; set; }
        public string U_ResName { get; set; }
    }
}
