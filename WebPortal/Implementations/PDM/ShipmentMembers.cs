﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ShipmentMembers
    {
        public ShipmentMembers()
        {
            this.ListStatus = new List<ARGNSContainerStatus>();
            this.ListPort = new List<ARGNSContainerPort>();
            this.ListFreight = new List<Freight>();
        }

        public List<ARGNSContainerStatus> ListStatus { get; set; }
        public List<ARGNSContainerPort> ListPort { get; set; }
        public List<Freight> ListFreight { get; set; }
    }
}
