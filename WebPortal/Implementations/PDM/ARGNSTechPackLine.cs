﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSTechPackLine
    {
        public string Code { get; set; }
        
        public int LineId { get; set; }
        
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_Sector { get; set; }

        public string U_ModCode { get; set; }

        public string U_InstCod { get; set; }

        public string U_Instuct { get; set; }

        public string U_Descrip { get; set; }

        public string U_Imag { get; set; }

        public string U_Version { get; set; }

        public string U_Active { get; set; }

        //Agregados
        public bool IsNew { get; set; }
        public bool ImgChanged { get; set; }
    }
}
