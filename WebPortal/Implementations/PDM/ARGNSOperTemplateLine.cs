﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSOperTemplateLine
    {
        public string Code { get; set; }
        
        public int LineId { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_ItemCode { get; set; }

        public string U_ItemName { get; set; }

        public decimal? U_Quantity { get; set; }

        public string U_UoM { get; set; }

        public short? U_PList { get; set; }

        public decimal? U_Price { get; set; }

        public string U_EndProd { get; set; }

        public string U_WorkCtr { get; set; }

        public string U_ResourID { get; set; }

        public string U_ResName { get; set; }

        public string U_Currency { get; set; }

        public short? U_TRAPlatz { get; set; }

        public short? U_TR2APlat { get; set; }

        public decimal? U_TEAPlatz { get; set; }

        public short? U_menge_je { get; set; }

        public short? U_menge_ze { get; set; }

        public short? U_nutzen { get; set; }

        public short? U_anzahl { get; set; }

        public string U_bde { get; set; }

        public short? U_SAM { get; set; }

        public string U_ResCode { get; set; }

        public short? U_LeadTime { get; set; }

        public int? U_Seconds { get; set; }

        public string U_SOTCode { get; set; }
    }
}
