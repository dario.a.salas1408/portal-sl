﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelSample : IARGNSModelSample
    {
        
        public ARGNSModelSample()
        {
            ListSampleVal = new List<ARGNS_SampleVal>();
            ListUser = new List<UserSAP>();
            ListPOM = new List<ARGNSModelPomTemplate>();
            ListScale = new List<ARGNSScale>();
            ListLine = new List<ARGNSModelSampleLine>();
        }
        
        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ModCode { get; set; }
        public string U_Type { get; set; }
        public string U_SampCode { get; set; }
        public string U_Status { get; set; }
        public string U_SclCode { get; set; }
        public string U_POM { get; set; }
        public DateTime U_Date { get; set; }
        public string U_User { get; set; }
        public DateTime U_ApproDate { get; set; }
        public string U_Active { get; set; }
        public string U_Comments { get; set; }
        public string U_BPCustomer { get; set; }
        public string U_BPSupp { get; set; }
        public string U_Picture { get; set; }
        public List<ARGNS_SampleVal> ListSampleVal { get; set; }
        public List<UserSAP> ListUser { get; set; }
        public List<ARGNSModelPomTemplate> ListPOM { get; set; }
        public List<ARGNSScale> ListScale { get; set; }
        public List<ARGNSModelSampleLine> ListLine { get; set; }
        public string USER_CODE { get; set; }
    }
}
