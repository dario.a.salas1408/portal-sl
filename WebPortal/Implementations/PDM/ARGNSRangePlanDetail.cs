﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSRangePlanDetail
    {
        public string Code { get; set; }

        public int LineId { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_ModCode { get; set; }

        public string U_ModDesc { get; set; }

        public string U_Season { get; set; }

        public string U_Coll { get; set; }

        public string U_ProdLine { get; set; }

        public string U_ProdGrou { get; set; }

        public string U_Brand { get; set; }

        public string U_ModPic { get; set; }

        public decimal? U_ModPrice { get; set; }

        public string U_ModCol { get; set; }

        public string U_Page { get; set; }

        public string U_PicCode { get; set; }

        public string U_PicSize { get; set; }
        public string U_Comments { get; set; }
        
    }
}
