﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelComment : IARGNSModelComment
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_USER { get; set; }
        public string U_COMMENT { get; set; }
        public string U_FILE { get; set; }
        public string U_FILEPATH { get; set; }
        public string U_MODCODE { get; set; }
        public System.DateTime U_DATE { get; set; }
    }
}
