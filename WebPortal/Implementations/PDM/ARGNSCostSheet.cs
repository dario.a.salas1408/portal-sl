﻿using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSCostSheet : IARGNSCostSheet
    {
        public ARGNSCostSheet()
        {
            CostSheetCombo = new CostSheetComboSAP();
        }

        public string Code { get; set; }

        public string Name { get; set; }

        public int DocEntry { get; set; }

        public string U_ModCode { get; set; }

        public string U_Currency { get; set; }

        public string U_Description { get; set; }

        public string U_OTCode { get; set; }

        public string U_SchCode { get; set; }
        public string U_CodeTmpl { get; set; }

        public decimal? U_PurchPrice { get; set; }

        public decimal? U_TMaterials { get; set; }

        public decimal? U_TOperations { get; set; }

        public decimal? U_IndrCost { get; set; }

        public decimal? U_TCost { get; set; }

        public decimal? U_CostPercent { get; set; }

        public decimal? U_SlsPercent { get; set; }

        public decimal? U_StrFactor { get; set; }

        public decimal? U_FPrice { get; set; }

        public decimal? U_Duty { get; set; }

        public decimal? U_IntTransp { get; set; }

        public decimal? U_Freight { get; set; }

        public decimal? U_Margin { get; set; }

        public decimal? U_DDP { get; set; }

        public decimal? U_TMargin { get; set; }

        public decimal? U_CIF { get; set; }

        public string U_UserText1 { get; set; }

        public string U_UserText2 { get; set; }

        public string U_UserText3 { get; set; }

        //Agregados en el view
        public List<ARGNSCSMaterial> ListMaterial { get; set; }
        public List<ARGNSCSOperation> ListOperation { get; set; }
        public List<ARGNSCSSchema> ListSchema { get; set; }
        public List<ARGNSCSPatterns> ListPattern { get; set; }
        public CostSheetComboSAP CostSheetCombo { get; set; }
        public string ModeDBCode { get; set; }
        public int LineId { get; set; }
        public string Object { get; set; }
        public string LogInst { get; set; }
        public string U_CSCode { get; set; }
        public string U_Desc { get; set; }
        public string U_Selected { get; set; }
        public string U_Customer { get; set; }
        public string U_Owner { get; set; }
        public CostSheetUDF mCostSheetUDF { get; set; }

        //Agregados
        public string LocalCurrency { get; set; }
    }
}
