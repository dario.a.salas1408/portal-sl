﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelLogo
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string Object { get; set; }
        public int? LogInst { get; set; }
        public string U_CodeLogo { get; set; }
        public string U_LogDesc { get; set; }
        public string U_Location { get; set; }
        public string U_Position { get; set; }
        public string U_ModColor { get; set; }
        public string U_CodColow { get; set; }
        public string U_Colorway { get; set; }
        public string U_Variable { get; set; }
    }
}
