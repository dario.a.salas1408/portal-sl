﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSendOutPar : IARGNSSendOutPar
    {      
        public int DocEntry { get; set; }
        public int LineId { get; set; }    
        public string U_BarCode { get; set; } 
        public string U_Rework { get; set; }
    }
}
