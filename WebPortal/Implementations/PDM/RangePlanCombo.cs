﻿using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class RangePlanCombo
    {
        public RangePlanCombo()
        {
            this.ListSeason = new List<ARGNSSeason>();
            this.ListCollection = new List<ARGNSCollection>();
            this.ListProductLine = new List<ARGNSProductLine>();
            this.ListModelGroup = new List<ARGNSModelGroup>();
            this.ListBrand = new List<ARGNSBrand>();
            this.ListDivision = new List<ARGNSDivision>();
            this.ListSubCollection = new List<ARGNSSubCollection>();
            this.ListProject = new List<ProjectSAP>();
            this.ListEmployee = new List<EmployeeSAP>();
        }

        public List<ARGNSSeason> ListSeason { get; set; }
        public List<ARGNSCollection> ListCollection { get; set; }
        public List<ARGNSProductLine> ListProductLine { get; set; }
        public List<ARGNSModelGroup> ListModelGroup { get; set; }
        public List<ARGNSBrand> ListBrand { get; set; }
        public List<ARGNSDivision> ListDivision { get; set; }
        public List<ARGNSSubCollection> ListSubCollection { get; set; }
        public List<EmployeeSAP> ListEmployee { get; set; }
        public List<ProjectSAP> ListProject { get; set; }

    }
}
