﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSubCollection : IARGNSSubCollection
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string U_AmbCode { get; set; }
        public string U_Desc { get; set; }
        public string U_DetDesc { get; set; }
        public string U_Collect { get; set; }
    }
}
