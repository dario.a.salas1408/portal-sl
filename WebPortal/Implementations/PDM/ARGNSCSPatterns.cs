﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSCSPatterns
    {
        public ARGNSCSPatterns()
        {

        }

        public ARGNSCSPatterns(int pNextId, List<UDF_ARGNS> mUDFList)
        {
            this.LineId = pNextId;
            this.IsNew = true;
            this.MappedUdf = mUDFList;
        }

        public string Code { get; set; }
        
        public int LineId { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_PattCode { get; set; }

        public string U_Desc { get; set; }

        public string U_ItemCode { get; set; }

        public string U_ItemName { get; set; }

        public decimal? U_Quantity { get; set; }

        public string U_UoM { get; set; }

        public decimal? U_PattQty { get; set; }

        public string U_Active { get; set; }

        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
