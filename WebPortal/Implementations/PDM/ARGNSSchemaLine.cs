﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSchemaLine
    {
        public string Code { get; set; }
        
        public int LineId { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_ItemCode { get; set; }

        public string U_ItemName { get; set; }

        public short? U_PList { get; set; }

        public decimal? U_Percent { get; set; }

        public decimal? U_Amount { get; set; }

        public string U_Currency { get; set; }

        public string U_AplicTo { get; set; }
    }
}
