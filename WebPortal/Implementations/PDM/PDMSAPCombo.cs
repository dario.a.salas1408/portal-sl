﻿using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class PDMSAPCombo : IPDMSAPCombo
    {
        public PDMSAPCombo()
        {
            ListStatus = new List<ARGNSStyleStatus>();
            ListStatus.Add(new ARGNSStyleStatus());

            ListProductLine = new List<ARGNSProductLine>();
            ListProductLine.Add(new ARGNSProductLine());

            ListModelGroup = new List<ARGNSModelGroup>();
            ListModelGroup.Add(new ARGNSModelGroup());

            ListItemGroup = new List<ItemGroup>();

            ListSeason = new List<ARGNSSeason>();
            ListSeason.Add(new ARGNSSeason());

            ListBrand = new List<ARGNSBrand>();
            ListBrand.Add(new ARGNSBrand());

            ListDivision = new List<ARGNSDivision>();
            ListDivision.Add(new ARGNSDivision());

            ListYear = new List<ARGNSYear>();
            ListYear.Add(new ARGNSYear());

            PriceList = new List<PriceListSAP>();
            PriceList.Add(new PriceListSAP());

            ListSegmentation = new List<ARGNSSegmentation>();
            ListSegmentation.Add(new ARGNSSegmentation());

            ListCollection = new List<ARGNSCollection>();
            ListCollection.Add(new ARGNSCollection());

            ListSubCollection = new List<ARGNSSubCollection>();
            ListSubCollection.Add(new ARGNSSubCollection());

            CompositionList = new List<ARGNSComposition>();
            CompositionList.Add(new ARGNSComposition());

            SimbolList = new List<ARGNSSimbol>();
            SimbolList.Add(new ARGNSSimbol());

            BusinessPartnerList = new List<BusinessPartnerSAP>();
            BusinessPartnerList.Add(new BusinessPartnerSAP());

            CurrencyList = new List<CurrencySAP>();
            CurrencyList.Add(new CurrencySAP());

            DocNumbering = new List<ARGNSDocNumber>();

            DesignerList = new List<EmployeeSAP>();
            DesignerList.Add(new EmployeeSAP());
        }

        public List<ARGNSStyleStatus> ListStatus { get; set; }
        public List<ARGNSProductLine> ListProductLine { get; set; }
        public List<ARGNSModelGroup> ListModelGroup { get; set; }
        public List<ItemGroup> ListItemGroup { get; set; }
        public List<ARGNSSeason> ListSeason { get; set; }
        public List<ARGNSBrand> ListBrand { get; set; }
        public List<ARGNSDivision> ListDivision { get; set; }
        public List<ARGNSYear> ListYear { get; set; }
        public List<PriceListSAP> PriceList { get; set; }
        public List<ARGNSSegmentation> ListSegmentation { get; set; }
        public List<ARGNSCollection> ListCollection { get; set; }
        public List<ARGNSSubCollection> ListSubCollection { get; set; }
        public List<ARGNSComposition> CompositionList { get; set; }
        public List<ARGNSSimbol> SimbolList { get; set; }
        public List<BusinessPartnerSAP> BusinessPartnerList { get; set; }
        public List<CurrencySAP> CurrencyList { get; set; }
        public List<ARGNSDocNumber> DocNumbering { get; set; }
        public List<EmployeeSAP> DesignerList { get; set; }
    }
}
