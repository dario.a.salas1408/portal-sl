﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSPatternTemplateLine
    {
        public string Code { get; set; }

        public int LineId { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_PattCode { get; set; }

        public string U_Desc { get; set; }

        public decimal? U_PattQty { get; set; }

        public string U_Active { get; set; }
    }
}
