﻿namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSizeRunLine
    {
        public string Code { get; set; }

        public int LineId { get; set; }
        
        public string U_SizeRunCode { get; set; }

        public string U_SizeCode { get; set; }

        public short? U_Percent { get; set; }

        public short? U_Qty { get; set; }
    }
}
