﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSContainerLine
    {
        public int DocEntry { get; set; }
        
        public int LineId { get; set; }

        public int? VisOrder { get; set; }
        
        public string Object { get; set; }

        public int? LogInst { get; set; }
        
        public string U_BaseEntry { get; set; }
        
        public string U_BaseDocNo { get; set; }

        public short? U_BaseLine { get; set; }
        
        public string U_ItemCode { get; set; }
        
        public string U_ItemName { get; set; }
        
        public string U_WhsCode { get; set; }
        
        public string U_UoM { get; set; }

        public DateTime? U_ESD { get; set; }

        public DateTime? U_EDA { get; set; }
        
        public decimal? U_Price { get; set; }
        
        public string U_ObjType { get; set; }
        
        public string U_LineStatus { get; set; }
        
        public string U_CardCode { get; set; }
        
        public string U_CardName { get; set; }
        
        public string U_ARGNS_MOD { get; set; }
        
        public string U_ARGNS_COL { get; set; }
        
        public string U_ARGNS_SIZE { get; set; }
        
        public string U_ItemGroup { get; set; }
        
        public string U_ARGNS_SEASON { get; set; }
        
        public string U_UserTxt1 { get; set; }
        
        public string U_UserTxt2 { get; set; }
        
        public string U_UserTxt3 { get; set; }
        
        public string U_UserTxt4 { get; set; }

        public DraftLine DraftLine { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
