﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSPSlipPackage
    {
        public int DocEntry { get; set; }
        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public short? U_PackageNum { get; set; }

        public string U_PackageTyp { get; set; }

        public decimal? U_Weight { get; set; }

        public string U_WeightUnit { get; set; }

        public string U_ARGNS_UCC128 { get; set; }

        public string U_ARGNS_TRACKN { get; set; }

        public short? U_CntnerNum { get; set; }

        //Lines
        public List<ARGNSPSlipPackageLine> Lines { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
