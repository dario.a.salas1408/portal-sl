﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.MaterialDetails
{
    public class ARGNSMDFabric
    {
        public ARGNSMDFabric()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        public ARGNSMDFabric(int pNextId, List<UDF_ARGNS> mUDFList)
        {
            this.LineId = pNextId;
            this.IsNew = true;
            this.MappedUdf = mUDFList;
        }

        public string Code { get; set; }
        
        public int LineId { get; set; }
        
        public string U_CompCode { get; set; }
        
        public string U_ColCode { get; set; }

        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
