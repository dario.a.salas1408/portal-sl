﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.MaterialDetails
{
    public class ARGNSMDFootwearAccess
    {
        public ARGNSMDFootwearAccess()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        public ARGNSMDFootwearAccess(int pNextId, List<UDF_ARGNS> mUDFList)
        {
            this.LineId = pNextId;
            this.IsNew = true;
            this.MappedUdf = mUDFList;
        }

        public string Code { get; set; }
        
        public int LineId { get; set; }
        
        public string U_SLaces { get; set; }
        
        public string U_Eyelets { get; set; }
        
        public string U_Buckles { get; set; }
        
        public string U_Enclosures { get; set; }
        
        public string U_Labelling { get; set; }
        
        public string U_Others { get; set; }

        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
