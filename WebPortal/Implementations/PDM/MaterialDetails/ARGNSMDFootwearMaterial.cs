﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.MaterialDetails
{
    public class ARGNSMDFootwearMaterial
    {
        public ARGNSMDFootwearMaterial()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        public ARGNSMDFootwearMaterial(int pNextId, List<UDF_ARGNS> mUDFList)
        {
            this.LineId = pNextId;
            this.IsNew = true;
            this.MappedUdf = mUDFList;
        }

        public string Code { get; set; }
        
        public int LineId { get; set; }
        
        public string U_Sfabric { get; set; }
        
        public string U_Lining { get; set; }
        
        public string U_Insole { get; set; }
        
        public string U_Outsole { get; set; }
        
        public string U_Heel { get; set; }

        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
