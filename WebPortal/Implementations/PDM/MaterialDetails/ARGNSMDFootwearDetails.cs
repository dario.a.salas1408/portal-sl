﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.MaterialDetails
{
    public class ARGNSMDFootwearDetails
    {
        public ARGNSMDFootwearDetails()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        public ARGNSMDFootwearDetails(int pNextId, List<UDF_ARGNS> mUDFList)
        {
            this.LineId = pNextId;
            this.IsNew = true;
            this.MappedUdf = mUDFList;
        }

        public string Code { get; set; }
        
        public int LineId { get; set; }
        
        public string Object { get; set; }

        public int? LogInst { get; set; }
        
        public string U_HHeight { get; set; }
        
        public string U_BHeight { get; set; }
        
        public string U_BWidth { get; set; }
        
        public string U_Strap { get; set; }
        
        public string U_Seams { get; set; }
        
        public string U_Others { get; set; }

        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
