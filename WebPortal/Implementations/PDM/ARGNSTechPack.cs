﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSTechPack
    {
        public string Code { get; set; }

        public string ModelCode { get; set; }

        public string ModelDesc { get; set; }

        public List<ARGNSTechPackSection> Sections { get; set; }

        public List<ARGNSTechPackLine> Lines { get; set; }
    }
}
