﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelBom
    {
        public string Code {get; set;}
        public int LineId {get; set;}
        public string Object {get; set;}
        public int? LogInst {get; set;}
        public string U_ItemCode {get; set;}
        public decimal? U_Quantity {get; set;}
        public string U_UOM {get; set;}
        public string U_Whse {get; set;}
        public string U_IssueMth {get; set;}
        public short? U_PList {get; set;}
        public decimal? U_UPrice { get; set; }
        public decimal? U_Total { get; set; }
        public string U_Comments {get; set;}
        public string U_TreeType {get; set;}
        public string U_PVendor { get; set; }
    }
}
