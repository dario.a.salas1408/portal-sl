﻿using ARGNS.Model.Interfaces.PDM;
using ARGNS.Model.Interfaces.PDM.ComboList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSCollection : IARGNSCollection
    {
        public ARGNSCollection()
        {
                
        }

        public ARGNSCollection(string Code, string U_Desc)
        {
            this.Code = Code;
            this.U_Desc = U_Desc;
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_CollCode { get; set; }
        public string U_Desc { get; set; }
        public string U_Season { get; set; }
    }
}
