﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSMODVENDOR : IARGNSMODVENDOR
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string U_ModCode { get; set; }
        public string U_CardCode { get; set; }
        public string U_CardType { get; set; }
    }
}
