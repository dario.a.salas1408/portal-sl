﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSRoutingLine
    {
        public string Code { get; set; }
        
        public int LineId { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_Workflow { get; set; }

        public string U_WorkfDes { get; set; }

        public string U_Depart { get; set; }

        public short? U_LeadTime { get; set; }

        public string U_Active { get; set; }

        public string U_Role { get; set; }

        public string U_Manager { get; set; }

        public string U_User { get; set; }

        public short? U_LTime { get; set; }

        public string U_Usrname { get; set; }
    }
}
