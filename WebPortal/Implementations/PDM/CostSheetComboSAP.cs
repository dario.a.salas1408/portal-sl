﻿using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class CostSheetComboSAP : ICostSheetComboSAP
    {
        public List<PriceListSAP> PriceList { get; set; }
        public List<CurrencySAP> CurrencyList { get; set; }
        public List<BusinessPartnerSAP> VendorList { get; set; }
    }
}
