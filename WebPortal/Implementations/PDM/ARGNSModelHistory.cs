﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelHistory
    {
        public string Code { get; set; }
        public int LogInst { get; set; }
        public DateTime UpdateDate { get; set; }
        public int? UserSign { get; set; }
        public string UserSignName { get; set; }
    }
}
