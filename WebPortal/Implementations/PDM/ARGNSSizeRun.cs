﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSizeRun
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public int DocEntry { get; set; }

        public string U_SizeRunCode { get; set; }

        public string U_SizeRunDesc { get; set; }

        public string U_SclCode { get; set; }

        public string U_CustCode { get; set; }

        public string U_Country { get; set; }

        public string U_Active { get; set; }

        public string U_shipto { get; set; }

        //Agregados

        public List<ARGNSSizeRunLine> Lines { get; set; }
    }
}
