﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelPriceList
    {
        
        public string Code { get; set; }    
        public string Name { get; set; }      
        public string U_ModCode { get; set; }
        public short? U_ListNum { get; set; }      
        public string U_ListName { get; set; }      
        public decimal? U_Price { get; set; }     
        public string U_DefList { get; set; }        
        public string U_BasList { get; set; }
        public short? U_BASE_NUM { get; set; }        
        public decimal? U_Factor { get; set; }       
        public decimal? U_BasPrice { get; set; }       
        public string U_Manual { get; set; }       
        public string U_Currency { get; set; }
        public int DocEntry { get; set; }
    }
}
