﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSWorkCenter
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
