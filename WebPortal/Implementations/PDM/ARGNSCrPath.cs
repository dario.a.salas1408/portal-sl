﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSCrPath : IARGNSCrPath
    {
        public ARGNSCrPath()
        {
            ActivitiesList = new List<ARGNSCrPathActivities>();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ProyCode { get; set; }
        public string U_Desc { get; set; }
        public string U_Model { get; set; }
        public string U_Status { get; set; }
        public string U_Planning { get; set; }
        public List<ARGNSCrPathActivities> ActivitiesList { get; set; }

        //Descripciones
        public string SeasonDesc { get; set; }
        public string CollectionDesc { get; set; }
        public string SubCollDesc { get; set; }
        public string ModelPicture { get; set; }
        public string ModelDesc { get; set; }

        //Sales Order Info
        public string SalesOrderNum { get; set; }
        public string SalesCardCode { get; set; }
        public string SalesCardName { get; set; }
        public DateTime? SalesPostingDate { get; set; }
        public DateTime? SalesDelivDate { get; set; }
        public decimal? SalesQty { get; set; }
        public string U_Vendor {get; set; }
        public string U_Customer { get; set; }
       
    }
}
