﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSShifts
    {
        public ARGNSShifts()
        {
                
        }
        public ARGNSShifts(string Code, string U_Desc, string U_ShiftCode)
        {
            this.Code = Code;
            this.U_Desc = U_Desc;
            this.U_ShiftCode = U_ShiftCode;
        }

        public string Code { get; set; }
        public string DocEntry { get; set; }
        public string U_ShiftCode { get; set; }
        public string U_Desc { get; set; }
        public string U_TotProdHour { get; set; }
        public string U_TotOverHour { get; set; }
        public string U_Active { get; set; }
    }
}
