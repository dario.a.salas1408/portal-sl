﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSAprtxSetup
    {
        public string Code { get; set; }

        public string U_SBODec { get; set; }

        public short? U_BOMDSPL { get; set; }

        public string U_BPDef { get; set; }
    }
}
