﻿using ARGNS.Model.Interfaces.PDM;

namespace ARGNS.Model.Implementations.PDM
{
    public class ModelDesc : IModelDesc
    {
        public string Code { get; set; }
        public string ModelCode { get; set; }
        public string ModDescription { get; set; }
        public string SeasonDesc { get; set; }
        public string CollDesc { get; set; }
        public string SubCollDesc { get; set; }
        public string BrandDesc { get; set; }
        public string ProdGroupDesc { get; set; }
        public string ModelImage { get; set; }
        public decimal? Modelprice { get; set; }
        public decimal? Stock { get; set; }
        public string ImageName { get; set; }
        public bool HasPermissionToViewStock { get; set; }
        public string CatalogCode { get; set; }
        public decimal? StockPrepack { get; set; }
    }
}
