﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSPDCLine
    {
        public string Code { get; set; }
        public string LineId { get; set; }
        public string Object { get; set; }
        public string LogInst { get; set; }
        public string U_CodeBars { get; set; }
        public string U_ProdOrd { get; set; }
        public string U_Model { get; set; }
        public string U_Color { get; set; }
        public string U_Scale { get; set; }
        public string U_Size { get; set; }
        public string U_Variable { get; set; }
        public string U_WorkCtr { get; set; }
        public string U_ResourID { get; set; }
        public string U_ResName { get; set; }
        public string U_OperCode { get; set; }
        public string U_Qty { get; set; }
        public string U_BadQty { get; set; }
        public string U_Stage { get; set; }
        //Agregados 
        public string U_IssQty { get; set; }
        public string LQty { get; set; }
    }
}
