﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSRouting
    {
        public string Code { get; set; }

        public string U_RoutCode { get; set; }

        public string U_Desc { get; set; }
    }
}
