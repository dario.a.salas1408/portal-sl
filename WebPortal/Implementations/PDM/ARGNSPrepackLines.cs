﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSPrepackLines
    {
        public string Code { get; set; }
        
        public int LineId { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public string U_SizeCode { get; set; }

        public string U_SizeDesc { get; set; }

        public string U_ColCode { get; set; }

        public string U_ColDesc { get; set; }

        public decimal? U_Value { get; set; }

        public string U_VarCode { get; set; }

        public string U_VarDesc { get; set; }

        public string U_SclCode { get; set; }

        public string U_Sruncode { get; set; }
    }
}
