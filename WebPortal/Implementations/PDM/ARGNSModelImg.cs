﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelImg : IARGNSModelImg
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string U_ModCode { get; set; }
        public string U_File { get; set; }
        public string U_Path { get; set; }
        public string Object { get; set; }
        public string LogInst { get; set; }
        public string U_FType { get; set; }
    }
}
