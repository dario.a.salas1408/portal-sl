﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSContainerPort
    {
        public ARGNSContainerPort()
        {

        }

        public ARGNSContainerPort(string Code, string U_Description)
        {
            this.Code = Code;
            this.U_Description = U_Description;
        }

        public string Code { get; set; }
        
        public string Name { get; set; }
        
        public string U_Description { get; set; }
    }
}
