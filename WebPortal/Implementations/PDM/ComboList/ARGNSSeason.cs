﻿using ARGNS.Model.Interfaces.PDM.ComboList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.ComboList
{
    public class ARGNSSeason : IARGNSSeason
    {
        public ARGNSSeason()
        {
                
        }

        public ARGNSSeason(string Code, string Name)
        {
            this.Code = Code;
            this.Name = Name;
        }

        public string Code { get; set; }
        public string Name { get; set; }
    }
}
