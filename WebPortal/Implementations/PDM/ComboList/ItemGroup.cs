﻿using ARGNS.Model.Interfaces.PDM.ComboList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.ComboList
{
    public class ItemGroup : IItemGroup
    {
        public short ItmsGrpCod { get; set; }
        public string ItmsGrpNam { get; set; }
    }
}
