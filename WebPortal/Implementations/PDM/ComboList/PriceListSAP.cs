﻿using ARGNS.Model.Interfaces.PDM.ComboList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.ComboList
{
    public class PriceListSAP : IPriceList
    {
        public short ListNum { get; set; }
        public string ListName { get; set; }
        public Nullable<short> BASE_NUM { get; set; }
        public Nullable<decimal> Factor { get; set; }
        public Nullable<short> RoundSys { get; set; }
        public Nullable<short> GroupCode { get; set; }
    }
}
