﻿using ARGNS.Model.Interfaces.PDM.ComboList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM.ComboList
{
    public class ARGNSModelGroup : IARGNSModelGroup
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string U_NFRCode { get; set; }
        public string U_ProdLine { get; set; }
    }
}
