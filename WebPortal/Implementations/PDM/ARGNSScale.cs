﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSScale : IARGNSScale
    {

        public ARGNSScale()
        {
            SizeList = new List<ARGNSSize>();
        }
        public string Code { get; set; }
        public string Name { get; set; }
        public string U_SclCode { get; set; }
        public string U_SclDesc { get; set; }
        public string U_Active { get; set; }
        public Nullable<short> U_VOrder { get; set; }
        public List<ARGNSSize> SizeList { get; set; }
    }
}
