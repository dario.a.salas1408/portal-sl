﻿using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSCSMaterial : IARGNSCSMaterial
    {
        public ARGNSCSMaterial()
        {

        }
        public ARGNSCSMaterial(int pNextId, List<UDF_ARGNS> mUDFList)
        {
            this.LineId = pNextId;
            this.IsNew = true;
            this.MappedUdf = mUDFList;
        }

        public string Code { get; set; }

        public int LineId { get; set; }

        public string U_ItemCode { get; set; }

        public string U_ItemName { get; set; }

        public decimal? U_Quantity { get; set; }

        public string U_UoM { get; set; }

        public string U_Whse { get; set; }

        public decimal? U_UPrice { get; set; }

        public string U_TreeType { get; set; }

        public string U_PVendor { get; set; }
       
        public string U_OcrCode { get; set; }

        public string U_Currency { get; set; }

        public string U_CxT { get; set; }

        public string U_CxS { get; set; }

        public string U_CxC { get; set; }

        public string U_CxGT { get; set; }

        public string U_ItmLinkO { get; set; }

        public string U_IssueMth { get; set; }

        public short? U_PList { get; set; }

        public string U_Comments { get; set; }

        public int? LogInst { get; set; }

        public string Object { get; set; }

        public string U_FCode { get; set; }

        public decimal? U_QtyTime { get; set; }

        public decimal? U_Total { get; set; }

        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }

        //Para la view
        public bool IsNew { get; set; }
    }
}
