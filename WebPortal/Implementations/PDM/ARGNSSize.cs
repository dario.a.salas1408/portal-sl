﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSize : IARGNSSize
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string U_SizeCod { get; set; }
        public string U_SizeCode { get; set; }
        public string U_SizeDesc { get; set; }
        public Nullable<short> U_VOrder { get; set; }
        public string U_Active { get; set; }
        public string U_Selected { get; set; }
       
    }
}
