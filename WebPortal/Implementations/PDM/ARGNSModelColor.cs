﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelColor : IARGNSModelColor
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string Object { get; set; }
        public string LogInst { get; set; }
        public string U_ModCode { get; set; }
        public string U_ColCode { get; set; }
        public string U_Active { get; set; }
        public string ColorDesc { get; set; }
        public string U_Argb { get; set; }

        public string getColorRGB()
        {
            string ret = string.Empty;
            if (!string.IsNullOrEmpty(U_Argb))
            {
                Color color = Color.FromArgb(Convert.ToInt32(this.U_Argb.ToString()));
                ret = ("rgb(" + color.R + ", " + color.G + ", " + color.B + ")");
            }

            return ret;
        }
    }
}
