﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class PDCSAPCombo
    {
        public PDCSAPCombo()
        {
            ListShifts = new List<ARGNSShifts>();
            ListShifts.Add(new ARGNSShifts("0","","-1"));
            ListBusinessPartner = new List<BusinessPartnerSAP>();
            ListBusinessPartner.Add(new BusinessPartnerSAP());
            ListEmployee = new List<EmployeeSAP>();
            ListEmployee.Add(new EmployeeSAP(-1,"",""));
        }
        public List<ARGNSShifts> ListShifts { get; set; }
        public List<BusinessPartnerSAP> ListBusinessPartner { get; set; }
        public List<EmployeeSAP> ListEmployee { get; set; }
        public List<ARGNSWorkCenter> ListWorkCenter { get; set; }

    }
}
