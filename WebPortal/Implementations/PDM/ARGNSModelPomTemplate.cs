﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelPomTemplate : IARGNSModelPomTemplate
    {
        public string Code { get; set; }
        public string U_CodeTmpl { get; set; }
        public string U_Desc { get; set; }
        public string U_ProdGrp { get; set; }
        public string U_Active { get; set; }
        public string U_Scale { get; set; }
        public string U_SSize { get; set; }
    }
}
