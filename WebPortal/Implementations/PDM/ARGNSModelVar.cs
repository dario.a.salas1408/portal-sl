﻿using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModelVar : IARGNSModelVar
    {
        public string Code { get; set; }
        public int LineId { get; set; }
        public string U_ModCode { get; set; }
        public string U_VarCode { get; set; }
        public string U_Selected { get; set; }
        public string U_VarDesc { get; set; }
        public string U_ATGrp { get; set; }
        public string Object { get; set; }
        public string LogInst { get; set; }
        public string U_IntVCode { get; set; }

    }
}

