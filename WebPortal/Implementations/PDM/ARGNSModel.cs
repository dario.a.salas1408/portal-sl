﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.PDM;
using System;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSModel : IARGNSModel
    {

        public ARGNSModel()
        {
            ModelColorList = new List<ARGNSModelColor>();
            ModelSizeList = new List<ARGNSModelSize>();
            ModelVariableList = new List<ARGNSModelVar>();
            ModelImageList = new List<ARGNSModelImg>();
            ModelFileList = new List<ARGNSModelFile>();
            ModelStockList = new List<StockModel>();
            ModelPomList = new List<ARGNSModelPom>();
            ListPDMSAPCombo = new PDMSAPCombo();
            U_RMaterial = "N";
            ModelFittList = new List<ARGNSModelFitt>();
            ModelInstList = new List<ARGNSModelInst>();
            ModelBomList = new List<ARGNSModelBom>();
            ModelWFLOWList = new List<ARGNSModelWFLOW>();
            ModelLogosList = new List<ARGNSModelLogos>();
            ModelCSList = new List<ARGNSModelCostSheet>();
            ModelSegmentation = new ARGNSSegmentation();
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public int DocEntry { get; set; }
        public string U_ATGrp { get; set; }
        public string U_ModCode { get; set; }
        public string U_ModDesc { get; set; }
        public Nullable<System.DateTime> U_SSDate { get; set; }
        public Nullable<System.DateTime> U_SCDate { get; set; }
        public string U_Year { get; set; }
        public string U_COO { get; set; }
        public string U_Pic { get; set; }
        public string U_Active { get; set; }
        public Nullable<short> U_SapGrp { get; set; }
        public Nullable<short> U_PList { get; set; }
        public string U_Division { get; set; }
        public string U_Season { get; set; }
        public string U_FrgnDesc { get; set; }
        public string U_ModGrp { get; set; }
        public string U_SclCode { get; set; }
        public string U_LineCode { get; set; }
        public string U_Vendor { get; set; }
        public string U_MainWhs { get; set; }
        public string U_Comments { get; set; }
        public string U_Owner { get; set; }
        public string U_Approved { get; set; }
        public string U_PicR { get; set; }
        public Nullable<decimal> U_Price { get; set; }
        public string U_WhsNewI { get; set; }
        public string U_CollCode { get; set; }
        public string U_AmbCode { get; set; }
        public string U_CompCode { get; set; }
        public string U_Brand { get; set; }
        public string U_ChartCod { get; set; }
        public string U_Designer { get; set; }
        public string U_Customer { get; set; }
        public string U_GrpSCod { get; set; }
        public string U_Currency { get; set; }
        public string U_Status { get; set; }
        public string U_RMaterial { get; set; }
        public string U_DocNumb { get; set; }
        public List<ARGNSModelColor> ModelColorList { get; set; }
        public List<ARGNSModelSize> ModelSizeList { get; set; }
        public List<ARGNSModelVar> ModelVariableList { get; set; }
        public List<ARGNSModelImg> ModelImageList { get; set; }
        public List<ARGNSModelFile> ModelFileList { get; set; }
        public List<ARGNSModelPom> ModelPomList { get; set; }
        public PDMSAPCombo ListPDMSAPCombo { get; set; }
        public List<StockModel> ModelStockList { get; set; }
        public AdministrationApparel administrationApparel { get; set; }
        public AdministrationSAP administrationSAP { get; set; }
        public ARGNSSegmentation ModelSegmentation { get; set; }
        public string U_SclPOM { get; set; }
        public string U_CodePOM { get; set; }
        public List<ARGNSModelFitt> ModelFittList { get; set; }
        public List<ARGNSModelInst> ModelInstList { get; set; }
        public List<ARGNSModelBom> ModelBomList { get; set; }
        public List<ARGNSModelWFLOW> ModelWFLOWList { get; set; }
        public List<ARGNSModelLogos> ModelLogosList { get; set; }
        public List<ARGNSModelCostSheet> ModelCSList { get; set; }

        //ChangeLOG
        public short UpdateTime { get; set; }
        public string U_InsChart { get; set; }

        //Contenedor de udfs
        public List<UDF_ARGNS> MappedUdf { get; set; }

        public string ImageName { get; set; }
    }
}
