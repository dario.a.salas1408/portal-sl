﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSPSlipPackageType
    {
        public int PkgCode { get; set; }

        public string PkgType { get; set; }

        public decimal? Length1 { get; set; }

        public short? Len1Unit { get; set; }

        public decimal? length2 { get; set; }

        public short? Len2Unit { get; set; }

        public decimal? Width1 { get; set; }

        public short? Wdth1Unit { get; set; }

        public decimal? Width2 { get; set; }

        public short? Wdth2Unit { get; set; }

        public decimal? Height1 { get; set; }

        public short? Hght1Unit { get; set; }

        public decimal? Height2 { get; set; }

        public short? Hght2Unit { get; set; }

        public decimal? Volume { get; set; }

        public short? VolUnit { get; set; }

        public decimal? Weight1 { get; set; }

        public short? WghtUnit { get; set; }

        public decimal? Weight2 { get; set; }

        public short? Wght2Unit { get; set; }

        public string U_ARGNS_UNITS { get; set; }

        public string U_ARGNS_PCKCODE { get; set; }
    }
}
