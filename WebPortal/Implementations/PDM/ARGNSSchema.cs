﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Implementations.PDM
{
    public class ARGNSSchema
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public int DocEntry { get; set; }

        public string Canceled { get; set; }

        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        public string DataSource { get; set; }

        public string U_SchCode { get; set; }

        public string U_Active { get; set; }
    }
}
