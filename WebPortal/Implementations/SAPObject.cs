﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{
	public class SAPObject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdObject { get; set; }

        [StringLength(100)]
        public string ObjectName { get; set; }
        
        [StringLength(50)]
        public string ObjectTable { get; set; }

        public bool Visible { get; set; }
    }
}
