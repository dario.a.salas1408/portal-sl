﻿using ARGNS.Model.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ARGNS.Model.Implementations
{
	public partial class UsersSetting : IUsersSetting
    {
       
        [Column(Order = 0), Key, ForeignKey("User")]
        public int IdUser { get; set; }
        [Column(Order = 1), Key, ForeignKey("CompanyConn")]
        public int IdCompany { get; set; }
        public virtual CompanyConn CompanyConn { get; set; }
        public virtual User User { get; set; }
        public string IdBp { get; set; }
        public int? IdSE { get; set; }
        [StringLength(50)]
        public string UserNameSAP { get; set; }
        [StringLength(50)]
        public string UserPasswordSAP { get; set; }
        public string UserCodeSAP { get; set; }
        public string DftDistRule { get; set; }
        public string DftWhs { get; set; }       
        public string SalesTaxCodeDef { get; set; }
        public string PurchaseTaxCodeDef { get; set; }
        public int? UserGroupId { get; set; }
        public int? CostPriceList { get; set; }
        public int? BPGroupId { get; set; }
    }
}
