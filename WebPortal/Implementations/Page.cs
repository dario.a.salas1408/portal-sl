﻿using ARGNS.Model.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ARGNS.Model.Implementations
{
	public partial class Page:IPage
    {
        public Page()
        {
            RolPageActions = new HashSet<RolPageAction>();
            UserPageActions = new HashSet<UserPageAction>();
            Actions = new HashSet<Actions>();
            CRPageMaps = new HashSet<CRPageMap>();
        }

        [Key]
        public int IdPage { get; set; }
        public int IdAreaKeys { get; set; }
        public virtual AreaKey AreaKey { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        [StringLength(150)]
        public string Controller { get; set; }
        [StringLength(150)]
        public string Action { get; set; }
        [StringLength(150)]
        public string KeyResource { get; set; }
        [StringLength(500)]
        public string KeyAdd { get; set; }
        public bool Active { get; set; }
        public string InternalKey { get; set; }
        public virtual ICollection<RolPageAction> RolPageActions { get; set; }
        public virtual ICollection<UserPageAction> UserPageActions { get; set; }
        public virtual ICollection<Actions> Actions { get; set; }
        public virtual ICollection<CRPageMap> CRPageMaps { get; set; }
        public bool CR { get; set; }
    }
}
