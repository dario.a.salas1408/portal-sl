﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ARGNS.Model.Implementations
{
	public class QRConfig
    {
        
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdQRConfig { get; set; }
        public int IdCompany { get; set; }
        public string Separator { get; set; }
        public int ItemCodePosition { get; set; }
        public int? DscriptionPosition { get; set; }
        public int? WhsCodePosition { get; set; }
        public int? OcrCodePosition { get; set; }
        public int? GLAccountPosition { get; set; }
        public int? FreeTxtPosition { get; set; }
        public int? QuantityPosition { get; set; }
        public int? CurrencyPosition { get; set; }
        public int? PricePosition { get; set; }
        public int? SerialPosition { get; set; }
        public int? BatchPosition { get; set; }
        public int? UomCodePosition { get; set; }
        
    }
}
