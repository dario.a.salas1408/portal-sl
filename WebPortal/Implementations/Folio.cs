﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;

namespace ARGNS.Model.Implementations
{
    public class Folio:IFolio
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [StringLength(50)]
        public string Type { get; set; }
        
        public int FolioNum { get; set; }
    }
}
