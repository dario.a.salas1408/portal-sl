﻿using ARGNS.Model.Interfaces;
using System.Collections.Generic;

namespace ARGNS.Model.Implementations
{
	public class BusinessPartnerCombo : IBusinessPartnerCombo
    {
        public List<BusinessPartnerType> BPTypeList { get; set; }
      
    }
}
