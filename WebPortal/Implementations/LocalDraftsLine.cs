﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;
using System;

namespace ARGNS.Model.Implementations
{
	public class LocalDraftsLine : ILocalDraftsLine
    {
        public LocalDraftsLine()
        {

        }

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id{ get; set; }
        public int LineNum { get; set; }
        public string ItemCode { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? PriceBefDi { get; set; }
        public DateTime? ShipDate { get; set; }
        public string WhsCode { get; set; }
        public decimal? DiscPrcnt { get; set; }

        [ForeignKey("Header")]
        public int HeaderId { get; set; }
        public LocalDrafts Header { get; set; }
        public string Currency { get; set; }
        public string Dscription { get; set; }
        public string UomCode { get; set; }
    }
}
