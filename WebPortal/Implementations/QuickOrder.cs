﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ARGNS.Model.Interfaces;

namespace ARGNS.Model.Implementations
{
	public class QuickOrder : IQuickOrder
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdQuickOrder { get; set; }

        [Column(Order = 1)]
        public int IdUser { get; set; }

        [Column(Order = 2)]
        public string IdBp { get; set; }

        [Key]
        [Column(Order = 3)]
        public string ItemCode { get; set; }

        [Column(Order = 4)]
        public int Quantity { get; set; }

        [Column(Order = 5)]
        [StringLength(10)]
        public string WarehouseCode { get; set; }

        [Column(Order = 6)]
        public int? IdCompany { get; set; }

        public virtual User Users { get; set; }
    }
}
