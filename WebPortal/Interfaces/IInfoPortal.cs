﻿namespace ARGNS.Model.Interfaces
{
	public interface IInfoPortal
    {
        int Id { get; set; }
        string vsPortal { get; set; }
        string vsDB { get; set; }
        System.DateTime IniDB { get; set; }
        System.DateTime LastUpdateDB { get; set; }
    }
}
