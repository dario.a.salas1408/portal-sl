﻿namespace ARGNS.Model.Interfaces
{
	public interface IAuthorization
    {
        int Id { get; set; }
        string Name { get; set; }
        char Level { get; set; }
    }
}
