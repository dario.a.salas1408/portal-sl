﻿using ARGNS.Model.Implementations;

namespace ARGNS.Model.Interfaces
{
	public interface ICompanyStock
    {
        int IdCompanyStock { get; set; }
        int IdCompany { get; set; }
        bool ValidateStock { get; set; }
        string Formula { get; set; }
        CompanyConn CompanyConns { get; set; }
    }
}
