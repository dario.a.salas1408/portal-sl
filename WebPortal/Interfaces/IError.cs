﻿namespace ARGNS.Model.Interfaces
{
	public interface IError
    {
        int ErrorCode { get; set; }
        string ErrorDescription { get; set; }
    }
}
