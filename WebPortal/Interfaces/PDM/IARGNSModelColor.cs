﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelColor
    {
        string Code { get; set; }
        int LineId { get; set; }
        string U_ModCode { get; set; }
        string U_ColCode { get; set; }
        string U_Active { get; set; }
        string ColorDesc { get; set; }
        string U_Argb { get; set; }
    }
}
