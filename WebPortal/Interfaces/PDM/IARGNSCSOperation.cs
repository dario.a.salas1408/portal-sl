﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSCSOperation
    {
         string Code { get; set; }

         int LineId { get; set; }

         string U_ItemCode { get; set; }

         string U_ItemName { get; set; }

         decimal? U_Quantity { get; set; }

         string U_UoM { get; set; }

         decimal? U_Price { get; set; }

         string U_Currency { get; set; }

         short? U_PList { get; set; }
        
    }
}
