﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelInst
    {
        string Code { get; set; }
        int LineId { get; set; }
        string Object { get; set; }
        Nullable<int> LogInst { get; set; }
        string U_Sector { get; set; }
        string U_ModCode { get; set; }
        string U_InstCod { get; set; }
        string U_Instuct { get; set; }
        string U_Descrip { get; set; }
        string U_Imag { get; set; }
        string U_Version { get; set; }
        string U_Active { get; set; }
    }
}
