﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface ISkuModel
    {
        string ItemCode { get; set; }
        string U_ARGNS_SIZE { get; set; }
        string U_ARGNS_COL { get; set; }
        string U_ARGNS_VAR { get; set; }
        string ColorDesc { get; set; }
        string VarDesc { get; set; }
    }
}
