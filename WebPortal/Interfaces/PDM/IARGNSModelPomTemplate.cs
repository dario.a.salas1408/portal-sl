﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSModelPomTemplate
    {
        string Code { get; set; }
        string U_CodeTmpl { get; set; }
        string U_Desc { get; set; }
        string U_ProdGrp { get; set; }
        string U_Active { get; set; }
        string U_Scale { get; set; }
        string U_SSize { get; set; }
    }
}
