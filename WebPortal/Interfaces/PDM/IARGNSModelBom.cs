﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelBom
    {
        string Code { get; set; }
        int LineId { get; set; }
        string Object { get; set; }
        Nullable<int> LogInst { get; set; }
        string U_ItemCode { get; set; }
        Nullable<decimal> U_Quantity { get; set; }
        string U_UOM { get; set; }
        string U_Whse { get; set; }
        string U_IssueMth { get; set; }
        Nullable<short> U_PList { get; set; }
        Nullable<decimal> U_UPrice { get; set; }
        Nullable<decimal> U_Total { get; set; }
        string U_Comments { get; set; }
        string U_TreeType { get; set; }
        string U_PVendor { get; set; }
    }
}
