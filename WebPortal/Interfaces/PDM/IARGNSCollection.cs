﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSCollection
    {
         string Code { get; set; }
         string Name { get; set; }
         int DocEntry { get; set; }
         string U_CollCode { get; set; }
         string U_Desc { get; set; }
    }
}
