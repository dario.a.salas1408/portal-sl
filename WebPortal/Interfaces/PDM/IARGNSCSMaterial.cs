﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSCSMaterial
    {
        string Code { get; set; }

        int LineId { get; set; }

        string U_ItemCode { get; set; }

        string U_ItemName { get; set; }

        decimal? U_Quantity { get; set; }

        string U_UoM { get; set; }

        string U_Whse { get; set; }

        decimal? U_UPrice { get; set; }

        string U_TreeType { get; set; }

        string U_PVendor { get; set; }

        string U_OcrCode { get; set; }

        string U_Currency { get; set; }

        string U_CxT { get; set; }

        string U_CxS { get; set; }

        string U_CxC { get; set; }

        string U_CxGT { get; set; }

        string U_ItmLinkO { get; set; }

        string U_IssueMth { get; set; }

        short? U_PList { get; set; }

        string U_Comments { get; set; }
    }
}
