﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelImg
    {
        string Code { get; set; }
        int LineId { get; set; }
        string U_ModCode { get; set; }
        string U_File { get; set; }
        string U_Path { get; set; }
    }
}
