﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSSize
    {
        string Code { get; set; }
        int LineId { get; set; }
        string U_SizeCod { get; set; }
        string U_SizeCode { get; set; }
        string U_SizeDesc { get; set; }
        Nullable<short> U_VOrder { get; set; }
        string U_Active { get; set; }
        string U_Selected { get; set; }
        
    }
}
