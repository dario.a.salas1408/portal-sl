﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSMODVENDOR
    {   
         string Code { get; set; }      
         string Name { get; set; }    
         string U_ModCode { get; set; }       
         string U_CardCode { get; set; }       
         string U_CardType { get; set; }
    }
}
