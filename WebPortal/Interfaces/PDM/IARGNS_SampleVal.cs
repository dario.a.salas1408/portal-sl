﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNS_SampleVal
    {
        string Code { get; set; }
        string Name { get; set; }
        string U_Descript { get; set; }
    }
}
