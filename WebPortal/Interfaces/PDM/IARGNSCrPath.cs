﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSCrPath
    {

        string Code { get; set; }
        string Name { get; set; }
        int DocEntry { get; set; }
        string U_ProyCode { get; set; }
        string U_Desc { get; set; }
        string U_Model { get; set; }
        string U_Status { get; set; }
        string U_Planning { get; set; }
        List<ARGNSCrPathActivities> ActivitiesList { get; set; }

        //Descripciones
        string SeasonDesc { get; set; }
        string CollectionDesc { get; set; }
        string ModelPicture { get; set; }
        string ModelDesc { get; set; }

        //Sales Order Info
        string SalesOrderNum { get; set; }
        string SalesCardCode { get; set; }
        string SalesCardName { get; set; }
        DateTime? SalesPostingDate { get; set; }
        DateTime? SalesDelivDate { get; set; }
        decimal? SalesQty { get; set; }
    }
}
