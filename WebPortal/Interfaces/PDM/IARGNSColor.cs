﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSColor
    {
        string Code { get; set; }
        string Name { get; set; }
        int DocEntry { get; set; }
        string U_ColCode { get; set; }
        string U_ColDesc { get; set; }
        string U_Shade { get; set; }
        string U_NRFCode { get; set; }
        string U_Active { get; set; }
        string U_Argb { get; set; }
        string U_Pic { get; set; }
    }
}
