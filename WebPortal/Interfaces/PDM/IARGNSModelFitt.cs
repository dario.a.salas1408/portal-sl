﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelFitt
    {
        string Code { get; set; }
        int LineId { get; set; }
        string Object { get; set; }
        Nullable<int> LogInst { get; set; }
        string U_ModCode { get; set; }
        string U_FittCod { get; set; }
        string U_Fitting { get; set; }
    }
}
