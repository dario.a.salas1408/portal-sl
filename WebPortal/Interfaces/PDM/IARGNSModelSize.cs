﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelSize
    {
        string Code { get; set; }
        int LineId { get; set; }
        string U_ModCode { get; set; }
        string U_SizeCod { get; set; }
        string U_Selected { get; set; }
        string U_SclCode { get; set; }
        Nullable<short> U_VOrder { get; set; }
    }
}
