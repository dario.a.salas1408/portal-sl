﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelLogf
    {
        string Code { get; set; }
        string Name { get; set; }
        string U_ModCode { get; set; }
        string U_File { get; set; }
        string U_Path { get; set; }
        string U_Oprtn { get; set; }
        string U_User { get; set; }
        Nullable<System.DateTime> U_Date { get; set; }
        string U_Line { get; set; }
    }
}
