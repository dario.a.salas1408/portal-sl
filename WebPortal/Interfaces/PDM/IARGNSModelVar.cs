﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelVar
    {
        string Code { get; set; }
        int LineId { get; set; }
        string U_ModCode { get; set; }
        string U_VarCode { get; set; }
        string U_Selected { get; set; }
        string U_VarDesc { get; set; }
        string U_ATGrp { get; set; }
    }
}
