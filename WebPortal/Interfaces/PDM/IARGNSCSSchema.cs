﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSCSSchema
    {
        string Code { get; set; }

        int LineId { get; set; }

        string U_ItemCode { get; set; }

        string U_ItemName { get; set; }

        decimal? U_Percent { get; set; }

        decimal? U_Amount { get; set; }

        string U_Currency { get; set; }

        string U_AplicTo { get; set; }

        short? U_PList { get; set; }

    }
}
