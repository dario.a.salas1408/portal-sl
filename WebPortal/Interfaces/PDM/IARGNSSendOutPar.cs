﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSSendOutPar
    {
        int DocEntry { get; set; }
         int LineId { get; set; }
         string U_BarCode { get; set; }
         string U_Rework { get; set; }
    }
}
