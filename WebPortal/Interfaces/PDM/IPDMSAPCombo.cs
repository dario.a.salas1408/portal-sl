﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IPDMSAPCombo
    {
        List<ARGNSStyleStatus> ListStatus { get; set; }
        List<ARGNSProductLine> ListProductLine { get; set; }
        List<ARGNSModelGroup> ListModelGroup { get; set; }
        List<ItemGroup> ListItemGroup { get; set; }
        List<ARGNSSeason> ListSeason { get; set; }
        List<ARGNSBrand> ListBrand { get; set; }
        List<ARGNSDivision> ListDivision { get; set; }
        List<ARGNSYear> ListYear { get; set; }
        List<PriceListSAP> PriceList { get; set; }
        List<ARGNSSegmentation> ListSegmentation { get; set; }
        List<ARGNSCollection> ListCollection { get; set; }
        List<ARGNSSubCollection> ListSubCollection { get; set; }
    }
}
