﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelLogos
    {
        string Code { get; set; }
        int LineId { get; set; }
        string Object { get; set; }
        Nullable<int> LogInst { get; set; }
        string U_CodeLogo { get; set; }
        string U_LogDesc { get; set; }
        string U_Location { get; set; }
        string U_Position { get; set; }
        string U_ModColor { get; set; }
        string U_CodColow { get; set; }
        string U_Colorway { get; set; }
        string U_Variable { get; set; }
    }
}
