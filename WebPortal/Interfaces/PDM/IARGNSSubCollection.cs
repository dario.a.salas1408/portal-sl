﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSSubCollection
    {
         string Code { get; set; }
         string Name { get; set; }        
         string U_AmbCode { get; set; }
         string U_Desc { get; set; }
         string U_DetDesc { get; set; }
     
    }
}
