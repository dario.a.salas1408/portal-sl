﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSCostSheet
    {
        string Code { get; set; }

        string Name { get; set; }

        int DocEntry { get; set; }

        string U_ModCode { get; set; }

        string U_Currency { get; set; }

        string U_Description { get; set; }

        string U_OTCode { get; set; }

        string U_SchCode { get; set; }

        //Agregados en el view
        List<ARGNSCSMaterial> ListMaterial { get; set; }
        List<ARGNSCSOperation> ListOperation { get; set; }
        List<ARGNSCSSchema> ListSchema { get; set; }
        CostSheetComboSAP CostSheetCombo { get; set; }
        string ModeDBCode { get; set; }
    }
}
