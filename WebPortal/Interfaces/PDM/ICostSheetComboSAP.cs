﻿using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface ICostSheetComboSAP
    {
        List<PriceListSAP> PriceList { get; set; }
        List<CurrencySAP> CurrencyList { get; set; }
        List<BusinessPartnerSAP> VendorList { get; set; }
    }
}
