﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IStockModel
    {
        string ItemCode { get; set; }
        string WhsCode { get; set; }
        string WhsName { get; set; }
        decimal? OnHand { get; set; }
        decimal? IsCommited { get; set; }
        decimal? OnOrder { get; set; }
        string U_ARGNS_SIZE { get; set; }
        short? U_ARGNS_SIZEVO { get; set; }
        string U_ARGNS_COL { get; set; }
        string U_ARGNS_VAR { get; set; }
    }
}
