﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSVariable
    {
         string Code { get; set; }
         string Name { get; set; }
         int DocEntry { get; set; }
         string U_VarCode { get; set; }
         string U_VarDesc { get; set; }
         string U_Active { get; set; }
         string U_ATGrp { get; set; }
    }
}
