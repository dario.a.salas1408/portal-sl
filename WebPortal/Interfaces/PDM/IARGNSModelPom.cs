﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSModelPom
    {
        string Code { get; set; }
        int LineId { get; set; }
        string U_Desc { get; set; }
        string U_SizeCode { get; set; }
        string U_SizeDesc { get; set; }
        decimal? U_Value { get; set; }
        decimal? U_TolPosit { get; set; }
        decimal? U_TolNeg { get; set; }
        string U_QAPoint { get; set; }
        string U_POM { get; set; }
    }
}
