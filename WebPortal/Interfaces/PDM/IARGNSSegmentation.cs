﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSSegmentation
    {
         string Code { get; set; }
         string Name { get; set; }
         int DocEntry { get; set; }       
    }
}
