﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM.ComboList
{
    interface IItemGroup
    {
         short ItmsGrpCod { get; set; }
         string ItmsGrpNam { get; set; }
    }
}
