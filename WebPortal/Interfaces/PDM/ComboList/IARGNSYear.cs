﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM.ComboList
{
    interface IARGNSYear
    {
         string Code { get; set; }
         string Name { get; set; }
         Nullable<short> U_Year { get; set; }
    }
}
