﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM.ComboList
{
    interface IPriceList
    {
         short ListNum { get; set; }
         string ListName { get; set; }
         Nullable<short> BASE_NUM { get; set; }
         Nullable<decimal> Factor { get; set; }
         Nullable<short> RoundSys { get; set; }
         Nullable<short> GroupCode { get; set; }
      
    }
}
