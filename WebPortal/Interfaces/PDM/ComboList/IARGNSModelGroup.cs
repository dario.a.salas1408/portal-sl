﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM.ComboList
{
    interface IARGNSModelGroup
    {
         string Code { get; set; }
         string Name { get; set; }
         string U_NFRCode { get; set; }
         string U_ProdLine { get; set; }
    }
}
