﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSModelSampleLine
    {
        string Code { get; set; }
        int LineId { get; set; }
        string Object { get; set; }
        int LogInst { get; set; }
        string U_SclCode { get; set; }
        string U_POM { get; set; }
        string U_Desc { get; set; }
        string U_SizeCode { get; set; }
        string U_SizeDesc { get; set; }
        decimal U_Target { get; set; }
        decimal U_Actual { get; set; }
        string U_Revision { get; set; }
    }
}
