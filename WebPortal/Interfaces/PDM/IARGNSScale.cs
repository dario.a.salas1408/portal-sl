﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSScale
    {
        string Code { get; set; }
        string Name { get; set; }
        string U_SclCode { get; set; }
        string U_SclDesc { get; set; }
        string U_Active { get; set; }
        Nullable<short> U_VOrder { get; set; }
        List<ARGNSSize> SizeList { get; set; }
    }
}
