﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModelWflow
    {
        string Code { get; set; }
        int LineId { get; set; }
        string Object { get; set; }
        Nullable<int> LogInst { get; set; }
        string U_Workflow { get; set; }
        string U_WorkfDes { get; set; }
        string U_Depart { get; set; }
        Nullable<short> U_LeadTime { get; set; }
        string U_Active { get; set; }
        string U_Role { get; set; }
        string U_CodeMger { get; set; }
        string U_Manager { get; set; }
        string U_User { get; set; }
    }
}
