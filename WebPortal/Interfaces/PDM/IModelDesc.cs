﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IModelDesc
    {
        string Code { get; set; }
        string ModelCode { get; set; }
        string ModDescription { get; set; }
        string SeasonDesc { get; set; }
        string CollDesc { get; set; }
        string SubCollDesc { get; set; }
        string BrandDesc { get; set; }
        string ProdGroupDesc { get; set; }
        string ModelImage { get; set; }
        decimal? Modelprice { get; set; }
    }
}
