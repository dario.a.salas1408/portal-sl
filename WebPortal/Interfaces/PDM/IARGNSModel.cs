﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    interface IARGNSModel
    {
        string Code { get; set; }
        string Name { get; set; }
        int DocEntry { get; set; }
        string U_ATGrp { get; set; }
        string U_ModCode { get; set; }
        string U_ModDesc { get; set; }
        Nullable<System.DateTime> U_SSDate { get; set; }
        Nullable<System.DateTime> U_SCDate { get; set; }
        string U_Year { get; set; }
        string U_COO { get; set; }
        string U_Pic { get; set; }
        string U_Active { get; set; }
        Nullable<short> U_SapGrp { get; set; }
        Nullable<short> U_PList { get; set; }
        string U_Division { get; set; }
        string U_Season { get; set; }
        string U_FrgnDesc { get; set; }
        string U_ModGrp { get; set; }
        string U_SclCode { get; set; }
        string U_LineCode { get; set; }
        string U_Vendor { get; set; }
        string U_MainWhs { get; set; }
        string U_Comments { get; set; }
        string U_Owner { get; set; }
        string U_Approved { get; set; }
        string U_PicR { get; set; }
        Nullable<decimal> U_Price { get; set; }
        string U_WhsNewI { get; set; }
        string U_CollCode { get; set; }
        string U_AmbCode { get; set; }
        string U_CompCode { get; set; }
        string U_Brand { get; set; }
        string U_ChartCod { get; set; }
        string U_Designer { get; set; }
        string U_Customer { get; set; }
        string U_GrpSCod { get; set; }
        string U_Currency { get; set; }
        string U_Status { get; set; }
        string U_RMaterial { get; set; }
        string U_DocNumb { get; set; }
        List<ARGNSModelColor> ModelColorList { get; set; }
        List<ARGNSModelSize> ModelSizeList { get; set; }
        List<ARGNSModelVar> ModelVariableList { get; set; }
        List<ARGNSModelImg> ModelImageList { get; set; }
        List<ARGNSModelFile> ModelFileList { get; set; }
        List<StockModel> ModelStockList { get; set; }
        List<ARGNSModelPom> ModelPomList { get; set; }
        PDMSAPCombo ListPDMSAPCombo { get; set; }
        AdministrationApparel administrationApparel { get; set; }        
        AdministrationSAP administrationSAP { get; set; }

        string U_SclPOM { get; set; }
        string U_CodePOM { get; set; }
    }
}
