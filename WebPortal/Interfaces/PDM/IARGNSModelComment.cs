﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSModelComment
    {
        string Code { get; set; }
        string Name { get; set; }
        int DocEntry { get; set; }
        string U_USER { get; set; }
        string U_COMMENT { get; set; }
        string U_FILE { get; set; }
        string U_FILEPATH { get; set; }
        string U_MODCODE { get; set; }
        System.DateTime U_DATE { get; set; }


    }
}
