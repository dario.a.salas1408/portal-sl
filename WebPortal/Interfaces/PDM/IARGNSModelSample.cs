﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.PDM
{
    public interface IARGNSModelSample
    {
        string Code { get; set; }
        string Name { get; set; }
        int DocEntry { get; set; }
        string U_ModCode { get; set; }
        string U_Type { get; set; }
        string U_SampCode { get; set; }
        string U_Status { get; set; }
        string U_SclCode { get; set; }
        string U_POM { get; set; }
        DateTime U_Date { get; set; }
        string U_User { get; set; }
        DateTime U_ApproDate { get; set; }
        string U_Active { get; set; }
        string U_Comments { get; set; }
        string U_BPCustomer { get; set; }
        string U_BPSupp { get; set; }
        string U_Picture { get; set; }
        List<ARGNS_SampleVal> ListSampleVal { get; set; }
        List<UserSAP> ListUser { get; set; }
        List<ARGNSModelPomTemplate> ListPOM { get; set; }
        List<ARGNSScale> ListScale { get; set; }
        string USER_CODE { get; set; }

    }
}
