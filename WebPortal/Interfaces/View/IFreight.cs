﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.View
{
    public interface IFreight
    {
        int DocEntry { get; set; }

        int? ExpnsCode { get; set; }

        string ExpnsName { get; set; }

        decimal? LineTotal { get; set; }

        decimal? TotalFrgn { get; set; }

        string Comments { get; set; }

        string ObjType { get; set; }

        string DistrbMthd { get; set; }

        decimal? VatSum { get; set; }

        string TaxCode { get; set; }

        int LineNum { get; set; }

        string Status { get; set; }

        string OcrCode { get; set; }

        string TaxDistMtd { get; set; }

        string Project { get; set; }

        int? BaseAbsEnt { get; set; }

        int? BaseType { get; set; }

        int? BaseRef { get; set; }

        int? BaseLnNum { get; set; }

        List<DistrRuleSAP> ListDistrRule { get; set; }
        List<SalesTaxCodesSAP> ListTax { get; set; }
        List<ProjectSAP> ListProject { get; set; }
    }
}
