﻿namespace ARGNS.Model.Interfaces
{
	public interface IQuickOrder
    {
        int IdQuickOrder { get; set; }
        int IdUser { get; set; }
        string IdBp { get; set; }
        string ItemCode { get; set; }
        int Quantity { get; set; }
    }
}
