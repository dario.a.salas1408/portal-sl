﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces
{
    public interface ISerial
    {
        string DistNumber { get; set; }
        int SysNumber { get; set; }
        int DocLine { get; set; }
        int AbsEntry { get; set; }
        decimal? AllocQty { get; set; }
        string whsCode { get; set; }
        List<UDF_ARGNS> UDFs { get; set; }
    }
}
