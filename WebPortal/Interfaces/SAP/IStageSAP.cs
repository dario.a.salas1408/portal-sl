﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IStageSAP
    {
        int WstCode { get; set; }

        string Name { get; set; }
    }
}
