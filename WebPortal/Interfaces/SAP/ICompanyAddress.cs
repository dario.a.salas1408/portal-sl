﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface ICompanyAddress
    {
        string Street { get; set; }

        string Block { get; set; }

        string City { get; set; }

        string ZipCode { get; set; }

        string County { get; set; }

        string State { get; set; }

        string Country { get; set; }

        int Code { get; set; }

        string AddrType { get; set; }

        string StreetNo { get; set; }

        string Building { get; set; }

        string GlblLocNum { get; set; }
    }
}
