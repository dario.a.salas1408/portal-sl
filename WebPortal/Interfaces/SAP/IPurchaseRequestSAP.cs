﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IPurchaseRequestSAP
    {
        int? ReqType { get; set; }

        string Requester { get; set; }

        string ReqName { get; set; }

        short? Branch { get; set; }

        short? Department { get; set; }

        string Email { get; set; }

        string Notify { get; set; }

        List<PurchaseRequestLineSAP> Lines { get; set; }

        decimal? TotalExpns { get; set; }

        bool Draft { get; set; }

        DocumentSettingsSAP SAPDocSettings { get; set; }
        string LocalCurrency { get; set; }
        string SystemCurrency { get; set; }
        string RateCurrency { get; set; }

    }
}
