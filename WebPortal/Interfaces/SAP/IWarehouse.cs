﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IWarehouse
    {
        string WhsCode { get; set; }
        string WhsName { get; set; }
        int? BPLid { get; set; }
    }
}
