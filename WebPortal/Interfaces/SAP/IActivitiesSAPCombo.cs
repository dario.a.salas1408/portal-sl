﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    interface IActivitiesSAPCombo
    {
        List<ActivityTypeSAP> ActivityTypeList { get; set; }
        List<ActivityPriority> ActivityPriorityList { get; set; }
        List<ActivityAction> ActivityActionList { get; set; }
        List<ActivityUserType> ActivityUserTypeList { get; set; }
        List<ActivityUser> ActivityUserList { get; set; }
    }
}
