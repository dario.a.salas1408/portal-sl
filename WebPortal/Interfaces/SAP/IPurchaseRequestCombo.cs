﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IPurchaseRequestCombo
    {
        List<Department> DepartmentList { get; set; }
        List<CurrencySAP> CurrencyList { get; set; }
        List<Branch> BranchList { get; set; }
        List<RatesSystem> RatesList { get; set; }
    }
}
