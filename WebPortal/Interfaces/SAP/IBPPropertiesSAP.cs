﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IBPPropertiesSAP
    {
        short GroupCode { get; set; }

        string GroupName { get; set; }

        short? UserSign { get; set; }

        string Filler { get; set; }
    }
}
