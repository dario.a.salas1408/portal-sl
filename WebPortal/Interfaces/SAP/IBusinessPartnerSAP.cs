﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IBusinessPartnerSAP
    {
         string CardCode { get; set; }
         string CardName { get; set; }
         string CardType { get; set; }
         string Address { get; set; }
         List<BPAddressesSAP> Addresses { get; set; }
         string ZipCode { get; set; }
         string MailAddres { get; set; }
         List<ContacPerson> ListContact { get; set; }
         string RateCurrency { get; set; } 
         string Currency { get; set; }
         string ShipToDef { get; set; }
         string BillToDef { get; set; }
         short? ListNum { get; set; }
         short? ShipType { get; set; }

         string Notes { get; set; }

         int? SlpCode { get; set; }

         string QryGroup1 { get; set; }

         string QryGroup2 { get; set; }

         string QryGroup3 { get; set; }

         string QryGroup4 { get; set; }

         string QryGroup5 { get; set; }

         string QryGroup6 { get; set; }

         string QryGroup7 { get; set; }

         string QryGroup8 { get; set; }

         string QryGroup9 { get; set; }

         string QryGroup10 { get; set; }

         string QryGroup11 { get; set; }

         string QryGroup12 { get; set; }

         string QryGroup13 { get; set; }

         string QryGroup14 { get; set; }

         string QryGroup15 { get; set; }

         string QryGroup16 { get; set; }

         string QryGroup17 { get; set; }

         string QryGroup18 { get; set; }

         string QryGroup19 { get; set; }

         string QryGroup20 { get; set; }

         string QryGroup21 { get; set; }

         string QryGroup22 { get; set; }

         string QryGroup23 { get; set; }

         string QryGroup24 { get; set; }

         string QryGroup25 { get; set; }

         string QryGroup26 { get; set; }

         string QryGroup27 { get; set; }

         string QryGroup28 { get; set; }

         string QryGroup29 { get; set; }

         string QryGroup30 { get; set; }

         string QryGroup31 { get; set; }

         string QryGroup32 { get; set; }

         string QryGroup33 { get; set; }

         string QryGroup34 { get; set; }

         string QryGroup35 { get; set; }

         string QryGroup36 { get; set; }

         string QryGroup37 { get; set; }

         string QryGroup38 { get; set; }

         string QryGroup39 { get; set; }

         string QryGroup40 { get; set; }

         string QryGroup41 { get; set; }

         string QryGroup42 { get; set; }

         string QryGroup43 { get; set; }

         string QryGroup44 { get; set; }

         string QryGroup45 { get; set; }

         string QryGroup46 { get; set; }

         string QryGroup47 { get; set; }

         string QryGroup48 { get; set; }

         string QryGroup49 { get; set; }

         string QryGroup50 { get; set; }

         string QryGroup51 { get; set; }

         string QryGroup52 { get; set; }

         string QryGroup53 { get; set; }

         string QryGroup54 { get; set; }

         string QryGroup55 { get; set; }

         string QryGroup56 { get; set; }

         string QryGroup57 { get; set; }

         string QryGroup58 { get; set; }

         string QryGroup59 { get; set; }

         string QryGroup60 { get; set; }

         string QryGroup61 { get; set; }

         string QryGroup62 { get; set; }

         string QryGroup63 { get; set; }

         string QryGroup64 { get; set; }

         decimal? Balance { get; set; }

         decimal? DNotesBal { get; set; }

         decimal? OrdersBal { get; set; }

         int? OprCount { get; set; }

         string LicTradNum { get; set; }


    }
}
