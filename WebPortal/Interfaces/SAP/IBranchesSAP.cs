﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IBranchesSAP
    {
        int BPLId { get; set; }

        string BPLName { get; set; }
    }
}
