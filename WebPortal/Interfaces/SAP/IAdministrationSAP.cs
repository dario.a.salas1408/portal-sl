﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IAdministrationSAP
    {
        int Code { get; set; }
        string TimeFormat { get; set; }
        string DateFormat { get; set; }
        string DateSep { get; set; }
        short? SumDec { get; set; }
        short? PriceDec { get; set; }
        short? RateDec { get; set; }
        short? QtyDec { get; set; }
        short? PercentDec { get; set; }
        short? MeasureDec { get; set; }
        short? QueryDec { get; set; }
        string DecSep { get; set; }
    }
}
