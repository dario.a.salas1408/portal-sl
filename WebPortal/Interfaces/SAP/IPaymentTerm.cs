﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IPaymentTerm
    {
        short GroupNum { get; set; }
        string PymntGroup { get; set; }
    }
}
