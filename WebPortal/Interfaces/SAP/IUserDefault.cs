﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    interface IUserDefault
    {
        string Code { get; set; }
        string DfltsGroup { get; set; }
        string Warehouse { get; set; }
        string USER_CODE { get; set; }
    }
}
