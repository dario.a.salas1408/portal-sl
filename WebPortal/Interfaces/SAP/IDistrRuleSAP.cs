﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IDistrRuleSAP
    {
        string OcrCode { get; set; }

        string OcrName { get; set; }

        string Locked { get; set; }

        string Active { get; set; }
    }
}
