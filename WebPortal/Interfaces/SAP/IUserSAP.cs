﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IUserSAP
    {
        short USERID { get; set; }
        string PASSWORD { get; set; }
        string USER_CODE { get; set; }
        string U_NAME { get; set; }
    }
}
