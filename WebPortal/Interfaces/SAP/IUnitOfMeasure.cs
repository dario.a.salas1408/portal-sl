﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    interface IUnitOfMeasure
    {
         int UomEntry { get; set; }
         string UomCode { get; set; }
         string UomName { get; set; }
    }
}
