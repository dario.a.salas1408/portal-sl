﻿using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IItemMasterSAP
    {
        string ItemCode { get; set; }

        string ItemName { get; set; }
        string PrchseItem { get; set; }

        string SellItem { get; set; }

        string InvntItem { get; set; }

        List<ItemPrices> ItemPrices { get; set; }

        string ManBtchNum { get; set; }

        string ManSerNum { get; set; }

        string CardCode { get; set; }

        string PicturName { get; set; }

        string UserText { get; set; }

        string ItemType { get; set; }

        List<ItemGroup> ListItemGroup { get; set; }

        List<PriceListSAP> ListPriceListSAP { get; set; }

        List<AttachmentSAP> ListAttachmentSAP { get; set; }

        int? AtcEntry { get; set; }
        int PriceUnit { get; set; }
        int? PUoMEntry { get; set; }
        int? SUoMEntry { get; set; }
    }
}
