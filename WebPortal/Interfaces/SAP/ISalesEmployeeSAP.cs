﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface ISalesEmployeeSAP
    {
        int SlpCode { get; set; }

        string SlpName { get; set; }
    }
}
