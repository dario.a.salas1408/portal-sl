﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IDocument
    {
        int DocEntry { get; set; }
        int DocNum { get; set; }
        string DocType { get; set; }
        Nullable<System.DateTime> DocDate { get; set; }
        Nullable<System.DateTime> DocDueDate { get; set; }
        Nullable<System.DateTime> TaxDate { get; set; }
        Nullable<System.DateTime> CancelDate { get; set; }
        Nullable<System.DateTime> ReqDate { get; set; }
        string CardCode { get; set; }
        string CardName { get; set; }
        string Project { get; set; }
        string RevisionPo { get; set; }
        string SummryType { get; set; }
        string NumAtCard { get; set; }
        Nullable<int> SlpCode { get; set; }
        Nullable<int> OwnerCode { get; set; }
        string DocCur { get; set; }
        Nullable<decimal> DocRate { get; set; }
        Nullable<int> CntctCode { get; set; }
        Nullable<short> GroupNum { get; set; }
        string PeyMethod { get; set; }
        Nullable<decimal> DiscPrcnt { get; set; }
        Nullable<short> TrnspCode { get; set; }
        string Address { get; set; }
        string Address2 { get; set; }
        string ObjType { get; set; }
        string DocStatus { get; set; }
        string Comments { get; set; }
        string JrnlMemo { get; set; }
        string FatherType { get; set; }
        string ShipToCode { get; set; }
        //DocumentSAPCombo ListDocumentSAPCombo { get; set; }
        //string LocalCurrency { get; set; }
        //string SystemCurrency { get; set; }
        //string RateCurrency { get; set; }
        decimal? DocTotal { get; set; }
        EmployeeSAP Employee { get; set; }
        List<DistrRuleSAP> DistrRuleSAP { get; set; }
        List<Warehouse> Warehouse { get; set; }
        List<GLAccountSAP> GLAccountSAP { get; set; }
    }
}
