﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IBranch
    {
        short Code { get; set; }

        string Name { get; set; }

        string Remarks { get; set; }

        short? UserSign { get; set; }
    }
}
