﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IDocumentSAPCombo
    {
        List<Buyer> ListBuyer { get; set; }
        //List<ContacPerson> ListContacPerson { get; set; }
        List<PaymentMethod> ListPaymentMethod { get; set; }
        List<CurrencySAP> ListCurrency { get; set; }
        List<PaymentTerm> ListPaymentTerm { get; set; }
        List<ShippingType> ListShippingType { get; set; }
        List<Taxt> ListTaxt { get; set; }
        List<TypeCurrency> ListTypeCurrency { get; set; }
    }
}
