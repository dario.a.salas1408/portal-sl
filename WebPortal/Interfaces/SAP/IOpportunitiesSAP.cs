﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    interface IOpportunitiesSAP
    {
        int OpprId { get; set; }
        string CardCode { get; set; }
        int? SlpCode { get; set; }
        int? CprCode { get; set; }
        int? Source { get; set; }
        int? IntCat1 { get; set; }
        int? IntCat2 { get; set; }
        int? IntCat3 { get; set; }
        int? IntRate { get; set; }
        DateTime? OpenDate { get; set; }
        string DifType { get; set; }
        DateTime? PredDate { get; set; }
        decimal? MaxSumLoc { get; set; }
        decimal? MaxSumSys { get; set; }
        decimal? WtSumLoc { get; set; }
        decimal? WtSumSys { get; set; }
        decimal? PrcnProf { get; set; }
        decimal? SumProfL { get; set; }
        decimal? SumProfS { get; set; }
        string Memo { get; set; }
        string Status { get; set; }
        string StatusRem { get; set; }
        int? Reason { get; set; }
        decimal? RealSumLoc { get; set; }
        decimal? RealSumSys { get; set; }
        decimal? RealProfL { get; set; }
        decimal? RealProfS { get; set; }
        decimal? CloPrcnt { get; set; }
        short? StepLast { get; set; }
        short? UserSign { get; set; }
        string Transfered { get; set; }
        short? Instance { get; set; }
        string CardName { get; set; }
        DateTime? CloseDate { get; set; }
        int? LastSlp { get; set; }
        string Name { get; set; }
        int? Territory { get; set; }
        int? Industry { get; set; }
        string ChnCrdCode { get; set; }
        string ChnCrdName { get; set; }
        string PrjCode { get; set; }
        short? CardGroup { get; set; }
        int? ChnCrdCon { get; set; }
        int? Owner { get; set; }
        string attachment { get; set; }
        string DocType { get; set; }
        int? DocNum { get; set; }
        int? DocEntry { get; set; }
        string DocChkbox { get; set; }
        int? AtcEntry { get; set; }
        int? LogInstanc { get; set; }
        short? UserSign2 { get; set; }
        DateTime? UpdateDate { get; set; }
        string OpprType { get; set; }

    }
}
