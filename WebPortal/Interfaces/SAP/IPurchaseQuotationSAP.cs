﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IPurchaseQuotationSAP
    {
        //Document Lines
        List<PurchaseQuotationSAPLine> Lines { get; set; }
        DocumentSAPCombo ListDocumentSAPCombo { get; set; }
        string LocalCurrency { get; set; }
        string SystemCurrency { get; set; }
        string RateCurrency { get; set; }
        List<RatesSystem> ListSystemRates { get; set; }
        CompanyAddress companyAddress { get; set; }
        DocumentAddress PQAddress { get; set; }
        DocumentSettingsSAP SAPDocSettings { get; set; }
        List<Freight> ListFreight { get; set; }
        decimal? TotalExpns { get; set; }
        bool Draft { get; set; }

    }
}
