﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IPurchaseInvoiceLineSAP
    {
         int DocEntry { get; set; }

         int LineNum { get; set; }

         int? TargetType { get; set; }

         int? TrgetEntry { get; set; }

         string BaseRef { get; set; }

         int? BaseType { get; set; }

         int? BaseEntry { get; set; }

         int? BaseLine { get; set; }

         string LineStatus { get; set; }

         string ItemCode { get; set; }

         string Dscription { get; set; }

         decimal? Quantity { get; set; }

         DateTime? ShipDate { get; set; }

         decimal? OpenQty { get; set; }

         decimal? Price { get; set; }

         string Currency { get; set; }

         decimal? Rate { get; set; }

         decimal? DiscPrcnt { get; set; }

         decimal? LineTotal { get; set; }

         string WhsCode { get; set; }

         int? SlpCode { get; set; }

         decimal? PriceBefDi { get; set; }

         string OcrCode { get; set; }

         string FreeTxt { get; set; }

         string LineVendor { get; set; }
    }
}
