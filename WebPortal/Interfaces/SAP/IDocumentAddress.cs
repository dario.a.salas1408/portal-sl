﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IDocumentAddress
    {
        int DocEntry { get; set; }

        string StreetS { get; set; }

        string BlockS { get; set; }

        string BuildingS { get; set; }

        string CityS { get; set; }

        string ZipCodeS { get; set; }

        string CountyS { get; set; }

        string StateS { get; set; }

        string CountryS { get; set; }

        string AddrTypeS { get; set; }

        string StreetNoS { get; set; }

        string StreetB { get; set; }

        string BlockB { get; set; }

        string BuildingB { get; set; }

        string CityB { get; set; }

        string ZipCodeB { get; set; }

        string CountyB { get; set; }

        string StateB { get; set; }

        string CountryB { get; set; }

        string AddrTypeB { get; set; }

        string StreetNoB { get; set; }

        string GlbLocNumS { get; set; }

        string GlbLocNumB { get; set; }

    }
}
