﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface ITypeCurrency
    {
        string CurSource { get; set; }
        string Description { get; set; }
    }
}
