﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    interface IServiceCallSAP
    {
        int callID { get; set; }
        string subject { get; set; }
        string customer { get; set; }
        string custmrName { get; set; }
        int? contctCode { get; set; }
        string manufSN { get; set; }
        string internalSN { get; set; }
        int? contractID { get; set; }
        DateTime? cntrctDate { get; set; }
        DateTime? resolDate { get; set; }
        short? resolTime { get; set; }
        string free_1 { get; set; }
        DateTime? free_2 { get; set; }
        short? origin { get; set; }
        string itemCode { get; set; }
        string itemName { get; set; }
        short? itemGroup { get; set; }
        short? status { get; set; }
        string priority { get; set; }
        short? callType { get; set; }
        short? problemTyp { get; set; }
        short? assignee { get; set; }
        string descrption { get; set; }
        string objType { get; set; }
        int? logInstanc { get; set; }
        short? userSign { get; set; }
        DateTime? createDate { get; set; }
        short? createTime { get; set; }
        DateTime? closeDate { get; set; }
        short? closeTime { get; set; }
        short? userSign2 { get; set; }
        DateTime? updateDate { get; set; }
        int? SCL1Count { get; set; }
        int? SCL2Count { get; set; }
        string isEntitled { get; set; }
        int? insID { get; set; }
        int? technician { get; set; }
        string resolution { get; set; }
        int? Scl1NxtLn { get; set; }
        int? Scl2NxtLn { get; set; }
        int? Scl3NxtLn { get; set; }
        int? Scl4NxtLn { get; set; }
        int? Scl5NxtLn { get; set; }
        string isQueue { get; set; }
        string Queue { get; set; }
        DateTime? resolOnDat { get; set; }
        short? resolOnTim { get; set; }
        DateTime? respByDate { get; set; }
        short? respByTime { get; set; }
        DateTime? respOnDate { get; set; }
        short? respOnTime { get; set; }
        short? respAssign { get; set; }
        DateTime? AssignDate { get; set; }
        short? AssignTime { get; set; }
        short? UpdateTime { get; set; }
        short? responder { get; set; }
        string Transfered { get; set; }
        short Instance { get; set; }
        int DocNum { get; set; }
        int? Series { get; set; }
        string Handwrtten { get; set; }
        string PIndicator { get; set; }
        DateTime? StartDate { get; set; }
        int? StartTime { get; set; }
        DateTime? EndDate { get; set; }
        int? EndTime { get; set; }
        decimal? Duration { get; set; }
        string DurType { get; set; }
        string Reminder { get; set; }
        decimal? RemQty { get; set; }
        string RemType { get; set; }
        DateTime? RemDate { get; set; }
        string RemSent { get; set; }
        short? RemTime { get; set; }
        short? Location { get; set; }
        string AddrName { get; set; }
        string AddrType { get; set; }
        string Street { get; set; }
        string City { get; set; }
        string Room { get; set; }
        string State { get; set; }
        string Country { get; set; }
        string DisplInCal { get; set; }
        string SupplCode { get; set; }
        string Attachment { get; set; }
        int? AtcEntry { get; set; }
        string NumAtCard { get; set; }
        short? ProSubType { get; set; }

    }
}
