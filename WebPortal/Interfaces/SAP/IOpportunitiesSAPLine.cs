﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    interface IOpportunitiesSAPLine
    {
        int OpprId { get; set; }
        short Line { get; set; }
        int? SlpCode { get; set; }
        int? CntctCode { get; set; }
        DateTime? OpenDate { get; set; }
        DateTime? CloseDate { get; set; }
        int? Step_Id { get; set; }
        decimal? ClosePrcnt { get; set; }
        decimal? MaxSumLoc { get; set; }
        decimal? MaxSumSys { get; set; }
        string Memo { get; set; }
        int? DocId { get; set; }
        string ObjType { get; set; }
        string Status { get; set; }
        string Linked { get; set; }
        decimal? WtSumLoc { get; set; }
        decimal? WtSumSys { get; set; }
        short? UserSign { get; set; }
        string ChnCrdCode { get; set; }
        string ChnCrdName { get; set; }
        int? ChnCrdCon { get; set; }
        string DocChkbox { get; set; }
        int? Owner { get; set; }
        int? DocNumber { get; set; }

    }
}
