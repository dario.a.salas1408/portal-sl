﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IDocumentLine
    {
        int DocEntry { get; set; }
        int LineNum { get; set; }
        string ItemCode { get; set; }
        string Dscription { get; set; }
        string SubCatNum { get; set; }
        string AcctCode { get; set; }
        Nullable<decimal> Quantity { get; set; }
        Nullable<decimal> Price { get; set; }
        Nullable<decimal> PriceBefDi { get; set; }
        string Currency { get; set; }
        string WhsCode { get; set; }
        string TaxCode { get; set; }
        string Project { get; set; }
        string OcrCode { get; set; }
        string FreeTxt { get; set; }
        Nullable<System.DateTime> ShipDate { get; set; }  
    }
}
