﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IPurchaseRequestLineSAP
    {

        int? TargetType { get; set; }

        int? TrgetEntry { get; set; }

        string BaseRef { get; set; }

        int? BaseType { get; set; }

        int? BaseEntry { get; set; }

        int? BaseLine { get; set; }

        string LineVendor { get; set; }

        DateTime? PQTReqDate { get; set; }

        decimal? DiscPrcnt { get; set; }

        //Agregados
        GLAccountSAP GLAccount { get; set; }
    }
}
