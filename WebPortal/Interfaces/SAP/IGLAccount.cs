﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IGLAccount
    {
        string AcctCode { get; set; }

        string AcctName { get; set; }

        string FormatCode { get; set; }
    }
}
