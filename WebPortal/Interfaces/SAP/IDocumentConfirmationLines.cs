﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IDocumentConfirmationLines
    {
        int WddCode { get; set; }

        int StepCode { get; set; }

        int UserID { get; set; }

        string Status { get; set; }

        string Remarks { get; set; }

        short? UserSign { get; set; }

        DateTime? CreateDate { get; set; }

        short? CreateTime { get; set; }

        DateTime? UpdateDate { get; set; }

        short? UpdateTime { get; set; }

        List<ApproveDesicion> ApproveDesicionList { get; set; }
        //Agregados para poder mostrar en el listado de My Approvals
        DateTime? TaxDate { get; set; }
        DateTime? ReqDate { get; set; }
        string RequestorRemark { get; set; }
        string ObjType { get; set; }
        int ODRFDocEntry { get; set; }
        decimal? DocTotal { get; set; }
        string DocumentComments { get; set; }
    }
}
