﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IActivitiesSAP
    {
         int ClgCode { get; set; }
         string CardCode { get; set; }
         string Notes { get; set; } 
         Nullable<System.DateTime> Recontact { get; set; }
         Nullable<System.DateTime>endDate { get; set; }
         string Closed { get; set; }
         string inactive { get; set; }
         Nullable<short> AttendUser { get; set; }
         string Action { get; set; }
         string Details { get; set; }
         Nullable<short> CntctType { get; set; } 
         string Priority { get; set; }                            
         Nullable<short> CntctSbjct { get; set; }
         ActivitiesSAPCombo ActivityCombo { get; set; }
         string CardName { get; set; }
         string ModelName { get; set; }
         string ActionName { get; set; }
         string ModelImage { get; set; }
         string AssignedUserName { get; set; }
         string U_ARGNS_PRDCODE { get; set; }
         string U_ARGNS_PROYECT { get; set; }
         Nullable<int> AttendEmpl { get; set; }
    }
}
