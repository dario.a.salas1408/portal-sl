﻿using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IDraft
    {
         int DocEntry { get; set; }

         int? DocNum { get; set; }

         string DocType { get; set; }

         string DocStatus { get; set; }

         string ObjType { get; set; }

         DateTime? DocDate { get; set; }

         DateTime? DocDueDate { get; set; }

         string CardCode { get; set; }

         string CardName { get; set; }

         string Address { get; set; }

         string NumAtCard { get; set; }

         decimal? VatSum { get; set; }

         decimal? DiscPrcnt { get; set; }

         decimal? DiscSum { get; set; }

         string DocCur { get; set; }

         decimal? DocRate { get; set; }

         decimal? DocTotal { get; set; }

         string Ref1 { get; set; }

         string Ref2 { get; set; }

         string Comments { get; set; }

         string JrnlMemo { get; set; }

         int? SlpCode { get; set; }

         short? TrnspCode { get; set; }

         DateTime? CreateDate { get; set; }

         DateTime? TaxDate { get; set; }

         short? UserSign { get; set; }

         string WddStatus { get; set; }

         int? draftKey { get; set; }

         decimal? TotalExpns { get; set; }

         DateTime? ReqDate { get; set; }

         DateTime? CancelDate { get; set; }

         int? OwnerCode { get; set; }

         string Requester { get; set; }

         string ReqName { get; set; }

         short? Branch { get; set; }

         short? Department { get; set; }

         string Email { get; set; }

         string Notify { get; set; }

         List<DraftLine> Lines { get; set; }
         DocumentSettingsSAP SAPDocSettings { get; set; }
    }
}
