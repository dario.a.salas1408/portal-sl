﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IBPAddressesSAP
    {
        string Address { get; set; }

        string CardCode { get; set; }

        string Street { get; set; }

        string Block { get; set; }

        string ZipCode { get; set; }

        string City { get; set; }

        string County { get; set; }

        string Country { get; set; }

        string State { get; set; }

        int? LineNum { get; set; }

        string Building { get; set; }

        string AdresType { get; set; }

        string StreetNo { get; set; }

        string GlblLocNum { get; set; }

        string LicTradNum { get; set; }

        string TaxCode { get; set; }
    }
}
