﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IItemsSAP
    {
         string ItemCode { get; set; }
         string ItemName { get; set; }
    }
}
