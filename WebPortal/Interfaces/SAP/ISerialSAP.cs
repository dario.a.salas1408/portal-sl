﻿using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface ISerialSAP
    {
        List<Serial> SerialsSAP { get; set; }
        List<Serial> SerialsInDocument { get; set; }
    }
}
