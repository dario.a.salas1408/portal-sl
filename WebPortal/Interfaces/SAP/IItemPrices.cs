﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IItemPrices
    {
        string ItemCode { get; set; }

        bool HasUOMPrice { get; set; }

        bool HasCurrencyPrice { get; set; }

        string UOMCode { get; set; }

        decimal? Factor { get; set; }

        short? PriceList { get; set; }

        decimal? Price { get; set; }

        string Currency { get; set; }
    }
}
