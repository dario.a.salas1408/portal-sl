﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces.SAP
{
    public interface IEmployeeSAP
    {    
         int empID { get; set; }
         string lastName { get; set; }    
         string firstName { get; set; }
    }
}
