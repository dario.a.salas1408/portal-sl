﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces
{
    interface IDiverSOResult
    {
        bool succeed { get; set; }
        string error { get; set; }
    }
}
