﻿using ARGNS.Model.Implementations;
using System.Collections.Generic;

namespace ARGNS.Model.Interfaces
{
	public interface ICompany
    {
        int IdCompany { get; set; }
        int ServerType { get; set; }
        string Server { get; set; }
        string CompanyDB { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string DbUserName { get; set; }
        string DbPassword { get; set; }
        bool UseTrusted { get; set; }
        bool UseCompanyUser { get; set; }
        bool ItemInMultipleLines { get; set; }
        bool OnDemand { get; set; }
        ICollection<UsersSetting> UsersSettings { get; set; }
        ICollection<CRPageMap> CRPageMaps { get; set; }
        bool Active { get; set; }
        int? CodeType { get; set; }
    }
}
