﻿using System;

namespace ARGNS.Model.Interfaces
{
	public interface INews
    {
        int Id { get; set; }
        string Head { get; set; }
        string Description { get; set; }
        DateTime Fecha { get; set; }
        bool Active { get; set; }
    }
}
