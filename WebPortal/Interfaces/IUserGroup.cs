﻿namespace ARGNS.Model.Interfaces
{
	public interface IUserGroup
    {
        int Id { get; set; }
        string Name { get; set; }
        int IdCompany { get; set; }
        string CSSName { get; set; }
    }
}
