﻿using ARGNS.Model.Implementations;
using System.Collections.Generic;

namespace ARGNS.Model.Interfaces
{
	public interface IPage
    {
        int IdPage { get; set; }
        int IdAreaKeys { get; set; }
        AreaKey AreaKey { get; set; }
        string Description { get; set; }
        string Controller { get; set; }
        string Action { get; set; }
        string KeyResource { get; set; }
        bool Active { get; set; }
        ICollection<RolPageAction> RolPageActions { get; set; }
        ICollection<UserPageAction> UserPageActions { get; set; }
        ICollection<CRPageMap> CRPageMaps { get; set; }
        bool CR { get; set; }
    }
}
