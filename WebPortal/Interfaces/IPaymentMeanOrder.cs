﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces
{
    public interface IPaymentMeanOrder
    {
        int DocEntry { get; set; }
        int U_DocType { get; set; }
        int U_DocEntryOrder { get; set; }
        string U_PaymentCode { get; set; }
        decimal U_Amount { get; set; }
        int? U_Quotas { get; set; }
        Int16 U_CreditCard { get; set; }
        string U_Voucher { get; set; }
        DateTime? U_Date { get; set; }
        DateTime? U_DueDate { get; set; }
        string U_Account { get; set; }
        string U_Reference { get; set; }
        string U_CUIT { get; set; }
        decimal U_Neto { get; set; }
    }
}
