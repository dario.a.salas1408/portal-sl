﻿using System;

namespace ARGNS.Model.Interfaces
{
    public interface ILocalDrafts
    {
        int DocNum { get; set; }
        string ObjectCode { get; set; }
        string CardCode { get; set; }
        string CardName { get; set; }
        string NumAtCard { get; set; }
        DateTime? DocDate { get; set; }
        DateTime? DocDueDate { get; set; }
        string Comments { get; set; }
        int UserId { get; set; }
        int CompanyId { get; set; }
        string DocStatus { get; set; }
        string ObjType { get; set; }
        DateTime LastUpdatedTime { get; set; }
        short? GroupNum { get; set; }
        int DraftOrigin { get; set; }
    }
}
