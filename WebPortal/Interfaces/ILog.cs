﻿using ARGNS.Model.Implementations;
using System;

namespace ARGNS.Model.Interfaces
{
	public interface ILog
    {
        int IdLog { get; set; }
        int IdUser { get; set; }
        string strClave { get; set; }
        string Tipo { get; set; }
        string strLog { get; set; }
        DateTime dtLog { get; set; }
        User User { get; set; }

    }
}
