﻿using ARGNS.Model.Implementations;
using System.Collections.Generic;

namespace ARGNS.Model.Interfaces
{
	public interface IActions
    {

        int IdAction { get; set; }

        string Description { get; set; }

        bool Active { get; set; }

        ICollection<RolPageAction> RolPageActions { get; set; }

        ICollection<UserPageAction> UserPageActions { get; set; }
    }
}
