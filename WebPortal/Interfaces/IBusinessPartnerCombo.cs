﻿using ARGNS.Model.Implementations;
using System.Collections.Generic;

namespace ARGNS.Model.Interfaces
{
	interface IBusinessPartnerCombo
    {
        List<BusinessPartnerType> BPTypeList { get; set; }
    }
}
