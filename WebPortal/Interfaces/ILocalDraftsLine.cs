﻿using ARGNS.Model.Implementations;
using System;

namespace ARGNS.Model.Interfaces
{
    public interface ILocalDraftsLine
    {
        int Id { get; set; }
        int LineNum { get; set; }
        string ItemCode { get; set; }
        decimal? Quantity { get; set; }
        decimal? PriceBefDi { get; set; }
        DateTime? ShipDate { get; set; }
        string WhsCode { get; set; }
        decimal? DiscPrcnt { get; set; }
        LocalDrafts Header { get; set; }
        string Currency { get; set; }
        string Dscription { get; set; }
    }
}
