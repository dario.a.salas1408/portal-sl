﻿namespace ARGNS.Model.Interfaces
{
	public interface IAlert
    {
        int Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
    }
}
