﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces
{
    public interface IPaymentMean
    {
        string Code { get; set; }
        string Name { get; set; }
        string U_BranchCode { get; set; }
        decimal? U_AmountRule { get; set; }
        decimal? U_Surcharge { get; set; }
        string U_Type { get; set; }
    }
}
