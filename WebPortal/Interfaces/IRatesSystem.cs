﻿namespace ARGNS.Model.Interfaces
{
	public interface IRatesSystem
    {
        string Currency { get; set; }
        double Rate { get; set; }
        bool Local { get; set; }
        bool System { get; set; }
    }
}
