﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Model.Interfaces
{
    public interface IPaymentMeanType
    {
        string Code { get; set; }
        string Name { get; set; }
        string U_type { get; set; }
    }
}
