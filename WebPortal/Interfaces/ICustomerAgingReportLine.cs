﻿using System;

namespace ARGNS.Model.Interfaces
{
	public interface ICustomerAgingReportLine
    {
        string CustNum { get; set; }
        string CustName { get; set; }
        decimal? DebitAmt { get; set; }
        decimal? CreditAmt { get; set; }
        string TransType { get; set; }
        string Reference { get; set; }
        string Currency { get; set; }
        DateTime PostingDate { get; set; }
        DateTime DueDate { get; set; }
        DateTime DocDate { get; set; }
        decimal? from0to30Days { get; set; }
        decimal? from31to60Days { get; set; }
        decimal? from61to90days { get; set; }
        decimal? from90to120Days { get; set; }
        decimal? from120PlusDays { get; set; }
    }
}
