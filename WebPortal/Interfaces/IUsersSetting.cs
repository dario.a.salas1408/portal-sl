﻿using ARGNS.Model.Implementations;

namespace ARGNS.Model.Interfaces
{
	public interface IUsersSetting
    {
        //int Id { get; set; }
        CompanyConn CompanyConn { get; set; }
        User User { get; set; }
        string IdBp { get; set; }
        int? IdSE { get; set; }
        string UserNameSAP { get; set; }
        string UserPasswordSAP { get; set; }
        string UserCodeSAP { get; set; }
        string DftDistRule { get; set; }
        string DftWhs { get; set; }
    }
}
