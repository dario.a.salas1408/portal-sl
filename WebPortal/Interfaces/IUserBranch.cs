﻿namespace ARGNS.Model.Interfaces
{
    public interface IUserBranch
    {
        int IdUserBranches { get; set; }
        int IdUser { get; set; }
        int IdCompany { get; set; }
        int IdBranchSAP { get; set; }
        string BranchName { get; set; }
    }
}
