﻿using ARGNS.Model.Implementations;

namespace ARGNS.Model.Interfaces
{
	public interface IUserArea
    {

        int IdAreaKeys { get; set; }

        int IdUser { get; set; }

        string KeyAdd { get; set; }

        AreaKey AreaKey { get; set; }

        User User { get; set; }
    }
}
