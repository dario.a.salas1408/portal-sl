﻿using ARGNS.Model.Implementations;

namespace ARGNS.Model.Interfaces
{
	public interface IUserPageAction
    {
        int IdUser { get; set; }

        int IdPage { get; set; }

        int IdAction { get; set; }

        Actions Action { get; set; }

        Page Page { get; set; }

        User User { get; set; }
    }
}
