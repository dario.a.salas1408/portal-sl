﻿using ARGNS.Model.Implementations;
using System.Collections.Generic;

namespace ARGNS.Model.Interfaces
{
	public interface IUser
    {
        int IdUser { get; set; }
        string Name { get; set; }
        string Password { get; set; }
        short UserIdSAP { get; set; }
        //List<UserGroup> UserGroup { get; set; }
        List<Authorization> Authorization { get; set; }
        ICollection<UsersSetting> UsersSettings { get; set; }
        bool Active { get; set; }
    }
}
