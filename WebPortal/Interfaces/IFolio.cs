﻿namespace ARGNS.Model.Interfaces
{
    public interface IFolio
    {
        int Id { get; set; }
        string Type { get; set; }
        int FolioNum { get; set; }
    }
}
