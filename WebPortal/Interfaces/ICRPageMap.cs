﻿using ARGNS.Model.Implementations;

namespace ARGNS.Model.Interfaces
{
	public interface ICRPageMap
    {
        int IdICRPageMap { get; set; }
        CompanyConn CompanyConn { get; set; }
        Page Page { get; set; }
        string CRName { get; set; }
    }
}
