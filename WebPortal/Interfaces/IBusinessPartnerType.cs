﻿namespace ARGNS.Model.Interfaces
{
	interface IBusinessPartnerType
    {
        string Code { get; set; }
        string Name { get; set; }
    }
}
