namespace ARGNS.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SerialBatch2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QRConfigs", "SerialPosition", c => c.Int());
            AddColumn("dbo.QRConfigs", "BatchPosition", c => c.Int());
            DropColumn("dbo.QRConfigs", "SerialBatchPosition");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QRConfigs", "SerialBatchPosition", c => c.Int());
            DropColumn("dbo.QRConfigs", "BatchPosition");
            DropColumn("dbo.QRConfigs", "SerialPosition");
        }
    }
}
