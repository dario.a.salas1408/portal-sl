namespace ARGNS.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class qrconfig : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.QRConfigs", "IdUser");
        }
        
        public override void Down()
        {
            AddColumn("dbo.QRConfigs", "IdUser", c => c.Int(nullable: false));
        }
    }
}
