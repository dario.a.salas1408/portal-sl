namespace ARGNS.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testing : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actions",
                c => new
                    {
                        IdAction = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        InternalKey = c.String(),
                        KeyResource = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.IdAction);
            
            CreateTable(
                "dbo.Pages",
                c => new
                    {
                        IdPage = c.Int(nullable: false, identity: true),
                        IdAreaKeys = c.Int(nullable: false),
                        Description = c.String(maxLength: 50),
                        Controller = c.String(maxLength: 150),
                        Action = c.String(maxLength: 150),
                        KeyResource = c.String(maxLength: 150),
                        KeyAdd = c.String(maxLength: 500),
                        Active = c.Boolean(nullable: false),
                        InternalKey = c.String(),
                        CR = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdPage)
                .ForeignKey("dbo.AreaKeys", t => t.IdAreaKeys, cascadeDelete: true)
                .Index(t => t.IdAreaKeys);
            
            CreateTable(
                "dbo.AreaKeys",
                c => new
                    {
                        IdAreaKeys = c.Int(nullable: false, identity: true),
                        Area = c.String(nullable: false, maxLength: 20),
                        KeyNumber = c.String(nullable: false, maxLength: 500),
                        Controller = c.String(),
                        Action = c.String(maxLength: 150),
                        KeyResource = c.String(maxLength: 150),
                    })
                .PrimaryKey(t => t.IdAreaKeys);
            
            CreateTable(
                "dbo.UserAreas",
                c => new
                    {
                        IdAreaKeys = c.Int(nullable: false),
                        IdUser = c.Int(nullable: false),
                        KeyAdd = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => new { t.IdAreaKeys, t.IdUser })
                .ForeignKey("dbo.AreaKeys", t => t.IdAreaKeys, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.IdUser, cascadeDelete: true)
                .Index(t => t.IdAreaKeys)
                .Index(t => t.IdUser);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        IdUser = c.Int(nullable: false, identity: true),
                        IdRol = c.Int(),
                        Name = c.String(maxLength: 100),
                        Password = c.String(),
                        UserIdSAP = c.Short(nullable: false),
                        UserPasswordSAP = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdUser)
                .ForeignKey("dbo.Roles", t => t.IdRol)
                .Index(t => t.IdRol);
            
            CreateTable(
                "dbo.Authorizations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        UserGroup_Id = c.Int(),
                        User_IdUser = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserGroups", t => t.UserGroup_Id)
                .ForeignKey("dbo.Users", t => t.User_IdUser)
                .Index(t => t.UserGroup_Id)
                .Index(t => t.User_IdUser);
            
            CreateTable(
                "dbo.UserGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        User_IdUser = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_IdUser)
                .Index(t => t.User_IdUser);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        IdRol = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 50),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdRol);
            
            CreateTable(
                "dbo.RolPageAction",
                c => new
                    {
                        IdRol = c.Int(nullable: false),
                        IdPage = c.Int(nullable: false),
                        IdAction = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdRol, t.IdPage, t.IdAction })
                .ForeignKey("dbo.Roles", t => t.IdRol)
                .ForeignKey("dbo.Pages", t => t.IdPage)
                .ForeignKey("dbo.Actions", t => t.IdAction)
                .Index(t => t.IdRol)
                .Index(t => t.IdPage)
                .Index(t => t.IdAction);
            
            CreateTable(
                "dbo.UserPageAction",
                c => new
                    {
                        IdUser = c.Int(nullable: false),
                        IdPage = c.Int(nullable: false),
                        IdAction = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdUser, t.IdPage, t.IdAction })
                .ForeignKey("dbo.Users", t => t.IdUser)
                .ForeignKey("dbo.Pages", t => t.IdPage)
                .ForeignKey("dbo.Actions", t => t.IdAction)
                .Index(t => t.IdUser)
                .Index(t => t.IdPage)
                .Index(t => t.IdAction);
            
            CreateTable(
                "dbo.UsersSettings",
                c => new
                    {
                        IdUser = c.Int(nullable: false),
                        IdCompany = c.Int(nullable: false),
                        IdBp = c.String(),
                        IdSE = c.Int(),
                        UserNameSAP = c.String(maxLength: 50),
                        UserPasswordSAP = c.String(maxLength: 50),
                        UserCodeSAP = c.String(),
                        DftDistRule = c.String(),
                        DftWhs = c.String(),
                    })
                .PrimaryKey(t => new { t.IdUser, t.IdCompany })
                .ForeignKey("dbo.CompanyConns", t => t.IdCompany)
                .ForeignKey("dbo.Users", t => t.IdUser)
                .Index(t => t.IdUser)
                .Index(t => t.IdCompany);
            
            CreateTable(
                "dbo.CompanyConns",
                c => new
                    {
                        IdCompany = c.Int(nullable: false, identity: true),
                        ServerType = c.Int(nullable: false),
                        Server = c.String(maxLength: 100),
                        CompanyDB = c.String(maxLength: 100),
                        UserName = c.String(maxLength: 8),
                        Password = c.String(maxLength: 254),
                        DbUserName = c.String(maxLength: 8),
                        DbPassword = c.String(maxLength: 254),
                        UseTrusted = c.Boolean(nullable: false),
                        UseCompanyUser = c.Boolean(nullable: false),
                        ItemInMultipleLines = c.Boolean(nullable: false),
                        UseHandheld = c.Boolean(nullable: false),
                        CheckBasketID = c.Boolean(nullable: false),
                        OnDemand = c.Boolean(nullable: false),
                        Active = c.Boolean(nullable: false),
                        LicenseServer = c.String(),
                        PortNumber = c.Int(nullable: false),
                        SAPLanguaje = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.IdCompany);
            
            CreateTable(
                "dbo.CompanyStock",
                c => new
                    {
                        IdCompanyStock = c.Int(nullable: false, identity: true),
                        IdCompany = c.Int(nullable: false),
                        ValidateStock = c.Boolean(nullable: false),
                        Formula = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.IdCompanyStock)
                .ForeignKey("dbo.CompanyConns", t => t.IdCompany)
                .Index(t => t.IdCompany);
            
            CreateTable(
                "dbo.CRPageMaps",
                c => new
                    {
                        IdICRPageMap = c.Int(nullable: false, identity: true),
                        IdPage = c.Int(nullable: false),
                        IdCompany = c.Int(nullable: false),
                        CRName = c.String(),
                    })
                .PrimaryKey(t => t.IdICRPageMap)
                .ForeignKey("dbo.CompanyConns", t => t.IdCompany)
                .ForeignKey("dbo.Pages", t => t.IdPage)
                .Index(t => t.IdPage)
                .Index(t => t.IdCompany);
            
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Description = c.String(maxLength: 254),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Head = c.String(maxLength: 100),
                        Description = c.String(),
                        Fecha = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PageActions",
                c => new
                    {
                        IdAction = c.Int(nullable: false),
                        IdPage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IdAction, t.IdPage })
                .ForeignKey("dbo.Actions", t => t.IdAction, cascadeDelete: true)
                .ForeignKey("dbo.Pages", t => t.IdPage, cascadeDelete: true)
                .Index(t => t.IdAction)
                .Index(t => t.IdPage);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserPageAction", "IdAction", "dbo.Actions");
            DropForeignKey("dbo.RolPageAction", "IdAction", "dbo.Actions");
            DropForeignKey("dbo.PageActions", "IdPage", "dbo.Pages");
            DropForeignKey("dbo.PageActions", "IdAction", "dbo.Actions");
            DropForeignKey("dbo.UserPageAction", "IdPage", "dbo.Pages");
            DropForeignKey("dbo.RolPageAction", "IdPage", "dbo.Pages");
            DropForeignKey("dbo.CRPageMaps", "IdPage", "dbo.Pages");
            DropForeignKey("dbo.UsersSettings", "IdUser", "dbo.Users");
            DropForeignKey("dbo.UsersSettings", "IdCompany", "dbo.CompanyConns");
            DropForeignKey("dbo.CRPageMaps", "IdCompany", "dbo.CompanyConns");
            DropForeignKey("dbo.CompanyStock", "IdCompany", "dbo.CompanyConns");
            DropForeignKey("dbo.UserPageAction", "IdUser", "dbo.Users");
            DropForeignKey("dbo.UserGroups", "User_IdUser", "dbo.Users");
            DropForeignKey("dbo.UserAreas", "IdUser", "dbo.Users");
            DropForeignKey("dbo.Users", "IdRol", "dbo.Roles");
            DropForeignKey("dbo.RolPageAction", "IdRol", "dbo.Roles");
            DropForeignKey("dbo.Authorizations", "User_IdUser", "dbo.Users");
            DropForeignKey("dbo.Authorizations", "UserGroup_Id", "dbo.UserGroups");
            DropForeignKey("dbo.UserAreas", "IdAreaKeys", "dbo.AreaKeys");
            DropForeignKey("dbo.Pages", "IdAreaKeys", "dbo.AreaKeys");
            DropIndex("dbo.PageActions", new[] { "IdPage" });
            DropIndex("dbo.PageActions", new[] { "IdAction" });
            DropIndex("dbo.CRPageMaps", new[] { "IdCompany" });
            DropIndex("dbo.CRPageMaps", new[] { "IdPage" });
            DropIndex("dbo.CompanyStock", new[] { "IdCompany" });
            DropIndex("dbo.UsersSettings", new[] { "IdCompany" });
            DropIndex("dbo.UsersSettings", new[] { "IdUser" });
            DropIndex("dbo.UserPageAction", new[] { "IdAction" });
            DropIndex("dbo.UserPageAction", new[] { "IdPage" });
            DropIndex("dbo.UserPageAction", new[] { "IdUser" });
            DropIndex("dbo.RolPageAction", new[] { "IdAction" });
            DropIndex("dbo.RolPageAction", new[] { "IdPage" });
            DropIndex("dbo.RolPageAction", new[] { "IdRol" });
            DropIndex("dbo.UserGroups", new[] { "User_IdUser" });
            DropIndex("dbo.Authorizations", new[] { "User_IdUser" });
            DropIndex("dbo.Authorizations", new[] { "UserGroup_Id" });
            DropIndex("dbo.Users", new[] { "IdRol" });
            DropIndex("dbo.UserAreas", new[] { "IdUser" });
            DropIndex("dbo.UserAreas", new[] { "IdAreaKeys" });
            DropIndex("dbo.Pages", new[] { "IdAreaKeys" });
            DropTable("dbo.PageActions");
            DropTable("dbo.News");
            DropTable("dbo.Alerts");
            DropTable("dbo.CRPageMaps");
            DropTable("dbo.CompanyStock");
            DropTable("dbo.CompanyConns");
            DropTable("dbo.UsersSettings");
            DropTable("dbo.UserPageAction");
            DropTable("dbo.RolPageAction");
            DropTable("dbo.Roles");
            DropTable("dbo.UserGroups");
            DropTable("dbo.Authorizations");
            DropTable("dbo.Users");
            DropTable("dbo.UserAreas");
            DropTable("dbo.AreaKeys");
            DropTable("dbo.Pages");
            DropTable("dbo.Actions");
        }
    }
}
