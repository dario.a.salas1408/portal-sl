﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace CodedUITestProject
{
	/// <summary>
	/// Summary description for CodedUITest1
	/// </summary>
	[CodedUITest]
	public class CodedUITest1
	{
		public CodedUITest1()
		{

		}

		string bpCode = "C";

		[TestMethod]
		public void AddAndEditBP()
		{
			this.UIMap.Loggin();
			bpCode = this.UIMap.AddAndEditBusinessPartner();
		}

		[TestMethod]
		public void CreatePurchaseQuotation()
		{
			this.UIMap.Loggin();
			bpCode = "V";
			this.UIMap.CreatePurchaseQuotation(bpCode);
            System.Threading.Thread.Sleep(10000);
			this.UIMap.UpdatePurchaseQuotation();
		}

        [TestMethod]
        public void CreatePurchaseOrderFromRequest()
        {
            this.UIMap.Loggin();
            this.UIMap.CreatePurchaseOrderFromRequest();
        }

        [TestMethod]
		public void CreatePurchaseRequest()
		{
			this.UIMap.Loggin();
            this.UIMap.CreatePurchaseRequest(bpCode);
		}

		[TestMethod]
		public void AddVendor()
		{
			this.UIMap.Loggin();
			this.UIMap.AddVendor();
		}

		[TestMethod]
		public void CreateSalesOrder()
		{
			this.UIMap.Loggin();
			this.UIMap.CreateSalesOrder(bpCode);
		}

		[TestMethod]
		public void CreatePurchaseOrder()
		{
			this.UIMap.Loggin();
			bpCode = "V";

			this.UIMap.CreatePurchaseOrder(bpCode);
		}

        [TestMethod]
        public void CreateSalesQuotation()
        {
            this.UIMap.Loggin();
            this.UIMap.CreateUpdateSalesQuotation();
        }

        [TestMethod]
		public void CreateSalesOpportunity()
		{
			this.UIMap.Loggin();

			this.UIMap.CreateSalesOpportunity(bpCode);
			this.UIMap.UpdateSalesOpportunity();
		}

		#region Additional test attributes

		// You can use the following additional attributes as you write your tests:

		////Use TestInitialize to run code before running each test 
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{        
		//    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
		//}

		////Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{        
		//    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
		//}

		#endregion

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}
		private TestContext testContextInstance;

		public UIMap UIMap
		{
			get
			{
				if ((this.map == null))
				{
					this.map = new UIMap();
				}

				return this.map;
			}
		}

		private UIMap map;
	}
}
