﻿namespace CodedUITestProject
{
    using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using System.Threading;

    public partial class UIMap
	{
		/// <summary>
		/// AddAndEditBusinessPartner - Use 'AddAndEditBusinessPartnerParams' to pass parameters into this method.
		/// </summary>
		public string AddAndEditBusinessPartner()
		{
			// Type 'V00001' in 'CardCode' text box
			Random ran = new Random();
			string bpCode = "";
			bpCode = "C" + ran.Next(999999).ToString("0000000");

			#region Variable Declarations
			HtmlHyperlink uIAreasHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIDatosMaestrosHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIDatosMaestrosHyperlink;
			HtmlDiv uIItemPane = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIItemPane;
			HtmlHyperlink uIDatosMaestrosHyperlink1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UIDatosMaestrosHyperlink;
			HtmlHyperlink uISociosdeNegocioHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UISociosdeNegocioHyperlink;
			HtmlDiv uIItemPane1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UIItemPane;
			BrowserWindow uIHotmailOutlookSkypenWindow = this.UIHotmailOutlookSkypenWindow;
			HtmlDiv uIDatosMaestrosMisSociPane = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UIDatosMaestrosMisSociPane;
			HtmlHyperlink uIAgregarSociodeNegociHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UIBodyBPPane.UIAgregarSociodeNegociHyperlink;
			HtmlEdit uICardCodeEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UICardCodeEdit;
			HtmlDiv uIItemPane2 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UIFormBusinessPartnerCustom.UIItemPane;
			HtmlEdit uICardNameEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UICardNameEdit;
			HtmlDocument uIHttpwebportalbaselinDocument3 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3;
			HtmlButton uIOKButton1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UIOKButton;
			HtmlEdit uITxtCodeBPEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument4.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument4.UIBuscarButton;
			HtmlCell uIAutomaticTestTestCell = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument4.UIAssetsdatatableTable.UIAutomaticTestTestCell;
			HtmlHyperlink uIEditarHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument4.UIAssetsdatatableTable.UIEditarHyperlink;
			HtmlDocument uIHttpwebportalbaselinDocument5 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument5;
			HtmlButton uIOKButton2 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument5.UIOKButton;
			HtmlHyperlink uICancelarHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument5.UICancelarHyperlink;
			HtmlEdit uITxtCodeBPEdit1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UIBuscarButton;
			HtmlDiv uISearchBuscarPane = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UISearchBuscarPane;
			HtmlDiv uIShow102550100entriesPane = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UIBodyBPPane1.UIShow102550100entriesPane;
			HtmlDiv uIShow102550100entriesPane1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UIBodyBPPane1.UIShow102550100entriesPane1;
			HtmlHyperlink uIEditarHyperlink1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UIAssetsdatatableTable.UIEditarHyperlink;
			HtmlEdit uIOprCountEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument5.UIOprCountEdit;
			HtmlDiv uIInformacióndeContactPane = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument5.UIInformacióndeContactPane;
			HtmlEdit uICellularEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument5.UICellularEdit;
			HtmlEdit uIMailAddresEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument5.UIMailAddresEdit;
			#endregion

			OpenBussinessPartner();

			// Click 'Agregar Socio de Negocio' link
			Mouse.Click(uIAgregarSociodeNegociHyperlink);

   //         uICardCodeEdit.Text = bpCode;

			//// Type '{Tab}' in 'CardCode' text box
			//Keyboard.SendKeys(uICardCodeEdit, this.AddAndEditBusinessPartnerParams.UICardCodeEditSendKeys, ModifierKeys.None);


			//// Type 'AutomaticTest Test' in 'CardName' text box
			//uICardNameEdit.Text = this.AddAndEditBusinessPartnerParams.UICardNameEditText;

			//// Click 'Ok' button to save the BP 
			//Mouse.Click(uIOKButton1);

			//// Type 'V000' in 'txtCodeBP' text box
			//uITxtCodeBPEdit1.Text = bpCode;

			//// Click 'Buscar' button
			//Mouse.Click(uIBuscarButton1);

			//// Click 'Editar' link
			//Mouse.Click(uIEditarHyperlink1);

			//// Type 'AutomaticTest.oviedo551' in 'MailAddres' text box
			//uIMailAddresEdit.Text = this.AddAndEditBusinessPartnerParams.UIMailAddresEditText;

			//// Type 'Alt + {NumPad6}' in 'MailAddres' text box
			//Keyboard.SendKeys(uIMailAddresEdit, this.AddAndEditBusinessPartnerParams.UIMailAddresEditSendKeys, ModifierKeys.Alt);

			//// Type 'Alt + {NumPad4}' in 'MailAddres' text box
			//Keyboard.SendKeys(uIMailAddresEdit, this.AddAndEditBusinessPartnerParams.UIMailAddresEditSendKeys1, ModifierKeys.Alt);

			//// Type 'AutomaticTest.oviedo551@gmail.com' in 'MailAddres' text box
			//uIMailAddresEdit.Text = this.AddAndEditBusinessPartnerParams.UIMailAddresEditText1;

			//// Click 'Ok' button
			//Mouse.Click(uIOKButton2);
			return bpCode;
		}

		/// <summary>
		/// 
		/// </summary>
		public void Loggin()
		{
			HtmlEdit uIUsernameEdit = this.UIHotmailOutlookSkypenWindow.UIWebPortalDocument.UIUsernameEdit;
			HtmlEdit uIPasswordEdit = this.UIHotmailOutlookSkypenWindow.UIWebPortalDocument.UIPasswordEdit;
			HtmlButton uIIngresarButton = this.UIHotmailOutlookSkypenWindow.UIWebPortalDocument.UILoginFormCustom.UIIngresarButton;
			HtmlButton uICancelarButton = this.UIHotmailOutlookSkypenWindow.UIWebPortalDocument.UICancelarButton;
			HtmlComboBox uIItemComboBox = this.UIHotmailOutlookSkypenWindow.UIWebPortalDocument.UIMyModalPane1.UIItemComboBox;
			HtmlButton uIOKButton = this.UIHotmailOutlookSkypenWindow.UIWebPortalDocument.UIOKButton;

			// Go to web page 'http://webportalbaseline.com.ar/' using new browser instance
			this.UIHotmailOutlookSkypenWindow.LaunchUrl(new System.Uri(this.AddAndEditBusinessPartnerParams.UIHotmailOutlookSkypenWindowUrl));

			// Last mouse action was not recorded.

			// Type 'demo' in 'username' text box
			uIUsernameEdit.Text = this.AddAndEditBusinessPartnerParams.UIUsernameEditText;

			// Type '{Tab}' in 'username' text box
			Keyboard.SendKeys(uIUsernameEdit, this.AddAndEditBusinessPartnerParams.UIUsernameEditSendKeys, ModifierKeys.None);

			// Type '********' in 'password' text box
			uIPasswordEdit.Password = this.AddAndEditBusinessPartnerParams.UIPasswordEditPassword1;

			// Click 'Ingresar' button
			Mouse.Click(uIIngresarButton);

			// Select 'ArgentinaTest' in combo box
			uIItemComboBox.SelectedItem = this.AddAndEditBusinessPartnerParams.UIItemComboBoxSelectedItem;

			// Click 'Ok' button
			Mouse.Click(uIOKButton);
		}

		public virtual AddAndEditBusinessPartnerParams AddAndEditBusinessPartnerParams
		{
			get
			{
				if ((this.mAddAndEditBusinessPartnerParams == null))
				{
					this.mAddAndEditBusinessPartnerParams = new AddAndEditBusinessPartnerParams();
				}
				return this.mAddAndEditBusinessPartnerParams;
			}
		}

		private AddAndEditBusinessPartnerParams mAddAndEditBusinessPartnerParams;

		/// <summary>
		/// CreateSaelsOrder - Use 'CreateSaelsOrderParams' to pass parameters into this method.
		/// </summary>
		public void CreateSalesOrder(string bpCode)
		{
			#region Variable Declarations
			HtmlHyperlink uIWebPortalHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UIWebPortalHyperlink;

			HtmlDiv uIBuscarBuscarClPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UIBuscarBuscarClPane;
			HtmlHyperlink uINuevaOrdendeVentaHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UIBodyPOPane.UINuevaOrdendeVentaHyperlink;
			HtmlDiv uIDolaramericanoEuroPePane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIDolaramericanoEuroPePane;
			HtmlLabel uIPersonadeContactoLabel = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIPersonadeContactoLabel;
			HtmlDiv uIBuscarPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIBuscarPane;
			HtmlDiv uIClientesPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIClientesPane;
			HtmlDiv uIBuscarDocumentoNroPePane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIBuscarDocumentoNroPePane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIBuscarButton;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIBuscarButton;
			HtmlDiv uIShow102550100entriesPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIVendorModalPane.UIShow102550100entriesPane;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlDiv uIInformaciónPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIInformaciónPane;
			HtmlButton uINuevoporListaButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UINuevoporListaButton;
			HtmlDiv uICódigoPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIMyModalPane.UICódigoPane;
			HtmlEdit uICodeEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UICodeEdit;
			HtmlButton uIBuscarButton11 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIBuscarButton1;
			HtmlDiv uIAssetsdatatableListIPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIAssetsdatatableListIPane;
			HtmlCheckBox uICkSelectItem_MasterICheckBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UICkSelectItem_MasterICheckBox;
			HtmlButton uIElegirButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIElegirButton;
			HtmlDiv uILoadingPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UILoadingPane.UILoadingPane1;
			HtmlDiv uIItemsSelectPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIItemsSelectPane;
			HtmlEdit uITxt0Edit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UITxt0Edit;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument4.UIBtnOkCustom;
			HtmlDiv uIOkCancelarPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument5.UISystem__ComObjectCustom.UIOkCancelarPane;
			HtmlHyperlink uICancelarHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument5.UICancelarHyperlink;
			#endregion

			WinButton uIInternetExplorer1runButton = this.UIItemWindow.UIRunningapplicationsToolBar.UIInternetExplorer1runButton;
			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIVentasHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIVentasHyperlink;
			HtmlHyperlink uIVentasHyperlink1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIVentasHyperlink;
			HtmlHyperlink uIÓrdenesdeVentaHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIÓrdenesdeVentaHyperlink;

			//// Click 'Internet Explorer - 1 running window' button
			//Mouse.Click(uIInternetExplorer1runButton, new Point(19, 29));

			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink, new Point(25, 27));

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink, new Point(48, 6));

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink1, new Point(52, 28));

			// Click 'Órdenes de Venta' link
			Mouse.Click(uIÓrdenesdeVentaHyperlink, new Point(86, 9));

			// Click 'Nueva Orden de Venta' link
			Mouse.Click(uINuevaOrdendeVentaHyperlink);

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton);

			// Type 'C001020' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1);

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton);

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton);

			// Click 'Código' pane
			Mouse.Click(uICódigoPane);

			// Type 'master item' in 'Code' text box
			uICodeEdit.Text = this.CreateSaelsOrderParams.UICodeEditText;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton11);

			// Select 'ckSelectItem_Master Item' check box
			uICkSelectItem_MasterICheckBox.Checked = this.CreateSaelsOrderParams.UICkSelectItem_MasterICheckBoxChecked;

			// Click 'Elegir' button
			Mouse.Click(uIElegirButton);

			// Type '150' in 'txt0' text box
			uITxt0Edit.Text = this.CreateSaelsOrderParams.UITxt0EditText;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom);
			System.Threading.Thread.Sleep(5000);

			#region Variable Declarations
			HtmlCustom uISystem__ComObjectCustom = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument7.UISystem__ComObjectCustom;
			//HtmlHyperlink uICancelarHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument7.UICancelarHyperlink;
			HtmlEdit uITxtCodeVendorEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UITxtCodeVendorEdit;
			HtmlEdit uITxtDateEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UITxtDateEdit;
			HtmlButton uIBuscarButton2 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UIBuscarButton;
			HtmlHyperlink uIEditarHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UIAssetsdatatableTable.UIEditarHyperlink;
			//HtmlDiv uIInformaciónPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument7.UISystem__ComObjectCustom.UIInformaciónPane;
			HtmlHeaderCell uIFechadeEnvíoCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument7.UITableItemsFormPane.UIItemTable.UIFechadeEnvíoCell;
			HtmlEdit uITypeaEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument7.UITypeaEdit;
			HtmlCustom uIBtnOkCustom2 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument7.UIBtnOkCustom;
			#endregion

			// Click 'Cancelar' link
			Mouse.Click(uICancelarHyperlink, new Point(43, 25));


			// Type 'p' in 'txtDate' text box
			uITxtDateEdit.Text = this.RecordedMethod4Params.UITxtDateEditText;

			// Type '{Tab}' in 'txtDate' text box
			Keyboard.SendKeys(uITxtDateEdit, this.RecordedMethod4Params.UITxtDateEditSendKeys, ModifierKeys.None);

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton2, new Point(57, 25));

			// Click 'Editar' link
			Mouse.Click(uIEditarHyperlink, new Point(50, 15));

			// Type '12' in 'type "a"' text box
			uITypeaEdit.Text = this.RecordedMethod4Params.UITypeaEditText;

			// Type '{Tab}' in 'type "a"' text box
			Keyboard.SendKeys(uITypeaEdit, this.RecordedMethod4Params.UITypeaEditSendKeys, ModifierKeys.None);

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom2, new Point(18, 30));
		}

		public virtual CreateSaelsOrderParams CreateSaelsOrderParams
		{
			get
			{
				if ((this.mCreateSaelsOrderParams == null))
				{
					this.mCreateSaelsOrderParams = new CreateSaelsOrderParams();
				}
				return this.mCreateSaelsOrderParams;
			}
		}

		private CreateSaelsOrderParams mCreateSaelsOrderParams;

		/// <summary>
		/// CreateSalesQuotation - Use 'CreateSalesQuotationParams' to pass parameters into this method.
		/// </summary>
		public void CreateSalesQuotation(string bpCode)
		{
			#region Variable Declarations
			HtmlHyperlink uINuevaCotizacióndeVenHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument3.UIBodyPOPane.UINuevaCotizacióndeVenHyperlink;
			HtmlDiv uITotalantesdedescuentPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UISystem__ComObjectCustom.UITotalantesdedescuentPane;
			HtmlHeaderCell uICódigodeAlmacénCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UITableItemsFormPane.UIItemTable.UICódigodeAlmacénCell;
			HtmlComboBox uIListContactComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIListContactComboBox;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UISystem__ComObjectCustom.UIBuscarButton;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UITxtCodeBPEdit;
			HtmlDiv uICódigoNombrePane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIVendorModalPane.UICódigoNombrePane;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIBuscarButton;
			HtmlDiv uIBuscarClienteCódigoNPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIVendorModalPane.UIBuscarClienteCódigoNPane;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlComboBox uIItem1PeyMethodComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIItem1PeyMethodComboBox;
			HtmlButton uINuevoporListaButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UINuevoporListaButton;
			HtmlEdit uINameEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UINameEdit;
			HtmlLabel uINombreLabel = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIMyModalPane.UINombreLabel;
			HtmlEdit uICodeEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UICodeEdit;
			HtmlSpan uIItemPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIBuscarButton1.UIItemPane;
			HtmlDiv uIModalItemsTablePane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIModalItemsTablePane;
			HtmlCheckBox uICkSelectItem_MasterICheckBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UICkSelectItem_MasterICheckBox;
			HtmlButton uIElegirButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIElegirButton;
			HtmlDiv uILoadingPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UILoadingPane.UILoadingPane1;
			HtmlDiv uIOkCancelarPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UISystem__ComObjectCustom.UIOkCancelarPane;
			#endregion


			#region Variable Declarations
			HtmlHyperlink uIAreasHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIVentasHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIVentasHyperlink;
			HtmlHyperlink uIVentasHyperlink1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIVentasHyperlink;
			HtmlHyperlink uICotizacionesdeVentaHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UICotizacionesdeVentaHyperlink;
			#endregion

			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink, new Point(21, 27));

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink, new Point(36, 15));

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink1, new Point(42, 19));

			// Click 'Cotizaciones de Venta' link
			Mouse.Click(uICotizacionesdeVentaHyperlink, new Point(120, 14));


			// Click 'Nueva Cotización de Venta' link
			Mouse.Click(uINuevaCotizacióndeVenHyperlink);
			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton);

			// Type 'c0' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1);

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton);

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton);

			// Type 'master' in 'Code' text box
			uICodeEdit.Text = this.CreateSalesQuotationParams.UICodeEditText;

			// Click pane
			Mouse.Click(uIItemPane);

			// Select 'ckSelectItem_Master Item' check box
			uICkSelectItem_MasterICheckBox.Checked = this.CreateSalesQuotationParams.UICkSelectItem_MasterICheckBoxChecked;

			// Click 'Elegir' button
			Mouse.Click(uIElegirButton);

			// Click 'Ok Cancelar' pane
			Mouse.Click(uIOkCancelarPane);
		}

		public virtual CreateSalesQuotationParams CreateSalesQuotationParams
		{
			get
			{
				if ((this.mCreateSalesQuotationParams == null))
				{
					this.mCreateSalesQuotationParams = new CreateSalesQuotationParams();
				}
				return this.mCreateSalesQuotationParams;
			}
		}

		private CreateSalesQuotationParams mCreateSalesQuotationParams;

		/// <summary>
		/// AddVendor - Use 'AddVendorParams' to pass parameters into this method.
		/// </summary>
		public string AddVendor()
		{
			#region Variable Declarations
			HtmlHyperlink uIWebPortalHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UIWebPortalHyperlink;
			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIAreasHyperlink;
			HtmlHyperlink uIDatosMaestrosHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIDatosMaestrosHyperlink;
			HtmlHyperlink uIDatosMaestrosHyperlink1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UIDatosMaestrosHyperlink;
			HtmlHyperlink uISociosdeNegocioHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UISociosdeNegocioHyperlink;
			HtmlDiv uIItemPane1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UIItemPane1;
			HtmlHyperlink uIAgregarSociodeNegociHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument2.UIBodyBPPane.UIAgregarSociodeNegociHyperlink;
			HtmlEdit uICardCodeEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UICardCodeEdit;
			HtmlEdit uICardNameEdit = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UICardNameEdit;
			HtmlComboBox uICardTypeComboBox = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UICardTypeComboBox;
			HtmlComboBox uICurrencyComboBox = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UICurrencyComboBox;
			HtmlDiv uISociosdeNegocioPane = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UISociosdeNegocioPane;
			HtmlDiv uIVarformModeAddvarLawPane = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UIVarformModeAddvarLawPane;
			HtmlComboBox uIListNumComboBox = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UIListNumComboBox;
			HtmlButton uIOKButton = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument3.UIOKButton;
			#endregion

			Random ran = new Random();
			string vendorCode = "V" + ran.Next(999999).ToString("0000000");

			OpenBussinessPartner();

			// Click 'Agregar Socio de Negocio' link
			Mouse.Click(uIAgregarSociodeNegociHyperlink);

			// Type 'V000001' in 'CardCode' text box
			uICardCodeEdit.Text = vendorCode;

			// Type 'AutomaticTest Vendor' in 'CardName' text box
			uICardNameEdit.Text = this.AddVendorParams.UICardNameEditText;

			// Select 'Vendor' in 'CardType' combo box
			uICardTypeComboBox.SelectedItem = this.AddVendorParams.UICardTypeComboBoxSelectedItem;


			// Click 'Socios de Negocio' pane
			Mouse.Click(uISociosdeNegocioPane);

			// Click 'var formMode = 'Add'; var LawSet = 'AR';' pane
			Mouse.Click(uIVarformModeAddvarLawPane);

			// Click 'Ok' button
			Mouse.Click(uIOKButton);

			return vendorCode;
		}

		public virtual AddVendorParams AddVendorParams
		{
			get
			{
				if ((this.mAddVendorParams == null))
				{
					this.mAddVendorParams = new AddVendorParams();
				}
				return this.mAddVendorParams;
			}
		}

		private AddVendorParams mAddVendorParams;

		/// <summary>
		/// CreatePurchaseOrder - Use 'CreatePurchaseOrderParams' to pass parameters into this method.
		/// </summary>
		public void CreatePurchaseOrder(string vendorCode)
		{
			#region Variable Declarations
			HtmlHyperlink uIWebPortalHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument4.UIWebPortalHyperlink;
			HtmlDiv uIBuscarBuscarVPane = this.UIHttpwebportalbaselinWindow2.UIHttpwebportalbaselinDocument3.UIBuscarBuscarVPane;
			HtmlHyperlink uINuevaOrdendeCompraHyperlink = this.UIHttpwebportalbaselinWindow2.UIHttpwebportalbaselinDocument3.UIBodyPOPane.UINuevaOrdendeCompraHyperlink;
			HtmlDiv uIVendedorBuscarDocumePane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIVendedorBuscarDocumePane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIBuscarButton;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIBuscarButton;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlEdit uICancelDateEdit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICancelDateEdit;
			HtmlDiv uINohaytarifasdisponibPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UINohaytarifasdisponibPane;
			HtmlButton uINuevoporListaButton = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UINuevoporListaButton;
			HtmlEdit uINameEdit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UINameEdit;
			HtmlEdit uICodeEdit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICodeEdit;
			HtmlLabel uINombreLabel = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIMyModalPane.UINombreLabel;
			HtmlButton uIBuscarButton11 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIBuscarButton1;
			HtmlDiv uIModalItemsTablePane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIModalItemsTablePane;
			HtmlCheckBox uICkSelectItem_MasterICheckBox = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICkSelectItem_MasterICheckBox;
			HtmlButton uIElegirButton = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIElegirButton;
			HtmlDiv uILoadingPane1 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UILoadingPane.UILoadingPane1;
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIItemPane;
			HtmlEdit uITypeaEdit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UITypeaEdit;
			HtmlEdit uITxtcount1Edit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UITxtcount1Edit;
			HtmlComboBox uIDp1ComboBox = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIDp1ComboBox;
			HtmlEdit uITxt1Edit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UITxt1Edit;
			HtmlDocument uIHttpwebportalbaselinDocument4 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIBtnOkCustom;
			#endregion

			#region Variable Declarations
			HtmlHyperlink uIAreasHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIComprasHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIComprasHyperlink;
			HtmlHyperlink uIComprasHyperlink1 = this.UIHttpwebportalbaselinWindow1.UIHttpwebportalbaselinDocument2.UIComprasHyperlink;
			HtmlHyperlink uIÓrdenesdeCompraHyperlink = this.UIHttpwebportalbaselinWindow1.UIHttpwebportalbaselinDocument2.UIÓrdenesdeCompraHyperlink;
			#endregion

			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink, new Point(32, 41));

			// Click 'Compras' link
			Mouse.Click(uIComprasHyperlink, new Point(66, 14));

			// Click 'Compras' link
			Mouse.Click(uIComprasHyperlink1, new Point(46, 13));

			// Click 'Órdenes de Compra' link
			Mouse.Click(uIÓrdenesdeCompraHyperlink, new Point(140, 8));

			// Click 'Nueva Orden de Compra' link
			Mouse.Click(uINuevaOrdendeCompraHyperlink);

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(32, 11));

			// Type 'V00' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = vendorCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(31, 19));

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton, new Point(41, 9));

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton, new Point(50, 12));

			// Type 'item master' in 'Code' text box
			uICodeEdit.Text = "master";

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton11, new Point(33, 5));

            // Select 'ckSelectItem_Master Item' check box
            uICkSelectItem_MasterICheckBox.Checked = this.CreatePurchaseOrderParams.UICkSelectItem_MasterICheckBoxChecked;

			// Click 'Elegir' button
			Mouse.Click(uIElegirButton, new Point(27, 9));

			// Type '20' in 'txtcount1' text box
			uITxtcount1Edit.Text = this.CreatePurchaseOrderParams.UITxtcount1EditText;

			// Type '{Tab}' in 'txtcount1' text box
			Keyboard.SendKeys(uITxtcount1Edit, this.CreatePurchaseOrderParams.UITxtcount1EditSendKeys, ModifierKeys.None);

			// Type '600' in 'txt1' text box
			uITxt1Edit.Text = this.CreatePurchaseOrderParams.UITxt1EditText;

			// Type '{Tab}' in 'txt1' text box
			Keyboard.SendKeys(uITxt1Edit, this.CreatePurchaseOrderParams.UITxt1EditSendKeys, ModifierKeys.None);

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(23, 27));

			HtmlEdit uITxtDateEdit = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UITxtDateEdit;
			HtmlComboBox uITxtDocStatusComboBox = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UITxtDocStatusComboBox;
			HtmlDiv uIBuscarPaneEdit = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UICollapseOnePane.UIBuscarPane;
			HtmlButton uIBuscarButtonEdit = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UIBuscarButton;
			HtmlHyperlink uIEditarHyperlinkEdit = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UIAssetsdatatableTable.UIEditarHyperlink;
			HtmlDiv uIImpuestosPane = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UISystem__ComObjectCustom.UIImpuestosPane;
			HtmlHeaderCell uIDescripciónCell = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UITableItemsFormPane.UIItemTable.UIDescripciónCell;
			HtmlCheckBox uICkSelectItem_ItemMasCheckBox = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UICkSelectItem_ItemMasCheckBox;
			HtmlDiv uIBuscarSearchByCataloPane = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UIMyModalPane.UIBuscarSearchByCataloPane;

			System.Threading.Thread.Sleep(10000);

			// Type 'p' in 'txtDate' text box
			uITxtDateEdit.Text = this.RecordedMethod6Params.UITxtDateEditText;

			// Type '{Tab}' in 'txtDate' text box
			Keyboard.SendKeys(uITxtDateEdit, this.RecordedMethod6Params.UITxtDateEditSendKeys, ModifierKeys.None);

			// Select 'Abierta' in 'txtDocStatus' combo box
			uITxtDocStatusComboBox.SelectedItem = this.RecordedMethod6Params.UITxtDocStatusComboBoxSelectedItem;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButtonEdit, new Point(45, 13));

			// Click 'Editar' link
			Mouse.Click(uIEditarHyperlinkEdit, new Point(45, 24));

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton, new Point(61, 20));

			// Type 'master' in 'Code' text box
			uICodeEdit.Text = this.RecordedMethod6Params.UICodeEditText;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton11, new Point(44, 17));

            // Select 'ckSelectItem_Item Master' check box
            uICkSelectItem_MasterICheckBox.Checked = this.CreatePurchaseOrderParams.UICkSelectItem_MasterICheckBoxChecked;

            // Click 'Elegir' button
            Mouse.Click(uIElegirButton, new Point(27, 9));

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(9, 9));
		}

		public virtual CreatePurchaseOrderParams CreatePurchaseOrderParams
		{
			get
			{
				if ((this.mCreatePurchaseOrderParams == null))
				{
					this.mCreatePurchaseOrderParams = new CreatePurchaseOrderParams();
				}
				return this.mCreatePurchaseOrderParams;
			}
		}

		private CreatePurchaseOrderParams mCreatePurchaseOrderParams;

		/// <summary>
		/// OpenBussinessPartner
		/// </summary>
		public void OpenBussinessPartner()
		{
			#region Variable Declarations
			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIDatosMaestrosHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIDatosMaestrosHyperlink;
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIItemPane;
			HtmlHyperlink uIDatosMaestrosHyperlink1 = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UIDatosMaestrosHyperlink;
			HtmlHyperlink uISociosdeNegocioHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument1.UISociosdeNegocioHyperlink;
			#endregion
			HtmlHyperlink uIWebPortalHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIWebPortalHyperlink;

			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink);
            Thread.Sleep(5000);

			// Click 'Datos Maestros' link
			Mouse.Click(uIDatosMaestrosHyperlink);
            Thread.Sleep(5000);

            // Click 'Datos Maestros' link
            Mouse.Click(uIDatosMaestrosHyperlink1);
            Thread.Sleep(5000);

            // Click 'Socios de Negocio' link
            Mouse.Click(uISociosdeNegocioHyperlink);
            Thread.Sleep(5000);
        }

		/// <summary>
		/// CreatePurchaseQuotation - Use 'CreatePurchaseQuotationParams' to pass parameters into this method.
		/// </summary>
		public void CreatePurchaseQuotation(string vendorCode)
		{
			HtmlHyperlink uIWebPortalHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument4.UIWebPortalHyperlink;
			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIAreasHyperlink;
			HtmlHyperlink uIComprasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIComprasHyperlink;
			HtmlHyperlink uIComprasHyperlink1 = this.UIHttpwebportalbaselinWindow1.UIHttpwebportalbaselinDocument2.UIComprasHyperlink;

			#region Variable Declarations
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow1.UIHttpwebportalbaselinDocument2.UIItemPane;

			HtmlHyperlink uICotizacionesdeCompraHyperlink = this.UIHttpwebportalbaselinWindow1.UIHttpwebportalbaselinDocument2.UICotizacionesdeCompraHyperlink;
			HtmlHyperlink uINuevaCotizacióndeComHyperlink = this.UIHttpwebportalbaselinWindow8.UIHttpwebportalbaselinDocument1.UIBodyPOPane.UINuevaCotizacióndeComHyperlink;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UISystem__ComObjectCustom.UIBuscarButton;
			HtmlEdit uITxtNameBPEdit = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UITxtNameBPEdit;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIBuscarButton;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlEdit uIReqDateEdit = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIReqDateEdit;
			HtmlLabel uIFechaRequeridaLabel = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UISystem__ComObjectCustom.UIFechaRequeridaLabel;
			HtmlButton uINuevoporListaButton = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UINuevoporListaButton;
			HtmlEdit uICodeEdit = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UICodeEdit;
			HtmlButton uIBuscarButton11 = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIBuscarButton1;
			HtmlDiv uITableListItemsPane = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UITableListItemsPane;
			HtmlDiv uIModalItemsTablePane = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIModalItemsTablePane;
			HtmlCell uIItemCell = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIAssetsdatatableListITable.UIItemCell;
			HtmlCheckBox uICkSelectItem_MasterICheckBox = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UICkSelectItem_MasterICheckBox;
			HtmlButton uIElegirButton = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIElegirButton;
			HtmlDiv uILoadingPane1 = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UILoadingPane.UILoadingPane1;
			HtmlDiv uIInformaciónPane = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UISystem__ComObjectCustom.UIInformaciónPane;
			HtmlEdit uITxt1Edit = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UITxt1Edit;
			HtmlDocument uIHttpwebportalbaselinDocument2 = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2;
			HtmlCustom uISystem__ComObjectCustom = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UISystem__ComObjectCustom;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow9.UIHttpwebportalbaselinDocument2.UIBtnOkCustom;
			#endregion


			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink);

			// Click 'Compras' link
			Mouse.Click(uIComprasHyperlink);

			// Click 'Compras' link
			Mouse.Click(uIComprasHyperlink1);

			// Click 'Cotizaciones de Compra' link
			Mouse.Click(uICotizacionesdeCompraHyperlink);

			// Click 'Nueva Cotización de Compra' link
			Mouse.Click(uINuevaCotizacióndeComHyperlink);

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton);

			// Type 'v0' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = vendorCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(28, 13));

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton, new Point(53, 18));

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton, new Point(54, 12));

			// Type 'master' in 'Code' text box
			uICodeEdit.Text = this.CreatePurchaseQuotationParams.UICodeEditText1;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton11, new Point(66, 10));

			// Click cell
			Mouse.Click(uIItemCell, new Point(11, 5));

			// Select 'ckSelectItem_Master Item' check box
			uICkSelectItem_MasterICheckBox.Checked = this.CreatePurchaseQuotationParams.UICkSelectItem_MasterICheckBoxChecked;

			// Click 'Elegir' button
			Mouse.Click(uIElegirButton, new Point(37, 15));

			// Type '500' in 'txt1' text box
			uITxt1Edit.Text = this.CreatePurchaseQuotationParams.UITxt1EditText;

			// Type '{Tab}' in 'txt1' text box
			Keyboard.SendKeys(uITxt1Edit, this.CreatePurchaseQuotationParams.UITxt1EditSendKeys, ModifierKeys.None);

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(13, 20));
		}

		public virtual CreatePurchaseQuotationParams CreatePurchaseQuotationParams
		{
			get
			{
				if ((this.mCreatePurchaseQuotationParams == null))
				{
					this.mCreatePurchaseQuotationParams = new CreatePurchaseQuotationParams();
				}
				return this.mCreatePurchaseQuotationParams;
			}
		}

		private CreatePurchaseQuotationParams mCreatePurchaseQuotationParams;

		/// <summary>
		/// CreatingSalesOpportunity - Use 'CreatingSalesOpportunityParams' to pass parameters into this method.
		/// </summary>
		public void CreatingSalesOpportunity(string bpCode)
		{
			#region Variable Declarations
			HtmlHyperlink uIWebPortalHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIWebPortalHyperlink;
			HtmlHyperlink uIConfiguracionesGenerHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIConfiguracionesGenerHyperlink;
			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIAreasHyperlink;
			HtmlHyperlink uIVentasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIVentasHyperlink;
			HtmlHyperlink uIVentasHyperlink1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIVentasHyperlink;
			HtmlHyperlink uIFacturasdeVentasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIFacturasdeVentasHyperlink;
			HtmlHyperlink uIOportunidadesHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIOportunidadesHyperlink;
			HtmlHyperlink uINuevaOportunidadHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIBodyOpportunityPane.UINuevaOportunidadHyperlink;
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIItemPane;
			HtmlHeaderCell uIPorcentajeCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITableItemsFormPane.UIItemTable.UIPorcentajeCell;
			HtmlComboBox uISlpCodeComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UISlpCodeComboBox;
			HtmlDiv uIBuscarPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMshtmlHTMLInputElemeCustom.UIBuscarPane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBuscarButton;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBuscarButton1;
			HtmlDiv uIShow102550100entriesPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIVendorModalPane.UIShow102550100entriesPane;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlButton uINuevoButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UINuevoButton;
			HtmlButton uIEliminarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIEliminarButton;
			HtmlCell uIItemCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITableItemsFormPane1.UIItemTable.UIItemCell;
			HtmlComboBox uIStep_IdComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIStep_IdComboBox;
			HtmlCell uIJuancitoSalesNingúneCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITableItemsFormPane1.UIItemTable.UIJuancitoSalesNingúneCell;
			HtmlComboBox uISlpCodeLineComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UISlpCodeLineComboBox;
			HtmlEdit uIClosePrcntEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIClosePrcntEdit;
			HtmlHyperlink uICancelarHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UICancelarHyperlink;
			HtmlTextArea uIMemoEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMemoEdit;
			HtmlHyperlink uIAgregarHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITr0Row.UIAgregarHyperlink;
			HtmlButton uISearchButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UIFormActivityCustom.UISearchButton;
			HtmlEdit uITxtCodeBPEdit1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton2 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UIBuscarButton;
			HtmlDiv uISearchBPCódigoNombrePane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UIVendorModalPane.UISearchBPCódigoNombrePane;
			HtmlButton uISeleccionarButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlComboBox uIItem1statusComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UIItem1statusComboBox;
			HtmlEdit uIDetailsEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UIDetailsEdit;
			HtmlButton uIOKButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument11.UIFormActivityCustom.UIOKButton;
			HtmlHeaderCell uIEstadoCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UITableItemsFormPane.UIItemTable.UIEstadoCell;
			HtmlComboBox uIStep_IdComboBox1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIStep_IdComboBox;
			HtmlList uIItemList = this.UIItemCustom.UIItemList;
			HtmlTextArea uIMemoEdit1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIMemoEdit;
			HtmlDiv uICreatingdocumentPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIMshtmlHTMLInputElemeCustom.UICreatingdocumentPane;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIBtnOkCustom;
			HtmlSpan uIItemPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UITr0Row.UIItemPane;
			HtmlButton uIBuscarButton3 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIBuscarButton;
			HtmlButton uIBuscarButton11 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIBuscarButton1;
			HtmlDiv uIShow102550100entriesPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIVendorModalPane.UIShow102550100entriesPane;
			HtmlHeaderCell uIItemCell1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIAssetsdatatableBPTable.UIItemCell;
			HtmlButton uISeleccionarButton2 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlCell uISeleccionarCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIAssetsdatatableBPTable.UISeleccionarCell;
			HtmlDiv uIItemPane2 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIItemPane;
			HtmlEdit uIPleaseinsertpotentiaEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIPleaseinsertpotentiaEdit;
			HtmlDiv uILoadingPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UILoadingPane.UILoadingPane1;
			HtmlButton uIOKButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument21.UIOKButton;
			HtmlButton uIBuscarButton4 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UIBuscarButton;
			HtmlDiv uIBuscarPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UICollapseOnePane.UIBuscarPane;
			HtmlDiv uIBuscarBuscarSociPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UIBuscarBuscarSociPane;
			HtmlEdit uITxtCodeVendorEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UITxtCodeVendorEdit;
			HtmlDiv uIItemPane3 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UIItemPane;
			HtmlDiv uILoadingPane11 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UILoadingPane.UILoadingPane1;
			HtmlHyperlink uIEditarHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UIBodyOpportunityPane.UIEditarHyperlink;
			HtmlButton uINuevoButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UINuevoButton;
			HtmlCustom uIMshtmlHTMLInputElemeCustom = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIMshtmlHTMLInputElemeCustom;
			HtmlEdit uIOpenDateLineEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIOpenDateLineEdit;
			HtmlComboBox uIStep_IdComboBox2 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIStep_IdComboBox;
			HtmlComboBox uISlpCodeLineComboBox1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UISlpCodeLineComboBox;
			HtmlTextArea uIMemoEdit2 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIMemoEdit;
			HtmlCustom uIBtnOkCustom1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIBtnOkCustom;
			HtmlDiv uILoadingPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UILoadingPane;
			HtmlDiv uILoadingPane12 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UILoadingPane.UILoadingPane1;
			HtmlDiv uILoadingPane111 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UILoadingPane.UILoadingPane11;
			HtmlDiv uIItemPane4 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIItemPane;
			HtmlDiv uIComentariosupdatngdoPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIMshtmlHTMLInputElemeCustom.UIComentariosupdatngdoPane;
			HtmlDiv uIComentariosupdatngdoPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UIMshtmlHTMLInputElemeCustom.UIComentariosupdatngdoPane1;
			HtmlHyperlink uICancelarHyperlink1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument22.UICancelarHyperlink;
			#endregion

			//// Click 'Web Portal' link
			//Mouse.Click(uIWebPortalHyperlink, new Point(41, 29));

			// Click 'Areas' link
			//Mouse.Click(uIAreasHyperlink, new Point(41, 27));

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink);

			// Click 'Ventas' link
			//Mouse.Click(uIVentasHyperlink1);

			// Click 'Oportunidades' link
			Mouse.Click(uIOportunidadesHyperlink);

			// Click 'Nueva Oportunidad' link
			Mouse.Click(uINuevaOportunidadHyperlink);

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton);

			// Type 'c0' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1);

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton);

			//// Click 'Nuevo' button
			//Mouse.Click(uINuevoButton, new Point(38, 14));

			//// Click 'Eliminar' button
			//Mouse.Click(uIEliminarButton, new Point(26, 6));

			// Click 'Step_Id' combo box
			Mouse.Click(uIStep_IdComboBox, new Point(44, 7));

			// Click 'Juancito Sales -Ningún empleado del depa' cell
			Mouse.Click(uIJuancitoSalesNingúneCell);

			// Select 'Pepito Sales' in 'SlpCodeLine' combo box
			uISlpCodeLineComboBox.SelectedItem = this.CreatingSalesOpportunityParams.UISlpCodeLineComboBoxSelectedItem;

			// Type '25' in 'ClosePrcnt' text box
			uIClosePrcntEdit.Text = this.CreatingSalesOpportunityParams.UIClosePrcntEditText;

			// Type 'creating sales oportunity' in 'Memo' text box
			uIMemoEdit.Text = this.CreatingSalesOpportunityParams.UIMemoEditText;

			// Click 'Agregar' link
			Mouse.Click(uIAgregarHyperlink, new Point(52, 16));

			// Click 'Search' button
			Mouse.Click(uISearchButton);

			// Type 'c' in 'txtCodeBP' text box
			uITxtCodeBPEdit1.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton2);

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton1);

			// Select 'No iniciado' in 'Item1.status' combo box
			uIItem1statusComboBox.SelectedItem = this.CreatingSalesOpportunityParams.UIItem1statusComboBoxSelectedItem;

			// Type 'llamadas por teléfono al cliente' in 'Details' text box
			uIDetailsEdit.Text = this.CreatingSalesOpportunityParams.UIDetailsEditText;

			// Click 'Ok' button
			Mouse.Click(uIOKButton, new Point(10, 21));

			// Click 'Step_Id' combo box
			Mouse.Click(uIStep_IdComboBox1, new Point(54, 10));

			// Click list box
			Mouse.Click(uIItemList, new Point(71, 6));

			// Type 'Creating document ' in 'Memo' text box
			uIMemoEdit1.Text = this.CreatingSalesOpportunityParams.UIMemoEditText1;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(30, 24));

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(18, 14));

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton3, new Point(34, 12));

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton11, new Point(25, 5));

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton2, new Point(78, 6));


			// Type '500' in 'Please insert potential amount' text box
			uIPleaseinsertpotentiaEdit.Text = this.CreatingSalesOpportunityParams.UIPleaseinsertpotentiaEditText;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(20, 7));

			// Click 'Ok' button
			Mouse.Click(uIOKButton1, new Point(22, 13));

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(20, 8));

			// Type 'c0' in 'txtCodeVendor' text box
			uITxtCodeVendorEdit.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton4, new Point(61, 26));

			// Click 'Editar' link
			Mouse.Click(uIEditarHyperlink, new Point(48, 14));

			// Click 'Nuevo' button
			Mouse.Click(uINuevoButton1, new Point(54, 14));

			// Click 'Step_Id' combo box
			Mouse.Click(uIStep_IdComboBox2, new Point(85, 11));

			// Click list box
			Mouse.Click(uIItemList, new Point(82, 12));

			// Select 'Juancito Sales' in 'SlpCodeLine' combo box
			uISlpCodeLineComboBox1.SelectedItem = this.CreatingSalesOpportunityParams.UISlpCodeLineComboBoxSelectedItem1;

			// Type 'updatng document ' in 'Memo' text box
			uIMemoEdit2.Text = this.CreatingSalesOpportunityParams.UIMemoEditText2;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom1, new Point(26, 11));

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom1, new Point(23, 18));

			// Click 'Cancelar' link
			Mouse.Click(uICancelarHyperlink1, new Point(35, 10));
		}

		public virtual CreatingSalesOpportunityParams CreatingSalesOpportunityParams
		{
			get
			{
				if ((this.mCreatingSalesOpportunityParams == null))
				{
					this.mCreatingSalesOpportunityParams = new CreatingSalesOpportunityParams();
				}
				return this.mCreatingSalesOpportunityParams;
			}
		}

		private CreatingSalesOpportunityParams mCreatingSalesOpportunityParams;

		/// <summary>
		/// AddSalesQuotations - Use 'AddSalesQuotationsParams' to pass parameters into this method.
		/// </summary>
		public void AddSalesQuotations(string bpCode)
		{
			#region Variable Declarations
			HtmlHyperlink uIVentasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument6.UIVentasHyperlink;
			HtmlHyperlink uICotizacionesdeVentaHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument6.UICotizacionesdeVentaHyperlink;
			HtmlHyperlink uIOportunidadesHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument6.UIOportunidadesHyperlink;
			HtmlDiv uIBuscarOportunidadesBPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIBuscarOportunidadesBPane;
			HtmlDiv uICodigoSNNombreSNNombPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIBodyOpportunityPane.UICodigoSNNombreSNNombPane;
			HtmlDiv uIBuscarBuscarSociPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIBuscarBuscarSociPane;
			HtmlEdit uITxtDateEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UITxtDateEdit;
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIItemPane;
			HtmlHyperlink uINuevaOportunidadHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIBodyOpportunityPane.UINuevaOportunidadHyperlink;
			HtmlHeaderCell uIMostrarBPsDocCell = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITableItemsFormPane.UIItemTable.UIMostrarBPsDocCell;
			HtmlDiv uIOportunidadNombreTipPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMshtmlHTMLInputElemeCustom.UIOportunidadNombreTipPane;
			HtmlEdit uIWtSumLocEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIWtSumLocEdit;
			HtmlDiv uIFechadecierreplanifiPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMshtmlHTMLInputElemeCustom.UIFechadecierreplanifiPane;
			HtmlDiv uIPotencialFechadecierPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMshtmlHTMLInputElemeCustom.UIPotencialFechadecierPane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBuscarButton;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBuscarButton1;
			HtmlDiv uIBuscarClienteCódigoNPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIVendorModalPane.UIBuscarClienteCódigoNPane;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIAssetsdatatableBPTable1.UISeleccionarButton;
			HtmlButton uINuevoButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UINuevoButton;
			HtmlDiv uINuevoPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMshtmlHTMLInputElemeCustom.UINuevoPane;
			HtmlButton uIEliminarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIEliminarButton;
			HtmlTextArea uIMemoEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMemoEdit;
			HtmlDiv uIProyectosComentariosPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMshtmlHTMLInputElemeCustom.UIProyectosComentariosPane;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBtnOkCustom;
			HtmlDiv uILoadingPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UILoadingPane;
			HtmlButton uIOKButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIOKButton;
			HtmlDiv uIOKPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIErrorBoxPane.UIOKPane;
			HtmlEdit uIMaxSumLocEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMaxSumLocEdit;
			#endregion

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink, new Point(38, 28));

			// Click 'Oportunidades' link
			Mouse.Click(uIOportunidadesHyperlink, new Point(95, 11));

			// Click 'Nueva Oportunidad' link
			Mouse.Click(uINuevaOportunidadHyperlink, new Point(38, 19));


			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(27, 14));

			// Type 'C0018608' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(59, 22));

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton, new Point(47, 8));

			// Click 'Nuevo' button
			Mouse.Click(uINuevoButton, new Point(35, 18));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Nuevo' pane at (1, 1)
			Mouse.Hover(uINuevoPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Eliminar' button
			Mouse.Click(uIEliminarButton, new Point(26, 20));

			// Type 'comments' in 'Memo' text box
			uIMemoEdit.Text = this.AddSalesQuotationsParams.UIMemoEditText;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(17, 20));

			// Click 'Ok' button
			Mouse.Click(uIOKButton, new Point(31, 2));

			// Type '100' in 'MaxSumLoc' text box
			uIMaxSumLocEdit.Text = this.AddSalesQuotationsParams.UIMaxSumLocEditText;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(16, 25));
		}

		public virtual AddSalesQuotationsParams AddSalesQuotationsParams
		{
			get
			{
				if ((this.mAddSalesQuotationsParams == null))
				{
					this.mAddSalesQuotationsParams = new AddSalesQuotationsParams();
				}
				return this.mAddSalesQuotationsParams;
			}
		}

		private AddSalesQuotationsParams mAddSalesQuotationsParams;

		/// <summary>
		/// CreateSalesOpportunity - Use 'CreateSalesOpportunityParams' to pass parameters into this method.
		/// </summary>
		public void CreateSalesOpportunity(string vendorCode)
		{
			#region Variable Declarations
			HtmlHyperlink uIWebPortalHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument.UIWebPortalHyperlink;
			HtmlHyperlink uIOportunidadesHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIOportunidadesHyperlink;
			HtmlHyperlink uINuevaOportunidadHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIBodyOpportunityPane.UINuevaOportunidadHyperlink;
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument31.UIItemPane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBuscarButton;
			HtmlDiv uIItemPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIVendorModalPane.UIItemPane;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBuscarButton1;
			HtmlDiv uIBuscarClienteCódigoNPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIVendorModalPane.UIBuscarClienteCódigoNPane;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlDocument uIHttpwebportalbaselinDocument42 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42;
			HtmlEdit uIMaxSumLocEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMaxSumLocEdit;
			HtmlEdit uIWtSumLocEdit1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIWtSumLocEdit1;
			HtmlComboBox uIObjTypeLineComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIObjTypeLineComboBox;
			HtmlTextArea uIMemoEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMemoEdit;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIBtnOkCustom;
			HtmlEdit uIPleaseinsertpotentiaEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIPleaseinsertpotentiaEdit;
			HtmlLabel uIFechadeCierreLabel = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UIMshtmlHTMLInputElemeCustom.UIFechadeCierreLabel;
			HtmlHyperlink uICancelarHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument42.UICancelarHyperlink;
			HtmlDiv uIBuscarPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument12.UICollapseOnePane.UIBuscarPane;
			#endregion

			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIVentasHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIVentasHyperlink;
			HtmlHyperlink uIVentasHyperlink1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIVentasHyperlink;

			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink);

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink);

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink1);

			// Click 'Oportunidades' link
			Mouse.Click(uIOportunidadesHyperlink);

			// Click 'Nueva Oportunidad' link
			Mouse.Click(uINuevaOportunidadHyperlink);

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton);

			// Type 'c' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = vendorCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(46, 9));

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton, new Point(30, 11));

			// Type '150' in 'MaxSumLoc' text box
			uIMaxSumLocEdit.Text = this.CreateSalesOpportunityParams.UIMaxSumLocEditText;

			// Type '200' in 'WtSumLoc' text box
			uIWtSumLocEdit1.Text = this.CreateSalesOpportunityParams.UIWtSumLocEdit1Text;

			// Select 'Sales Quotations' in 'ObjTypeLine' combo box
			uIObjTypeLineComboBox.SelectedItem = this.CreateSalesOpportunityParams.UIObjTypeLineComboBoxSelectedItem;

			// Type 'creating sales opportunity' in 'Memo' text box
			uIMemoEdit.Text = this.CreateSalesOpportunityParams.UIMemoEditText;

			// Type '500' in 'Please insert potential amount' text box
			uIPleaseinsertpotentiaEdit.Text = this.CreateSalesOpportunityParams.UIPleaseinsertpotentiaEditText;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(2, 27));

			// Click 'Buscar' pane
			Mouse.Click(uIBuscarPane, new Point(98, 25));
		}

		public virtual CreateSalesOpportunityParams CreateSalesOpportunityParams
		{
			get
			{
				if ((this.mCreateSalesOpportunityParams == null))
				{
					this.mCreateSalesOpportunityParams = new CreateSalesOpportunityParams();
				}
				return this.mCreateSalesOpportunityParams;
			}
		}

		private CreateSalesOpportunityParams mCreateSalesOpportunityParams;

		/// <summary>
		/// CreatePurchaseOrderFromPQ - Use 'CreatePurchaseOrderFromPQParams' to pass parameters into this method.
		/// </summary>
		public void CreatePurchaseOrderFromPQ(string bpCode)
		{
			#region Variable Declarations
			HtmlHyperlink uINuevaOrdendeCompraHyperlink = this.UIHttpwebportalbaselinWindow2.UIHttpwebportalbaselinDocument3.UIBodyPOPane.UINuevaOrdendeCompraHyperlink;
			HtmlDiv uILocalCurrencySystemCPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UILocalCurrencySystemCPane;
			HtmlDiv uINroRefSociodeNegocioPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UINroRefSociodeNegocioPane;
			HtmlDiv uIBuscarDocumentoNroPePane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIBuscarDocumentoNroPePane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIBuscarButton;
			HtmlDiv uIItemPane1 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIItemPane1;
			HtmlDiv uIBuscarVendedorPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIVendorModalPane.UIBuscarVendedorPane;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIBuscarButton;
			HtmlDiv uIShow102550100entriesPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIVendorModalPane.UIShow102550100entriesPane;
			HtmlCell uIVendedorCell = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIAssetsdatatableBPTable1.UIVendedorCell;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIAssetsdatatableBPTable1.UISeleccionarButton;
			HtmlCustom uIItemCustom = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIAssetsdatatableBP_wrPane.UIItemCustom;
			HtmlEdit uITaxDateEdit = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UITaxDateEdit;
			HtmlButton uICopiarDesdeButton = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICopiarDesdeButton;
			HtmlCustom uIItemCustom1 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UIItemCustom;
			HtmlDiv uIItemPane11 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIItemPane11;
			HtmlDiv uIBuscarShow1Pane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICopyFromModalPane.UIBuscarShow1Pane;
			HtmlCell uINodataavailableintabCell = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIAssetsdatatableListPTable.UINodataavailableintabCell;
			HtmlDiv uIAssetsdatatableListPPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIAssetsdatatableListPPane;
			HtmlSpan uIItemPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIBuscarButton.UIItemPane;
			HtmlDiv uIBuscarVendedorCódigoPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIVendorModalPane.UIBuscarVendedorCódigoPane;
			HtmlSpan uIItemPane2 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIAssetsdatatableBPTable1.UIItemPane;
			HtmlDiv uIItemPane21 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIItemPane2;
			HtmlDiv uITotalantesdedescuentPane = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UISystem__ComObjectCustom.UITotalantesdedescuentPane;
			HtmlSpan uIItemPane3 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICopiarDesdeButton.UIItemPane;
			HtmlDiv uIItemPane31 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIItemPane3;
			HtmlDiv uIShow102550100entriesPane1 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICopyFromModalPane.UIShow102550100entriesPane;
			HtmlCheckBox uICkcopyfrom_1CheckBox = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UICkcopyfrom_1CheckBox;
			HtmlButton uIElegirButton1 = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIElegirButton1;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow3.UIHttpwebportalbaselinDocument4.UIBtnOkCustom;
			#endregion

			// Click 'Nueva Orden de Compra' link
			Mouse.Click(uINuevaOrdendeCompraHyperlink, new Point(95, 17));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Nueva Orden de Compra' link at (1, 1)
			Mouse.Hover(uINuevaOrdendeCompraHyperlink, new Point(1, 1));

			// Mouse hover 'Local Currency System Currency BP Curren' pane at (1, 1)
			Mouse.Hover(uILocalCurrencySystemCPane, new Point(1, 1));

			// Mouse hover 'Nro. Ref. Socio de Negocio' pane at (1, 1)
			Mouse.Hover(uINroRefSociodeNegocioPane, new Point(1, 1));

			// Mouse hover 'Buscar Documento Nro. Persona de' pane at (1, 1)
			Mouse.Hover(uIBuscarDocumentoNroPePane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(51, 1));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane1, new Point(1, 1));

			// Mouse hover '× Buscar Vendedor' pane at (1, 1)
			Mouse.Hover(uIBuscarVendedorPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Type 'v0000' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(62, 10));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Show 102550100 entries Previous1Next' pane at (1, 1)
			Mouse.Hover(uIShow102550100entriesPane, new Point(1, 1));

			// Mouse hover 'Vendedor' cell at (1, 1)
			Mouse.Hover(uIVendedorCell, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton, new Point(26, 8));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  custom control at (1, 1)
			Mouse.Hover(uIItemCustom, new Point(1, 1));

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane1, new Point(1, 1));

			// Mouse hover 'TaxDate' text box at (1, 1)
			Mouse.Hover(uITaxDateEdit, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Copiar Desde' button
			Mouse.Click(uICopiarDesdeButton, new Point(66, 18));

			// Click custom control
			Mouse.Click(uIItemCustom1, new Point(70, 10));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane11, new Point(1, 1));

			// Mouse hover '× Buscar Show 1' pane at (1, 1)
			Mouse.Hover(uIBuscarShow1Pane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Double-Click 'No data available in table' cell
			Mouse.DoubleClick(uINodataavailableintabCell, new Point(622, 16));

			// Type '{Escape}' in 'No data available in table' cell
			Keyboard.SendKeys(uINodataavailableintabCell, this.CreatePurchaseOrderFromPQParams.UINodataavailableintabCellSendKeys, ModifierKeys.None);

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'assets-data-table-ListPQ_wrapper' pane at (1, 1)
			Mouse.Hover(uIAssetsdatatableListPPane, new Point(1, 1));

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane11, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(67, 5));

			// Type 'v0' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = bpCode;

			// Click pane
			Mouse.Click(uIItemPane, new Point(5, 3));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover '× Buscar Vendedor Código' pane at (1, 1)
			Mouse.Hover(uIBuscarVendedorCódigoPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click pane
			Mouse.Click(uIItemPane2, new Point(8, 8));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover '× Buscar Vendedor Código' pane at (1, 1)
			Mouse.Hover(uIBuscarVendedorCódigoPane, new Point(1, 1));

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane21, new Point(1, 1));

			// Mouse hover 'Total antes de descuentos Descuento' pane at (1, 1)
			Mouse.Hover(uITotalantesdedescuentPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click pane
			Mouse.Click(uIItemPane3, new Point(2, 3));

			// Click custom control
			Mouse.Click(uIItemCustom1, new Point(69, 11));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane31, new Point(1, 1));

			// Mouse hover 'No data available in table' cell at (1, 1)
			Mouse.Hover(uINodataavailableintabCell, new Point(1, 1));

			// Mouse hover 'Show 102550100 entries Processing..' pane at (1, 1)
			Mouse.Hover(uIShow102550100entriesPane1, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Select 'ckcopyfrom_1' check box
			uICkcopyfrom_1CheckBox.Checked = this.CreatePurchaseOrderFromPQParams.UICkcopyfrom_1CheckBoxChecked;

			// Click 'Elegir' button
			Mouse.Click(uIElegirButton1, new Point(24, 14));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover '× Buscar Show 1' pane at (1, 1)
			Mouse.Hover(uIBuscarShow1Pane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(23, 22));
		}

		public virtual CreatePurchaseOrderFromPQParams CreatePurchaseOrderFromPQParams
		{
			get
			{
				if ((this.mCreatePurchaseOrderFromPQParams == null))
				{
					this.mCreatePurchaseOrderFromPQParams = new CreatePurchaseOrderFromPQParams();
				}
				return this.mCreatePurchaseOrderFromPQParams;
			}
		}

		private CreatePurchaseOrderFromPQParams mCreatePurchaseOrderFromPQParams;

		/// <summary>
		/// CreatePurchaseRequest - Use 'CreatePurchaseRequestParams' to pass parameters into this method.
		/// </summary>
		public void CreatePurchaseRequest(string vendorCode)
		{
			#region Variable Declarations
			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIAreasHyperlink;
			HtmlHyperlink uIComprasHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument1.UIComprasHyperlink;
			HtmlHyperlink uIComprasHyperlink1 = this.UIHttpwebportalbaselinWindow1.UIHttpwebportalbaselinDocument2.UIComprasHyperlink;
			HtmlHyperlink uIMisDocumentosHyperlink = this.UIHttpwebportalbaselinWindow1.UIHttpwebportalbaselinDocument2.UIMisDocumentosHyperlink;
			HtmlButton uIAgregarDocumentoButton = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UIBodyDraftPane.UIAgregarDocumentoButton;
			HtmlHyperlink uIRequisicióndeCompraHyperlink = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UIBodyDraftPane.UIRequisicióndeCompraHyperlink;
			HtmlDiv uITipoUserEmployeeSoliPane = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UISystem__ComObjectCustom.UITipoUserEmployeeSoliPane;
			HtmlComboBox uISelectDepartmentComboBox = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UISelectDepartmentComboBox;
			HtmlButton uINuevoporListaButton = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UINuevoporListaButton;
			HtmlEdit uINameEdit = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UINameEdit;
			HtmlEdit uICodeEdit = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UICodeEdit;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UIBuscarButton;
			HtmlDiv uIModalItemsTablePane = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UIModalItemsTablePane;
			HtmlCheckBox uICkSelectItem_MasterICheckBox = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UICkSelectItem_MasterICheckBox;
			HtmlCell uIItemCell = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UIAssetsdatatableListITable.UIItemCell;
			HtmlButton uIElegirButton = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UIElegirButton;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow5.UIHttpwebportalbaselinDocument3.UIBtnOkCustom;
			#endregion

			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink, new Point(36, 24));

			// Click 'Compras' link
			Mouse.Click(uIComprasHyperlink, new Point(44, 15));

			// Click 'Compras' link
			Mouse.Click(uIComprasHyperlink1, new Point(34, 28));

			// Click 'Mis Documentos' link
			Mouse.Click(uIMisDocumentosHyperlink, new Point(107, 9));

			// Click 'Agregar Documento' button
			Mouse.Click(uIAgregarDocumentoButton, new Point(120, 19));

			// Click 'Requisición de Compra' link
			Mouse.Click(uIRequisicióndeCompraHyperlink, new Point(77, 11));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Requisición de Compra' link at (1, 1)
			Mouse.Hover(uIRequisicióndeCompraHyperlink, new Point(1, 1));

			// Mouse hover 'Tipo User Employee Solicitud' pane at (1, 1)
			Mouse.Hover(uITipoUserEmployeeSoliPane, new Point(1, 1));

			// Mouse hover 'selectDepartment' combo box at (1, 1)
			Mouse.Hover(uISelectDepartmentComboBox, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton, new Point(44, 17));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Name' text box at (1, 1)
			Mouse.Hover(uINameEdit, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Type 'master' in 'Code' text box
			uICodeEdit.Text = this.CreatePurchaseRequestParams.UICodeEditText;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(50, 14));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'ModalItemsTable' pane at (1, 1)
			Mouse.Hover(uIModalItemsTablePane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Select 'ckSelectItem_Master Item' check box
			uICkSelectItem_MasterICheckBox.Checked = this.CreatePurchaseRequestParams.UICkSelectItem_MasterICheckBoxChecked;

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  cell at (1, 1)
			Mouse.Hover(uIItemCell, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Elegir' button
			Mouse.Click(uIElegirButton, new Point(15, 7));

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(19, 15));
		}

		public virtual CreatePurchaseRequestParams CreatePurchaseRequestParams
		{
			get
			{
				if ((this.mCreatePurchaseRequestParams == null))
				{
					this.mCreatePurchaseRequestParams = new CreatePurchaseRequestParams();
				}
				return this.mCreatePurchaseRequestParams;
			}
		}

		private CreatePurchaseRequestParams mCreatePurchaseRequestParams;

		/// <summary>
		/// CreateSalesOrderFromPR - Use 'CreateSalesOrderFromPRParams' to pass parameters into this method.
		/// </summary>
		public void CreateSalesOrderFromPR(string bpCode)
		{
			#region Variable Declarations
			HtmlEdit uITxtCodeVendorEdit = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UITxtCodeVendorEdit;
			HtmlEdit uITxtDocDateFromEdit = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UITxtDocDateFromEdit;
			HtmlEdit uITxtDocDateToEdit = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UITxtDocDateToEdit;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UIBuscarButton;
			HtmlDiv uIBuscarPane = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UICollapseOnePane.UIBuscarPane;
			HtmlHyperlink uIVerHyperlink = this.UIHttpwebportalbaselinWindow4.UIHttpwebportalbaselinDocument2.UIDatatableMyDocumentPTable.UIVerHyperlink;
			HtmlHeaderCell uIDescripciónCell = this.UIHttpwebportalbaselinWindow6.UIHttpwebportalbaselinDocument1.UITableItemsFormPane.UIItemTable.UIDescripciónCell;
			HtmlButton uICopiarAButton = this.UIHttpwebportalbaselinWindow6.UIHttpwebportalbaselinDocument1.UISystem__ComObjectCustom.UICopiarAButton;
			HtmlHyperlink uIOrdendeCompraHyperlink = this.UIHttpwebportalbaselinWindow6.UIHttpwebportalbaselinDocument1.UISystem__ComObjectCustom.UIOrdendeCompraHyperlink;
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow6.UIHttpwebportalbaselinDocument1.UISystem__ComObjectCustom.UIItemPane;
			HtmlEdit uIFreeTxt1Edit = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIFreeTxt1Edit;
			HtmlCustom uISystem__ComObjectCustom = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UISystem__ComObjectCustom;
			HtmlEdit uITxt1Edit = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UITxt1Edit;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIBtnOkCustom;
			HtmlDiv uIOKPane = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIErrorBoxPane.UIOKPane;
			HtmlButton uIOKButton = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIOKButton;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UISystem__ComObjectCustom.UIBuscarButton;
			HtmlDiv uIItemPane1 = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIItemPane;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton2 = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIBuscarButton;
			HtmlDiv uIShow102550100entriesPane = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIVendorModalPane.UIShow102550100entriesPane;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UIAssetsdatatableBPTable.UISeleccionarButton;
			HtmlDiv uIVendedorBuscarDocumePane = this.UIHttpwebportalbaselinWindow7.UIHttpwebportalbaselinDocument2.UISystem__ComObjectCustom.UIVendedorBuscarDocumePane;
			#endregion

			HtmlHyperlink uIAreasHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIVentasHyperlink = this.UIHttpwebportalbaselinWindow10.UIHttpwebportalbaselinDocument.UIVentasHyperlink;
			HtmlHyperlink uIVentasHyperlink1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIVentasHyperlink;



			// Type '' in 'txtCodeVendor' text box
			uITxtCodeVendorEdit.Text = bpCode;

			// Type 'p' in 'txtDocDateFrom' text box
			uITxtDocDateFromEdit.Text = this.CreateSalesOrderFromPRParams.UITxtDocDateFromEditText;

			// Type '{Tab}' in 'txtDocDateFrom' text box
			Keyboard.SendKeys(uITxtDocDateFromEdit, this.CreateSalesOrderFromPRParams.UITxtDocDateFromEditSendKeys, ModifierKeys.None);

			// Type 'p' in 'txtDocDateTo' text box
			uITxtDocDateToEdit.Text = this.CreateSalesOrderFromPRParams.UITxtDocDateToEditText;

			// Type '{Tab}' in 'txtDocDateTo' text box
			Keyboard.SendKeys(uITxtDocDateToEdit, this.CreateSalesOrderFromPRParams.UITxtDocDateToEditSendKeys, ModifierKeys.None);

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(41, 3));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Buscar' pane at (1, 1)
			Mouse.Hover(uIBuscarPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Ver' link
			Mouse.Click(uIVerHyperlink, new Point(27, 7));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Descripción' cell at (1, 1)
			Mouse.Hover(uIDescripciónCell, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Copiar A' button
			Mouse.Click(uICopiarAButton, new Point(58, 25));

			// Click 'Orden de Compra' link
			Mouse.Click(uIOrdendeCompraHyperlink, new Point(69, 14));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane, new Point(1, 1));

			// Mouse hover 'FreeTxt1' text box at (1, 1)
			Mouse.Hover(uIFreeTxt1Edit, new Point(1, 1));

			// Mouse hover 'System.__ComObject' custom control at (1, 1)
			Mouse.Hover(uISystem__ComObjectCustom, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Type '600' in 'txt1' text box
			uITxt1Edit.Text = this.CreateSalesOrderFromPRParams.UITxt1EditText;

			// Type '{Tab}' in 'txt1' text box
			Keyboard.SendKeys(uITxt1Edit, this.CreateSalesOrderFromPRParams.UITxt1EditSendKeys, ModifierKeys.None);

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(22, 19));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Ok' pane at (1, 1)
			Mouse.Hover(uIOKPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Ok' button
			Mouse.Click(uIOKButton, new Point(28, 12));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Ok' pane at (1, 1)
			Mouse.Hover(uIOKPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(55, 5));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane1, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Type 'v0' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = bpCode;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton2, new Point(46, 5));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Show 102550100 entries Previous1Next' pane at (1, 1)
			Mouse.Hover(uIShow102550100entriesPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton, new Point(52, 6));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Vendedor Buscar Docume' pane at (1, 1)
			Mouse.Hover(uIVendedorBuscarDocumePane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(18, 16));
		}

		public virtual CreateSalesOrderFromPRParams CreateSalesOrderFromPRParams
		{
			get
			{
				if ((this.mCreateSalesOrderFromPRParams == null))
				{
					this.mCreateSalesOrderFromPRParams = new CreateSalesOrderFromPRParams();
				}
				return this.mCreateSalesOrderFromPRParams;
			}
		}

		private CreateSalesOrderFromPRParams mCreateSalesOrderFromPRParams;

		/// <summary>
		/// CreateUpdateSalesQuotation - Use 'CreateUpdateSalesQuotationParams' to pass parameters into this method.
		/// </summary>
		public void CreateUpdateSalesQuotation()
		{
			#region Variable Declarations
			HtmlHyperlink uIAreasHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIAreasHyperlink;
			HtmlHyperlink uIPLMHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIPLMHyperlink;
			HtmlHyperlink uIVentasHyperlink = this.UIHotmailOutlookSkypenWindow.UIHttpwebportalbaselinDocument.UIVentasHyperlink;
			HtmlHyperlink uIVentasHyperlink1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UIVentasHyperlink;
			HtmlHyperlink uICotizacionesdeVentaHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument2.UICotizacionesdeVentaHyperlink;
			HtmlDiv uIBuscarBuscarClPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument3.UIBuscarBuscarClPane;
			HtmlHyperlink uINuevaCotizacióndeVenHyperlink = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument3.UIBodyPOPane.UINuevaCotizacióndeVenHyperlink;
			HtmlDiv uIItemsSelectPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIItemsSelectPane;
			HtmlEdit uINumAtCardEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UINumAtCardEdit;
			HtmlDiv uIBuscarPane = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UISystem__ComObjectCustom.UIBuscarPane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UISystem__ComObjectCustom.UIBuscarButton;
			HtmlEdit uITxtCodeBPEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UITxtCodeBPEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIBuscarButton;
			HtmlButton uISeleccionarButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIAssetsdatatableBPTable2.UISeleccionarButton;
			HtmlComboBox uIItem1PeyMethodComboBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIItem1PeyMethodComboBox;
			HtmlButton uINuevoporListaButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UINuevoporListaButton;
			HtmlEdit uICodeEdit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UICodeEdit;
			HtmlButton uIBuscarButton11 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIBuscarButton1;
			HtmlCheckBox uICkSelectItem_ItemMasCheckBox = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UICkSelectItem_ItemMasCheckBox;
			HtmlButton uIElegirButton = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIElegirButton;
			HtmlDiv uILoadingPane1 = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UILoadingPane.UILoadingPane1;
			HtmlEdit uITxtcount0Edit = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UITxtcount0Edit;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow.UIHttpwebportalbaselinDocument41.UIBtnOkCustom;
			HtmlEdit uITxtDateEdit = this.UIHttpwebportalbaselinWindow14.UIHttpwebportalbaselinDocument4.UITxtDateEdit;
			HtmlComboBox uITxtDocStatusComboBox = this.UIHttpwebportalbaselinWindow14.UIHttpwebportalbaselinDocument4.UITxtDocStatusComboBox;
			HtmlButton uIBuscarButton2 = this.UIHttpwebportalbaselinWindow14.UIHttpwebportalbaselinDocument4.UIBuscarButton;
			HtmlDiv uIBodyPOPane = this.UIHttpwebportalbaselinWindow14.UIHttpwebportalbaselinDocument4.UIBodyPOPane;
			HtmlHyperlink uIEditarHyperlink = this.UIHttpwebportalbaselinWindow14.UIHttpwebportalbaselinDocument4.UIAssetsdatatableTable.UIEditarHyperlink;
			HtmlDiv uINuevoporListaToggleDPane = this.UIHttpwebportalbaselinWindow15.UIHttpwebportalbaselinDocument5.UISystem__ComObjectCustom.UINuevoporListaToggleDPane;
			HtmlCell uIItemCell = this.UIHttpwebportalbaselinWindow15.UIHttpwebportalbaselinDocument5.UITableItemsFormPane.UIItemTable.UIItemCell;
			HtmlCell uIItemCell1 = this.UIHttpwebportalbaselinWindow15.UIHttpwebportalbaselinDocument5.UITableItemsFormPane.UIItemTable.UIItemCell1;
			HtmlHeaderCell uIItemCell2 = this.UIHttpwebportalbaselinWindow15.UIHttpwebportalbaselinDocument5.UITableItemsFormPane.UIItemTable.UIItemCell2;
			HtmlEdit uINumAtCardEdit1 = this.UIHttpwebportalbaselinWindow15.UIHttpwebportalbaselinDocument5.UINumAtCardEdit;
			HtmlCustom uIBtnOkCustom1 = this.UIHttpwebportalbaselinWindow15.UIHttpwebportalbaselinDocument5.UIBtnOkCustom;
			#endregion

			// Click 'Areas' link
			Mouse.Click(uIAreasHyperlink, new Point(39, 26));

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink, new Point(46, 9));

			// Click 'Ventas' link
			Mouse.Click(uIVentasHyperlink1, new Point(54, 17));

			// Click 'Cotizaciones de Venta' link
			Mouse.Click(uICotizacionesdeVentaHyperlink, new Point(111, 12));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Buscar Buscar Cl' pane at (1, 1)
			Mouse.Hover(uIBuscarBuscarClPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Nueva Cotización de Venta' link
			Mouse.Click(uINuevaCotizacióndeVenHyperlink, new Point(85, 22));

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(63, 8));

			// Type 'C' in 'txtCodeBP' text box
			uITxtCodeBPEdit.Text = this.CreateUpdateSalesQuotationParams.UITxtCodeBPEditText;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(38, 5));

			// Click 'Seleccionar' button
			Mouse.Click(uISeleccionarButton, new Point(31, 18));

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton, new Point(61, 15));

			// Type 'item' in 'Code' text box
			uICodeEdit.Text = this.CreateUpdateSalesQuotationParams.UICodeEditText;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton11, new Point(66, 11));

            // Select 'ckSelectItem_Item Master' check box
            uICkSelectItem_ItemMasCheckBox.Checked = this.CreateUpdateSalesQuotationParams.UICkSelectItem_ItemMasCheckBoxChecked;

            // Click 'Elegir' button
            Mouse.Click(uIElegirButton, new Point(39, 16));

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(29, 18));

			// Type 'p' in 'txtDate' text box
			uITxtDateEdit.Text = this.CreateUpdateSalesQuotationParams.UITxtDateEditText;

			// Type '{Tab}' in 'txtDate' text box
			Keyboard.SendKeys(uITxtDateEdit, this.CreateUpdateSalesQuotationParams.UITxtDateEditSendKeys, ModifierKeys.None);

			// Select 'Abierta' in 'txtDocStatus' combo box
			uITxtDocStatusComboBox.SelectedItem = this.CreateUpdateSalesQuotationParams.UITxtDocStatusComboBoxSelectedItem;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton2, new Point(57, 37));

			// Click 'Editar' link
			Mouse.Click(uIEditarHyperlink, new Point(17, 10));

			// Type '1234' in 'NumAtCard' text box
			uINumAtCardEdit1.Text = this.CreateUpdateSalesQuotationParams.UINumAtCardEditText;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom1, new Point(27, 18));
		}

		public virtual CreateUpdateSalesQuotationParams CreateUpdateSalesQuotationParams
		{
			get
			{
				if ((this.mCreateUpdateSalesQuotationParams == null))
				{
					this.mCreateUpdateSalesQuotationParams = new CreateUpdateSalesQuotationParams();
				}
				return this.mCreateUpdateSalesQuotationParams;
			}
		}

		private CreateUpdateSalesQuotationParams mCreateUpdateSalesQuotationParams;

		/// <summary>
		/// RecordedMethod6 - Use 'RecordedMethod6Params' to pass parameters into this method.
		/// </summary>
		public void UpdateSalesOpportunity()
		{
			#region Variable Declarations
			HtmlEdit uITxtDateEdit = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UITxtDateEdit;
			HtmlComboBox uITxtDocStatusComboBox = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UITxtDocStatusComboBox;
			HtmlDiv uIBuscarPane = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UICollapseOnePane.UIBuscarPane;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UIBuscarButton;
			HtmlHyperlink uIEditarHyperlink = this.UIHttpwebportalbaselinWindow12.UIHttpwebportalbaselinDocument.UIAssetsdatatableTable.UIEditarHyperlink;
			HtmlDiv uIImpuestosPane = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UISystem__ComObjectCustom.UIImpuestosPane;
			HtmlHeaderCell uIDescripciónCell = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UITableItemsFormPane.UIItemTable.UIDescripciónCell;
			HtmlButton uINuevoporListaButton = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UINuevoporListaButton;
			HtmlEdit uICodeEdit = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UICodeEdit;
			HtmlButton uIBuscarButton1 = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UIBuscarButton;
			HtmlCheckBox uICkSelectItem_ItemMasCheckBox = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UICkSelectItem_ItemMasCheckBox;
			HtmlDiv uIBuscarSearchByCataloPane = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UIMyModalPane.UIBuscarSearchByCataloPane;
			HtmlButton uIElegirButton = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UIElegirButton;
			HtmlDiv uIItemPane = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UIItemPane;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow13.UIHttpwebportalbaselinDocument1.UIBtnOkCustom;
			#endregion

			// Type 'p' in 'txtDate' text box
			uITxtDateEdit.Text = this.RecordedMethod6Params.UITxtDateEditText;

			// Type '{Tab}' in 'txtDate' text box
			Keyboard.SendKeys(uITxtDateEdit, this.RecordedMethod6Params.UITxtDateEditSendKeys, ModifierKeys.None);

			// Select 'Abierta' in 'txtDocStatus' combo box
			uITxtDocStatusComboBox.SelectedItem = this.RecordedMethod6Params.UITxtDocStatusComboBoxSelectedItem;

			// Click 'Buscar' pane
			Mouse.Click(uIBuscarPane, new Point(164, 12));

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(45, 13));

			// Click 'Editar' link
			Mouse.Click(uIEditarHyperlink, new Point(45, 24));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Editar' link at (1, 1)
			Mouse.Hover(uIEditarHyperlink, new Point(1, 1));

			// Mouse hover 'Impuestos' pane at (1, 1)
			Mouse.Hover(uIImpuestosPane, new Point(1, 1));

			// Mouse hover 'Descripción' cell at (1, 1)
			Mouse.Hover(uIDescripciónCell, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Nuevo por Lista' button
			Mouse.Click(uINuevoporListaButton, new Point(61, 20));

			// Type 'master' in 'Code' text box
			uICodeEdit.Text = this.RecordedMethod6Params.UICodeEditText;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton1, new Point(44, 17));

			// Select 'ckSelectItem_Item Master' check box
			uICkSelectItem_ItemMasCheckBox.Checked = this.RecordedMethod6Params.UICkSelectItem_ItemMasCheckBoxChecked;

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover '× Buscar Search By Catalog' pane at (1, 1)
			Mouse.Hover(uIBuscarSearchByCataloPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'Elegir' button
			Mouse.Click(uIElegirButton, new Point(32, 3));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover  pane at (1, 1)
			Mouse.Hover(uIItemPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(9, 9));
		}

		public virtual RecordedMethod6Params RecordedMethod6Params
		{
			get
			{
				if ((this.mRecordedMethod6Params == null))
				{
					this.mRecordedMethod6Params = new RecordedMethod6Params();
				}
				return this.mRecordedMethod6Params;
			}
		}

		private RecordedMethod6Params mRecordedMethod6Params;

		/// <summary>
		/// UpdatePurchaseQuotation - Use 'UpdatePurchaseQuotationParams' to pass parameters into this method.
		/// </summary>
		public void UpdatePurchaseQuotation()
		{
			#region Variable Declarations
			HtmlEdit uITxtDateEdit = this.UIHttpwebportalbaselinWindow16.UIHttpwebportalbaselinDocument.UITxtDateEdit;
			HtmlComboBox uITxtDocStatusComboBox = this.UIHttpwebportalbaselinWindow16.UIHttpwebportalbaselinDocument.UITxtDocStatusComboBox;
			HtmlButton uIBuscarButton = this.UIHttpwebportalbaselinWindow16.UIHttpwebportalbaselinDocument.UIBuscarButton;
			HtmlHyperlink uIEditarHyperlink = this.UIHttpwebportalbaselinWindow16.UIHttpwebportalbaselinDocument.UIAssetsdatatableTable.UIEditarHyperlink;
			HtmlComboBox uIItem1PeyMethodComboBox = this.UIHttpwebportalbaselinWindow17.UIHttpwebportalbaselinDocument1.UIItem1PeyMethodComboBox;
			HtmlDiv uINuevoporListaToggleDPane = this.UIHttpwebportalbaselinWindow17.UIHttpwebportalbaselinDocument1.UISystem__ComObjectCustom.UINuevoporListaToggleDPane;
			HtmlDiv uIInformaciónPane = this.UIHttpwebportalbaselinWindow17.UIHttpwebportalbaselinDocument1.UISystem__ComObjectCustom.UIInformaciónPane;
			HtmlEdit uINumAtCardEdit = this.UIHttpwebportalbaselinWindow17.UIHttpwebportalbaselinDocument1.UINumAtCardEdit;
			HtmlCustom uIBtnOkCustom = this.UIHttpwebportalbaselinWindow17.UIHttpwebportalbaselinDocument1.UIBtnOkCustom;
			#endregion

			// Type 'p' in 'txtDate' text box
			uITxtDateEdit.Text = this.UpdatePurchaseQuotationParams.UITxtDateEditText;

			// Type '{Tab}' in 'txtDate' text box
			Keyboard.SendKeys(uITxtDateEdit, this.UpdatePurchaseQuotationParams.UITxtDateEditSendKeys, ModifierKeys.None);

			// Select 'Abierta' in 'txtDocStatus' combo box
			uITxtDocStatusComboBox.SelectedItem = this.UpdatePurchaseQuotationParams.UITxtDocStatusComboBoxSelectedItem;

			// Click 'Buscar' button
			Mouse.Click(uIBuscarButton, new Point(53, 16));

			// Click 'Editar' link
			Mouse.Click(uIEditarHyperlink, new Point(38, 3));

			// Set flag to allow play back to continue if non-essential actions fail. (For example, if a mouse hover action fails.)
			Playback.PlaybackSettings.ContinueOnError = true;

			// Mouse hover 'Item1.PeyMethod' combo box at (1, 1)
			Mouse.Hover(uIItem1PeyMethodComboBox, new Point(1, 1));

			// Mouse hover 'Nuevo por Lista Toggle Dropdown Nuev' pane at (1, 1)
			Mouse.Hover(uINuevoporListaToggleDPane, new Point(1, 1));

			// Mouse hover 'Información' pane at (1, 1)
			Mouse.Hover(uIInformaciónPane, new Point(1, 1));

			// Reset flag to ensure that play back stops if there is an error.
			Playback.PlaybackSettings.ContinueOnError = false;

			// Type '123' in 'NumAtCard' text box
			uINumAtCardEdit.Text = this.UpdatePurchaseQuotationParams.UINumAtCardEditText;

			// Click 'btnOk' custom control
			Mouse.Click(uIBtnOkCustom, new Point(29, 22));
		}

		public virtual UpdatePurchaseQuotationParams UpdatePurchaseQuotationParams
		{
			get
			{
				if ((this.mUpdatePurchaseQuotationParams == null))
				{
					this.mUpdatePurchaseQuotationParams = new UpdatePurchaseQuotationParams();
				}
				return this.mUpdatePurchaseQuotationParams;
			}
		}

		private UpdatePurchaseQuotationParams mUpdatePurchaseQuotationParams;
	}
	/// <summary>
	/// Parameters to be passed into 'AddAndEditBusinessPartner'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class AddAndEditBusinessPartnerParams
	{

		#region Fields
		/// <summary>
		/// Go to web page 'http://webportalbaseline.com.ar/' using new browser instance
		/// </summary>
		public string UIHotmailOutlookSkypenWindowUrl = "http://webportalbaseline.com.ar/";

		/// <summary>
		/// Type 'demo' in 'username' text box
		/// </summary>
		public string UIUsernameEditText = "demo";

		/// <summary>
		/// Type '{Tab}' in 'username' text box
		/// </summary>
		public string UIUsernameEditSendKeys = "{Tab}";

		/// <summary>
		/// Type '********' in 'password' text box
		/// </summary>
		public string UIPasswordEditPassword = "A2RcPsxsl5ATiwB7aYvPzVij3YOpp1Qx";

		/// <summary>
		/// Type '********' in 'password' text box
		/// </summary>
		public string UIPasswordEditPassword1 = "1MwQdUUKBZEfTj/q4K2+/O55weERCdhe";

		/// <summary>
		/// Select 'ArgentinaTest' in combo box
		/// </summary>
		public string UIItemComboBoxSelectedItem = "ArgentinaTest";

		/// <summary>
		/// Restore window 'Hotmail, Outlook, Skype, noticias y videos en MSN ...'
		/// </summary>
		public bool UIHotmailOutlookSkypenWindowRestored = true;

		/// <summary>
		/// Maximize window 'Hotmail, Outlook, Skype, noticias y videos en MSN ...'
		/// </summary>
		public bool UIHotmailOutlookSkypenWindowMaximized = true;

		/// <summary>
		/// Type 'V00001' in 'CardCode' text box
		/// </summary>
		public string UICardCodeEditText = "V00001";

		/// <summary>
		/// Type '{Tab}' in 'CardCode' text box
		/// </summary>
		public string UICardCodeEditSendKeys = "{Tab}";

		/// <summary>
		/// Type 'AutomaticTest Test' in 'CardName' text box
		/// </summary>
		public string UICardNameEditText = "AutomaticTest Test";

		/// <summary>
		/// Type 'V000' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "V000";

		/// <summary>
		/// Type 'V000' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText1 = "V000";

		/// <summary>
		/// Type 'AutomaticTest.oviedo551' in 'MailAddres' text box
		/// </summary>
		public string UIMailAddresEditText = "AutomaticTest.oviedo551";

		/// <summary>
		/// Type 'Alt + {NumPad6}' in 'MailAddres' text box
		/// </summary>
		public string UIMailAddresEditSendKeys = "{NumPad6}";

		/// <summary>
		/// Type 'Alt + {NumPad4}' in 'MailAddres' text box
		/// </summary>
		public string UIMailAddresEditSendKeys1 = "{NumPad4}";

		/// <summary>
		/// Type 'AutomaticTest.oviedo551@gmail.com' in 'MailAddres' text box
		/// </summary>
		public string UIMailAddresEditText1 = "AutomaticTest.oviedo551@gmail.com";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreateSaelsOrder'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreateSaelsOrderParams
	{

		#region Fields
		/// <summary>
		/// Type 'C001020' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "C001020";

		/// <summary>
		/// Type 'master item' in 'Code' text box
		/// </summary>
		public string UICodeEditText = "master item";

		/// <summary>
		/// Select 'ckSelectItem_Master Item' check box
		/// </summary>
		public bool UICkSelectItem_MasterICheckBoxChecked = true;

		/// <summary>
		/// Type '150' in 'txt0' text box
		/// </summary>
		public string UITxt0EditText = "150";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreateSalesQuotation'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreateSalesQuotationParams
	{

		#region Fields
		/// <summary>
		/// Type 'c0' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "c0";

		/// <summary>
		/// Type 'master' in 'Code' text box
		/// </summary>
		public string UICodeEditText = "master";

		/// <summary>
		/// Select 'ckSelectItem_Master Item' check box
		/// </summary>
		public bool UICkSelectItem_MasterICheckBoxChecked = true;
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'AddVendor'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class AddVendorParams
	{

		#region Fields
		/// <summary>
		/// Type 'V000001' in 'CardCode' text box
		/// </summary>
		public string UICardCodeEditText = "V000001";

		/// <summary>
		/// Type 'AutomaticTest Vendor' in 'CardName' text box
		/// </summary>
		public string UICardNameEditText = "AutomaticTest Vendor";

		/// <summary>
		/// Select 'Vendor' in 'CardType' combo box
		/// </summary>
		public string UICardTypeComboBoxSelectedItem = "Vendor";

		/// <summary>
		/// Select 'Peso argentino' in 'Currency' combo box
		/// </summary>
		public string UICurrencyComboBoxSelectedItem = "Peso argentino";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreatePurchaseOrder'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreatePurchaseOrderParams
	{

		#region Fields
		/// <summary>
		/// Type 'V00' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "V00";

		/// <summary>
		/// Type 'item master' in 'Code' text box
		/// </summary>
		public string UICodeEditText = "item master";

		/// <summary>
		/// Type '' in 'Code' text box
		/// </summary>
		public string UICodeEditText1 = "";

		/// <summary>
		/// Select 'ckSelectItem_Master Item' check box
		/// </summary>
		public bool UICkSelectItem_MasterICheckBoxChecked = true;

		/// <summary>
		/// Type '20' in 'txtcount1' text box
		/// </summary>
		public string UITxtcount1EditText = "20";

		/// <summary>
		/// Type '{Tab}' in 'txtcount1' text box
		/// </summary>
		public string UITxtcount1EditSendKeys = "{Tab}";

		/// <summary>
		/// Select 'Peso argentino' in 'dp1' combo box
		/// </summary>
		public string UIDp1ComboBoxSelectedItem = "Peso argentino";

		/// <summary>
		/// Type '600' in 'txt1' text box
		/// </summary>
		public string UITxt1EditText = "600";

		/// <summary>
		/// Type '{Tab}' in 'txt1' text box
		/// </summary>
		public string UITxt1EditSendKeys = "{Tab}";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreatePurchaseQuotation'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreatePurchaseQuotationParams
	{

		#region Fields
		/// <summary>
		/// Type 'v0' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "v0";

		/// <summary>
		/// Type 'mster' in 'Code' text box
		/// </summary>
		public string UICodeEditText = "mster";

		/// <summary>
		/// Type 'Control + {Back}' in 'Code' text box
		/// </summary>
		public string UICodeEditSendKeys = "{Back}";

		/// <summary>
		/// Type 'master' in 'Code' text box
		/// </summary>
		public string UICodeEditText1 = "master";

		/// <summary>
		/// Select 'ckSelectItem_Master Item' check box
		/// </summary>
		public bool UICkSelectItem_MasterICheckBoxChecked = true;

		/// <summary>
		/// Type '500' in 'txt1' text box
		/// </summary>
		public string UITxt1EditText = "500";

		/// <summary>
		/// Type '{Tab}' in 'txt1' text box
		/// </summary>
		public string UITxt1EditSendKeys = "{Tab}";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreatingSalesOpportunity'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreatingSalesOpportunityParams
	{

		#region Fields
		/// <summary>
		/// Type 'c0' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "c0";

		/// <summary>
		/// Select 'Pepito Sales' in 'SlpCodeLine' combo box
		/// </summary>
		public string UISlpCodeLineComboBoxSelectedItem = "Pepito Sales";

		/// <summary>
		/// Type '25' in 'ClosePrcnt' text box
		/// </summary>
		public string UIClosePrcntEditText = "25";

		/// <summary>
		/// Type 'creating sales oportunity' in 'Memo' text box
		/// </summary>
		public string UIMemoEditText = "creating sales oportunity";

		/// <summary>
		/// Type 'c' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText1 = "c";

		/// <summary>
		/// Select 'No iniciado' in 'Item1.status' combo box
		/// </summary>
		public string UIItem1statusComboBoxSelectedItem = "No iniciado";

		/// <summary>
		/// Type 'llamadas por teléfono al cliente' in 'Details' text box
		/// </summary>
		public string UIDetailsEditText = "llamadas por teléfono al cliente";

		/// <summary>
		/// Type 'Creating document ' in 'Memo' text box
		/// </summary>
		public string UIMemoEditText1 = "Creating document ";

		/// <summary>
		/// Type '500' in 'Please insert potential amount' text box
		/// </summary>
		public string UIPleaseinsertpotentiaEditText = "500";

		/// <summary>
		/// Type 'c0' in 'txtCodeVendor' text box
		/// </summary>
		public string UITxtCodeVendorEditText = "c0";

		/// <summary>
		/// Select 'Juancito Sales' in 'SlpCodeLine' combo box
		/// </summary>
		public string UISlpCodeLineComboBoxSelectedItem1 = "Juancito Sales";

		/// <summary>
		/// Type 'updatng document ' in 'Memo' text box
		/// </summary>
		public string UIMemoEditText2 = "updatng document ";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'AddSalesQuotations'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class AddSalesQuotationsParams
	{

		#region Fields
		/// <summary>
		/// Type 'C0018608' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "C0018608";

		/// <summary>
		/// Type 'comments' in 'Memo' text box
		/// </summary>
		public string UIMemoEditText = "comments";

		/// <summary>
		/// Type '100' in 'MaxSumLoc' text box
		/// </summary>
		public string UIMaxSumLocEditText = "100";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreateSalesOpportunity'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreateSalesOpportunityParams
	{

		#region Fields
		/// <summary>
		/// Type 'c' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "c";

		/// <summary>
		/// Type '150' in 'MaxSumLoc' text box
		/// </summary>
		public string UIMaxSumLocEditText = "150";

		/// <summary>
		/// Type '200' in 'WtSumLoc' text box
		/// </summary>
		public string UIWtSumLocEdit1Text = "200";

		/// <summary>
		/// Select 'Sales Quotations' in 'ObjTypeLine' combo box
		/// </summary>
		public string UIObjTypeLineComboBoxSelectedItem = "Sales Quotations";

		/// <summary>
		/// Type 'creating sales opportunity' in 'Memo' text box
		/// </summary>
		public string UIMemoEditText = "creating sales opportunity";

		/// <summary>
		/// Type '500' in 'Please insert potential amount' text box
		/// </summary>
		public string UIPleaseinsertpotentiaEditText = "500";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreatePurchaseOrderFromPQ'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreatePurchaseOrderFromPQParams
	{

		#region Fields
		/// <summary>
		/// Type 'v0000' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "v0000";

		/// <summary>
		/// Type '{Escape}' in 'No data available in table' cell
		/// </summary>
		public string UINodataavailableintabCellSendKeys = "{Escape}";

		/// <summary>
		/// Type 'v0' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText1 = "v0";

		/// <summary>
		/// Select 'ckcopyfrom_1' check box
		/// </summary>
		public bool UICkcopyfrom_1CheckBoxChecked = true;
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreatePurchaseRequest'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreatePurchaseRequestParams
	{

		#region Fields
		/// <summary>
		/// Type 'master' in 'Code' text box
		/// </summary>
		public string UICodeEditText = "master";

		/// <summary>
		/// Select 'ckSelectItem_Master Item' check box
		/// </summary>
		public bool UICkSelectItem_MasterICheckBoxChecked = true;
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreateSalesOrderFromPR'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreateSalesOrderFromPRParams
	{

		#region Fields
		/// <summary>
		/// Type '' in 'txtCodeVendor' text box
		/// </summary>
		public string UITxtCodeVendorEditText = "";

		/// <summary>
		/// Type 'p' in 'txtDocDateFrom' text box
		/// </summary>
		public string UITxtDocDateFromEditText = "p";

		/// <summary>
		/// Type '{Tab}' in 'txtDocDateFrom' text box
		/// </summary>
		public string UITxtDocDateFromEditSendKeys = "{Tab}";

		/// <summary>
		/// Type 'p' in 'txtDocDateTo' text box
		/// </summary>
		public string UITxtDocDateToEditText = "p";

		/// <summary>
		/// Type '{Tab}' in 'txtDocDateTo' text box
		/// </summary>
		public string UITxtDocDateToEditSendKeys = "{Tab}";

		/// <summary>
		/// Type '600' in 'txt1' text box
		/// </summary>
		public string UITxt1EditText = "600";

		/// <summary>
		/// Type '{Tab}' in 'txt1' text box
		/// </summary>
		public string UITxt1EditSendKeys = "{Tab}";

		/// <summary>
		/// Type 'v0' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "v0";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'CreateUpdateSalesQuotation'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class CreateUpdateSalesQuotationParams
	{

		#region Fields
		/// <summary>
		/// Type 'C' in 'txtCodeBP' text box
		/// </summary>
		public string UITxtCodeBPEditText = "C";

		/// <summary>
		/// Type 'item' in 'Code' text box
		/// </summary>
		public string UICodeEditText = "Master Item";

		/// <summary>
		/// Select 'ckSelectItem_Item Master' check box
		/// </summary>
		public bool UICkSelectItem_ItemMasCheckBoxChecked = true;

		/// <summary>
		/// Type 'p' in 'txtDate' text box
		/// </summary>
		public string UITxtDateEditText = "p";

		/// <summary>
		/// Type '{Tab}' in 'txtDate' text box
		/// </summary>
		public string UITxtDateEditSendKeys = "{Tab}";

		/// <summary>
		/// Select 'Abierta' in 'txtDocStatus' combo box
		/// </summary>
		public string UITxtDocStatusComboBoxSelectedItem = "Abierta";

		/// <summary>
		/// Type '1234' in 'NumAtCard' text box
		/// </summary>
		public string UINumAtCardEditText = "1234";
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'RecordedMethod6'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class RecordedMethod6Params
	{

		#region Fields
		/// <summary>
		/// Type 'p' in 'txtDate' text box
		/// </summary>
		public string UITxtDateEditText = "p";

		/// <summary>
		/// Type '{Tab}' in 'txtDate' text box
		/// </summary>
		public string UITxtDateEditSendKeys = "{Tab}";

		/// <summary>
		/// Select 'Abierta' in 'txtDocStatus' combo box
		/// </summary>
		public string UITxtDocStatusComboBoxSelectedItem = "Abierta";

		/// <summary>
		/// Type 'master' in 'Code' text box
		/// </summary>
		public string UICodeEditText = "master";

		/// <summary>
		/// Select 'ckSelectItem_Item Master' check box
		/// </summary>
		public bool UICkSelectItem_ItemMasCheckBoxChecked = true;
		#endregion
	}
	/// <summary>
	/// Parameters to be passed into 'UpdatePurchaseQuotation'
	/// </summary>
	[GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
	public class UpdatePurchaseQuotationParams
	{

		#region Fields
		/// <summary>
		/// Type 'p' in 'txtDate' text box
		/// </summary>
		public string UITxtDateEditText = "p";

		/// <summary>
		/// Type '{Tab}' in 'txtDate' text box
		/// </summary>
		public string UITxtDateEditSendKeys = "{Tab}";

		/// <summary>
		/// Select 'Abierta' in 'txtDocStatus' combo box
		/// </summary>
		public string UITxtDocStatusComboBoxSelectedItem = "Abierta";

		/// <summary>
		/// Type '123' in 'NumAtCard' text box
		/// </summary>
		public string UINumAtCardEditText = "123";
		#endregion
	}
}
