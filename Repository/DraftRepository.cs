﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class DraftRepository
    {
        private WSDocument.DocumentServiceClient mDocumentService;

        public DraftRepository()
        {
            mDocumentService = new WSDocument.DocumentServiceClient();
        }

        public JsonObjectResult GetPurchaseUserDraftListSearch(CompanyConn pCc, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            using (mDocumentService)
            {

                return mDocumentService.GetPurchaseUserDraftListSearch(pCc, pUserSign, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
        }

        public JsonObjectResult GetPurchaseCustomerDraftListSearch(CompanyConn pCc, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            using (mDocumentService)
            {

                return mDocumentService.GetPurchaseCustomerDraftListSearch(pCc, pBP, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
        }

        public JsonObjectResult GetPurchaseSalesEmployeeDraftListSearch(CompanyConn pCc, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            using (mDocumentService)
            {

                return mDocumentService.GetPurchaseSalesEmployeeDraftListSearch(pCc, pSalesEmployee, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
        }

        public JsonObjectResult GetSalesUserDraftListSearch(CompanyConn pCc, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mDocumentService.GetSalesUserDraftListSearch(pCc, pUserSign, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult GetSalesCustomerDraftListSearch(CompanyConn pCc, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            using (mDocumentService)
            {

                return mDocumentService.GetSalesCustomerDraftListSearch(pCc, pBP, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
        }

        public JsonObjectResult GetSalesSalesEmployeeDraftListSearch(CompanyConn pCc, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            using (mDocumentService)
            {

                return mDocumentService.GetSalesSalesEmployeeDraftListSearch(pCc, pSalesEmployee, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
        }

        public Draft GetDraftById(int pCode, CompanyConn pCc)
        {
            using (mDocumentService)
            {
                return mDocumentService.GetDraftById(pCc, pCode);
            }

        }
        public bool CreateDocumentDIServer(CompanyConn pCc, string pSession, int DraftDocEntry)
        {
            mDocumentService = new WSDocument.DocumentServiceClient();
            
            if (mDocumentService.CreateDocumentDIServer(pCc, DraftDocEntry, pSession).Split(':')[0] == "Error")
                return false;
            else
                return true;
        }

        public List<DocumentConfirmationLines> GetApprovalListByDocumentId(CompanyConn pCc, int pDocEntry, string pObjType)
        {
            mDocumentService = new WSDocument.DocumentServiceClient();
            return mDocumentService.GetApprovalListByDocumentId(pCc, pDocEntry, pObjType).ToList();
        }

        public List<StageSAP> GetStageList(CompanyConn pCc)
        {
            mDocumentService = new WSDocument.DocumentServiceClient();
            return mDocumentService.GetStageList(pCc).ToList();
        }

    }
}
