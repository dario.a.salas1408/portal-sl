﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class GlobalDocumentRepository
    {
        private WSDocument.DocumentServiceClient mDoc;

        public GlobalDocumentRepository()
        {
            mDoc = new WSDocument.DocumentServiceClient();
        }

        public string GetLocalCurrency(CompanyConn pCompanyParam)
        {
            return mDoc.GetLocalCurrency(pCompanyParam);
        }

        public string GetSystemCurrency(CompanyConn pCompanyParam)
        {
            return mDoc.GetSystemCurrency(pCompanyParam);   
        }

        public string GetCurrencyRate(string pCurrencyType, DateTime pDate, CompanyConn pCompanyParam)
        {
            return mDoc.GetCurrencyRate(pCurrencyType, pDate, pCompanyParam);   
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyConn pCompanyParam)
        {
            return mDoc.GetTaxCode(pCompanyParam).ToList();
        }
    }
}
