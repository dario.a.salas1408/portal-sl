﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Repository
{
    public class ColorRepository
    {

        private PDMDocument.MasterDataServiceClient mMasterData;

        public ColorRepository()
        {
            mMasterData = new PDMDocument.MasterDataServiceClient();
        }

        public ARGNSColor GetColorByCode(CompanyConn pCc, string pColorCode)
        {
            try
            {
                return mMasterData.GetColorByCode(pCc, pColorCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSColor> GetColorListSearch(CompanyConn pCc, string pColorCode = "", string pColorName = "")
        {
            using (mMasterData)
            {
                return mMasterData.GetColorMasterListSearch(pCc, pColorCode, pColorName).ToList();
            }

        }

        public string Add(CompanyConn pCompanyParam, ARGNSColor pColor)
        {
            mMasterData = new PDMDocument.MasterDataServiceClient();
            string result = mMasterData.AddColor(pColor, pCompanyParam);

            if (result.Split(':')[0] == "Error")
                return Logger.WriteErrorDb(Enums.Documents.Color, pCompanyParam.IdUserConected, result.Split(':')[1], pColor.Code);
            else
                return "Ok";
        }

        public string Update(CompanyConn pCompanyParam, ARGNSColor pColor)
        {
            mMasterData = new PDMDocument.MasterDataServiceClient();
            string result = mMasterData.UpdateColor(pColor, pCompanyParam);

            if (result.Split(':')[0] == "Error")
                return Logger.WriteErrorDb(Enums.Documents.Color, pCompanyParam.IdUserConected, result.Split(':')[1], pColor.Code);
            else
                return "Ok";
        }
    }
}
