﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class WarehouseRepository
    {
        private WSDocument.DocumentServiceClient mWHS;

        public WarehouseRepository()
        {
            mWHS = new WSDocument.DocumentServiceClient();
        }

        public List<Warehouse> GetListWarehouse(CompanyConn pCc)
        {
            return mWHS.GetListWarehouse(pCc).ToList();
        }

    }
}
