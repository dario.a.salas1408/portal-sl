﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using System;
using System.Linq;

namespace ARGNS.Repository
{
    public class FolioRepository
    {
        private WebPortalModel Db;

        public FolioRepository()
        {
            Db = new WebPortalModel();
        }

        public int New(string pFolioType)
        {
            int ret = 0;
            try
            {
                var folios = Db.Folios.Where(c => c.Type == pFolioType);
                int folioNum = folios.Count() == 0 ? 0 : folios.Max(c => c.FolioNum);
                folioNum++;
                if (folioNum > 0)
                {                    
                    Folio folio = new Folio
                    {
                        FolioNum = folioNum,
                        Type = pFolioType
                    };
                    Db.Folios.Add(folio);
                    Db.SaveChanges();
                    ret = folioNum;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError(ex.Message);
            }
            return ret;
        }
    }
}
