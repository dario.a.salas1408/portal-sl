﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class RolesRepository
    {
        WebPortalModel Db;

        public RolesRepository()
        {
            Db = new WebPortalModel();
        }

        public List<Role> GetRoles()
        {
            using (Db)
            {
                return Db.Roles.Include("RolPageActions.Action").Include("RolPageActions.Page").ToList();
            }
        }

        public Role GetRol(int IdRol)
        {
            using (Db)
            {
                return Db.Roles.Include("RolPageActions.Page.Actions").Include("RolPageActions.Page.AreaKey").Include("RolPageActions.Action").Where(c => c.IdRol == IdRol).FirstOrDefault(); 
            }
        }
    }
}
