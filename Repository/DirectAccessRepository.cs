﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class DirectAccessRepository
    {
        WebPortalModel Db;

        public DirectAccessRepository()
        {
            Db = new WebPortalModel();
        }

        public List<DirectAccess> GetDirectAccessList()
        {
            return Db.DirectAccesses.ToList();
        }

        public List<UserDirectAccess> GetUserDirectAccessList(int pIdUSer, int pIdCompany)
        {
            return Db.UserDirectAccesses.Where(c => c.IdUser == pIdUSer && c.IdCompany == pIdCompany).ToList();
        }

        public List<Actions> GetActionList()
        {
            return Db.Actions.ToList();
        }

        public bool UpdateMyDirectAccesses(List<int> pListDirectAccesses, List<int> pListDirectAccessesCustom, int pIdUser, int pIdCompany)
        {
            try
            {
                List<UserDirectAccess> mUserDirectAccessList = Db.UserDirectAccesses.Where(c => c.IdUser == pIdUser && c.IdCompany == pIdCompany && c.IdTile == 0).ToList();
                //Agrego los que fueron seleccionados y no estaban
                if (pListDirectAccesses != null)
                {
                    foreach (int mIdAux in pListDirectAccesses)
                    {
                        UserDirectAccess mUserDirectAccessAux = mUserDirectAccessList.Where(c => c.IdDirectAccess == mIdAux).FirstOrDefault();
                        if (mUserDirectAccessAux != null)
                        {
                            mUserDirectAccessList.Remove(mUserDirectAccessAux);
                        }
                        else
                        {
                            UserDirectAccess mUserDirectAccessNew = new UserDirectAccess(mIdAux, pIdUser, pIdCompany, 0);
                            Db.UserDirectAccesses.Add(mUserDirectAccessNew);
                            Db.SaveChanges();
                        }
                    }
                }
                //Elimino los que estaban pero ya no fueron seleccionados
                foreach (UserDirectAccess mUserDirectAccessAux in mUserDirectAccessList)
                {
                    Db.UserDirectAccesses.Remove(mUserDirectAccessAux);
                    Db.SaveChanges();
                }

                mUserDirectAccessList = Db.UserDirectAccesses.Where(c => c.IdUser == pIdUser && c.IdCompany == pIdCompany && c.IdDirectAccess == 0).ToList();
                //Agrego los que fueron seleccionados y no estaban
                if (pListDirectAccessesCustom != null)
                {
                    foreach (int mIdAux in pListDirectAccessesCustom)
                    {
                        UserDirectAccess mUserDirectAccessAux = mUserDirectAccessList.Where(c => c.IdTile == mIdAux).FirstOrDefault();
                        if (mUserDirectAccessAux != null)
                        {
                            mUserDirectAccessList.Remove(mUserDirectAccessAux);
                        }
                        else
                        {
                            UserDirectAccess mUserDirectAccessNew = new UserDirectAccess(0, pIdUser, pIdCompany, mIdAux);
                            Db.UserDirectAccesses.Add(mUserDirectAccessNew);
                            Db.SaveChanges();
                        }
                    }
                }
                //Elimino los que estaban pero ya no fueron seleccionados
                foreach (UserDirectAccess mUserDirectAccessAux in mUserDirectAccessList)
                {
                    Db.UserDirectAccesses.Remove(mUserDirectAccessAux);
                    Db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DirectAccessManager -> UpdateMyDirectAccesses: " + ex.InnerException);
                return false;
            }
        }
    }
}
