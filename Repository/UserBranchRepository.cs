﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
    public class UserBranchRepository
    {
        WebPortalModel Db;
        private WSDocument.DocumentServiceClient branches;
        public UserBranchRepository()
        {
            Db = new WebPortalModel();
            branches = new WSDocument.DocumentServiceClient();
        }

        public UserBranch GetUserBranch(int id)
        {
            try
            {
                return Db.UserBranches.Where(c => c.IdUserBranches == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<UserBranch> GetUserBranchList(int idUser, int idCompany)
        {
            try
            {
                List<UserBranch> listreturn = null;

                listreturn = Db.UserBranches.Include("CompanyConn").Where(c => c.IdUser == idUser && c.IdCompany == idCompany).ToList();

                return listreturn;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Add(UserBranch userBranch)
        {
            try
            {
                Db.UserBranches.Add(userBranch);

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Delete(int idUserBranch)
        {
            try
            {
                var userBranch = Db.UserBranches.Where(c => c.IdUserBranches == idUserBranch).FirstOrDefault();

                Db.UserBranches.Remove(userBranch);

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<BranchesSAP> GetBranchesByUser(CompanyConn pCc, string userSAP)
        {
            try
            {
                
                if(pCc.UseCompanyUser)
                {
                    userSAP = Db.UsersSettings.Where(c => c.IdUser == pCc.IdUserConected && c.IdCompany == pCc.IdCompany).FirstOrDefault().UserCodeSAP; 
                }


                return branches.GetBranchesByUser(pCc, userSAP).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
