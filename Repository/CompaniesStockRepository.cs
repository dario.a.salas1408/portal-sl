﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class CompaniesStockRepository
    {
        WebPortalModel Db;

        public CompaniesStockRepository()
        {
            Db = new WebPortalModel();
        }

        public CompanyStock GetCompanyStock(int pIdCompany)
        {
            try
            {

                return Db.CompanyStock.Where(c => c.IdCompany == pIdCompany).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<WarehousePortal> GetWarehousePortalList(int pIdCompany)
        {
            try
            {
                return Db.WarehousesPortal.Where(c => c.IdCompany == pIdCompany).ToList(); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add(CompanyStock pCompanyStock)
        {
            try
            {
                using (Db)
                {
                    Db.CompanyStock.Add(pCompanyStock);

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool Update(CompanyStock pCompanyStock)
        {
            try
            {
                using (Db)
                {
                    CompanyStock mCompanyStock = Db.CompanyStock.Where(c => c.IdCompany == pCompanyStock.IdCompany).FirstOrDefault();

                    mCompanyStock.ValidateStock = pCompanyStock.ValidateStock;
                    mCompanyStock.Formula = pCompanyStock.Formula;

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool AddWarehousePortalList(List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                Db = new WebPortalModel();
                using (Db)
                {
                    foreach (WarehousePortal mWarehousePortalAux in pWarehousePortalList)
                    {
                        Db.WarehousesPortal.Add(mWarehousePortalAux);

                        Db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool UpdateWarehousePortalList(List<WarehousePortal> pWarehousePortalList, int pCompanyId)
        {
            try
            {
                Db = new WebPortalModel();
                using (Db)
                {
                    //Si hay mas de un warehouse en la lista controlo si debo agregar o eliminar de lo que haya en BD
                    if (pWarehousePortalList.Count > 0)
                    {
                        List<WarehousePortal> mWarehousePortalListAux = Db.WarehousesPortal.Where(c => c.IdCompany == pCompanyId).ToList();
                        foreach (WarehousePortal mWarehousePortalAux in pWarehousePortalList)
                        {
                            WarehousePortal mWarehousePortalAuxFromList = mWarehousePortalListAux.Where(c => c.WhsCode == mWarehousePortalAux.WhsCode).FirstOrDefault();
                            //Si es distinto de null es porque ya estaba en la base de datos no debo hacer nada solo borrarlo de la lista
                            if (mWarehousePortalAuxFromList != null)
                            {
                                mWarehousePortalListAux.Remove(mWarehousePortalAuxFromList);
                            }
                            //Si es null no estaba entonces debo agregarlo.
                            else
                            {
                                Db.WarehousesPortal.Add(mWarehousePortalAux);
                                Db.SaveChanges();
                                mWarehousePortalListAux.Remove(mWarehousePortalAuxFromList);
                            }
                        }
                        //Los que quedan en esta lista son los que estaban guardados en la BD pero ahora ya no fueron seleccionados por lo que debo borrarlos
                        foreach (WarehousePortal mWarehousePortalAux in mWarehousePortalListAux)
                        {
                            Db.WarehousesPortal.Remove(mWarehousePortalAux);
                            Db.SaveChanges();
                        }
                    }
                    //Si no hay ninguno en la lista simplemente controlo si debo eliminar los que estan en la BD
                    else
                    {
                        List<WarehousePortal> mWarehousePortalListAux = Db.WarehousesPortal.Where(c => c.IdCompany == pCompanyId).ToList();
                        foreach (WarehousePortal mWarehousePortalAux in mWarehousePortalListAux)
                        {
                            Db.WarehousesPortal.Remove(mWarehousePortalAux);
                            Db.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
