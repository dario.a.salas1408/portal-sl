﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class PurchaseRequestRepository
    {
        private WSDocument.DocumentServiceClient mPR;
        private WebPortalModel Db;

        public PurchaseRequestRepository()
        {
            mPR = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        public PurchaseRequestSAP GetPurchaseRequestById(int pCode, int pUserId, CompanyConn pCc)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();

            List<UDF_ARGNS> mAuxUserUDFOPRQ = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OPRQ").ToList());
            List<UDF_ARGNS> mAuxUserUDFPRQ1 = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "PRQ1").ToList());
            return mPR.GetAllbyPR(pCc, pCode, mAuxUserUDFOPRQ.ToArray(), mAuxUserUDFPRQ1.ToArray());
        }

        public JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCc, string pRequest = "", DateTime? pDate = null, int? pDocNum = null, int? pDepartment = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            return mPR.GetPurchaseRequestListSearch(pCc, pRequest, pDate, pDocNum, pDepartment, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
        }

        public List<PurchaseRequestLineSAP> GetPurchaseRequestLinesSearch(CompanyConn pCc, string[] pDocuments)
        {
            return mPR.GetPurchaseRequestLinesSearch(pCc, pDocuments).ToList();
        }

        public PurchaseRequestCombo GetPurchaseRequestCombo(CompanyConn pCc)
        {
            return mPR.GetPurchaseRequestCombo(pCc);
        }

        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            mPR = new WSDocument.DocumentServiceClient();
            using (mPR)
            {
                return mPR.GetPurchaseRequestFreights(pCc, pDocEntry).ToList();
            }
        }

        public string Add(PurchaseRequestSAP pPR, CompanyConn pCompanyParam)
        {
            using (mPR)
            {
                string mResult = mPR.AddPurchaseRequest(pPR, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                    return Logger.WriteErrorDb(Enums.Documents.PurchaseRequest, pCompanyParam.IdUserConected, mResult.Split(':')[1], pPR.DocEntry.ToString());
                else
                    return "Ok";
            }
        }

        public string Update(PurchaseRequestSAP pPR, CompanyConn pCompanyParam)
        {
            using (mPR)
            {
                string mResult = mPR.UpdatePurchaseRequest(pPR, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                    return Logger.WriteErrorDb(Enums.Documents.PurchaseRequest, pCompanyParam.IdUserConected, mResult.Split(':')[1], pPR.DocEntry.ToString());
                else
                    return "Ok";
            }
        }

    }
}
