﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class OpportunityRepository
    {

        private WSDocument.DocumentServiceClient mOpportunity;
        public OpportunityRepository()
        {
            mOpportunity = new WSDocument.DocumentServiceClient();
        }

        public OpportunitiesSAPLine GetOpportunityLine(CompanyConn pCompanyParam)
        {
            using (mOpportunity)
            {
                //return mSO.GetSaleOrderById(pCc, pCode);
                return mOpportunity.GetOpportunityLine(pCompanyParam);
            }
        }

        public OpportunityStage GetStageById(CompanyConn pCompanyParam, int pId)
        {
            using (mOpportunity)
            {
                //return mSO.GetSaleOrderById(pCc, pCode);
                return mOpportunity.GetStageById(pCompanyParam, pId);
            }
        }

        public OpportunitiesSAP GetOportunityById(CompanyConn pCompanyParam, int pId)
        {
            using (mOpportunity)
            {
                //return mSO.GetSaleOrderById(pCc, pCode);
                return mOpportunity.GetOportunityById(pCompanyParam, pId);
            }
        }


       public  string AddOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam) 
        {
            using (mOpportunity)
            {
                //return mSO.GetSaleOrderById(pCc, pCode);
                return mOpportunity.AddOportunity(pObject, pCompanyParam);
            }
        }

        
       public  string UpdateOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            using (mOpportunity)
            {
                //return mSO.GetSaleOrderById(pCc, pCode);
                return mOpportunity.UpdateOportunity(pObject, pCompanyParam);
            }
        }

       public List<OpportunitiesSAP> GetOpportunityListSearch(CompanyConn pCompanyParam, string pCodeBP = "", int? pSalesEmp = null, string pOpportunityName = "", int? pOwner = null, DateTime? pDate = null, string pStatus = "") 
       {
           using (mOpportunity)
           {
               //return mSO.GetSaleOrderById(pCc, pCode);
               return mOpportunity.GetOpportunityListSearch(pCompanyParam, pCodeBP, pSalesEmp, pOpportunityName, pOwner, pDate, pStatus).ToList();
           }
       }

    }
}
