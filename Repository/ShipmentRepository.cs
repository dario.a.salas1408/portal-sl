﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ShipmentRepository
    {
        private PDMDocument.MasterDataServiceClient mPDM;

        public ShipmentRepository()
        {
            mPDM = new PDMDocument.MasterDataServiceClient();
        }

        public List<ARGNSContainer> GetShipmentListSearch(CompanyConn pCc, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            using (mPDM)
            {
                return mPDM.GetContainerList(pCc, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA).ToList();
            }
        }

        public List<ARGNSContainer> GetMyShipmentListSearchBP(CompanyConn pCc, string pBP, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            using (mPDM)
            {
                return mPDM.GetMyShipmentListSearchBP(pCc, pBP, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA).ToList();
            }
        }
        

        public ARGNSContainer GetShipmentByCode(CompanyConn pCc, int pShipmentID)
        {
            using (mPDM)
            {
                return mPDM.GetContainerById(pCc, pShipmentID);
            }
        }

        public List<ARGNSContainerPackage> GetContainerPackageList(CompanyConn pCc, int pShipmentID)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();

            using (mPDM)
            {
                return mPDM.GetContainerPackageList(pCc, pShipmentID).ToList();
            }
        }

        public List<ARGNSContainerSetup> GetContainerSetupList(CompanyConn pCc)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();

            using (mPDM)
            {
                return mPDM.GetContainerSetupList(pCc).ToList();
            }
        }

        public string Update(CompanyConn pCc, ARGNSContainer pShipment)
        {
            using (mPDM)
            {
                return mPDM.UpdateShipment(pCc, pShipment);
            }
        }

        public string Add(CompanyConn pCc, ARGNSContainer pShipment)
        {
            using (mPDM)
            {
                return mPDM.AddShipment(pCc, pShipment);
            }
        }

        public List<ARGNSPSlipPackage> GetPSlipPackageList(CompanyConn pCc, int pContainerID, int pShipmentID)
        {
            using (mPDM)
            {
                return mPDM.GetPSlipPackageList(pCc, pContainerID, pShipmentID).ToList();
            }
        }

        public List<ARGNSPSlipPackageType> GetPSlipPackageTypeList(CompanyConn pCc)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();

            using (mPDM)
            {
                return mPDM.GetPSlipPackageTypeList(pCc).ToList();
            }
        }

        public List<Draft> GetDocumentWhitOpenLines(CompanyConn pCc, string pObjType)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();

            using (mPDM)
            {
                return mPDM.GetDocumentWhitOpenLines(pCc, pObjType).ToList();
            }
        }

        public List<DraftLine> GetDocumentOpenLines(CompanyConn pCc, string pObjType, List<int> pDocEntryList)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();

            using (mPDM)
            {
                return mPDM.GetDocumentOpenLines(pCc, pObjType, pDocEntryList.ToArray()).ToList();
            }
        }

        public List<Draft> GetShipmentDocument(CompanyConn pCc, string pShipmentID, string pObjType)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();

            using (mPDM)
            {
                return mPDM.GetShipmentDocument(pCc, pShipmentID, pObjType).ToList();
            }
        }

        public ShipmentMembers GetShipmentMembersObject(CompanyConn pCc)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();

            using (mPDM)
            {
                return mPDM.GetShipmentMembersObject(pCc);
            }
        }

    }
}
