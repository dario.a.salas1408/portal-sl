﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.MaterialDetails;
using ARGNS.Model.Implementations.View;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class MaterialDetailRepository
    {
        private PDMDocument.MasterDataServiceClient mPDM;
        private WebPortalModel Db;

        public MaterialDetailRepository()
        {
            mPDM = new PDMDocument.MasterDataServiceClient();
            Db = new WebPortalModel();
        }

        public ARGNSMaterialDetail GetMaterialDetails(CompanyConn pCc, string pModCode, int pUserId)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();

            ARGNSMaterialDetail mMaterialDetail = new ARGNSMaterialDetail();
            MaterialDetailsUDF mMaterialDetailsUDF = new MaterialDetailsUDF();

            mMaterialDetailsUDF.ListUDFFabric = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_FABRIC").ToList());
            mMaterialDetailsUDF.ListUDFAccessories = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_ACCESS").ToList());
            mMaterialDetailsUDF.ListUDFCareInstructions = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_CARE_INST").ToList());
            mMaterialDetailsUDF.ListUDFLabelling = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_LABELLING").ToList());
            mMaterialDetailsUDF.ListUDFPackaging = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_PACKAGING").ToList());
            mMaterialDetailsUDF.ListUDFFootwearMaterial = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_FOOTWEAR").ToList());
            mMaterialDetailsUDF.ListUDFFootwearDetail = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_FDETAILS").ToList());
            mMaterialDetailsUDF.ListUDFFootwearPackaging = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_FPACKAGING").ToList());
            mMaterialDetailsUDF.ListUDFFootwearPictogram = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_FPICTOGRAM").ToList());
            mMaterialDetailsUDF.ListUDFFootwearSizeRange = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_FSRANGE").ToList());
            mMaterialDetailsUDF.ListUDFFootwearAccessories = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MD_FACCESS").ToList());

            mMaterialDetail = mPDM.GetMaterialDetails(pCc, mMaterialDetailsUDF, pModCode);
            mMaterialDetail.mMaterialDetailsUDF = mMaterialDetailsUDF;

            return mMaterialDetail;

        }

        public List<ARGNSModelColor> GetColor(CompanyConn pCc, string pModCode)
        {
            return mPDM.GetColorByModel(pCc, pModCode).ToList();
        }

        public string Update(CompanyConn pCc, ARGNSMaterialDetail pMaterialDetail)
        {
            return mPDM.UpdateMaterialDetail(pCc, pMaterialDetail);
        }

        public string Add(CompanyConn pCc, ARGNSMaterialDetail pMaterialDetail)
        {
            return mPDM.AddMaterialDetail(pCc, pMaterialDetail);
        }
    }
}
