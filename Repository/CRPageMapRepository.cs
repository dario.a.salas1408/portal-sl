﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class CRPageMapRepository
    {
        WebPortalModel Db;

        public CRPageMapRepository()
        {
            Db = new WebPortalModel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdCompany"></param>
        /// <returns></returns>
        public List<CRPageMap> GetCRPageMap(int pIdCompany)
        {
            try
            {
                Db = new WebPortalModel();
                List<CRPageMap> mCRPageMap = Db.CRPageMap.Where(w => w.IdCompany == pIdCompany).ToList();
                List<Page> mPages = Db.Pages.ToList();
                mCRPageMap = mCRPageMap.Select(c => { c.Page = mPages.Where(j => j.IdPage == c.IdPage).FirstOrDefault(); return c; }).ToList();
                return mCRPageMap;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPagesAdded"></param>
        /// <returns></returns>
        public List<Page> getPagesToAdd(string[] pPagesAdded)
        {
            try
            {
                Db = new WebPortalModel();

                return Db.Pages.Where(c => c.CR == true).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listCRPageMap"></param>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public bool Update(List<CRPageMap> listCRPageMap, int IdCompany)
        {
            try
            {
                using (Db)
                {
                    //Actualizo en base de datos los registros que ya existian
                    int[] listPagesId = listCRPageMap.Select(j => j.IdICRPageMap).ToArray();
                    List<CRPageMap> listToUpdate = Db.CRPageMap.Where(c => listPagesId.Contains(c.IdICRPageMap)).ToList();
                    foreach (CRPageMap aux in listToUpdate)
                    {
                        aux.CRName = listCRPageMap.Where(c => c.IdICRPageMap == aux.IdICRPageMap).FirstOrDefault().CRName;
                    }
                    Db.SaveChanges();

                    //Agrego a la base de datos los nuevos registros
                    listPagesId = listToUpdate.Select(c => c.IdICRPageMap).ToArray();
                    List<CRPageMap> listToAdd = listCRPageMap.Where(c => !listPagesId.Contains(c.IdICRPageMap)).ToList();
                    foreach (CRPageMap aux in listToAdd)
                    {
                        Db.CRPageMap.Add(new CRPageMap { IdCompany = aux.IdCompany, IdPage = aux.IdPage, CRName = aux.CRName });
                        Db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdICRPageMap"></param>
        /// <returns></returns>
        public bool Delete(int pIdICRPageMap)
        {
            Db = new WebPortalModel();
            try
            {
                CRPageMap auxPageMap = Db.CRPageMap.Where(c => c.IdICRPageMap == pIdICRPageMap).FirstOrDefault();
                if (auxPageMap != null)
                {
                    Db.CRPageMap.Remove(auxPageMap);
                    Db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public List<string> HasAPageCRMapping(Enums.Pages page, CompanyConn pCc)
        {
            try
            {
                using (Db)
                {
                    string mKeyResource = page.ToDescriptionString();
                    Page DBPage = Db.Pages.Where(c => c.InternalKey == mKeyResource).FirstOrDefault();
                    if (DBPage != null)
                    {
                        return Db.CRPageMap.Where(c => c.IdCompany == pCc.IdCompany && c.IdPage == DBPage.IdPage).Select(c => c.CRName).ToList();
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("CRPageMapRepository -> HasAPageCRMapping:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
