﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class QuickOrderRepository
    {
        WebPortalModel Db;

        public QuickOrderRepository()
        {
            Db = new WebPortalModel();
        }

        public List<QuickOrder> GetQuickOrders(int pIdUser, int pIdCompany)
        {
            try
            {
                return Db.QuickOrder.Where(c => c.IdUser == pIdUser && c.IdCompany == pIdCompany).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public QuickOrder GetQuickOrderLine(int pIdUser, string pItemCode, int pIdCompany)
        {
            try
            {
                return Db.QuickOrder.Where(c => c.IdUser == pIdUser && c.IdCompany == pIdCompany && c.ItemCode == pItemCode).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Add(QuickOrder pQuickOrder)
        {
            try
            {

                using (Db)
                {
                    
                    Db.QuickOrder.Add(pQuickOrder);
                    Db.SaveChanges();
                    
                    return true;
                }


            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(QuickOrder pQuickOrder)
        {
            try
            {
                using (Db)
                {

                    QuickOrder mQuickOrder = Db.QuickOrder.Where(c => c.IdUser == pQuickOrder.IdUser && c.IdCompany == pQuickOrder.IdCompany && c.ItemCode == pQuickOrder.ItemCode).FirstOrDefault();

                    mQuickOrder.Quantity = pQuickOrder.Quantity;
                    mQuickOrder.WarehouseCode = pQuickOrder.WarehouseCode;
                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool Delete(QuickOrder pQuickOrder)
        {
            try
            {
                using (Db)
                {
                    var mQuickOrder = Db.QuickOrder.FirstOrDefault(c => c.IdUser == pQuickOrder.IdUser && c.IdCompany == pQuickOrder.IdCompany && c.ItemCode == pQuickOrder.ItemCode);

                    Db.QuickOrder.Remove(mQuickOrder);

                    Db.SaveChanges();
                    

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool DeleteCart(int pIdUser, int pIdCompany)
        {
            try
            {
                using (Db)
                {
                    List<QuickOrder> mQuickOrderListAux = Db.QuickOrder.Where(c => c.IdUser == pIdUser && c.IdCompany == pIdCompany).ToList();
                    foreach (QuickOrder mQuickOrder in mQuickOrderListAux)
                    {
                        Db.QuickOrder.Remove(mQuickOrder);   
                    }

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
