﻿using ARGNS.Model;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ARGNS.Repository
{
	public class InfoPortalRepository
    {
        WebPortalModel Db;

        public InfoPortalRepository()
        {
            Db = new WebPortalModel();
        }

        public System.DateTime? GetLasUpdate()
        {
            try
            {
                return Db.InfoPortals.Select(c => c.LastUpdateDB).First();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool IsDbCreated()
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(Db.Database.Connection.ConnectionString);
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;

                cmd.CommandText = "select * from Actions";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;

                sqlConnection.Open();
                reader = cmd.ExecuteReader();
                sqlConnection.Close();

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
