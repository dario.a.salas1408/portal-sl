﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class PQRepository
    {
        private WSDocument.DocumentServiceClient mPQ;
        private WebPortalModel Db;

        public PQRepository()
        {
            mPQ = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        public PurchaseQuotationSAP GetPurchaseQuotationById(int pCode, int pUserId, CompanyConn pCc)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();

            List<UDF_ARGNS> mAuxUserUDFOPQT = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OPQT").ToList());
            List<UDF_ARGNS> mAuxUserUDFPQT1 = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "PQT1").ToList());
            return mPQ.GetAllbyPQ(pCc, pCode, mAuxUserUDFOPQT.ToArray(), mAuxUserUDFPQT1.ToArray());
        }


        public JsonObjectResult GetPurchaseQuotationListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            return mPQ.GetPurchaseQuotationListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
        }

        public List<PurchaseQuotationSAPLine> GetPurchaseQuotationLinesSearch(CompanyConn pCc, string[] pDocuments)
        {
            return mPQ.GetPurchaseQuotationLinesSearch(pCc, pDocuments).ToList();
        }

        public string Add(PurchaseQuotationSAP pPQ, CompanyConn pCompanyParam)
        {
            using (mPQ)
            {
                string mResult = mPQ.AddPurchaseQuotation(pPQ, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                    return Logger.WriteErrorDb(Enums.Documents.PurchaseQuotation, pCompanyParam.IdUserConected, mResult.Split(':')[1], pPQ.DocEntry.ToString());
                else
                    return "Ok";
            }
        }


        public string Update(PurchaseQuotationSAP pPQ, CompanyConn pCompanyParam)
        {
            using (mPQ)
            {
                string mResult = mPQ.UpdatePurchaseQuotation(pPQ, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                    return Logger.WriteErrorDb(Enums.Documents.PurchaseQuotation, pCompanyParam.IdUserConected, mResult.Split(':')[1], pPQ.DocEntry.ToString());
                else
                    return "Ok";
            }
        }

        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCc)
        {
            using (mPQ)
            {
                return mPQ.GetDocumentSAPCombo(pCc);
            }
        }

        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            mPQ = new WSDocument.DocumentServiceClient();

            using (mPQ)
            {
                return mPQ.GetPurchaseQuotationFreights(pCc, pDocEntry).ToList();
            }
        }

    }
}
