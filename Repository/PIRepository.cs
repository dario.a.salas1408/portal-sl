﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class PIRepository
    {
        private WSDocument.DocumentServiceClient mPI;
        private WebPortalModel Db;

        public PIRepository()
        {
            mPI = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        public PurchaseInvoiceSAP GetPurchaseInvoiceById(int pCode, int pUserId, CompanyConn pCc)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();

            List<UDF_ARGNS> mAuxUserUDFOINV = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OINV").ToList());
            List<UDF_ARGNS> mAuxUserUDFINV1 = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "INV1").ToList());
            return mPI.GetAllbyPI(pCc, pCode, mAuxUserUDFOINV.ToArray(), mAuxUserUDFINV1.ToArray());
        }

        public JsonObjectResult GetPurchaseInvoiceListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            mPI = new WSDocument.DocumentServiceClient();

            using (mPI)
            {
                return mPI.GetPurchaseInvoiceListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
        }

        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCc)
        {
            using (mPI)
            {
                return mPI.GetDocumentSAPCombo(pCc);
            }
        }
    }
}
