﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ServiceCallRepository
    {
        private WSServiceCall.ServiceCallServiceClient mServiceCall;

        public ServiceCallRepository()
        {
            mServiceCall = new WSServiceCall.ServiceCallServiceClient();
        }


        public ServiceCallComboList GetServiceCallComboList(CompanyConn pCompanyParam)
        {
            return mServiceCall.GetServiceCallComboList(pCompanyParam);
        }
        public ServiceCallSAP GetServiceCallById(CompanyConn pCompanyParam, int pId)
        {

            return mServiceCall.GetServiceCallById(pCompanyParam, pId);

        }

        public JsonObjectResult AddServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {
            return mServiceCall.AddServiceCall(pObject, pCompanyParam);
        }

        public string UpdateServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {
            return mServiceCall.UpdateServiceCall(pObject, pCompanyParam);
        }

        public List<ServiceCallSAP> GetServiceCallListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null)
        {
            return mServiceCall.GetServiceCallListSearch(pCompanyParam, pCodeBP, pPriority, pSubject, pRefNumber, pDate, pStatus,pUser).ToList();
        }

        public List<EquipmentCardSAP> GetEquipmentCardListSearch(CompanyConn pCompanyParam, string pItemCode = "", string pItemName = "")
        {
            return mServiceCall.GetEquipmentCardListSearch(pCompanyParam, pItemCode, pItemName).ToList();
        }

        public List<ServiceStatus> GetServiceCallStatusList(CompanyConn pCompanyParam)
        {
            return mServiceCall.GetServiceCallStatusList(pCompanyParam).ToList();
        }

    }
}
