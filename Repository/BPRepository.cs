﻿using ARGNS.Model.Implementations.SAP;
using System.Collections.Generic;
using System.Linq;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using ARGNS.Model.Implementations.View;
using ARGNS.Model;
using ARGNS.Model.Implementations.Portal;
using AutoMapper;
using System.Web.Configuration;

namespace ARGNS.Repository
{
    public class BPRepository
    {
        private BusinessPartner.BPServiceClient mBP;
        private WSDocument.DocumentServiceClient mDoc;
        private WebPortalModel DbPortal;

        public BPRepository()
        {
            mBP = new BusinessPartner.BPServiceClient();
            mDoc = new WSDocument.DocumentServiceClient();
            DbPortal = new WebPortalModel();
        }

        public List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCc)
        {
            return mBP.GetBusinessPartnerList(pCc).ToList();
        }

        public List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCc, Enums.BpType pType)
        {
            return mBP.GetBusinessPartnerListByType(pCc, pType).ToList();
        }

        public BusinessPartnerSAP GetBusinessPartnerById(string pCode, CompanyConn pCc, int pUserId)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();


            List<UDF_ARGNS> mAuxUserUDFCRD1 = Mapper.Map<List<UDF_ARGNS>>(DbPortal.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "CRD1").ToList());

            List<UDF_ARGNS> mAuxUserUDFs = Mapper.Map<List<UDF_ARGNS>>(DbPortal.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OCRD").ToList());

            return mBP.GetBusinessPartnerById(pCc, pCode, mAuxUserUDFCRD1.ToArray(), mAuxUserUDFs.ToArray());
        }

        public string Add(BusinessPartnerSAP pBp, CompanyConn pCompanyParam)
        {

            var result = mBP.AddBusinessPartner(pBp, pCompanyParam);

            return result;
            
        }

        public string Update(BusinessPartnerSAP pBp, CompanyConn pCompanyParam)
        {
            var result = mBP.UpdateBusinessPartner(pBp, pCompanyParam);

            return result;
        }

        public List<BusinessPartnerSAP> GetBusinessPartnerListSearch(CompanyConn pCc, string pCodeBP = "", string pNameBP = "", string pTypeBP = "")
        {
            return mBP.GetBusinessPartnerListSearch(pCc, pCodeBP, pNameBP, pTypeBP).ToList();
        }

        public List<StateSAP> GetStateList(CompanyConn pCc, string pCountry)
        {
            return mBP.GetStateList(pCc, pCountry).ToList();
        }

        public JsonObjectResult GetCustomerAgingReport(CompanyConn pCc, List<string> mBPListToFilter)
        {
            mBP = new BusinessPartner.BPServiceClient();
            return mBP.GetCustomerAgingReport(pCc, (mBPListToFilter != null ? mBPListToFilter.ToArray() : null));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pType"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pFilterUser"></param>
        /// <param name="pSECode"></param>
        /// <param name="pBPGroupId"></param>
        /// <param name="pGetLeads"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetBusinessPartnerListByUser(CompanyConn pCc, Enums.BpType pType, bool pOnlyActiveItems, string pCodeBP = "", string pNameBP = "",
            bool pFilterUser = false, int? pSECode = null, int? pBPGroupId = null, bool pGetLeads = false, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null,
            bool pFilterByBP = false, string licTracNum = "")
        {
            List<BPGroupLine> ListBPGroupLine = DbPortal.BPGroupLines.Where(c => c.BPGroup.Id == pBPGroupId).ToList();

            var FilterBPbySE = WebConfigurationManager.AppSettings["FilterBPbySE"] == "true" ? true : false;

            pFilterUser = FilterBPbySE ? pFilterUser : false;

            return mBP.GetBusinessPartnerListByUser(pCc, pType, pOnlyActiveItems, pCodeBP, pNameBP, pFilterUser, pSECode, ListBPGroupLine.Select(j => j.CardCode).ToArray(), pGetLeads,
                pStart, pLength, pOrderColumn, pFilterByBP, licTracNum);
        }

        public BusinessPartnerSAP GetBusinessPartnerMinimalData(CompanyConn pCc, string pCardCode)
        {
            return mBP.GetBusinessPartnerMinimalData(pCc, pCardCode);
        }
    }
}
