﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class SQRepository
    {
        private WSDocument.DocumentServiceClient mSQ;
        private WebPortalModel Db;

        public SQRepository()
        {
            mSQ = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public SalesQuotationSAP GetSaleQuotationById(int pCode, int pUserId, CompanyConn pCc)
        {
            using (mSQ)
            {
                Mapper.CreateMap<UDF_ARGNS, UserUDF>();
                Mapper.CreateMap<UserUDF, UDF_ARGNS>();

                List<UDF_ARGNS> mAuxUserUDFOPQT = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OQUT").ToList());
                List<UDF_ARGNS> mAuxUserUDFPQT1 = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "QUT1").ToList());
                return mSQ.GetAllbySQ(pCc, pCode, mAuxUserUDFOPQT.ToArray(), mAuxUserUDFPQT1.ToArray());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesQuotationListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            return mSQ.GetSalesQuotationListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
        public List<SalesQuotationSAPLine> GetSalesQuotationLinesSearch(CompanyConn pCc, string[] pDocuments)
        {
            return mSQ.GetSalesQuotationLinesSearch(pCc, pDocuments).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSQ"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SalesQuotationSAP pSQ, CompanyConn pCompanyParam)
        {
             return mSQ.AddSalesQuotation(pSQ, pCompanyParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSQ"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string Update(SalesQuotationSAP pSQ, CompanyConn pCompanyParam)
        {
            using (mSQ)
            {
                string mResult = mSQ.UpdateSalesQuotation(pSQ, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                    return Logger.WriteErrorDb(Enums.Documents.SalesQuotation, pCompanyParam.IdUserConected, mResult.Split(':')[1], pSQ.DocEntry.ToString());
                else
                    return "Ok";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCc)
        {
            using (mSQ)
            {
                return mSQ.GetDocumentSAPCombo(pCc);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocEntry"></param>
        /// <returns></returns>
        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            return mSQ.GetSalesQuotationFreights(pCc, pDocEntry).ToList();
        }
    }
}
