﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ARGNS.Util.Enums;

namespace ARGNS.Repository
{
    public class SerialRepository
    {
        private WSDocument.DocumentServiceClient wsDocument;
        private WebPortalModel Db;

        public SerialRepository()
        {
            wsDocument = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        public SerialSAP GetSerialsByDocument(CompanyConn pCompanyParam, int docNum, int line, string whsCode, DocumentsType docType, string itemCode, int idUser, int anio)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();
            
            List<UDF_ARGNS> mAuxUserUDF = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == idUser && c.IdCompany == pCompanyParam.IdCompany && c.Document == "OSRN").ToList());

            var serialReturn = wsDocument.GetSerialsByDocument(pCompanyParam, docNum, line, whsCode, docType, itemCode, mAuxUserUDF.ToArray(), anio);

            return serialReturn;

        }

    }
}
