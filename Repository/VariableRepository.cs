﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class VariableRepository
    {
        private PDMDocument.MasterDataServiceClient mMasterData;

        public VariableRepository()
        {
            mMasterData = new PDMDocument.MasterDataServiceClient();
        }

        public List<ARGNSVariable> GetVariableListSearch(CompanyConn pCc, ref List<ARGNSSegmentation> pSegmentationList, string pVarCode = "", string pVarName = "", string pVarSegmentation = "")
        {
            ARGNSSegmentation[] mArraySegmentationList = pSegmentationList.ToArray();
            List<ARGNSVariable> returnList = mMasterData.GetVariableListSearch(pCc, ref mArraySegmentationList, pVarCode, pVarName, pVarSegmentation).ToList();
            pSegmentationList = mArraySegmentationList.ToList();
            return returnList;
        }

        public List<ARGNSSegmentation> GetSegmentationList(CompanyConn pCc)
        {
                mMasterData = new PDMDocument.MasterDataServiceClient();
                return mMasterData.GetSegmentationList(pCc).ToList();
        }
    }
}
