﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class CostSheetRepository
    {
        private PDMDocument.MasterDataServiceClient mCommentsPDM;
        private WebPortalModel Db;

        public CostSheetRepository()
        {
            mCommentsPDM = new PDMDocument.MasterDataServiceClient();
            Db = new WebPortalModel();
        }

        public List<ARGNSCostSheet> GetListCostSheet(CompanyConn pCc, string ItemCode)
        {
            using (mCommentsPDM)
            {
                return mCommentsPDM.GetModelCostSheet(pCc, ItemCode).ToList();
            }
        }

        public List<ARGNSCostSheet> GetCostSheetListSearch(CompanyConn pCc, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            using (mCommentsPDM)
            {
                return mCommentsPDM.GetCostSheetListSearch(pCc, txtStyle, txtCodeCostSheet, txtDescription).ToList();
            }
        }

        public ARGNSCostSheet GetListCostSheetByCode(CompanyConn pCc, string Code, string pModelCode, int pUserId)
        {
            using (mCommentsPDM)
            {
                ARGNSCostSheet mARGNSCostSheet = new ARGNSCostSheet();
                CostSheetUDF mCostSheetUDF = new CostSheetUDF();

                mCostSheetUDF.ListUDFMaterial = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_CS_MATERIALS").ToList());
                mCostSheetUDF.ListUDFOperation = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_CS_OPERATIONS").ToList());
                mCostSheetUDF.ListUDFSchema = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_CS_SCHEMAS").ToList());
                mCostSheetUDF.ListUDFPattern = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_CS_PATTERNS").ToList());

                mARGNSCostSheet =  mCommentsPDM.GetModelCostSheetByCode(pCc, Code, pModelCode);
                mARGNSCostSheet.mCostSheetUDF = mCostSheetUDF;

                return mARGNSCostSheet;
            }
        }

        public List<ARGNSSchema> GetCSSchemaList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                return mCommentsPDM.GetCSSchemaList(pCompanyParam, pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetRepository -> GetCSSchemaList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSSchemaLine> GetCSSchemaLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                return mCommentsPDM.GetCSSchemaLineList(pCompanyParam, pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetRepository -> GetCSSchemaLineList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSOperTemplate> GetCSOperationTemplateList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                return mCommentsPDM.GetCSOperationTemplateList(pCompanyParam, pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetRepository -> GetCSOperationTemplateList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSOperTemplateLine> GetCSOperationTemplateLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                return mCommentsPDM.GetCSOperationTemplateLineList(pCompanyParam, pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetRepository -> GetCSOperationTemplateLineList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSPatternTemplate> GetCSPatternList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                return mCommentsPDM.GetCSPatternList(pCompanyParam, pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetRepository -> GetCSPatternList :" + ex.Message);
                throw ex;
            }
        }

        public List<ARGNSPatternTemplateLine> GetCSPatternLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                return mCommentsPDM.GetCSPatternLineList(pCompanyParam, pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetRepository -> GetCSPatternLineList :" + ex.Message);
                throw ex;
            }
        }

        public string Update(ARGNSCostSheet pCostSheet, CompanyConn pCc)
        {
            using (mCommentsPDM)
            {
                string result = mCommentsPDM.UpdateCostSheet(pCostSheet, pCc);

                if (result.Split(':')[0] == "Error")
                    return Logger.WriteErrorDb(Enums.Documents.CostSheet, pCc.IdUserConected, result.Split(':')[1], pCostSheet.U_CSCode);
                else
                    return "Ok";

            }
        }

        public JsonObjectResult AddCostSheet(ARGNSCostSheet pCostSheet, CompanyConn pCc)
        {
            using (mCommentsPDM)
            {
                return mCommentsPDM.AddCostSheet(pCostSheet, pCc);
            }
        }
    }
}
