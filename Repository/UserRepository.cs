﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class UserRepository
    {
        WebPortalModel Db;

        public UserRepository()
        {
            Db = new WebPortalModel();
        }

        public User Signin(string pUserName, string pPsw)
        {
            try
            {

                return Db.Users.Where(c => c.Password == pPsw && c.Name.ToUpper() == pUserName.ToUpper() && c.Active == true).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonObjectResult GetListUser(int pStart, int pLength, string pUserName)
        {
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                mJsonObjectResult.UserList = Db.Users.AsNoTracking()
                    .Where(c => c.Active == true && 
                    (c.Name.Contains(pUserName) || pUserName.Equals("")))
                    .OrderBy(o=> o.IdUser)
                    .Skip(pStart).Take(pLength)
                    .ToList();

                mJsonObjectResult.Others.Add("TotalRecords",
                    Db.Users.AsNoTracking().Where(w =>w.Active == true &&
                            (w.Name.Contains(pUserName) || pUserName.Equals("")))
                            .Count().ToString());

                return mJsonObjectResult; 
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserRepository -> GetListUser: " + ex.Message);
                Logger.WriteError("UserRepository -> GetListUser: " + ex.InnerException);
                return new JsonObjectResult(); 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public User GetUser(int Id)
        {
            try
            {
                return Db.Users.Where(c => c.IdUser == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Add(User pUser)
        {
            try
            {

               
                    pUser.Active = true;
                    Security mSecurity = new Security();

                    ICollection<UserPageAction> cUserPageAction = pUser.UserPageActions;
                    ICollection<UserArea> cUserArea = pUser.UserAreas;

                    pUser.UserPageActions = null;
                    pUser.UserAreas = null;

                    Db.Users.Add(pUser);
                    Db.SaveChanges();

                    foreach (UserPageAction item in cUserPageAction)
                    {
                        item.IdUser = pUser.IdUser;
                        Db.UserPageActions.Add(item);
                        Db.SaveChanges();
                    }

                    foreach (UserArea item in cUserArea)
                    {
                        UserArea mUserAreaAdd = new UserArea { IdAreaKeys = item.AreaKey.IdAreaKeys, IdUser = pUser.IdUser, KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyUser(item.AreaKey.Area, pUser.IdUser), "Argentis") };

                        Db.UserAreas.Add(mUserAreaAdd);
                        Db.SaveChanges();
                    }

                    return true;
               
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Update(User pUser)
        {
            try
            {
                
                    Security mSecurity = new Security();
                    User mUser = Db.Users.Where(c => c.IdUser == pUser.IdUser).FirstOrDefault();

                    mUser.IdUser = pUser.IdUser;
                    mUser.Name = pUser.Name;
                    mUser.Password = pUser.Password;
                    mUser.UserIdSAP = pUser.UserIdSAP;
                    mUser.IdRol = pUser.IdRol;
                    Db.SaveChanges();

                    ICollection<UserPageAction> cUserPageAction = mUser.UserPageActions;
                    ICollection<UserArea> cUserArea = mUser.UserAreas;

                    Db.UserPageActions.RemoveRange(cUserPageAction);
                    Db.SaveChanges();

                    Db.UserAreas.RemoveRange(cUserArea);
                    Db.SaveChanges();

                    foreach (UserPageAction item in pUser.UserPageActions)
                    {
                        mUser.UserPageActions.Add(item);
                        Db.SaveChanges();
                    }

                    foreach (UserArea item in pUser.UserAreas)
                    {
                        UserArea mUserAreaAdd = new UserArea { IdAreaKeys = item.AreaKey.IdAreaKeys, IdUser = pUser.IdUser, KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyUser(item.AreaKey.Area, pUser.IdUser), "Argentis") };
                        Db.UserAreas.Add(mUserAreaAdd);
                        Db.SaveChanges();
                    }

                    return true;
        
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Delete(User pUser)
        {
            try
            {
                
                    Db.Users.Where(c => c.IdUser == pUser.IdUser)
                        .FirstOrDefault().Active = false;

                    Db.SaveChanges();

                    var id = pUser.IdUser;

                    return true;
               
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool UserAlreadyExists(string userName)
        {
            try
            {
                
                    return Db.Users.AsNoTracking().Where(w=> w.Name == userName).Any();
                
            }
            catch (Exception ex)
            { 
                Logger.WriteError("UserRepository -> ValidateExistingUser: ");
                return false; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <param name="IdCompany"></param>
        /// <param name="pUserNameSAP"></param>
        /// <param name="pUserPasswordSAP"></param>
        /// <param name="UserCodeSAP"></param>
        /// <param name="BPCode"></param>
        /// <param name="SECode"></param>
        /// <param name="DftDistRule"></param>
        /// <param name="DftWhs"></param>
        /// <param name="pSalesTaxCodeDef"></param>
        /// <param name="pPurchaseTaxCodeDef"></param>
        /// <param name="pUserGroupId"></param>
        /// <param name="pCostPriceList"></param>
        /// <param name="pBPGroupId"></param>
        /// <returns></returns>
        public bool AddCompanyToUser(int IdUser, int IdCompany, string pUserNameSAP, string pUserPasswordSAP, string UserCodeSAP, string BPCode, int? SECode, string DftDistRule, string DftWhs, string pSalesTaxCodeDef, string pPurchaseTaxCodeDef, int? pUserGroupId, int? pCostPriceList, int? pBPGroupId)
        {

            try
            {
                
                    var mUser = Db.Users.FirstOrDefault(c => c.IdUser == IdUser);

                    var mCompany = Db.Companies.FirstOrDefault(c => c.IdCompany == IdCompany);

                    Db.UsersSettings.Add(new UsersSetting { User = mUser, CompanyConn = mCompany, UserNameSAP = pUserNameSAP, UserPasswordSAP = pUserPasswordSAP, UserCodeSAP = UserCodeSAP, IdBp = BPCode, IdSE = SECode, DftDistRule = DftDistRule, DftWhs = DftWhs, SalesTaxCodeDef = pSalesTaxCodeDef, PurchaseTaxCodeDef = pPurchaseTaxCodeDef, UserGroupId = pUserGroupId, CostPriceList = pCostPriceList, BPGroupId = pBPGroupId });

                    Db.SaveChanges();

                    return true;
              
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public bool DeleteCompanyToUser(int IdUser, int IdCompany)
        {

            try
            {
                
                    var mUser = Db.Users.FirstOrDefault(c => c.IdUser == IdUser);

                    Db.UsersSettings.Remove(mUser.UsersSettings.Where(c => c.CompanyConn.IdCompany == IdCompany).FirstOrDefault());

                    Db.SaveChanges();

                    return true;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUserId"></param>
        /// <param name="pNewUserName"></param>
        /// <param name="pNewUserPassword"></param>
        /// <returns></returns>
        public bool Duplicate(int pUserId, string pNewUserName, string pNewUserPassword)
        {
            try
            {
                Security mSecurity = new Security();
                List<UserArea> mListAreaAux = new List<UserArea>();

                User originalUser = Db.Users.AsNoTracking().FirstOrDefault(c => c.IdUser == pUserId);
                //guardamos las areas del usuario viejo para poder duplicarlas luego igual para el nuevo.
                mListAreaAux = originalUser.UserAreas.ToList();

                List<UserDirectAccess> directAccess = Db.UserDirectAccesses.AsNoTracking().Where(w => w.IdUser == pUserId).ToList();

                List<UserPageAction> userPageActions = Db.UserPageActions.AsNoTracking().Where(w => w.IdUser == pUserId).ToList();

                List<UserUDF> userUDFs = Db.UserUDFs.AsNoTracking().Where(w => w.IdUser == pUserId).ToList();

                List<UsersSetting> usersSettings = Db.UsersSettings.AsNoTracking().Where(w => w.IdUser == pUserId).ToList();

                originalUser.IdUser = 0;
                originalUser.Name = pNewUserName;
                originalUser.Password = pNewUserPassword;
                //Lo ponemos en null para que no inserte las areas del usuario viejo.
                originalUser.UserAreas = null;

                Db.Users.Add(originalUser);
                Db.SaveChanges();

                foreach (var userSett in userPageActions)
                {
                    userSett.IdUser = originalUser.IdUser;
                    Db.UserPageActions.Add(userSett);
                    Db.SaveChanges();
                }

                foreach (var directAccessItem in directAccess)
                {
                    directAccessItem.IdUser = originalUser.IdUser;
                    Db.UserDirectAccesses.Add(directAccessItem);
                    Db.SaveChanges();
                }

                foreach (var udf in userUDFs)
                {
                    udf.IdUser = originalUser.IdUser;
                    Db.UserUDFs.Add(udf);
                    Db.SaveChanges();
                }

                foreach (var uset in usersSettings)
                {
                    uset.IdUser = originalUser.IdUser;
                    Db.UsersSettings.Add(uset);
                    Db.SaveChanges();
                }

                foreach (UserArea item in mListAreaAux)
                {
                    UserArea mUserAreaAdd = new UserArea { IdAreaKeys = item.AreaKey.IdAreaKeys, IdUser = originalUser.IdUser, KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyUser(item.AreaKey.Area, originalUser.IdUser), "Argentis") };

                    Db.UserAreas.Add(mUserAreaAdd);
                    Db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserRepository -> Duplicate: " + ex.Message);
                Logger.WriteError("UserRepository -> Duplicate: " + ex.InnerException);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool UpdateMyAccount(User pUser)
        {
            try
            {
                
                    User mUser = Db.Users.Where(c => c.IdUser == pUser.IdUser).FirstOrDefault();

                    mUser.Name = pUser.Name;
                    mUser.Password = pUser.Password;
                    Db.SaveChanges();

                    return true;
            
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<SAPObject> GetSAPObjects()
        {
            try
            {
                List<SAPObject> mUser = Db.SAPObjects.ToList();
                return mUser;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool AddUDF(int pUserID, int pCompanyId, string pTableID, string pUDFName, int pFieldID, string pTypeID, string pUDFDesc, string pRTable)
        {
            try
            {
                UserUDF mUDFToAdd = new UserUDF();
                mUDFToAdd.IdUser = pUserID;
                mUDFToAdd.IdCompany = pCompanyId;
                mUDFToAdd.Document = pTableID;
                mUDFToAdd.UDFName = pUDFName;
                mUDFToAdd.FieldID = (short)pFieldID;
                mUDFToAdd.TypeID = pTypeID;
                mUDFToAdd.UDFDesc = pUDFDesc;
                mUDFToAdd.RTable = pRTable;

                Db.UserUDFs.Add(mUDFToAdd);
                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool DeleteUDF(int IdUser, int IdUDF)
        {
            try
            {
                Db.UserUDFs.Remove(Db.UserUDFs.Where(c => c.IdUser == IdUser && c.IdUDF == IdUDF).FirstOrDefault());
                Db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
