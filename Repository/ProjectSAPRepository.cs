﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ProjectSAPRepository
    {
        private WSDocument.DocumentServiceClient mPO;

        public ProjectSAPRepository()
        {
            mPO = new WSDocument.DocumentServiceClient();
        }

        public List<ProjectSAP> GetProjectsSAPList(CompanyConn pCc, string pProjectCode, string pProjectName)
        {
            try
            {
                mPO = new WSDocument.DocumentServiceClient();

                return mPO.GetProjectsSAPList(pCc, pProjectCode, pProjectName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
