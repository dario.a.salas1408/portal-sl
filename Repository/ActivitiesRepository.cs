﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ActivitiesRepository
    {
        private Activities.ActivityServiceClient mActivities;

        public ActivitiesRepository()
        {
            mActivities = new Activities.ActivityServiceClient();
        }

        public List<ActivitiesSAP> GetActivitiesList(CompanyConn pCc)
        {
            return mActivities.GetActivitiesList(pCc).ToList();
        }

        public List<ActivitiesSAP> GetActivitiesListSearch(CompanyConn pCc, int UserType, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            return mActivities.GetActivitiesListSearch(pCc, UserType, pModel, pProject, txtStatus, txtRemarks).ToList();
        }

        public List<ActivityStatusSAP> GetActivityStatusList(CompanyConn pCc)
        {
            return mActivities.GetActivityStatusList(pCc).ToList();
        }

        public ActivitiesSAP GetActivityById(int pCode, CompanyConn pCc, bool CheckApparelInstallation)
        {
            return mActivities.GetActivityById(pCc, pCode, CheckApparelInstallation);
        }



        public string Add(ActivitiesSAP pActivity, CompanyConn pCc)
        {
            string mResult = mActivities.AddActivity(pActivity, pCc);

            if (mResult.Split(':')[0] == "Error")
            {
                return Logger.WriteErrorDb(Enums.Documents.Activity, pCc.IdUserConected, mResult.Split(':')[1], pActivity.ClgCode.ToString());
            }
            else
            {
                return mResult;
            }

        }

        public string Update(ActivitiesSAP pActivity, CompanyConn pCc)
        {
            string mResult = mActivities.UpdateActivity(pActivity, pCc);

            if (mResult.Split(':')[0] == "Error")
            {
                return Logger.WriteErrorDb(Enums.Documents.Activity, pCc.IdUserConected, mResult.Split(':')[1], pActivity.ClgCode.ToString());
            }
            else
                return "Ok";
        }

        public string UpdateSalesOpportunitiesActivity(int pActivityId, int? pOpporId, short? pLine, CompanyConn pCompanyParam)
        {
            string mResult = mActivities.UpdateSalesOpportunitiesActivity(pActivityId, pOpporId, pLine, pCompanyParam);

            if (mResult.Split(':')[0] == "Error")
            {
                return Logger.WriteErrorDb(Enums.Documents.Activity, pCompanyParam.IdUserConected, mResult.Split(':')[1], pActivityId.ToString());
            }
            else
                return "Ok";
        }

        public List<ActivitiesSAP> GetCRMActivitiesListSearch(CompanyConn pCc, int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode)
        {
            return mActivities.GetCRMActivitiesListSearch(pCc, UserType, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks, txtBPCode).ToList();
        }

        public ActivitiesSAPCombo GetActivityCombo(CompanyConn pCompanyParam)
        {
            return mActivities.GetActivityCombo(pCompanyParam);
        }

        public List<ActivitiesSAP> GetBPCRMActivitiesSearch(CompanyConn pCc, int UserType, string pBPCode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            return mActivities.GetBPCRMActivitiesSearch(pCc, UserType, pBPCode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks).ToList();
        }

        public List<ActivitiesSAP> GetUserCRMActivitiesSearch(CompanyConn pCc, int UserType, int pUserId, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            return mActivities.GetUserCRMActivitiesSearch(pCc, UserType, pUserId, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks).ToList();
        }

        public List<ActivitiesSAP> GetSalesEmployeeCRMActivitiesSeach(CompanyConn pCc, int UserType, int pSECode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            return mActivities.GetSalesEmployeeCRMActivitiesSeach(pCc, UserType, pSECode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks).ToList();
        }

        public List<ActivitiesSAP> GetCRMActivitiesListFromTo(CompanyConn pCc, DateTime? txtStartDate, DateTime? txtEndDate, int pUserId)
        {
            return mActivities.GetCRMActivitiesListFromTo(pCc, txtStartDate, txtEndDate, pUserId).ToList();
        }

        public List<ActivitiesSAP> GetBPMyActivitiesSearch(CompanyConn pCc, int UserType, string pBPCode, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            return mActivities.GetBPMyActivitiesSearch(pCc, UserType, pBPCode, txtModel, txtProject, txtStatus, txtRemarks).ToList();
        }

        public List<ActivitiesSAP> GetUserMyActivitiesSearch(CompanyConn pCc, int UserType, int pUserId, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            return mActivities.GetUserMyActivitiesSearch(pCc, UserType, pUserId, txtModel, txtProject, txtStatus, txtRemarks).ToList();
        }
        
    }
}
