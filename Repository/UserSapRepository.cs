﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class UserSapRepository
    {
        WebPortalModel Db;

        public UserSapRepository()
        {
            Db = new WebPortalModel();
        }

        public List<UserSAP> GetListUsers(CompanyConn pCc)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient ();
                return mUsrSap.GetUserList(pCc).ToList();  
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeList(CompanyConn pCc)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetEmployeeList(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<EmployeeSAP> GetAllEmployeeList(CompanyConn pCc)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetAllEmployee(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesEmployeeSAP> GetAllSalesEmployeeList(CompanyConn pCc)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetAllSalesEmployee(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesEmployeeSAP> GetSalesEmployeeSearchByName(CompanyConn pCc, string pSEName)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetSalesEmployeeSearchByName(pCc, pSEName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SalesTaxCodesSAP> GetSalesTaxCode(CompanyConn pCc)
        {
            try
            {
                WSDocument.DocumentServiceClient mDocSap = new WSDocument.DocumentServiceClient();
                return mDocSap.GetTaxCode(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCc)
        {
            try
            {
                BusinessPartner.BPServiceClient mBPClient = new BusinessPartner.BPServiceClient();
                return mBPClient.GetBusinessPartnerList(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UsersSetting GetCompany(int id , int idUser)
        {
            try
            {
                return Db.UsersSettings.Where(c => c.IdCompany == id && c.IdUser == idUser).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserSAP> GetUserSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetUserSAPSearchByCode(pCc, pRequesterCode, pRequesterName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserSAP GetUserSAPSearchByName(CompanyConn pCc, string pUserName)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetUserSAPSearchByName(pCc, pUserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "", short? pDepartment = null, bool pHaveUserCode = true)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetEmployeeSAPSearchByCode(pCc, pRequesterCode, pRequesterName, pDepartment, pHaveUserCode).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public EmployeeSAP GetEmployeeById(CompanyConn pCc, int? pCode)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetEmployeeById(pCc,pCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DistrRuleSAP> GetDistributionRuleList(CompanyConn pCc)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetDistributionRuleList(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Warehouse> GetWarehouseList(CompanyConn pCc)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetWarehouseList(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Warehouse> GetWarehouseListSearch(CompanyConn pCc, string pWhsCode, string pWhsName)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetWarehouseListSearch(pCc, pWhsCode, pWhsName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserUDF> GetUserUDF(int IdUser)
        {
            try
            {
                return Db.UserUDFs.Where(c => c.IdUser == IdUser).OrderBy(j => j.Document).ThenBy(j =>  j.UDFName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserUDF> GetUserUDFSearch(int pIdUser, int pIdCompany, string pDocument)
        {
            try
            {
                return Db.UserUDFs.Where(c => c.IdUser == pIdUser && c.IdCompany == pIdCompany && c.Document == pDocument).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UDF_SAP> GetUDFByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetUDFByTableID(pCompanyParam,pTableID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UFD1_SAP> GetUDF1ByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetUDF1ByTableID(pCompanyParam, pTableID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AlertSAP> GetAlertsByUser(CompanyConn pCompanyParam, int pUser)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetAlertsByUser(pCompanyParam, pUser).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AlertSAP GetAlertById(CompanyConn pCompanyParam, int pAlertId)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetAlertById(pCompanyParam, pAlertId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetQueryResult(CompanyConn pCc, string mQuery)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetQueryResult(pCc, mQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonObjectResult GetSAPQueryResult(CompanyConn pCc, string pQuery)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetSAPQueryResult(pCc, pQuery);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PriceListSAP> GetAllPriceListSAP(CompanyConn pCc)
        {
            try
            {
                UsrSap.UserServiceClient mUsrSap = new UsrSap.UserServiceClient();
                return mUsrSap.GetAllPriceListSAP(pCc).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
