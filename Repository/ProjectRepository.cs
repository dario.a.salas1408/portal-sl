﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ProjectRepository
    {
        private PDMDocument.MasterDataServiceClient mPDM;
        private Activities.ActivityServiceClient mActivityService;

        public ProjectRepository()
        {
            mPDM = new PDMDocument.MasterDataServiceClient();
            mActivityService = new Activities.ActivityServiceClient();
        }

        public ARGNSProject GetProject(CompanyConn pCc, string pProjectCode, string pModelId)
        {
            return mPDM.GetProjectByCode(pCc, pProjectCode, pModelId);
        }

        public List<ARGNSRoutingLine> GetWorkflowLines(CompanyConn pCc, string pWorkflow)
        {
            return mPDM.GetWorkflowLines(pCc, pWorkflow).ToList();
        }

        public string GetCriticalPathDefaultBP(CompanyConn pCc)
        {
            return mPDM.GetCriticalPathDefaultBP(pCc);
        }
        

        public string CreateActivities(CompanyConn pCc, List<ActivitiesSAP> pListActivities)
        {
            try
            {
                Dictionary<string, string> mActivityCodeList = new Dictionary<string, string>();

                foreach (ActivitiesSAP mActivity in pListActivities)
                {
                    string mResult = mActivityService.AddActivity(mActivity, pCc);
                    if (mResult.Split(':')[0] == "Error")
                    {
                        return Logger.WriteErrorDb(Enums.Documents.Activity, pCc.IdUserConected, mResult.Split(':')[1], mActivity.ClgCode.ToString());
                    }
                    else
                    {
                        mActivityCodeList.Add("LineId_" + mActivity.ProjectLineId,mResult.Replace("Ok;",""));
                    }
                }

                string mResultUpdate = mPDM.UpdateActivityCodeInProjectLines(pCc, mActivityCodeList, pListActivities.FirstOrDefault().ClgCode.ToString());
                if (mResultUpdate.Split(':')[0] == "Error")
                {
                    return Logger.WriteErrorDb(Enums.Documents.Activity, pCc.IdUserConected, mResultUpdate.Split(':')[1], "");
                }
                else
                {
                    return "OK";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(CompanyConn pCc, ARGNSProject pProject)
        {
            try
            {
                return mPDM.UpdateProject(pCc, pProject);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonObjectResult Add(CompanyConn pCc, ARGNSProject pProject)
        {
            try
            {
                return mPDM.AddProject(pCc, pProject);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
