﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class PDCRepository
    {
        private PDMDocument.MasterDataServiceClient mPDC;

        public PDCRepository()
        {
            mPDC = new PDMDocument.MasterDataServiceClient();
        }

        public List<ARGNSPDC> GetPDCList(CompanyConn pCc, ref PDCSAPCombo combo, DateTime? pPD, string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "")
        {
            List<ARGNSPDC> ret = mPDC.GetPDCList(pCc, ref combo, pPD, pCode, pEmployee, pShift, pVendor).ToList();
            return ret;
        }

        public ARGNSPDC GetPDCById(CompanyConn pCc, string pPDCCode, ref PDCSAPCombo combo)
        {
            ARGNSPDC mARGNSModel = new ARGNSPDC();
            mARGNSModel = mPDC.GetPDCById(pCc, pPDCCode, ref combo);
            return mARGNSModel;
        }

        public PDCSAPCombo GetCombo(CompanyConn pCc)
        {
            try
            {
                PDCSAPCombo combo = mPDC.GetPDCCombo(pCc);
                return combo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ARGNSPDCLine GetPDCLineCodeBar(CompanyConn pCc, string CodeBar)
        {
            try
            {
                return mPDC.GetPDCLineBarCode(pCc, CodeBar);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSPDCLine> GetPDCLinesCutTick(CompanyConn pCc, string CutTick)
        {
            try
            {
                return mPDC.GetPDCLineCutTick(pCc, CutTick).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(CompanyConn pCc, ARGNSPDC model)
        {
            try
            {
                string mResult = mPDC.AddPDC(pCc, model);

                if (mResult.Split(':')[0] == "Error")
                {
                    return Logger.WriteErrorDb(Enums.Documents.PDC, pCc.IdUserConected, mResult.Split(':')[1], "0");
                }
                else
                    return "Ok";
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
