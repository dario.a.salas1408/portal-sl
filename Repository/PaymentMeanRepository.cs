﻿using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Repository
{
    public class PaymentMeanRepository
    {
        private WSDocument.DocumentServiceClient wsDocument;
        
        public PaymentMeanRepository()
        {
            wsDocument = new WSDocument.DocumentServiceClient();
        }

        public List<PaymentMean> GetPaymentMeans(CompanyConn pCompanyParam, string branchCode)
        {
            var paymentMeans = wsDocument.GetPaymentMeans(pCompanyParam, branchCode).ToList();

            return paymentMeans;
        }

        public List<PaymentMeanType> GetPaymentMeanType(CompanyConn pCompanyParam)
        {
            var paymentMeanType = wsDocument.GetPaymentMeanType(pCompanyParam).ToList();

            return paymentMeanType;
        }


        public List<PaymentMeanOrder> GetForDraft(CompanyConn companyParam, int code)
        {
            var aaaa = wsDocument.GetForDraft(companyParam, code).ToList();


            return wsDocument.GetForDraft(companyParam, code).ToList();
        }
    }
}
