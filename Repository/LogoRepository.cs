﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class LogoRepository
    {
        private PDMDocument.MasterDataServiceClient mMasterData;

        public LogoRepository()
        {
            mMasterData = new PDMDocument.MasterDataServiceClient();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public List<ARGNSModelLogo> GetLogoList(CompanyConn pCc, string pCode)
        {
            using (mMasterData)
            {
                return mMasterData.GetLogoList(pCc, pCode).ToList();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public ARGNSLogo GetLogoById(CompanyConn pCc, string pCode)
        {
            mMasterData = new PDMDocument.MasterDataServiceClient();
            return mMasterData.GetLogoById(pCc, pCode);
        }
    }
}
