﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class LocalDraftsHeaderRepository
    {
        WebPortalModel Db;

        public LocalDraftsHeaderRepository()
        {
            Db = new WebPortalModel();
        }

        public List<User> GetListUser()
        {
            try
            {
                return Db.Users.Where(c => c.Active == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUser(int Id)
        {
            try
            {
                return Db.Users.Where(c => c.IdUser == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Add(User pUser)
        {
            try
            {

                using (Db)
                {

                    pUser.Active = true;
                    Security mSecurity = new Security();

                    ICollection<UserPageAction> cUserPageAction = pUser.UserPageActions;
                    ICollection<UserArea> cUserArea = pUser.UserAreas;

                    pUser.UserPageActions = null;
                    pUser.UserAreas = null;

                    Db.Users.Add(pUser);
                    Db.SaveChanges();

                    foreach (UserPageAction item in cUserPageAction)
                    {
                        item.IdUser = pUser.IdUser;
                        Db.UserPageActions.Add(item);
                        Db.SaveChanges();
                    }

                    foreach (UserArea item in cUserArea)
                    {
                        UserArea mUserAreaAdd = new UserArea { IdAreaKeys = item.AreaKey.IdAreaKeys, IdUser = pUser.IdUser, KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyUser(item.AreaKey.Area, pUser.IdUser), "Argentis") };

                        Db.UserAreas.Add(mUserAreaAdd);
                        Db.SaveChanges();
                    }

                    return true;
                }


            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(User pUser)
        {
            try
            {
                using (Db)
                {
                    Security mSecurity = new Security();
                    User mUser = Db.Users.Where(c => c.IdUser == pUser.IdUser).FirstOrDefault();

                    mUser.IdUser = pUser.IdUser;
                    mUser.Name = pUser.Name;
                    mUser.Password = pUser.Password;
                    mUser.UserIdSAP = pUser.UserIdSAP;
                    mUser.IdRol = pUser.IdRol;
                    Db.SaveChanges();

                    ICollection<UserPageAction> cUserPageAction = mUser.UserPageActions;
                    ICollection<UserArea> cUserArea = mUser.UserAreas;

                    Db.UserPageActions.RemoveRange(cUserPageAction);
                    Db.SaveChanges();

                    Db.UserAreas.RemoveRange(cUserArea);
                    Db.SaveChanges();

                    foreach (UserPageAction item in pUser.UserPageActions)
                    {
                        mUser.UserPageActions.Add(item);
                        Db.SaveChanges();
                    }

                    foreach (UserArea item in pUser.UserAreas)
                    {
                        UserArea mUserAreaAdd = new UserArea { IdAreaKeys = item.AreaKey.IdAreaKeys, IdUser = pUser.IdUser, KeyAdd = mSecurity.Encrypt(mSecurity.GetKeyUser(item.AreaKey.Area, pUser.IdUser), "Argentis") };
                        Db.UserAreas.Add(mUserAreaAdd);
                        Db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool Delete(User pUser)
        {
            try
            {
                using (Db)
                {
                    Db.Users.Where(c => c.IdUser == pUser.IdUser).FirstOrDefault().Active = false;

                    Db.SaveChanges();

                    var id = pUser.IdUser;

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool AddCompanyToUser(int IdUser, int IdCompany, string pUserNameSAP, string pUserPasswordSAP, string UserCodeSAP, string BPCode, int? SECode, string DftDistRule, string DftWhs, string pSalesTaxCodeDef, string pPurchaseTaxCodeDef, int? pUserGroupId, int? pCostPriceList, int? pBPGroupId)
        {

            try
            {
                using (Db)
                {
                    var mUser = Db.Users.FirstOrDefault(c => c.IdUser == IdUser);

                    var mCompany = Db.Companies.FirstOrDefault(c => c.IdCompany == IdCompany);

                    Db.UsersSettings.Add(new UsersSetting { User = mUser, CompanyConn = mCompany, UserNameSAP = pUserNameSAP, UserPasswordSAP = pUserPasswordSAP, UserCodeSAP = UserCodeSAP, IdBp = BPCode, IdSE = SECode, DftDistRule = DftDistRule, DftWhs = DftWhs, SalesTaxCodeDef = pSalesTaxCodeDef, PurchaseTaxCodeDef = pPurchaseTaxCodeDef, UserGroupId = pUserGroupId, CostPriceList = pCostPriceList, BPGroupId = pBPGroupId });

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool DeleteCompanyToUser(int IdUser, int IdCompany)
        {

            try
            {
                using (Db)
                {
                    var mUser = Db.Users.FirstOrDefault(c => c.IdUser == IdUser);

                    Db.UsersSettings.Remove(mUser.UsersSettings.Where(c => c.CompanyConn.IdCompany == IdCompany).FirstOrDefault());

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool UpdateMyAccount(User pUser)
        {
            try
            {
                using (Db)
                {
                    User mUser = Db.Users.Where(c => c.IdUser == pUser.IdUser).FirstOrDefault();

                    mUser.Name = pUser.Name;
                    mUser.Password = pUser.Password;
                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<SAPObject> GetSAPObjects()
        {
            try
            {
                List<SAPObject> mUser = Db.SAPObjects.ToList();
                return mUser;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool AddUDF(int pUserID, int pCompanyId, string pTableID, string pUDFName, int pFieldID, string pTypeID, string pUDFDesc, string pRTable)
        {
            try
            {
                UserUDF mUDFToAdd = new UserUDF();
                mUDFToAdd.IdUser = pUserID;
                mUDFToAdd.IdCompany = pCompanyId;
                mUDFToAdd.Document = pTableID;
                mUDFToAdd.UDFName = pUDFName;
                mUDFToAdd.FieldID = (short)pFieldID;
                mUDFToAdd.TypeID = pTypeID;
                mUDFToAdd.UDFDesc = pUDFDesc;
                mUDFToAdd.RTable = pRTable;

                Db.UserUDFs.Add(mUDFToAdd);
                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool DeleteUDF(int IdUser, int IdUDF)
        {
            try
            {
                Db.UserUDFs.Remove(Db.UserUDFs.Where(c => c.IdUser == IdUser && c.IdUDF == IdUDF).FirstOrDefault());
                Db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
