﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ChartRepository
    {
        WebPortalModel Db;

        public ChartRepository()
        {
            Db = new WebPortalModel();
        }

        public List<PortalChart> GetPortalChartList(int pIdCompany)
        {
            try
            {
                return Db.PortalCharts.Where(c => c.IdCompany == pIdCompany).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PortalChart> GetPortalChartListSearch(string pPortalPage, int pIdCompany, UserGroup pUserGroupI)
        {
            try
            {
                List<PortalChart> mListPortalChart = new List<PortalChart>();
                mListPortalChart = Db.PortalCharts.Include("PortalChartColumns").Include("PortalChartUserGroups").Where(c => c.PortalPage == pPortalPage && c.IdCompany == pIdCompany && c.PortalChartUserGroups.Count == 0).ToList();
                if (pUserGroupI != null)
                {
                    mListPortalChart.AddRange(Db.PortalCharts.Include("PortalChartColumns").Include("PortalChartUserGroups").Where(c => c.PortalPage == pPortalPage && c.IdCompany == pIdCompany && c.PortalChartUserGroups.Where(j => j.UserGroup_Id == pUserGroupI.Id).ToList().Count > 0).ToList());
                }

                return mListPortalChart;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public PortalChart GetPortalChartById(CompanyConn pCc, int pChartId)
        {
            try
            {
                PortalChart mPortalChart =  Db.PortalCharts.Include("PortalChartColumns").Include("PortalChartParamList").Where(c => c.IdChart == pChartId).FirstOrDefault();
                //mPortalChart.PortalChartParamList = Db.PortalChartParams.

                return mPortalChart;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
        public List<PortalChartType> GetPortalChartTypeList(CompanyConn pCc)
        {
            try
            {
                List<PortalChartType> list = new List<Model.Implementations.Portal.PortalChartType>();
                list.Add(new Model.Implementations.Portal.PortalChartType() { ChartColumnNumber = 0, ChartName = "", IdChartType = 0 });
                list.AddRange(Db.PortalChartTypes.ToList());
                return list;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PortalChartUserGroup> GetPortalChartUserGroupList(CompanyConn pCc, int pIdChart)
        {
            try
            {
                return Db.PortalChartUserGroups.Where(c => c.PortalChart_IdChart == pIdChart).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PortalChartParamsType> GetPortalChartParamsTypeList(CompanyConn pCc)
        {
            try
            {
                return Db.PortalChartParamsTypes.ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PortalChartParamsValueType> GetPortalChartParamsValueTypeList(CompanyConn pCc)
        {
            try
            {
                return Db.PortalChartParamsValueTypes.ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonObjectResult Update(PortalChart pPortalChart)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                PortalChart mPortalChart = Db.PortalCharts.Where(c => c.IdChart == pPortalChart.IdChart).FirstOrDefault();

                mPortalChart.ChartName = pPortalChart.ChartName;
                mPortalChart.IdChartType = pPortalChart.IdChartType;
                mPortalChart.IdSAPQuery = pPortalChart.IdSAPQuery;
                mPortalChart.NameSAPQuery = pPortalChart.NameSAPQuery;
                mPortalChart.PortalPage = pPortalChart.PortalPage;
                mPortalChart.MenuChart = pPortalChart.MenuChart;

                //Si hay mas de un PortalChartColumn en la lista controlo si debo agregar o eliminar de lo que haya en BD
                if (pPortalChart.PortalChartColumns.Count > 0)
                {
                    List<PortalChartColumn> mPortalChartColumnListAux = Db.PortalChartColumns.Where(c => c.IdChart == pPortalChart.IdChart).ToList();
                    foreach (PortalChartColumn mPortalChartColumnAux in pPortalChart.PortalChartColumns)
                    {
                        PortalChartColumn mPortalChartColumnAuxFromList = mPortalChartColumnListAux.Where(c => c.IdPortalColumn == mPortalChartColumnAux.IdPortalColumn).FirstOrDefault();
                        //Si es distinto de null es porque ya estaba en la base de datos no debo hacer nada solo borrarlo de la lista
                        if (mPortalChartColumnAuxFromList != null)
                        {
                            mPortalChartColumnAuxFromList.MappedFieldName = mPortalChartColumnAux.MappedFieldName;
                            Db.SaveChanges();
                            mPortalChartColumnListAux.Remove(mPortalChartColumnAuxFromList);
                        }
                        //Si es null no estaba entonces debo agregarlo.
                        else
                        {
                            Db.PortalChartColumns.Add(mPortalChartColumnAux);
                            Db.SaveChanges();
                            mPortalChartColumnListAux.Remove(mPortalChartColumnAuxFromList);
                        }
                    }
                    //Los que quedan en esta lista son los que estaban guardados en la BD pero ahora ya no fueron seleccionados por lo que debo borrarlos
                    foreach (PortalChartColumn mPortalChartColumnAux in mPortalChartColumnListAux)
                    {
                        Db.PortalChartColumns.Remove(mPortalChartColumnAux);
                        Db.SaveChanges();
                    }
                }
                //Si no hay ninguno en la lista simplemente controlo si debo eliminar los que estan en la BD
                else
                {
                    List<PortalChartColumn> mPortalChartColumnListAux = Db.PortalChartColumns.Where(c => c.IdChart == pPortalChart.IdChart).ToList();
                    foreach (PortalChartColumn mPortalChartColumnAux in mPortalChartColumnListAux)
                    {
                        Db.PortalChartColumns.Remove(mPortalChartColumnAux);
                        Db.SaveChanges();
                    }
                }

                //Si hay mas de un PortalChartUserGroup en la lista controlo si debo agregar o eliminar de lo que haya en BD
                if (pPortalChart.PortalChartUserGroups.Count > 0)
                {
                    List<PortalChartUserGroup> mPortalChartUserGroupListAux = Db.PortalChartUserGroups.Where(c => c.PortalChart_IdChart == pPortalChart.IdChart).ToList();
                    foreach (PortalChartUserGroup mPortalChartUserGroupAux in pPortalChart.PortalChartUserGroups)
                    {
                        PortalChartUserGroup mPortalChartUserGroupAuxFromList = mPortalChartUserGroupListAux.Where(c => c.UserGroup_Id == mPortalChartUserGroupAux.UserGroup_Id).FirstOrDefault();
                        //Si es distinto de null es porque ya estaba en la base de datos no debo hacer nada solo borrarlo de la lista
                        if (mPortalChartUserGroupAuxFromList != null)
                        {
                            mPortalChartUserGroupListAux.Remove(mPortalChartUserGroupAuxFromList);
                        }
                        //Si es null no estaba entonces debo agregarlo.
                        else
                        {
                            Db.PortalChartUserGroups.Add(mPortalChartUserGroupAux);
                            Db.SaveChanges();
                            mPortalChartUserGroupListAux.Remove(mPortalChartUserGroupAuxFromList);
                        }
                    }
                    //Los que quedan en esta lista son los que estaban guardados en la BD pero ahora ya no fueron seleccionados por lo que debo borrarlos
                    foreach (PortalChartUserGroup mPortalChartUserGroupAux in mPortalChartUserGroupListAux)
                    {
                        Db.PortalChartUserGroups.Remove(mPortalChartUserGroupAux);
                        Db.SaveChanges();
                    }
                }
                //Si no hay ninguno en la lista simplemente controlo si debo eliminar los que estan en la BD
                else
                {
                    List<PortalChartUserGroup> mPortalChartUserGroupListAux = Db.PortalChartUserGroups.Where(c => c.PortalChart_IdChart == pPortalChart.IdChart).ToList();
                    foreach (PortalChartUserGroup mPortalChartUserGroupAux in mPortalChartUserGroupListAux)
                    {
                        Db.PortalChartUserGroups.Remove(mPortalChartUserGroupAux);
                        Db.SaveChanges();
                    }
                }

                if (pPortalChart.PortalChartParamList.Count > 0)
                {
                    List<PortalChartParam> mPortalParamListAux = Db.PortalChartParams.Where(c => c.PortalChart.IdChart == pPortalChart.IdChart).ToList();
                    foreach (PortalChartParam mPortalParamAux in pPortalChart.PortalChartParamList)
                    {
                        PortalChartParam mPortalParamAuxFromList = mPortalParamListAux.Where(c => c.Id == mPortalParamAux.Id && mPortalParamAux.IsNew == false).FirstOrDefault();
                        //Si es distinto de null es porque ya estaba en la base de datos deboActualizar los datos
                        if (mPortalParamAuxFromList != null)
                        {
                            mPortalParamAuxFromList.ParamName = mPortalParamAux.ParamName;
                            mPortalParamAuxFromList.ParamNamePortal = mPortalParamAux.ParamNamePortal;
                            mPortalParamAuxFromList.ParamValue = mPortalParamAux.ParamValue;
                            mPortalParamAuxFromList.ParamFixedValue = mPortalParamAux.ParamFixedValue;
                            mPortalParamAuxFromList.ParamValueType = mPortalParamAux.ParamValueType;
                            mPortalParamListAux.Remove(mPortalParamAuxFromList);
                            Db.SaveChanges();
                        }
                        //Si es null no estaba entonces debo agregarlo.
                        else
                        {
                            mPortalParamAux.PortalChart = mPortalChart;
                            Db.PortalChartParams.Add(mPortalParamAux);
                            Db.SaveChanges();
                        }
                    }
                    //Los que quedaron en la lista es porque estaban en bd pero ya no estan mas, debo borrarlos.
                    foreach (PortalChartParam mPortalParampAux in mPortalParamListAux)
                    {
                        Db.PortalChartParams.Remove(mPortalParampAux);
                        Db.SaveChanges();
                    }
                }
                //Si no hay ninguno en la lista simplemente controlo si debo eliminar los que estan en la BD
                else
                {
                    List<PortalChartParam> mPortalParamListAux = Db.PortalChartParams.Where(c => c.PortalChart.IdChart == pPortalChart.IdChart).ToList();
                    foreach (PortalChartParam mPortalParampAux in mPortalParamListAux)
                    {
                        Db.PortalChartParams.Remove(mPortalParampAux);
                        Db.SaveChanges();
                    }
                }

                Db.SaveChanges();
                
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        //Falta Corregir el Add
        public JsonObjectResult Add(PortalChart pPortalChart)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                PortalChart mPortalChartAdded = Db.PortalCharts.Add(pPortalChart);

                Db.SaveChanges();

                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
                mJsonObjectResult.Code = mPortalChartAdded.IdChart.ToString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public bool Delete(int pIdChartDelete)
        {
            try
            {
                List<PortalChartColumn> mPortalChartColumnList = Db.PortalChartColumns.Where(c => c.IdChart == pIdChartDelete).ToList();
                Db.PortalChartColumns.RemoveRange(mPortalChartColumnList);
                List<PortalChartParam> mPortalChartParamList = Db.PortalChartParams.Where(c => c.PortalChart.IdChart == pIdChartDelete).ToList();
                Db.PortalChartParams.RemoveRange(mPortalChartParamList);
                PortalChart mPortalChart = Db.PortalCharts.Where(c => c.IdChart == pIdChartDelete).FirstOrDefault();
                Db.PortalCharts.Remove(mPortalChart);

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
