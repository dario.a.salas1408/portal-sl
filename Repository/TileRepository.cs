﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class TileRepository
    {
        WebPortalModel Db;

        public TileRepository()
        {
            Db = new WebPortalModel();
        }

        public List<PortalTile> GetPortalTileList(int pIdCompany)
        {
            try
            {
                return Db.PortalTiles.Where(c => c.IdCompany == pIdCompany).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PortalTile> GetPortalTileListSearch(int pIdCompany, UserGroup pUserGroup)
        {
            try
            {
                List<PortalTile> mListPortalTile = new List<PortalTile>();
                mListPortalTile = Db.PortalTiles.Include("PortalTileUserGroups").Where(c => c.IdCompany == pIdCompany && c.PortalTileUserGroups.Count == 0).ToList();
                if (pUserGroup != null)
                {
                    mListPortalTile.AddRange(Db.PortalTiles.Include("PortalTileUserGroups").Where(c => c.IdCompany == pIdCompany && c.PortalTileUserGroups.Where(j => j.UserGroup_Id == pUserGroup.Id).ToList().Count > 0).ToList());
                }

                return mListPortalTile;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public PortalTile GetPortalTileById(CompanyConn pCc, int pTileId)
        {
            try
            {
                PortalTile mPortalTile = Db.PortalTiles.Include("PortalTileParams").Where(c => c.IdTile == pTileId).FirstOrDefault();
                //mPortalChart.PortalChartParamList = Db.PortalChartParams.

                return mPortalTile;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PortalTileType> GetPortalTileTypeList(CompanyConn pCc)
        {
            try
            {
                return Db.PortalTileTypes.ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PortalTileUserGroup> GetPortalTileUserGroupList(CompanyConn pCc, int pIdTile)
        {
            try
            {
                return Db.PortalTileUserGroups.Where(c => c.PortalTile.IdTile == pIdTile).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonObjectResult Update(PortalTile pPortalTile)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                PortalTile mPortalTile = Db.PortalTiles.Where(c => c.IdTile == pPortalTile.IdTile).FirstOrDefault();

                mPortalTile.TileName = pPortalTile.TileName;
                mPortalTile.IdTileType = pPortalTile.IdTileType;
                mPortalTile.IdSAPQuery = pPortalTile.IdSAPQuery;
                mPortalTile.NameSAPQuery = pPortalTile.NameSAPQuery;
                mPortalTile.UrlPage = pPortalTile.UrlPage;
                mPortalTile.HtmlText = pPortalTile.HtmlText;
                mPortalTile.VisualOrder = pPortalTile.VisualOrder;

                //Si hay mas de un PortalTileUserGroup en la lista controlo si debo agregar o eliminar de lo que haya en BD
                if (pPortalTile.PortalTileUserGroups.Count > 0)
                {
                    List<PortalTileUserGroup> mPortalTileUserGroupListAux = Db.PortalTileUserGroups.Where(c => c.PortalTile.IdTile == pPortalTile.IdTile).ToList();
                    foreach (PortalTileUserGroup mPortalTileUserGroupAux in pPortalTile.PortalTileUserGroups)
                    {
                        PortalTileUserGroup mPortalTileUserGroupAuxFromList = mPortalTileUserGroupListAux.Where(c => c.UserGroup_Id == mPortalTileUserGroupAux.UserGroup_Id).FirstOrDefault();
                        //Si es distinto de null es porque ya estaba en la base de datos no debo hacer nada solo borrarlo de la lista
                        if (mPortalTileUserGroupAuxFromList != null)
                        {
                            mPortalTileUserGroupListAux.Remove(mPortalTileUserGroupAuxFromList);
                        }
                        //Si es null no estaba entonces debo agregarlo.
                        else
                        {
                            Db.PortalTileUserGroups.Add(mPortalTileUserGroupAux);
                            Db.SaveChanges();
                            mPortalTileUserGroupListAux.Remove(mPortalTileUserGroupAuxFromList);
                        }
                    }
                    //Los que quedan en esta lista son los que estaban guardados en la BD pero ahora ya no fueron seleccionados por lo que debo borrarlos
                    foreach (PortalTileUserGroup mPortalTileUserGroupAux in mPortalTileUserGroupListAux)
                    {
                        Db.PortalTileUserGroups.Remove(mPortalTileUserGroupAux);
                        Db.SaveChanges();
                    }
                }
                //Si no hay ninguno en la lista simplemente controlo si debo eliminar los que estan en la BD
                else
                {
                    List<PortalTileUserGroup> mPortalTileUserGroupListAux = Db.PortalTileUserGroups.Where(c => c.PortalTile.IdTile == pPortalTile.IdTile).ToList();
                    foreach (PortalTileUserGroup mPortalTileUserGroupAux in mPortalTileUserGroupListAux)
                    {
                        Db.PortalTileUserGroups.Remove(mPortalTileUserGroupAux);
                        Db.SaveChanges();
                    }
                }

                if (pPortalTile.PortalTileParams.Count > 0)
                {
                    List<PortalTileParam> mPortalParamListAux = Db.PortalTileParams.Where(c => c.PortalTile.IdTile == pPortalTile.IdTile).ToList();
                    foreach (PortalTileParam mPortalParamAux in pPortalTile.PortalTileParams)
                    {
                        PortalTileParam mPortalParamAuxFromList = mPortalParamListAux.Where(c => c.Id == mPortalParamAux.Id && mPortalParamAux.IsNew == false).FirstOrDefault();
                        //Si es distinto de null es porque ya estaba en la base de datos deboActualizar los datos
                        if (mPortalParamAuxFromList != null)
                        {
                            mPortalParamAuxFromList.ParamName = mPortalParamAux.ParamName;
                            mPortalParamAuxFromList.ParamValue = mPortalParamAux.ParamValue;
                            mPortalParamAuxFromList.ParamFixedValue = mPortalParamAux.ParamFixedValue;
                            mPortalParamListAux.Remove(mPortalParamAuxFromList);
                            Db.SaveChanges();
                        }
                        //Si es null no estaba entonces debo agregarlo.
                        else
                        {
                            mPortalParamAux.PortalTile = mPortalTile;
                            Db.PortalTileParams.Add(mPortalParamAux);
                            Db.SaveChanges();
                        }
                    }
                    //Los que quedaron en la lista es porque estaban en bd pero ya no estan mas, debo borrarlos.
                    foreach (PortalTileParam mPortalParampAux in mPortalParamListAux)
                    {
                        Db.PortalTileParams.Remove(mPortalParampAux);
                        Db.SaveChanges();
                    }
                }
                //Si no hay ninguno en la lista simplemente controlo si debo eliminar los que estan en la BD
                else
                {
                    List<PortalTileParam> mPortalParamListAux = Db.PortalTileParams.Where(c => c.PortalTile.IdTile == pPortalTile.IdTile).ToList();
                    foreach (PortalTileParam mPortalParampAux in mPortalParamListAux)
                    {
                        Db.PortalTileParams.Remove(mPortalParampAux);
                        Db.SaveChanges();
                    }
                }

                Db.SaveChanges();

                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        //Falta Corregir el Add
        public JsonObjectResult Add(PortalTile pPortalTile)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                Db.PortalTiles.Add(pPortalTile);

                Db.SaveChanges();

                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public bool Delete(int pIdTileDelete)
        {
            try
            {
                PortalTile mPortalTile = Db.PortalTiles.Where(c => c.IdTile == pIdTileDelete).FirstOrDefault();
                Db.PortalTiles.Remove(mPortalTile);

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
