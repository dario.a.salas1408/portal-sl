﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class PORepository
    {
        private WSDocument.DocumentServiceClient mPO;
        private WebPortalModel Db;
        public PORepository()
        {
            mPO = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        public PurchaseOrderSAP GetPurchaseOrderById(int pCode, int pUserId, CompanyConn pCc)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();

            List<UDF_ARGNS> mAuxUserUDFOPOR = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OPOR").ToList());
            List<UDF_ARGNS> mAuxUserUDFPOR1 = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "POR1").ToList());
            return mPO.GetAllbyPO(pCc, pCode, mAuxUserUDFOPOR.ToArray(), mAuxUserUDFPOR1.ToArray());

        }

        public JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            mPO = new WSDocument.DocumentServiceClient();

            return mPO.GetPurchaseOrderListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
        }

        public string Add(PurchaseOrderSAP pPO, CompanyConn pCompanyParam)
        {
            using (mPO)
            {
                string mResult = mPO.AddPurchaseOrder(pPO, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                {
                    return Logger.WriteErrorDb(Enums.Documents.PurchaseOrder, pCompanyParam.IdUserConected, mResult.Split(':')[1], pPO.DocEntry.ToString());
                }
                else
                    return "Ok";
            }
        }

        public string Update(PurchaseOrderSAP pPO, CompanyConn pCompanyParam)
        {
            using (mPO)
            {
                string mResult = mPO.UpdatePurchaseOrder(pPO, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                    return Logger.WriteErrorDb(Enums.Documents.PurchaseOrder, pCompanyParam.IdUserConected, mResult.Split(':')[1], pPO.DocEntry.ToString());
                else
                    return "Ok";
            }
        }

        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCc)
        {
            using (mPO)
            {
                return mPO.GetDocumentSAPCombo(pCc);
            }
        }

        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            using (mPO)
            {
                return mPO.GetPurchaseOrderFreights(pCc, pDocEntry).ToList();
            }
        }
    }
}
