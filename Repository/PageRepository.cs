﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class PageRepository
    {
        WebPortalModel Db;

        public PageRepository()
        {
            Db = new WebPortalModel();
        }

        public Page GetPage(int Id)
        {
            try
            {
                return Db.Pages.Where(c => c.IdPage == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Page> GetPageList()
        {
            try
            {
                return Db.Pages.ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<Page> GetPageByArea(string Area)
        {
            try
            {
                return Db.Pages.Where(c => c.AreaKey.Area == Area).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
