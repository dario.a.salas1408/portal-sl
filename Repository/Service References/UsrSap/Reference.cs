﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ARGNS.Repository.UsrSap {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="UsrSap.IUserService")]
    public interface IUserService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserList", ReplyAction="http://tempuri.org/IUserService/GetUserListResponse")]
        ARGNS.Model.Implementations.SAP.UserSAP[] GetUserList(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserList", ReplyAction="http://tempuri.org/IUserService/GetUserListResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP[]> GetUserListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserById", ReplyAction="http://tempuri.org/IUserService/GetUserByIdResponse")]
        ARGNS.Model.Implementations.SAP.UserSAP GetUserById(ARGNS.Model.Implementations.CompanyConn pCompanyParam, short pCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserById", ReplyAction="http://tempuri.org/IUserService/GetUserByIdResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP> GetUserByIdAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, short pCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetEmployeeList", ReplyAction="http://tempuri.org/IUserService/GetEmployeeListResponse")]
        ARGNS.Model.Implementations.SAP.EmployeeSAP[] GetEmployeeList(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetEmployeeList", ReplyAction="http://tempuri.org/IUserService/GetEmployeeListResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP[]> GetEmployeeListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetEmployeeSAPSearchByCode", ReplyAction="http://tempuri.org/IUserService/GetEmployeeSAPSearchByCodeResponse")]
        ARGNS.Model.Implementations.SAP.EmployeeSAP[] GetEmployeeSAPSearchByCode(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName, System.Nullable<short> pDepartment, bool pHaveUserCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetEmployeeSAPSearchByCode", ReplyAction="http://tempuri.org/IUserService/GetEmployeeSAPSearchByCodeResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP[]> GetEmployeeSAPSearchByCodeAsync(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName, System.Nullable<short> pDepartment, bool pHaveUserCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserSAPSearchByCode", ReplyAction="http://tempuri.org/IUserService/GetUserSAPSearchByCodeResponse")]
        ARGNS.Model.Implementations.SAP.UserSAP[] GetUserSAPSearchByCode(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserSAPSearchByCode", ReplyAction="http://tempuri.org/IUserService/GetUserSAPSearchByCodeResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP[]> GetUserSAPSearchByCodeAsync(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserSAPSearchByName", ReplyAction="http://tempuri.org/IUserService/GetUserSAPSearchByNameResponse")]
        ARGNS.Model.Implementations.SAP.UserSAP GetUserSAPSearchByName(ARGNS.Model.Implementations.CompanyConn pCc, string pUserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUserSAPSearchByName", ReplyAction="http://tempuri.org/IUserService/GetUserSAPSearchByNameResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP> GetUserSAPSearchByNameAsync(ARGNS.Model.Implementations.CompanyConn pCc, string pUserName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetEmployeeById", ReplyAction="http://tempuri.org/IUserService/GetEmployeeByIdResponse")]
        ARGNS.Model.Implementations.SAP.EmployeeSAP GetEmployeeById(ARGNS.Model.Implementations.CompanyConn pCompanyParam, System.Nullable<int> pCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetEmployeeById", ReplyAction="http://tempuri.org/IUserService/GetEmployeeByIdResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP> GetEmployeeByIdAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, System.Nullable<int> pCode);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAllEmployee", ReplyAction="http://tempuri.org/IUserService/GetAllEmployeeResponse")]
        ARGNS.Model.Implementations.SAP.EmployeeSAP[] GetAllEmployee(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAllEmployee", ReplyAction="http://tempuri.org/IUserService/GetAllEmployeeResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP[]> GetAllEmployeeAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAllSalesEmployee", ReplyAction="http://tempuri.org/IUserService/GetAllSalesEmployeeResponse")]
        ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[] GetAllSalesEmployee(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAllSalesEmployee", ReplyAction="http://tempuri.org/IUserService/GetAllSalesEmployeeResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[]> GetAllSalesEmployeeAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetWarehouseList", ReplyAction="http://tempuri.org/IUserService/GetWarehouseListResponse")]
        ARGNS.Model.Implementations.SAP.Warehouse[] GetWarehouseList(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetWarehouseList", ReplyAction="http://tempuri.org/IUserService/GetWarehouseListResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.Warehouse[]> GetWarehouseListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetDistributionRuleList", ReplyAction="http://tempuri.org/IUserService/GetDistributionRuleListResponse")]
        ARGNS.Model.Implementations.SAP.DistrRuleSAP[] GetDistributionRuleList(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetDistributionRuleList", ReplyAction="http://tempuri.org/IUserService/GetDistributionRuleListResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.DistrRuleSAP[]> GetDistributionRuleListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUDFByTableID", ReplyAction="http://tempuri.org/IUserService/GetUDFByTableIDResponse")]
        ARGNS.Model.Implementations.SAP.UDF_SAP[] GetUDFByTableID(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUDFByTableID", ReplyAction="http://tempuri.org/IUserService/GetUDFByTableIDResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UDF_SAP[]> GetUDFByTableIDAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUDF1ByTableID", ReplyAction="http://tempuri.org/IUserService/GetUDF1ByTableIDResponse")]
        ARGNS.Model.Implementations.SAP.UFD1_SAP[] GetUDF1ByTableID(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetUDF1ByTableID", ReplyAction="http://tempuri.org/IUserService/GetUDF1ByTableIDResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UFD1_SAP[]> GetUDF1ByTableIDAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetQueryResult", ReplyAction="http://tempuri.org/IUserService/GetQueryResultResponse")]
        string GetQueryResult(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetQueryResult", ReplyAction="http://tempuri.org/IUserService/GetQueryResultResponse")]
        System.Threading.Tasks.Task<string> GetQueryResultAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAlertsByUser", ReplyAction="http://tempuri.org/IUserService/GetAlertsByUserResponse")]
        ARGNS.Model.Implementations.SAP.AlertSAP[] GetAlertsByUser(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pUser);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAlertsByUser", ReplyAction="http://tempuri.org/IUserService/GetAlertsByUserResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.AlertSAP[]> GetAlertsByUserAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pUser);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAlertById", ReplyAction="http://tempuri.org/IUserService/GetAlertByIdResponse")]
        ARGNS.Model.Implementations.SAP.AlertSAP GetAlertById(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pAlertId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAlertById", ReplyAction="http://tempuri.org/IUserService/GetAlertByIdResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.AlertSAP> GetAlertByIdAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pAlertId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetSalesEmployeeSearchByName", ReplyAction="http://tempuri.org/IUserService/GetSalesEmployeeSearchByNameResponse")]
        ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[] GetSalesEmployeeSearchByName(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pSEName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetSalesEmployeeSearchByName", ReplyAction="http://tempuri.org/IUserService/GetSalesEmployeeSearchByNameResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[]> GetSalesEmployeeSearchByNameAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pSEName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetWarehouseListSearch", ReplyAction="http://tempuri.org/IUserService/GetWarehouseListSearchResponse")]
        ARGNS.Model.Implementations.SAP.Warehouse[] GetWarehouseListSearch(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string WhsCode, string WhsName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetWarehouseListSearch", ReplyAction="http://tempuri.org/IUserService/GetWarehouseListSearchResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.Warehouse[]> GetWarehouseListSearchAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string WhsCode, string WhsName);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetSAPQueryListSearch", ReplyAction="http://tempuri.org/IUserService/GetSAPQueryListSearchResponse")]
        ARGNS.Model.Implementations.SAP.QuerySAP[] GetSAPQueryListSearch(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetSAPQueryListSearch", ReplyAction="http://tempuri.org/IUserService/GetSAPQueryListSearchResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.QuerySAP[]> GetSAPQueryListSearchAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetSAPQueryResult", ReplyAction="http://tempuri.org/IUserService/GetSAPQueryResultResponse")]
        ARGNS.Model.Implementations.View.JsonObjectResult GetSAPQueryResult(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetSAPQueryResult", ReplyAction="http://tempuri.org/IUserService/GetSAPQueryResultResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.View.JsonObjectResult> GetSAPQueryResultAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAllPriceListSAP", ReplyAction="http://tempuri.org/IUserService/GetAllPriceListSAPResponse")]
        ARGNS.Model.Implementations.PDM.ComboList.PriceListSAP[] GetAllPriceListSAP(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUserService/GetAllPriceListSAP", ReplyAction="http://tempuri.org/IUserService/GetAllPriceListSAPResponse")]
        System.Threading.Tasks.Task<ARGNS.Model.Implementations.PDM.ComboList.PriceListSAP[]> GetAllPriceListSAPAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IUserServiceChannel : ARGNS.Repository.UsrSap.IUserService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class UserServiceClient : System.ServiceModel.ClientBase<ARGNS.Repository.UsrSap.IUserService>, ARGNS.Repository.UsrSap.IUserService {
        
        public UserServiceClient() {
        }
        
        public UserServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public UserServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public UserServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public UserServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ARGNS.Model.Implementations.SAP.UserSAP[] GetUserList(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetUserList(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP[]> GetUserListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetUserListAsync(pCompanyParam);
        }
        
        public ARGNS.Model.Implementations.SAP.UserSAP GetUserById(ARGNS.Model.Implementations.CompanyConn pCompanyParam, short pCode) {
            return base.Channel.GetUserById(pCompanyParam, pCode);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP> GetUserByIdAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, short pCode) {
            return base.Channel.GetUserByIdAsync(pCompanyParam, pCode);
        }
        
        public ARGNS.Model.Implementations.SAP.EmployeeSAP[] GetEmployeeList(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetEmployeeList(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP[]> GetEmployeeListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetEmployeeListAsync(pCompanyParam);
        }
        
        public ARGNS.Model.Implementations.SAP.EmployeeSAP[] GetEmployeeSAPSearchByCode(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName, System.Nullable<short> pDepartment, bool pHaveUserCode) {
            return base.Channel.GetEmployeeSAPSearchByCode(pCc, pRequesterCode, pRequesterName, pDepartment, pHaveUserCode);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP[]> GetEmployeeSAPSearchByCodeAsync(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName, System.Nullable<short> pDepartment, bool pHaveUserCode) {
            return base.Channel.GetEmployeeSAPSearchByCodeAsync(pCc, pRequesterCode, pRequesterName, pDepartment, pHaveUserCode);
        }
        
        public ARGNS.Model.Implementations.SAP.UserSAP[] GetUserSAPSearchByCode(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName) {
            return base.Channel.GetUserSAPSearchByCode(pCc, pRequesterCode, pRequesterName);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP[]> GetUserSAPSearchByCodeAsync(ARGNS.Model.Implementations.CompanyConn pCc, string pRequesterCode, string pRequesterName) {
            return base.Channel.GetUserSAPSearchByCodeAsync(pCc, pRequesterCode, pRequesterName);
        }
        
        public ARGNS.Model.Implementations.SAP.UserSAP GetUserSAPSearchByName(ARGNS.Model.Implementations.CompanyConn pCc, string pUserName) {
            return base.Channel.GetUserSAPSearchByName(pCc, pUserName);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UserSAP> GetUserSAPSearchByNameAsync(ARGNS.Model.Implementations.CompanyConn pCc, string pUserName) {
            return base.Channel.GetUserSAPSearchByNameAsync(pCc, pUserName);
        }
        
        public ARGNS.Model.Implementations.SAP.EmployeeSAP GetEmployeeById(ARGNS.Model.Implementations.CompanyConn pCompanyParam, System.Nullable<int> pCode) {
            return base.Channel.GetEmployeeById(pCompanyParam, pCode);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP> GetEmployeeByIdAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, System.Nullable<int> pCode) {
            return base.Channel.GetEmployeeByIdAsync(pCompanyParam, pCode);
        }
        
        public ARGNS.Model.Implementations.SAP.EmployeeSAP[] GetAllEmployee(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetAllEmployee(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.EmployeeSAP[]> GetAllEmployeeAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetAllEmployeeAsync(pCompanyParam);
        }
        
        public ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[] GetAllSalesEmployee(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetAllSalesEmployee(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[]> GetAllSalesEmployeeAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetAllSalesEmployeeAsync(pCompanyParam);
        }
        
        public ARGNS.Model.Implementations.SAP.Warehouse[] GetWarehouseList(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetWarehouseList(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.Warehouse[]> GetWarehouseListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetWarehouseListAsync(pCompanyParam);
        }
        
        public ARGNS.Model.Implementations.SAP.DistrRuleSAP[] GetDistributionRuleList(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetDistributionRuleList(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.DistrRuleSAP[]> GetDistributionRuleListAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetDistributionRuleListAsync(pCompanyParam);
        }
        
        public ARGNS.Model.Implementations.SAP.UDF_SAP[] GetUDFByTableID(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID) {
            return base.Channel.GetUDFByTableID(pCompanyParam, pTableID);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UDF_SAP[]> GetUDFByTableIDAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID) {
            return base.Channel.GetUDFByTableIDAsync(pCompanyParam, pTableID);
        }
        
        public ARGNS.Model.Implementations.SAP.UFD1_SAP[] GetUDF1ByTableID(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID) {
            return base.Channel.GetUDF1ByTableID(pCompanyParam, pTableID);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.UFD1_SAP[]> GetUDF1ByTableIDAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pTableID) {
            return base.Channel.GetUDF1ByTableIDAsync(pCompanyParam, pTableID);
        }
        
        public string GetQueryResult(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery) {
            return base.Channel.GetQueryResult(pCompanyParam, pQuery);
        }
        
        public System.Threading.Tasks.Task<string> GetQueryResultAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery) {
            return base.Channel.GetQueryResultAsync(pCompanyParam, pQuery);
        }
        
        public ARGNS.Model.Implementations.SAP.AlertSAP[] GetAlertsByUser(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pUser) {
            return base.Channel.GetAlertsByUser(pCompanyParam, pUser);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.AlertSAP[]> GetAlertsByUserAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pUser) {
            return base.Channel.GetAlertsByUserAsync(pCompanyParam, pUser);
        }
        
        public ARGNS.Model.Implementations.SAP.AlertSAP GetAlertById(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pAlertId) {
            return base.Channel.GetAlertById(pCompanyParam, pAlertId);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.AlertSAP> GetAlertByIdAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, int pAlertId) {
            return base.Channel.GetAlertByIdAsync(pCompanyParam, pAlertId);
        }
        
        public ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[] GetSalesEmployeeSearchByName(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pSEName) {
            return base.Channel.GetSalesEmployeeSearchByName(pCompanyParam, pSEName);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.SalesEmployeeSAP[]> GetSalesEmployeeSearchByNameAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pSEName) {
            return base.Channel.GetSalesEmployeeSearchByNameAsync(pCompanyParam, pSEName);
        }
        
        public ARGNS.Model.Implementations.SAP.Warehouse[] GetWarehouseListSearch(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string WhsCode, string WhsName) {
            return base.Channel.GetWarehouseListSearch(pCompanyParam, WhsCode, WhsName);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.Warehouse[]> GetWarehouseListSearchAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string WhsCode, string WhsName) {
            return base.Channel.GetWarehouseListSearchAsync(pCompanyParam, WhsCode, WhsName);
        }
        
        public ARGNS.Model.Implementations.SAP.QuerySAP[] GetSAPQueryListSearch(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetSAPQueryListSearch(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.SAP.QuerySAP[]> GetSAPQueryListSearchAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetSAPQueryListSearchAsync(pCompanyParam);
        }
        
        public ARGNS.Model.Implementations.View.JsonObjectResult GetSAPQueryResult(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery) {
            return base.Channel.GetSAPQueryResult(pCompanyParam, pQuery);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.View.JsonObjectResult> GetSAPQueryResultAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam, string pQuery) {
            return base.Channel.GetSAPQueryResultAsync(pCompanyParam, pQuery);
        }
        
        public ARGNS.Model.Implementations.PDM.ComboList.PriceListSAP[] GetAllPriceListSAP(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetAllPriceListSAP(pCompanyParam);
        }
        
        public System.Threading.Tasks.Task<ARGNS.Model.Implementations.PDM.ComboList.PriceListSAP[]> GetAllPriceListSAPAsync(ARGNS.Model.Implementations.CompanyConn pCompanyParam) {
            return base.Channel.GetAllPriceListSAPAsync(pCompanyParam);
        }
    }
}
