﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class NewRepository
    {
        WebPortalModel Db;

        public NewRepository()
        {
            Db = new WebPortalModel();
        }


        public List<News> GetNews()
        {
            try
            {
                using (Db)
                {
                    return Db.News.Where(c => c.Active == true).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public News GetNew(int Id)
        {
            try
            {
                using (Db)
                {
                    return Db.News.Where(c => c.Id == Id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Add(News pNews)
        {
            try
            {
                using (Db)
                {
                    pNews.Fecha = DateTime.Now;
                    pNews.Active = true;

                    Db.News.Add(pNews);

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(News pNews)
        {
            try
            {
                using (Db)
                {
                    News mNews = Db.News.Where(c => c.Id == pNews.Id).FirstOrDefault();

                    mNews.Id = pNews.Id;
                    mNews.Head = pNews.Head;
                    mNews.Description = pNews.Description;
                    mNews.Url = pNews.Url;
                    if(pNews.ImgUploadChange)
                        mNews.ImageName = pNews.ImageName;

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool Delete(News pNews)
        {
            try
            {
                using (Db)
                {
                    Db.News.Where(c => c.Id == pNews.Id).FirstOrDefault().Active = false;

                    Db.SaveChanges();

                    var id = pNews.Id;

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
