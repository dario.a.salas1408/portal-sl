﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using ARGNS.Repository.ServiceItemMaster;
using ARGNS.Model.Implementations.View;
using AutoMapper;
using ARGNS.Model;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Util;
using System.Web.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using ARGNS.View;
using Newtonsoft.Json;
using ARGNS.Model.Implementations.DiverSO;
using ARGNS.Model.Implementations.DiverSO.SalesDocProcess;
using static ARGNS.Util.Enums;

namespace ARGNS.Repository
{
    public class ItemMasterRepository
    {

        private WSDocument.DocumentServiceClient mOITM;
        private ServiceItemMasterClient mItemMasterService;
        private WebPortalModel Db;

        public ItemMasterRepository()
        {
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            mOITM = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        /// <summary>
        /// 
        /// </summary>   
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public DiverSOResultItems GetDiverSOItems(string pItemCode, string pCardCode, DiverSOItemMethod pType)
        {
            DiverSOResultItems oResult = null;
            ItemMasterDSORequest oParams = null;
            try
            {
                using (var client = new System.Net.Http.HttpClient())
                {
                    switch (pType)
                    {
                        case DiverSOItemMethod.Comparatives:
                            oParams = new ItemMasterDSORequest(DiverSOItemMethod.Comparatives.ToDescriptionString(), pItemCode, pCardCode);
                            break;
                        case DiverSOItemMethod.Complementaries:
                            oParams = new ItemMasterDSORequest(DiverSOItemMethod.Complementaries.ToDescriptionString(), pItemCode, pCardCode);
                            break;
                        case DiverSOItemMethod.Substitutes:
                            oParams = new ItemMasterDSORequest(DiverSOItemMethod.Substitutes.ToDescriptionString(), pItemCode, pCardCode);
                            break;
                        default:
                            break;
                    }
                    
                    // HTTP POST              
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage result = client.PostAsync(WebConfigurationManager.AppSettings["DiverSoUrl"] + "item", new StringContent(JsonConvert.SerializeObject(oParams))).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var oObject = result.Content.ReadAsStringAsync().Result;
                        oResult = JsonConvert.DeserializeObject<DiverSOResultItems>(oObject);
                        oResult.TotalRecords = oResult.value.Count;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterRepository -> GetDiverSOItems :" + ex.Message);
                Logger.WriteError("ItemMasterRepository -> GetDiverSOItems :" + ex.InnerException.Message);
            }

            return oResult;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetOITMListBy(
            CompanyConn pCc, bool pCheckApparelInstallation,
            string pPrchseItem, string pSellItem, string pInventoryItem,
            string pItemsWithStock, string pItemGroup,
            string pModCode, int pUserId, List<UDF_ARGNS> pMappedUdf,
            string pItemCode, string pItemName,
            bool pOnlyActiveItems,
            List<WarehousePortal> pWarehousePortalList,
            bool pCkCatalogueNum = false,
            string pBPCode = "", string pBPCatalogCode = "",
            int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                try
                {
                    List<UDF_ARGNS> mAuxUserUDFOITM = Mapper.Map<List<UDF_ARGNS>>(
                        Db.UserUDFs.Where(c => c.IdUser == pUserId &&
                        c.IdCompany == pCc.IdCompany && c.Document == "OITM").ToList());

                    if (pMappedUdf == null)
                        pMappedUdf = new List<UDF_ARGNS>();

                    return mItemMasterService.GetOITMListBy(pCc, pCheckApparelInstallation,
                        pPrchseItem, pSellItem, pInventoryItem,
                        pItemsWithStock, pItemGroup, pModCode,
                        pMappedUdf.ToArray(), pItemCode, pItemName,
                        pOnlyActiveItems, pWarehousePortalList.ToArray(),
                        mAuxUserUDFOITM.ToArray(),
                        pCkCatalogueNum, pBPCode, pBPCatalogCode,
                        pStart, pLength, pOrderColumn);
                }
                catch (Exception ex)
                {
                    Logger.WriteError("ItemMasterRepository -> GetOITMListBy :" + ex.Message);
                    Logger.WriteError("ItemMasterRepository -> GetOITMListBy :" + ex.InnerException.Message);

                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pListItems"></param>
        /// <param name="pItemType"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetOITMListByList(CompanyConn pCc, List<string> pListItems, string pItemType)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                try
                {
                    return mItemMasterService.GetOITMListByList(pCc, pListItems.ToArray(), pItemType).ToList();
                }
                catch (Exception ex)
                {
                    Logger.WriteError("ItemMasterRepository -> GetOITMListByList :" + ex.Message);
                    Logger.WriteError("ItemMasterRepository -> GetOITMListByList :" + ex.InnerException.Message);

                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pModelCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public StockModel GetStockByModel(CompanyConn pCompanyParam,
            string pModelCode, List<WarehousePortal> pWarehousePortalList)
        {
            mItemMasterService = new ServiceItemMasterClient();
            return mItemMasterService.GetStockByModel(pCompanyParam, pModelCode,
                pWarehousePortalList.ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="CheckApparelInstallation"></param>
        /// <param name="PrchseItem"></param>
        /// <param name="SellItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pCostPriceList"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="uCardCode"></param>
        /// <param name="pItemGroupCode"></param>
        /// <param name="pListUDFOITM"></param>
        /// <param name="pNoItemsWithZeroStockCk"></param>
        /// <param name="pSupplierCardCode"></param>
        /// <param name="pItemList"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetQuickOrderListBy(CompanyConn pCc, bool CheckApparelInstallation,
            string PrchseItem, string SellItem, string pModCode,
            int pUserId, int? pCostPriceList, List<UDF_ARGNS> pMappedUdf,
            string pItemCode, string pItemName,
            string uCardCode, short pItemGroupCode, bool pOnlyActiveItems, bool isSO,
            List<UDF_ARGNS> pListUDFOITM = null, bool pNoItemsWithZeroStockCk = false,
            string pSupplierCardCode = "", List<string> pItemList = null,
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                List<UDF_ARGNS> mAuxUserUDFOITM = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OITM").ToList());
                if (pMappedUdf == null)
                    pMappedUdf = new List<UDF_ARGNS>();

                return mItemMasterService.GetQuickOrderListBy(pCc, CheckApparelInstallation,
                    PrchseItem, SellItem, pModCode, pCostPriceList, pMappedUdf.ToArray(), pItemCode,
                    pItemName, uCardCode, pItemGroupCode, pOnlyActiveItems, isSO, mAuxUserUDFOITM.ToArray(),
                    pNoItemsWithZeroStockCk, pSupplierCardCode,
                    (pItemList != null ? pItemList.ToArray() : null), pStart, pLength, pOrderColumn);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="PrchseItem"></param>
        /// <param name="SellItem"></param>
        /// <param name="pCodeBar"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCodeBar(CompanyConn pCc, string PrchseItem, string SellItem, string pCodeBar)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GeItemByCodeBar(pCc, PrchseItem, SellItem, pCodeBar);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="ListItems"></param>
        /// <param name="CardCode"></param>
        /// <param name="pSession"></param>
        /// <param name="pOrderItems"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsPrice(CompanyConn pCc, List<ItemMasterSAP> ListItems, string CardCode, string pSession, bool isSO, bool pOrderItems = true, string ano ="")
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GetItemsPrice(pCc, ListItems.ToArray(), CardCode, pSession, isSO, pOrderItems, ano).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<StockModel> GetItemStock(CompanyConn pCc, string[] pItemCode, List<WarehousePortal> pWarehousePortalList)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GetItemStock(pCc, pItemCode, pWarehousePortalList.ToArray()).ToList();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public List<GLAccountSAP> GetGLAccountList(CompanyConn pCc)
        {
            using (mOITM)
            {
                return mOITM.GetGLAccountList(pCc).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCode(CompanyConn pCc, string pItemCode, int idUser, string pBarCode = null)
        {
            mItemMasterService = new ServiceItemMasterClient();

            List<UDF_ARGNS> mAuxUserUDF = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == idUser && c.IdCompany == pCc.IdCompany && (c.Document == "OSRN" || c.Document == "OITM")).ToList());
            
            using (mItemMasterService)
            {
                var reList = mItemMasterService.GeItemByCode(pCc, pItemCode, pBarCode, mAuxUserUDF.ToArray());
                return reList;
            }
        }

        public decimal GetPriceItemByPriceList(CompanyConn pCc, string pItemCode, short pPriceList)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GetPriceItemByPriceList(pCc, pItemCode, pPriceList);
            }
        }

        public List<UDF_ARGNS> GetItemMasterUDF(CompanyConn pCc, int pUserId)
        {
            List<UDF_ARGNS> mAuxUserUDFOITM = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "OITM").ToList());
            return mAuxUserUDFOITM;
        }

        public List<ItemGroupSAP> GetItemGroupList(CompanyConn pCc)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GetItemGroupList(pCc).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCodeFrom"></param>
        /// <param name="pItemCodeTo"></param>
        /// <param name="pVendorFrom"></param>
        /// <param name="pVendorTo"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pSelectedWH"></param>
        /// <returns></returns>
        public JsonObjectResult GetInventoryStatus(CompanyConn pCc, string pItemCodeFrom,
            string pItemCodeTo, string pVendorFrom, string pVendorTo,
            string pItemGroup, string[] pSelectedWH,
            int pStart, int pLength, OrderColumn pOrderColumn = null)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GetInventoryStatus(pCc,
                    pItemCodeFrom, pItemCodeTo,
                    pVendorFrom, pVendorTo,
                    pItemGroup, pSelectedWH,
                    pStart, pLength, pOrderColumn);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        public List<AvailableToPromise> GetATPByWarehouse(CompanyConn pCc, string pItemCode, string pWarehouse)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GetATPByWarehouse(pCc, pItemCode, pWarehouse).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="mListItems"></param>
        /// <param name="pFormula"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsWithPositiveStock(CompanyConn pCc, List<ItemMasterSAP> mListItems, string[] pFormula)
        {
            mItemMasterService = new ServiceItemMasterClient();
            using (mItemMasterService)
            {
                return mItemMasterService.GetItemsWithPositiveStock(pCc, mListItems.ToArray(), pFormula).ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<Warehouse> GetAvailableWarehousesByCompany(CompanyConn pCc, List<WarehousePortal> pWarehousePortalList)
        {
            mItemMasterService = new ServiceItemMasterClient();

            return mItemMasterService.GetAvailableWarehousesByCompany(pCc, pWarehousePortalList.ToArray()).ToList();
        }

        public List<ItemMasterSAP> GeUOMByItemList(CompanyConn pCc, List<ItemMasterSAP> pListItem, string pSellItem, string pPrchseItem)
        {
            mItemMasterService = new ServiceItemMasterClient();

            return mItemMasterService.GeUOMByItemList(pCc, pListItem.ToArray(), pSellItem, pPrchseItem).ToList();
        }
    }
}
