﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class CommentRepository
    {
        private PDMDocument.MasterDataServiceClient mCommentsPDM;

        public CommentRepository()
        {
            mCommentsPDM = new PDMDocument.MasterDataServiceClient();
        }

        public List<ARGNSModelComment> GetListComments(CompanyConn pCc, string ItemCode)
        {
            using (mCommentsPDM)
            {
                return mCommentsPDM.GetModelComment(pCc, ItemCode).ToList();
            }
        }

        public string AddComment(ARGNSModelComment pModel, CompanyConn pCompanyParam)
        {

            using (mCommentsPDM)
            {
                
                string mResult = mCommentsPDM.AddLogCommentMaster(pModel, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                {
                    return Logger.WriteErrorDb(Enums.Documents.SalesOrder, pCompanyParam.IdUserConected, mResult.Split(':')[1], pModel.U_MODCODE);
                }
                else
                    return "Ok";


            }
        }

        public string  UpdateComment(ARGNSModelComment pModel, CompanyConn pCompanyParam)
        {
            using (mCommentsPDM)
            {
               
                string mResult = mCommentsPDM.UpdateLogCommentMaster(pModel, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                {
                    return Logger.WriteErrorDb(Enums.Documents.SalesOrder, pCompanyParam.IdUserConected, mResult.Split(':')[1], pModel.U_MODCODE);
                }
                else
                    return "Ok";
            }
        }

    }
}
