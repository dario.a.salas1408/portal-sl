﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ScaleMasterRepository
    {
         private PDMDocument.MasterDataServiceClient mMasterData;

         public ScaleMasterRepository()
        {
            mMasterData = new PDMDocument.MasterDataServiceClient();
        }

         public List<ARGNSScale> GetScaleListSearch(CompanyConn pCc, string pScaleCode = "", string pScaleName = "")
         {
             using (mMasterData)
             {
                 return mMasterData.GetScaleMasterListSearch(pCc, pScaleCode, pScaleName).ToList();
             }

         }

         public List<ARGNSSize> GetScaleDescriptionListSearch(CompanyConn pCc, string pScaleCode = "")
         {
             //using (mMasterData)
             //{
               return mMasterData.GetScaleDescriptionListSearch(pCc, pScaleCode).ToList();
             //}
         }

         public List<ARGNSSize> GetSizeByScale(CompanyConn pCc, string pScaleCode = "", string pModeCode = "")
         {
             using (mMasterData)
             {
                 return mMasterData.GetSizeByScale(pCc, pScaleCode, pModeCode).ToList();
             }
         }
    }
}
