﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class LogRepository
    {
        WebPortalModel Db;

        public LogRepository()
        {
            Db = new WebPortalModel();
        }

        public List<Log> GetListLogByUser(int pIdUser)
        {
            try
            {
                return Db.Logs.Where(c => c.IdUser == pIdUser).OrderByDescending(c => c.dtLog).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
