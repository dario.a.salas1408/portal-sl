﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class ApprovalRepository
    {
        private WSDocument.DocumentServiceClient mDocumentService;

        public ApprovalRepository()
        {
            mDocumentService = new WSDocument.DocumentServiceClient();
        }

        public List<DocumentConfirmationLines> GetApprovalListSearch(CompanyConn pCc, int pUserSign, 
			DateTime? pDocDate = null, string pDocStatus = "", string pObjectId ="")
        {
            return mDocumentService.GetPurchaseApprovalListSearch(pCc, pUserSign, pDocDate, pDocStatus, pObjectId).ToList();
        }

        public List<DocumentConfirmationLines> GetPurchaseApprovalByOriginator(CompanyConn pCc, int pUserSign, DateTime? pDocDate = null, 
			string pDocStatus = "", string pObjectId = "")
        {
            return mDocumentService.GetPurchaseApprovalByOriginator(pCc, pUserSign, pDocDate, pDocStatus, pObjectId).ToList();
        }

        public List<DocumentConfirmationLines> GetSalesApprovalListSearch(CompanyConn pCc, 
			int pUserSign, DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            return mDocumentService.GetSalesApprovalListSearch(pCc, pUserSign, pDocDate, pDocStatus, pObjectId).ToList();
        }

        public List<DocumentConfirmationLines> GetSalesApprovalByOriginator(CompanyConn pCc, int pUserSign, 
			DateTime? pDocDate = null, string pDocStatus = "", string pObjectId ="")
        {
            return mDocumentService.GetSalesApprovalByOriginator(pCc, pUserSign, pDocDate, pDocStatus, pObjectId).ToList();
        }

        public bool SaveApprovalResponseDIServer(int owwdCode, string remark, string UserNameSAP, string UserPasswordSAP, string approvalCode, CompanyConn pCc)
        {
            if (mDocumentService.SaveApprovalResponseDIServer(owwdCode, remark, UserNameSAP, UserPasswordSAP, approvalCode, pCc).Split(':')[0] == "Error")
                return false;
            else
                return true;
        }

        public List<DocumentConfirmationLines> GetApprovalListByID(CompanyConn pCc,int pWddCode)
        {
            mDocumentService = new WSDocument.DocumentServiceClient();
            
            return mDocumentService.GetApprovalListByID(pCc, pWddCode).ToList();
            
        }

        public List<StageSAP> GetStageList(CompanyConn pCc)
        {
            mDocumentService = new WSDocument.DocumentServiceClient();
            return mDocumentService.GetStageList(pCc).ToList();
        }

        
    }
}
