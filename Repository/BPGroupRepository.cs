﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class BPGroupRepository
    {
        WebPortalModel Db;

        public BPGroupRepository()
        {
            Db = new WebPortalModel();
        }

        public BPGroup GetBPGroup(int Id)
        {
            try
            {
                return Db.BPGroups.Where(c => c.Id == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<BPGroup> GetBPGroupList(int IdCompany)
        {
            try
            {
                return Db.BPGroups.Where(c => c.IdCompany == IdCompany).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Add(BPGroup pBPGroup)
        {
            try
            {
                Db.BPGroups.Add(pBPGroup);

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(BPGroup pBPGroup)
        {
            try
            {
                BPGroup pBPGroupModel = Db.BPGroups.Where(c => c.Id == pBPGroup.Id).FirstOrDefault();
                List<BPGroupLine> ListAux = new List<BPGroupLine>();
                pBPGroupModel.Name = pBPGroup.Name;
                foreach (BPGroupLine mLine in pBPGroupModel.BPGroupLines)
                {
                    if(pBPGroup.BPGroupLines.Select(c => c.CardCode).Contains(mLine.CardCode))
                    {
                        pBPGroup.BPGroupLines = pBPGroup.BPGroupLines.Where(c => c.CardCode != mLine.CardCode).ToList();
                    }
                    else
                    {
                        ListAux.Add(mLine); 
                    }
                }
                foreach (BPGroupLine mLine in pBPGroup.BPGroupLines)
                {
                    mLine.BPGroup = pBPGroupModel;
                    pBPGroupModel.BPGroupLines.Add(mLine);
                }
                foreach (BPGroupLine mLine in ListAux)
                {
                    Db.BPGroupLines.Remove(mLine);
                }

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
