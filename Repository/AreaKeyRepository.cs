﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class AreaKeyRepository
    {
        WebPortalModel Db;

        public AreaKeyRepository()
        {
            Db = new WebPortalModel();
        }

        public AreaKey GetAreaKey(string pAreaName)
        {

            return Db.AreaKeys.Where(c => c.Area == pAreaName).FirstOrDefault();

        }

        public List<AreaKey> GetAreasKey()
        {
            using (Db)
            { return Db.AreaKeys.Include("Pages.Actions").ToList(); }

        }

        public AreaKey GetAreaById(int pId)
        {
            using (Db)
            { return Db.AreaKeys.Include("Pages.Actions").Where(c => c.IdAreaKeys == pId).FirstOrDefault(); }

        }

        public List<AreaKey> GetAreasKeyList()
        {
            return Db.AreaKeys.ToList(); 
        }
    }
}
