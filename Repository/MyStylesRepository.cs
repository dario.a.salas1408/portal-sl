﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.View;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class MyStylesRepository
	{
		private PDMDocument.MasterDataServiceClient mMyStyles;

		public MyStylesRepository()
		{
			mMyStyles = new PDMDocument.MasterDataServiceClient();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pUserCode"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <returns></returns>
        public JsonObjectResult GetUserPDMListSearch(CompanyConn pCc, string pUserCode,
            ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "",
            string pNameModel = "", string pSeasonModel = "",
            string pGroupModel = "", string pBrand = "",
            string pCollection = "", string pSubCollection = "",
            int pStart = 0, int pLength = 100)  
        {
            using (mMyStyles)
            {
                return mMyStyles.GetUserPDMListSearch(pCc, pUserCode, ref mPDMSAPCombo,
                    pCodeModel, pNameModel, pSeasonModel,
                    pGroupModel, pBrand, pCollection, pSubCollection,
                    pStart, pLength);
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCc"></param>
		/// <param name="pCardCode"></param>
		/// <param name="mPDMSAPCombo"></param>
		/// <param name="pCodeModel"></param>
		/// <param name="pNameModel"></param>
		/// <param name="pSeasonModel"></param>
		/// <param name="pGroupModel"></param>
		/// <param name="pBrand"></param>
		/// <param name="pCollection"></param>
		/// <param name="pSubCollection"></param>
		/// <returns></returns>
		public List<ARGNSModel> GetBPPDMListSearch(CompanyConn pCc, string pCardCode,
			ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "",
			string pNameModel = "", string pSeasonModel = "",
			string pGroupModel = "", string pBrand = "",
			string pCollection = "", string pSubCollection = "",
			int pStart = 0, int pLength = 0)
		{
			using (mMyStyles)
			{
				return mMyStyles.GetBPPDMListSearch(pCc, pCardCode, ref mPDMSAPCombo,
					pCodeModel, pNameModel, pSeasonModel,
					pGroupModel, pBrand, pCollection,
					pSubCollection, pStart, pLength).ToList();
			}

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCc"></param>
		/// <param name="pUserCode"></param>
		/// <param name="txtStyle"></param>
		/// <param name="txtCodeCostSheet"></param>
		/// <param name="txtDescription"></param>
		/// <returns></returns>
		public List<ARGNSCostSheet> GetUserCostSheetListSearch(CompanyConn pCc, string pUserCode, 
			string txtStyle = "", 
			string txtCodeCostSheet = "", 
			string txtDescription = "")
		{
			using (mMyStyles)
			{
				return mMyStyles.GetUserCostSheetListSearch(pCc, pUserCode, txtStyle, txtCodeCostSheet, txtDescription).ToList();
			}

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCc"></param>
		/// <param name="pCardCode"></param>
		/// <param name="txtStyle"></param>
		/// <param name="txtCodeCostSheet"></param>
		/// <param name="txtDescription"></param>
		/// <returns></returns>
		public List<ARGNSCostSheet> GetBPCostSheetListSearch(CompanyConn pCc, string pCardCode, 
			string txtStyle = "", 
			string txtCodeCostSheet = "", 
			string txtDescription = "")
		{
			using (mMyStyles)
			{
				return mMyStyles.GetBPCostSheetListSearch(pCc, pCardCode, txtStyle, txtCodeCostSheet, txtDescription).ToList();
			}

		}

		//public PDMSAPCombo GetPDMSAPCombo(CompanyConn pCc)
		//{

		//    PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
		//    using (mMyStyles)
		//    {
		//        mPDMSAPCombo.ListBrand = mMyStyles.GetBrandList(pCc).ToList();
		//        mPDMSAPCombo.ListModelGroup = mMyStyles.GetModelGroupList(pCc).ToList();
		//        mPDMSAPCombo.ListSeason = mMyStyles.GetSeasonList(pCc).ToList();
		//        mPDMSAPCombo.ListCollection = mMyStyles.GetCollectionList(pCc).ToList();
		//        mPDMSAPCombo.ListSubCollection = mMyStyles.GetSubCollectionList(pCc).ToList();

		//        return mPDMSAPCombo;
		//    }
		//}

	}
}
