﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class SamplesRepository
    {
        private PDMDocument.MasterDataServiceClient mCommentsPDM;

        public SamplesRepository()
        {
            mCommentsPDM = new PDMDocument.MasterDataServiceClient();
        }

        public List<ARGNSModelSample> GetListSample(CompanyConn pCc, string ItemCode)
        {
            return mCommentsPDM.GetModelSapmples(pCc, ItemCode).ToList();
        }

        public ARGNSModelSample GetSample(CompanyConn pCc, string SampleCode, string pModCode)
        {
            return mCommentsPDM.GetSample(pCc, SampleCode, pModCode);
        }


        public string AddSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            string mResult = mCommentsPDM.AddSample(pObject, pCompanyParam);

            if (mResult.Split(':')[0] == "Error")
            {
                return Logger.WriteErrorDb(Enums.Documents.Sample, pCompanyParam.IdUserConected, mResult.Split(':')[1], pObject.Code);
            }
            else
                return "Ok";

        }

        public string UpdateSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            string mResult = mCommentsPDM.UpdateSample(pObject, pCompanyParam);

            if (mResult.Split(':')[0] == "Error")
            {
                return Logger.WriteErrorDb(Enums.Documents.Sample, pCompanyParam.IdUserConected, mResult.Split(':')[1], pObject.Code);
            }
            else
                return "Ok";
        }

        public ARGNSModelSample GetSampleByCode(CompanyConn pCc, string SampleCode, string pModCode)
        {
            return mCommentsPDM.GetSampleByCodSample(pCc, SampleCode, pModCode);
        }
    }
}
