﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;

namespace ARGNS.Repository
{
	public class TechPackRepository
    {
        private PDMDocument.MasterDataServiceClient mApparelService;

        public TechPackRepository()
        {
            mApparelService = new PDMDocument.MasterDataServiceClient();
        }


        public ARGNSTechPack GetTechPack(CompanyConn pCc, int pCode)
        {
            return mApparelService.GetTechPack(pCc, pCode);
        }

        public string UpdateTechPack(ARGNSTechPack pTechPack, CompanyConn pCompanyParam)
        {
            mApparelService = new PDMDocument.MasterDataServiceClient();
            string result = mApparelService.UpdateTechPack(pTechPack, pCompanyParam);

            if (result.Split(':')[0] == "Error")
                return Logger.WriteErrorDb(Enums.Documents.TechPack, pCompanyParam.IdUserConected, result.Split(':')[1], pTechPack.ModelCode);
            else
                return "Ok";
        }
    }
}
