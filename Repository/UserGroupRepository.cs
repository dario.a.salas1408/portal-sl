﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class UserGroupRepository
    {
        WebPortalModel Db;

        public UserGroupRepository()
        {
            Db = new WebPortalModel();
        }

        public UserGroup GetUserGroup(int Id)
        {
            try
            {
                return Db.UserGroups.Where(c => c.Id == Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<UserGroup> GetUserGroupList(int IdCompany)
        {
            try
            {
                return Db.UserGroups.Where(c => c.IdCompany == IdCompany).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Add(UserGroup pUserGroup)
        {
            try
            {
                Db.UserGroups.Add(pUserGroup);

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(UserGroup pUserGroup)
        {
            try
            {
                UserGroup pUserGroupModel = Db.UserGroups.Where(c => c.Id == pUserGroup.Id).FirstOrDefault();

                pUserGroupModel.Name = pUserGroup.Name;
                pUserGroupModel.CSSName = pUserGroup.CSSName;
                pUserGroupModel.JSName = pUserGroup.JSName;

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
