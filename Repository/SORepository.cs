﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.DiverSO;
using ARGNS.Model.Implementations.DiverSO.Catalog;
using ARGNS.Model.Implementations.DiverSO.SalesDocProcess;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Configuration;

namespace ARGNS.Repository
{
    public class SORepository
    {
        private WSDocument.DocumentServiceClient mSO;
        private WebPortalModel Db;

        public SORepository()
        {
            mSO = new WSDocument.DocumentServiceClient();
            Db = new WebPortalModel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public SaleOrderSAP GetSaleOrderById(int pCode, int pUserId, CompanyConn pCc, bool pShowOpenQuantity)
        {
            //using (mSO)
            //{
            try
            {
                Mapper.CreateMap<UDF_ARGNS, UserUDF>();
                Mapper.CreateMap<UserUDF, UDF_ARGNS>();

                List<UDF_ARGNS> mAuxUserUDFORDR = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "ORDR").ToList());
                List<UDF_ARGNS> mAuxUserUDFRDR1 = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "RDR1").ToList());
                List<UDF_ARGNS> mAuxUserUDFCRD1 = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "CRD1").ToList());
                
                return mSO.GetAllbySO(pCc, pCode,
                    pShowOpenQuantity, mAuxUserUDFORDR.ToArray(),
                    mAuxUserUDFRDR1.ToArray(),
                    mAuxUserUDFCRD1.ToArray());
            }
            catch (Exception ex)
            {
                Logger.WriteError("SORepository -> GetSaleOrderById :" + ex.Message);
                return new SaleOrderSAP();
            }

            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCc,
            bool pShowOpenQuantityStatus,
            string pCodeVendor = "", DateTime? pDate = null,
            int? pDocNum = null, string pDocStatus = "",
            string pOwnerCode = "", string pSECode = "",
            int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            return mSO.GetSalesOrderListSearch(pCc,
                pShowOpenQuantityStatus,
                pCodeVendor,
                pDate, pDocNum, pDocStatus,
                pOwnerCode, pSECode, pStart,
                pLength, pOrderColumn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SaleOrderSAP pSO, CompanyConn pCompanyParam)
        {
            //using (mSO)
            //{

            var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

            if (udfIdPayment != "")
            {
                var idPayment = Db.Database.SqlQuery<int>("SELECT NEXT VALUE FOR GetIdPaymentMeans").FirstOrDefault();
                pSO.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value = idPayment.ToString();
            }

            JsonObjectResult mJsonObjectResult = mSO.AddSaleOrder(pSO, pCompanyParam);


            if (mJsonObjectResult.ServiceAnswer == Enums.ServiceResult.Error.ToDescriptionString())
            {
                Logger.WriteErrorDb(Enums.Documents.SalesOrder, pCompanyParam.IdUserConected, mJsonObjectResult.ErrorMsg, pSO.DocEntry.ToString());
                return mJsonObjectResult;
            }
            else
            {
                return mJsonObjectResult;
            }
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string Update(SaleOrderSAP pSO, CompanyConn pCompanyParam)
        {
            using (mSO)
            {
                string mResult = mSO.UpdateSaleOrder(pSO, pCompanyParam);

                if (mResult.Split(':')[0] == "Error")
                {
                    return Logger.WriteErrorDb(Enums.Documents.SalesOrder, pCompanyParam.IdUserConected, mResult.Split(':')[1], pSO.DocEntry.ToString());
                }
                else
                    return "Ok";
            }
        }

        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCc)
        {
            using (mSO)
            {
                return mSO.GetDocumentSAPCombo(pCc);
            }
        }

        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            return mSO.GetSalesOrderFreights(pCc, pDocEntry).ToList();
        }

        public DiverSOSalesDocProcessResult GetPromotionItemsByOrder(SaleOrderSAP pSalesOrder)
        {
            DiverSOSalesDocProcessResult oResult = null;
            try
            {
                List<DiverSOSalesDocProcessRequestDocLines> mRequestLines = new List<DiverSOSalesDocProcessRequestDocLines>();
                foreach (SaleOrderSAPLine mLine in pSalesOrder.Lines)
                {
                    DiverSOSalesDocProcessRequestDocLines mLineAux = new DiverSOSalesDocProcessRequestDocLines((short)mLine.LineNum, mLine.ItemCode, mLine.Quantity.Value, mLine.PriceAfDisc.Value, (mLine.Quantity.Value * mLine.PriceAfDisc.Value));
                    mRequestLines.Add(mLineAux);
                }
                string mOrigin = "";

                if (pSalesOrder.MappedUdf.Where(c => c.UDFName == "U_DocOri").FirstOrDefault() != null)
                {
                    mOrigin = pSalesOrder.MappedUdf.Where(c => c.UDFName == "U_DocOri").FirstOrDefault().Value;
                }

                DiverSOSalesDocProcessRequest oRequest = new DiverSOSalesDocProcessRequest("process", pSalesOrder.CardCode, mOrigin, pSalesOrder.DocDate.Value.ToString("yyyy-MM-ddTHH:mm:ss"), mRequestLines);

                using (var client = new System.Net.Http.HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage result = client.PostAsync(WebConfigurationManager.AppSettings["DiverSoUrl"] + "salesdoc", new StringContent(JsonConvert.SerializeObject(oRequest))).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var oObject = result.Content.ReadAsStringAsync().Result;
                        oResult = JsonConvert.DeserializeObject<DiverSOSalesDocProcessResult>(oObject);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("SORepository -> GetPromotionItemsByOrder :" + ex.Message);
                Logger.WriteError("SORepository -> GetPromotionItemsByOrder :" + ex.InnerException.Message);
            }

            return oResult;
        }

        public DiverSOCatalogResult GetDSOCatalogs(string pCatalogCode, string pCatalogName, string pCardCode, string pSlpCode, DateTime pDate, bool pIncludeItems, string pItemCode, string pItemName, string pItemGroup, int pStart = 0, int pLength = 0)
        {
            DiverSOCatalogResult oResult = null;
            try
            {
                DiverSOCatalogRequest oRequest = new DiverSOCatalogRequest("find", pCardCode, pCatalogCode, pCatalogName, pSlpCode, pDate.ToString("yyyy-MM-ddTHH:mm:ss"), pIncludeItems, pItemCode, pItemName, pItemGroup, pStart, pLength);

                using (var client = new System.Net.Http.HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage result = client.PostAsync(WebConfigurationManager.AppSettings["DiverSoUrl"] + "catalog", new StringContent(JsonConvert.SerializeObject(oRequest))).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var oObject = result.Content.ReadAsStringAsync().Result;
                        oResult = JsonConvert.DeserializeObject<DiverSOCatalogResult>(oObject);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("SORepository -> GetDSOCatalogs :" + ex.Message);
                Logger.WriteError("SORepository -> GetDSOCatalogs :" + ex.InnerException.Message);
            }

            return oResult;
        }

        
    }
}
