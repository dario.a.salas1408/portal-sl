﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ARGNS.Repository
{
	public class QueryManagerRepository
    {
        WebPortalModel Db;
        private UsrSap.UserServiceClient mUsrSap;
        public QueryManagerRepository()
        {
            Db = new WebPortalModel();
            mUsrSap = new UsrSap.UserServiceClient();
        }

        public List<QueryManagerItem> GetListQueryByCompany(int pIdCompany)
        {
            try
            {
                return Db.QueryManagerItems.Where(c => c.IdCompany == pIdCompany && c.Active == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QueryManagerItem GetQuery(int pIdQuery)
        {
            try
            {
                return Db.QueryManagerItems.Where(c => c.IdQuery == pIdQuery).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add(QueryManagerItem pQueryManagerItemModel)
        {
            try
            {
                pQueryManagerItemModel.Active = true;

                Db.QueryManagerItems.Add(pQueryManagerItemModel);

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Update(QueryManagerItem pQueryManagerItemModel)
        {
            try
            {
                QueryManagerItem mQueryManagerItemModel = Db.QueryManagerItems.Where(c => c.IdQuery == pQueryManagerItemModel.IdQuery).FirstOrDefault();

                mQueryManagerItemModel.SelectField = pQueryManagerItemModel.SelectField;
                mQueryManagerItemModel.FromField = pQueryManagerItemModel.FromField;
                mQueryManagerItemModel.WhereField = pQueryManagerItemModel.WhereField;
                mQueryManagerItemModel.OrderByField = pQueryManagerItemModel.OrderByField;
                mQueryManagerItemModel.GroupByField = pQueryManagerItemModel.GroupByField;
                mQueryManagerItemModel.QueryIdentifier = pQueryManagerItemModel.QueryIdentifier;

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool Delete(QueryManagerItem pQueryManagerItemModel)
        {
            try
            {
                Db.QueryManagerItems.Where(c => c.IdQuery == pQueryManagerItemModel.IdQuery).FirstOrDefault().Active = false;

                Db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public string GetQueryResult(CompanyConn pCompanyParam, string pQueryIdentifier, Dictionary<string, string> pParams)
        {
            try
            {
                QueryManagerItem mQueryManargerItem = Db.QueryManagerItems.Where(c => c.QueryIdentifier == pQueryIdentifier && c.IdCompany == pCompanyParam.IdCompany).FirstOrDefault();
                string mQuery = mQueryManargerItem.SelectField + " " + mQueryManargerItem.FromField + (!string.IsNullOrEmpty(mQueryManargerItem.WhereField) ? " " + mQueryManargerItem.WhereField : "") + (!string.IsNullOrEmpty(mQueryManargerItem.GroupByField) ? " " + mQueryManargerItem.GroupByField : "") + (!string.IsNullOrEmpty(mQueryManargerItem.OrderByField) ? " " + mQueryManargerItem.OrderByField : "");
                if(pParams != null)
                { 
                    foreach (var mDictRow in pParams)
                    {
                        mQuery = mQuery.Replace("{" + mDictRow.Key + "}", mDictRow.Value);
                    }
                }
                return mUsrSap.GetQueryResult(pCompanyParam, mQuery);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<QuerySAP> GetSAPQueryListSearch(CompanyConn pCompanyParam)
        {
            try
            {
                return mUsrSap.GetSAPQueryListSearch(pCompanyParam).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
