﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class DesignRepository
    {
        private PDMDocument.MasterDataServiceClient mDesignService;

        public DesignRepository()
        {
            mDesignService = new PDMDocument.MasterDataServiceClient();
        }

        public ARGNSModel GetDesignById(CompanyConn pCc, string pCode)
        {
            ARGNSModel mARGNSModel = new ARGNSModel();
            using (mDesignService)
            {
                mARGNSModel = mDesignService.GetDesignById(pCc, pCode);
                return mARGNSModel;
            }
        }

        public List<ARGNSModel> GetDesignListSearch(CompanyConn pCc, ref PDMSAPCombo mPDMSAPCombo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "", int pStart = 0, int pLength = 100)
        {
            using (mDesignService)
            {
                return mDesignService.GetDesignListSearch(pCc, ref mPDMSAPCombo, pCodeModel, 
					pNameModel, pSeasonModel, pGroupModel, 
					pBrand, pCollection, pSubCollection, pStart, pLength).ToList();
            }

        }

        public string UpdateDesign(ARGNSModel pDesign, CompanyConn pCompanyParam)
        {
            mDesignService = new PDMDocument.MasterDataServiceClient();
            string result = mDesignService.UpdateDesign(pDesign, pCompanyParam);

            if (result.Split(':')[0] == "Error")
                return Logger.WriteErrorDb(Enums.Documents.Design, pCompanyParam.IdUserConected, result.Split(':')[1], pDesign.Code);
            else
                return "Ok";

        }

        public string AddDesign(ARGNSModel pDesign, CompanyConn pCompanyParam)
        {
            mDesignService = new PDMDocument.MasterDataServiceClient();
            string result = mDesignService.AddDesign(pDesign, pCompanyParam);

            if (result.Split(':')[0] == "Error")
                return Logger.WriteErrorDb(Enums.Documents.Design, pCompanyParam.IdUserConected, result.Split(':')[1], pDesign.Code);
            else
                return "Ok";
        }
    }
}
