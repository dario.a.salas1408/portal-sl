﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class PDMRepository
    {
        private PDMDocument.MasterDataServiceClient mPDM;
        private WebPortalModel Db;

        public PDMRepository()
        {
            mPDM = new PDMDocument.MasterDataServiceClient();
            Db = new WebPortalModel();
        }

        public List<ARGNSModelFile> GetModelFileList(CompanyConn pCc, string pCode, string pU_ModCode)
        {
            List<ARGNSModelFile> ret = mPDM.GetModelFileList(pCc, pCode, pU_ModCode).ToList();
            return ret;
        }

        public ARGNSModel GetModelById(string pCode, string pModCode, int pUserId, CompanyConn pCc)
        {
            Mapper.CreateMap<UDF_ARGNS, UserUDF>();
            Mapper.CreateMap<UserUDF, UDF_ARGNS>();

            ARGNSModel mARGNSModel = new ARGNSModel();
            List<UDF_ARGNS> mAuxUserUDFModel = Mapper.Map<List<UDF_ARGNS>>(Db.UserUDFs.Where(c => c.IdUser == pUserId && c.IdCompany == pCc.IdCompany && c.Document == "@ARGNS_MODEL").ToList());
            
            mARGNSModel = mPDM.GetModelById(pCc, pCode, pModCode, mAuxUserUDFModel.ToArray());
            return mARGNSModel;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCode"></param>
        /// <param name="pImgList"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<SkuModel> GetSkuModelList(CompanyConn pCc, 
            string pCode, ref List<ARGNSModelImg> pImgList)
        {
            ARGNSModelImg[] pImgArray = pImgList.ToArray();

            List<SkuModel> returnList = mPDM.GetSkuModelList
                (pCc, pCode, ref pImgArray).ToList();

            pImgList = pImgArray.ToList();
            return returnList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModel"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateModel(ARGNSModel pModel, CompanyConn pCompanyParam)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();
            string result = mPDM.UpdateModel(pModel, pCompanyParam);

            if (result.Split(':')[0] == "Error")
                return Logger.WriteErrorDb(Enums.Documents.PDM, pCompanyParam.IdUserConected, result.Split(':')[1], pModel.U_ModCode);
            else
                return "Ok";
        }

        public JsonObjectResult AddModel(ARGNSModel pModel, CompanyConn pCompanyParam)
        {
            mPDM = new PDMDocument.MasterDataServiceClient();
            JsonObjectResult mJsonObjectResult = mPDM.AddModel(pModel, pCompanyParam);

            if (mJsonObjectResult.ServiceAnswer == Enums.ServiceResult.Error.ToDescriptionString())
            {
                Logger.WriteErrorDb(Enums.Documents.PDM, pCompanyParam.IdUserConected, mJsonObjectResult.ErrorMsg, pModel.U_ModCode);
                return mJsonObjectResult;
            }
            else
            { 
                return mJsonObjectResult;
            }
        }


        public List<ARGNSModel> GetPDMListSearch(CompanyConn pCc, ref PDMSAPCombo mPDMSAPCombo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "", int pStart = 0, int pLength = 100)
        {
            return mPDM.GetPDMListSearch(pCc, ref mPDMSAPCombo, pCodeModel, pNameModel, 
				pSeasonModel, pGroupModel, pBrand, 
				pCollection, pSubCollection, 
				pStart, pLength).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <param name="pCatalogCode"></param>
        /// <returns></returns>
        public JsonObjectResult GetPDMListDesc(CompanyConn pCc, 
            int pStart, int pLength,
            string pCodeModel = "", string pNameModel = "", string pSeasonModel = "", 
            string pGroupModel = "", string pBrand = "", string pCollection = "", 
            string pSubCollection = "", string pCatalogCode = "")
        {
            return mPDM.GetPDMListDesc(pCc, pStart, pLength,  pCodeModel, 
                pNameModel, pSeasonModel, pGroupModel, pBrand, pCollection, 
                pSubCollection, pCatalogCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public ModelDesc GetModelDesc(CompanyConn pCc, string pModCode)
        {
            return mPDM.GetModelDesc(pCc, pModCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public List<ARGNSCrPath> GetModelProjectList(CompanyConn pCc, string pModCode)
        {
            return mPDM.GetModelProjectList(pCc, pModCode).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public List<ARGNSModelPom> GetModelPomList(CompanyConn pCc, string pModCode)
        {
            return mPDM.GetModelPomList(pCc, pModCode).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pSclcodeList"></param>
        /// <param name="pPomCodeList"></param>
        /// <returns></returns>
        public List<ARGNSModelPomTemplate> GetModelPomTemplateList(CompanyConn pCc, List<string> pSclcodeList, List<string> pPomCodeList)
        {
            return mPDM.GetModelPomTemplateList(pCc, pSclcodeList.ToArray(), pPomCodeList.ToArray()).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pPOMCode"></param>
        /// <returns></returns>
        public List<ARGNSPomTemplateLines> GetModelPomTemplateLines(CompanyConn pCc, string pPOMCode)
        {
            return mPDM.GetModelPomTemplateLines(pCc, pPOMCode).ToList();
        }

        public List<ARGNSCrPath> GetCriticalPathListSearch(CompanyConn pCc, string pCollCode = "", string pSubCollCode = "", string pSeasonCode = "", string pModelCode = "", string pProjectCode = "", string pVendorCode = "", string pCustomerCode = "")
        {
            return mPDM.GetCriticalPathListSearch(pCc, pCollCode, pSubCollCode, pSeasonCode, pModelCode, pProjectCode, pVendorCode, pCustomerCode).ToList();
        }

        public List<ARGNSModelHistory> GetModelHistoryList(CompanyConn pCompanyParam, string pModelCode = "")
        {
            return mPDM.GetModelHistoryList(pCompanyParam, pModelCode).ToList();
        }

        public List<SAPLogs> GetSAPModelLogs(CompanyConn pCompanyParam, string pModelCode = "", string pLogInst = "")
        {
            return mPDM.GetSAPModelLogs(pCompanyParam, pModelCode, pLogInst).ToList();
        }

        public ARGNSSegmentation GetSegmentationById(CompanyConn pCompanyParam, string pCode)
        {
            return mPDM.GetSegmentationById(pCompanyParam, pCode);
        }

        public bool GetModelExist(CompanyConn pCompanyParam, string pModCode)
        {
            return mPDM.GetModelExist(pCompanyParam, pModCode);
        }

        public List<ARGNSCatalog> GetCatalogListSearch(CompanyConn pCc, string pCatalogCode = "", string pCatalogName = "", int? pSalesEmployee = null)
        {
            return mPDM.GetCatalogListSearch(pCc, pCatalogCode, pCatalogName, pSalesEmployee).ToList();
        }

        public List<ARGNSPrepack> GetModelPrepacks(CompanyConn pCc, string ModelId)
        {
            return mPDM.GetModelPrepacks(pCc, ModelId).ToList();
        }

        public List<ARGNSSizeRun> GetSizeRunsByScale(CompanyConn pCc, string pScaleCode)
        {
            return mPDM.GetSizeRunsByScale(pCc, pScaleCode).ToList();
        }
    }
}
