﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ARGNS.Repository
{
	public class CompaniesRepository
    {
        WebPortalModel Db;

        public CompaniesRepository()
        {
            Db = new WebPortalModel();
        }

        public List<CompanyConn> GetCompanies()
        {
            try
            {

                return Db.Companies.ToList();


            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<CompanyConn> GetAllCompaniesActives()
        {
            try
            {

                return Db.Companies.Where(c => c.Active == true).ToList();


            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<CompanyConn> GetCompaniesByUser(User pUser)
        {
            try
            {
                using (Db)
                {
                    return null;

                    //return Db.Users.Where(c => c.IdUser == pUser.IdUser).FirstOrDefault().Companies.Where(c => c.Active == true).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public CompanyConn GetCompany(int IdCompany)
        {
            try
            {

                return Db.Companies.Where(c => c.IdCompany == IdCompany).FirstOrDefault();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool Add(CompanyConn pCompany, User pUser)
        {
            try
            {
                using (Db)
                {
                    Db.Companies.Add(pCompany);

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool Update(CompanyConn pCompany)
        {
            try
            {
                using (Db)
                {
                    CompanyConn mCompany = Db.Companies.Where(c => c.IdCompany == pCompany.IdCompany).FirstOrDefault();

                    mCompany.CompanyDB = pCompany.CompanyDB;
                    mCompany.DbPassword = pCompany.DbPassword;
                    mCompany.DbUserName = pCompany.DbUserName;
                    mCompany.Password = pCompany.Password;
                    mCompany.Server = pCompany.Server;
                    mCompany.ServerType = pCompany.ServerType;
                    mCompany.UserName = pCompany.UserName;
                    mCompany.UseTrusted = pCompany.UseTrusted;
                    mCompany.Active = pCompany.Active;
                    mCompany.UseCompanyUser = pCompany.UseCompanyUser;
                    mCompany.UseHandheld = pCompany.UseHandheld;
                    mCompany.CodeType = pCompany.CodeType;
                    mCompany.CheckBasketID = pCompany.CheckBasketID;
                    mCompany.OnDemand = pCompany.OnDemand;
                    mCompany.ItemInMultipleLines = pCompany.ItemInMultipleLines;
                    mCompany.LicenseServer = pCompany.LicenseServer;
                    mCompany.PortNumber = pCompany.PortNumber;
                    mCompany.SAPLanguaje = pCompany.SAPLanguaje;
                    mCompany.UrlHana = pCompany.UrlHana;
                    mCompany.UrlHanaGetQueryResult = pCompany.UrlHanaGetQueryResult;
                    mCompany.UrlSL = pCompany.UrlSL;
                    mCompany.VisualOrder = pCompany.VisualOrder;

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool Delete(CompanyConn pCompany)
        {
            try
            {
                using (Db)
                {
                    Db.Companies.Where(c => c.IdCompany == pCompany.IdCompany).FirstOrDefault().Active = false;

                    Db.SaveChanges();
                }
                var id = pCompany.IdCompany;

                return true;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public CompanyConn SignIn(CompanyConn pCompany)
        {
            try
            {
                Connection.DIServerLoginClient mConnection = new Connection.DIServerLoginClient();

                return mConnection.Login(pCompany);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<UsersSetting> GetUserSettings()
        {
            try
            {
                Db = new WebPortalModel();
                return Db.UsersSettings.ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCompany"></param>
        /// <param name="idUserPortal"></param>
        /// <returns></returns>
        public UsersSetting GetUserSettingsByCompanyAndUser(int idCompany, int idUserPortal)
        {
            try
            {
                Db = new WebPortalModel();
                return Db.UsersSettings.Where(c => c.IdCompany == idCompany && c.IdUser == idUserPortal).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pServer"></param>
        /// <param name="pUser"></param>
        /// <param name="pPassword"></param>
        /// <param name="pCompanyDB"></param>
        /// <returns></returns>
        public bool ValidateSQLConnection(string pServer, string pUser, string pPassword, string pCompanyDB)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection("Server='" + pServer + "';Database='" + pCompanyDB + "';User ID='" + pUser + "';Password='" + pPassword + "'"))
                {
                    try
                    {
                        connection.Open();
                        if (connection.State == ConnectionState.Open)
                        {
                            return true;
                        }
                        return false;
                    }
                    catch (SqlException ex)
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool CheckApparelInstallation(CompanyConn pCompany)
        {
            try
            {
                Connection.DIServerLoginClient mConnection = new Connection.DIServerLoginClient();

                return mConnection.CheckApparelInstallation(pCompany);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public CompanySAPConfig GetCompanySAPConfig(CompanyConn pCompany)
        {
            try
            {
                Connection.DIServerLoginClient mConnection = new Connection.DIServerLoginClient();

                return mConnection.GetCompanySAPConfig(pCompany);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region QRConfig

        public QRConfig GetQRConfig(int IdCompany)
        {
            try
            {
                return Db.QRConfig.Where(c => c.IdCompany == IdCompany).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool AddQRConfig(QRConfig pQRConfig)
        {
            try
            {
                using (Db)
                {
                    Db.QRConfig.Add(pQRConfig);

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool UpdateQRConfig(QRConfig pQRConfig)
        {
            try
            {
                using (Db)
                {
                    QRConfig mQRConfig = Db.QRConfig.Where(c => c.IdCompany == pQRConfig.IdCompany).FirstOrDefault();

                    mQRConfig.Separator = pQRConfig.Separator;
                    mQRConfig.ItemCodePosition = pQRConfig.ItemCodePosition;
                    mQRConfig.OcrCodePosition = pQRConfig.OcrCodePosition;
                    mQRConfig.PricePosition = pQRConfig.PricePosition;
                    mQRConfig.QuantityPosition = pQRConfig.QuantityPosition;
                    mQRConfig.WhsCodePosition = pQRConfig.WhsCodePosition;
                    mQRConfig.CurrencyPosition = pQRConfig.CurrencyPosition;
                    mQRConfig.DscriptionPosition = pQRConfig.DscriptionPosition;
                    mQRConfig.FreeTxtPosition = pQRConfig.FreeTxtPosition;
                    mQRConfig.GLAccountPosition = pQRConfig.GLAccountPosition;
                    mQRConfig.UomCodePosition = pQRConfig.UomCodePosition;
                    mQRConfig.SerialPosition = pQRConfig.SerialPosition;
                    mQRConfig.BatchPosition = pQRConfig.BatchPosition;

                    Db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

    }
}
