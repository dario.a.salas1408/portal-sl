﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Repository
{
	public class RangePlanRepository
    {
        private PDMDocument.MasterDataServiceClient mApparelService;

        public RangePlanRepository()
        {
            mApparelService = new PDMDocument.MasterDataServiceClient();
        }

        public List<ARGNSRangePlan> GetRangePlanListSearch(CompanyConn pCompanyParam, ref RangePlanCombo mRangePlanCombo, string pCode = "", string pSeason = "", string pCollection = "", string pEmployee = "")
        {
            return mApparelService.GetRangePlanListSearch(pCompanyParam, ref mRangePlanCombo, pCode, pSeason, pCollection, pEmployee).ToList();
        }

        public ARGNSRangePlan GetRangePlanById(CompanyConn pCc, string pCode)
        {
            ARGNSRangePlan mRangePlan = new ARGNSRangePlan();

            mRangePlan = mApparelService.GetRangePlanById(pCc, pCode);
            return mRangePlan;
        }
        public RangePlanCombo GetRangePlanCombo(CompanyConn pCc)
        {
            return mApparelService.GetRangePlanCombo(pCc);
        }
        
        public string Update(ARGNSRangePlan pRangePlan, CompanyConn pCompanyParam)
        {
            mApparelService = new PDMDocument.MasterDataServiceClient();
            string result = mApparelService.UpdateRangePlan(pRangePlan, pCompanyParam);

            if (result.Split(':')[0] == "Error")
                return Logger.WriteErrorDb(Enums.Documents.PDM, pCompanyParam.IdUserConected, result.Split(':')[1], pRangePlan.Code);
            else
                return "Ok";
        }
    }
}
