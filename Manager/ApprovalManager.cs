﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class ApprovalManager
    {
        ApprovalRepository mApprovalRepository;

        public ApprovalManager()
        {
            mApprovalRepository = new ApprovalRepository();
        }

        public List<DocumentConfirmationLines> GetApprovalListSearch(CompanyConn pCc, int pUserSign, DateTime? pDocDate = null, 
			string pDocStatus = "", string pObjectId ="")
        {
            try
            {
                return mApprovalRepository.GetApprovalListSearch(pCc, pUserSign,pDocDate,pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ApprovalManager -> GetApprovalListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public List<DocumentConfirmationLines> GetPurchaseApprovalByOriginator(CompanyConn pCc, int pUserSign, DateTime? pDocDate = null, 
			string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                return mApprovalRepository.GetPurchaseApprovalByOriginator(pCc, pUserSign, pDocDate, pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ApprovalManager -> GetPurchaseApprovalByOriginator: " + ex.InnerException);
                throw ex;
            }
        }

        public List<DocumentConfirmationLines> GetSalesApprovalListSearch(CompanyConn pCc, 
			int pUserSign, DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                return mApprovalRepository.GetSalesApprovalListSearch(pCc, pUserSign, pDocDate, pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ApprovalManager -> GetSalesApprovalListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public List<DocumentConfirmationLines> GetSalesApprovalByOriginator(CompanyConn pCc, int pUserSign, DateTime? pDocDate = null, 
			string pDocStatus = "", string pObjectId ="")
        {
            try
            {
                return mApprovalRepository.GetSalesApprovalByOriginator(pCc, pUserSign, pDocDate, pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ApprovalManager -> GetSalesApprovalByOriginator: " + ex.InnerException);
                throw ex;
            }
        }

        public bool SaveApprovalResponse(CompanyConn pCc, int owwdCode, string remark, string UserNameSAP, string UserPasswordSAP, string approvalCode)
        {
            try
            {
                return mApprovalRepository.SaveApprovalResponseDIServer(owwdCode, remark, UserNameSAP, UserPasswordSAP, approvalCode, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ApprovalManager -> SaveApprovalResponse:" + ex.InnerException);
                throw;
            }
        }

        public List<DocumentConfirmationLines> GetApprovalListByID(CompanyConn pCc,int pWddCode)
        {
            try
            {
                return mApprovalRepository.GetApprovalListByID(pCc, pWddCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ApprovalManager -> GetApprovalListByID: " + ex.InnerException);
                throw ex;
            }
        }

        public List<StageSAP> GetStageList(CompanyConn pCc)
        {
            try
            {
                return mApprovalRepository.GetStageList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ApprovalManager -> GetStageList: " + ex.InnerException);
                throw ex;
            }
        }
        
    }
}
