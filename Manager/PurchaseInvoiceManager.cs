﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class PurchaseInvoiceManager
    {
        PIRepository mPIRepository;

        public PurchaseInvoiceManager()
        {
            mPIRepository = new PIRepository();

        }
        public PurchaseInvoiceSAP GetPurchaseInvoice(CompanyConn pCc, int Id, int pUserId)
        {
            try
            {
                PurchaseInvoiceSAP mreturn = Mapper.Map<PurchaseInvoiceSAP>(mPIRepository.GetPurchaseInvoiceById(Id, pUserId, pCc));
                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseInvoiceManager -> GetPurchaseInvoice:" + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetPurchaseInvoiceListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mPIRepository.GetPurchaseInvoiceListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseInvoiceManager -> GetPurchaseInvoiceListSearch:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
