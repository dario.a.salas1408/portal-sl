﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class BusinessPartnerManager
    {
        BPRepository mBPRepository;
        GlobalDocumentRepository mDocRepository;
        CompaniesManager mCompaniesManager;
        public BusinessPartnerManager()
        {
            mBPRepository = new BPRepository();
            mDocRepository = new GlobalDocumentRepository();
            mCompaniesManager = new CompaniesManager();
        }

        public BusinessPartnerSAP GetBusinessPartner(CompanyConn pCc, string Id, int pUserId, string LocalCurrency = "")
        {
            try
            {
                BusinessPartnerSAP mReturn = Mapper.Map<BusinessPartnerSAP>(mBPRepository.GetBusinessPartnerById(Id, pCc, pUserId));

                if (LocalCurrency != "" && LocalCurrency != mReturn.Currency)
                {
                    mReturn.RateCurrency = mDocRepository.GetCurrencyRate(mReturn.Currency, DateTime.Now, pCc);

                    if (mReturn.RateCurrency.Split(':')[0] == "Error")
                    {
                        mReturn.RateCurrency = "0";
                    }
                }
                else
                {
                    mReturn.RateCurrency = "1";
                }

                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> GetBusinessPartner:" + ex.InnerException);
                throw ex;
            }
        }

        public List<BusinessPartnerSAP> GetBusinessPartners(CompanyConn pCc)
        {
            try
            {
                return Mapper.Map<List<BusinessPartnerSAP>>(mBPRepository.GetBusinessPartnerList(pCc));
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> GetBusinessPartners:" + ex.InnerException);
                throw ex;
            }
        }

        public List<BusinessPartnerSAP> GetBusinessPartners(CompanyConn pCc, Enums.BpType Type)
        {
            try
            {
                return Mapper.Map<List<BusinessPartnerSAP>>(mBPRepository.GetBusinessPartnerList(pCc, Type)); ;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> GetBusinessPartners:" + ex.InnerException);
                throw ex;
            }
        }

        public string Add(BusinessPartnerSAP pBp, CompanyConn pCc)
        {
            try
            {
                return mBPRepository.Add(pBp, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public string Update(BusinessPartnerSAP pBp, CompanyConn pCc)
        {
            try
            {
                return mBPRepository.Update(pBp, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public List<BusinessPartnerSAP> GetBusinessPartnerListSearch(CompanyConn pCc, string pCodeBP = "", string pNameBP = "", string pTypeBP = "")
        {
            try
            {
                return Mapper.Map<List<BusinessPartnerSAP>>(mBPRepository.GetBusinessPartnerListSearch(pCc, pCodeBP, pNameBP, pTypeBP));
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> GetBusinessPartnerListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<StateSAP> GetStateList(CompanyConn pCc, string pCountry)
        {
            try
            {
                return mBPRepository.GetStateList(pCc, pCountry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> GetStateList:" + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetCustomerAgingReport(CompanyConn pCc, List<string> mBPListToFilter)
        {
            return mBPRepository.GetCustomerAgingReport(pCc, mBPListToFilter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pType"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pFilterUser"></param>
        /// <param name="pSECode"></param>
        /// <param name="pBPGroupId"></param>
        /// <param name="pGetLeads"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetBusinessPartnerListByUser(CompanyConn pCc, Enums.BpType pType, 
            bool pOnlyActiveItems, string pCodeBP = "", string pNameBP = "", bool pFilterUser = false, 
            int? pSECode = null, int? pBPGroupId = null, bool pGetLeads = false, int pStart = 0, 
            int pLength = 0, OrderColumn pOrderColumn = null, bool pFilterByBP = false, string licTracNum = "")
        {
            try
            {
                return mBPRepository.GetBusinessPartnerListByUser(pCc, pType, pOnlyActiveItems, 
                    pCodeBP, pNameBP, pFilterUser, pSECode, pBPGroupId, pGetLeads, 
                    pStart, pLength, pOrderColumn, pFilterByBP, licTracNum);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> GetBusinessPartnerListByUser:" + ex.InnerException);
                throw ex;
            }
        }

        public BusinessPartnerSAP GetBusinessPartnerMinimalData(CompanyConn pCc, string pCardCode)
        {
            try
            {
                return mBPRepository.GetBusinessPartnerMinimalData(pCc, pCardCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BusinessPartnerManager -> GetBusinessPartnerMinimalData:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
