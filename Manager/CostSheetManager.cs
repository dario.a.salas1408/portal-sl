﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Manager
{
	public class CostSheetManager
    {
        CostSheetRepository mCostSheetRepository;
        GlobalDocumentManager mGlobalDocumentManager;

        public CostSheetManager()
        {
            mCostSheetRepository = new CostSheetRepository();
            mGlobalDocumentManager = new GlobalDocumentManager();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        public List<ARGNSCostSheet> GetListCostSheet(CompanyConn pCc, string ItemCode)
        {
            try
            {
                return mCostSheetRepository.GetListCostSheet(pCc, ItemCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetListCostSheet: " + ex.InnerException);
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="txtStyle"></param>
        /// <param name="txtCodeCostSheet"></param>
        /// <param name="txtDescription"></param>
        /// <returns></returns>
        public List<ARGNSCostSheet> GetCostSheetListSearch(CompanyConn pCc, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            try
            {
                return mCostSheetRepository.GetCostSheetListSearch(pCc, txtStyle, txtCodeCostSheet, txtDescription);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetCostSheetListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="Code"></param>
        /// <param name="pModelCode"></param>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public ARGNSCostSheet GetListCostSheetByCode(CompanyConn pCc, string Code, string pModelCode, int pUserId)
        {
            try
            {
                return mCostSheetRepository.GetListCostSheetByCode(pCc, Code, pModelCode, pUserId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetListCostSheetByCode: " + ex.InnerException);
                return new ARGNSCostSheet();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pName"></param>
        /// <returns></returns>
        public List<ARGNSSchema> GetCSSchemaList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                return mCostSheetRepository.GetCSSchemaList(pCompanyParam, pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetCSSchemaList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public List<ARGNSSchemaLine> GetCSSchemaLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                return mCostSheetRepository.GetCSSchemaLineList(pCompanyParam, pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetCSSchemaLineList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pName"></param>
        /// <returns></returns>
        public List<ARGNSOperTemplate> GetCSOperationTemplateList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                return mCostSheetRepository.GetCSOperationTemplateList(pCompanyParam, pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetCSOperationTemplateList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public List<ARGNSOperTemplateLine> GetCSOperationTemplateLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                return mCostSheetRepository.GetCSOperationTemplateLineList(pCompanyParam, pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetCSOperationTemplateLineList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pName"></param>
        /// <returns></returns>
        public List<ARGNSPatternTemplate> GetCSPatternList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                return mCostSheetRepository.GetCSPatternList(pCompanyParam, pCode, pName).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetCSPatternList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public List<ARGNSPatternTemplateLine> GetCSPatternLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                return mCostSheetRepository.GetCSPatternLineList(pCompanyParam, pCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> GetCSPatternLineList :" + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCostSheet"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public string Update(ARGNSCostSheet pCostSheet, CompanyConn pCc)
        {
            try
            {
                return mCostSheetRepository.Update(pCostSheet, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCostSheet"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public JsonObjectResult AddCostSheet(ARGNSCostSheet pCostSheet, CompanyConn pCc)
        {
            try
            {
                return mCostSheetRepository.AddCostSheet(pCostSheet, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CostSheetManager -> AddCostSheet:" + ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="mListCurrency"></param>
        /// <param name="DocDate"></param>
        /// <returns></returns>
        public List<RatesSystem> GetListRates(CompanyConn pCc, List<CurrencySAP> mListCurrency, DateTime DocDate)
        {
            try
            {
                return mGlobalDocumentManager.GetListRatesSystem(pCc, mListCurrency, DocDate);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> GetListRates:" + ex.InnerException);
                throw ex;
            }
        }

    }
}
