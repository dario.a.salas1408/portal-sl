﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class DesignManager
    {
        DesignRepository mDesignRepository;

        public DesignManager()
        {
            mDesignRepository = new DesignRepository();
        }

        public ARGNSModel GetDesignById(CompanyConn pCc, string pCode)
        {
            try
            {
                ARGNSModel mModel = new ARGNSModel();
                mModel = Mapper.Map<ARGNSModel>(mDesignRepository.GetDesignById(pCc, pCode));
                return mModel;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DesignManager -> GetDesignById:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSModel> GetDesignListSearch(CompanyConn pCc, ref PDMSAPCombo mPDMSAPCombo, 
			string pCodeModel = "", string pNameModel = "", string pSeasonModel = "", 
			string pGroupModel = "", string pBrand = "", 
			string pCollection = "", string pSubCollection = "",
			int pStart = 0, int pLength = 0)
        {
            try
            {

                return Mapper.Map<List<ARGNSModel>>(
					mDesignRepository.GetDesignListSearch(pCc, ref mPDMSAPCombo, pCodeModel, 
						pNameModel, pSeasonModel, pGroupModel, pBrand, 
						pCollection, pSubCollection, pStart, pLength));
            }
            catch (Exception ex)
            {
                Logger.WriteError("DesignManager -> GetPDMListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public string UpdateDesign(ARGNSModel pDesign, CompanyConn pCc)
        {
            try
            {
                return mDesignRepository.UpdateDesign(pDesign, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DesignManager -> UpdateDesign:" + ex.InnerException);
                throw;
            }
        }

        public string AddDesign(ARGNSModel pDesign, CompanyConn pCc)
        {
            try
            {
                return mDesignRepository.AddDesign(pDesign, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DesignManager -> UpdateDesign:" + ex.InnerException);
                throw;
            }
        }
    }
}
