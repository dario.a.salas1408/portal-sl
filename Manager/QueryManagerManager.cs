﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class QueryManagerManager
    {
        QueryManagerRepository mQueryManagerRepository;

        public QueryManagerManager()
        {
            mQueryManagerRepository = new QueryManagerRepository();
        }

        public List<QueryManagerItem> GetListQueryByCompany(int pIdCompany)
        {
            try
            {
                return mQueryManagerRepository.GetListQueryByCompany(pIdCompany);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public QueryManagerItem GetQuery(int pIdQuery)
        {
            try
            {
                return mQueryManagerRepository.GetQuery(pIdQuery);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QueryManagerManager -> GetQuery:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Add(QueryManagerItem pQueryManagerItemModel)
        {
            try
            {
                return mQueryManagerRepository.Add(pQueryManagerItemModel);
            }
            catch (Exception ex)
            {

                Logger.WriteError("QueryManagerManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(QueryManagerItem pQueryManagerItemModel)
        {
            try
            {
                return mQueryManagerRepository.Update(pQueryManagerItemModel);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QueryManagerManager -> Update:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Delete(QueryManagerItem pQueryManagerItemModel)
        {
            try
            {
                return mQueryManagerRepository.Delete(pQueryManagerItemModel);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QueryManagerManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }

        public string GetQueryResult(CompanyConn pCompanyParam, string pQueryIdentifier, Dictionary<string, string> pParams)
        {
            try
            {
                return mQueryManagerRepository.GetQueryResult(pCompanyParam,pQueryIdentifier, pParams);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QueryManagerManager -> GetQueryResult:" + ex.InnerException);
                throw ex;
            }
        }

        public List<QuerySAP> GetSAPQueryListSearch(CompanyConn pCompanyParam)
        {
            try
            {
                return mQueryManagerRepository.GetSAPQueryListSearch(pCompanyParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
