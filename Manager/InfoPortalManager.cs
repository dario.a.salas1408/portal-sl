﻿using ARGNS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class InfoPortalManager
    {
        private InfoPortalRepository mInfoPortalRepository;

        public InfoPortalManager()
        {
            mInfoPortalRepository = new InfoPortalRepository();
        }

        public System.DateTime? GetLasUpdate()
        {
            return mInfoPortalRepository.GetLasUpdate();
        }

        public bool IsDbCreated()
        {
            return mInfoPortalRepository.IsDbCreated();
        }
    }
}
