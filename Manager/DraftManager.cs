﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class DraftManager
    {
        DraftRepository mDraftRepository;

        public DraftManager()
        {
            mDraftRepository = new DraftRepository();
        }

        public JsonObjectResult GetPurchaseUserDraftListSearch(CompanyConn pCc, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null,  DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mDraftRepository.GetPurchaseUserDraftListSearch(pCc, pUserSign, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetPurchaseUserDraftListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetPurchaseCustomerDraftListSearch(CompanyConn pCc, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mDraftRepository.GetPurchaseCustomerDraftListSearch(pCc, pBP, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetPurchaseCustomerDraftListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetPurchaseSalesEmployeeDraftListSearch(CompanyConn pCc, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mDraftRepository.GetPurchaseSalesEmployeeDraftListSearch(pCc, pSalesEmployee, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetPurchaseSalesEmployeeDraftListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetSalesUserDraftListSearch(CompanyConn pCc, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mDraftRepository.GetSalesUserDraftListSearch(pCc, pUserSign, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetSalesUserDraftListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetSalesCustomerDraftListSearch(CompanyConn pCc, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mDraftRepository.GetSalesCustomerDraftListSearch(pCc, pBP, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetSalesCustomerDraftListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetSalesSalesEmployeeDraftListSearch(CompanyConn pCc, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mDraftRepository.GetSalesSalesEmployeeDraftListSearch(pCc, pSalesEmployee, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetSalesSalesEmployeeDraftListSearch: " + ex.InnerException);
                throw ex;
            }
        }

        public Draft GetDraftById(CompanyConn pCc, int pCode)
        {
            try
            {
                Draft mReturn = mDraftRepository.GetDraftById(pCode, pCc);

                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetDraftById:" + ex.InnerException);
                throw ex;
            }
        }

        public bool CreateDocument(CompanyConn pCc, string pSession, int DraftDocEntry)
        {
            try
            {
                return mDraftRepository.CreateDocumentDIServer(pCc, pSession, DraftDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> SaveDraftToDocument:" + ex.InnerException);
                throw;
            }
        }

        public List<DocumentConfirmationLines> GetApprovalListByDocumentId(CompanyConn pCc, int pDocEntry, string pObjType)
        {
            try
            {
                return mDraftRepository.GetApprovalListByDocumentId(pCc, pDocEntry, pObjType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetApprovalListByDocumentId:" + ex.InnerException);
                throw;
            }
        }

        public List<StageSAP> GetStageList(CompanyConn pCc)
        {
            try
            {
                return mDraftRepository.GetStageList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftManager -> GetStageList: " + ex.InnerException);
                throw ex;
            }
        }

    }
}
