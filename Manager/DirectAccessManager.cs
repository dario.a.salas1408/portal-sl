﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class DirectAccessManager
    {
        private DirectAccessRepository mDirectAccessRepository;

        public DirectAccessManager()
        {
            mDirectAccessRepository = new DirectAccessRepository();
        }

        public List<DirectAccess> GetDirectAccessList()
        {
            try
            {
                return mDirectAccessRepository.GetDirectAccessList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DirectAccessManager -> GetDirectAccessList: " + ex.InnerException);
                return null;
            }
        }

        public List<UserDirectAccess> GetUserDirectAccessList(int pIdUSer, int pIdCompany)
        {
            try
            {
                return mDirectAccessRepository.GetUserDirectAccessList(pIdUSer, pIdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DirectAccessManager -> GetUserDirectAccessList: " + ex.InnerException);
                return null;
            }
        }

        public List<Actions> GetActionList()
        {
            try
            {
                return mDirectAccessRepository.GetActionList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DirectAccessManager -> GetActionList: " + ex.InnerException);
                return null;
            }
        }

        public bool UpdateMyDirectAccesses(List<int> pListDirectAccesses, List<int> pListDirectAccessesCustom, int pIdUser, int pIdCompany)
        {
            try
            {
                return mDirectAccessRepository.UpdateMyDirectAccesses(pListDirectAccesses, pListDirectAccessesCustom, pIdUser, pIdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DirectAccessManager -> UpdateMyDirectAccesses: " + ex.InnerException);
                return false;
            }

        }
    }
}
