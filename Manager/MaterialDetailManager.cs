﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.MaterialDetails;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class MaterialDetailManager
    {
        MaterialDetailRepository mMaterialDetailRepository;

        public MaterialDetailManager()
        {
            mMaterialDetailRepository = new MaterialDetailRepository();
        }

        public ARGNSMaterialDetail GetMaterialDetails(CompanyConn pCc, string pModCode, int pUserId)
        {
            try
            {
                Mapper.CreateMap<ARGNSMaterialDetail, UserUDF>();
                Mapper.CreateMap<UserUDF, ARGNSMaterialDetail>();

                ARGNSMaterialDetail mMaterialDetail = new ARGNSMaterialDetail();
                mMaterialDetail = mMaterialDetailRepository.GetMaterialDetails(pCc, pModCode, pUserId);

                return mMaterialDetail;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MaterialDetailManager -> GetMaterialDetails:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSModelColor> GetColor(CompanyConn pCc, string pModCode)
        {
            try
            {
                List<ARGNSModelColor> mListColor = mMaterialDetailRepository.GetColor(pCc, pModCode);

                return mListColor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(CompanyConn pCc, ARGNSMaterialDetail pMaterialDetail)
        {
            try
            {
                return mMaterialDetailRepository.Update(pCc, pMaterialDetail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Add(CompanyConn pCc, ARGNSMaterialDetail pMaterialDetail)
        {
            try
            {
                return mMaterialDetailRepository.Add(pCc, pMaterialDetail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
