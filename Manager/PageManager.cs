﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class PageManager
    {
        PageRepository mPageRepository;
        public PageManager()
        {
            mPageRepository = new PageRepository();
        }

        public Page GetPage(int Id)
        {
            try
            {
                return mPageRepository.GetPage(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PageManager -> GetPage:" + ex.InnerException);
                throw ex;
            }
        }

        public List<Page> GetPageList()
        {
            try
            {
                return mPageRepository.GetPageList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("PageManager -> GetPageList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<Page> GetPageByArea(string Area)
        {
            try
            {
                return mPageRepository.GetPageByArea(Area);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PageManager -> GetPageByArea:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
