﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class PurchaseRequestManager
    {
        PurchaseRequestRepository mPRRepository;
        GlobalDocumentManager mGlobalDocumentManager;

        public PurchaseRequestManager()
        {
            mPRRepository = new PurchaseRequestRepository();
            mGlobalDocumentManager = new GlobalDocumentManager();
        }

        public PurchaseRequestSAP GetPurchaseRequest(CompanyConn pCc, int pCode, int pUserId)
        {
            try
            {
                PurchaseRequestSAP mreturn = mPRRepository.GetPurchaseRequestById(pCode, pUserId, pCc);

                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseRequestManager -> GetPurchaseRequest:" + ex.InnerException);
                throw ex;
            }
        }

        public string Add(PurchaseRequestSAP pPR, CompanyConn pCc)
        {
            try
            {
                return mPRRepository.Add(pPR, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public string Update(PurchaseRequestSAP pPR, CompanyConn pCc)
        {
            try
            {
                return mPRRepository.Update(pPR, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public JsonObjectResult GetPurchaseRequestListSearch(CompanyConn pCc, string pRequest = "", DateTime? pDate = null, int? pDocNum = null, int? pDepartment = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mPRRepository.GetPurchaseOrderListSearch(pCc, pRequest, pDate, pDocNum, pDepartment, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseRequestManager -> GetPurchaseRequestListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PurchaseRequestLineSAP> GetPurchaseRequestLinesSearch(CompanyConn pCc, string[] pDocuments)
        {
            try
            {
                return mPRRepository.GetPurchaseRequestLinesSearch(pCc, pDocuments);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseRequestManager -> GetPurchaseRequestLinesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public PurchaseRequestCombo GetPurchaseRequestCombo(CompanyConn pCc)
        {
            try
            {
                PurchaseRequestCombo mreturn = mPRRepository.GetPurchaseRequestCombo(pCc);
                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseRequestManager -> GetPurchaseRequestCombo:" + ex.InnerException);
                throw ex;
            }
        }

        public List<RatesSystem> GetListRates(CompanyConn pCc, List<CurrencySAP> mListCurrency, DateTime DocDate)
        {
            try
            {
                return mGlobalDocumentManager.GetListRatesSystem(pCc, mListCurrency, DocDate);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseRequestManager -> GetListRates:" + ex.InnerException);
                throw ex;
            }
        }

        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            try
            {
                return mPRRepository.GetFreights(pCc, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseRequestManager -> GetFreights:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
