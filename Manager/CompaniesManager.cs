﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class CompaniesManager
    {
        CompaniesRepository mCompaniesRepository;

        public CompaniesManager()
        {
            mCompaniesRepository = new CompaniesRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CompanyConn> GetCompanies()
        {
            try
            {
                return mCompaniesRepository.GetCompanies();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetCompanies:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CompanyConn> GetAllCompaniesActives()
        {
            try
            {
                return mCompaniesRepository.GetAllCompaniesActives();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetAllCompaniesActives:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public CompanyConn GetCompany(int IdCompany)
        {
            try
            {
                return mCompaniesRepository.GetCompany(IdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetCompany:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public List<CompanyConn> GetCompaniesByUser(User pUser)
        {
            try
            {
                return mCompaniesRepository.GetCompaniesByUser(pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetCompaniesByUser:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Add(CompanyConn pCompany, User pUser)
        {
            try
            {
                return mCompaniesRepository.Add(pCompany, pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> Add :" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool Update(CompanyConn pCompany)
        {
            try
            {
                return mCompaniesRepository.Update(pCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> Update :" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool Delete(CompanyConn pCompany)
        {
            try
            {
                return mCompaniesRepository.Delete(pCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> Delete :" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public CompanyConn SignIn(CompanyConn pCompany)
        {
            try
            {
                return mCompaniesRepository.SignIn(pCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> SignIn :" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<UsersSetting> GetUserSettings()
        {
            try
            {
                return mCompaniesRepository.GetUserSettings();
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetUserSettings:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCompany"></param>
        /// <param name="idUserPortal"></param>
        /// <returns></returns>
        public UsersSetting GetUserSettingsByCompanyAndUser(int idCompany, int idUserPortal)
        {
            try
            {
                return mCompaniesRepository.GetUserSettingsByCompanyAndUser(idCompany,idUserPortal);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetUserSettingsByCompanyAndUser:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pServer"></param>
        /// <param name="pUser"></param>
        /// <param name="pPassword"></param>
        /// <param name="pCompanyDB"></param>
        /// <returns></returns>
        public bool ValidateSQLConnection(string pServer, string pUser, string pPassword, string pCompanyDB)
        {
            try
            {
                return mCompaniesRepository.ValidateSQLConnection(pServer,pUser,pPassword,pCompanyDB);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> ValidateSQLConnection:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public bool CheckApparelInstallation(CompanyConn pCompany)
        {
            try
            {
                return mCompaniesRepository.CheckApparelInstallation(pCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> CheckApparelInstallation :" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public CompanySAPConfig GetCompanySAPConfig(CompanyConn pCompany)
        {
            try
            {
                return mCompaniesRepository.GetCompanySAPConfig(pCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetCompanySAPConfig :" + ex.InnerException);
                throw ex;
            }
        }

        public QRConfig GetQRConfig(int IdCompany)
        {
            try
            {
                return mCompaniesRepository.GetQRConfig(IdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> GetQRConfig :" + ex.InnerException);
                throw ex;
            }
        }

        public bool AddQRConfig(QRConfig pQRConfig)
        {
            try
            {
                return mCompaniesRepository.AddQRConfig(pQRConfig);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> AddQRConfig :" + ex.InnerException);
                throw ex;
            }
        }

        public bool UpdateQRConfig(QRConfig pQRConfig)
        {
            try
            {
                return mCompaniesRepository.UpdateQRConfig(pQRConfig);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesManager -> UpdateQRConfig :" + ex.InnerException);
                throw ex;
            }
        }
        
    }
}
