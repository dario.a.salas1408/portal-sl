﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class SaleQuotationManager
    {
        SQRepository mSQRepository;
        ItemMasterRepository mOITMRepository;
        GlobalDocumentManager mGlobalDocumentManager;


        public SaleQuotationManager()
        {
            mSQRepository = new SQRepository();
            mOITMRepository = new ItemMasterRepository();
            mGlobalDocumentManager = new GlobalDocumentManager();


        }
        public SalesQuotationSAP GetSaleQuotation(CompanyConn pCc, int Id, int pUserId)
        {
            try
            {
                SalesQuotationSAP mreturn = Mapper.Map<SalesQuotationSAP>(mSQRepository.GetSaleQuotationById(Id, pUserId, pCc));

                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> GetSaleQuotation:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSQ"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SalesQuotationSAP pSQ, CompanyConn pCc)
        {
            try
            {
                JsonObjectResult mJsonObjectResult = mSQRepository.Add(pSQ, pCc);

                if (mJsonObjectResult.ServiceAnswer == Enums.ServiceResult.Ok.ToDescriptionString())
                {
                    //If code is null, the order entry in approval process.
                    if (!string.IsNullOrEmpty(mJsonObjectResult.Code))
                    {
                        mJsonObjectResult.RedirectUrl = "/Sales/SalesQuotation/ActionPurchaseOrder?ActionPurchaseOrder=Update&IdPO={Code}&fromController={From}";
                        mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{Code}", mJsonObjectResult.Code);
                        mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{From}", "SalesQuotation");
                    }
                }

                return mJsonObjectResult; 
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public string Update(SalesQuotationSAP pSQ, CompanyConn pCc)
        {
            try
            {
                return mSQRepository.Update(pSQ, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesQuotationListSearch(CompanyConn pCc, string pCodeVendor = "", 
            DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", 
            string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mSQRepository.GetSalesQuotationListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> GetSalesQuotationListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
        public List<SalesQuotationSAPLine> GetSalesQuotationLinesSearch(CompanyConn pCc, string[] pDocuments)
        {
            try
            {
                return mSQRepository.GetSalesQuotationLinesSearch(pCc, pDocuments);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> GetSalesQuotationLinesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pSQ"></param>
        /// <returns></returns>
        public SalesQuotationSAP GetSQInternalObjects(CompanyConn pCc, SalesQuotationSAP pSQ)
        {
            try
            {
                DocumentSAPCombo mDocSAPCombo = mSQRepository.GetDocumentSAPCombo(pCc);
                pSQ.ListDocumentSAPCombo = mDocSAPCombo;
                return pSQ;

            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> GetSQInternalObjects:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocEntry"></param>
        /// <returns></returns>
        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            try
            {
                return mSQRepository.GetFreights(pCc, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> GetFreights:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="mListCurrency"></param>
        /// <param name="DocDate"></param>
        /// <param name="pLocalCurrency"></param>
        /// <param name="pSystemCurrency"></param>
        /// <returns></returns>
        public List<RatesSystem> GetListRates(CompanyConn pCc, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mGlobalDocumentManager.GetListRatesSystem(pCc, mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleQuotationManager -> GetListRates:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
