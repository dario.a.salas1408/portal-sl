﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class VariableManager
    {
         private VariableRepository mVarRep;

         public VariableManager()
        {
            mVarRep = new VariableRepository();
        }

         public List<ARGNSVariable> GetVariableListSearch(CompanyConn pCc, ref List<ARGNSSegmentation> pSegmentationList, string pVarCode = "", string pVarName = "", string pVarSegmentation = "")
        {
            try
            {
                return mVarRep.GetVariableListSearch(pCc, ref pSegmentationList, pVarCode, pVarName,pVarSegmentation).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ARGNSSegmentation> GetSegmentationList(CompanyConn pCc)
        {
            try
            {
                return mVarRep.GetSegmentationList(pCc).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
