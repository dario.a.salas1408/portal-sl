﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class ProjectSAPManager
    {
        private ProjectSAPRepository mProjectSAPRepository;

        public ProjectSAPManager()
        {
            mProjectSAPRepository = new ProjectSAPRepository();
        }

        public List<ProjectSAP> GetProjectsSAPList(CompanyConn pCc, string pProjectCode, string pProjectName)
        {
            try
            {
                return mProjectSAPRepository.GetProjectsSAPList(pCc, pProjectCode, pProjectName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
