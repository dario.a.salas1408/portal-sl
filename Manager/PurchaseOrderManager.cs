﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Repository;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using AutoMapper;
using ARGNS.Util;
using ARGNS.Model.Implementations.View;

namespace ARGNS.Manager
{
    public class PurchaseOrderManager
    {
        PORepository mPORepository;
        ItemMasterRepository mOITMRepository;
        GlobalDocumentManager mGlobalDocumentManager;

        public PurchaseOrderManager()
        {
            mPORepository = new PORepository();
            mOITMRepository = new ItemMasterRepository();
            mGlobalDocumentManager = new GlobalDocumentManager();

        }
        public PurchaseOrderSAP GetPurchaseOrder(CompanyConn pCc, int Id,int pUserId)
        {
            try
            {
                PurchaseOrderSAP mreturn = Mapper.Map<PurchaseOrderSAP>(mPORepository.GetPurchaseOrderById(Id, pUserId, pCc));

                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> GetPurchaseOrder:" + ex.InnerException);
                throw ex;
            }
        }

        public PurchaseOrderSAP GetPOInternalObjects(CompanyConn pCc, PurchaseOrderSAP pPO)
        {
            try
            {
                DocumentSAPCombo mDocSAPCombo = mPORepository.GetDocumentSAPCombo(pCc);
                pPO.ListDocumentSAPCombo = mDocSAPCombo;
                return pPO;

            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> GetPOInternalObjects:" + ex.InnerException);
                throw ex;
            }
        }

        public string Add(PurchaseOrderSAP pPO, CompanyConn pCc)
        {
            try
            {
                return mPORepository.Add(pPO, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public string Update(PurchaseOrderSAP pPO, CompanyConn pCc)
        {
            try
            {
                return mPORepository.Update(pPO, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mPORepository.GetPurchaseOrderListSearch(pCc, pCodeVendor, pDate, pDocNum,pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> GetPurchaseOrders:" + ex.InnerException);
                throw ex;
            }
        }

        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            try
            {
                return mPORepository.GetFreights(pCc, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> GetFreights:" + ex.InnerException);
                throw ex;
            }
        }

        public List<RatesSystem> GetListRates(CompanyConn pCc, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mGlobalDocumentManager.GetListRatesSystem(pCc, mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> GetListRates:" + ex.InnerException);
                throw ex;
            } 
        }

    }
}
