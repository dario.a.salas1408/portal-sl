﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Manager
{
	public class AreaKeyManager
    {
        private AreaKeyRepository mAreaKeyRepository;
        private Security mSecurity;

        public AreaKeyManager()
        {
            mAreaKeyRepository = new AreaKeyRepository();
            mSecurity = new Security();
        }

        public List<AreaKey> GetAreasAllowed()
        {
            try
            {
                List<AreaKey> mReturn = new List<AreaKey>();
                
                foreach (AreaKey item in mAreaKeyRepository.GetAreasKey())
                {
                    List<Page> mPage = new List<Page>();    

                    if (mSecurity.CkeckKey(item.KeyNumber.Trim(), item.Area.Trim()))
                    {
                        foreach (Page itemPage in item.Pages.Where(c => c.IdAreaKeys == item.IdAreaKeys))
                        {
                            bool UseSap = true;
                            if (mSecurity.CkeckPage(itemPage.KeyAdd, itemPage.IdPage, out UseSap) || itemPage.Description == "Inventory Status")
                            {
                                mPage.Add(itemPage);
                            }
                        }
                        item.Pages = mPage;
                        mReturn.Add(item);
                    }
                }

                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("AreaKeyManager -> GetAreasAllowed:" + ex.InnerException);
                throw ex;
            }

        }

        public bool GetAreasAccess(Enums.Areas pArea)
        {
            try
            {

                return true;

                //AreaKey mAreaKey = mAreaKeyRepository.GetAreaKey(pArea.ToDescriptionString());

                //mSecurity = new Security();

                //return mSecurity.CkeckKey(mAreaKey.KeyNumber, pArea.ToDescriptionString());

            }
            catch (Exception ex)
            {
                Logger.WriteError("AreaKeyManager -> GetAreasAccess: " + ex.InnerException);
                return false;
            }

        }

        public AreaKey GetAreaById(int pId)
        {
            try
            {
                AreaKey mAreaKey = mAreaKeyRepository.GetAreaById(pId);

                mSecurity = new Security();

                return mAreaKey;

            }
            catch (Exception ex)
            {
                Logger.WriteError("AreaKeyManager -> GetAreaById: " + ex.InnerException);
                return null;
            }

        }

        public List<AreaKey> GetAreasKeyList()
        {
            return mAreaKeyRepository.GetAreasKeyList();

        }

    }
}
