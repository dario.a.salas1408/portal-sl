﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class MyStylesManager
	{
		MyStylesRepository mMSRepository;

		public MyStylesManager()
		{
			mMSRepository = new MyStylesRepository();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pUserCode"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <returns></returns>
        public JsonObjectResult GetUserPDMListSearch(CompanyConn pCc, string pUserCode,
            ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "",
            string pNameModel = "", string pSeasonModel = "",
            string pGroupModel = "", string pBrand = "",
            string pCollection = "", string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            try
            {
                return mMSRepository.GetUserPDMListSearch(pCc, pUserCode,
                    ref mPDMSAPCombo, pCodeModel, pNameModel,
                    pSeasonModel, pGroupModel, pBrand,
                    pCollection, pSubCollection,
                    pStart, pLength);
            }
            catch (Exception ex)
            {
                Logger.WriteError("MyStylesManager -> GetUserPDMListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pBPCode"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <returns></returns>
        public List<ARGNSModel> GetBPPDMListSearch(CompanyConn pCc, 
			string pBPCode, ref PDMSAPCombo mPDMSAPCombo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "", 
			int pStart = 0, int pLength = 0)
		{
			try
			{
				return Mapper.Map<List<ARGNSModel>>(
					mMSRepository.GetBPPDMListSearch(pCc, pBPCode, ref mPDMSAPCombo, 
					pCodeModel, pNameModel, pSeasonModel, pGroupModel, 
					pBrand, pCollection, pSubCollection, pStart, pLength));
			}
			catch (Exception ex)
			{
				Logger.WriteError("MyStylesManager -> GetBPPDMListSearch:" + ex.InnerException);
				throw ex;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCc"></param>
		/// <param name="pUserCode"></param>
		/// <param name="txtStyle"></param>
		/// <param name="txtCodeCostSheet"></param>
		/// <param name="txtDescription"></param>
		/// <returns></returns>
		public List<ARGNSCostSheet> GetUserCostSheetListSearch(CompanyConn pCc, string pUserCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
		{
			try
			{

				return Mapper.Map<List<ARGNSCostSheet>>(mMSRepository.GetUserCostSheetListSearch(pCc, pUserCode, txtStyle, txtCodeCostSheet, txtDescription));
			}
			catch (Exception ex)
			{
				Logger.WriteError("MyStylesManager -> GetUserCostSheetListSearch:" + ex.InnerException);
				throw ex;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCc"></param>
		/// <param name="pBPCode"></param>
		/// <param name="txtStyle"></param>
		/// <param name="txtCodeCostSheet"></param>
		/// <param name="txtDescription"></param>
		/// <returns></returns>
		public List<ARGNSCostSheet> GetBPCostSheetListSearch(CompanyConn pCc, string pBPCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
		{
			try
			{

				return Mapper.Map<List<ARGNSCostSheet>>(mMSRepository.GetBPCostSheetListSearch(pCc, pBPCode, txtStyle, txtCodeCostSheet, txtDescription));
			}
			catch (Exception ex)
			{
				Logger.WriteError("MyStylesManager -> GetBPCostSheetListSearch:" + ex.InnerException);
				throw ex;
			}
		}

		//public PDMSAPCombo GetPDMSAPCombo(CompanyConn pCc)
		//{
		//    try
		//    {
		//        mMSRepository = new MyStylesRepository();
		//        return mMSRepository.GetPDMSAPCombo(pCc);
		//    }
		//    catch (Exception ex)
		//    {
		//        Logger.WriteError("MyStylesManager -> getPDMSAPCombo:" + ex.InnerException);
		//        throw ex;
		//    }
		//}
	}
}
