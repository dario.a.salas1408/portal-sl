﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class ScaleMasterManager
    {
        private ScaleMasterRepository mScaleRep;

        public ScaleMasterManager()
        {
            mScaleRep = new ScaleMasterRepository();
        }

        public List<ARGNSScale> GetColorListSearch(CompanyConn pCc, string pScaleCode = "", string pScaleName = "")
        {
            try
            {
                return mScaleRep.GetScaleListSearch(pCc, pScaleCode, pScaleName).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ARGNSSize> GetScaleDescriptionListSearch(CompanyConn pCc, string pScaleCode = "")
        {
            try
            {
                return mScaleRep.GetScaleDescriptionListSearch(pCc, pScaleCode).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ARGNSSize> GetSizeByScale(CompanyConn pCc, string pScaleCode = "", string pModeCode = "")
        {
            try
            {
                return mScaleRep.GetSizeByScale(pCc, pScaleCode, pModeCode).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
