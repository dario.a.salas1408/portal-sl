﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using AutoMapper;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class ActivitiesManager
    {
        ActivitiesRepository mARepository;


        public ActivitiesManager()
        {
            mARepository = new ActivitiesRepository();
        }
        public ActivitiesSAP GetActivity(CompanyConn pCc, int Id, bool CheckApparelInstallation)
        {
            try
            {
                return Mapper.Map<ActivitiesSAP>(mARepository.GetActivityById(Id, pCc, CheckApparelInstallation));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetActivity:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetActivities(CompanyConn pCc)
        {
            try
            {
                return Mapper.Map<List<ActivitiesSAP>>(mARepository.GetActivitiesList(pCc));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetActivities:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetActivitiesSearch(CompanyConn pCc, int UserType, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            try
            {
                return mARepository.GetActivitiesListSearch(pCc, UserType, pModel, pProject, txtStatus, txtRemarks);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetActivitiesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivityStatusSAP> GetActivityStatusList(CompanyConn pCc)
        {
            try
            {
                return mARepository.GetActivityStatusList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetActivityStatusList:" + ex.InnerException);
                throw ex;
            }
        }


        public string Add(ActivitiesSAP pActivities, CompanyConn pCc)
        {
            try
            {
                return mARepository.Add(pActivities, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public string Update(ActivitiesSAP pActivities, CompanyConn pCc)
        {
            try
            {
                return mARepository.Update(pActivities, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public void UpdateSalesOpportunitiesActivity(List<ActivitiesSAP> pActList, int? pOpporId, short? pLine, CompanyConn pCompanyParam)
        {
            try
            {
                foreach (var item in pActList)
                {
                    mARepository.UpdateSalesOpportunitiesActivity(item.ClgCode, pOpporId, pLine, pCompanyParam);
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> UpdateSalesOpportunitiesActivity:" + ex.InnerException);
                throw;
            }
        }

        public List<ActivitiesSAP> GetCRMActivitiesSearch(CompanyConn pCc, int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode)
        {
            try
            {
                return mARepository.GetCRMActivitiesListSearch(pCc, UserType, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks, txtBPCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetCRMActivitiesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public ActivitiesSAPCombo GetActivityCombo(CompanyConn pCompanyParam)
        {
            try
            {
                return mARepository.GetActivityCombo(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetActivityCombo:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetBPCRMActivitiesSearch(CompanyConn pCc, int UserType, string pBPCode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            try
            {
                return mARepository.GetBPCRMActivitiesSearch(pCc, UserType, pBPCode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetBPCRMActivitiesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetUserCRMActivitiesSearch(CompanyConn pCc, int UserType, int pUserId, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            try
            {
                return mARepository.GetUserCRMActivitiesSearch(pCc, UserType, pUserId, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetUserCRMActivitiesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetSalesEmployeeCRMActivitiesSeach(CompanyConn pCc, int UserType, int pSECode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            try
            {
                return mARepository.GetSalesEmployeeCRMActivitiesSeach(pCc, UserType, pSECode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetSalesEmployeeCRMActivitiesSeach:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetCRMActivitiesListFromTo(CompanyConn pCc,  DateTime? txtStartDate, DateTime? txtEndDate, int pUserId)
        {
            try
            {
                return mARepository.GetCRMActivitiesListFromTo(pCc, txtStartDate, txtEndDate, pUserId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetCRMActivitiesListFromTo:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetBPMyActivitiesSearch(CompanyConn pCc, int UserType, string pBPCode, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            try
            {
                return mARepository.GetBPMyActivitiesSearch(pCc, UserType, pBPCode, txtModel, txtProject, txtStatus, txtRemarks);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetBPMyActivitiesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ActivitiesSAP> GetUserMyActivitiesSearch(CompanyConn pCc, int UserType, int pUserId, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            try
            {
                return mARepository.GetUserMyActivitiesSearch(pCc, UserType, pUserId, txtModel, txtProject, txtStatus, txtRemarks);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivitiesManager -> GetUserMyActivitiesSearch:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
