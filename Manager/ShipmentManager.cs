﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class ShipmentManager
    {
        ShipmentRepository mShipmentRepository;

        public ShipmentManager()
        {
            mShipmentRepository = new ShipmentRepository();
        }

        public List<ARGNSContainer> GetShipmentListSearch(CompanyConn pCc, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            try
            {
                return mShipmentRepository.GetShipmentListSearch(pCc, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetShipmentListSearch: " + ex.InnerException);
                throw ex;
            }

        }

        public List<ARGNSContainer> GetMyShipmentListSearchBP(CompanyConn pCc, string pBP, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            try
            {
                return mShipmentRepository.GetMyShipmentListSearchBP(pCc, pBP, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetShipmentListSearch: " + ex.InnerException);
                throw ex;
            }

        }
        

        public ARGNSContainer GetShipmentByCode(CompanyConn pCc, int pShipmentID)
        {
            try
            {
                return mShipmentRepository.GetShipmentByCode(pCc, pShipmentID);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetShipmentByCode: " + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSContainerPackage> GetContainerPackageList(CompanyConn pCc, int pShipmentID)
        {
            try
            {
                return mShipmentRepository.GetContainerPackageList(pCc, pShipmentID);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetContainerPackageList: " + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSContainerSetup> GetContainerSetupList(CompanyConn pCc)
        {
            try
            {
                return mShipmentRepository.GetContainerSetupList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetContainerSetupList: " + ex.InnerException);
                throw ex;
            }
        }

        public string Update(CompanyConn pCc, ARGNSContainer pShipment)
        {
            try
            {
                return mShipmentRepository.Update(pCc, pShipment);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> Update: " + ex.InnerException);
                throw ex;
            }
        }

        public string Add(CompanyConn pCc, ARGNSContainer pShipment)
        {
            try
            {
                return mShipmentRepository.Add(pCc, pShipment);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> Add: " + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSPSlipPackage> GetPSlipPackageList(CompanyConn pCc, int pContainerID, int pShipmentID)
        {
            try
            {
                return mShipmentRepository.GetPSlipPackageList(pCc, pContainerID, pShipmentID);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetPSlipPackageList: " + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSPSlipPackageType> GetPSlipPackageTypeList(CompanyConn pCc)
        {
            try
            {
                return mShipmentRepository.GetPSlipPackageTypeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetPSlipPackageTypeList: " + ex.InnerException);
                throw ex;
            }
        }

        public List<Draft> GetDocumentWhitOpenLines(CompanyConn pCc, string pObjType)
        {
            try
            {
                return mShipmentRepository.GetDocumentWhitOpenLines(pCc, pObjType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetDocumentWhitOpenLines: " + ex.InnerException);
                throw ex;
            }
        }

        public List<DraftLine> GetDocumentOpenLines(CompanyConn pCc, string pObjType, List<int> pDocEntryList)
        {
            try
            {
                return mShipmentRepository.GetDocumentOpenLines(pCc, pObjType, pDocEntryList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetDocumentOpenLines: " + ex.InnerException);
                throw ex;
            }
        }

        public List<Draft> GetShipmentDocument(CompanyConn pCc, string pShipmentID, string pObjType)
        {
            try
            {
                return mShipmentRepository.GetShipmentDocument(pCc, pShipmentID, pObjType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetShipmentDocument: " + ex.InnerException);
                throw ex;
            }
        }

        public ShipmentMembers GetShipmentMembersObject(CompanyConn pCc)
        {
            try
            {
                return mShipmentRepository.GetShipmentMembersObject(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ShipmentManager -> GetShipmentMembersObject: " + ex.InnerException);
                throw ex;
            }
        }
    }
}
