﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ARGNS.Manager
{
	public class ColorManager
    {
        private ColorRepository mColorRep;

        public ColorManager()
        {
            mColorRep = new ColorRepository();          
        }

        public ARGNSColor GetColorByCode(CompanyConn pCc, string pColorCode)
        {
            try
            {
                return mColorRep.GetColorByCode(pCc, pColorCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARGNSColor> GetColorListSearch(CompanyConn pCc, string pColorCode = "", string pColorName = "")
        {
            try
            {
                return mColorRep.GetColorListSearch(pCc, pColorCode, pColorName).ToList();
            }
            catch (Exception)
            {                
                throw;
            } 
        }

        public string Add(CompanyConn pCc, ARGNSColor pColor)
        {
            try
            {
                return mColorRep.Add(pCc, pColor);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ColorManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public string Update(CompanyConn pCc, ARGNSColor pColor)
        {
            try
            {
                return mColorRep.Update(pCc, pColor);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ColorManager -> Update:" + ex.InnerException);
                throw;
            }
        }

    }
}
