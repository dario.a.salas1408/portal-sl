﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class SalesInvoiceManager
    {
        SIRepository mSIRepository;

        public SalesInvoiceManager()
        {
            mSIRepository = new SIRepository();

        }
        public SalesInvoiceSAP GetSalesInvoice(CompanyConn pCc, int Id, int pUserId)
        {
            try
            {
                SalesInvoiceSAP mreturn = Mapper.Map<SalesInvoiceSAP>(mSIRepository.GetSalesInvoiceById(Id,pUserId, pCc));
                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesInvoiceManager -> GetSalesInvoice:" + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetSalesInvoiceListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mSIRepository.GetSalesInvoiceListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesInvoiceManager -> GetSalesInvoiceListSearch:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
