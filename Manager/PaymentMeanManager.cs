﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class PaymentMeanManager
    {
        PaymentMeanRepository paymentMeanRepository;

        public PaymentMeanManager()
        {
            paymentMeanRepository = new PaymentMeanRepository();
        }

        public List<PaymentMean> GetPaymentMeans(CompanyConn companyConn, string branchCode)
        {
            try
            {
                var paymentMeans = paymentMeanRepository.GetPaymentMeans(companyConn, branchCode);
                
                return paymentMeans;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PaymentMeanManager -> GetPaymentMeans:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PaymentMeanType> GetPaymentMeanType(CompanyConn companyParam)
        {
            try
            {
                var paymentMeanType = paymentMeanRepository.GetPaymentMeanType(companyParam);

                return paymentMeanType;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PaymentMeanManager -> GetPaymentMeanType:" + ex.InnerException);
                throw ex;
            }
        }


        public List<PaymentMeanOrder> GetForDraft(CompanyConn companyParam, int code)
        {
            try
            {
                var paymentMeanType = paymentMeanRepository.GetForDraft(companyParam, code);

                return paymentMeanType;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PaymentMeanManager -> GetForDraft:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
