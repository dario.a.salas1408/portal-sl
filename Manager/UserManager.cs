﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class UserManager
    {
        UserRepository mUserRepository;
        public UserManager()
        {
            mUserRepository = new UserRepository();
        }

        public UserManager GetUsuario()
        {
            try
            {

                WebPortalModel Db = new WebPortalModel();

                Db.News.Add(new Model.Implementations.News { Description = "" });

                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> GetUsuario:" + ex.InnerException);
                throw ex;
            }
        }

        public User GetUsuario(int Id)
        {
            try
            {
                return mUserRepository.GetUser(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> GetUsuario:" + ex.InnerException);
                throw ex;
            }
        }

        public User Signin(string pUserName, string pPsw)
        {
            try
            {
                return mUserRepository.Signin(pUserName, pPsw);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> Signin:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <returns></returns>
        public JsonObjectResult GetUsuarios(int pStart, int pLength, string pUserName)
        {
            try
            {
                return mUserRepository.GetListUser(pStart, pLength, pUserName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> GetUsuarios:" + ex.Message);
                Logger.WriteError("UserManager -> GetUsuarios:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public bool Add(User pUser)
        {
            try
            {
                return mUserRepository.Add(pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(User pUser)
        {
            try
            {
                return mUserRepository.Update(pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> Update:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Delete(User pUser)
        {
            try
            {
                return mUserRepository.Delete(pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }

        public bool AddCompanyToUser(int IdUser, int IdCompany, string UserNameSAP, string UserPasswordSAP, string UserCodeSAP, string BPCode, int? SECode, string DftDistRule, string DftWhs, string pSalesTaxCodeDef, string pPurchaseTaxCodeDef, int? pUserGroupId, int? pCostPriceList, int? pBPGroupId)
        {

            try
            {
                return mUserRepository.AddCompanyToUser(IdUser, IdCompany, UserNameSAP, UserPasswordSAP, UserCodeSAP, BPCode, SECode, DftDistRule, DftWhs, pSalesTaxCodeDef, pPurchaseTaxCodeDef, pUserGroupId, pCostPriceList, pBPGroupId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> AddCompanyToUser:" + ex.InnerException);

                throw ex;
            }

        }

        public bool DeleteCompanyToUser(int IdUser, int IdCompany)
        {
            try
            {
                return mUserRepository.DeleteCompanyToUser(IdUser, IdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> DeleteCompanyToUser:" + ex.InnerException);

                throw ex;
            }
        }

        public bool UpdateMyAccount(User pUser)
        {
            try
            {
                return mUserRepository.UpdateMyAccount(pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> UpdateMyAccount:" + ex.InnerException);
                throw ex;
            }
        }

        public List<SAPObject> GetSAPObjects()
        {
            try
            {
                return mUserRepository.GetSAPObjects();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> GetSAPObjects:" + ex.InnerException);
                throw ex;
            }
        }

        public bool AddUDF(int pUserID, int pCompanyId, string pTableID, string pUDFName, int pFieldID, string pTypeID, string pUDFDesc, string pRTable)
        {
            try
            {
                return mUserRepository.AddUDF(pUserID, pCompanyId, pTableID, pUDFName, pFieldID, pTypeID, pUDFDesc, pRTable);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> AddUDF:" + ex.InnerException);
                throw ex;
            }
        }

        public bool DeleteUDF(int IdUser, int IdUDF)
        {
            try
            {
                return mUserRepository.DeleteUDF(IdUser, IdUDF);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> DeleteUDF:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUserId"></param>
        /// <param name="pNewUserName"></param>
        /// <param name="pNewUserPassword"></param>
        /// <returns></returns>
        public bool Duplicate(int pUserId, string pNewUserName, 
            string pNewUserPassword)
        {
            try
            {
                return mUserRepository.Duplicate(pUserId,
                    pNewUserName, pNewUserPassword);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> Duplicate:" + 
                    ex.InnerException);
                return false; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool ValidateExistingUser(string userName)
        {
            try
            {
                return mUserRepository.UserAlreadyExists(userName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserManager -> ValidateExistingUser:" +
                    ex.InnerException);
                return false;
            }
        }
    }
}
