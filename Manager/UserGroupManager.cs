﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class UserGroupManager
    {
        private UserGroupRepository mUserGroupRepository;
        public UserGroupManager()
        {
            mUserGroupRepository = new UserGroupRepository();
        }

        public UserGroup GetUserGroup(int Id)
        {
            try
            {
                return mUserGroupRepository.GetUserGroup(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserGroupManager -> GetUserGroup:" + ex.InnerException);
                throw ex;
            }
        }

        public List<UserGroup> GetUserGroupList(int IdCompany)
        {
            try
            {
                return mUserGroupRepository.GetUserGroupList(IdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserGroupManager -> GetUserGroupList:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Add(UserGroup pUserGroup)
        {
            try
            {
                return mUserGroupRepository.Add(pUserGroup);
            }
            catch (Exception ex)
            {

                Logger.WriteError("UserGroupManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(UserGroup pUserGroup)
        {
            try
            {
                return mUserGroupRepository.Update(pUserGroup);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserGroupManager -> Update:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
