﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class LogoManager
    {
        private LogoRepository mLogoRepository;

        public LogoManager()
        {
            mLogoRepository = new LogoRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public List<ARGNSModelLogo> GetLogoList(CompanyConn pCc, string pCode)
        {
            try
            {
                return mLogoRepository.GetLogoList(pCc, pCode).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ARGNSLogo GetLogoById(CompanyConn pCc, string pCode)
        {
            try
            {
                return mLogoRepository.GetLogoById(pCc, pCode);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
