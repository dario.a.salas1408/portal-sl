﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class ProjectManager
    {
        private ProjectRepository mProjectRepository;

        public ProjectManager()
        {
            mProjectRepository = new ProjectRepository();
        }

        public ARGNSProject GetProject(CompanyConn pCc, string pProjectCode, string pModelId)
        {
            try
            {
                return mProjectRepository.GetProject(pCc, pProjectCode, pModelId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProjectManager -> GetProject:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSRoutingLine> GetWorkflowLines(CompanyConn pCc, string pWorkflow)
        {
            try
            {
                return mProjectRepository.GetWorkflowLines(pCc, pWorkflow);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProjectManager -> GetWorkflowLines:" + ex.InnerException);
                throw ex;
            }
        }

        public string GetCriticalPathDefaultBP(CompanyConn pCc)
        {
            try
            {
                return mProjectRepository.GetCriticalPathDefaultBP(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProjectManager -> GetCriticalPathDefaultBP:" + ex.InnerException);
                throw ex;
            }
        }

        
        public string CreateActivities(CompanyConn pCc, List<ActivitiesSAP> pListActivities)
        {
            try
            {
                return mProjectRepository.CreateActivities(pCc, pListActivities);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProjectManager -> CreateActivities:" + ex.InnerException);
                throw ex;
            }
        }

        public string Update(CompanyConn pCc, ARGNSProject pProject)
        {
            try
            {
                return mProjectRepository.Update(pCc, pProject);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProjectManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public JsonObjectResult Add(CompanyConn pCc, ARGNSProject pProject)
        {
            try
            {
                return mProjectRepository.Add(pCc, pProject);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProjectManager -> Add:" + ex.InnerException);
                throw;
            }
        }


    }
}
