﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class ChartManager
    {
        ChartRepository mChartRepository;

        public ChartManager()
        {
            mChartRepository = new ChartRepository();
        }

        public List<PortalChart> GetPortalChartList(int pIdCompany)
        {
            try
            {
                List<PortalChart> mPortalChartList = mChartRepository.GetPortalChartList(pIdCompany);

                return mPortalChartList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> GetPortalChartList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalChart> GetPortalChartListSearch(string pPortalPage, int pIdCompany, UserGroup pUserGroupI)
        {
            try
            {
                List<PortalChart> mPortalChartList = mChartRepository.GetPortalChartListSearch(pPortalPage, pIdCompany,pUserGroupI);

                return mPortalChartList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> GetPortalChartList:" + ex.InnerException);
                throw ex;
            }
        }

        public PortalChart GetPortalChartById(CompanyConn pCc, int pChartId)
        {
            try
            {
                PortalChart mPortalChart = new PortalChart();
                mPortalChart = mChartRepository.GetPortalChartById(pCc, pChartId);

                return mPortalChart;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> GetPortalChartById:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalChartType> GetPortalChartTypeList(CompanyConn pCc)
        {
            try
            {
                return mChartRepository.GetPortalChartTypeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> GetPortalChartTypeList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalChartUserGroup> GetPortalChartUserGroupList(CompanyConn pCc, int IdChart)
        {
            try
            {
                return mChartRepository.GetPortalChartUserGroupList(pCc, IdChart);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> GetPortalChartUserGroupList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalChartParamsType> GetPortalChartParamsTypeList(CompanyConn pCc)
        {
            try
            {
                return mChartRepository.GetPortalChartParamsTypeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> GetPortalChartParamsTypeList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalChartParamsValueType> GetPortalChartParamsValueTypeList(CompanyConn pCc)
        {
            try
            {
                return mChartRepository.GetPortalChartParamsValueTypeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> GetPortalChartParamsValueTypeList:" + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult Update(PortalChart pPortalChart)
        {
            try
            {
                return mChartRepository.Update(pPortalChart);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public JsonObjectResult Add(PortalChart pPortalChart)
        {
            try
            {
                return mChartRepository.Add(pPortalChart);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public bool Delete(int pIdChartDelete)
        {
            try
            {
                return mChartRepository.Delete(pIdChartDelete);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ChartManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
