﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class UserBranchManager
    {
        private UserBranchRepository userBranchRepository;

        public UserBranchManager()
        {
            userBranchRepository = new UserBranchRepository();
        }

        public UserBranch GetUserBranch(int id)
        {
            try
            {
                return userBranchRepository.GetUserBranch(id);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserBranchManager -> GetUserBranch:" + ex.InnerException);
                throw;
            }
        }

        public List<UserBranch> GetUserBranchList(int idUser, int idCompany)
        {
            try
            {
                return userBranchRepository.GetUserBranchList(idUser, idCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserBranchManager -> GetUserBranchList:" + ex.InnerException);
                throw;
            }
        }

        public bool Add(UserBranch userBranch)
        {
            try
            {
               
                return userBranchRepository.Add(userBranch);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserGroupManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Delete(int idUserBranch)
        {
            try
            {

                return userBranchRepository.Delete(idUserBranch);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserGroupManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }


        public List<BranchesSAP> GetBranchesByUser(CompanyConn pCc, string userSAP)
        {
            try
            {

                return userBranchRepository.GetBranchesByUser(pCc, userSAP);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserGroupManager -> GetBranchesByUser:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
