﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Util;
using ARGNS.Model.Implementations.View;

namespace ARGNS.Manager
{
    public class PDMManager
    {
        PDMRepository mPDMRepository;

        public PDMManager()
        {
            mPDMRepository = new PDMRepository();
        }

        public ARGNSModel GetModel(CompanyConn pCc, string Id, string pModCode, int pUserId)
        {
            try
            {
                ARGNSModel mModel = new ARGNSModel();
                mModel = Mapper.Map<ARGNSModel>(mPDMRepository.GetModelById(Id, pModCode, pUserId, pCc));

                ARGNS.Model.Implementations.PDM.ComboList.ARGNSStyleStatus mStatus = mModel.ListPDMSAPCombo.ListStatus.Where(c => c.Code == "-1").SingleOrDefault();
                if (mStatus != null)
                {
                    mModel.ListPDMSAPCombo.ListStatus.Remove(mStatus);
                }

                return mModel;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModel:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="Id"></param>
        /// <param name="pImgList"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<SkuModel> GetSkuModelList(CompanyConn pCc, string Id,
            ref List<ARGNSModelImg> pImgList)
        {
            try
            {
                return mPDMRepository.GetSkuModelList(pCc, Id, ref pImgList);

            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetSkuModelList:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCode"></param>
        /// <param name="pU_ModCode"></param>
        /// <returns></returns>
        public List<ARGNSModelFile> GetModelFileList(CompanyConn pCc, string pCode, string pU_ModCode)
        {
            try
            {
                return mPDMRepository.GetModelFileList(pCc, pCode, pU_ModCode).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelList:" + ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModel"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public string UpdateModel(ARGNSModel pModel, CompanyConn pCc)
        {
            try
            {
                return mPDMRepository.UpdateModel(pModel, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModel"></param>
        /// <param name="pCc"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        public JsonObjectResult AddModel(ARGNSModel pModel, CompanyConn pCc, string pFrom)
        {
            string ret = string.Empty;
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {

                if (mPDMRepository.GetModelExist(pCc, pModel.U_ModCode))
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = "The Code already exist.";
                    return mJsonObjectResult;
                }

                if (pModel.ModelSegmentation.U_UseCol == "Y" && pModel.ModelColorList.Count == 0)
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = "Please, Select a Color";
                    return mJsonObjectResult;
                }

                if (pModel.ModelSegmentation.U_UseScl == "Y" && pModel.ModelSizeList.Count == 0)
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = "Please, Select a Scale";
                    return mJsonObjectResult;
                }

                if (pModel.ModelSegmentation.U_UseVar == "Y" && pModel.ModelVariableList.Count == 0)
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = "Please, Select a Variable";
                    return mJsonObjectResult;
                }

                if (pModel.U_ModCode.Length > pModel.ModelSegmentation.U_ModLen)
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = "The max defined length for this segment code is " + pModel.ModelSegmentation.U_ModLen.Value.ToString();
                    return mJsonObjectResult;
                }


                mJsonObjectResult = mPDMRepository.AddModel(pModel, pCc);
                if (mJsonObjectResult.ServiceAnswer == Enums.ServiceResult.Ok.ToDescriptionString())
                {
                    mJsonObjectResult.RedirectUrl = "/PLM/PDM/ActionPDM?ActionPDM=UpdateModel&ModelCode={Code}&pU_ModCode={U_ModCode}&From={From}";
                    mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{Code}", mJsonObjectResult.Code);
                    mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{U_ModCode}", mJsonObjectResult.Others.Where(c => c.Key == "U_ModCode").FirstOrDefault().Value);
                    mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{From}", pFrom);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> AddModel:" + ex.InnerException);
            }

            return mJsonObjectResult;
        }

        public List<ARGNSModel> GetPDMListSearch(CompanyConn pCc, ref PDMSAPCombo mPDMSAPCombo,
            string pCodeModel = "", string pNameModel = "",
            string pSeasonModel = "", string pGroupModel = "",
            string pBrand = "", string pCollection = "",
            string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            try
            {
                return Mapper.Map<List<ARGNSModel>>(
                    mPDMRepository.GetPDMListSearch(pCc, ref mPDMSAPCombo, pCodeModel, pNameModel,
                    pSeasonModel, pGroupModel, pBrand, pCollection, pSubCollection, pStart, pLength));
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetPDMListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <param name="pCatalogCode"></param>
        /// <returns></returns>
        public JsonObjectResult GetPDMListDesc(CompanyConn pCc,
            int pStart, int pLength,
            string pCodeModel = "", string pNameModel = "", string pSeasonModel = "",
            string pGroupModel = "", string pBrand = "", string pCollection = "",
            string pSubCollection = "", string pCatalogCode = "")
        {
            try
            {
                return mPDMRepository.GetPDMListDesc(pCc, pStart, pLength,
                    pCodeModel, pNameModel, pSeasonModel, pGroupModel, pBrand,
                    pCollection, pSubCollection, pCatalogCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetPDMListDesc:" + ex.InnerException);
                return new JsonObjectResult();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public ModelDesc GetModelDesc(CompanyConn pCc, string pModCode)
        {
            try
            {
                return mPDMRepository.GetModelDesc(pCc, pModCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelDesc:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public List<ARGNSCrPath> GetModelProjectList(CompanyConn pCc, string pModCode)
        {
            try
            {
                return mPDMRepository.GetModelProjectList(pCc, pModCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelProjectList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSModelPom> GetModelPomList(CompanyConn pCc, string pModCode)
        {
            try
            {
                return mPDMRepository.GetModelPomList(pCc, pModCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelPomList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSModelPomTemplate> GetModelPomTemplateList(CompanyConn pCc, List<string> pSclcodeList, List<string> pPomCodeList)
        {
            try
            {
                return mPDMRepository.GetModelPomTemplateList(pCc, pSclcodeList, pPomCodeList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelPomTemplateList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSPomTemplateLines> GetModelPomTemplateLines(CompanyConn pCc, string pPOMCode)
        {
            try
            {
                return mPDMRepository.GetModelPomTemplateLines(pCc, pPOMCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelPomTemplateLines:" + ex.InnerException);
                throw ex;
            }
        }


        public List<ARGNSCrPath> GetCriticalPathListSearch(CompanyConn pCc, string pCollCode = "", string pSubCollCode = "", string pSeasonCode = "", string pModelCode = "", string pProjectCode = "", string pVendorCode = "", string pCustomerCode = "")
        {
            try
            {
                return mPDMRepository.GetCriticalPathListSearch(pCc, pCollCode, pSubCollCode, pSeasonCode, pModelCode, pProjectCode, pVendorCode, pCustomerCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetCriticalPathListSearch:" + ex.InnerException);
                throw ex;
            }
        }


        public List<ARGNSModelHistory> GetModelHistoryList(CompanyConn pCompanyParam, string pModelCode = "")
        {
            try
            {
                return mPDMRepository.GetModelHistoryList(pCompanyParam, pModelCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelHistoryList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<SAPLogs> GetSAPModelLogs(CompanyConn pCompanyParam, string pModelCode = "", string pLogInst = "")
        {
            try
            {
                return mPDMRepository.GetSAPModelLogs(pCompanyParam, pModelCode, pLogInst);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetSAPModelLogs:" + ex.InnerException);
                throw ex;
            }
        }

        public ARGNSSegmentation GetSegmentationById(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                return mPDMRepository.GetSegmentationById(pCompanyParam, pCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetSegmentationById:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSCatalog> GetCatalogListSearch(CompanyConn pCc, string pCatalogCode = "", string pCatalogName = "", int? pSalesEmployee = null)
        {
            try
            {
                return mPDMRepository.GetCatalogListSearch(pCc, pCatalogCode, pCatalogName, pSalesEmployee);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetCatalogListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSPrepack> GetModelPrepacks(CompanyConn pCc, string ModelId)
        {
            try
            {
                return mPDMRepository.GetModelPrepacks(pCc, ModelId);

            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetModelPrepacks:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSSizeRun> GetSizeRunsByScale(CompanyConn pCc, string pScaleCode)
        {
            try
            {
                return mPDMRepository.GetSizeRunsByScale(pCc, pScaleCode);

            }
            catch (Exception ex)
            {
                Logger.WriteError("PDMManager -> GetSizeRunsByScale:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
