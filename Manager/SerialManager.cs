﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ARGNS.Util.Enums;

namespace ARGNS.Manager
{
    public class SerialManager
    {
        SerialRepository serialRepository;

        public SerialManager()
        {
            serialRepository = new SerialRepository();
        }

        public SerialSAP GetSerialsByDocument(CompanyConn pCompanyParam, int docNum, int line, string whsCode, DocumentsType docType, string itemCode,int idUser, int anio)
        {
            try
            {
                var serials = serialRepository.GetSerialsByDocument(pCompanyParam, docNum, line, whsCode, docType, itemCode, idUser, anio);

                return serials;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SerialManager -> GetSerialsByDocument:" + ex.InnerException);
                throw;
            }
            
        }

    }
}
