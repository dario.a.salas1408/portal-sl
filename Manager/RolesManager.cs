﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class RolesManager
    {
        private RolesRepository mRolesRepository;

        public RolesManager()
        {
            mRolesRepository = new RolesRepository();
        }

        public List<Role> GetRoles()
        {
            try
            {
                List<Role> mReturn = new List<Role>();

                mReturn = mRolesRepository.GetRoles();

                return mReturn.Where(c => c.Active == true ).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("RolesManager -> GetRoles:" + ex.InnerException);
                throw ex;
            }

        }

        public Role GetRol(int IdRol)
        {
            try
            {
                return mRolesRepository.GetRol(IdRol);
            }
            catch (Exception ex)
            {
                Logger.WriteError("RolesManager -> GetRol:" + ex.InnerException);
                throw ex;
            }

        }
    }
}
