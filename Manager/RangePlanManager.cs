﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class RangePlanManager
    {
        RangePlanRepository mRangePlanRepository;

        public RangePlanManager()
        {
            mRangePlanRepository = new RangePlanRepository();
        }

        public List<ARGNSRangePlan> GetRangePlanListSearch(CompanyConn pCompanyParam, ref RangePlanCombo mRangePlanCombo, string pCode = "", string pSeason = "", string pCollection = "", string pEmployee = "")
        {
            try
            {

                return mRangePlanRepository.GetRangePlanListSearch(pCompanyParam, ref mRangePlanCombo, pCode, pSeason, pCollection, pEmployee);
            }
            catch (Exception ex)
            {
                Logger.WriteError("RangePlanManager -> GetRangePlanListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public ARGNSRangePlan GetRangePlanById(CompanyConn pCc, string pCode)
        {
            try
            {
                ARGNSRangePlan mRangePlan = new ARGNSRangePlan();
                mRangePlan = mRangePlanRepository.GetRangePlanById(pCc, pCode);

                return mRangePlan;
            }
            catch (Exception ex)
            {
                Logger.WriteError("RangePlanManager -> GetRangePlanById:" + ex.InnerException);
                throw ex;
            }
        }

        public RangePlanCombo GetRangePlanCombo(CompanyConn pCc)
        {
            try
            {
                return mRangePlanRepository.GetRangePlanCombo(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("RangePlanManager -> GetRangePlanCombo:" + ex.InnerException);
                throw ex;
            }
        }
        
        public string Update(ARGNSRangePlan pRangePlan, CompanyConn pCc)
        {
            try
            {
                return mRangePlanRepository.Update(pRangePlan, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("RangePlanManager -> Update:" + ex.InnerException);
                throw;
            }
        }

    }
}
