﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class SampleManager
    {
        SamplesRepository mSamplesRepository;
        GlobalDocumentManager mGlobalDocumentManager;

        public SampleManager()
        {
            mSamplesRepository = new SamplesRepository();
            mGlobalDocumentManager = new GlobalDocumentManager();
        }

        public List<ARGNSModelSample> GetListSample(CompanyConn pCc, string ItemCode)
        {
            try
            {
                return mSamplesRepository.GetListSample(pCc, ItemCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SampleManager -> GetListSample: " + ex.InnerException);
                throw ex;
            }

        }

        public ARGNSModelSample GetSample(CompanyConn pCc, string SampleCode, string pModCode)
        {
            try
            {
                return mSamplesRepository.GetSample(pCc, SampleCode, pModCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SampleManager -> GetSample: " + ex.InnerException);
                throw ex;
            }
        }


        public string AddSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return mSamplesRepository.AddSample(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SampleManager -> AddSample: " + ex.InnerException);
                throw ex;
            }
        }

        public string UpdateSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return mSamplesRepository.UpdateSample(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SampleManager -> UpdateSample: " + ex.InnerException);
                throw ex;
            }
        }

        public ARGNSModelSample GetSampleByCode(CompanyConn pCc, string SampleCode, string pModCode)
        {
            try
            {
                return mSamplesRepository.GetSampleByCode(pCc, SampleCode, pModCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SampleManager -> GetSampleByCode: " + ex.InnerException);
                throw ex;
            }
        }
    }
}
