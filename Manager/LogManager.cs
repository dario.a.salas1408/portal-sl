﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class LogManager
    {
        LogRepository mLogRepository;

        public LogManager()
        {
            mLogRepository = new LogRepository();
        }

        public List<Log> GetListLogByUser(int pIdUser)
        {
            try
            {
                return mLogRepository.GetListLogByUser(pIdUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
