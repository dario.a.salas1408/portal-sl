﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARGNS.Repository;
using AutoMapper;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using ARGNS.Model.Implementations.View;

namespace ARGNS.Manager
{
    public class PurchaseQuotationManager
    {
        PQRepository mPQRepository;
        GlobalDocumentManager mGlobalDocumentManager;


        public PurchaseQuotationManager()
        {
            mPQRepository = new PQRepository();
            mGlobalDocumentManager = new GlobalDocumentManager();

        }
        public PurchaseQuotationSAP GetPurchaseQuotation(CompanyConn pCc, int Id, int pUserId)
        {
            try
            {
                PurchaseQuotationSAP mreturn = Mapper.Map<PurchaseQuotationSAP>(mPQRepository.GetPurchaseQuotationById(Id, pUserId, pCc));

                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseQuotationManager -> GetPurchaseQuotation:" + ex.InnerException);
                throw ex;
            }
        }

        public string Add(PurchaseQuotationSAP pPQ, CompanyConn pCc)
        {
            try
            {
                return mPQRepository.Add(pPQ, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseQuotationManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public string Update(PurchaseQuotationSAP pPQ, CompanyConn pCc)
        {
            try
            {
                return mPQRepository.Update(pPQ, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseQuotationManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public JsonObjectResult GetPurchaseQuotationListSearch(CompanyConn pCc, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mPQRepository.GetPurchaseQuotationListSearch(pCc, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseQuotationManager -> GetPurchaseQuotationListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PurchaseQuotationSAPLine> GetPurchaseQuotationLinesSearch(CompanyConn pCc, string[] pDocuments)
        {
            try
            {
                return mPQRepository.GetPurchaseQuotationLinesSearch(pCc, pDocuments);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseQuotationManager -> GetPurchaseQuotationLinesSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public PurchaseQuotationSAP GetPQInternalObjects(CompanyConn pCc, PurchaseQuotationSAP pPQ)
        {
            try
            {
                DocumentSAPCombo mDocSAPCombo = mPQRepository.GetDocumentSAPCombo(pCc);
                pPQ.ListDocumentSAPCombo = mDocSAPCombo;
                return pPQ;

            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderManager -> GetPOInternalObjects:" + ex.InnerException);
                throw ex;
            }
        }

        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            try
            {
                return mPQRepository.GetFreights(pCc, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseQuotationManager -> GetFreights:" + ex.InnerException);
                throw ex;
            }
        }

        public List<RatesSystem> GetListRates(CompanyConn pCc, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mGlobalDocumentManager.GetListRatesSystem(pCc, mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseQuotationManager -> GetListRates:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
