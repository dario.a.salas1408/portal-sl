﻿using ARGNS.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.DiverSO;
using static ARGNS.Util.Enums;

namespace ARGNS.Manager
{
    public class ItemMasterManager
    {
        private ItemMasterRepository mItemMasterRepository;

        public ItemMasterManager()
        {
            mItemMasterRepository = new ItemMasterRepository();
        }


        public DiverSOResultItems GetDiverSOItems(string pItemCode, string pCardCode, DiverSOItemMethod pType)
        {
            try
            {
                return mItemMasterRepository.GetDiverSOItems(pItemCode, pCardCode, pType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetDiverSOItems :" + ex.Message);
                Logger.WriteError("ItemMasterManager -> GetDiverSOItems :" + ex.InnerException.Message);

                return null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pModCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetOITMListBy(CompanyConn pCc,
            bool pCheckApparelInstallation,
            string pPrchseItem, string pSellItem,
            string pInventoryItem,
            string pItemsWithStock,
            string pItemGroup,
            List<UDF_ARGNS> pMappedUdf, string pModCode,
            int pUserId, string pItemCode, string pItemName,
            bool pOnlyActiveItems,
            List<WarehousePortal> pWarehousePortalList,
            bool pCkCatalogueNum = false, string pBPCode = "",
            string pBPCatalogCode = "", int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mItemMasterRepository.GetOITMListBy(pCc,
                    pCheckApparelInstallation,
                    pPrchseItem, pSellItem, pInventoryItem,
                    pItemsWithStock, pItemGroup, pModCode,
                    pUserId, pMappedUdf, pItemCode,
                    pItemName, pOnlyActiveItems,
                    pWarehousePortalList,
                    pCkCatalogueNum, pBPCode, pBPCatalogCode,
                    pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetOITMListBy :" + ex.Message);
                Logger.WriteError("ItemMasterManager -> GetOITMListBy :" + ex.InnerException.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pListItems"></param>
        /// <param name="pItemType"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetOITMListByList(CompanyConn pCc, List<string> pListItems, string pItemType)
        {
            try
            {
                return mItemMasterRepository.GetOITMListByList(pCc, pListItems, pItemType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetOITMListByList :" + ex.Message);
                Logger.WriteError("ItemMasterManager -> GetOITMListByList :" + ex.InnerException.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pListItems"></param>
        /// <param name="pItemType"></param>
        /// <returns></returns>
        public StockModel GetStockByModel(CompanyConn pCompanyParam, string pModelCode,
           List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                return mItemMasterRepository.GetStockByModel(pCompanyParam, pModelCode, pWarehousePortalList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetOITMListByList :" + ex.Message);
                Logger.WriteError("ItemMasterManager -> GetOITMListByList :" + ex.InnerException.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pModCode"></param>
        /// <param name="pUserId"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="uCardCode"></param>
        /// <param name="pItemGroupCode"></param>
        /// <param name="pCostPriceList"></param>
        /// <param name="pNoItemsWithZeroStockCk"></param>
        /// <param name="pSupplierCardCode"></param>
        /// <param name="pItemList"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetQuickOrderListBy(CompanyConn pCc, bool pCheckApparelInstallation,
            string pPrchseItem, string pSellItem, List<UDF_ARGNS> pMappedUdf,
            string pModCode, int pUserId, string pItemCode, string pItemName,
            string uCardCode, short pItemGroupCode, int? pCostPriceList,
            bool pOnlyActiveItems, bool isSo,
            bool pNoItemsWithZeroStockCk = false, string pSupplierCardCode = "",
            List<string> pItemList = null, int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                return mItemMasterRepository.GetQuickOrderListBy(pCc, pCheckApparelInstallation,
                    pPrchseItem, pSellItem, pModCode, pUserId,
                    pCostPriceList, pMappedUdf, pItemCode,
                    pItemName, uCardCode, pItemGroupCode, pOnlyActiveItems, isSo,
                    null, pNoItemsWithZeroStockCk, pSupplierCardCode,
                    pItemList, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetQuickOrderListBy :" + ex.InnerException.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="PrchseItem"></param>
        /// <param name="SellItem"></param>
        /// <param name="pCodeBar"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCodeBar(CompanyConn pCc, string PrchseItem, string SellItem, string pCodeBar)
        {
            try
            {
                return mItemMasterRepository.GeItemByCodeBar(pCc, PrchseItem, SellItem, pCodeBar);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GeItemByCodeBar :" + ex.InnerException.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCode(CompanyConn pCc, string pItemCode, int idUser = 0, string pBarCode = null)
        {
            try
            {
                return mItemMasterRepository.GeItemByCode(pCc, pItemCode, idUser, pBarCode);
            }
            catch (Exception ex)
            {

                Logger.WriteError("ItemMasterManager -> GeItemByCode :" + ex.InnerException.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="ListItems"></param>
        /// <param name="CardCode"></param>
        /// <param name="pSession"></param>
        /// <param name="pOrderItems"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsPrice(CompanyConn pCc, List<ItemMasterSAP> ListItems, string CardCode, string pSession, bool pOrderItems = true, string ano = "")
        {
            try
            {
                return mItemMasterRepository.GetItemsPrice(pCc, ListItems, CardCode, pSession, false, pOrderItems, ano);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetItemsPrice :" + ex.InnerException.Message);
                return ListItems;
            }
        }

        public List<StockModel> GetItemStock(CompanyConn pCc, string[] pItemCode, List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                return mItemMasterRepository.GetItemStock(pCc, pItemCode, pWarehousePortalList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetItemStock :" + ex.InnerException.Message);
                return null;
            }
        }

        public List<StockModel> GetItemWithStock(CompanyConn pCc, List<WarehousePortal> pWarehousePortalList, List<string> pItemsList = null)
        {
            try
            {
                if (pItemsList == null)
                {
                    pItemsList = new List<string>();
                }

                List<StockModel> mReturn = mItemMasterRepository.GetItemStock(pCc, pItemsList.ToArray(), pWarehousePortalList).GroupBy(c => c.ItemCode).Select(c => new StockModel { ItemCode = c.Key }).ToList();

                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetItemWithStock :" + ex.InnerException.Message);
                return null;
            }
        }

        public List<GLAccountSAP> GetGLAccountList(CompanyConn pCc)
        {
            try
            {

                List<GLAccountSAP> mReturn = mItemMasterRepository.GetGLAccountList(pCc).ToList();

                return mReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetGLAccountList :" + ex.InnerException.Message);
                return null;
            }
        }

        public decimal GetPriceItemByPriceList(CompanyConn pCc, string pItemCode, short pPriceList)
        {
            try
            {
                return mItemMasterRepository.GetPriceItemByPriceList(pCc, pItemCode, pPriceList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetPriceItemByPriceList :" + ex.InnerException.Message);
                return 0;
            }

        }

        public List<UDF_ARGNS> GetItemMasterUDF(CompanyConn pCc, int pUserId)
        {
            try
            {

                return mItemMasterRepository.GetItemMasterUDF(pCc, pUserId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetItemMasterUDF :" + ex.InnerException.Message);
                return null;
            }

        }

        public List<ItemGroupSAP> GetItemGroupList(CompanyConn pCc)
        {
            try
            {

                return mItemMasterRepository.GetItemGroupList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetItemGroupList :" + ex.InnerException.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCodeFrom"></param>
        /// <param name="pItemCodeTo"></param>
        /// <param name="pVendorFrom"></param>
        /// <param name="pVendorTo"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pSelectedWH"></param>
        /// <returns></returns>
        public JsonObjectResult GetInventoryStatus(CompanyConn pCc,
            string pItemCodeFrom, string pItemCodeTo,
            string pVendorFrom, string pVendorTo,
            string pItemGroup, string[] pSelectedWH,
            int pStart, int pLength,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                return mItemMasterRepository.GetInventoryStatus(pCc,
                    pItemCodeFrom, pItemCodeTo,
                    pVendorFrom, pVendorTo,
                    pItemGroup, pSelectedWH,
                    pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetInventoryStatus :" + ex.InnerException.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        public List<AvailableToPromise> GetATPByWarehouse(CompanyConn pCc, string pItemCode, string pWarehouse)
        {
            try
            {

                return mItemMasterRepository.GetATPByWarehouse(pCc, pItemCode, pWarehouse);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetATPByWarehouse :" + ex.InnerException.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="mListItems"></param>
        /// <param name="pFormula"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsWithPositiveStock(CompanyConn pCc, List<ItemMasterSAP> mListItems, string[] pFormula)
        {
            try
            {
                return mItemMasterRepository.GetItemsWithPositiveStock(pCc, mListItems, pFormula);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetItemsWithPositiveStock :" + ex.InnerException.Message);

                return null;
            }
        }

        public List<Warehouse> GetAvailableWarehousesByCompany(CompanyConn pCc, List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                return mItemMasterRepository.GetAvailableWarehousesByCompany(pCc, pWarehousePortalList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GetAvailableWarehousesByCompany :" + ex.InnerException.Message);

                return null;
            }
        }

        public List<ItemMasterSAP> GeUOMByItemList(CompanyConn pCc, List<ItemMasterSAP> pListItem, string pSellItem, string pPrchseItem)
        {
            try
            {
                return mItemMasterRepository.GeUOMByItemList(pCc, pListItem, pSellItem, pPrchseItem);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterManager -> GeUOMByItemList :" + ex.InnerException.Message);

                return null;
            }
        }
    }
}
