﻿using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class FolioManager
    {
        public int New(string pFolioType)
        {
            int ret = 0;
            try
            {
                FolioRepository folioRepository = new FolioRepository();
                ret = folioRepository.New(pFolioType);
            }
            catch (Exception ex)
            {
                Logger.WriteError(ex.Message);
            }
            return ret;
            

        }
    }
}
