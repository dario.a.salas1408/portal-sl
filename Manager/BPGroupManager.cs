﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class BPGroupManager
    {
        private BPGroupRepository mBPGroupRepository;
        public BPGroupManager()
        {
            mBPGroupRepository = new BPGroupRepository();
        }
        
        public BPGroup GetBPGroup(int Id)
        {
            try
            {
                return mBPGroupRepository.GetBPGroup(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPGroupManager -> GetBPGroup:" + ex.InnerException);
                throw ex;
            }
        }

        public List<BPGroup> GetBPGroupList(int IdCompany)
        {
            try
            {
                return mBPGroupRepository.GetBPGroupList(IdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPGroupManager -> GetBPGroupList:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Add(BPGroup pBPGroup)
        {
            try
            {
                return mBPGroupRepository.Add(pBPGroup);
            }
            catch (Exception ex)
            {

                Logger.WriteError("BPGroupManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(BPGroup pBPGroup)
        {
            try
            {
                return mBPGroupRepository.Update(pBPGroup);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPGroupManager -> Update:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
