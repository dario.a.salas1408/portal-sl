﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class CommentManager
    {
        CommentRepository mCommentRepository;

        public CommentManager()
        {
            mCommentRepository = new CommentRepository();
        }

        public List<ARGNSModelComment> GetListComments(CompanyConn pCc, string ItemCode)
        {
            return mCommentRepository.GetListComments(pCc, ItemCode);
        }

        public string AddComment(ARGNSModelComment pModel, CompanyConn pCc)
        {
            try
            {
                return mCommentRepository.AddComment(pModel, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CommentManager -> AddComment:" + ex.InnerException);
                throw;
            }
        }

        public string UpdateComment(ARGNSModelComment pModel, CompanyConn pCc)
        {
            try
            {
                return mCommentRepository.UpdateComment(pModel, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CommentManager -> UpdateComment:" + ex.InnerException);
                throw;
            }
        }
    }
}
