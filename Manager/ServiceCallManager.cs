﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class ServiceCallManager
    {

        ServiceCallRepository mServRepository;

        public ServiceCallManager()
        {
            mServRepository = new ServiceCallRepository();
        }

        public ServiceCallComboList GetServiceCallComboList(CompanyConn pCompanyParam)
        {
            try
            {
                return mServRepository.GetServiceCallComboList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManager -> GetServiceCallComboList:" + ex.InnerException);
                throw;
            }
        }

        public ServiceCallSAP GetServiceCallById(CompanyConn pCompanyParam, int pId)
        {
            try
            {
                return mServRepository.GetServiceCallById(pCompanyParam, pId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManager -> GetServiceCallById:" + ex.InnerException);
                throw;
            }
        }

        public JsonObjectResult AddServiceCall(CompanyConn pCompanyParam, ServiceCallSAP pObject)
        {
            try
            {
                return mServRepository.AddServiceCall(pObject,pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManager -> AddServiceCall:" + ex.InnerException);
                throw;
            }
        }


        public string UpdateServiceCall(CompanyConn pCompanyParam, ServiceCallSAP pObject)
        {
            try
            {
                return mServRepository.UpdateServiceCall(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManager -> UpdateServiceCall:" + ex.InnerException);
                throw;
            }
        }

        public List<ServiceCallSAP> GetServiceCallListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null)
        {
            try
            {
                return mServRepository.GetServiceCallListSearch(pCompanyParam, pCodeBP, pPriority, pSubject, pRefNumber, pDate, pStatus,pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManager -> GetServiceCallListSearch:" + ex.InnerException);
                throw;
            }
        }

        public List<EquipmentCardSAP> GetEquipmentCardListSearch(CompanyConn pCompanyParam, string pItemCode = "", string pItemName = "")
        {
            try
            {
                return mServRepository.GetEquipmentCardListSearch(pCompanyParam, pItemCode, pItemName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManager -> GetEquipmentCardListSearch:" + ex.InnerException);
                throw;
            }
        }

        public List<ServiceStatus> GetServiceCallStatusList(CompanyConn pCompanyParam)
        {
            try
            {
                return mServRepository.GetServiceCallStatusList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallManager -> GetServiceCallStatusList:" + ex.InnerException);
                throw;
            }
        }

    }
}
