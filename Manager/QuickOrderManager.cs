﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Interfaces;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class QuickOrderManager
    {
        QuickOrderRepository mQuickOrderRepository;
        public QuickOrderManager()
        {
            mQuickOrderRepository = new QuickOrderRepository();
        }

        public List<QuickOrder> GetQuickOrders(int pIdUser, int pCompanyId)
        {
            try
            {
                return mQuickOrderRepository.GetQuickOrders(pIdUser, pCompanyId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManager -> GetQuickOrders:" + ex.InnerException);
                throw ex;
            }
        }

        public QuickOrder GetQuickOrderLine(int pIdUser, string pItemCode, int pCompanyId)
        {
            try
            {
                return mQuickOrderRepository.GetQuickOrderLine(pIdUser, pItemCode, pCompanyId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManager -> GetQuickOrderLine:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Add(QuickOrder pQuickOrder)
        {
            try
            {
                return mQuickOrderRepository.Add(pQuickOrder);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(QuickOrder pQuickOrder)
        {
            try
            {
                return mQuickOrderRepository.Update(pQuickOrder);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManager -> Update:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Delete(QuickOrder pQuickOrder)
        {
            try
            {
                return mQuickOrderRepository.Delete(pQuickOrder);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }

        public bool DeleteCart(int pIdUser, int pCompanyId)
        {
            try
            {
                return mQuickOrderRepository.DeleteCart(pIdUser, pCompanyId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("QuickOrderManager -> DeleteCart:" + ex.InnerException);
                throw ex;
            }
        }





    }
}
