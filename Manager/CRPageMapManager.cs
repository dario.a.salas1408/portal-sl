﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class CRPageMapManager
    {
        CRPageMapRepository mCRPageMapRepository;

        public CRPageMapManager()
        {
            mCRPageMapRepository = new CRPageMapRepository();
        }

        public List<CRPageMap> GetCRPageMap(int pIdCompany)
        {
            try
            {
                return mCRPageMapRepository.GetCRPageMap(pIdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CRPageMapManager -> GetCRPageMap :" + ex.InnerException);
                throw ex;
            }
        }

        public List<Page> getPagesToAdd(string[] pPagesAdded)
        {
            try
            {
                return mCRPageMapRepository.getPagesToAdd(pPagesAdded);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CRPageMapManager -> getPagesToAdd :" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(List<CRPageMap> listCRPageMap, int IdCompany)
        {
            try
            {
                return mCRPageMapRepository.Update(listCRPageMap, IdCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CRPageMapManager -> Update:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Delete(int pIdICRPageMap)
        {
            try
            {
                return mCRPageMapRepository.Delete(pIdICRPageMap);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CRPageMapManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }

        public List<string> HasAPageCRMapping(Enums.Pages page, CompanyConn pCc)
        {
            try
            {
                return mCRPageMapRepository.HasAPageCRMapping(page,pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CRPageMapManager -> HasAPageCRMapping:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
