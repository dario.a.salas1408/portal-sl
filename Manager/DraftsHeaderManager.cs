﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;

namespace ARGNS.Manager
{
    public class LocalDraftsHeaderManager
    {
        LocalDraftsHeaderRepository mDraftsHeaderRepository;
        public LocalDraftsHeaderManager()
        {
            mDraftsHeaderRepository = new LocalDraftsHeaderRepository();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCv"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SaleOrderSAP pSO, CompanyConn pCv)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                LocalDrafts draftHeader = new LocalDrafts()
                {
                    CardCode = pSO.CardCode,
                    DocDate = pSO.DocDate,
                    NumAtCard = pSO.NumAtCard,
                    ObjectCode = "17",
                    DocDueDate = DateTime.Now,
                    Comments = pSO.Comments,
                    CardName = pSO.CardName,
                    CompanyId = pCv.IdCompany,
                    UserId = pCv.IdUserConected,
                    ObjType = "0",
                    LastUpdatedTime = DateTime.Now,
                    GroupNum = pSO.GroupNum
                };

                draftHeader.Lines = new System.Collections.Generic.List<LocalDraftsLine>();

                foreach (SaleOrderSAPLine line in pSO.Lines)
                {
                    LocalDraftsLine newLine = new LocalDraftsLine()
                    {
                        DiscPrcnt = line.DiscPrcnt,
                        ItemCode = line.ItemCode,
                        PriceBefDi = line.PriceBefDi,
                        Quantity = line.Quantity,
                        ShipDate = line.ShipDate,
                        WhsCode = line.WhsCode,
                        LineNum = line.LineNum,
                        Currency = line.Currency,
                        Dscription = line.Dscription,
                        UomCode = line.UomCode
                    };

                    draftHeader.Lines.Add(newLine);
                }

                WebPortalModel Db = new WebPortalModel();
                draftHeader = Db.LocalDrafts.Add(draftHeader);
                Db.SaveChanges();
                mJsonObjectResult.Code = draftHeader.DocNum.ToString();
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (System.Exception ex)
            {
                Logger.WriteError("DraftsHeaderManager--> Add: " + ex.Message.ToString());
                Logger.WriteError("DraftsHeaderManager--> Add: " + ex.InnerException.ToString());
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = "Error on Creating Local Draft";
            }
            return mJsonObjectResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="draftHeader"></param>
        /// <param name="companyConn"></param>
        /// <returns></returns>
        public string Update(LocalDrafts updatedLocalDraft,
            CompanyConn companyConn)
        {
            WebPortalModel Db = new WebPortalModel();
            LocalDrafts originalLocalDraft = Db.LocalDrafts
                .AsNoTracking()
                .Where(w => w.DocNum == updatedLocalDraft.DocNum)
                .FirstOrDefault();

            try
            {
                originalLocalDraft.CardCode = updatedLocalDraft.CardCode;
                originalLocalDraft.DocDate = updatedLocalDraft.DocDate;
                originalLocalDraft.NumAtCard = updatedLocalDraft.NumAtCard;
                originalLocalDraft.Comments = updatedLocalDraft.Comments;
                originalLocalDraft.CardName = updatedLocalDraft.CardName;
                originalLocalDraft.LastUpdatedTime = DateTime.Now;
                originalLocalDraft.GroupNum = updatedLocalDraft.GroupNum;

                while (originalLocalDraft.Lines.Count > 0)
                {
                    Db.Entry(originalLocalDraft.Lines.FirstOrDefault()).State = EntityState.Deleted;
                }

                foreach (LocalDraftsLine line in updatedLocalDraft.Lines)
                {
                    LocalDraftsLine newLine = new LocalDraftsLine()
                    {
                        DiscPrcnt = line.DiscPrcnt,
                        ItemCode = line.ItemCode,
                        PriceBefDi = line.PriceBefDi,
                        Quantity = line.Quantity,
                        ShipDate = line.ShipDate,
                        WhsCode = line.WhsCode,
                        HeaderId = originalLocalDraft.DocNum,
                        Currency = line.Currency,
                        Dscription = line.Dscription,
                        UomCode = line.UomCode
                    };

                    originalLocalDraft.Lines.Add(newLine);
                    Db.Entry(newLine).State = EntityState.Added;
                }

                Db.LocalDrafts.Attach(originalLocalDraft);
                Db.Entry(originalLocalDraft).State = EntityState.Modified;
                Db.SaveChanges();

                return "Ok";
            }
            catch (System.Exception ex)
            {
                Logger.WriteError("DraftsHeaderManager--> Add: " + ex.Message.ToString());
                Logger.WriteError("DraftsHeaderManager--> Add: " + ex.InnerException.ToString());
                return "Error: " + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="draftId"></param>
        /// <returns></returns>
        public bool Delete(int draftId)
        {
            try
            {
                WebPortalModel Db = new WebPortalModel();
                LocalDrafts localDraft = Db.LocalDrafts
                    .Where(w => w.DocNum.Equals(draftId))
                    .FirstOrDefault();
                Db.LocalDrafts.Remove(localDraft);
                Db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("LocalDraftsHeaderManager --> Delete: " + ex.Message);
                Logger.WriteError("LocalDraftsHeaderManager --> Delete: " + ex.InnerException);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult ListAllDrafts(CompanyConn pCc, string pCodeVendor = "",
            DateTime? pDate = null, int? pDocNum = null, 
            string pDocStatus = "", string pOwnerCode = "",
            string pSECode = "", int pStart = 0, int pLength = 0, 
            OrderColumn pOrderColumn = null)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                WebPortalModel Db = new WebPortalModel();

                string orderByQuery = OrderString(pOrderColumn, pCc.ServerType != (int)Enums.eServerType.dst_HANA ? "SQL" : "HANA");


                mJsonObjectResult.LocalDraftsList = Db.LocalDrafts
                    .Where(w =>
                            (w.CardCode.Contains(pCodeVendor.Trim()) || pCodeVendor.Equals(""))
                            && w.UserId == pCc.IdUserConected
                            && w.CompanyId == pCc.IdCompany
                            && (pDocNum != null ? w.DocNum == pDocNum : true))
                    .OrderBy(OrderString(pOrderColumn))
                    .Skip(pStart).Take(pLength)
                    .ToList();

                mJsonObjectResult.Others.Add("TotalRecords",
                            Db.LocalDrafts.Where(w =>
                            (w.CardCode.Contains(pCodeVendor.Trim()) || pCodeVendor.Equals(""))
                            && w.UserId == pCc.IdUserConected
                            && w.CompanyId == pCc.IdCompany
                            && (pDocNum != null ? w.DocNum == pDocNum : true))
                            .Count().ToString());
                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DraftsHeaderManager--> ListAllDrafts: " + ex.Message.ToString());
                Logger.WriteError("DraftsHeaderManager--> ListAllDrafts: " + ex.InnerException.ToString());
                return mJsonObjectResult;
            }
        }

        public static string OrderString(OrderColumn pColumnToOrder, string pDBType = "SQL")
        {
            string mReturnString = "";
            if (pColumnToOrder != null)
            {
                if (pColumnToOrder.SortDirection == OrderColumn.OrderDirection.Ascendant)
                    mReturnString = (pDBType == "HANA" ? "\"" : "") + pColumnToOrder.Name + (pDBType == "HANA" ? "\"" : "") + " asc";
                else
                    mReturnString = (pDBType == "HANA" ? "\"" : "") + pColumnToOrder.Name + (pDBType == "HANA" ? "\"" : "") + " desc";
            }
            return mReturnString;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyConn"></param>
        /// <param name="idLocalDraft"></param>
        /// <returns></returns>
        public LocalDrafts GetDraftDocument(int idLocalDraft)
        {
            WebPortalModel Db = new WebPortalModel();
            return Db.LocalDrafts.AsNoTracking().Where(w => w.DocNum == idLocalDraft)
                .FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
        public List<LocalDraftsLine> ListLineDocuments(CompanyConn pCc, string[] pDocuments)
        {
            WebPortalModel Db = new WebPortalModel();
            List<LocalDraftsLine> listRestu = Db.LocalDraftsLines
                .AsNoTracking()
                .Where(c => pDocuments.ToList()
                .Contains(c.HeaderId.ToString()))
                .ToList();
            return listRestu;
        }
    }
}
