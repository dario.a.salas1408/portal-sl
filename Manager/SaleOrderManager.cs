﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.DiverSO;
using ARGNS.Model.Implementations.DiverSO.Catalog;
using ARGNS.Model.Implementations.DiverSO.SalesDocProcess;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class SaleOrderManager
    {
        SORepository mSORepository;
        ItemMasterRepository mOITMRepository;
        GlobalDocumentManager mGlobalDocumentManager;


        public SaleOrderManager()
        {
            mSORepository = new SORepository();
            mOITMRepository = new ItemMasterRepository();
            mGlobalDocumentManager = new GlobalDocumentManager();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pId"></param>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public SaleOrderSAP GetSaleOrder(CompanyConn pCc, int pId, int pUserId, bool pShowOpenQuantity)
        {
            try
            {
                SaleOrderSAP mreturn = Mapper.Map<SaleOrderSAP>(
                    mSORepository.GetSaleOrderById(pId, pUserId, pCc , pShowOpenQuantity));
                return mreturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> GetSaleOrder:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCc"></param>
        /// <param name="pFrom"></param>
        /// <param name="pUrlFrom"></param>
        /// <param name="pQuickOrderId"></param>
        /// <returns></returns>
        public JsonObjectResult Add(SaleOrderSAP pSO, CompanyConn pCc, string pFrom, string pUrlFrom, string pQuickOrderId = "")
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            try
            {
                mJsonObjectResult = mSORepository.Add(pSO, pCc);
                if (mJsonObjectResult.ServiceAnswer == Enums.ServiceResult.Ok.ToDescriptionString())
                {
                    //If code is null, the order entry in approval process.
                    if (!string.IsNullOrEmpty(mJsonObjectResult.Code))
                    { 
                        if (!string.IsNullOrEmpty(pQuickOrderId))
                        {
                            mJsonObjectResult.RedirectUrl = "/Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=View&IdPO={Code}&fromController={From}";
                            mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{Code}", mJsonObjectResult.Code);
                            mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{From}", pFrom);
                        }
                        else
                        {
                            mJsonObjectResult.RedirectUrl = "/Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=Update&IdPO={Code}&fromController={From}";
                            mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{Code}", mJsonObjectResult.Code);
                            mJsonObjectResult.RedirectUrl = mJsonObjectResult.RedirectUrl.Replace("{From}", pFrom);
                        }
                    }
                    else
                    {
                        mJsonObjectResult.RedirectUrl = pUrlFrom;
                    }
                }
                else
                {
                    if(!string.IsNullOrEmpty(pQuickOrderId))
                    {
                        mJsonObjectResult.RedirectUrl = "/Sales/QuickOrder/Index";
                    }
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSO"></param>
        /// <param name="pCc"></param>
        /// <returns></returns>
        public string Update(SaleOrderSAP pSO, CompanyConn pCc)
        {
            try
            {
                return mSORepository.Update(pSO, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesOrderListSearch(CompanyConn pCc, 
            bool pShowOpenQuantityStatus, string pCodeVendor = "", 
            DateTime? pDate = null, int? pDocNum = null, 
            string pDocStatus = "", string pOwnerCode = "", 
            string pSECode = "", int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                return mSORepository.GetPurchaseOrderListSearch(pCc, pShowOpenQuantityStatus, 
                    pCodeVendor, pDate, 
                    pDocNum, pDocStatus, pOwnerCode, pSECode, 
                    pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> GetPurchaseOrderListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pSO"></param>
        /// <returns></returns>
        public SaleOrderSAP GetSOInternalObjects(CompanyConn pCc, SaleOrderSAP pSO)
        {
            try
            {
                DocumentSAPCombo mDocSAPCombo = mSORepository.GetDocumentSAPCombo(pCc);
                pSO.ListDocumentSAPCombo = mDocSAPCombo;
                return pSO;

            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> GetSOInternalObjects:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pDocEntry"></param>
        /// <returns></returns>
        public List<Freight> GetFreights(CompanyConn pCc, int pDocEntry)
        {
            try
            {
                return mSORepository.GetFreights(pCc, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> GetFreights:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="mListCurrency"></param>
        /// <param name="DocDate"></param>
        /// <param name="pLocalCurrency"></param>
        /// <param name="pSystemCurrency"></param>
        /// <returns></returns>
        public List<RatesSystem> GetListRates(CompanyConn pCc, List<CurrencySAP> mListCurrency, DateTime DocDate, string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                return mGlobalDocumentManager.GetListRatesSystem(pCc, mListCurrency, DocDate, pLocalCurrency, pSystemCurrency);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> GetListRates:" + ex.InnerException);
                throw ex;
            }
        }

        public DiverSOSalesDocProcessResult GetPromotionItemsByOrder(SaleOrderSAP pSalesOrder)
        {
            try
            {
                return mSORepository.GetPromotionItemsByOrder(pSalesOrder);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> GetPromotionItemsByOrder :" + ex.Message);
                Logger.WriteError("SaleOrderManager -> GetPromotionItemsByOrder :" + ex.InnerException.Message);

                throw ex;
            }
        }

        public DiverSOCatalogResult GetDSOCatalogs(string pCatalogCode, string pCatalogName, string pCardCode, string pSlpCode, DateTime pDate, bool pIncludeItems, string pItemCode, string pItemName, string pItemGroup, int pStart = 0, int pLength = 0)
        {
            try
            {
                return mSORepository.GetDSOCatalogs(pCatalogCode, pCatalogName, pCardCode, pSlpCode, pDate, pIncludeItems, pItemCode, pItemName, pItemGroup, pStart, pLength);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderManager -> GetDSOCatalogs :" + ex.Message);
                Logger.WriteError("SaleOrderManager -> GetDSOCatalogs :" + ex.InnerException.Message);

                throw ex;
            }
        }

        

    }
}
