﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class UserSapManager
    {
        private UserSapRepository mUserSapRepository;

        public UserSapManager()
        {
            mUserSapRepository = new UserSapRepository();
        }

        public List<UserSAP> GetListUsers(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetListUsers(pCc); 
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetListUsers :" + pCc.UserName + " - " + pCc.Password + " - " + pCc.CompanyDB);
                Logger.WriteError("UserSapManager -> GetListUsers :" + ex.InnerException); 
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeList(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetEmployeeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetEmployeeList :" + ex.InnerException);
                throw ex;
            }
        }

        public List<EmployeeSAP> GetAllEmployeeList(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetAllEmployeeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetAllEmployeeList :" + ex.InnerException);
                throw ex;
            }
        }

        public List<SalesEmployeeSAP> GetAllSalesEmployeeList(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetAllSalesEmployeeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetAllSalesEmployeeList :" + ex.InnerException);
                throw ex;
            }
        }

        public List<SalesEmployeeSAP> GetSalesEmployeeSearchByName(CompanyConn pCc, string pSEName)
        {
            try
            {
                return mUserSapRepository.GetSalesEmployeeSearchByName(pCc, pSEName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetSalesEmployeeSearchByName :" + ex.InnerException);
                throw ex;
            }
        }

        public List<SalesTaxCodesSAP> GetSalesTaxCode(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetSalesTaxCode(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetAllSalesEmployee :" + ex.InnerException);
                throw ex;
            }
        }

        public List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetBusinessPartnerList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetBusinessPartnerList :" + ex.InnerException);
                throw ex;
            }
        }

        public UsersSetting GetCompany(int id, int idUser)
        {

            try
            {
                return mUserSapRepository.GetCompany(id,idUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetCompany :" + ex.InnerException);
                throw ex;
            }
        }

        public List<UserSAP> GetUserSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                return mUserSapRepository.GetUserSAPSearchByCode(pCc, pRequesterCode, pRequesterName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetUserSAPSearchByCode :" + ex.InnerException);
                throw ex;
            }
        }

        public UserSAP GetUserSAPSearchByName(CompanyConn pCc, string pUserName)
        {
            try
            {
                return mUserSapRepository.GetUserSAPSearchByName(pCc, pUserName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetUserSAPSearchByName :" + ex.InnerException);
                throw ex;
            }
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "", short? pDepartment = null, bool pHaveUserCode = true)
        {
            try
            {
                return mUserSapRepository.GetEmployeeSAPSearchByCode(pCc, pRequesterCode, pRequesterName, pDepartment, pHaveUserCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetEmployeeSAPSearchByCode :" + ex.InnerException);
                throw ex;
            }
        }

        public EmployeeSAP GetEmployeeById(CompanyConn pCc, int? pCode)
        {
            try
            {
                return mUserSapRepository.GetEmployeeById(pCc, pCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetEmployeeById:" + ex.InnerException);
                throw ex;
            }
        }

        public List<DistrRuleSAP> GetDistributionRuleList(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetDistributionRuleList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetDistributionRuleList :" + ex.InnerException);
                throw ex;
            }
        }

        public List<Warehouse> GetWarehouseList(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetWarehouseList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetWarehouseList :" + ex.InnerException);
                throw ex;
            }
        }

        public List<Warehouse> GetWarehouseListSearch(CompanyConn pCc, string pWhsCode, string pWhsName)
        {
            try
            {
                return mUserSapRepository.GetWarehouseListSearch(pCc, pWhsCode, pWhsName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetWarehouseListSearch :" + ex.InnerException);
                throw ex;
            }
        }

        public List<UserUDF> GetUserUDF(int IdUser)
        {
            try
            {

                return mUserSapRepository.GetUserUDF(IdUser).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetUserUDF :" + ex.InnerException);
                throw ex;
            }
        }

        public List<UserUDF> GetUserUDFSearch(int pIdUser, int pIdCompany, string pDocument)
        {
            try
            {
                return mUserSapRepository.GetUserUDFSearch(pIdUser, pIdCompany, pDocument).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetUserUDFSearch :" + ex.InnerException);
                throw ex;
            }
        }

        public List<UDF_SAP> GetUDFByTableID(CompanyConn pCc, string pTableID)
        {
            try
            {

                return mUserSapRepository.GetUDFByTableID(pCc, pTableID).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetUDFByTableID :" + ex.InnerException);
                throw ex;
            }
        }

        public List<UFD1_SAP> GetUDF1ByTableID(CompanyConn pCc, string pTableID)
        {
            try
            {
                return mUserSapRepository.GetUDF1ByTableID(pCc, pTableID).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetUDF1ByTableID :" + ex.InnerException);
                throw ex;
            }
        }

        public List<AlertSAP> GetAlertsByUser(CompanyConn pCc, int pUser)
        {
            try
            {
                return mUserSapRepository.GetAlertsByUser(pCc, pUser).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetAlertsByUser :" + ex.InnerException);
                throw ex;
            }
        }

        public AlertSAP GetAlertById(CompanyConn pCc, int pAlertId)
        {
            try
            {
                return mUserSapRepository.GetAlertById(pCc, pAlertId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetAlertById :" + ex.InnerException);
                throw ex;
            }
        }

        public string GetQueryResult(CompanyConn pCc, string mQuery)
        {
            try
            {
                return mUserSapRepository.GetQueryResult(pCc, mQuery);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetQueryResult :" + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult GetSAPQueryResult(CompanyConn pCc, string pQuery)
        {
            try
            {
                return mUserSapRepository.GetSAPQueryResult(pCc, pQuery);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetSAPQueryResult :" + ex.InnerException);
                throw ex;
            }
        }

        public List<PriceListSAP> GetAllPriceListSAP(CompanyConn pCc)
        {
            try
            {
                return mUserSapRepository.GetAllPriceListSAP(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSapManager -> GetAllPriceListSAP :" + ex.InnerException);
                throw ex;
            }
        }
    }
}
