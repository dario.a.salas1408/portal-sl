﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class TechPackManager
    {

        private TechPackRepository mTechPackRepository;

        public TechPackManager()
        {
            mTechPackRepository = new TechPackRepository();
        }


        public ARGNSTechPack GetTechPack(CompanyConn pCc, int Id)
        {
            try
            {
                return mTechPackRepository.GetTechPack(pCc, Id);
            }
            catch (Exception ex)
            {
                Logger.WriteError("TechPackManager -> GetTechPack:" + ex.InnerException);
                throw ex;
            }
        }

        public string UpdateTechPack(ARGNSTechPack pTechPack, CompanyConn pCc)
        {
            try
            {
                return mTechPackRepository.UpdateTechPack(pTechPack, pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("TechPackManager -> UpdateTechPack:" + ex.InnerException);
                throw;
            }
        }
    }
       
}
