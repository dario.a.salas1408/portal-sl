﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class OpportunityManager
    {
        OpportunityRepository mOpporRepository;

        public OpportunityManager()
        {
            mOpporRepository = new OpportunityRepository();
        }

        public OpportunitiesSAPLine GetOpportunityLine(CompanyConn pCompanyParam)
        {
            try
            {
                return mOpporRepository.GetOpportunityLine(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManager -> GetOpportunityLine:" + ex.InnerException);
                throw;
            }
        }

        public OpportunityStage GetStageById(CompanyConn pCompanyParam, int pId)
        {
            try
            {
                return mOpporRepository.GetStageById(pCompanyParam, pId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManager -> GetStageById:" + ex.InnerException);
                throw;
            }
        }

        public OpportunitiesSAP GetOportunityById(CompanyConn pCompanyParam, int pId)
        {
            try
            {
                return mOpporRepository.GetOportunityById(pCompanyParam, pId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManager -> GetOportunityById:" + ex.InnerException);
                throw;
            }          
        }

        public string AddOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return mOpporRepository.AddOportunity(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManager -> AddOportunity:" + ex.InnerException);
                throw;
            }  
        }

        public string UpdateOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return mOpporRepository.UpdateOportunity(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManager -> UpdateOportunity:" + ex.InnerException);
                throw;
            }
        }

        public List<OpportunitiesSAP> GetOpportunityListSearch(CompanyConn pCompanyParam, string pCodeBP = "", int? pSalesEmp = null, string pOpportunityName = "", int? pOwner = null, DateTime? pDate = null, string pStatus = "") 
        {
            try
            {
                return mOpporRepository.GetOpportunityListSearch(pCompanyParam, pCodeBP, pSalesEmp, pOpportunityName, pOwner, pDate, pStatus);
            }
            catch (Exception ex)
            {
                Logger.WriteError("OpportunityManager -> GetOpportunityListSearch:" + ex.InnerException);
                throw;
            }
        }


    }
}
