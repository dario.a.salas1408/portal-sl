﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{

    public class GlobalDocumentManager
    {
        private GlobalDocumentRepository mGlobalDocumentRepository;

        public GlobalDocumentManager()
        {
            mGlobalDocumentRepository = new GlobalDocumentRepository();
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCc"></param>
		/// <param name="ListCurrency"></param>
		/// <param name="DocDate"></param>
		/// <param name="pLocalCurrency"></param>
		/// <param name="pSystemCurrency"></param>
		/// <returns></returns>
        public List<RatesSystem> GetListRatesSystem(CompanyConn pCc, 
			List<CurrencySAP> ListCurrency, DateTime DocDate, 
			string pLocalCurrency = "", string pSystemCurrency = "")
        {
            try
            {
                string mSystemCurrency = string.Empty;
                string mLocalCurrency = string.Empty;

                List<RatesSystem> mReturnListRate = new List<RatesSystem>();
                string mRate = string.Empty;

                mLocalCurrency = (pLocalCurrency == "" || pLocalCurrency  == null? mGlobalDocumentRepository.GetLocalCurrency(pCc) : pLocalCurrency);

                mSystemCurrency = (pSystemCurrency == "" || pLocalCurrency == null ? mGlobalDocumentRepository.GetSystemCurrency(pCc) : pSystemCurrency);

                mReturnListRate.Add(
                    new RatesSystem {
                        Currency = mLocalCurrency,
                        Rate = Convert.ToDouble(1),
                        Local = true,
                        System = false });


                if (mLocalCurrency != mSystemCurrency)
                {
                    mRate = mGlobalDocumentRepository.GetCurrencyRate(mSystemCurrency, DocDate, pCc);

                    //mRate = "0.857927";

                    mRate = (mRate.Split(':')[0] == "Error" ? "0" : mRate);

                    mReturnListRate.Add(new RatesSystem { Currency = mSystemCurrency, Rate = Convert.ToDouble(mRate, CultureInfo.InvariantCulture), Local = false, System = true });
                }
                else
                {
                    mReturnListRate.Add(new RatesSystem { Currency = mSystemCurrency, Rate = Convert.ToDouble(1), Local = false, System = true });
                }

                foreach (CurrencySAP item in ListCurrency)
                {

                    if (item.CurrCode != mLocalCurrency)
                    {
                        mRate = mGlobalDocumentRepository.GetCurrencyRate(item.CurrCode, DocDate, pCc);

                        //mRate = "0.857927";

                        mRate = (mRate.Split(':')[0] == "Error" ? "0" : mRate);

                        mReturnListRate.Add(new RatesSystem { Currency = item.CurrCode, Rate = Convert.ToDouble(mRate, CultureInfo.InvariantCulture), Local = false, System = false });

                    }
                    else
                    {
                        mReturnListRate.Add(new RatesSystem { Currency = item.CurrCode, Rate = Convert.ToDouble(1), Local = false, System = false });
                    }

                }

                return mReturnListRate;
            }
            catch (Exception ex)
            {
                Logger.WriteError("GlobalDocumentManager -> GetListRatesSystem :" + ex.InnerException.Message);
                throw ex;
            }
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyConn pCompanyParam)
        {
            try
            {
                return mGlobalDocumentRepository.GetTaxCode(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("GlobalDocumentManager -> GetTaxCode :" + ex.InnerException.Message);
                return null;
            }
        }
    }
}
