﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.View;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class TileManager
    {
        TileRepository mTileRepository;

        public TileManager()
        {
            mTileRepository = new TileRepository();
        }

        public List<PortalTile> GetPortalTileList(int pIdCompany)
        {
            try
            {
                List<PortalTile> mPortalTileList = mTileRepository.GetPortalTileList(pIdCompany);

                return mPortalTileList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> GetPortalTileList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalTile> GetPortalTileListSearch(int pIdCompany, UserGroup pUserGroup)
        {
            try
            {
                List<PortalTile> mPortalTileList = mTileRepository.GetPortalTileListSearch(pIdCompany, pUserGroup);

                return mPortalTileList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> GetPortalTileListSearch:" + ex.InnerException);
                throw ex;
            }
        }

        public PortalTile GetPortalTileById(CompanyConn pCc, int pTileId)
        {
            try
            {
                PortalTile mPortalTile = new PortalTile();
                mPortalTile = mTileRepository.GetPortalTileById(pCc, pTileId);

                return mPortalTile;
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> GetPortalTileById:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalTileType> GetPortalTileTypeList(CompanyConn pCc)
        {
            try
            {
                return mTileRepository.GetPortalTileTypeList(pCc);
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> GetPortalTileTypeList:" + ex.InnerException);
                throw ex;
            }
        }

        public List<PortalTileUserGroup> GetPortalTileUserGroupList(CompanyConn pCc, int IdTile)
        {
            try
            {
                return mTileRepository.GetPortalTileUserGroupList(pCc, IdTile);
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> GetPortalTileUserGroupList:" + ex.InnerException);
                throw ex;
            }
        }

        public JsonObjectResult Update(PortalTile pPortalTile)
        {
            try
            {
                return mTileRepository.Update(pPortalTile);
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> Update:" + ex.InnerException);
                throw;
            }
        }

        public JsonObjectResult Add(PortalTile pPortalTile)
        {
            try
            {
                return mTileRepository.Add(pPortalTile);
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> Add:" + ex.InnerException);
                throw;
            }
        }

        public bool Delete(int pIdTileDelete)
        {
            try
            {
                return mTileRepository.Delete(pIdTileDelete);
            }
            catch (Exception ex)
            {
                Logger.WriteError("TileManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
