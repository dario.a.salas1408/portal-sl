﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;

namespace ARGNS.Manager
{
	public class CompaniesStockManager
    {
        CompaniesStockRepository mCompaniesStockRepository;

        public CompaniesStockManager()
        {
            mCompaniesStockRepository = new CompaniesStockRepository();
        }

        public CompanyStock GetCompanyStock(int pIdCompany)
        {
            try
            {
                CompanyStock mCompanyStock = mCompaniesStockRepository.GetCompanyStock(pIdCompany);

                return mCompanyStock;
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesStockManager -> GetCompanyStock:" + ex.InnerException);
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIdCompany"></param>
        /// <returns></returns>
        public List<WarehousePortal> GetWarehousePortalList(int pIdCompany)
        {
            try
            {
                List<WarehousePortal> mWarehousePortalList = mCompaniesStockRepository.GetWarehousePortalList(pIdCompany);


                return mWarehousePortalList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesStockManager -> GetWarehousePortalList:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Add(CompanyStock pCompanyStock)
        {
            try
            {
                if (string.IsNullOrEmpty(pCompanyStock.Formula))
                {
                    pCompanyStock.Formula = "A + B - C";
                }

                return mCompaniesStockRepository.Add(pCompanyStock);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesStockManager -> Add :" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(CompanyStock pCompany)
        {
            try
            {
                if (string.IsNullOrEmpty(pCompany.Formula))
                {
                    pCompany.Formula = "A + B - C";
                }

                return mCompaniesStockRepository.Update(pCompany);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesStockManager -> Update :" + ex.InnerException);
                throw ex;
            }
        }

        public bool AddWarehousePortalList(List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                return mCompaniesStockRepository.AddWarehousePortalList(pWarehousePortalList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesStockManager -> AddWarehousePortalList:" + ex.InnerException);
                throw ex;
            }
        }

        public bool UpdateWarehousePortalList(List<WarehousePortal> pWarehousePortalList, int pCompanyId)
        {
            try
            {
                return mCompaniesStockRepository.UpdateWarehousePortalList(pWarehousePortalList, pCompanyId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("CompaniesStockManager -> UpdateWarehousePortalList:" + ex.InnerException);
                throw ex;
            }
        }

    }
}
