﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Repository;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class PDCManager
    {
        PDCRepository mPDCRepository;

        public PDCManager()
        {
            mPDCRepository = new PDCRepository();
        }

        public ARGNSPDC GetPDCByID(CompanyConn pCc, string pPDCCode, ref PDCSAPCombo combo)
        {
            try
            {
                ARGNSPDC mARGNSPDC = new ARGNSPDC();
                mARGNSPDC = Mapper.Map<ARGNSPDC>(mPDCRepository.GetPDCById(pCc, pPDCCode, ref combo));
                return mARGNSPDC;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDCManager -> GetPDCByID:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSPDC> GetPDCList(CompanyConn pCc, ref PDCSAPCombo combo,DateTime? pPD,string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "")
        {
            try
            {
                return Mapper.Map<List<ARGNSPDC>>(mPDCRepository.GetPDCList(pCc, ref combo, pPD, pCode, pEmployee, pShift, pVendor));
             
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDCManager -> GetPDCList:" + ex.InnerException);
                throw ex;
            }
        }

        public PDCSAPCombo GetCombo(CompanyConn pCc)
        {
            try
            {
                return mPDCRepository.GetCombo(pCc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ARGNSPDCLine GetPDCLineCodeBar(CompanyConn pCc, string CodeBar)
        {
            try
            {
                return mPDCRepository.GetPDCLineCodeBar(pCc, CodeBar);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDCManager -> GetPDCLineCodeBar:" + ex.InnerException);
                throw ex;
            }
        }

        public List<ARGNSPDCLine> GetPDCLinesCutTick(CompanyConn pCc, string CutTick)
        {
            try
            {
                return mPDCRepository.GetPDCLinesCutTick(pCc, CutTick).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDCManager -> GetPDCLinesCutTick:" + ex.InnerException);
                throw ex;
            }
        }

        public string Add(CompanyConn pCc, ARGNSPDC model)
        {
            try
            {
                return mPDCRepository.Add(pCc, model);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PDCManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
