﻿using ARGNS.Model.Implementations;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class NewManager
    {
        NewRepository mNewRepository;
        public NewManager()
        {
            mNewRepository = new NewRepository();
        }

        public List<News> GetNews()
        {
            try
            {
                return mNewRepository.GetNews();
            }
            catch (Exception ex)
            {
                Logger.WriteError("NewManager -> GetNews:" + ex.InnerException);
                throw ex;
            }
        }

        public News GetNew(int Id)
        {
            try
            {
                return mNewRepository.GetNew(Id);
            }
            catch (Exception ex)
            {
                Logger.WriteError("NewManager -> GetNew:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Add(News pNews)
        {
            try
            {
                return mNewRepository.Add(pNews);
            }
            catch (Exception ex) 
            {

                Logger.WriteError("NewManager -> Add:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Update(News pNews)
        {
            try
            {
                return mNewRepository.Update(pNews);
            }
            catch (Exception ex)
            {
                Logger.WriteError("NewManager -> Update:" + ex.InnerException);
                throw ex;
            }
        }

        public bool Delete(News pNews)
        {
            try
            {
                return mNewRepository.Delete(pNews);
            }
            catch (Exception ex)
            {
                Logger.WriteError("NewManager -> Delete:" + ex.InnerException);
                throw ex;
            }
        }
    }
}
