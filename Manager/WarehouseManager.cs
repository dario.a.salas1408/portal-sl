﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Repository;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Manager
{
    public class WarehouseManager
    {
        private WarehouseRepository mWarehouseRepository;

        public WarehouseManager()
        {
            mWarehouseRepository = new WarehouseRepository();
        }

        public List<Warehouse> GetListWarehouse(CompanyConn pCc)
        {
            try
            {
                return mWarehouseRepository.GetListWarehouse(pCc); ;
            }
            catch (Exception ex)
            {
                Logger.WriteError("WarehouseManager -> GetListWarehouse: " + ex.InnerException);
                return null;
            }

        }

    }
}
