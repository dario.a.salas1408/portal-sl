﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace ARGNS.Util
{
    public class UDFUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObjType"></param>
        /// <param name="pTableName"></param>
        /// <param name="pListUDF"></param>
        /// <param name="pWhere"></param>
        /// <returns></returns>
        public static string GetSelectQuery(Type pObjType, string pTableName, List<UDF_ARGNS> pListUDF, string pWhere = null)
        {
            System.Reflection.PropertyInfo[] properties = pObjType.GetProperties();

            string selectTest = "SELECT 1";
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                if (property.Name != "MappedUdf")
                    selectTest += ", " + property.Name;
                else
                {
                    foreach (UDF_ARGNS mUDF in pListUDF)
                    {
                        selectTest += ", " + mUDF.UDFName;
                    }
                }
            }
            selectTest += " FROM [" + pTableName + "]";
            if (pWhere != null)
                selectTest += " WHERE " + pWhere;

            return selectTest;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSQLQuery"></param>
        /// <param name="pConnectionString"></param>
        /// <param name="pListUDF"></param>
        /// <param name="pObjType"></param>
        /// <param name="pTableName"></param>
        /// <returns></returns>
        public static List<Object> GetDBObjectList(string pSQLQuery, string pConnectionString, List<UDF_ARGNS> pListUDF, Type pObjType, string pTableName)
        {
            using (SqlConnection oSQLConnection = new SqlConnection(pConnectionString))
            {
                oSQLConnection.Open();
                SqlDataAdapter datosSQLDA = new SqlDataAdapter();
                DataTable dtSQLResults = new DataTable();
                DataTable dtSQLResultsUFD1 = new DataTable();
                try
                {
                    System.Data.SqlClient.SqlCommand comando = new SqlCommand();
                    comando.CommandType = CommandType.Text;

                    comando.CommandText = pSQLQuery;
                    comando.Connection = oSQLConnection;
                    datosSQLDA = new SqlDataAdapter(comando);
                    datosSQLDA.Fill(dtSQLResults);

                    comando.CommandText = "SELECT TableID, FieldID, IndexID, FldValue, Descr FROM UFD1 WHERE TableID = '" + pTableName + "'";
                    datosSQLDA = new SqlDataAdapter(comando);
                    datosSQLDA.Fill(dtSQLResultsUFD1);

                    List<Object> listToReturn = FromDataTableToList(dtSQLResults, dtSQLResultsUFD1, pObjType, pListUDF);

                    return listToReturn;
                }
                catch (SqlException ex)
                {
                    return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    //Close of the Open Connection
                    if (oSQLConnection.State == System.Data.ConnectionState.Open)
                    {
                        oSQLConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="dataTableUFD1"></param>
        /// <param name="className"></param>
        /// <param name="pListUDF"></param>
        /// <returns></returns>
        private static List<Object> FromDataTableToList(DataTable dataTable, DataTable dataTableUFD1, Type className, List<UDF_ARGNS> pListUDF)
        {
            IList<PropertyInfo> properties = className.GetProperties().ToList();
            List<Object> listToReturn = new List<Object>();
            DataColumnCollection columns = dataTable.Columns;

            foreach (DataRow row in dataTable.Rows)
            {
                Object item = Activator.CreateInstance(className);
                foreach (PropertyInfo property in properties)
                {
                    if (columns.Contains(property.Name))
                    {
                        property.SetValue(item, (row[property.Name] == DBNull.Value ? null : row[property.Name]), null);
                    }
                    else
                    {
                        if (property.Name == "MappedUdf")
                        {
                            List<UDF_ARGNS> auxUDF = new List<UDF_ARGNS>();
                            if (pListUDF != null)
                            {
                                foreach (UDF_ARGNS mUDF in pListUDF)
                                {
                                    if (columns.Contains(mUDF.UDFName))
                                    {
                                        UDF_ARGNS mUDFAux = new UDF_ARGNS();
                                        mUDFAux.FieldID = mUDF.FieldID;
                                        mUDFAux.ListUFD1 = mUDF.ListUFD1;
                                        mUDFAux.UDFName = mUDF.UDFName;
                                        mUDFAux.UDFDesc = mUDF.UDFDesc;
                                        mUDFAux.Value = (row[mUDFAux.UDFName] == DBNull.Value ? null : row[mUDFAux.UDFName].ToString());
                                        mUDFAux.RTable = mUDF.RTable;
                                        auxUDF.Add(mUDFAux);
                                    }
                                }
                                auxUDF = auxUDF.Select(c => { c.ListUFD1 = dataTableUFD1.AsEnumerable().Where(j => j.Field<short>("FieldID") == c.FieldID).Select(j => new UFD1_SAP { TableID = j.Field<string>("TableID"), FieldID = j.Field<short>("FieldID"), IndexID = j.Field<short>("IndexID"), FldValue = j.Field<string>("FldValue"), Descr = j.Field<string>("Descr") }).ToList(); return c; }).ToList();
                                property.SetValue(item, auxUDF, null);
                            }
                        }
                    }
                }
                listToReturn.Add(item);
            }
            return listToReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pListUDF"></param>
        /// <param name="pListUFD1"></param>
        /// <param name="pJsonObject"></param>
        /// <returns></returns>
        public static List<UDF_ARGNS> GetObjectListWithUDFHana(List<UDF_ARGNS> pListUDF, List<UFD1_SAP> pListUFD1, Newtonsoft.Json.Linq.JObject pJsonObject)
        {
            List<UDF_ARGNS> pListUDFReturn = new List<UDF_ARGNS>();
            if (pListUDF != null)
            {
                foreach (UDF_ARGNS mUDFAux in pListUDF)
                {
                    UDF_ARGNS mUDFToAdd = new UDF_ARGNS();
                    mUDFToAdd.UDFName = mUDFAux.UDFName;
                    mUDFToAdd.UDFDesc = mUDFAux.UDFDesc;
                    mUDFToAdd.Value = (string)pJsonObject[mUDFAux.UDFName];
                    mUDFToAdd.FieldID = mUDFAux.FieldID;
                    mUDFToAdd.TypeID = mUDFAux.TypeID;
                    mUDFToAdd.RTable = mUDFAux.RTable;
                    pListUDFReturn.Add(mUDFToAdd);
                }
            }
            if (pListUFD1 != null)
            {
                pListUDFReturn = pListUDFReturn.Select(c => { c.ListUFD1 = pListUFD1.Where(j => j.FieldID == c.FieldID).ToList(); return c; }).ToList();
            }

            return pListUDFReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pListUDF"></param>
        /// <param name="pWhere"></param>
        /// <param name="pObjectType"></param>
        /// <param name="pConnectionString"></param>
        /// <param name="pTableName"></param>
        /// <returns></returns>
        public static List<object> GetObjectListWithUDF(List<UDF_ARGNS> pListUDF, string pWhere, Type pObjectType, string pConnectionString, string pTableName)
        {
            string sqlquery = GetSelectQuery(pObjectType, pTableName, pListUDF, pWhere);
            List<Object> mAuxList = GetDBObjectList(sqlquery, pConnectionString, pListUDF, pObjectType, pTableName);

            return mAuxList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pXml"></param>
        /// <param name="pListUDF"></param>
        /// <returns></returns>
        public static string AddUDFToXml(string pXml, List<UDF_ARGNS> pListUDF)
        {
            foreach (UDF_ARGNS mUDF in pListUDF)
            {
                pXml = pXml + "<" + mUDF.UDFName + ">" + mUDF.Value + "</" + mUDF.UDFName + ">";
            }

            return pXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pXml"></param>
        /// <param name="pUDFName"></param>
        /// <param name="pUDFValue"></param>
        /// <returns></returns>
        public static string AddUDFToXml(string pXml, string pUDFName, string pUDFValue)
        {
            pXml = pXml + "<" + pUDFName + ">" + pUDFValue + "</" + pUDFName + ">";

            return pXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSQLQuery"></param>
        /// <param name="pConnectionString"></param>
        /// <param name="pListUDF"></param>
        /// <param name="pObjType"></param>
        /// <param name="pTableName"></param>
        /// <returns></returns>
        public static int GetCountValue(string pSQLQuery, string pConnectionString)
        {
            using (SqlConnection oSQLConnection = new SqlConnection(pConnectionString))
            {
                oSQLConnection.Open();
                SqlDataAdapter datosSQLDA = new SqlDataAdapter();
                DataTable dtSQLResults = new DataTable();
                DataTable dtSQLResultsUFD1 = new DataTable();
                try
                {
                    System.Data.SqlClient.SqlCommand comando = new SqlCommand();
                    comando.CommandType = CommandType.Text;

                    comando.CommandText = pSQLQuery;
                    comando.Connection = oSQLConnection;

                    return int.Parse(comando.ExecuteScalar().ToString());
                }
                catch (SqlException)
                {
                    return 0;
                }
                catch (Exception)
                {
                    return 0;
                }
                finally
                {
                    //Close of the Open Connection
                    if (oSQLConnection.State == System.Data.ConnectionState.Open)
                    {
                        oSQLConnection.Close();
                    }
                }
            }
        }
    }
}


