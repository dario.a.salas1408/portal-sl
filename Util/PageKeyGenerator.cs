﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Util
{
    public static class PageKeyGenerator
    {
        public static string GetPageKey()
        {
            string mPageKey = string.Empty;
            try
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                var stringChars = new char[8];
                var random = new Random();


                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }

                mPageKey = new String(stringChars);

            }
            catch (Exception ex)
            {
                Logger.WriteError("PageKeyGenerator -> GetPageKey:" + ex.InnerException);
            }
            return mPageKey;
        }
    }
}
