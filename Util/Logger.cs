﻿using ARGNS.Model;
using ARGNS.Model.Implementations;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Util
{
    public static class Logger
    {
        private static readonly ILog _logger = LogManager.GetLogger("Log");

        static Logger()
        {
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile));
        }


        public static void WriteError(string log)
        {
            _logger.Error(log);
        }

        public static void WriteInfo(string log)
        {
            _logger.Info(log);
        }

        public static string WriteErrorDb(Enums.Documents pDoc, int pIdUser, string pError, string pClave)
        {
            WebPortalModel Db = new WebPortalModel();

            Db.Logs.Add(new Log { IdUser = pIdUser, strLog = pError, dtLog = System.DateTime.Now, Tipo = pDoc.ToDescriptionString(), strClave = pClave });

            Db.SaveChanges();

            return pError;
        }
    }
}
