﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Util
{
    public class SQLQueryUtil
    {
        public static string getSelectQuery(string pSelect, string pTableName, List<UDF_ARGNS> pListUDF, string pWhere = null)
        {
            foreach (UDF_ARGNS mUDF in pListUDF)
            {
                pSelect += ", " + mUDF.UDFName;
            }
            pSelect += " FROM [" + pTableName + "]";
            if (pWhere != null)
                pSelect += " WHERE " + pWhere;

            return pSelect;
        }

        public static string getSelectQuery2(string pSelect, string pTableName, List<UDF_ARGNS> pListUDF, string pWhere = null, string pOrderBy = null, string pGroupBy = null, string pDBType = "SQL")
        {
            if(pListUDF != null)
            {
                foreach (UDF_ARGNS mUDF in pListUDF)
                {
                    pSelect += ", " + (pDBType == "HANA" ? "\"" : "") + mUDF.UDFName + (pDBType == "HANA" ? "\"" : "");
                }
            }
            pSelect += " " + pTableName;
            if (pWhere != null)
                pSelect += " " + pWhere;
            if (pGroupBy != null)
                pSelect += " " + pGroupBy;
            if (pOrderBy != null)
                pSelect += " " + pOrderBy;

            return pSelect;
        }

        public static DataTable GetQueryResult(string pSQLQuery, string pConnectionString)
        {
            using (SqlConnection oSQLConnection = new SqlConnection(pConnectionString))
            {
                oSQLConnection.Open();
                SqlDataAdapter datosSQLDA = new SqlDataAdapter();
                DataTable dtSQLResults = new DataTable();
                try
                {
                    System.Data.SqlClient.SqlCommand comando = new SqlCommand();
                    comando.CommandType = CommandType.Text;

                    comando.CommandText = pSQLQuery;
                    comando.Connection = oSQLConnection;
                    datosSQLDA = new SqlDataAdapter(comando);
                    datosSQLDA.Fill(dtSQLResults);

                    return dtSQLResults;
                }
                catch (SqlException ex)
                {
                    return null;
                }
                finally
                {
                    //Close of the Open Connection
                    if (oSQLConnection.State == System.Data.ConnectionState.Open)
                    {
                        oSQLConnection.Close();
                    }
                }
            }
        }
    }
}
