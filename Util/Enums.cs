﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Util
{
    public class Enums
    {
        public enum FolioType
        {

            [Description("Sale Draft")]
            SaleDraft,
            [Description("Sale Order")]
            SaleOrder
        };

        public enum DiverSOItemMethod
        {
            [Description("getSubstitutes")]
            Substitutes,
            [Description("getComparatives")]
            Comparatives,
            [Description("getComplementaries")]
            Complementaries
        };



        public enum ServiceResult
        {

            [Description("Ok")]
            Ok,
            [Description("Error")]
            Error
        };

        public enum Pages
        {
            [Description("WP-ABMCompanies")]
            ABMCompanies,
            [Description("WP-ABMUsers")]
            ABMUsers,
            [Description("WPABMNews")]
            ABMNews,
            [Description("WP-ABMQuery")]
            ABMQuery,
            [Description("WP-ABMChart")]
            ABMChart,
            [Description("PLM-StyleGrid")]
            StyleGrid,
            [Description("PLM-StylList")]
            StyleList,
            [Description("PLM-MyStyleGrid")]
            MyStyleGrid,
            [Description("PLM-MyStyleList")]
            MyStyleList,
            [Description("PLM-MyCostSheet")]
            MyCostSheet,
            [Description("PLM-CostSheet")]
            CostSheet,
            [Description("PLM-ColorMaster")]
            ColorMaster,
            [Description("PLM-ScaleMaster")]
            ScaleMaster,
            [Description("PLM-VarMaster")]
            VarMaster,
            [Description("PLM-Activity")]
            Activity,
            [Description("PLM-MyActivities")]
            PLMMyActivities,
            [Description("PLM-PDC")]
            PDC,
            [Description("PLM-MaterialDetail")]
            MaterialDetail,
            [Description("PLM-Shipment")]
            Shipment,
            [Description("PLM-MyShipment")]
            MyShipment,
            [Description("PLM-Reports")]
            PLMReports,
            [Description("PU-PurchaseOrder")]
            PurchaseOrder,
            [Description("PU-PurchaseQuotation")]
            PurchaseQuotation,
            [Description("PU-PurchaseInvoice")]
            PurchaseInvoice,
            [Description("SO-SalesOrder")]
            SalesOrder,
            [Description("SO-SalesQuotation")]
            SalesQuotation,
            [Description("PU-PurchaseRequest")]
            PurchaseRequest,
            [Description("PU-Draft")]
            PurchaseDraft,
            [Description("PU-Approval")]
            PurchaseApproval,
            [Description("SO-Draft")]
            SalesDraft,
            [Description("SO-SalesInvoice")]
            SalesInvoice,
            [Description("SO-SalesApproval")]
            SalesApproval,
            [Description("MD-BP")]
            ABMBP,
            [Description("MD-MyBP")]
            ABMMyBP,
            [Description("MD-CustomerAging")]
            CustomerAging,
            [Description("PLM-PDM")]
            ProductDataManagement,
            [Description("SO-Opportunities")]
            OpportunitiesList,
            [Description("SO-MyOpportunities")]
            MyOpportunitiesList,
            [Description("ServCall")]
            ServiceCallList,
            [Description("MyServCall")]
            MyServiceCallList,
            [Description("CRM-Activities")]
            CRMActivities,
            [Description("CRM-MyActivities")]
            CRMMyActivities,
            [Description("PLM-RangePlan")]
            PLMRangePlan,
            [Description("MD-ItemMaster")]
            ItemMaster,
            [Description("PLM-CPath")]
            PLMCPath,
            [Description("PLM-TechPack")]
            PLMTechPack,
            [Description("SO-QuickOrder")]
            SOQuickOrder,
            [Description("MD-MyCustomerAging")]
            MyCustomerAging,
            [Description("MD-InvStatusRep")]
            InventoryStatus
        };

        public enum Actions
        {
            [Description("KeyList")]
            List,
            [Description("KeyAdd")]
            Add,
            [Description("KeyDelete")]
            Delete,
            [Description("KeyUpdate")]
            Update,
            [Description("KeyView")]
            View,
            [Description("KeyAsigCompany")]
            AsigCompany,
            [Description("KeyExecuteQuery")]
            ExecuteQuery,
            [Description("StockMKTDoc")]
            StockMKTDoc, 
            [Description("ShowOpenQuantity")]
            ShowOpenQuantity
        };

        public enum Rols
        {
            [Description("ADMINISTRATOR")]
            Administrator,

            [Description("BUSINESS PARTNER")]
            BusinessPartner,

            [Description("SALES EMPLOYEE")]
            SalesEmployee,

            [Description("USER")]
            User
        };


        public enum Areas
        {
            [Description("WEBPORTAL")]
            WEBPORTAL,
            [Description("PLM")]
            PLM,
            [Description("WMS")]
            WMS,
            [Description("PURCHASE")]
            Purchase,
            [Description("SALES")]
            Sales,
            [Description("MASTERDATA")]
            MasterData,
            [Description("SERVICECALL")]
            ServiceCall,
            [Description("CRM")]
            CRM
        };

        public enum BpType
        {
            [Description("S")]
            Vendor,
            [Description("C")]
            Customer,
            [Description("L")]
            Lead,
            [Description("A")]
            All
        };

        public enum DocumentStatus
        {
            [Description("O")]
            Open,
            [Description("C")]
            Closed
        };


        public enum eServerType
        {
            dst_MSSQL = 1,
            dst_MSSQL2005 = 4,
            dst_MSSQL2008 = 6,
            dst_MSSQL2012 = 7,
            dst_MSSQL2014 = 8,
            dst_HANA = 9
        }

        public enum Documents
        {
            [Description("PO")]
            PurchaseOrder,
            [Description("PQ")]
            PurchaseQuotation,
            [Description("PR")]
            PurchaseRequest,
            [Description("PI")]
            PurchaseInvoice,
            [Description("SO")]
            SalesOrder,
            [Description("SQ")]
            SalesQuotation,
            [Description("SD")]
            SalesDraft,
            [Description("SA")]
            SalesApproval,
            [Description("SI")]
            SalesInvoice,
            [Description("PDM")]
            PDM,
            [Description("CostSheet")]
            CostSheet,
            [Description("Design")]
            Design,
            [Description("Activity")]
            Activity,
            [Description("PDC")]
            PDC,
            [Description("Comment")]
            Comment,
            [Description("Sample")]
            Sample,
            [Description("TechPack")]
            TechPack,
            [Description("Color")]
            Color,
        };

        public enum DocumentsType
        {
            [Description("SalesOrder")]
            SO = 17,
        }

        public enum Decimals
        {
            [Description("Amount")]
            Amount,
            [Description("Price")]
            Price,
            [Description("Rate")]
            Rate,
            [Description("Quantity")]
            Quantity,
            [Description("Percent")]
            Percent,
            [Description("Unit")]
            Unit,
            [Description("DecimalInQuery")]
            DecimalInQuery
        };

        public class StringValue : System.Attribute
        {
            private string _value;

            public StringValue(string value)
            {
                _value = value;
            }

            public string Value
            {
                get { return _value; }
            }

        }

        


    }

    public static class EnumDesc {
        public static string GetDescription(this Enum e)
        {
            var attribute =
                e.GetType()
                    .GetTypeInfo()
                    .GetMember(e.ToString())
                    .FirstOrDefault(member => member.MemberType == MemberTypes.Field)
                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
                    .SingleOrDefault()
                    as DescriptionAttribute;

            return attribute?.Description ?? e.ToString();
        }
    }

    public static class Test
    {
        public static T StringToEnum<T>(string name)
        {
            return (T)Enum.Parse(typeof(T), name);
        }

        public static string ToDescriptionString(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
    }
    
}
