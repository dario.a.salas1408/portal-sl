﻿using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ARGNS.Util
{
    public class Miscellaneous
    {
		/// <summary>
		/// 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string ReplaceUTF(string str)
		{
            try
            {
                if (str == null)
                {
                    return "";
                }
                else
                {
                    string stFormD = str.Normalize(NormalizationForm.FormD);
                    StringBuilder sb = new StringBuilder();

                    for (int ich = 0; ich < stFormD.Length; ich++)
                    {
                        UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                        if (uc != UnicodeCategory.NonSpacingMark)
                        {
                            sb.Append(stFormD[ich]);
                        }
                    }

                    return (sb.ToString().Normalize(NormalizationForm.FormC));
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="mPDMSAPCombo"></param>
		/// <param name="mJsonObjectResult"></param>
		public void ReplaceCodesByDescripcionInModel(PDMSAPCombo mPDMSAPCombo,
            JsonObjectResult mJsonObjectResult)
        {
            //Replace string values

            foreach (var item in mJsonObjectResult.ARGNSModelList)
            {
                //season
                if (item.U_Season != null)
                {
                    ARGNSSeason season = mPDMSAPCombo.ListSeason
                            .Where(w => w.Code == item.U_Season)
                            .FirstOrDefault();

                    if (season != null)
                    {
                        item.U_Season = season.Name;
                    }
                }

                //item group
                if (item.U_ModGrp != null)
                {
                    ARGNSModelGroup group = mPDMSAPCombo.ListModelGroup
                            .Where(w => w.Code == item.U_ModGrp)
                            .FirstOrDefault();

                    if (group != null)
                    {
                        item.U_ModGrp = group.Name;
                    }
                }

                //brand
                if (item.U_Brand != null)
                {
                    ARGNSBrand brand = mPDMSAPCombo.ListBrand
                            .Where(w => w.Code == item.U_Brand)
                            .FirstOrDefault();

                    if (brand != null)
                    {
                        item.U_Brand = brand.Name;
                    }
                }

                //collection
                if (item.U_CollCode != null)
                {
                    ARGNSCollection collection = mPDMSAPCombo.ListCollection
                            .Where(w => w.U_CollCode == item.U_CollCode)
                            .FirstOrDefault();

                    if (collection != null)
                    {
                        item.U_CollCode = collection.U_Desc;
                    }
                }

                //subcollection
                if (item.U_AmbCode != null)
                {
                    ARGNSSubCollection subCollection = mPDMSAPCombo.ListSubCollection
                            .Where(w => w.U_AmbCode == item.U_AmbCode)
                            .FirstOrDefault();

                    if (subCollection != null)
                    {
                        item.U_AmbCode = subCollection.U_Desc;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pColumnToOrder"></param>
        /// <param name="pDBType"></param>
        /// <returns></returns>
        public static string OrderString(OrderColumn pColumnToOrder, string pDBType = "SQL")
        {
            string mReturnString = "";
            if (pColumnToOrder != null)
            {
                if (pColumnToOrder.SortDirection == OrderColumn.OrderDirection.Ascendant)
                    mReturnString = (pDBType == "HANA" ? "\"" : "") + pColumnToOrder.Name + (pDBType == "HANA" ? "\"" : "") + " asc";
                else
                    mReturnString = (pDBType == "HANA" ? "\"" : "") + pColumnToOrder.Name + (pDBType == "HANA" ? "\"" : "") + " desc";
            }
            return mReturnString;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pAvailable"></param>
        /// <param name="pStockModelView"></param>
        /// <returns></returns>
        public static decimal GetAvailable(string[] pAvailable, StockModel pStockModelView, bool isPrepack = false)
        {
            try
            {
                decimal? mAvailable = 0;

                bool mAvChange = false;

                for (int i = 0; i < pAvailable.Length - 1; i++)
                {
                    switch (pAvailable[i])
                    {
                        case "A":
                            mAvailable = GetABCValue(pStockModelView, pAvailable[i], isPrepack);
                            mAvChange = true;
                            break;
                        case "B":
                            mAvailable = GetABCValue(pStockModelView, pAvailable[i], isPrepack);
                            mAvChange = true;
                            break;
                        case "C":
                            mAvailable = GetABCValue(pStockModelView, pAvailable[i], isPrepack);
                            mAvChange = true;
                            break;
                    }

                    if (mAvChange)
                    {
                        break;
                    }

                }

                for (int i = 0; i < pAvailable.Length - 1; i++)
                {
                    switch (pAvailable[i])
                    {
                        case "+":

                            i++;

                            if (i > pAvailable.Length)
                            {
                                break;
                            }

                            if (pAvailable[i] != "")
                            {
                                mAvailable = mAvailable + GetABCValue(pStockModelView, pAvailable[i], isPrepack);
                            }

                            break;

                        case "-":

                            i++;

                            if (i > pAvailable.Length)
                            {
                                break;
                            }

                            if (pAvailable[i] != "")
                            {
                                mAvailable = mAvailable - GetABCValue(pStockModelView, pAvailable[i], isPrepack);
                            }

                            break;

                        case "/":

                            i++;

                            if (i > pAvailable.Length)
                            {
                                break;
                            }

                            if (pAvailable[i] != "")
                            {
                                mAvailable = mAvailable / GetABCValue(pStockModelView, pAvailable[i], isPrepack);
                            }

                            break;

                        case "*":

                            i++;

                            if (i > pAvailable.Length)
                            {
                                break;
                            }

                            if (pAvailable[i] != "")
                            {
                                mAvailable = mAvailable * GetABCValue(pStockModelView, pAvailable[i], isPrepack);
                            }

                            break;

                        default:
                            break;
                    }
                }

                return Convert.ToDecimal(mAvailable);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pStockModelView"></param>
		/// <param name="pVar"></param>
		/// <returns></returns>
        public static decimal GetABCValue(StockModel pStockModelView, string pVar, bool isPrepack = false)
        {
            try
            {
                decimal mAvailable = 0;

                decimal? mA = pStockModelView.OnOrder;
                decimal? mB = isPrepack == true ? pStockModelView.OnHandPrePack : pStockModelView.OnHand;
                decimal? mC = pStockModelView.IsCommited;

                switch (pVar)
                {
                    case "A":
                        mAvailable = Convert.ToDecimal(mA);
                        break;
                    case "B":
                        mAvailable = Convert.ToDecimal(mB);
                        break;
                    case "C":
                        mAvailable = Convert.ToDecimal(mC);
                        break;
                }

                return mAvailable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
