﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Management;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.Util
{
    public class Security
    {
        public string GetKey(string pProduct, int CountLicenses)
        {

            //Retorna el Serial del Procesador
            ManagementObjectCollection mbsList = null;
            ManagementObjectSearcher mbs = new ManagementObjectSearcher("Select * From Win32_processor");
            mbsList = mbs.Get();
            string mSerialPro = "";
            foreach (ManagementObject mo in mbsList)
            {
                mSerialPro = mo["ProcessorID"].ToString();
            }

            //Retorna serial Placa Madre
            ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
            ManagementObjectCollection moc = mos.Get();
            string motherBoard = "";
            foreach (ManagementObject mo in moc)
            {
                motherBoard = (string)mo["SerialNumber"];
            }

            motherBoard = motherBoard.Replace('/','%');

            //Retorna Serial Hd
            string drive = "C";
            ManagementObject dsk = new ManagementObject(
                @"win32_logicaldisk.deviceid=""" + drive + @":""");
            dsk.Get();
            string volumeSerial = dsk["VolumeSerialNumber"].ToString();

            //Retorna la Mac Address
            ManagementObjectSearcher objMOS = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where ipenabled = true");
            ManagementObjectCollection objMOC = objMOS.Get();
            string MACAddress = String.Empty;
            foreach (ManagementObject objMO in objMOC)
            {
                if (MACAddress == String.Empty)
                {
                    MACAddress = objMO["MacAddress"].ToString();
                }
                objMO.Dispose();
            }

            return motherBoard + "/" + MACAddress + "/" + pProduct + "/" + CountLicenses + "/" + mSerialPro + "/" + volumeSerial;
        }

        public string GetKeyUser(string pProduct, int pIdUser)
        {
            var mRa1 = GetRandom();
            var mRa2 = GetRandom();
            var mRa3 = GetRandom();

            return pIdUser.ToString() + "-" + mRa1 + "-" + pProduct + "-" + mRa2 + "-" + "WEBADD" + "-" + mRa3;
        }

        public string GetKeyPage(bool UseSap, int pIdPage)
        {
            var mRa1 = GetRandom();
            var mRa2 = GetRandom();
            var mRa3 = GetRandom();

            return pIdPage.ToString() + "-" + mRa1 + "-" + UseSap.ToString() + "-" + mRa2 + "-" + "SYSTEMADD" + "-" + mRa3;
        }

        public string GetRandom()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            return finalString;
        }

        private static byte[] Encrypt(byte[] clearText, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(clearText, 0, clearText.Length);
            cs.Close();
            byte[] encryptedData = ms.ToArray();
            return encryptedData;
        }

        public string Encrypt(string clearText, string Password)
        {
            byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(clearText);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            byte[] encryptedData = Encrypt(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            return Convert.ToBase64String(encryptedData);
        }

        private static byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(cipherData, 0, cipherData.Length);
            cs.Close();
            byte[] decryptedData = ms.ToArray();
            return decryptedData;
        }

        public string Decrypt(string cipherText, string Password)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            byte[] decryptedData = Decrypt(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            return System.Text.Encoding.Unicode.GetString(decryptedData);
        }

        public bool CkeckKey(string pKey, string pProduct)
        {
            try
            {
                //string KeyDesc = Decrypt(pKey, "Argentis");

                //string[] mHardware = KeyDesc.Split('/');

                //if (pProduct != mHardware[2])
                //{ return false; }

                ////Retorna el Serial del Procesador
                //ManagementObjectCollection mbsList = null;
                //ManagementObjectSearcher mbs = new ManagementObjectSearcher("Select * From Win32_processor");
                //mbsList = mbs.Get();
                //string mSerialPro = "";
                //foreach (ManagementObject mo in mbsList)
                //{
                //    mSerialPro = mo["ProcessorID"].ToString();
                //}

                //if (mSerialPro != mHardware[4])
                //{ return false; }

                ////Retorna serial Placa Madre
                //ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
                //ManagementObjectCollection moc = mos.Get();
                //string motherBoard = "";
                //foreach (ManagementObject mo in moc)
                //{
                //    motherBoard = (string)mo["SerialNumber"];
                //}

                //if (motherBoard != mHardware[0].Replace('%', '/'))
                //{ return false; }

                ////Retorna Serial Hd
                //string drive = "C";
                //ManagementObject dsk = new ManagementObject(
                //    @"win32_logicaldisk.deviceid=""" + drive + @":""");
                //dsk.Get();
                //string volumeSerial = dsk["VolumeSerialNumber"].ToString();

                //if (volumeSerial != mHardware[5])
                //{ return false; }

                ////Retorna la Mac Address
                //ManagementObjectSearcher objMOS = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration where ipenabled = true");
                //ManagementObjectCollection objMOC = objMOS.Get();
                //string MACAddress = String.Empty;
                //foreach (ManagementObject objMO in objMOC)
                //{
                //    if (MACAddress == String.Empty)
                //    {
                //        MACAddress = objMO["MacAddress"].ToString();
                //    }
                //    objMO.Dispose();
                //}

                //if (MACAddress != mHardware[1])
                //{ return false; }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("Security -> CkeckKey:" + ex.InnerException);
                return false;
            }

        }

        public bool CkeckPage(string pKey, int pIdPage, out bool UseSap)
        {
            try
            {
                string KeyDesc = Decrypt(pKey, "Argentis");

                string[] mPage = KeyDesc.Split('-');

                UseSap = true;

                if (mPage[4] != "SYSTEMADD")
                { return false; }

                if (mPage[0] != pIdPage.ToString())
                { return false; }

                if (mPage[2] != "True")
                { UseSap = false; }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("Security -> CkeckPage:" + ex.InnerException);
                UseSap = true;
                return false;
            }

        }

        public bool CkeckUser(string pKey, string pProduct, int pIdUse)
        {
            try
            {

                string KeyDesc = Decrypt(pKey, "Argentis");

                string[] mValues = KeyDesc.Split('-');

                if (mValues[2] != pProduct)
                {
                    return false;
                }

                if (mValues[0] != pIdUse.ToString())
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("Security -> CkeckUser:" + ex.InnerException);
                return false;
            }

        }

        public bool CkeckUserintegrity(string pKey, int pIdUse)
        {
            try
            {
                string KeyDesc = Decrypt(pKey, "Argentis");

                string[] mValues = KeyDesc.Split('-');

                if (mValues[4] != "WEBADD")
                {
                    return false;
                }

                if (mValues[0] != pIdUse.ToString())
                {
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("Security -> CkeckUserintegrity:" + ex.InnerException);
                return false;
            }

        }

        //Get Count Licence Assigned 
        public int GetCountLCAssigned(string pKeyUserAreas)
        {
            try
            {
                string KeyDesc = Decrypt(pKeyUserAreas, "Argentis");

                if (KeyDesc.Split('-')[4] == "WEBADD")
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("Security -> GetCountLCAssigned:" + ex.InnerException);

                return 0;
            }
        }

        //Get Count Product Licenses  
        public int GetCountProductLc(string pKeyProduct)
        {
            try
            {
                string KeyDesc = Decrypt(pKeyProduct, "Argentis");

                return Convert.ToInt32(KeyDesc.Split('/')[3]);
            }
            catch (Exception ex)
            {
                Logger.WriteError("Security -> GetCountProductLc:" + ex.InnerException);

                return 0;
            }
        }

    }
}
