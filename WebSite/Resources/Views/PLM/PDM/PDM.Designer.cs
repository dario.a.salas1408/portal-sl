﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ARGNS.WebSite.Resources.Views.PLM.PDM {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PDM {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PDM() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ARGNS.WebSite.Resources.Views.PLM.PDM.PDM", typeof(PDM).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        public static string Active {
            get {
                return ResourceManager.GetString("Active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activities.
        /// </summary>
        public static string Activities {
            get {
                return ResourceManager.GetString("Activities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SAP Item Group.
        /// </summary>
        public static string AG {
            get {
                return ResourceManager.GetString("AG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add New Model.
        /// </summary>
        public static string AM {
            get {
                return ResourceManager.GetString("AM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Archive.
        /// </summary>
        public static string Archive {
            get {
                return ResourceManager.GetString("Archive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Assigned To.
        /// </summary>
        public static string AT {
            get {
                return ResourceManager.GetString("AT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Attachments.
        /// </summary>
        public static string Attachments {
            get {
                return ResourceManager.GetString("Attachments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Available.
        /// </summary>
        public static string Available {
            get {
                return ResourceManager.GetString("Available", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By Code.
        /// </summary>
        public static string BC {
            get {
                return ResourceManager.GetString("BC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By Name.
        /// </summary>
        public static string BN {
            get {
                return ResourceManager.GetString("BN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Partner.
        /// </summary>
        public static string BP {
            get {
                return ResourceManager.GetString("BP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Brand.
        /// </summary>
        public static string Brand {
            get {
                return ResourceManager.GetString("Brand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Calendar.
        /// </summary>
        public static string Calendar {
            get {
                return ResourceManager.GetString("Calendar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code Cost Sheet.
        /// </summary>
        public static string CCS {
            get {
                return ResourceManager.GetString("CCS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ship Cancel Date.
        /// </summary>
        public static string CD {
            get {
                return ResourceManager.GetString("CD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Log.
        /// </summary>
        public static string ChangeLog {
            get {
                return ResourceManager.GetString("ChangeLog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Choose.
        /// </summary>
        public static string Choose {
            get {
                return ResourceManager.GetString("Choose", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to City.
        /// </summary>
        public static string City {
            get {
                return ResourceManager.GetString("City", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code.
        /// </summary>
        public static string Code {
            get {
                return ResourceManager.GetString("Code", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Collection.
        /// </summary>
        public static string Coll {
            get {
                return ResourceManager.GetString("Coll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Colors.
        /// </summary>
        public static string Colors {
            get {
                return ResourceManager.GetString("Colors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Colorway.
        /// </summary>
        public static string Colorway {
            get {
                return ResourceManager.GetString("Colorway", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comment.
        /// </summary>
        public static string Comments {
            get {
                return ResourceManager.GetString("Comments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Committed.
        /// </summary>
        public static string Committed {
            get {
                return ResourceManager.GetString("Committed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Composition.
        /// </summary>
        public static string Composition {
            get {
                return ResourceManager.GetString("Composition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cost Sheets.
        /// </summary>
        public static string CS {
            get {
                return ResourceManager.GetString("CS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cost Sheet List.
        /// </summary>
        public static string CSL {
            get {
                return ResourceManager.GetString("CSL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Currency.
        /// </summary>
        public static string Currency {
            get {
                return ResourceManager.GetString("Currency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer.
        /// </summary>
        public static string Customer {
            get {
                return ResourceManager.GetString("Customer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Colorway Code.
        /// </summary>
        public static string CWC {
            get {
                return ResourceManager.GetString("CWC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date.
        /// </summary>
        public static string Date {
            get {
                return ResourceManager.GetString("Date", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Department.
        /// </summary>
        public static string Department {
            get {
                return ResourceManager.GetString("Department", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string Desc {
            get {
                return ResourceManager.GetString("Desc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Decrip. Language.
        /// </summary>
        public static string DescLang {
            get {
                return ResourceManager.GetString("DescLang", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Designer.
        /// </summary>
        public static string Designer {
            get {
                return ResourceManager.GetString("Designer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date From.
        /// </summary>
        public static string DF {
            get {
                return ResourceManager.GetString("DF", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Division.
        /// </summary>
        public static string Division {
            get {
                return ResourceManager.GetString("Division", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Doc. Numbering.
        /// </summary>
        public static string DN {
            get {
                return ResourceManager.GetString("DN", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document.
        /// </summary>
        public static string Document {
            get {
                return ResourceManager.GetString("Document", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Documents.
        /// </summary>
        public static string Documents {
            get {
                return ResourceManager.GetString("Documents", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ship Start Date.
        /// </summary>
        public static string DOS {
            get {
                return ResourceManager.GetString("DOS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date To.
        /// </summary>
        public static string DT {
            get {
                return ResourceManager.GetString("DT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employee.
        /// </summary>
        public static string Employee {
            get {
                return ResourceManager.GetString("Employee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Field Changed.
        /// </summary>
        public static string FieldChange {
            get {
                return ResourceManager.GetString("FieldChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File.
        /// </summary>
        public static string File {
            get {
                return ResourceManager.GetString("File", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Grid.
        /// </summary>
        public static string Grid {
            get {
                return ResourceManager.GetString("Grid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Height.
        /// </summary>
        public static string Height {
            get {
                return ResourceManager.GetString("Height", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hoop Size.
        /// </summary>
        public static string HS {
            get {
                return ResourceManager.GetString("HS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Image.
        /// </summary>
        public static string Image {
            get {
                return ResourceManager.GetString("Image", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instance.
        /// </summary>
        public static string Instance {
            get {
                return ResourceManager.GetString("Instance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lead.
        /// </summary>
        public static string Lead {
            get {
                return ResourceManager.GetString("Lead", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Line.
        /// </summary>
        public static string Line {
            get {
                return ResourceManager.GetString("Line", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to List.
        /// </summary>
        public static string List {
            get {
                return ResourceManager.GetString("List", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Location.
        /// </summary>
        public static string Location {
            get {
                return ResourceManager.GetString("Location", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log Comments.
        /// </summary>
        public static string LogComments {
            get {
                return ResourceManager.GetString("LogComments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lead Time Planned.
        /// </summary>
        public static string LTP {
            get {
                return ResourceManager.GetString("LTP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Cost Sheet.
        /// </summary>
        public static string MCS {
            get {
                return ResourceManager.GetString("MCS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Model.
        /// </summary>
        public static string Model {
            get {
                return ResourceManager.GetString("Model", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Model Code.
        /// </summary>
        public static string ModelCode {
            get {
                return ResourceManager.GetString("ModelCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Model Description.
        /// </summary>
        public static string ModelDesc {
            get {
                return ResourceManager.GetString("ModelDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Main Warehouse.
        /// </summary>
        public static string MW {
            get {
                return ResourceManager.GetString("MW", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Design.
        /// </summary>
        public static string ND {
            get {
                return ResourceManager.GetString("ND", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Data To Display.
        /// </summary>
        public static string NDTD {
            get {
                return ResourceManager.GetString("NDTD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Value.
        /// </summary>
        public static string NewValue {
            get {
                return ResourceManager.GetString("NewValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old Value.
        /// </summary>
        public static string OldValue {
            get {
                return ResourceManager.GetString("OldValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to On Hand.
        /// </summary>
        public static string OnHand {
            get {
                return ResourceManager.GetString("OnHand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ordered.
        /// </summary>
        public static string Ordered {
            get {
                return ResourceManager.GetString("Ordered", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Owner.
        /// </summary>
        public static string Owner {
            get {
                return ResourceManager.GetString("Owner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Planned Date From.
        /// </summary>
        public static string PDF {
            get {
                return ResourceManager.GetString("PDF", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Planned Date To.
        /// </summary>
        public static string PDT {
            get {
                return ResourceManager.GetString("PDT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Group.
        /// </summary>
        public static string PG {
            get {
                return ResourceManager.GetString("PG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Line.
        /// </summary>
        public static string PL {
            get {
                return ResourceManager.GetString("PL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Point of measure.
        /// </summary>
        public static string POM {
            get {
                return ResourceManager.GetString("POM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Position.
        /// </summary>
        public static string Position {
            get {
                return ResourceManager.GetString("Position", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price.
        /// </summary>
        public static string Price {
            get {
                return ResourceManager.GetString("Price", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price List.
        /// </summary>
        public static string PriceList {
            get {
                return ResourceManager.GetString("PriceList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Priority.
        /// </summary>
        public static string Priority {
            get {
                return ResourceManager.GetString("Priority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Info.
        /// </summary>
        public static string ProducInfo {
            get {
                return ResourceManager.GetString("ProducInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        public static string Product {
            get {
                return ResourceManager.GetString("Product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product  List.
        /// </summary>
        public static string ProductList {
            get {
                return ResourceManager.GetString("ProductList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Projects.
        /// </summary>
        public static string Projects {
            get {
                return ResourceManager.GetString("Projects", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Project was updated correctly.
        /// </summary>
        public static string ProjectUpdateMessage {
            get {
                return ResourceManager.GetString("ProjectUpdateMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantity.
        /// </summary>
        public static string Quantity {
            get {
                return ResourceManager.GetString("Quantity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantity to order.
        /// </summary>
        public static string RequiredQuantity {
            get {
                return ResourceManager.GetString("RequiredQuantity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SAP Activity.
        /// </summary>
        public static string SAPActivity {
            get {
                return ResourceManager.GetString("SAPActivity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stich Count.
        /// </summary>
        public static string SC {
            get {
                return ResourceManager.GetString("SC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scale.
        /// </summary>
        public static string Scale {
            get {
                return ResourceManager.GetString("Scale", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sub Collections.
        /// </summary>
        public static string SColl {
            get {
                return ResourceManager.GetString("SColl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        public static string Search {
            get {
                return ResourceManager.GetString("Search", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Season.
        /// </summary>
        public static string Season {
            get {
                return ResourceManager.GetString("Season", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Section.
        /// </summary>
        public static string Section {
            get {
                return ResourceManager.GetString("Section", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sections.
        /// </summary>
        public static string Sections {
            get {
                return ResourceManager.GetString("Sections", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Segmentation.
        /// </summary>
        public static string Segmentation {
            get {
                return ResourceManager.GetString("Segmentation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Care Instruction Group.
        /// </summary>
        public static string SG {
            get {
                return ResourceManager.GetString("SG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show Differences.
        /// </summary>
        public static string ShowDiff {
            get {
                return ResourceManager.GetString("ShowDiff", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sizes.
        /// </summary>
        public static string Sizes {
            get {
                return ResourceManager.GetString("Sizes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stock.
        /// </summary>
        public static string Stock {
            get {
                return ResourceManager.GetString("Stock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stock Prepack.
        /// </summary>
        public static string StockPrepack {
            get {
                return ResourceManager.GetString("StockPrepack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Style.
        /// </summary>
        public static string Style {
            get {
                return ResourceManager.GetString("Style", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sub Collection.
        /// </summary>
        public static string SubColl {
            get {
                return ResourceManager.GetString("SubColl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Range Plan was updated correctly..
        /// </summary>
        public static string SuccessfullyUpdated {
            get {
                return ResourceManager.GetString("SuccessfullyUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Secondary Warehouse.
        /// </summary>
        public static string SW {
            get {
                return ResourceManager.GetString("SW", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tech Pack.
        /// </summary>
        public static string TechPack {
            get {
                return ResourceManager.GetString("TechPack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Title.
        /// </summary>
        public static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Line.
        /// </summary>
        public static string TL {
            get {
                return ResourceManager.GetString("TL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Tech Pack was updated correctly.
        /// </summary>
        public static string TTPWUC {
            get {
                return ResourceManager.GetString("TTPWUC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        public static string Type {
            get {
                return ResourceManager.GetString("Type", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Model.
        /// </summary>
        public static string UM {
            get {
                return ResourceManager.GetString("UM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Updated.
        /// </summary>
        public static string Updated {
            get {
                return ResourceManager.GetString("Updated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Updated By.
        /// </summary>
        public static string UpdatedBy {
            get {
                return ResourceManager.GetString("UpdatedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Tech Pack.
        /// </summary>
        public static string UpdateTechPack {
            get {
                return ResourceManager.GetString("UpdateTechPack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User.
        /// </summary>
        public static string User {
            get {
                return ResourceManager.GetString("User", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Variable.
        /// </summary>
        public static string Variable {
            get {
                return ResourceManager.GetString("Variable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Variant.
        /// </summary>
        public static string Variant {
            get {
                return ResourceManager.GetString("Variant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View details.
        /// </summary>
        public static string VD {
            get {
                return ResourceManager.GetString("VD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vendor.
        /// </summary>
        public static string Vendor {
            get {
                return ResourceManager.GetString("Vendor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View.
        /// </summary>
        public static string View {
            get {
                return ResourceManager.GetString("View", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View Tech Pack.
        /// </summary>
        public static string ViewTechPack {
            get {
                return ResourceManager.GetString("ViewTechPack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print Techpack.
        /// </summary>
        public static string VT {
            get {
                return ResourceManager.GetString("VT", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warehouse.
        /// </summary>
        public static string Wh {
            get {
                return ResourceManager.GetString("Wh", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warehouse Code.
        /// </summary>
        public static string WhCode {
            get {
                return ResourceManager.GetString("WhCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Warehouse Name.
        /// </summary>
        public static string WhName {
            get {
                return ResourceManager.GetString("WhName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Width.
        /// </summary>
        public static string Width {
            get {
                return ResourceManager.GetString("Width", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Workflow.
        /// </summary>
        public static string Workflow {
            get {
                return ResourceManager.GetString("Workflow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Write the message!.
        /// </summary>
        public static string WriteMessage {
            get {
                return ResourceManager.GetString("WriteMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Year.
        /// </summary>
        public static string Year {
            get {
                return ResourceManager.GetString("Year", resourceCulture);
            }
        }
    }
}
