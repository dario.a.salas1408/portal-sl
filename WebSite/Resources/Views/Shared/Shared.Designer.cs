﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ARGNS.WebSite.Resources.Views.Shared {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Shared {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Shared() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ARGNS.WebSite.Resources.Views.Shared.Shared", typeof(Shared).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activities.
        /// </summary>
        public static string Activities {
            get {
                return ResourceManager.GetString("Activities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activity.
        /// </summary>
        public static string Activity {
            get {
                return ResourceManager.GetString("Activity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alerts.
        /// </summary>
        public static string Alerts {
            get {
                return ResourceManager.GetString("Alerts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        public static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        public static string Back {
            get {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Partners.
        /// </summary>
        public static string BP {
            get {
                return ResourceManager.GetString("BP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branches.
        /// </summary>
        public static string Branches {
            get {
                return ResourceManager.GetString("Branches", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Calendar.
        /// </summary>
        public static string Calendar {
            get {
                return ResourceManager.GetString("Calendar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company Detail.
        /// </summary>
        public static string CD {
            get {
                return ResourceManager.GetString("CD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Charts.
        /// </summary>
        public static string Charts {
            get {
                return ResourceManager.GetString("Charts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Color Master.
        /// </summary>
        public static string ColorMaster {
            get {
                return ResourceManager.GetString("ColorMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Companies.
        /// </summary>
        public static string Companies {
            get {
                return ResourceManager.GetString("Companies", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Critical Path.
        /// </summary>
        public static string CrititalPath {
            get {
                return ResourceManager.GetString("CrititalPath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CRM.
        /// </summary>
        public static string CRM {
            get {
                return ResourceManager.GetString("CRM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cost Sheet.
        /// </summary>
        public static string CS {
            get {
                return ResourceManager.GetString("CS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Custom.
        /// </summary>
        public static string Custom {
            get {
                return ResourceManager.GetString("Custom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chart Viewer.
        /// </summary>
        public static string CV {
            get {
                return ResourceManager.GetString("CV", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Db Name.
        /// </summary>
        public static string DBName {
            get {
                return ResourceManager.GetString("DBName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Designs.
        /// </summary>
        public static string Designs {
            get {
                return ResourceManager.GetString("Designs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Design Grid.
        /// </summary>
        public static string DG {
            get {
                return ResourceManager.GetString("DG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disconnect.
        /// </summary>
        public static string Disconnect {
            get {
                return ResourceManager.GetString("Disconnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connected to SAP.
        /// </summary>
        public static string DIServerConnected {
            get {
                return ResourceManager.GetString("DIServerConnected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disconnected to SAP.
        /// </summary>
        public static string DIServerDisconnected {
            get {
                return ResourceManager.GetString("DIServerDisconnected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Design List.
        /// </summary>
        public static string DL {
            get {
                return ResourceManager.GetString("DL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Download as PDF.
        /// </summary>
        public static string DownloadPDF {
            get {
                return ResourceManager.GetString("DownloadPDF", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to first.
        /// </summary>
        public static string DTfirst {
            get {
                return ResourceManager.GetString("DTfirst", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Showing page _PAGE_ of _PAGES_.
        /// </summary>
        public static string DTinfo {
            get {
                return ResourceManager.GetString("DTinfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No records available.
        /// </summary>
        public static string DTinfoEmpty {
            get {
                return ResourceManager.GetString("DTinfoEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (filtered from _MAX_ total records).
        /// </summary>
        public static string DTinfoFiltered {
            get {
                return ResourceManager.GetString("DTinfoFiltered", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to last.
        /// </summary>
        public static string DTlast {
            get {
                return ResourceManager.GetString("DTlast", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Display _MENU_ records per page.
        /// </summary>
        public static string DTlengthMenu {
            get {
                return ResourceManager.GetString("DTlengthMenu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to next.
        /// </summary>
        public static string DTnext {
            get {
                return ResourceManager.GetString("DTnext", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to previous.
        /// </summary>
        public static string DTprevious {
            get {
                return ResourceManager.GetString("DTprevious", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nothing found - sorry.
        /// </summary>
        public static string DTzeroRecords {
            get {
                return ResourceManager.GetString("DTzeroRecords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General.
        /// </summary>
        public static string General {
            get {
                return ResourceManager.GetString("General", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to General Settings.
        /// </summary>
        public static string GS {
            get {
                return ResourceManager.GetString("GS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inventory Status Report.
        /// </summary>
        public static string InvStatus {
            get {
                return ResourceManager.GetString("InvStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Master Data.
        /// </summary>
        public static string MasterData {
            get {
                return ResourceManager.GetString("MasterData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Cost Sheet.
        /// </summary>
        public static string MCS {
            get {
                return ResourceManager.GetString("MCS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Messages.
        /// </summary>
        public static string Messages {
            get {
                return ResourceManager.GetString("Messages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Account.
        /// </summary>
        public static string MyAccount {
            get {
                return ResourceManager.GetString("MyAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Activities.
        /// </summary>
        public static string MyActivities {
            get {
                return ResourceManager.GetString("MyActivities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Direct Accesses.
        /// </summary>
        public static string MyDirectAccesses {
            get {
                return ResourceManager.GetString("MyDirectAccesses", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Opportunities.
        /// </summary>
        public static string MyOpportunity {
            get {
                return ResourceManager.GetString("MyOpportunity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Service Call.
        /// </summary>
        public static string MyServiceCall {
            get {
                return ResourceManager.GetString("MyServiceCall", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Shipment.
        /// </summary>
        public static string MyShMent {
            get {
                return ResourceManager.GetString("MyShMent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Style Grid.
        /// </summary>
        public static string MyStyleGrid {
            get {
                return ResourceManager.GetString("MyStyleGrid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Style List.
        /// </summary>
        public static string MyStyleList {
            get {
                return ResourceManager.GetString("MyStyleList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Styles.
        /// </summary>
        public static string MyStyles {
            get {
                return ResourceManager.GetString("MyStyles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to News.
        /// </summary>
        public static string News {
            get {
                return ResourceManager.GetString("News", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter a number greater than 0.
        /// </summary>
        public static string NumberGreaterZero {
            get {
                return ResourceManager.GetString("NumberGreaterZero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opportunities.
        /// </summary>
        public static string Opportunity {
            get {
                return ResourceManager.GetString("Opportunity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Data Collection.
        /// </summary>
        public static string PDC {
            get {
                return ResourceManager.GetString("PDC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product Data Management.
        /// </summary>
        public static string PDM {
            get {
                return ResourceManager.GetString("PDM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PLM.
        /// </summary>
        public static string PLM {
            get {
                return ResourceManager.GetString("PLM", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purchase Orders.
        /// </summary>
        public static string PO {
            get {
                return ResourceManager.GetString("PO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purchasing.
        /// </summary>
        public static string Purchases {
            get {
                return ResourceManager.GetString("Purchases", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Query Manager.
        /// </summary>
        public static string QueryManager {
            get {
                return ResourceManager.GetString("QueryManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Range Plan.
        /// </summary>
        public static string RangePlan {
            get {
                return ResourceManager.GetString("RangePlan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reports.
        /// </summary>
        public static string Reports {
            get {
                return ResourceManager.GetString("Reports", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show 10 per page.
        /// </summary>
        public static string S10PP {
            get {
                return ResourceManager.GetString("S10PP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show 20 per page.
        /// </summary>
        public static string S20PP {
            get {
                return ResourceManager.GetString("S20PP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show 50 per page.
        /// </summary>
        public static string S50PP {
            get {
                return ResourceManager.GetString("S50PP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sales.
        /// </summary>
        public static string Sales {
            get {
                return ResourceManager.GetString("Sales", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scale Master.
        /// </summary>
        public static string ScaleMaster {
            get {
                return ResourceManager.GetString("ScaleMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service Call.
        /// </summary>
        public static string ServiceCall {
            get {
                return ResourceManager.GetString("ServiceCall", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shipment.
        /// </summary>
        public static string Shipment {
            get {
                return ResourceManager.GetString("Shipment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign Out.
        /// </summary>
        public static string SignOut {
            get {
                return ResourceManager.GetString("SignOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Style List.
        /// </summary>
        public static string SL {
            get {
                return ResourceManager.GetString("SL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Activities.
        /// </summary>
        public static string String {
            get {
                return ResourceManager.GetString("String", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Style Grid.
        /// </summary>
        public static string StyleGrid {
            get {
                return ResourceManager.GetString("StyleGrid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Styles.
        /// </summary>
        public static string Styles {
            get {
                return ResourceManager.GetString("Styles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test.
        /// </summary>
        public static string Test {
            get {
                return ResourceManager.GetString("Test", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time and Action Calendar.
        /// </summary>
        public static string TimeAction {
            get {
                return ResourceManager.GetString("TimeAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Users.
        /// </summary>
        public static string Users {
            get {
                return ResourceManager.GetString("Users", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Variable Master.
        /// </summary>
        public static string VarMaster {
            get {
                return ResourceManager.GetString("VarMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View.
        /// </summary>
        public static string View {
            get {
                return ResourceManager.GetString("View", resourceCulture);
            }
        }
    }
}
