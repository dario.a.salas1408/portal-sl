﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ARGNS.WebSite.Resources.Views.PurchaseInvoices {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class PurchaseInvoices {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal PurchaseInvoices() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ARGNS.WebSite.Resources.Views.PurchaseInvoices.PurchaseInvoices", typeof(PurchaseInvoices).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applied Amount.
        /// </summary>
        public static string AA {
            get {
                return ResourceManager.GetString("AA", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Balance Due.
        /// </summary>
        public static string BD {
            get {
                return ResourceManager.GetString("BD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Freight.
        /// </summary>
        public static string Freight {
            get {
                return ResourceManager.GetString("Freight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purchase Invoice.
        /// </summary>
        public static string PI {
            get {
                return ResourceManager.GetString("PI", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purchase Invoice List.
        /// </summary>
        public static string PIL {
            get {
                return ResourceManager.GetString("PIL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purchase Invoices.
        /// </summary>
        public static string PIS {
            get {
                return ResourceManager.GetString("PIS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rounding.
        /// </summary>
        public static string Rounding {
            get {
                return ResourceManager.GetString("Rounding", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total Down Payment.
        /// </summary>
        public static string TDP {
            get {
                return ResourceManager.GetString("TDP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View Purchase Invoice.
        /// </summary>
        public static string VPI {
            get {
                return ResourceManager.GetString("VPI", resourceCulture);
            }
        }
    }
}
