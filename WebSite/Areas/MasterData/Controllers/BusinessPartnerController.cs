﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.BusinessPartners;
using ARGNS.WebSite.Resources.Views.Error;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.MasterData.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [CustomerAuthorize(Enums.Areas.MasterData)]
    public class BusinessPartnerController : Controller
    {
        private BusinessPartnerManagerView mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        private UserManagerView mUserManagerView;

        public BusinessPartnerController()
        {
            mUserManagerView = new UserManagerView();
        }

        [CustomerAuthorize(Enums.Areas.MasterData, Enums.Pages.ABMBP, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
                return View(ListBp);
            }

            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionBusinessPartner"></param>
        /// <param name="CardCodeBP"></param>
        /// <param name="fromController"></param>
        /// <returns></returns>
        public ActionResult ActionBusinessPartner(string ActionBusinessPartner, string CardCodeBP, string fromController)
        {
            try
            {
                BusinessPartnerView mBusinessPartnerView = null;

                switch (fromController)
                {
                    case "BusinessPartner":
                        ViewBag.URLRedirect = "/MasterData/BusinessPartner/Index";
                        ViewBag.controllerName = "BusinessPartner";
                        break;
                    case "MyBusinessPartner":
                        ViewBag.URLRedirect = "/MasterData/MyBusinessPartner/Index";
                        ViewBag.controllerName = "MyBusinessPartner";
                        break;
                    default:
                        ViewBag.URLRedirect = "/Home";
                        ViewBag.controllerName = "Home";
                        break;
                }
                switch (ActionBusinessPartner)
                {
                    case "Add":
                        ViewBag.Tite = BusinessPartners.ABP;
                        ViewBag.FormMode = ActionBusinessPartner;
                        mBusinessPartnerView = mBusinessPartnerManagerView.GetBusinessPartner(CardCodeBP, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);
                        TempData["Addresess"] = mBusinessPartnerView.Addresses;
                        break;

                    case "Update":
                        ViewBag.Tite = BusinessPartners.UBP;
                        ViewBag.FormMode = ActionBusinessPartner;
                        mBusinessPartnerView = mBusinessPartnerManagerView.GetBusinessPartner(CardCodeBP, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);
                        TempData["Addresess"] = mBusinessPartnerView.Addresses;
                        break;
                }

                foreach (var item in mBusinessPartnerView.BPCombo.BPTypeList)
                {
                    switch (item.Code)
                    {
                        case "C":
                            item.Name = Resources.Views.BusinessPartners.BusinessPartners.BPTypeC;
                            break;
                        case "S":
                            item.Name = Resources.Views.BusinessPartners.BusinessPartners.BPTypeS;
                            break;
                        case "L":
                            item.Name = Resources.Views.BusinessPartners.BusinessPartners.BPTypeL;
                            break;
                        default:
                            break;
                    }
                }

                mBusinessPartnerView.MappedUdf = mUserManagerView.GetUserUDFSearch(
                       ((UserView)Session["UserLogin"]).CompanyConnect,
                       ((UserView)Session["UserLogin"]).IdUser,
                       ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OCRD");

                ViewBag.LawSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                return View("_BusinessPartner", mBusinessPartnerView);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(BusinessPartnerView model)
        {
            var result = mBusinessPartnerManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString());

            return Json(result);

            //if (result == "Ok")
            //{ return Json("Ok"); }
            //else
            //{ return Json(@Error.WebServError); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(BusinessPartnerView model)
        {
            var result = mBusinessPartnerManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString());

            return Json(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListBP([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.All, false, searchViewModel.pCardCode,
                    searchViewModel.pCardName, ((UserView)Session["UserLogin"]).IsSalesEmployee,
                    ((UserView)Session["UserLogin"]).SECode, ((UserView)Session["UserLogin"]).BPGroupId,
                    true, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.All, false,
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName,
                    false, null, null, true, requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw,
                mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult GetStateList(string Country, string SelectedState)
        {
            List<StateSAP> StateList = mBusinessPartnerManagerView.GetStateList(((UserView)Session["UserLogin"]).CompanyConnect, Country);
            return PartialView("_States", Tuple.Create(StateList, SelectedState));
        }

        public JsonResult GetAddress(string pAddressCode, string pAddressType)
        {
            List<BPAddressesSAP> AddressList = (List<BPAddressesSAP>)TempData["Addresess"];
            BPAddressesSAP addressReturn = AddressList.Where(c => c.Address == pAddressCode && c.AdresType == pAddressType).FirstOrDefault();
            TempData["Addresess"] = AddressList;

            return Json(addressReturn);
        }
    }
}