﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataTables.Mvc;
using ARGNS.Model.Implementations.Portal;

namespace ARGNS.WebSite.Areas.MasterData.Controllers
{
    [CustomerAuthorize(Enums.Areas.MasterData)]
    public class InventoryStatusController : Controller
    {
        private WarehouseManagerView mWarehouseManagerView = new WarehouseManagerView();
        private UserManagerView mUserManagerView = new UserManagerView();
        private BusinessPartnerManagerView mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        private ItemMasterManagerView mItemMasterManagerView = new ItemMasterManagerView();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<WarehouseView> mWarehouseList = mWarehouseManagerView
                .GetListWarehouse(((UserView)Session["UserLogin"]).CompanyConnect);

            List<UDF_ARGNS> mListUDFSearch = mUserManagerView
                .GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, 
                ((UserView)Session["UserLogin"]).IdUser, 
                ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, 
                "OITM");

            List<ItemGroupSAP> mItemGroupList = mItemMasterManagerView
                .GetItemGroupList(((UserView)Session["UserLogin"]).CompanyConnect);

            ViewBag.Tite = "Inventory Status Report";
            return View(Tuple.Create(mWarehouseList, mListUDFSearch, mItemGroupList));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Vendors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Vendor, false,
                    searchViewModel.pCardCode, searchViewModel.pCardName,
                    ((UserView)Session["UserLogin"]).IsSalesEmployee,
                    ((UserView)Session["UserLogin"]).SECode,
                    ((UserView)Session["UserLogin"]).BPGroupId, false,
                    requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor, false,
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, false,
                    null, null, true, requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw,
                mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Column, OrderColumn>();
                    cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                    cfg.CreateMap<StockModel, StockModelView>();
                }).CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = GetItems(searchViewModel.pPageKey,
                    searchViewModel.pMappedUdf, false, "",
                    searchViewModel.pItemCode, searchViewModel.pItemName,
                    searchViewModel.pCkCatalogueNum,
                    searchViewModel.pCardCode,
                    searchViewModel.pBPCatalogCode,
                    searchViewModel.pInventoryItem,
                    requestModel.Start,
                    requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw,
                    mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pModeCode"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        private JsonObjectResult GetItems(string pPageKey, List<UDF_ARGNS> pMappedUdf, bool pOnlyActiveItems,
           string pModeCode = "", string pItemCode = "", string pItemName = "",
           bool pCkCatalogueNum = false, string pBPCode = "", string pBPCatalogCode = "",
           string pInventoryItem = "", int pStart = 0, int pLength = 0,
           OrderColumn pOrderColumn = null)
        {
            try
            {

                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ItemMasterSAP,
                    ItemMasterView>(); cfg.CreateMap<ItemMasterView, ItemMasterSAP>();
                    cfg.CreateMap<StockModel, StockModelView>();
                    cfg.CreateMap<StockModelView, StockModel>();
                })
                    .CreateMapper();

                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView
                    .GetOITMListBy(((UserView)Session["UserLogin"]).CompanyConnect,
                    "", "Y", pInventoryItem, "N", "", pMappedUdf, pModeCode,
                    ((UserView)Session["UserLogin"]).IdUser, pItemCode,
                    pItemName, pOnlyActiveItems, 
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                    pCkCatalogueNum, pBPCode, pBPCatalogCode,
                    pStart, pLength, pOrderColumn);

                List<ItemMasterView> listReturn = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);

                List<StockModelView> itemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect,
                    listReturn.Select(c => c.ItemCode).ToList());

                List<CurrencySAP> listCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                //if pLength is greatter than 0, i need to page the result, then i need to add the actual page to the previous ListItems
                if (pLength > 0)
                {
                    List<ItemMasterView> mListItemsAux = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]);
                    //If its the first time that i enter to GetItems and the TempData of ListItems is null i inizialice the variable.
                    mListItemsAux = (mListItemsAux == null ? new List<ItemMasterView>() : mListItemsAux);

                    //if the item not exist in mListItemsAux List i add it, else i do nothing
                    mListItemsAux.AddRange(listReturn.Where(
                        c => mListItemsAux.Select(x => x.ItemCode).Contains(c.ItemCode) == false)
                        .Select(c =>
                        {
                            c.ListCurrency = listCurrency;
                            c.hasStock = (itemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c;
                        })
                            .ToList());

                    TempData["ListItems" + pPageKey] = mListItemsAux;
                }
                else
                {
                    TempData["ListItems" + pPageKey] = listReturn.Select(c =>
                    {
                        c.ListCurrency = listCurrency;
                        c.hasStock = (itemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                        return c;
                    }).ToList();
                }

                //Save the ListCurrency and hasStock to ListReturn Items
                listReturn = listReturn.Select(c =>
                {
                    c.ListCurrency = listCurrency;
                    c.hasStock = (itemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                    return c;
                }).ToList();

                mJsonObjectResult.ItemMasterSAPList = mapper.Map<List<ItemMasterSAP>>(listReturn);
                TempData["ListCurrency" + pPageKey] = listCurrency;

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ItemMasterController -> GetOITMListBy :" + ex.Message);
                Logger.WriteError("ItemMasterController -> GetOITMListBy :" + ex.InnerException.Message);

                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <returns></returns>
        private List<ItemMasterView> GetItems(List<UDF_ARGNS> pMappedUdf, 
            bool pOnlyActiveItems, string pItemsWithStock, 
            string pItemCode = "", string pItemName = "")
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                    cfg.CreateMap<StockModel, StockModelView>();
                }).CreateMapper();

                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(
                    ((UserView)Session["UserLogin"]).CompanyConnect, "", "", "", 
                    pItemsWithStock, "", 
                    pMappedUdf, "", ((UserView)Session["UserLogin"]).IdUser, pItemCode, 
                    pItemName, pOnlyActiveItems, Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

                List<ItemMasterView> ListReturn = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);
                return ListReturn;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemCodeFrom"></param>
        /// <param name="pItemCodeTo"></param>
        /// <param name="pVendorFrom"></param>
        /// <param name="pVendorTo"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pSelectedWH"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _InventoryStatus([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            InventoryStatusSearchView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<Column, OrderColumn>();
                    cfg.CreateMap<InventoryStatus, InventoryStatusView>();
                }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetInventoryStatus(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                searchViewModel.pItemCodeFrom,
                searchViewModel.pItemCodeTo,
                searchViewModel.pVendorFrom,
                searchViewModel.pVendorTo,
                searchViewModel.pItemGroup,
                searchViewModel.pSelectedWH,
                requestModel.Start,
                requestModel.Length,
                mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            try
            {
                foreach (var item in mJsonObjectResult.InventoryStatusList)
                {
                    item.Available = item.OnHand - item.IsCommited + item.OnOrder;
                }

                return Json(new DataTablesResponse(requestModel.Draw,
                  mapper.Map<List<InventoryStatusView>>(mJsonObjectResult.InventoryStatusList),
                  Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                  Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}