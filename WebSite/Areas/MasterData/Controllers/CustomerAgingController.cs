﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.MasterData.Controllers
{
    [CustomerAuthorize(Enums.Areas.MasterData)]
    public class CustomerAgingController : Controller
    {

        BusinessPartnerManagerView mBusinessPartnerManagerView;

        public CustomerAgingController()
        {
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.MasterData, Enums.Pages.CustomerAging)]
        public ActionResult Index()
        {
            try
            {
                return View();
            }

            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _BPList([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser((
                    (UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer,
                    false, searchViewModel.pCardCode, searchViewModel.pCardName,
                    ((UserView)Session["UserLogin"]).IsSalesEmployee,
                    ((UserView)Session["UserLogin"]).SECode,
                    ((UserView)Session["UserLogin"]).BPGroupId, false, requestModel.Start,
                    requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser((
                    (UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, false,
                    ((UserView)Session["UserLogin"]).BPCode,
                    searchViewModel.pCardName, false, null, null,
                    false, requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ViewAgingReport(string pBPCode = "")
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();
                //Si esto termina yendo en null al servicio va a traer todos los BP (el usuario logueado deberia ser un usuario comun)
                List<string> mBPListToFilter = new List<string>();
                List<BusinessPartnerView> mListBPperVendor = mBusinessPartnerManagerView.GetBusinessPartners(
                    ((UserView)Session["UserLogin"]).CompanyConnect);

                if (((UserView)Session["UserLogin"]).SECode != null)
                {
                    mListBPperVendor = mapper.Map<List<BusinessPartnerView>>
                            (mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                            ((UserView)Session["UserLogin"]).CompanyConnect,
                            Enums.BpType.Customer, false,
                            "", "", true, ((UserView)Session["UserLogin"]).SECode).BusinessPartnerSAPList);
                }

                if (pBPCode == "")
                {
                    if (((UserView)Session["UserLogin"]).BPCode != null)
                    {
                        mBPListToFilter = new List<string>();
                        mBPListToFilter.Add(((UserView)Session["UserLogin"]).BPCode);
                    }
                    else
                    {
                        mListBPperVendor.ForEach(c => mBPListToFilter.Add(c.CardCode));
                    }
                }
                else
                {
                    if (((UserView)Session["UserLogin"]).SECode != null)
                    {
                        if (mListBPperVendor.Where(w => w.CardCode == pBPCode).Any())
                        {
                            mBPListToFilter = new List<string>();
                            mBPListToFilter.Add(pBPCode);
                        }
                    }
                    else
                    {
                        mBPListToFilter = new List<string>();
                        mBPListToFilter.Add(pBPCode);
                    }
                }

                JsonObjectResult mJsonObjectResult = mBusinessPartnerManagerView.GetCustomerAgingReport(
                    ((UserView)Session["UserLogin"]).CompanyConnect, mBPListToFilter);

                return PartialView("_CustomerAgingReportList", mJsonObjectResult);

            }
            catch (Exception ex)
            {
                Logger.WriteError("CustomerAgingController -> _ViewAgingReport : " + ex.Message);
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                return PartialView("_CustomerAgingReportList", mJsonObjectResult);
            }
        }
    }
}