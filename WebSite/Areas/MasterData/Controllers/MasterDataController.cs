﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.ItemMaster;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.MasterData.Controllers
{
    [CustomerAuthorize(Enums.Areas.MasterData)]
    public class MasterDataController : Controller
    {
        private ItemMasterManagerView mItemMasterManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private UserManagerView mUserManagerView;

        public MasterDataController()
        {
            mItemMasterManagerView = new ItemMasterManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mUserManagerView = new UserManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.MasterData, Enums.Pages.ItemMaster, Enums.Actions.List)]
        public ActionResult Index()
        {
            List<ItemMasterView> mListItemMaster = new List<ItemMasterView>();
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect,
                ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");

            return View(Tuple.Create(mListItemMaster, mOITMUDFList));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListItemMaster(
            [ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Column, OrderColumn>();
                    cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                    cfg.CreateMap<StockModel, StockModelView>();
                    cfg.CreateMap<WarehousePortal, WarehousePortalView>();
                })
                    .CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(
                    ((UserView)Session["UserLogin"]).CompanyConnect,
                    searchViewModel.pPrchseItem, searchViewModel.pSellItem,
                    searchViewModel.pInventoryItem, "N", "",
                    searchViewModel.pMappedUdf,
                    "", ((UserView)Session["UserLogin"]).IdUser,
                    searchViewModel.pItemCode,
                    searchViewModel.pItemName, false,
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList),
                    false, "", "",
                    requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw,
                    mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pActionItemMaster"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.MasterData, Enums.Pages.ItemMaster, Enums.Actions.View)]
        public ActionResult ActionItemMaster(string pActionItemMaster, string pItemCode, string pTypeView)
        {
            try
            {
                ItemMasterView mItemMasterView = new ItemMasterView();
                List<StockModelView> ItemsStock = new List<StockModelView>();

                switch (pActionItemMaster)
                {
                    case "View":
                        ViewBag.Tite = ItemMaster.IMV;
                        ViewBag.FormMode = "View";
                        if (!string.IsNullOrEmpty(pItemCode))
                        {
                            BusinessPartnerView mBPAux = null;
                            if (((UserView)Session["UserLogin"]).BPCode != null)
                            {
                                mBPAux = mBusinessPartnerManagerView.GetBusinessPartnerMinimalData(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).BPCode);
                            }
                            mItemMasterView = mItemMasterManagerView.GeItemByCode(((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, ((UserView)Session["UserLogin"]).IdUser, null, (mBPAux != null ? mBPAux.ListNum : null));
                            if (mBPAux != null)
                            {
                                if (mBPAux.ListNum != null)
                                    mItemMasterView.PriceSelect = (int)mBPAux.ListNum;
                            }

                            if(mItemMasterView.SerialsSAP == null)
                            {
                                mItemMasterView.SerialsSAP = new List<Serial>();
                            }

                            TempData["Serials"] = mItemMasterView.SerialsSAP;
                            List<string> mItems = new List<string>();
                            mItems.Add(pItemCode);
                            ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, mItems.ToArray());
                        }
                        break;
                }

                switch (pTypeView)
                {
                    case "ViewCommon":
                        return View("_ItemMaster", Tuple.Create(mItemMasterView, ItemsStock));
                    case "ViewHandheld":
                        ViewBag.Tite = ItemMaster.IMT;
                        ViewBag.ErroMsg = ItemMaster.INF;
                        return View("_ItemMasterHandheld", Tuple.Create(mItemMasterView, ItemsStock));
                    default:
                        return View("_ItemMaster", Tuple.Create(mItemMasterView, ItemsStock));
                }

            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemCode"></param>
        /// <param name="pPriceList"></param>
        /// <returns></returns>
        [HttpPost]
        public decimal GetPriceItemByPriceList(string pItemCode, short pPriceList)
        {
            return mItemMasterManagerView.GetPriceItemByPriceList(
                ((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, pPriceList);
        }

        [HttpPost]
        public PartialViewResult SearchItemHandheld(string pItemCode)
        {
            try
            {
                ItemMasterView mItemMasterView = new ItemMasterView();
                List<StockModelView> ItemsStock = new List<StockModelView>();

                mItemMasterView = mItemMasterManagerView.GeItemByCode(((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, ((UserView)Session["UserLogin"]).IdUser, pItemCode);

                List<string> mItems = new List<string>();
                BusinessPartnerView mBPAux = null;
                if (((UserView)Session["UserLogin"]).BPCode != null)
                {
                    mBPAux = mBusinessPartnerManagerView.GetBusinessPartnerMinimalData(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).BPCode);
                }
                mItemMasterView = mItemMasterManagerView.GeItemByCode(((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, ((UserView)Session["UserLogin"]).IdUser, null, (mBPAux != null ? mBPAux.ListNum : null));
                if (mBPAux != null)
                {
                    if (mBPAux.ListNum != null)
                        mItemMasterView.PriceSelect = (int)mBPAux.ListNum;
                }
                mItems.Add(pItemCode);
                ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, mItems.ToArray());

                return PartialView("_ItemMasterDetailHandheld", Tuple.Create(mItemMasterView, ItemsStock));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public PartialViewResult GetSerialDetail(int sysNumber)
        {
            try
            {
                var serials = (List<Serial>)TempData["Serials"];

                var serial = serials.Where(c => c.SysNumber == sysNumber).FirstOrDefault();

                TempData["Serials"] = serials;

                return PartialView("_ContSerialDetail", serial);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}