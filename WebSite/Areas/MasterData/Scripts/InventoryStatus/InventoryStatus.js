﻿var mItemFieldName;
var mVendorFieldName;
var bpFrom = true; 
var bpItemFrom = true; 

function SetVendor(pCode, pName) {
    if (bpFrom) {
        $('#VendorFrom').val(pCode);
        $('#VendorModal').modal('hide');
    }
    else {
        $('#VendorTo').val(pCode);
        $('#VendorModal').modal('hide');
    }
}

function SetItem(pItemFieldName)
{
    if (bpItemFrom) {
        $('#ItemCodeFrom').val(pItemFieldName);
        $('#ItemMasterModal').modal('hide');
    }
    else {
        $('#ItemCodeTo').val(pItemFieldName);
        $('#ItemMasterModal').modal('hide');
    }
}

function setVendorFocus(pVendorFieldName)
{
    mVendorFieldName = pVendorFieldName;
}

function setVendor(pVendorCode)
{
    $("#" + mVendorFieldName).val(pVendorCode);
    $('#VendorModal').modal('hide');
}

function setItem(pItemCode)
{
    $("#" + mItemFieldName).val(pItemCode);
    $('#ItemMasterModal').modal('hide');
}

function selectAllWH()
{
    var mListWH = $("input[id^=WH_]");
    if ($("#WHALL").is(':checked'))
    {
        for (i = 0; i < mListWH.length; i++)
        {
            $("#" + mListWH[i].id).prop('checked', true);
            $("#" + mListWH[i].id).prop('disabled', true);
        }
    }
    else
    {
        for (i = 0; i < mListWH.length; i++) {
            $("#" + mListWH[i].id).prop('checked', false);
            $("#" + mListWH[i].id).prop('disabled', false);
        }
    }
} 

$(document).ready(function () {
    $("#WHALL").prop('checked', true);
    selectAllWH();
    $("#LoadVendor").hide();

    $('#VendorModal').on('show.bs.modal', function (e) {
        $("#ModalBodyVendor").html("");
    });

    $("#LoadItems").hide();

    $('#ItemMasterModal').on('show.bs.modal', function (e) {
        $("#ModalBodyItems").html("");
    });

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init(false);
        }
        else {
            assetListBP.refresh();
        }
    });

    $("#ViewInventoryStatus").click(function () {
        if (typeof assetListInventory.dt === 'undefined' || assetListInventory.dt == null) {
            assetListInventory.init();
        }
        else {
            assetListInventory.destroy();
            assetListInventory.init();
        }
    });

    $("#SearchVendorFrom").click(function () {
        bpFrom = true; 
    });

    $("#SearchVendorTo").click(function () {
        bpFrom = false; 
    });

    $("#SearchItemFrom").click(function () {
        bpItemFrom = true;
    });

    $("#SearchItemTo").click(function () {
        bpItemFrom = false;
    });

    $("#SearchItems").click(function () {

        if (typeof assetListVM_ListItems.dt === 'undefined' || assetListVM_ListItems.dt == null) {
            assetListVM_ListItems.init();
            $("#btnChoose").prop('disabled', true);
        }
        else {
            assetListVM_ListItems.refresh();
            $("#btnChoose").prop('disabled', true);
        }
    });

    function GetItemsModel() {
        $.ajax({
            url: 'MasterData/InventoryStatus/_Items',
            contextType: 'application/html;charset=utf-8',
            data: {},
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ModalItemsTable").html(data);
        });
    }
});