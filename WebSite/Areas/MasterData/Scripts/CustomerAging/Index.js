﻿$(document).ready(function () {

    $("#txtCodeBP").prop('disabled', true);
    $("#txtNameBP").prop('disabled', true);

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    $("#btViewCAR").click(function () {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/MasterData/CustomerAging/_ViewAgingReport',
            contextType: 'application/html;charset=utf-8',
            data: { pBPCode: $("#txtCodeBP").val()},
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#BodyReport").html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            alert("error");
            $('#Loading').modal('hide');
        });

    });

    $("#btnClean").click(function () {
        $('#txtCodeBP').val("");
        $('#txtNameBP').val("");
    }); 
});

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeBP').val(pCode);
    $('#txtNameBP').val(mName);
    $('#VendorModal').modal('hide');

}