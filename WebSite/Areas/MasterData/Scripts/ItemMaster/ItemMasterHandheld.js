﻿$(document).ready(function () {

    $("#ButtonSearch").click(function () {
        $('#Loading').modal({
            backdrop: 'static'
        });

        $.ajax({
            url: '/MasterData/MasterData/SearchItemHandheld',
            contextType: 'application/html;charset=utf-8',
            data: { pItemCode: $("#ItemCodeSearch").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#Loading').modal('hide');
            $("#ItemMasterDetail").html(data);
            $("#ItemCodeSearch").val("");
        }).fail(function (data) {
            $('#Loading').modal('hide');
            $("#ItemCodeSearch").val("");
            ShowError(ErrorMsg);
        });
    });

    $(document).keypress(function (e) {
        if (e.which == 13 && $("#ItemCodeSearch").is(':focus')) {
            $("#ButtonSearch").click();
        }
    });

});


function ShowError(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}