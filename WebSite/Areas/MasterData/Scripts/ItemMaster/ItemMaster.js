﻿$(document).ready(function () {

    $("#Item1_ItemCode").prop('disabled', true);
    $("#Item1_ItemName").prop('disabled', true);
    $("#Item1_CardCode").prop('disabled', true);
    $("#Item1_CardCode").prop('disabled', true);
    $("#txtCodeBar").prop('disabled', true);

    $("#PriceSelect").change(function () {
        $.post("/MasterData/MasterData/GetPriceItemByPriceList", {
            pItemCode: $("#Item1_ItemCode").val(),
            pPriceList: $("#PriceSelect").val()
        })
            .success(function (data) {

                $("#Item1_PriceItemByListPrice").val(parseFloat(data).toFixed(2))

            })
            .error(function (err) { });

    });

});

function GetSerial(sysNumber) {

    $.ajax({
        url: '/MasterData/MasterData/GetSerialDetail',
        traditional: true,
        contextType: 'application/html;charset=utf-8',
        data: { sysNumber: sysNumber },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#ContentItemMasterSerials").html(data);
       
    });

}