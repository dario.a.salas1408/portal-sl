﻿$(document).ready(function () {
    if (formMode != "Add")
    {
        $("#CardCode").prop('disabled', true);
        if ($("#AdrsIDB").val() != "")
            $("#AdrsIDB").prop('disabled', true);
        if ($("#AdrsIDS").val() != "")
            $("#AdrsIDS").prop('disabled', true);
    }

    $("#Balance").prop('disabled', true);
    $("#DNotesBal").prop('disabled', true);
    $("#OrdersBal").prop('disabled', true);
    $("#OprCount").prop('disabled', true);
    $("#UpdateCorrectly").hide();

    CountryShipChange();
    CountryBillChange();

    $("#CountryS").change(function () {
        CountryShipChange();
    });

    $("#CountryB").change(function () {
        CountryBillChange();
    });

    $("#AdrsBillSelect").change(function () {
        $.post("/MasterData/BusinessPartner/GetAddress", {
            pAddressCode: $("#AdrsBillSelect").val(),
            pAddressType:"B"
        })
       .success(function (data) {
           $("#LineNumB").val(data.LineNum);
           $("#AdrsIDB").val(data.Address);
           $("#SPOBoxB").val(data.Street);
           $("#BlockB").val(data.Block);
           $("#CityB").val(data.City);
           $("#ZipCodeB").val(data.ZipCode);
           $("#CountyB").val(data.County);
           $("#CountryB").val(data.Country);
           $("#DefaultStateB").val(data.State);
           $("#SNOB").val(data.StreetNo);
           $("#BuildingB").val(data.Building);
           $("#GlbLocNumB").val(data.GlblLocNum);
           CountryBillChange();
       })
       .error(function (err) { Close(); });
       
    });

    $("#AdrsShipSelect").change(function () {
        $.post("/MasterData/BusinessPartner/GetAddress", {
            pAddressCode: $("#AdrsShipSelect").val(),
            pAddressType: "S"
        })
       .success(function (data) {
           $("#LineNumS").val(data.LineNum);
           $("#AdrsIDS").val(data.Address);
           $("#SPOBoxS").val(data.Street);
           $("#BlockS").val(data.Block);
           $("#CityS").val(data.City);
           $("#ZipCodeS").val(data.ZipCode);
           $("#CountyS").val(data.County);
           $("#CountryS").val(data.Country);
           $("#DefaultStateS").val(data.State);
           $("#SNOS").val(data.StreetNo);
           $("#BuildingS").val(data.Building);
           $("#GlbLocNumS").val(data.GlblLocNum);
           $("#FTIS").val(data.LicTradNum);
           $("#TOS").val(data.TaxCode);
           CountryShipChange();

       })
       .error(function (err) { Close(); });

    });

    $("#submitButton").click(function () {

        if (Validate() == false) { return false; }

        switch (formMode) {
            case "Add":
                Save();
                break;
            case "Update":
                Update();
                break;
            default:
                alert('Imposible realizar una acción')
                break;
        }
    });

    function CountryShipChange()
    {
        $.post("/MasterData/BusinessPartner/GetStateList", {
            Country: $("#CountryS").val(),
            SelectedState: $("#DefaultStateS").val()
        })
        .success(function (data) {
            $("#StateS").html(data);
        })
        .error(function (err) { Close(); });
    }

    function CountryBillChange() {
        $.post("/MasterData/BusinessPartner/GetStateList", {
            Country: $("#CountryB").val(),
            SelectedState: $("#DefaultStateB").val()
        })
        .success(function (data) {
            $("#StateB").html(data);
        })
        .error(function (err) { Close(); });
    }

    function Save() {

        $('#Loading').modal('show');

        var BusinessPartnerView = {
            CardCode: $("#CardCode").val(),
            CardName: $("#CardName").val(),
            CardType: $("#CardType").val(),
            MailAddres: $("#MailAddres").val(),
            Currency: $("#Currency").val(),
            ShipType: $("#ShipType").val(),
            SlpCode: $("#SalesEmployee").val(),
            ListNum: $("#PriceList").val(),
            LicTradNum: $("#LicTradNum").val(),
            Phone1: $("#Phone1").val(),
            Phone2: $("#Phone2").val(),
            Cellular: $("#Cellular").val(),
            Fax: $("#Fax").val(),
            E_Mail: $("#E_Mail").val(),
            IntrntSite: $("#IntrntSite").val(),
            U_B1SYS_FiscIdType: (LawSet == "AR" ? $("#U_B1SYS_FiscIdType").val() : ""),
            Addresses: [],
            MappedUdf: [],
            QryGroup1: $("#QryGroup1").prop('checked'), QryGroup2: $("#QryGroup2").prop('checked'), QryGroup3: $("#QryGroup3").prop('checked'),
            QryGroup4: $("#QryGroup4").prop('checked'), QryGroup5: $("#QryGroup5").prop('checked'), QryGroup6: $("#QryGroup6").prop('checked'),
            QryGroup7: $("#QryGroup7").prop('checked'), QryGroup8: $("#QryGroup8").prop('checked'), QryGroup9: $("#QryGroup9").prop('checked'),
            QryGroup10: $("#QryGroup10").prop('checked'), QryGroup11: $("#QryGroup11").prop('checked'), QryGroup12: $("#QryGroup12").prop('checked'),
            QryGroup13: $("#QryGroup13").prop('checked'), QryGroup14: $("#QryGroup14").prop('checked'), QryGroup15: $("#QryGroup15").prop('checked'),
            QryGroup16: $("#QryGroup16").prop('checked'), QryGroup17: $("#QryGroup17").prop('checked'), QryGroup18: $("#QryGroup18").prop('checked'),
            QryGroup19: $("#QryGroup19").prop('checked'), QryGroup20: $("#QryGroup20").prop('checked'), QryGroup21: $("#QryGroup21").prop('checked'),
            QryGroup22: $("#QryGroup22").prop('checked'), QryGroup23: $("#QryGroup23").prop('checked'), QryGroup24: $("#QryGroup24").prop('checked'),
            QryGroup25: $("#QryGroup25").prop('checked'), QryGroup26: $("#QryGroup26").prop('checked'), QryGroup27: $("#QryGroup27").prop('checked'),
            QryGroup28: $("#QryGroup28").prop('checked'), QryGroup29: $("#QryGroup29").prop('checked'), QryGroup30: $("#QryGroup30").prop('checked'),
            QryGroup31: $("#QryGroup31").prop('checked'), QryGroup32: $("#QryGroup32").prop('checked'), QryGroup33: $("#QryGroup33").prop('checked'),
            QryGroup34: $("#QryGroup34").prop('checked'), QryGroup35: $("#QryGroup35").prop('checked'), QryGroup36: $("#QryGroup36").prop('checked'),
            QryGroup37: $("#QryGroup37").prop('checked'), QryGroup38: $("#QryGroup38").prop('checked'), QryGroup39: $("#QryGroup39").prop('checked'),
            QryGroup40: $("#QryGroup40").prop('checked'), QryGroup41: $("#QryGroup41").prop('checked'), QryGroup42: $("#QryGroup42").prop('checked'),
            QryGroup43: $("#QryGroup43").prop('checked'), QryGroup44: $("#QryGroup44").prop('checked'), QryGroup45: $("#QryGroup45").prop('checked'),
            QryGroup46: $("#QryGroup46").prop('checked'), QryGroup47: $("#QryGroup47").prop('checked'), QryGroup48: $("#QryGroup48").prop('checked'),
            QryGroup49: $("#QryGroup49").prop('checked'), QryGroup50: $("#QryGroup50").prop('checked'), QryGroup51: $("#QryGroup51").prop('checked'),
            QryGroup52: $("#QryGroup52").prop('checked'), QryGroup53: $("#QryGroup53").prop('checked'), QryGroup54: $("#QryGroup54").prop('checked'),
            QryGroup55: $("#QryGroup55").prop('checked'), QryGroup56: $("#QryGroup56").prop('checked'), QryGroup57: $("#QryGroup57").prop('checked'),
            QryGroup58: $("#QryGroup58").prop('checked'), QryGroup59: $("#QryGroup59").prop('checked'), QryGroup60: $("#QryGroup60").prop('checked'),
            QryGroup61: $("#QryGroup61").prop('checked'), QryGroup62: $("#QryGroup62").prop('checked'), QryGroup63: $("#QryGroup63").prop('checked'),
            QryGroup64: $("#QryGroup64").prop('checked')
        };

        BusinessPartnerView.Addresses.push({
            Address: $("#AdrsIDB").val(),
            CardCode: $("#CardCode").val(),
            Street: $("#SPOBoxB").val(),
            Block: $("#BlockB").val(),
            ZipCode: $("#ZipCodeB").val(),
            City: $("#CityB").val(),
            County: $("#CountyB").val(),
            Country: $("#CountryB").val(),
            State: $("#StateB").val(),
            LineNum: $("#LineNumB").val(),
            Building: $("#BuildingB").val(),
            AdresType: "B",
            StreetNo: $("#SNOB").val(),
            GlblLocNum: $("#GlbLocNumB").val()
        });

        BusinessPartnerView.Addresses.push({
            Address: $("#AdrsIDS").val(),
            CardCode: $("#CardCode").val(),
            Street: $("#SPOBoxS").val(),
            Block: $("#BlockS").val(),
            ZipCode: $("#ZipCodeS").val(),
            City: $("#CityS").val(),
            County: $("#CountyS").val(),
            Country: $("#CountryS").val(),
            State: $("#StateS").val(),
            LineNum: $("#LineNumS").val(),
            Building: $("#BuildingS").val(),
            AdresType: "S",
            StreetNo: $("#SNOS").val(),
            GlblLocNum: $("#GlbLocNumS").val(),
            LicTradNum: $("#FTIS").val(),
            TaxCode: $("#TOS").val()
        });

        var listUDFHEAD = $("input[id^=HEADUDF_]");
        var listUDFHEADSelect = $("select[id^=HEADUDF_]");
        for (i = 0; i < listUDFHEAD.length; i++) {
            BusinessPartnerView.MappedUdf.push({
                UDFName: listUDFHEAD[i].id.substring(8),
                Value: $("#" + listUDFHEAD[i].id).val(),
            });
        }
        for (i = 0; i < listUDFHEADSelect.length; i++) {
            BusinessPartnerView.MappedUdf.push({
                UDFName: listUDFHEADSelect[i].id.substring(8),
                Value: $("#" + listUDFHEADSelect[i].id).val(),
            });
        }

        $.ajax({
            url: "/MasterData/BusinessPartner/Add",
            async: false,
            type: "POST",
            data: JSON.stringify(BusinessPartnerView),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data == "Ok") {
                    $('#Loading').modal('hide');
                    location.href = urlRedirect;
                }
                else {
                    Error(data);
                }
            }
        });
    }

    function Update() {

        $('#Loading').modal('show');

        var BusinessPartnerView = {
            CardCode: $("#CardCode").val(),
            CardName: $("#CardName").val(),
            CardType: $("#CardType").val(),
            MailAddres: $("#MailAddres").val(),
            Currency: $("#Currency").val(),
            ShipType: $("#ShipType").val(),
            SlpCode: $("#SalesEmployee").val(),
            ListNum: $("#PriceList").val(),
            LicTradNum: $("#LicTradNum").val(),
            Phone1: $("#Phone1").val(),
            Phone2: $("#Phone2").val(),
            Cellular: $("#Cellular").val(),
            Fax: $("#Fax").val(),
            E_Mail: $("#E_Mail").val(),
            IntrntSite: $("#IntrntSite").val(),
            U_B1SYS_FiscIdType: (LawSet == "AR" ? $("#U_B1SYS_FiscIdType").val() : ""),
            Addresses: [],
            MappedUdf: [],
            QryGroup1: $("#QryGroup1").prop('checked'), QryGroup2: $("#QryGroup2").prop('checked'), QryGroup3: $("#QryGroup3").prop('checked'),
            QryGroup4: $("#QryGroup4").prop('checked'), QryGroup5: $("#QryGroup5").prop('checked'), QryGroup6: $("#QryGroup6").prop('checked'),
            QryGroup7: $("#QryGroup7").prop('checked'), QryGroup8: $("#QryGroup8").prop('checked'), QryGroup9: $("#QryGroup9").prop('checked'),
            QryGroup10: $("#QryGroup10").prop('checked'), QryGroup11: $("#QryGroup11").prop('checked'), QryGroup12: $("#QryGroup12").prop('checked'),
            QryGroup13: $("#QryGroup13").prop('checked'), QryGroup14: $("#QryGroup14").prop('checked'), QryGroup15: $("#QryGroup15").prop('checked'),
            QryGroup16: $("#QryGroup16").prop('checked'), QryGroup17: $("#QryGroup17").prop('checked'), QryGroup18: $("#QryGroup18").prop('checked'),
            QryGroup19: $("#QryGroup19").prop('checked'), QryGroup20: $("#QryGroup20").prop('checked'), QryGroup21: $("#QryGroup21").prop('checked'),
            QryGroup22: $("#QryGroup22").prop('checked'), QryGroup23: $("#QryGroup23").prop('checked'), QryGroup24: $("#QryGroup24").prop('checked'),
            QryGroup25: $("#QryGroup25").prop('checked'), QryGroup26: $("#QryGroup26").prop('checked'), QryGroup27: $("#QryGroup27").prop('checked'),
            QryGroup28: $("#QryGroup28").prop('checked'), QryGroup29: $("#QryGroup29").prop('checked'), QryGroup30: $("#QryGroup30").prop('checked'),
            QryGroup31: $("#QryGroup31").prop('checked'), QryGroup32: $("#QryGroup32").prop('checked'), QryGroup33: $("#QryGroup33").prop('checked'),
            QryGroup34: $("#QryGroup34").prop('checked'), QryGroup35: $("#QryGroup35").prop('checked'), QryGroup36: $("#QryGroup36").prop('checked'),
            QryGroup37: $("#QryGroup37").prop('checked'), QryGroup38: $("#QryGroup38").prop('checked'), QryGroup39: $("#QryGroup39").prop('checked'),
            QryGroup40: $("#QryGroup40").prop('checked'), QryGroup41: $("#QryGroup41").prop('checked'), QryGroup42: $("#QryGroup42").prop('checked'),
            QryGroup43: $("#QryGroup43").prop('checked'), QryGroup44: $("#QryGroup44").prop('checked'), QryGroup45: $("#QryGroup45").prop('checked'),
            QryGroup46: $("#QryGroup46").prop('checked'), QryGroup47: $("#QryGroup47").prop('checked'), QryGroup48: $("#QryGroup48").prop('checked'),
            QryGroup49: $("#QryGroup49").prop('checked'), QryGroup50: $("#QryGroup50").prop('checked'), QryGroup51: $("#QryGroup51").prop('checked'),
            QryGroup52: $("#QryGroup52").prop('checked'), QryGroup53: $("#QryGroup53").prop('checked'), QryGroup54: $("#QryGroup54").prop('checked'),
            QryGroup55: $("#QryGroup55").prop('checked'), QryGroup56: $("#QryGroup56").prop('checked'), QryGroup57: $("#QryGroup57").prop('checked'),
            QryGroup58: $("#QryGroup58").prop('checked'), QryGroup59: $("#QryGroup59").prop('checked'), QryGroup60: $("#QryGroup60").prop('checked'),
            QryGroup61: $("#QryGroup61").prop('checked'), QryGroup62: $("#QryGroup62").prop('checked'), QryGroup63: $("#QryGroup63").prop('checked'),
            QryGroup64: $("#QryGroup64").prop('checked')
        };

        BusinessPartnerView.Addresses.push({
            Address : $("#AdrsIDB").val(),
            CardCode : $("#CardCode").val(),
            Street: $("#SPOBoxB").val(),
            Block: $("#BlockB").val(),
            ZipCode: $("#ZipCodeB").val(),
            City: $("#CityB").val(),
            County: $("#CountyB").val(),
            Country: $("#CountryB").val(),
            State: $("#StateB").val(),
            LineNum: $("#LineNumB").val(),
            Building: $("#BuildingB").val(),
            AdresType: "B",
            StreetNo: $("#SNOB").val(),
            GlblLocNum: $("#GlbLocNumB").val()
        });

        BusinessPartnerView.Addresses.push({
            Address: $("#AdrsIDS").val(),
            CardCode: $("#CardCode").val(),
            Street: $("#SPOBoxS").val(),
            Block: $("#BlockS").val(),
            ZipCode: $("#ZipCodeS").val(),
            City: $("#CityS").val(),
            County: $("#CountyS").val(),
            Country: $("#CountryS").val(),
            State: $("#StateS").val(),
            LineNum: $("#LineNumS").val(),
            Building: $("#BuildingS").val(),
            AdresType: "S",
            StreetNo: $("#SNOS").val(),
            GlblLocNum: $("#GlbLocNumS").val(),
            LicTradNum: $("#FTIS").val(),
            TaxCode: $("#TOS").val()
        });

        var listUDFHEAD = $("input[id^=HEADUDF_]");
        var listUDFHEADSelect = $("select[id^=HEADUDF_]");
        for (i = 0; i < listUDFHEAD.length; i++) {
            BusinessPartnerView.MappedUdf.push({
                UDFName: listUDFHEAD[i].id.substring(8),
                Value: $("#" + listUDFHEAD[i].id).val(),
            });
        }
        for (i = 0; i < listUDFHEADSelect.length; i++) {
            BusinessPartnerView.MappedUdf.push({
                UDFName: listUDFHEADSelect[i].id.substring(8),
                Value: $("#" + listUDFHEADSelect[i].id).val(),
            });
        }

        $.ajax({
            url: "/MasterData/BusinessPartner/Update",
            async: false,
            type: "POST",
            data: JSON.stringify(BusinessPartnerView),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data == "Ok") {
                    $('#Loading').modal('hide');
                    $("#UpdateCorrectly").show("slow");
                    $("#UpdateCorrectly").delay(1500).slideUp(1000);
                }
                else {
                   Error(data);
                }
            }
        });
    }

    function Error(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }

    function Validate() {

        if (validateEmail($("#E_Mail").val()) == false) {
            alertify.alert("Email no válido");
            $("#E_Mail").css("background-color", "#f99393");
            $("#E_Mail").focus();
            $('.nav-tabs a[href="#TabCI"]').tab('show');
            return false;
        }
        else {
            $("#E_Mail").css("background-color", "");
        }

        if ($("#LicTradNum").val() == "") {
            alertify.alert("Ingrese el CUIT");
            $("#LicTradNum").css("background-color", "#f99393");
            $("#LicTradNum").focus();
            $('.nav-tabs a[href="#TabCI"]').tab('show');
            return false;
        }
        else {
            $("#LicTradNum").css("background-color", "");
        }

        if ($("#Cellular").val() == "") {
            alertify.alert("Ingrese el Celular");
            $("#Cellular").css("background-color", "#f99393");
            $("#Cellular").focus();
            $('.nav-tabs a[href="#TabCI"]').tab('show');
            return false;
        }
        else {
            $("#Cellular").css("background-color", "");
        }

        if ($("#AdrsIDB").val() == "") {
            alertify.alert("Ingrese la Direccion");
            $("#AdrsIDB").css("background-color", "#f99393");
            $("#AdrsIDB").focus();
            $('.nav-tabs a[href="#TabBilling"]').tab('show');
            return false;
        }
        else {
            $("#AdrsIDB").css("background-color", "");
        }

        if ($("#SPOBoxB").val() == "") {
            alertify.alert("Ingrese la Calle");
            $("#SPOBoxB").css("background-color", "#f99393");
            $("#SPOBoxB").focus();
            $('.nav-tabs a[href="#TabBilling"]').tab('show');
            return false;
        }
        else {
            $("#SPOBoxB").css("background-color", "");
        }

        if ($("#CityB").val() == "") {
            alertify.alert("Ingrese la Ciudad");
            $("#CityB").css("background-color", "#f99393");
            $("#CityB").focus();
            $('.nav-tabs a[href="#TabBilling"]').tab('show');
            return false;
        }
        else {
            $("#CityB").css("background-color", "");
        }

        if ($("#ZipCodeB").val() == "") {
            alertify.alert("Ingrese el Codigo Postal");
            $("#ZipCodeB").css("background-color", "#f99393");
            $("#ZipCodeB").focus();
            $('.nav-tabs a[href="#TabBilling"]').tab('show');
            return false;
        }
        else {
            $("#ZipCodeB").css("background-color", "");
        }
        
        if ($("#SNOB").val() == "") {
            alertify.alert("Ingrese el Nro de Calle");
            $("#SNOB").css("background-color", "#f99393");
            $("#SNOB").focus();
            $('.nav-tabs a[href="#TabBilling"]').tab('show');
            return false;
        }
        else {
            $("#SNOB").css("background-color", "");
        }
        
        return true;
    }

    $.getScript("../../Scripts/ExternalJS/CustomizeBP.js", function (data, textStatus, jqxhr) { });

});