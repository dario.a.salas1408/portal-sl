﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class CostSheetController : Controller
    {
        private CostSheetManagerView mCostSheetManagerView;
        private PDMManagerView mPDMManagerView;
        private UserManagerView mUserManagerView;
        private CompaniesManagerView mCompaniesManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;
        private ItemMasterManagerView mItemMasterManagerView;

        public CostSheetController()
        {
            mCostSheetManagerView = new CostSheetManagerView();
            mPDMManagerView = new PDMManagerView();
            mUserManagerView = new UserManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
            mItemMasterManagerView = new ItemMasterManagerView();
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.CostSheet, Enums.Actions.List)]
        public ActionResult Index()
        {
            return View(new List<ARGNSCostSheetView>());
        }

        [HttpPost]
        public PartialViewResult _GetCostSheetList(string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            List<ARGNSCostSheetView> ListCostSheet = mCostSheetManagerView.GetCostSheetListSearch(((UserView)Session["UserLogin"]).CompanyConnect, txtStyle, txtCodeCostSheet, txtDescription);

            return PartialView("_CostSheetList", ListCostSheet);
        }

        public ActionResult _CostSheet(string ActionMyCostSheet, string Code, string ModeCodeDb, string fromController)
        {
            string mPageKey = PageKeyGenerator.GetPageKey();
            switch (fromController)
            {
                case "CostSheet":
                    TempData[mPageKey + "URLRedirect"] = "/PLM/CostSheet/_CostSheet?ActionMyCostSheet=Update&Code={$CostSheetCode}&ModeCodeDb={$ModelCodeDb}&fromController=CostSheet";
                    break;
                case "MyCostSheet":
                    TempData[mPageKey + "URLRedirect"] = "/PLM/CostSheet/_CostSheet?ActionMyCostSheet=Update&Code={$CostSheetCode}&ModeCodeDb={$ModelCodeDb}&fromController=CostSheet";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            
            string mTest = (string)Session["LocalCurrency"];
            ViewBag.PageKey = mPageKey;
            ViewBag.FormMode = ActionMyCostSheet;
            ARGNSCostSheetView mModel = mCostSheetManagerView.GetListCostSheetByCode(((UserView)Session["UserLogin"]).CompanyConnect, Code, ModeCodeDb, ((UserView)Session["UserLogin"]).IdUser);
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();

            mModel.ModeDBCode = ModeCodeDb;

            TempData[mPageKey + "CostSheet"] = mModel;
            TempData[mPageKey + "UserSettingTemp"] = userSetting;
            ViewBag.controllerName = fromController;
            return View("_CostSheet", Tuple.Create(mModel, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.CostSheet, ((UserView)Session["UserLogin"]).CompanyConnect), userSetting));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetModelListSearch(string pCodeModel = "", string pNameModel = "", 
            string pSeasonModel = "", string pGroupModel = "", string pBrand = "", 
            string pCollection = "", string pSubCollection = "")
        {
            int pStart = 0;
            int pLength = 100; 

            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            bool hasPermissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
                 .Where(c => c.PageInternalKey == Enums.Pages.CostSheet.ToDescriptionString()
                 && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
                 .FirstOrDefault() != null ? true : false;

            JsonObjectResult mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                hasPermissionToViewStock, pStart, pLength, pCodeModel,
                pNameModel, pSeasonModel, pGroupModel, pBrand, pCollection, pSubCollection);

            return PartialView("_ModelList", mJsonObjectResult);
        }

        [HttpPost]
        public PartialViewResult _GetCostSchemaTable(string pCode, string pName)
        {
            List<ARGNSSchema> mSchemaList = mCostSheetManagerView.GetCSSchemaList(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pName);

            return PartialView("_CostSchemaTable", mSchemaList);
        }

        [HttpPost]
        public PartialViewResult _GetSchemaLines(string pPageKey, string pCode)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ARGNSSchemaLine, ARGNSCSSchema>(); }).CreateMapper();

            ARGNSCostSheetView mCostSheet = (ARGNSCostSheetView)TempData[pPageKey + "CostSheet"];
            TempData[pPageKey + "CostSheet"] = mCostSheet;

            List<ARGNSSchemaLine> mSchemaLineList = mCostSheetManagerView.GetCSSchemaLineList(((UserView)Session["UserLogin"]).CompanyConnect, pCode);
            List<ARGNSCSSchema> mCSSchemaList = mapper.Map<List<ARGNSCSSchema>>(mSchemaLineList);
            mCSSchemaList = mCSSchemaList.Select(c => { c.IsNew = true; c.MappedUdf = mCostSheet.mCostSheetUDF.ListUDFSchema; return c; }).ToList();

            return PartialView("_SchemaLines", Tuple.Create(mCSSchemaList, mCostSheet.CostSheetCombo));
        }

        [HttpPost]
        public PartialViewResult _GetOperationsTemplateTable(string pCode, string pName)
        {
            List<ARGNSOperTemplate> mARGNSOperTemplateList = mCostSheetManagerView.GetCSOperationTemplateList(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pName);

            return PartialView("_OperationsTemplateTable", mARGNSOperTemplateList);
        }

        [HttpPost]
        public PartialViewResult _GetOperTemplateLines(string pPageKey, string pCode)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ARGNSOperTemplateLine, ARGNSCSOperation>(); }).CreateMapper();

            ARGNSCostSheetView mCostSheet = (ARGNSCostSheetView)TempData[pPageKey + "CostSheet"];
            TempData[pPageKey + "CostSheet"] = mCostSheet;

            List<ARGNSOperTemplateLine> mOperTemplateList = mCostSheetManagerView.GetCSOperationTemplateLineList(((UserView)Session["UserLogin"]).CompanyConnect, pCode);
            List<ARGNSCSOperation> mCSOperationeList = mapper.Map<List<ARGNSCSOperation>>(mOperTemplateList);
            mCSOperationeList = mCSOperationeList.Select(c => { c.IsNew = true; c.MappedUdf = mCostSheet.mCostSheetUDF.ListUDFOperation; return c; }).ToList();

            return PartialView("_OperationLines", Tuple.Create(mCSOperationeList, mCostSheet.CostSheetCombo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCode"></param>
        /// <param name="pName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetPatternTemplateTable(string pCode, string pName)
        {
            List<ARGNSPatternTemplate> mARGNSPatternTemplateList = mCostSheetManagerView.GetCSPatternList(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pName);

            return PartialView("_PatternsTemplateTable", mARGNSPatternTemplateList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetPatternTemplateLines(string pPageKey, string pCode)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ARGNSPatternTemplateLine, ARGNSCSPatterns>(); }).CreateMapper();

            ARGNSCostSheetView mCostSheet = (ARGNSCostSheetView)TempData[pPageKey + "CostSheet"];
            TempData[pPageKey + "CostSheet"] = mCostSheet;

            List<ARGNSPatternTemplateLine> mPatternTemplateLineList = mCostSheetManagerView.GetCSPatternLineList(((UserView)Session["UserLogin"]).CompanyConnect, pCode);
            List<ARGNSCSPatterns> mCSPatteernsList = mapper.Map<List<ARGNSCSPatterns>>(mPatternTemplateLineList);
            mCSPatteernsList = mCSPatteernsList.Select(c => { c.IsNew = true; c.MappedUdf = mCostSheet.mCostSheetUDF.ListUDFPattern; return c; }).ToList();

            return PartialView("_PatternLines", Tuple.Create(mCSPatteernsList, mCostSheet.CostSheetCombo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pTable"></param>
        /// <param name="pNextId"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        public ActionResult _GetRow(string pTable, int pNextId, string pPageKey)
        {
            ARGNSCostSheetView mCostSheet = (ARGNSCostSheetView)TempData[pPageKey + "CostSheet"];
            TempData[pPageKey + "CostSheet"] = mCostSheet;
            
            switch (pTable)
            {
                case "Material":
                    UsersSettingView mUserSetting = (UsersSettingView)TempData[pPageKey + "UserSettingTemp"];
                    TempData[pPageKey + "UserSettingTemp"] = mUserSetting;
                    return PartialView("_RowMaterial", Tuple.Create(new ARGNSCSMaterial(pNextId, mCostSheet.mCostSheetUDF.ListUDFMaterial), mUserSetting, mCostSheet.CostSheetCombo));
                    break;
                case "Operation":
                    return PartialView("_RowOperation", Tuple.Create(new ARGNSCSOperation(pNextId, mCostSheet.mCostSheetUDF.ListUDFOperation), mCostSheet.CostSheetCombo));
                    break;
                case "Schema":
                    return PartialView("_RowSchema", Tuple.Create(new ARGNSCSSchema(pNextId, mCostSheet.mCostSheetUDF.ListUDFSchema), mCostSheet.CostSheetCombo));
                    break;
                default:
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pForm"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateCostSheet(FormCollection pForm)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ARGNSCostSheetView mCostSheet = serializer.Deserialize<ARGNSCostSheetView>(pForm["ARGNSCostSheetView"]);
            string mPageKey = serializer.Deserialize<string>(pForm["pPageKey"]);

            return Json(mCostSheetManagerView.Update(mCostSheet, ((UserView)Session["UserLogin"]).CompanyConnect));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pForm"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddCostSheet(FormCollection pForm)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ARGNSCostSheetView mCostSheet = serializer.Deserialize<ARGNSCostSheetView>(pForm["ARGNSCostSheetView"]);
            string mPageKey = serializer.Deserialize<string>(pForm["pPageKey"]);
            string mURLRedirect = (string)TempData[mPageKey + "URLRedirect"];
            JsonObjectResult mJsonObjectResult = mCostSheetManagerView.AddCostSheet(mCostSheet, ((UserView)Session["UserLogin"]).CompanyConnect);
            mJsonObjectResult.RedirectUrl = mURLRedirect.Replace("{$CostSheetCode}", mJsonObjectResult.Code).Replace("{$ModelCodeDb}", mJsonObjectResult.Others["ModelCodeDB"]);

            return Json(mJsonObjectResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _GetWarehouseList()
        {
            try
            {
                List<ListItem> WhList = mUserManagerView.GetWarehouseList(((UserView)Session["UserLogin"]).CompanyConnect);

                return Json(WhList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// /
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _GetDistributionRuleList()
        {
            try
            {
                List<ListItem> DistributionRuleList = mUserManagerView.GetDistributionRuleList(((UserView)Session["UserLogin"]).CompanyConnect);

                return Json(DistributionRuleList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocDate"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateRateList(DateTime DocDate, string pPageKey)
        {
            ARGNSCostSheetView mCostSheet = (ARGNSCostSheetView)TempData[pPageKey + "CostSheet"];
            List<RatesSystem> ListRates = mCostSheetManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mCostSheet.CostSheetCombo.CurrencyList, DocDate);
            TempData[pPageKey + "CostSheet"] = mCostSheet;
            try
            {
                if (ListRates != null)
                {
                    return Json(ListRates, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pRowNum"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetItemsModel(int pRowNum)
        {
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");

            return PartialView("_Items", Tuple.Create(mOITMUDFList, pRowNum));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pRowNum"></param>
        /// <param name="pListItemsInMaterialsRow"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetItemsPattern(int pRowNum, List<string> pListItemsInMaterialsRow)
        {
            List<ItemMasterView> mItemList = mItemMasterManagerView.GetOITMListByList(((UserView)Session["UserLogin"]).CompanyConnect, pListItemsInMaterialsRow, "F");

            return PartialView("_ItemsPattern", Tuple.Create(mItemList, pRowNum));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>();
                    cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); })
                    .CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(((UserView)Session["UserLogin"]).CompanyConnect, 
                    "", "","", "N", "", searchViewModel.pMappedUdf, 
                    "", ((UserView)Session["UserLogin"]).IdUser, searchViewModel.pItemCode, 
                    searchViewModel.pItemName, false, 
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), false, 
                    searchViewModel.pCardCode, "", requestModel.Start, 
                    requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw, 
                    mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), 
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), 
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), 
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}