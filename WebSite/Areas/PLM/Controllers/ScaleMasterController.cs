﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class ScaleMasterController : Controller
    {
        private ScaleMasterManagerView mMamScaleMan = new ScaleMasterManagerView();

         [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.ScaleMaster, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<ScaleView> ListModel = new List<ScaleView>();
                return View(ListModel);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult ScaleList(string txtScaleCode = "", string txtScaleName = "")
        {
            List<ScaleView> ScaleList = mMamScaleMan.GetColorListSearch(((UserView)Session["UserLogin"]).CompanyConnect, txtScaleCode, txtScaleName);
            return PartialView("_ScaleList", ScaleList);
        }

        [HttpPost]
        public PartialViewResult GetScaleDescriptionListSearch(string pScaleCode = "")
        {
            List<ARGNSSize> SizeList = mMamScaleMan.GetScaleDescriptionListSearch(((UserView)Session["UserLogin"]).CompanyConnect, pScaleCode);
            return PartialView("_SizesTable", SizeList);
        }

    }
}