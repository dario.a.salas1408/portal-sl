﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.PLM.Activity;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class ProjectController : Controller
    {
        private ProjectManagerView mProjectMangerView;

        public ProjectController()
        {
            mProjectMangerView = new ProjectManagerView();
        }

        public ActionResult ProjectAction(string pAction, string pCode, string pModelCode, string pModelId)
        {
            try
            {
                ProjectView mProjectView = null;
                string mPageKey = PageKeyGenerator.GetPageKey();
                switch (pAction)
                {
                    case "Add":
                        ViewBag.Title = Activity.Addnew;
                        ViewBag.FormMode = pAction;
                        mProjectView = mProjectMangerView.GetProject(((UserView)Session["UserLogin"]).CompanyConnect, "0", pModelId);
                        break;

                    case "Update":
                        ViewBag.Title = Activity.Editnew;
                        ViewBag.FormMode = pAction;
                        mProjectView = mProjectMangerView.GetProject(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pModelId);
                        break;

                    case "View":
                        ViewBag.Title = Activity.ViewActivity;
                        ViewBag.FormMode = pAction;
                        mProjectView = mProjectMangerView.GetProject(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pModelId);
                        break;

                }
                mProjectView.PageKey = mPageKey;
                mProjectView.U_Model = pModelCode;
                mProjectView.ModelID = pModelId;
                TempData[mPageKey + "ProjectView"] = mProjectView;

                return View("_Project", mProjectView);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PartialViewResult _GetWorkflowLines(string pPageKey, string pWorkflow)
        {
            try
            {
                ProjectView mProjectView = (ProjectView)TempData[pPageKey + "ProjectView"]; 

                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ARGNSRoutingLine, ARGNSCrPathActivitiesView>(); }).CreateMapper();

                List<ARGNSRoutingLine> mRoutingLines = mProjectMangerView.GetWorkflowLines(((UserView)Session["UserLogin"]).CompanyConnect, pWorkflow);
                string mDefaultBP = mProjectMangerView.GetCriticalPathDefaultBP(((UserView)Session["UserLogin"]).CompanyConnect);
                List<ARGNSCrPathActivitiesView> mReturnList = new List<ARGNSCrPathActivitiesView>();
                foreach (ARGNSRoutingLine mAuxRoutingLine in mRoutingLines)
                {
                    ARGNSCrPathActivitiesView mARGNSCrPathActivitiesViewAux = new ARGNSCrPathActivitiesView();
                    mARGNSCrPathActivitiesViewAux.LineId = mAuxRoutingLine.LineId;
                    mARGNSCrPathActivitiesViewAux.U_PhaseId = mAuxRoutingLine.U_Workflow;
                    mARGNSCrPathActivitiesViewAux.U_Desc = mAuxRoutingLine.U_WorkfDes;
                    mARGNSCrPathActivitiesViewAux.U_Depart = mAuxRoutingLine.U_Depart;
                    mARGNSCrPathActivitiesViewAux.U_Role = mAuxRoutingLine.U_Role;
                    mARGNSCrPathActivitiesViewAux.U_Manager = mAuxRoutingLine.U_Manager;
                    mARGNSCrPathActivitiesViewAux.U_User = mAuxRoutingLine.U_User;
                    mARGNSCrPathActivitiesViewAux.U_PlanLead = mAuxRoutingLine.U_LeadTime;

                    mReturnList.Add(mARGNSCrPathActivitiesViewAux);
                }
                TempData[pPageKey + "ProjectView"] = mProjectView;

                return PartialView("_ActivitiesTable", Tuple.Create(mReturnList, mProjectView.DepartmentList, mProjectView.RolSAPList, mDefaultBP));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PartialViewResult _GetUserList(string pCode, string pName)
        {
            try
            {
                List<UserSAP> mUserList = mProjectMangerView.GetUserSAPList(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pName);

                return PartialView("_UserTable", mUserList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PartialViewResult _GetEmployeeList(string pCode, string pName, short? pDepartment)
        {
            try
            {
                List<EmployeeSAP> mEmployeeList = mProjectMangerView.GetEmployeeList(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pName, pDepartment);

                return PartialView("_EmployeeTable", mEmployeeList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PartialViewResult _GetBPList(string pCode, string pName)
        {
            try
            {
                List<BusinessPartnerView> mBPList = mProjectMangerView.GetBPList(((UserView)Session["UserLogin"]).CompanyConnect, pCode, pName);

                return PartialView("_BPTable", mBPList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult _CreateActivities(List<ARGNSCrPathActivitiesView> pActivityList, string pModelCode, string pModelId)
        {
            try
            {
                List<ActivitiesView> mActivityList = new List<ActivitiesView>();
                foreach(ARGNSCrPathActivitiesView mActivity in pActivityList)
                {
                    ActivitiesView mActivityAux = new ActivitiesView();
                    mActivityAux.Action = "T";
                    mActivityAux.CntctType = -1;
                    if(!string.IsNullOrEmpty(mActivity.U_Manager))
                    {
                        List<UserSAP> mUserList = mProjectMangerView.GetUserSAPList(((UserView)Session["UserLogin"]).CompanyConnect, "", mActivity.U_Manager);
                        mActivityAux.AttendUser = mUserList.FirstOrDefault().USERID;
                    }
                    if (!string.IsNullOrEmpty(mActivity.U_User))
                    {
                        List<EmployeeSAP> mEmployeeList = mProjectMangerView.GetEmployeeList(((UserView)Session["UserLogin"]).CompanyConnect, mActivity.U_User, "", null);
                        mActivityAux.AttendEmpl = mEmployeeList.FirstOrDefault().empID;
                    }
                    mActivityAux.AssignedUserName = ((UserView)Session["UserLogin"]).UserNameSAP;
                    mActivityAux.CardCode = mActivity.U_Bp;
                    mActivityAux.Details = pModelCode + " - " + mActivity.U_Desc;
                    mActivityAux.Recontact = mActivity.U_PlSDate.Value;
                    mActivityAux.endDate = mActivity.U_PlCDate.Value;
                    mActivityAux.Duration = (decimal)(mActivity.U_PlCDate.Value - mActivity.U_PlSDate.Value).TotalDays;
                    mActivityAux.status = -2;
                    mActivityAux.Priority = "1";
                    mActivityAux.U_ARGNS_PRDCODE = pModelCode;
                    mActivityAux.U_ARGNS_PROYECT = pModelId + "-"+ pModelCode + "-"+mActivity.Code;
                    mActivityAux.U_ARGNS_WORKF2 = mActivity.U_PhaseId;
                    mActivityAux.U_ARGNS_JOBID = 0;
                    mActivityAux.ProjectLineId = mActivity.LineId;
                    mActivityAux.ClgCode = Convert.ToInt32(mActivity.Code);

                    mActivityList.Add(mActivityAux);
                }

                return Json(mProjectMangerView.CreateActivities(((UserView)Session["UserLogin"]).CompanyConnect, mActivityList));
            }
            catch (Exception ex)
            {
                throw ex;
                
            }
        }

        [HttpPost]
        public string UpdateProject(ProjectView pProject)
        {
            return mProjectMangerView.Update(((UserView)Session["UserLogin"]).CompanyConnect, pProject);

        }

        [HttpPost]
        public JsonResult AddProject(ProjectView pProject)
        {
            JsonObjectResult mJsonObjectResult = mProjectMangerView.Add(((UserView)Session["UserLogin"]).CompanyConnect, pProject);
            mJsonObjectResult.RedirectUrl = "/PLM/Project/ProjectAction?pAction=Update&pCode=" + mJsonObjectResult.Code + "&pModelCode=" + pProject.U_Model + "&pModelId=" + pProject.ModelID;

            return Json(mJsonObjectResult);
        }
    }
}