﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PLM.PDM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class DesignController : Controller
    {
        private DesignManagerView mDesignManagerView;
        private string mPageKey;

        public DesignController()
        {
            mDesignManagerView = new DesignManagerView();
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.StyleList, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<ARGNSModelView> ListModel = new List<ARGNSModelView>();
                PDMSAPCombo mSAPCombo = new PDMSAPCombo();
                return View(Tuple.Create(ListModel, mSAPCombo));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.StyleGrid, Enums.Actions.List)]
        public ActionResult IndexGrid()
        {
            try
            {
                List<ARGNSModelView> ListModel = new List<ARGNSModelView>();
                PDMSAPCombo mSAPCombo = new PDMSAPCombo();
                return View("IndexGrid", Tuple.Create(ListModel, mSAPCombo));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("IndexGrid", "Error");
            }
        }

        public ActionResult ActionDesign(string ActionDesign, string ModelCode, string From)
        {
            ARGNSModelView mModelView = null;
            mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            switch (From)
            {
                case "DesignList":
                    ViewBag.URLRedirect = "/PLM/Design/Index";
                    break;
                case "DesignGrid":
                    ViewBag.URLRedirect = "/PLM/Design/IndexGrid";
                    break;

            }

            switch (ActionDesign)
            {
                case "UpdateDesign":
                    ViewBag.FormMode = ActionDesign;
                    mModelView = mDesignManagerView.GetDesignById(((UserView)Session["UserLogin"]).CompanyConnect, ModelCode);
                    ViewBag.Tite = PDM.Model + " " + mModelView.U_ModCode;
                    break;
                case "AddDesign":
                    ViewBag.FormMode = ActionDesign;
                    mModelView = mDesignManagerView.GetDesignById(((UserView)Session["UserLogin"]).CompanyConnect, ModelCode);
                    ViewBag.Tite = PDM.ND;
                    break;    
            }

            TempData[mPageKey + "ModelImageList"] = mModelView.ModelImageList;
            TempData[mPageKey + "Model"] = mModelView;
            return View("_Design", mModelView);
        }

        [HttpPost]
        public PartialViewResult ListDesign(string txtCodeModel = "", 
			string txtNameModel = "", string txtSeasonModel = "", 
			string txtGroupModel = "", string txtBrand = "", 
			string txtCollection = "", string txtSubCollection = "", 
			int pStart = 0, int pLength = 100)
        {
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();

            List<ARGNSModelView> ListDesign = mDesignManagerView.GetDesignListSearch((
				(UserView)Session["UserLogin"]).CompanyConnect, 
				ref mPDMSAPCombo, txtCodeModel, txtNameModel, 
				txtSeasonModel, txtGroupModel, txtBrand, 
				txtCollection, txtSubCollection, pStart, pLength);

            return PartialView("_DesignList", Tuple.Create(ListDesign, mPDMSAPCombo));
        }

        [HttpPost]
        public PartialViewResult ListDesignGrid(string txtCodeModel = "", 
												string txtNameModel = "", 
												string txtSeasonModel = "", 
												string txtGroupModel = "", 
												string txtBrand = "", 
												string txtCollection = "", 
												string txtSubCollection = "", 
												int pStart = 0, int pLength = 100)
        {
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();

            List<ARGNSModelView> ListDesign = mDesignManagerView.GetDesignListSearch(((UserView)Session["UserLogin"]).CompanyConnect, 
					ref mPDMSAPCombo, txtCodeModel, txtNameModel, txtSeasonModel, txtGroupModel, 
					txtBrand, txtCollection, txtSubCollection, pStart, pLength);

            return PartialView("_DesignGrid", Tuple.Create(ListDesign, mPDMSAPCombo));
        }

        [HttpPost]
        public JsonResult UpdateDesign(ARGNSModelView design)
        {
            mPageKey = design.PageKey;
            List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
            design.ModelImageList = ListaImg;
            if (ListaImg.Count > 0 && (design.U_Pic == null || design.U_Pic == ""))
            {
                design.U_Pic = ListaImg.FirstOrDefault().U_Path;
                design.U_PicR = ListaImg.FirstOrDefault().U_Path;
            }
            TempData[mPageKey + "ModelImageList"] = ListaImg;

            return Json(mDesignManagerView.UpdateDesign(design, ((UserView)Session["UserLogin"]).CompanyConnect)); 
        }

        [HttpPost]
        public JsonResult AddDesign(ARGNSModelView design, FormCollection pData)
        {
            mPageKey = design.PageKey;
            List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
            design.ModelImageList = ListaImg;
            if(ListaImg.Count > 0)
            {
                design.U_Pic = ListaImg.FirstOrDefault().U_Path;
                design.U_PicR = ListaImg.FirstOrDefault().U_Path;
            }
            TempData[mPageKey + "ModelImageList"] = ListaImg;

            return Json(mDesignManagerView.AddDesign(design, ((UserView)Session["UserLogin"]).CompanyConnect));
        }

        [HttpPost]
        public JsonResult UploadFile(FormCollection pData)
        {
            string mFileName = string.Empty;
            string mUModCode = pData["mUModCode"].ToString();
            mPageKey = pData["mPageKey"].ToString();
            try
            {
                List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
                if (ListaImg == null)
                {
                    ListaImg = new List<ARGNSModelImg>();
                }
                string mFolder = mUModCode;
                string mFullPath = string.Empty;
                string mName = string.Empty;
                string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;
                    foreach (string mKey in mFiles)
                    {
                        ARGNSModelImg mObj = new ARGNSModelImg();
                        HttpPostedFileBase mFilePost = mFiles[mKey];
                        mName = mFilePost.FileName;
                        mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

                        if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
                        }
                        mFileName = Server.MapPath("~/images/" + mFolder + "/" + mFileName);
                        mObj.U_File = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;
                        mObj.U_Path = mFileName;
                        mObj.Object = "ARGNS_MODEL";
                        mObj.Object = "ARGNS_MODEL";
                        mObj.U_FType = "HDR";
                        mFullPath = mFileName;
                        mFilePost.SaveAs(mFileName);
                        ListaImg.Add(mObj);
                    }
                }

                //if (Request.Files.Count > 0)
                //{
                //    HttpFileCollectionBase mFiles = Request.Files;

                //    for (int i = 0; i < mFiles.Count; i++)
                //    {
                //        HttpPostedFileBase mFilePost = mFiles[i];

                //        mName = mFilePost.FileName;

                //        if (mTechPack.Lines.Where(c => c.ImageName == mName).FirstOrDefault().ImgChanged == true)
                //        {
                //            mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

                //            if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
                //            {
                //                Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
                //            }

                //            mFullPath = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

                //            mFilePost.SaveAs(mFullPath);

                //            mTechPack.Lines.Where(c => c.ImageName == mName).FirstOrDefault().U_Imag = mFullPath;
                //        }
                //    }
                //}

                TempData[mPageKey + "ModelImageList"] = ListaImg;
                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public JsonResult DeleteDragAndDropImg()
        {
            return Json("Ok");
        }

    }
}