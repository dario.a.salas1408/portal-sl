﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class VarMasterController : Controller
    {
        private VariableManagerView mVariableMan = new VariableManagerView();

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.VarMaster, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<VariableView> ListModel = new List<VariableView>();
                List<ARGNSSegmentation> ListSegmentation = new List<ARGNSSegmentation>();
                return View(Tuple.Create(ListModel, ListSegmentation));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult VariableList(string txtVarCode = "", string txtVarName = "", string txtVarSegmentation = "")
        {
            List<ARGNSSegmentation> SegmentationList = new List<ARGNSSegmentation>();
            List<VariableView> VariableList = mVariableMan.GetVariableListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ref SegmentationList, txtVarCode, txtVarName, txtVarSegmentation);
            return PartialView("_VariableList", Tuple.Create(VariableList, SegmentationList));
        }
    }
}