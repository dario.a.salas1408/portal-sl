﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PLM.PDM;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
	[CustomerAuthorize(Enums.Areas.PLM)]
	public class PDMController : Controller
	{
		private PDMManagerView mPDMManagerView;
		private CommentManagerView mCommentManagerView;
		private CostSheetManagerView mCostSheetManagerView;
		private SampleManagerView mSampleManagerView;
		private CRPageMapManagerView mCRPageMapManagerView;
		private UserManagerView mUserManagerView;
		private MaterialDetailManagerView mMaterialDetailManagerView;

		public PDMController()
		{
			mPDMManagerView = new PDMManagerView();
			mCommentManagerView = new CommentManagerView();
			mCostSheetManagerView = new CostSheetManagerView();
			mCRPageMapManagerView = new CRPageMapManagerView();
			mSampleManagerView = new SampleManagerView();
			mUserManagerView = new UserManagerView();
			mMaterialDetailManagerView = new MaterialDetailManagerView();
		}

		[CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.StyleList, Enums.Actions.List)]
		public ActionResult Index()
		{
			try
			{
				List<ARGNSModelView> ListModel = new List<ARGNSModelView>();
				PDMSAPCombo mSAPCombo = new PDMSAPCombo();

				return View(Tuple.Create(ListModel, mSAPCombo));
			}
			catch (Exception ex)
			{
				TempData["Error"] = ex.InnerException;

				return RedirectToAction("Index", "Error");
			}
		}

		public ActionResult ActionPDM(string ActionPDM, string ModelCode, string pU_ModCode, string From)
		{
			ARGNSModelView mModelView = null;
			string mPageKey = PageKeyGenerator.GetPageKey();

			switch (ActionPDM)
			{
				case "UpdateModel":
					ViewBag.FormMode = ActionPDM;
					ViewBag.PageKey = mPageKey;
					ViewBag.From = From;
					mModelView = mPDMManagerView.GetModel(ModelCode, pU_ModCode, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
					ViewBag.Tite = PDM.Model + " " + mModelView.U_ModCode;
					break;
				case "AddModel":
					switch (From)
					{
						case "MyStyleList":
							ViewBag.PageFrom = "/PLM/MyStyles/Index";
							break;
						case "MyStyleGrid":
							ViewBag.PageFrom = "/PLM/MyStyles/PDMGrid";
							break;
						case "List":
							ViewBag.PageFrom = "/PLM/PDM/Index";
							break;
						case "Grid":
							ViewBag.PageFrom = "/PLM/PDM/PDMGrid";
							break;
						default:
							ViewBag.PageFrom = "/PLM/PDM/PDMGrid";
							break;
					}
					ViewBag.FormMode = ActionPDM;
					ViewBag.PageKey = mPageKey;
					ViewBag.From = From;
					mModelView = mPDMManagerView.GetModel(ModelCode, pU_ModCode, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
					mModelView.MappedUdf = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MODEL");
					ViewBag.Tite = PDM.Model;
					break;
			}

			TempData[mPageKey + "ModelSegmentation"] = mModelView.ModelSegmentation;
			TempData[mPageKey + "ModelColorList"] = mModelView.ModelColorList;
			TempData[mPageKey + "ModelSizeList"] = mModelView.ModelSizeList;
			TempData[mPageKey + "ModelVariableList"] = mModelView.ModelVariableList;
			TempData[mPageKey + "ModelImageList"] = mModelView.ModelImageList;
			TempData[mPageKey + "ModelPomList"] = mModelView.ModelPomList;
			TempData[mPageKey + "ModelBomList"] = mModelView.ModelBomList;
			TempData[mPageKey + "ModelFittList"] = mModelView.ModelFittList;
			TempData[mPageKey + "ModelInstList"] = mModelView.ModelInstList;
			TempData[mPageKey + "ModelLogosList"] = mModelView.ModelLogosList;
			TempData[mPageKey + "ModelWFLOWList"] = mModelView.ModelWFLOWList;
			TempData[mPageKey + "ModelCSList"] = mModelView.ModelCSList;
			TempData[mPageKey + "Model"] = mModelView;
			return View("_PDM", Tuple.Create(mModelView, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.ProductDataManagement, ((UserView)Session["UserLogin"]).CompanyConnect)));
		}

		[HttpPost]
		public JsonResult UpdateModel(FormCollection pForm)
		{
			try
			{
				JavaScriptSerializer serializer = new JavaScriptSerializer();
				ARGNSModelView model = Newtonsoft.Json.JsonConvert.DeserializeObject<ARGNSModelView>(pForm["ARGNSModelView"]);
				string mPageKey = serializer.Deserialize<string>(pForm["pPageKey"]);

				List<ARGNSModelColor> ListaColorTemp = ((List<ARGNSModelColor>)TempData[mPageKey + "ModelColorList"]).ToList();
				List<ARGNSModelSize> ListaSizeTemp = ((List<ARGNSModelSize>)TempData[mPageKey + "ModelSizeList"]).ToList();
				List<ARGNSModelVar> ListaVariableTemp = ((List<ARGNSModelVar>)TempData[mPageKey + "ModelVariableList"]).ToList();
				List<ARGNSModelImg> ListaImgTemp = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
				List<ARGNSModelPom> ListaPomTemp = ((List<ARGNSModelPom>)TempData[mPageKey + "ModelPomList"]);

				List<ARGNSModelBom> ListaBom = ((List<ARGNSModelBom>)TempData[mPageKey + "ModelBomList"]);
				List<ARGNSModelFitt> ListaFitt = ((List<ARGNSModelFitt>)TempData[mPageKey + "ModelFittList"]);
				List<ARGNSModelInst> ListaInst = ((List<ARGNSModelInst>)TempData[mPageKey + "ModelInstList"]);
				List<ARGNSModelLogos> ListaLogos = ((List<ARGNSModelLogos>)TempData[mPageKey + "ModelLogosList"]);
				List<ARGNSModelWFLOW> ListaWFLOW = ((List<ARGNSModelWFLOW>)TempData[mPageKey + "ModelWFLOWList"]);
				List<ARGNSModelCostSheet> ListaCSModel = ((List<ARGNSModelCostSheet>)TempData[mPageKey + "ModelCSList"]);

				List<ARGNSModelColor> ListaColor = ((List<ARGNSModelColor>)TempData[mPageKey + "ModelColorList"]).Select(c => { c.U_Active = "N"; return c; }).ToList();
				List<ARGNSModelSize> ListaSize = ((List<ARGNSModelSize>)TempData[mPageKey + "ModelSizeList"]).Select(c => { c.U_Selected = "N"; return c; }).ToList();
				List<ARGNSModelVar> ListaVariable = ((List<ARGNSModelVar>)TempData[mPageKey + "ModelVariableList"]).Select(c => { c.U_Selected = "N"; return c; }).ToList();
				List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);

				string mFolder = model.U_ModCode;
				string mFullPath = string.Empty;
				string mName = string.Empty;
				string mFileName = string.Empty;
				string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

				if (Request.Files.Count > 0)
				{
					HttpFileCollectionBase mFiles = Request.Files;

					for (int i = 0; i < mFiles.Count; i++)
					{
						HttpPostedFileBase mFilePost = mFiles[i];

						mName = mFilePost.FileName;

						mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

						if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
						{
							Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
						}

						mFullPath = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

						mFilePost.SaveAs(mFullPath);

						if (ListaImg.Count > 0)
						{
							ListaImg.Add(new ARGNSModelImg { Code = ListaImg.FirstOrDefault().Code, LineId = (ListaImg.Max(c => c.LineId) + 1), U_File = mFileName, U_FType = ListaImg.FirstOrDefault().U_FType, U_Path = mFullPath });
						}
						else
						{
							ListaImg.Add(new ARGNSModelImg { Code = model.Code, LineId = 1, U_File = mFileName, U_FType = "HDR", U_Path = mFullPath });
							model.U_Pic = mFullPath;
						}
					}
				}

				if (model.ModelPomList != null)
				{
					foreach (ARGNSModelPom item in model.ModelPomList)
					{
						ListaPomTemp.Where(c => c.Code == item.Code && c.LineId == item.LineId).FirstOrDefault().U_Value = item.U_Value;
						ListaPomTemp.Where(c => c.Code == item.Code && c.LineId == item.LineId).FirstOrDefault().U_TolPosit = item.U_TolPosit;
						ListaPomTemp.Where(c => c.Code == item.Code && c.LineId == item.LineId).FirstOrDefault().U_TolNeg = item.U_TolNeg;
						ListaPomTemp.Where(c => c.Code == item.Code && c.LineId == item.LineId).FirstOrDefault().U_QAPoint = item.U_QAPoint;
					}
				}

				string[] ListCodeColor = model.ColorTableItems;
				string[] ListCodeScale = model.ScaleTableItems;
				string[] ListCodeVariable = model.VariableTableItems;

				if (ListCodeColor != null)
					ListaColor = ListaColor.Select(c => { c.U_Active = (ListCodeColor.Contains(c.U_ColCode) == true ? "Y" : "N"); return c; }).ToList();

				if (ListCodeScale != null)
					ListaSize = ListaSize.Select(c => { c.U_Selected = (ListCodeScale.Contains(c.U_SizeCod) == true ? "Y" : "N"); return c; }).ToList();

				if (ListCodeVariable != null)
					ListaVariable = ListaVariable.Select(c => { c.U_Selected = (ListCodeVariable.Contains(c.U_VarCode) == true ? "Y" : "N"); return c; }).ToList();

				model.ModelColorList = ListaColor;
				model.ModelSizeList = ListaSize;
				model.ModelVariableList = ListaVariable;
				model.ModelImageList = ListaImg;
				model.ModelPomList = ListaPomTemp;
				model.ModelBomList = ListaBom;
				model.ModelFittList = ListaFitt;
				model.ModelInstList = ListaInst;
				model.ModelLogosList = ListaLogos;
				model.ModelWFLOWList = ListaWFLOW;
				model.ModelCSList = ListaCSModel;

				TempData[mPageKey + "ModelColorList"] = ListaColorTemp;
				TempData[mPageKey + "ModelSizeList"] = ListaSizeTemp;
				TempData[mPageKey + "ModelVariableList"] = ListaVariableTemp;
				TempData[mPageKey + "ModelImageList"] = ListaImgTemp;
				TempData["ModelPomList"] = ListaPomTemp;

				TempData[mPageKey + "ModelBomList"] = ListaBom;
				TempData[mPageKey + "ModelFittList"] = ListaFitt;
				TempData[mPageKey + "ModelInstList"] = ListaInst;
				TempData[mPageKey + "ModelLogosList"] = ListaLogos;
				TempData[mPageKey + "ModelWFLOWList"] = ListaWFLOW;
				TempData[mPageKey + "ModelCSList"] = ListaCSModel;
				return Json(mPDMManagerView.UpdateModel(model, ((UserView)Session["UserLogin"]).CompanyConnect));
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}


		[HttpPost]
		public JsonResult AddModel(FormCollection pForm)
		{
			JavaScriptSerializer serializer = new JavaScriptSerializer();
			ARGNSModelView model = Newtonsoft.Json.JsonConvert.DeserializeObject<ARGNSModelView>(pForm["ARGNSModelView"]);
			string mPageKey = serializer.Deserialize<string>(pForm["pPageKey"]);
			string pFrom = serializer.Deserialize<string>(pForm["pFrom"]);
			//List<KeyValue> mUDFList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValue>>(pForm["pUDFDict"]);
			//model.MappedUdf = mUDFList.ToDictionary(c => c.Key, c => c.Value);

			List<ARGNSModelColor> ListaColorTemp = ((List<ARGNSModelColor>)TempData[mPageKey + "ModelColorList"]).ToList();
			List<ARGNSModelSize> ListaSizeTemp = ((List<ARGNSModelSize>)TempData[mPageKey + "ModelSizeList"]).ToList();
			List<ARGNSModelVar> ListaVariableTemp = ((List<ARGNSModelVar>)TempData[mPageKey + "ModelVariableList"]).ToList();
			List<ARGNSModelImg> ListaImgTemp = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);

			List<ARGNSModelColor> ListaColor = ((List<ARGNSModelColor>)TempData[mPageKey + "ModelColorList"]).Select(c => { c.U_Active = "N"; return c; }).ToList();
			List<ARGNSModelSize> ListaSize = ((List<ARGNSModelSize>)TempData[mPageKey + "ModelSizeList"]).Select(c => { c.U_Selected = "N"; return c; }).ToList();
			List<ARGNSModelVar> ListaVariable = ((List<ARGNSModelVar>)TempData[mPageKey + "ModelVariableList"]).Select(c => { c.U_Selected = "N"; return c; }).ToList();
			List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
			ARGNSSegmentationView mSegmentationTMP = (ARGNSSegmentationView)TempData[mPageKey + "ModelSegmentation"];


			string mFolder = model.U_ModCode;
			string mFullPath = string.Empty;
			string mName = string.Empty;
			string mFileName = string.Empty;
			string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

			if (Request.Files.Count > 0)
			{
				HttpFileCollectionBase mFiles = Request.Files;

				HttpPostedFileBase mFilePost = mFiles[0];

				mName = mFilePost.FileName;

				mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

				if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
				{
					Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
				}

				mFullPath = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

				mFilePost.SaveAs(mFullPath);

				if (ListaImg.Count > 0)
				{
					ListaImg.Add(new ARGNSModelImg { Code = ListaImg.FirstOrDefault().Code, LineId = (ListaImg.Max(c => c.LineId) + 1), U_File = mFileName, U_FType = ListaImg.FirstOrDefault().U_FType, U_Path = mFullPath });
				}
				else
				{
					ListaImg.Add(new ARGNSModelImg { Code = model.Code, LineId = 1, U_File = mFileName, U_FType = "HDR", U_Path = mFullPath });
					model.U_Pic = mFullPath;
				}

			}

			string[] ListCodeColor = model.ColorTableItems;
			string[] ListCodeScale = model.ScaleTableItems;
			string[] ListCodeVariable = model.VariableTableItems;

			if (ListCodeColor != null)
				ListaColor = ListaColor.Select(c => { c.U_Active = (ListCodeColor.Contains(c.U_ColCode) == true ? "Y" : "N"); return c; }).ToList();

			if (ListCodeScale != null)
				ListaSize = ListaSize.Select(c => { c.U_Selected = (ListCodeScale.Contains(c.U_SizeCod) == true ? "Y" : "N"); return c; }).ToList();

			if (ListCodeVariable != null)
				ListaVariable = ListaVariable.Select(c => { c.U_Selected = (ListCodeVariable.Contains(c.U_VarCode) == true ? "Y" : "N"); return c; }).ToList();

			model.ModelColorList = ListaColor;
			model.ModelSizeList = ListaSize;
			model.ModelVariableList = ListaVariable;
			model.ModelImageList = ListaImg;
			model.ModelSegmentation = mSegmentationTMP;

			TempData[mPageKey + "ModelSegmentation"] = mSegmentationTMP;
			TempData[mPageKey + "ModelColorList"] = ListaColorTemp;
			TempData[mPageKey + "ModelSizeList"] = ListaSizeTemp;
			TempData[mPageKey + "ModelVariableList"] = ListaVariableTemp;
			TempData[mPageKey + "ModelImageList"] = ListaImgTemp;

			return Json(mPDMManagerView.AddModel(model, ((UserView)Session["UserLogin"]).CompanyConnect, pFrom));
		}

		[HttpPost]
		public PartialViewResult ListPDM(string txtCodeModel = "", string txtNameModel = "",
			string txtSeasonModel = "", string txtGroupModel = "",
			string txtBrand = "", string txtCollection = "",
			string txtSubCollection = "", int pStart = 0, int pLength = 100)
		{
			PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
			List<ARGNSModelView> ListPDM = mPDMManagerView.GetPDMListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
				ref mPDMSAPCombo, txtCodeModel, txtNameModel,
				txtSeasonModel, txtGroupModel,
				txtBrand, txtCollection, txtSubCollection,
				pStart, pLength);

			return PartialView("_PDMListView", Tuple.Create(ListPDM, mPDMSAPCombo));
		}

		[HttpPost]
		public PartialViewResult UploadAttachment(FormCollection pData)
		{
			string mFileName = string.Empty;
			string mModCode = pData["mCode"].ToString();
			string mUModCode = pData["mUModCode"].ToString();
			string mPageKey = pData["pPageKey"].ToString();
			ARGNSModelView mModel = new ARGNSModelView();
			try
			{
				List<ARGNSModelFile> mFileList = mPDMManagerView.GetModelFileList(((UserView)Session["UserLogin"]).CompanyConnect, mModCode, mUModCode);

				if (Request.Files.Count > 0)
				{
					HttpFileCollectionBase mFiles = Request.Files;
					ARGNSModelFile mObj = new ARGNSModelFile();
					foreach (string mKey in mFiles)
					{
						HttpPostedFileBase mFilePost = mFiles[mKey];
						mFileName = mFilePost.FileName;
						string path = "~/images/" + mUModCode + "_DCFiles/" + mFileName;
						mFileName = Server.MapPath(path);

						mObj.U_File = mFilePost.FileName;
						mObj.U_Path = mFileName;

						mFilePost.SaveAs(mFileName);
						mFileList.Add(mObj);
					}

				}

				List<StockModel> mListStock = new List<StockModel>();
				PDMSAPCombo mListPDMSAPCombo = new PDMSAPCombo();

				mModel = new ARGNSModelView();
				mModel = ((ARGNSModelView)TempData[mPageKey + "Model"]);

				mListStock = mModel.ModelStockList;
				mListPDMSAPCombo = mModel.ListPDMSAPCombo;
				mModel.ModelFileList = mFileList;

				mModel.ListPDMSAPCombo = new PDMSAPCombo();
				mModel.ModelStockList = new List<StockModel>();

				PDMManagerView test = new PDMManagerView();
				bool result = (test.UpdateModel(mModel, ((UserView)Session["UserLogin"]).CompanyConnect) == "Ok" ? true : false);

				PDMManagerView maxi = new PDMManagerView();
				mFileList = maxi.GetModelFileList(((UserView)Session["UserLogin"]).CompanyConnect, mModCode, mUModCode);

				//Limpio la lista y vuelvo a guardar el modelo en los temporales por si necesita subir nuevamente un attachemente sin salir de la pagina.
				mModel.ModelFileList = new List<ARGNSModelFile>();

				mModel.ModelStockList = mListStock;
				mModel.ListPDMSAPCombo = mListPDMSAPCombo;

				TempData[mPageKey + "Model"] = mModel;

				return PartialView("_FilesList", mFileList);
			}
			catch (Exception)
			{
				//Limpio la lista y vuelvo a guardar el modelo en los temporales por si necesita subir nuevamente un attachemente sin salir de la pagina.
				mModel.ModelFileList = new List<ARGNSModelFile>();
				TempData[mPageKey + "Model"] = mModel;
				throw;
			}

		}

		[CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.StyleGrid, Enums.Actions.List)]
		public ActionResult PDMGrid()
		{
			try
			{
				List<ARGNSModelView> ListModel = new List<ARGNSModelView>();
				PDMSAPCombo mSAPCombo = new PDMSAPCombo();
				return View("IndexGrid", Tuple.Create(ListModel, mSAPCombo));
			}
			catch (Exception ex)
			{
				TempData["Error"] = ex.InnerException;
				return RedirectToAction("IndexGrid", "Error");
			}
		}

		[HttpPost]
		public PartialViewResult ListPDMGrid(string txtCodeModel = "", string txtNameModel = "",
			string txtSeasonModel = "", string txtGroupModel = "",
			string txtBrand = "", string txtCollection = "",
			string txtSubCollection = "", int pStart = 0, int pLength = 100)
		{
			PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
			List<ARGNSModelView> ListPDM = mPDMManagerView.GetPDMListSearch(
				((UserView)Session["UserLogin"]).CompanyConnect, ref mPDMSAPCombo,
				txtCodeModel, txtNameModel, txtSeasonModel, txtGroupModel,
				txtBrand, txtCollection, txtSubCollection, pStart, pLength);

			return PartialView("_PDMGrid", Tuple.Create(ListPDM, mPDMSAPCombo));
		}

		[HttpPost]
		public PartialViewResult ListComment(string txtCodeModel = "")
		{
			List<ARGNSModelComment> ListMComm = mCommentManagerView.GetListComments(((UserView)Session["UserLogin"]).CompanyConnect, txtCodeModel);

			return PartialView("_PDMListComment", ListMComm);
		}

		[HttpPost]
		public JsonResult UploadFile(FormCollection pData)
		{
			string mFileName = string.Empty;
			string mModCode = pData["mCode"].ToString();
			string mUModCode = pData["mUModCode"].ToString();

			try
			{

				List<ARGNSModelFile> mFileList = mPDMManagerView.GetModelFileList(((UserView)Session["UserLogin"]).CompanyConnect, mModCode, mUModCode);
				string mFolder = mUModCode + "_DCFiles";
				string mFullPath = string.Empty;
				string mName = string.Empty;
				string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

				if (Request.Files.Count > 0)
				{
					HttpFileCollectionBase mFiles = Request.Files;
					ARGNSModelFile mObj = new ARGNSModelFile();


					foreach (string mKey in mFiles)
					{
						HttpPostedFileBase mFilePost = mFiles[mKey];

						mName = mFilePost.FileName;

						mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

						if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
						{
							Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
						}

						mFileName = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

						mObj.U_File = mFilePost.FileName;
						mObj.U_Path = mFileName;

						mFullPath = mFileName;

						mFilePost.SaveAs(mFileName);
						mFileList.Add(mObj);

					}


				}

				ARGNSModelComment mComment = new ARGNSModelComment { Code = mCode, U_COMMENT = pData["mComment"].ToString(), U_FILE = mName, U_FILEPATH = mFullPath, U_MODCODE = mUModCode, U_USER = ((UserView)Session["UserLogin"]).Name.ToString() };

				return Json(mCommentManagerView.AddComment(mComment, ((UserView)Session["UserLogin"]).CompanyConnect), JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(ex.InnerException.Message);
			}

		}

		[HttpPost]
		public PartialViewResult ListProjects(string txtModelCode = "", string txtModelId = "")
		{
			List<ARGNSCrPathView> ListProjects = mPDMManagerView.GetModelProjectList(((UserView)Session["UserLogin"]).CompanyConnect, txtModelCode);

			return PartialView("_PDMListProjects", Tuple.Create(ListProjects, txtModelId));
		}

		[HttpPost]
		public PartialViewResult GetModelPom(string txtModelCode = "", string pPageKey = "")
		{
			Mapper.CreateMap<ARGNSModelPom, ARGNSModelPomView>();
			Mapper.CreateMap<ARGNSModelPomView, ARGNSModelPom>();
			List<ARGNSModelPomView> mModelPomList = mPDMManagerView.GetModelPomList(((UserView)Session["UserLogin"]).CompanyConnect, txtModelCode);
			TempData[pPageKey + "ModelPomList"] = Mapper.Map<List<ARGNSModelPom>>(mModelPomList);
			return PartialView("_PDMListPom", mModelPomList);
		}

		[HttpPost]
		public PartialViewResult _GetModelPomTemplateList(string pPageKey = "")
		{
			ARGNSModelView mModel = ((ARGNSModelView)TempData[pPageKey + "Model"]);

			List<ARGNSModelPomTemplate> mModelPomList = mPDMManagerView.GetModelPomTemplateList(((UserView)Session["UserLogin"]).CompanyConnect, mModel.ModelSizeList.Select(c => c.U_SclCode).Distinct().ToList(), mModel.ModelPomList.Select(c => c.U_PomCode).Distinct().ToList());

			//TempData[pPageKey + "ModelPomList"] = Mapper.Map<List<ARGNSModelPom>>(mModelPomList);
			TempData[pPageKey + "Model"] = mModel;
			return PartialView("_POMTemplateTable", mModelPomList);
		}

		[HttpPost]
		public PartialViewResult _AddModelPom(string pPageKey, string pPOMCode, string pSclCode)
		{
			try
			{
				IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ARGNSPomTemplateLines, ARGNSModelPom>(); cfg.CreateMap<ARGNSModelPom, ARGNSModelPomView>(); }).CreateMapper();

				ARGNSModelView mModel = ((ARGNSModelView)TempData[pPageKey + "Model"]);

				List<ARGNSPomTemplateLines> mModelPomList = mPDMManagerView.GetModelPomTemplateLines(((UserView)Session["UserLogin"]).CompanyConnect, pPOMCode);
				int mMaxId = 1;
				if (mModel.ModelPomList.Count > 0)
				{
					mMaxId = mModel.ModelPomList.Max(c => c.LineId) + 1;
				}
				foreach (ARGNSPomTemplateLines mPomTemplateLine in mModelPomList)
				{
					mPomTemplateLine.LineId = mMaxId;
					mPomTemplateLine.Code = mModel.Code;
					mPomTemplateLine.U_SclPom = pSclCode;
					mMaxId++;
				}

				mModel.ModelPomList.AddRange(mapper.Map<List<ARGNSModelPom>>(mModelPomList));

				TempData[pPageKey + "ModelPomList"] = mModel.ModelPomList;
				TempData[pPageKey + "Model"] = mModel;

				return PartialView("_PDMListPom", mapper.Map<List<ARGNSModelPomView>>(mModel.ModelPomList));
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpPost]
		public PartialViewResult ListCostSheet(string txtCodeModel = "", string Code = "")
		{
			List<ARGNSCostSheet> ListCostSheet = mCostSheetManagerView.GetListCostSheet(((UserView)Session["UserLogin"]).CompanyConnect, txtCodeModel);

			return PartialView("_PDMListCostSheet", Tuple.Create(ListCostSheet, Code));
		}


		[HttpPost]
		public PartialViewResult ListSamples(string txtCodeModel = "")
		{
			//List<ARGNSModelComment> ListMComm = mCommentManagerView.GetListComments(((UserView)Session["UserLogin"]).CompanyConnect, txtCodeModel);

			List<ARGNSModelSample> mListReturn = mSampleManagerView.GetListSample(((UserView)Session["UserLogin"]).CompanyConnect, txtCodeModel);

			return PartialView("_PDMListSamples", mListReturn);
		}

		[HttpPost]
		public PartialViewResult ListMD(string txtCodeModel = "")
		{
			MaterialDetailView mReturn = mMaterialDetailManagerView.GetMaterialDetails(((UserView)Session["UserLogin"]).CompanyConnect, txtCodeModel, ((UserView)Session["UserLogin"]).IdUser);
			return PartialView("_PDMListMD", mReturn);
		}



		public ActionResult ListModelLog(string pModelCode = "")
		{
			ViewBag.Code = pModelCode;
			List<ARGNSModelHistoryView> mListReturn = mPDMManagerView.GetModelHistoryList(((UserView)Session["UserLogin"]).CompanyConnect, pModelCode);
			return View("_PDMChangeLog", mListReturn);
		}

		[HttpPost]
		public PartialViewResult LogDifferenceList(string pModelCode = "", string pSelected = "")
		{
			List<SAPLogsView> mListReturn = mPDMManagerView.GetSAPModelLogs(((UserView)Session["UserLogin"]).CompanyConnect, pModelCode, pSelected);
			return PartialView("_PDMListModelLog", mListReturn);
		}

		[HttpPost]
		public PartialViewResult _GetWarehouseListSearch(string pWhsCode = "", string pWhsName = "")
		{
			try
			{
				List<WarehouseView> WhList = mUserManagerView.GetWarehouseListSearch(((UserView)Session["UserLogin"]).CompanyConnect, pWhsCode, pWhsName);

				return PartialView("_WhList", WhList);
			}
			catch (Exception ex)
			{
				return null;
			}
		}
		[HttpPost]
		public PartialViewResult _GetSecondaryWarehouseListSearch(string pWhsCode = "", string pWhsName = "")
		{
			try
			{
				List<WarehouseView> WhList = mUserManagerView.GetWarehouseListSearch(((UserView)Session["UserLogin"]).CompanyConnect, pWhsCode, pWhsName);

				return PartialView("_SecondaryWhList", WhList);
			}
			catch (Exception ex)
			{
				return null;
			}
		}



		[HttpPost]
		public PartialViewResult ColorList(string pPageKey, string txtColorCode = "", string txtColorName = "")
		{
			ColorManagerView mColorMan = new ColorManagerView();
			List<ARGNSModelColor> ListaColorTemp = ((List<ARGNSModelColor>)TempData[pPageKey + "ModelColorList"]).ToList();
			TempData[pPageKey + "ModelColorList"] = ListaColorTemp;

			List<ColorView> ColorList = mColorMan.GetColorListSearch(((UserView)Session["UserLogin"]).CompanyConnect, txtColorCode, txtColorName);
			ColorList = ColorList.Where(c => !ListaColorTemp.Select(j => j.U_ColCode).Contains(c.U_ColCode)).ToList();

			return PartialView("_ColorList", ColorList);
		}


		[HttpPost]
		public PartialViewResult AddColor(FormCollection pData)
		{

			JavaScriptSerializer serializer = new JavaScriptSerializer();
			List<ARGNSModelColor> mColorAddList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelColor>>(pData["ARGNSModelColor"]);
			string mPageKey = serializer.Deserialize<string>(pData["pPageKey"]);

			List<ARGNSModelColor> ListaColorTemp = ((List<ARGNSModelColor>)TempData[mPageKey + "ModelColorList"]).ToList();

			foreach (ARGNSModelColor item in mColorAddList)
			{
				item.LineId = (ListaColorTemp.Count == 0) ? 1 : ListaColorTemp.Max(c => c.LineId) + 1;
				item.U_Active = "Y";
				ListaColorTemp.Add(item);
			}

			TempData[mPageKey + "ModelColorList"] = ListaColorTemp;
			return PartialView("_ModelColorList", ListaColorTemp);
		}

		[HttpPost]
		public PartialViewResult ScaleList(string txtScaleCode = "", string txtScaleName = "")
		{
			ScaleMasterManagerView mMamScaleMan = new ScaleMasterManagerView();
			List<ScaleView> ScaleList = mMamScaleMan.GetColorListSearch(((UserView)Session["UserLogin"]).CompanyConnect, txtScaleCode, txtScaleName);
			return PartialView("_ScaleList", ScaleList);
		}


		[HttpPost]
		public PartialViewResult AddScale(FormCollection pData)
		{
			ScaleMasterManagerView mMamScaleMan = new ScaleMasterManagerView();
			ARGNSModelSize mSize;
			JavaScriptSerializer serializer = new JavaScriptSerializer();
			string mPageKey = serializer.Deserialize<string>(pData["pPageKey"]);
			string[] mScaleList = serializer.Deserialize<string[]>(pData["ARGNSModelSize"]);
			string[] mScaleCode = serializer.Deserialize<string[]>(pData["pScaleCode"]);

			List<ARGNSSize> SizeList;
			List<ARGNSModelSize> SizeListTEMP = new List<ARGNSModelSize>();

			for (int i = 0; i < mScaleList.Length; i++)
			{
				SizeList = mMamScaleMan.GetScaleDescriptionListSearch(((UserView)Session["UserLogin"]).CompanyConnect, mScaleList[i]);

				foreach (ARGNSSize item in SizeList)
				{
					mSize = new ARGNSModelSize();
					mSize.LineId = (SizeListTEMP.Count == 0) ? 1 : SizeListTEMP.Max(c => c.LineId) + 1;
					mSize.U_SizeCod = item.U_SizeCode;
					mSize.U_VOrder = item.U_VOrder;
					mSize.U_Selected = "Y";
					mSize.U_SclCode = mScaleCode[i];

					SizeListTEMP.Add(mSize);
				}

			}

			TempData[mPageKey + "ModelSizeList"] = SizeListTEMP;
			return PartialView("_ModelSizesList", SizeListTEMP);
		}

		[HttpPost]
		public PartialViewResult VariableList(string txtVarCode = "", string txtVarName = "")
		{
			VariableManagerView mVariableMan = new VariableManagerView();
			List<ARGNSSegmentation> SegmentationList = new List<ARGNSSegmentation>();

			List<VariableView> VariableList = mVariableMan.GetVariableListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ref SegmentationList, txtVarCode, txtVarName, "");
			return PartialView("_VarList", VariableList);
		}

		[HttpPost]
		public PartialViewResult AddVariable(FormCollection pData)
		{

			JavaScriptSerializer serializer = new JavaScriptSerializer();
			List<ARGNSModelVar> mAddAddList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelVar>>(pData["ARGNSModelVar"]);
			string mPageKey = serializer.Deserialize<string>(pData["pPageKey"]);

			List<ARGNSModelVar> ListaVarTemp = ((List<ARGNSModelVar>)TempData[mPageKey + "ModelVariableList"]).ToList();

			foreach (ARGNSModelVar item in mAddAddList)
			{
				item.LineId = (ListaVarTemp.Count == 0) ? 1 : ListaVarTemp.Max(c => c.LineId) + 1;
				item.U_Selected = "Y";
				ListaVarTemp.Add(item);
			}

			TempData[mPageKey + "ModelVariableList"] = ListaVarTemp;
			return PartialView("_ModelVarList", ListaVarTemp);
		}


		[HttpPost]
		public JsonResult GetSegmentgation(string pCode, string pPageKey)
		{
			try
			{
				ARGNSSegmentationView mSegmentationTMP = mPDMManagerView.GetSegmentationById(((UserView)Session["UserLogin"]).CompanyConnect, pCode);
				TempData[pPageKey + "ModelSegmentation"] = mSegmentationTMP;

				return Json(mSegmentationTMP, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(@Error.WebServError);
			}
		}


		[HttpPost]
		public JsonResult _GetEmployeeList()
		{
			try
			{

				List<ListItem> EmplList = mUserManagerView.GetEmployeeList(((UserView)Session["UserLogin"]).CompanyConnect);

				return Json(EmplList, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(@Error.WebServError);
			}
		}

	}


}