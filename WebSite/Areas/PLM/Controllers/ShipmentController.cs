﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class ShipmentController : Controller
    {
        private ShipmentManagerView mShipmentManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;

        public ShipmentController()
        {
            mShipmentManagerView = new ShipmentManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
        }

        // GET: PLM/Shipment
        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.Shipment, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                ShipmentMembersView mShipmentMembersView = mShipmentManagerView.GetShipmentMembersObject(((UserView)Session["UserLogin"]).CompanyConnect);

                return View(mShipmentMembersView);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.MyShipment, Enums.Actions.List)]
        public ActionResult MyShipmentIndex()
        {
            try
            {
                ShipmentMembersView mShipmentMembersView = mShipmentManagerView.GetShipmentMembersObject(((UserView)Session["UserLogin"]).CompanyConnect);

                return View(mShipmentMembersView);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult _GetShipmentList(string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            List<ARGNSContainerView> ListShipment = mShipmentManagerView.GetShipmentListSearch(((UserView)Session["UserLogin"]).CompanyConnect, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA);
            ShipmentMembersView mShipmentMembersView = mShipmentManagerView.GetShipmentMembersObject(((UserView)Session["UserLogin"]).CompanyConnect);

            return PartialView("_ShipmentList", Tuple.Create(ListShipment, mShipmentMembersView));
        }

        [HttpPost]
        public PartialViewResult _GetMyShipmentListBP(string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            List<ARGNSContainerView> ListShipment = mShipmentManagerView.GetMyShipmentListSearchBP(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).BPCode, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA);
            ShipmentMembersView mShipmentMembersView = mShipmentManagerView.GetShipmentMembersObject(((UserView)Session["UserLogin"]).CompanyConnect);

            return PartialView("_ShipmentList", Tuple.Create(ListShipment, mShipmentMembersView));
        }

        public ActionResult _Shipment(string ActionShipment, int pShipmentID, string pFromController)
        {
            string mPageKey = PageKeyGenerator.GetPageKey();

            ViewBag.PageKey = mPageKey;
            ViewBag.FormMode = ActionShipment;

            ARGNSContainerView mContainer = mShipmentManagerView.GetShipmentByCode(((UserView)Session["UserLogin"]).CompanyConnect, pShipmentID);
            List<ARGNSContainerPackageView> mContainerPackageList = mShipmentManagerView.GetContainerPackageList(((UserView)Session["UserLogin"]).CompanyConnect, pShipmentID);
            switch (pFromController)
            {
                case "Shipment":
                    ViewBag.URLRedirect = "/PLM/Shipment/Index";
                    break;
                case "MyShipment":
                    ViewBag.URLRedirect = "/PLM/Shipment/MyShipmentIndex";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            switch (ActionShipment)
            {
                case "View":
                    foreach (ARGNSContainerLineView mContainerLine in mContainer.Lines)
                    {
                        mContainerLine.InContainerQty = 0;
                        foreach (ARGNSContainerPackageView mContainerPackage in mContainerPackageList)
                        {
                            ARGNSContainerPackageLineView mContainerPackageLineAux = mContainerPackage.Lines.Where(c => c.U_BaseEntry == mContainerLine.U_BaseEntry && c.U_BaseLine == mContainerLine.U_BaseLine.ToString()).FirstOrDefault();
                            if (mContainerPackageLineAux != null)
                            {
                                mContainerLine.InContainerQty += mContainerPackageLineAux.U_Quantity;
                            }
                        }
                    }
                    break;
                case "Update":
                    foreach (ARGNSContainerLineView mContainerLine in mContainer.Lines)
                    {
                        mContainerLine.InContainerQty = 0;
                        foreach (ARGNSContainerPackageView mContainerPackage in mContainerPackageList)
                        {
                            ARGNSContainerPackageLineView mContainerPackageLineAux = mContainerPackage.Lines.Where(c => c.U_BaseEntry == mContainerLine.U_BaseEntry && c.U_BaseLine == mContainerLine.U_BaseLine.ToString()).FirstOrDefault();
                            if (mContainerPackageLineAux != null)
                            {
                                mContainerLine.InContainerQty += mContainerPackageLineAux.U_Quantity;
                            }
                        }
                    }
                    break;
                case "Add":
                    break;
            }

            TempData[mPageKey + "ContainerView"] = mContainer;
            TempData[mPageKey + "ContainerLinesView"] = mContainer.Lines;

            return View("_Shipment", Tuple.Create(mContainer, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.Shipment, ((UserView)Session["UserLogin"]).CompanyConnect)));
        }

        [HttpPost]
        public PartialViewResult _BusinessPartner(string pVendorCode = "", string pVendorName = "")
        {
            List<BusinessPartnerView> ListReturn;
            pVendorCode = pVendorCode.TrimStart().TrimEnd();
            pVendorName = pVendorName.TrimStart().TrimEnd();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor).Where(c => (c.CardCode.ToUpper().Contains(pVendorCode.ToUpper())) && ((c.CardName == null ? false : c.CardName.ToUpper().Contains(pVendorName.ToUpper())))).ToList();
            }
            else
            {
                ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor).Where(c => c.CardCode == ((UserView)Session["UserLogin"]).BPCode).ToList();
            }
            return PartialView("_BusinessPartner", ListReturn);
        }

        [HttpPost]
        public JsonResult Update(ARGNSContainerView pShipmentView)
        {
            pShipmentView.Lines = ((List<ARGNSContainerLineView>)TempData[pShipmentView.PageKey + "ContainerLinesView"]);
            TempData[pShipmentView.PageKey + "ContainerLinesView"] = pShipmentView.Lines;
            pShipmentView.ListPackage = ((List<ARGNSContainerPackageView>)TempData[pShipmentView.PageKey + "ContainerPackageView"]);
            TempData[pShipmentView.PageKey + "ContainerPackageView"] = pShipmentView.ListPackage;
            pShipmentView.ListPackaginSlip = ((List<ARGNSPSlipPackageView>)TempData[pShipmentView.PageKey + "PSlipPackageView"]);
            TempData[pShipmentView.PageKey + "PSlipPackageView"] = pShipmentView.ListPackaginSlip;

            return Json(mShipmentManagerView.Update(((UserView)Session["UserLogin"]).CompanyConnect, pShipmentView));
        }

        [HttpPost]
        public JsonResult Add(ARGNSContainerView pShipmentView)
        {
            pShipmentView.Lines = ((List<ARGNSContainerLineView>)TempData[pShipmentView.PageKey + "ContainerLinesView"]);
            TempData[pShipmentView.PageKey + "ContainerLinesView"] = pShipmentView.Lines;
            pShipmentView.ListPackage = ((List<ARGNSContainerPackageView>)TempData[pShipmentView.PageKey + "ContainerPackageView"]);
            TempData[pShipmentView.PageKey + "ContainerPackageView"] = pShipmentView.ListPackage;
            pShipmentView.ListPackaginSlip = ((List<ARGNSPSlipPackageView>)TempData[pShipmentView.PageKey + "PSlipPackageView"]);
            TempData[pShipmentView.PageKey + "PSlipPackageView"] = pShipmentView.ListPackaginSlip;

            return Json(mShipmentManagerView.Add(((UserView)Session["UserLogin"]).CompanyConnect, pShipmentView));

        }

        #region Container Package

        [HttpPost]
        public PartialViewResult _ContainerPackage(int pShipmentID)
        {
            List<ARGNSContainerPackageView> mContainerPackage = mShipmentManagerView.GetContainerPackageList(((UserView)Session["UserLogin"]).CompanyConnect, pShipmentID);
            return PartialView("_ContainerPackageTable", mContainerPackage);
        }

        [HttpPost]
        public PartialViewResult _ContainerModalPackage(int pShipmentID, string pPageKey = "")
        {
            List<ARGNSContainerPackageView> mContainerPackage = mShipmentManagerView.GetContainerPackageList(((UserView)Session["UserLogin"]).CompanyConnect, pShipmentID);
            List<ARGNSContainerSetupView> mContainerSetupList = mShipmentManagerView.GetContainerSetupList(((UserView)Session["UserLogin"]).CompanyConnect);
            TempData[pPageKey + "ContainerPackageView"] = mContainerPackage;

            return PartialView("_ContainerModalPackageTable", Tuple.Create(mContainerPackage, mContainerSetupList));
        }

        [HttpPost]
        public PartialViewResult _ContainerModalAvailableItems(int pShipmentID, string pPageKey = "")
        {
            List<ARGNSContainerPackageView> mContainerPackageList = ((List<ARGNSContainerPackageView>)TempData[pPageKey + "ContainerPackageView"]).ToList();
            ARGNSContainerView mContainerView = ((ARGNSContainerView)TempData[pPageKey + "ContainerView"]);

            List<ARGNSAvailableItemView> mAvailableItemListView = new List<ARGNSAvailableItemView>();
            int mAvailableItemCounter = 0;
            foreach (ARGNSContainerLineView mContainerLine in mContainerView.Lines)
            {
                ARGNSAvailableItemView mAvailableItemAux = new ARGNSAvailableItemView();
                mAvailableItemAux.U_ItemCode = mContainerLine.U_ItemCode;
                mAvailableItemAux.U_ItemName = mContainerLine.U_ItemName;
                mAvailableItemAux.U_BaseEntry = mContainerLine.U_BaseEntry;
                mAvailableItemAux.U_BaseDocNo = mContainerLine.U_BaseDocNo;
                mAvailableItemAux.U_BaseLine = mContainerLine.U_BaseLine;
                mAvailableItemAux.Quantity = mContainerLine.DraftLine.Quantity;
                mAvailableItemAux.LineId = mAvailableItemCounter;
                foreach (ARGNSContainerPackageView mContainerPackage in mContainerPackageList)
                {
                    ARGNSContainerPackageLineView mContainerPackageLineAux = mContainerPackage.Lines.Where(c => c.U_BaseEntry == mContainerLine.U_BaseEntry && c.U_BaseLine == mContainerLine.U_BaseLine.ToString()).FirstOrDefault();
                    if (mContainerPackageLineAux != null)
                    {
                        mAvailableItemAux.Quantity -= mContainerPackageLineAux.U_Quantity;
                    }
                }
                if (mAvailableItemAux.Quantity > 0)
                    mAvailableItemListView.Add(mAvailableItemAux);

                mAvailableItemCounter++;
            }

            TempData[pPageKey + "AvailableItem"] = mAvailableItemListView;
            TempData[pPageKey + "ContainerPackageView"] = mContainerPackageList;
            TempData[pPageKey + "ContainerView"] = mContainerView;
            return PartialView("_ContainerModalAvailableItemsTable", mAvailableItemListView);
        }

        [HttpPost]
        public PartialViewResult _ContainerModalPackageItems(int pContainer, string pPageKey = "")
        {
            List<ARGNSContainerPackageView> mContainerPackageList = ((List<ARGNSContainerPackageView>)TempData[pPageKey + "ContainerPackageView"]);
            TempData[pPageKey + "ContainerPackageView"] = mContainerPackageList;

            return PartialView("_ContainerModalPackageItemsTable", mContainerPackageList.Where(c => c.U_CntnerNum == pContainer).FirstOrDefault());
        }

        [HttpPost]
        public PartialViewResult _AddItemToPackage(ItemToPackageView pItemToPackageView)
        {
            List<ARGNSContainerPackageView> mContainerPackageList = ((List<ARGNSContainerPackageView>)TempData[pItemToPackageView.pPageKey + "ContainerPackageView"]);
            ARGNSContainerPackageView mContainerPackage = mContainerPackageList.Where(c => c.U_CntnerNum == pItemToPackageView.PackageNum).FirstOrDefault();
            List<ARGNSAvailableItemView> mAvailableItemListTemp = ((List<ARGNSAvailableItemView>)TempData[pItemToPackageView.pPageKey + "AvailableItem"]);
            foreach (ARGNSAvailableItemView mAvailableItem in pItemToPackageView.ItemsToAdd)
            {
                ARGNSAvailableItemView mAvailableItemAux = mAvailableItemListTemp.Where(c => c.LineId == mAvailableItem.LineId).FirstOrDefault();
                if (mContainerPackage.Lines.Where(c => c.U_ItemCode == mAvailableItemAux.U_ItemCode).FirstOrDefault() != null)
                {
                    mContainerPackage.Lines.Where(c => c.U_ItemCode == mAvailableItemAux.U_ItemCode).FirstOrDefault().U_Quantity += mAvailableItem.Quantity;
                    mAvailableItemAux.Quantity -= mAvailableItem.Quantity;
                }
                else
                {
                    ARGNSContainerPackageLineView mAuxPackageLine = new ARGNSContainerPackageLineView();
                    if (mContainerPackage.Lines.Count > 0)
                        mAuxPackageLine.LineId = mContainerPackage.Lines.Max(c => c.LineId) + 1;
                    else
                        mAuxPackageLine.LineId = 1;
                    mAuxPackageLine.IsNew = true;
                    mAuxPackageLine.U_ItemCode = mAvailableItemAux.U_ItemCode;
                    mAuxPackageLine.U_BaseEntry = mAvailableItemAux.U_BaseEntry;
                    mAuxPackageLine.U_BaseLine = mAvailableItemAux.U_BaseLine.ToString();
                    mAuxPackageLine.U_BaseRef = mAvailableItemAux.U_BaseDocNo;
                    mAuxPackageLine.U_Quantity = mAvailableItem.Quantity;
                    mAuxPackageLine.U_ItemName = mAvailableItemAux.U_ItemName;
                    mAuxPackageLine.U_CntnerNum = (short)pItemToPackageView.PackageNum;
                    mContainerPackage.Lines.Add(mAuxPackageLine);
                    mAvailableItemListTemp.Where(c => c.LineId == mAvailableItem.LineId).FirstOrDefault().Quantity -= mAvailableItem.Quantity;
                }
            }

            TempData[pItemToPackageView.pPageKey + "ContainerPackageView"] = mContainerPackageList;
            mAvailableItemListTemp = mAvailableItemListTemp.Where(c => c.Quantity > 0).ToList();
            TempData[pItemToPackageView.pPageKey + "AvailableItem"] = mAvailableItemListTemp;

            return PartialView("_ContainerModalPackageItemsTable", mContainerPackageList.Where(c => c.U_CntnerNum == pItemToPackageView.PackageNum).FirstOrDefault());
        }

        [HttpPost]
        public PartialViewResult _RemoveItemToPackage(ItemToPackageView pItemToPackageView)
        {
            List<ARGNSContainerPackageView> mContainerPackageList = ((List<ARGNSContainerPackageView>)TempData[pItemToPackageView.pPageKey + "ContainerPackageView"]);
            ARGNSContainerPackageView mContainerPackage = mContainerPackageList.Where(c => c.U_CntnerNum == pItemToPackageView.PackageNum).FirstOrDefault();
            List<ARGNSAvailableItemView> mAvailableItemListTemp = ((List<ARGNSAvailableItemView>)TempData[pItemToPackageView.pPageKey + "AvailableItem"]);
            foreach (ARGNSAvailableItemView mAvailableItem in pItemToPackageView.ItemsToAdd)
            {
                ARGNSContainerPackageLineView mContainerPackageLineAux = mContainerPackage.Lines.Where(c => c.LineId == mAvailableItem.LineId).FirstOrDefault();
                if (mAvailableItemListTemp.Where(c => c.U_BaseEntry == mContainerPackageLineAux.U_BaseEntry && c.U_BaseLine.ToString() == mContainerPackageLineAux.U_BaseLine).FirstOrDefault() != null)
                {
                    mAvailableItemListTemp.Where(c => c.U_BaseEntry == mContainerPackageLineAux.U_BaseEntry && c.U_BaseLine.ToString() == mContainerPackageLineAux.U_BaseLine).FirstOrDefault().Quantity += mContainerPackageLineAux.U_Quantity;
                }
                else
                {
                    ARGNSAvailableItemView mAuxAvailableItem = new ARGNSAvailableItemView();
                    if (mAvailableItemListTemp.Count > 0)
                        mAuxAvailableItem.LineId = mAvailableItemListTemp.Max(c => c.LineId) + 1;
                    else
                        mAuxAvailableItem.LineId = 1;
                    mAuxAvailableItem.U_ItemCode = mContainerPackageLineAux.U_ItemCode;
                    mAuxAvailableItem.U_ItemName = mContainerPackageLineAux.U_ItemName;
                    mAuxAvailableItem.U_BaseEntry = mContainerPackageLineAux.U_BaseEntry;
                    mAuxAvailableItem.U_BaseLine = Convert.ToInt16(mContainerPackageLineAux.U_BaseLine);
                    mAuxAvailableItem.U_BaseDocNo = mContainerPackageLineAux.U_BaseRef;
                    mAuxAvailableItem.Quantity = mContainerPackageLineAux.U_Quantity;
                    mAvailableItemListTemp.Add(mAuxAvailableItem);
                }

                mContainerPackage.Lines.Remove(mContainerPackageLineAux);
            }

            TempData[pItemToPackageView.pPageKey + "ContainerPackageView"] = mContainerPackageList;
            mAvailableItemListTemp = mAvailableItemListTemp.Where(c => c.Quantity > 0).ToList();
            TempData[pItemToPackageView.pPageKey + "AvailableItem"] = mAvailableItemListTemp;

            return PartialView("_ContainerModalPackageItemsTable", mContainerPackageList.Where(c => c.U_CntnerNum == pItemToPackageView.PackageNum).FirstOrDefault());
        }

        [HttpPost]
        public PartialViewResult _UpdateAvailableItem(string pPageKey)
        {
            List<ARGNSAvailableItemView> mAvailableItemListTemp = ((List<ARGNSAvailableItemView>)TempData[pPageKey + "AvailableItem"]);
            TempData[pPageKey + "AvailableItem"] = mAvailableItemListTemp;

            return PartialView("_ContainerModalAvailableItemsTable", mAvailableItemListTemp);
        }

        [HttpPost]
        public PartialViewResult _AddNewContainer(string pPageKey = "")
        {
            List<ARGNSContainerSetupView> mContainerSetupList = mShipmentManagerView.GetContainerSetupList(((UserView)Session["UserLogin"]).CompanyConnect);
            List<ARGNSContainerPackageView> mContainerPackageList = ((List<ARGNSContainerPackageView>)TempData[pPageKey + "ContainerPackageView"]).ToList();
            ARGNSContainerPackageView mAuxContainer = new ARGNSContainerPackageView();
            if (mContainerPackageList.Count > 0)
            {
                mAuxContainer.LineId = mContainerPackageList.Max(c => c.LineId) + 1;
                mAuxContainer.U_CntnerNum = (short?)(mContainerPackageList.Max(c => c.U_CntnerNum) + 1);
            }
            else
            { 
                mAuxContainer.LineId = 1;
                mAuxContainer.U_CntnerNum = 1;
            }
            mAuxContainer.IsNew = true;
            mAuxContainer.U_CntnerTyp = mContainerSetupList.FirstOrDefault().Code;
            mContainerPackageList.Add(mAuxContainer);
            TempData[pPageKey + "ContainerPackageView"] = mContainerPackageList;

            return PartialView("_ContainerModalPackageTable", Tuple.Create(mContainerPackageList, mContainerSetupList));
        }

        #endregion

        #region Packagin Slip

        [HttpPost]
        public PartialViewResult _PSlipModalPackage(int pContainerID, int pShipmentID, string pPageKey = "")
        {
            List<ARGNSPSlipPackageView> mPSlipPackage = mShipmentManagerView.GetPSlipPackageList(((UserView)Session["UserLogin"]).CompanyConnect, pContainerID, pShipmentID);
            List<ARGNSPSlipPackageTypeView> mPSlipPackageTypeList = mShipmentManagerView.GetPSlipPackageTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
            TempData[pPageKey + "PSlipPackageView"] = mPSlipPackage;

            return PartialView("_PSlipModalPackageTable", Tuple.Create(mPSlipPackage, mPSlipPackageTypeList, pContainerID));
        }

        [HttpPost]
        public PartialViewResult _PSlipModalAvailableItems(int pContainerID, string pPageKey = "")
        {
            List<ARGNSPSlipPackageView> mPSlipPackageList = ((List<ARGNSPSlipPackageView>)TempData[pPageKey + "PSlipPackageView"]).ToList();
            List<ARGNSContainerPackageView> mContainerPackageView = ((List<ARGNSContainerPackageView>)TempData[pPageKey + "ContainerPackageView"]);

            List<ARGNSContainerPackageLineView> mContainerPackageLineView = new List<ARGNSContainerPackageLineView>();
            int mContainerPackageLineCounter = 0;
            foreach (ARGNSContainerPackageLineView mContainerPackageLine in mContainerPackageView.Where(c => c.U_CntnerNum == pContainerID).FirstOrDefault().Lines)
            {
                ARGNSContainerPackageLineView mAvailableItemAux = new ARGNSContainerPackageLineView();
                mAvailableItemAux.U_ItemCode = mContainerPackageLine.U_ItemCode;
                mAvailableItemAux.U_BaseEntry = mContainerPackageLine.U_BaseEntry;
                mAvailableItemAux.U_BaseRef = mContainerPackageLine.U_BaseRef;
                mAvailableItemAux.U_BaseLine = mContainerPackageLine.U_BaseLine;
                mAvailableItemAux.U_Quantity = mContainerPackageLine.U_Quantity;
                mAvailableItemAux.LineId = mContainerPackageLineCounter;

                foreach (ARGNSPSlipPackageView mPSlipPackage in mPSlipPackageList)
                {
                    ARGNSPSlipPackageLineView mPSlipPackageLineAux = mPSlipPackage.Lines.Where(c => c.U_BaseEntry == mContainerPackageLine.U_BaseEntry && c.U_BaseLine == mContainerPackageLine.U_BaseLine.ToString()).FirstOrDefault();
                    if (mPSlipPackageLineAux != null)
                    {
                        mAvailableItemAux.U_Quantity -= mPSlipPackageLineAux.U_Quantity;
                    }
                }
                if (mAvailableItemAux.U_Quantity > 0)
                    mContainerPackageLineView.Add(mAvailableItemAux);

                mContainerPackageLineCounter++;
            }

            TempData[pPageKey + "AvailablePSlipItem"] = mContainerPackageLineView;
            TempData[pPageKey + "ContainerPackageView"] = mContainerPackageView;
            TempData[pPageKey + "PSlipPackageView"] = mPSlipPackageList;
            return PartialView("_PSlipModalAvailableItemsTable", mContainerPackageLineView);
        }

        [HttpPost]
        public PartialViewResult _PSlipModalPackageItems(int pPackageNum, int pContainer, string pPageKey = "")
        {
            List<ARGNSPSlipPackageView> mPSLipPackageList = ((List<ARGNSPSlipPackageView>)TempData[pPageKey + "PSlipPackageView"]);
            TempData[pPageKey + "PSlipPackageView"] = mPSLipPackageList;

            return PartialView("_PSlipModalPackageItemsTable", mPSLipPackageList.Where(c => c.U_CntnerNum == pContainer && c.U_PackageNum == pPackageNum).FirstOrDefault());
        }

        [HttpPost]
        public PartialViewResult _AddItemToPSlip(ItemToPackageView pItemToPSlipView)
        {
            List<ARGNSPSlipPackageView> mPSlipPackageList = ((List<ARGNSPSlipPackageView>)TempData[pItemToPSlipView.pPageKey + "PSlipPackageView"]);

            ARGNSPSlipPackageView mPSlipPackage = mPSlipPackageList.Where(c => c.U_CntnerNum == pItemToPSlipView.ContainerNum && c.U_PackageNum == pItemToPSlipView.PackageNum).FirstOrDefault();
            List<ARGNSContainerPackageLineView> mAvailableItemListTemp = ((List<ARGNSContainerPackageLineView>)TempData[pItemToPSlipView.pPageKey + "AvailablePSlipItem"]);
            foreach (ARGNSAvailableItemView mAvailableItem in pItemToPSlipView.ItemsToAdd)
            {
                ARGNSContainerPackageLineView mAvailableItemAux = mAvailableItemListTemp.Where(c => c.LineId == mAvailableItem.LineId).FirstOrDefault();
                if (mPSlipPackage.Lines.Where(c => c.U_ItemCode == mAvailableItemAux.U_ItemCode).FirstOrDefault() != null)
                {
                    mPSlipPackage.Lines.Where(c => c.U_ItemCode == mAvailableItemAux.U_ItemCode).FirstOrDefault().U_Quantity += mAvailableItem.Quantity;
                    mAvailableItemAux.U_Quantity -= mAvailableItem.Quantity;
                }
                else
                {
                    ARGNSPSlipPackageLineView mAuxPackageLine = new ARGNSPSlipPackageLineView();
                    if(mPSlipPackage.Lines.Count > 0)
                        mAuxPackageLine.LineId = mPSlipPackage.Lines.Max(c => c.LineId) + 1;
                    else
                        mAuxPackageLine.LineId = 1;
                    mAuxPackageLine.IsNew = true;
                    mAuxPackageLine.U_ItemCode = mAvailableItemAux.U_ItemCode;
                    mAuxPackageLine.U_BaseEntry = mAvailableItemAux.U_BaseEntry;
                    mAuxPackageLine.U_BaseLine = mAvailableItemAux.U_BaseLine.ToString();
                    mAuxPackageLine.U_BaseRef = mAvailableItemAux.U_BaseRef;
                    mAuxPackageLine.U_Quantity = mAvailableItem.Quantity;
                    mAuxPackageLine.U_CntnerNum = (short)pItemToPSlipView.ContainerNum;
                    mAuxPackageLine.U_PackageNum = (short)pItemToPSlipView.PackageNum;
                    mPSlipPackage.Lines.Add(mAuxPackageLine);
                    mAvailableItemListTemp.Where(c => c.LineId == mAvailableItem.LineId).FirstOrDefault().U_Quantity -= mAvailableItem.Quantity;
                }
            }

            TempData[pItemToPSlipView.pPageKey + "PSlipPackageView"] = mPSlipPackageList;
            mAvailableItemListTemp = mAvailableItemListTemp.Where(c => c.U_Quantity > 0).ToList();
            TempData[pItemToPSlipView.pPageKey + "AvailablePSlipItem"] = mAvailableItemListTemp;

            return PartialView("_PSlipModalPackageItemsTable", mPSlipPackageList.Where(c => c.U_CntnerNum == pItemToPSlipView.ContainerNum && c.U_PackageNum == pItemToPSlipView.PackageNum).FirstOrDefault());
        }

        [HttpPost]
        public PartialViewResult _UpdatePSlipAvailableItem(string pPageKey)
        {
            List<ARGNSContainerPackageLineView> mAvailableItemListTemp = ((List<ARGNSContainerPackageLineView>)TempData[pPageKey + "AvailablePSlipItem"]);
            TempData[pPageKey + "AvailablePSlipItem"] = mAvailableItemListTemp;

            return PartialView("_PSlipModalAvailableItemsTable", mAvailableItemListTemp);
        }

        [HttpPost]
        public PartialViewResult _AddNewPackageSlip(short pContainerID, string pPageKey = "")
        {
            List<ARGNSPSlipPackageTypeView> mPSlipPackageTypeList = mShipmentManagerView.GetPSlipPackageTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
            List<ARGNSPSlipPackageView> mPSlipPackageList = ((List<ARGNSPSlipPackageView>)TempData[pPageKey + "PSlipPackageView"]).ToList();

            ARGNSPSlipPackageView mAuxPackageSlip = new ARGNSPSlipPackageView();
            if (mPSlipPackageList.Count > 0)
            { 
                mAuxPackageSlip.LineId = mPSlipPackageList.Max(c => c.LineId) + 1;
                mAuxPackageSlip.U_PackageNum = (short?)(mPSlipPackageList.Max(c => c.U_PackageNum) + 1);
            }
            else
            { 
                mAuxPackageSlip.LineId = 1;
                mAuxPackageSlip.U_PackageNum = 1;
            }
            mAuxPackageSlip.U_CntnerNum = pContainerID;
            mAuxPackageSlip.IsNew = true;
            mAuxPackageSlip.U_PackageTyp = mPSlipPackageTypeList.FirstOrDefault().PkgType;
            mPSlipPackageList.Add(mAuxPackageSlip);
            TempData[pPageKey + "PSlipPackageView"] = mPSlipPackageList;

            return PartialView("_PSlipModalPackageTable", Tuple.Create(mPSlipPackageList, mPSlipPackageTypeList, ((int)pContainerID)));
        }

        [HttpPost]
        public PartialViewResult _GetDocumentWhitOpenLines(string pObjType, string pPageKey = "")
        {
            List<DraftView> mDraftList = mShipmentManagerView.GetDocumentWhitOpenLines(((UserView)Session["UserLogin"]).CompanyConnect, pObjType);

            return PartialView("_DocumentTable", mDraftList);
        }

        [HttpPost]
        public PartialViewResult _GetDocumentOpenLines(string pObjType, List<int> pDocEntryList, string pPageKey = "")
        {
            List<DraftLineView> mDraftLinesList = mShipmentManagerView.GetDocumentOpenLines(((UserView)Session["UserLogin"]).CompanyConnect, pObjType, pDocEntryList);

            TempData[pPageKey + "DocumentLines"] = mDraftLinesList;

            return PartialView("_DocumentLinesTable", Tuple.Create(mDraftLinesList, pObjType));
        }

        [HttpPost]
        public PartialViewResult _AddDocumentLines(ARGNSContainerView pContainerView)
        {

            List<ARGNSContainerLineView> mContainerLinesList = ((List<ARGNSContainerLineView>)TempData[pContainerView.PageKey + "ContainerLinesView"]).ToList();
            List<DraftLineView> mDocumentLinesList = ((List<DraftLineView>)TempData[pContainerView.PageKey + "DocumentLines"]).ToList();

            foreach (ARGNSContainerLineView mLine in pContainerView.Lines)
            {
                ARGNSContainerLineView mLineAux = new ARGNSContainerLineView();
                DraftLineView mDraftLineAux = mDocumentLinesList.Where(c => c.DocEntry.ToString() == mLine.U_BaseEntry && c.LineNum == mLine.U_BaseLine).FirstOrDefault();
                if (mContainerLinesList.Count > 0)
                    mLineAux.LineId = mContainerLinesList.Max(c => c.LineId) + 1;
                else
                    mLineAux.LineId = 1;
                mLineAux.U_BaseEntry = mLine.U_BaseEntry;
                mLineAux.U_BaseLine = mLine.U_BaseLine;
                mLineAux.U_BaseDocNo = mLine.U_BaseEntry;
                mLineAux.U_CardCode = mDraftLineAux.CardCode;
                mLineAux.U_CardName = mDraftLineAux.CardName;
                mLineAux.U_ItemCode = mLine.U_ItemCode;
                mLineAux.U_ItemName = mDraftLineAux.ItemName;
                mLineAux.U_ARGNS_MOD = mDraftLineAux.U_ARGNS_MOD;
                mLineAux.U_ARGNS_COL = mDraftLineAux.U_ARGNS_COL;
                mLineAux.U_ARGNS_SIZE = mDraftLineAux.U_ARGNS_SIZE;
                mLineAux.U_ARGNS_SEASON = mDraftLineAux.U_ARGNS_SEASON;
                mLineAux.U_WhsCode = mDraftLineAux.WhsCode;
                mLineAux.InContainerQty = 0;
                mLineAux.U_ObjType = mLine.U_ObjType;
                mLineAux.U_LineStatus = mLine.U_LineStatus;
                mLineAux.DraftLine = new DraftLineView();
                mLineAux.DraftLine.Quantity = mLine.DraftLine.Quantity;
                mLineAux.DraftLine.DelivrdQty = mDraftLineAux.DelivrdQty;
                mLineAux.DraftLine.ShipToCode = mDraftLineAux.ShipToCode;
                mLineAux.IsNew = true;

                mContainerLinesList.Add(mLineAux);
            }

            TempData[pContainerView.PageKey + "ContainerLinesView"] = mContainerLinesList;
            ARGNSContainerView mContainerView = ((ARGNSContainerView)TempData[pContainerView.PageKey + "ContainerView"]);
            mContainerView.Lines = mContainerLinesList;
            TempData[pContainerView.PageKey + "ContainerView"] = mContainerView;

            return PartialView("_SPDTable", mContainerLinesList);
        }

        [HttpPost]
        public PartialViewResult _RemoveItemToPSlip(ItemToPackageView pItemToPSlipView)
        {
            List<ARGNSPSlipPackageView> mPSlipPackageList = ((List<ARGNSPSlipPackageView>)TempData[pItemToPSlipView.pPageKey + "PSlipPackageView"]);
            ARGNSPSlipPackageView mPSlipPackage = mPSlipPackageList.Where(c => c.U_CntnerNum == pItemToPSlipView.ContainerNum && c.U_PackageNum == pItemToPSlipView.PackageNum).FirstOrDefault();

            List<ARGNSContainerPackageLineView> mAvailableItemListTemp = ((List<ARGNSContainerPackageLineView>)TempData[pItemToPSlipView.pPageKey + "AvailablePSlipItem"]);
            foreach (ARGNSAvailableItemView mAvailableItem in pItemToPSlipView.ItemsToAdd)
            {
                ARGNSPSlipPackageLineView mPSlipPackageLineAux = mPSlipPackage.Lines.Where(c => c.LineId == mAvailableItem.LineId).FirstOrDefault();
                if (mAvailableItemListTemp.Where(c => c.U_BaseEntry == mPSlipPackageLineAux.U_BaseEntry && c.U_BaseLine.ToString() == mPSlipPackageLineAux.U_BaseLine).FirstOrDefault() != null)
                {
                    mAvailableItemListTemp.Where(c => c.U_BaseEntry == mPSlipPackageLineAux.U_BaseEntry && c.U_BaseLine.ToString() == mPSlipPackageLineAux.U_BaseLine).FirstOrDefault().U_Quantity += mPSlipPackageLineAux.U_Quantity;
                }
                else
                {
                    ARGNSContainerPackageLineView mAuxAvailableItem = new ARGNSContainerPackageLineView();
                    if (mAvailableItemListTemp.Count > 0)
                        mAuxAvailableItem.LineId = mAvailableItemListTemp.Max(c => c.LineId) + 1;
                    else
                        mAuxAvailableItem.LineId = 1;
                    mAuxAvailableItem.U_ItemCode = mPSlipPackageLineAux.U_ItemCode;
                    mAuxAvailableItem.U_ItemName = mPSlipPackageLineAux.U_ItemName;
                    mAuxAvailableItem.U_BaseEntry = mPSlipPackageLineAux.U_BaseEntry;
                    mAuxAvailableItem.U_BaseLine = mPSlipPackageLineAux.U_BaseLine;
                    mAuxAvailableItem.U_BaseRef = mPSlipPackageLineAux.U_BaseRef;
                    mAuxAvailableItem.U_Quantity = mPSlipPackageLineAux.U_Quantity;
                    mAvailableItemListTemp.Add(mAuxAvailableItem);
                }

                mPSlipPackage.Lines.Remove(mPSlipPackageLineAux);
            }

            TempData[pItemToPSlipView.pPageKey + "PSlipPackageView"] = mPSlipPackageList;
            mAvailableItemListTemp = mAvailableItemListTemp.Where(c => c.U_Quantity > 0).ToList();
            TempData[pItemToPSlipView.pPageKey + "AvailablePSlipItem"] = mAvailableItemListTemp;

            return PartialView("_PSlipModalPackageItemsTable", mPSlipPackageList.Where(c => c.U_CntnerNum == pItemToPSlipView.ContainerNum && c.U_PackageNum == pItemToPSlipView.PackageNum).FirstOrDefault());
        }

        [HttpPost]
        public PartialViewResult _GetShipmentDocument(string pShipmentID, string pPageKey)
        {
            try
            {
                List<DraftView> mShipmentDocumentList = ((List<DraftView>)TempData[pPageKey + "ShipmentDocument"]);

                if (mShipmentDocumentList == null)
                {
                    List<ARGNSContainerLineView> mShipmentLinesList = ((List<ARGNSContainerLineView>)TempData[pPageKey + "ContainerLinesView"]);
                    TempData[pPageKey + "ContainerLinesView"] = mShipmentLinesList;
                    if (mShipmentLinesList.Count > 0)
                    {
                        mShipmentDocumentList = mShipmentManagerView.GetShipmentDocument(((UserView)Session["UserLogin"]).CompanyConnect, pShipmentID, mShipmentLinesList.FirstOrDefault().U_ObjType);
                        TempData[pPageKey + "ShipmentDocument"] = mShipmentDocumentList;
                    }
                }

                return PartialView("_ShipmentDocumentTab", mShipmentDocumentList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}