﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.MaterialDetails;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.PLM.MaterialDetail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class MaterialDetailController : Controller
    {
        private UserManagerView mUserManagerView;
        private MaterialDetailManagerView mMaterialDetailManagerView;

        public MaterialDetailController()
        {
            mMaterialDetailManagerView = new MaterialDetailManagerView();
            mUserManagerView = new UserManagerView();
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.MaterialDetail)]
        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.MaterialDetail)]
        public ActionResult ActionMD(string ActionMD, string pModelCode)
        {
            MaterialDetailView mMaterialDetailView = null;
            string mPageKey = PageKeyGenerator.GetPageKey();

            switch (ActionMD)
            {
                case "View":
                    ViewBag.FormMode = ActionMD;
                    ViewBag.PageKey = mPageKey;
                    mMaterialDetailView = mMaterialDetailManagerView.GetMaterialDetails(((UserView)Session["UserLogin"]).CompanyConnect, pModelCode, ((UserView)Session["UserLogin"]).IdUser);
                    ViewBag.Tite = MaterialDetail.MD + " " + mMaterialDetailView.U_ModCode;
                    break;
                case "Update":
                    ViewBag.FormMode = ActionMD;
                    ViewBag.PageKey = mPageKey;
                    mMaterialDetailView = mMaterialDetailManagerView.GetMaterialDetails(((UserView)Session["UserLogin"]).CompanyConnect, pModelCode, ((UserView)Session["UserLogin"]).IdUser);
                    ViewBag.Tite = MaterialDetail.MD + " " + mMaterialDetailView.U_ModCode;
                    break;
                case "Add":
                    ViewBag.FormMode = ActionMD;
                    ViewBag.PageKey = mPageKey;
                    mMaterialDetailView = mMaterialDetailManagerView.GetMaterialDetails(((UserView)Session["UserLogin"]).CompanyConnect, pModelCode, ((UserView)Session["UserLogin"]).IdUser);
                    mMaterialDetailView.U_ModCode = pModelCode;
                    ViewBag.Tite = MaterialDetail.MD;
                    break;
            }

            return View("_MaterialDetail", mMaterialDetailView);
        }

        public ActionResult _GetRow(string pTable, int pNextId)
        {
            switch (pTable)
            {
                case "Fabric":
                    return PartialView("_RowFabric", new ARGNSMDFabric(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_FABRIC")));
                    break;
                case "Accessories":
                    return PartialView("_RowAccessories", new ARGNSMDAccess(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_ACCESS")));
                    break;
                case "CI":
                    return PartialView("_RowCareInstructions", new ARGNSMDCareInst(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_CARE_INST")));
                    break;
                case "Labelling":
                    return PartialView("_RowLabelling", new ARGNSMDLabelling(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_LABELLING")));
                    break;
                case "Packaging":
                    return PartialView("_RowPackaging", new ARGNSMDPackaging(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_PACKAGING")));
                    break;
                case "FootwearMaterial":
                    return PartialView("_RowFootwearMaterial", new ARGNSMDFootwearMaterial(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_FOOTWEAR")));
                    break;
                case "FootwearDetail":
                    return PartialView("_RowFootwearDetail", new ARGNSMDFootwearDetails(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_FDETAILS")));
                    break;
                case "FootwearPackaging":
                    return PartialView("_RowFootwearPackaging", new ARGNSMDFootwearPackaging(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_FPACKAGING")));
                    break;
                case "FootwearPictogram":
                    return PartialView("_RowFootwearPictogram", new ARGNSMDFootwearPictogram(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_FPICTOGRAM")));
                    break;
                case "FootwearSizeRange":
                    return PartialView("_RowFootwearSizeRange", new ARGNSMDFootwearSRange(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_FSRANGE")));
                    break;
                case "FootwearAccessories":
                    return PartialView("_RowFootwearAccessories", new ARGNSMDFootwearAccess(pNextId, mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "@ARGNS_MD_FACCESS")));
                    break;
                default:
                    return null;
            }
        }

        [HttpPost]
        public ActionResult _GetColor(string pModelCode, string pColorCellId)
        {
            List<ARGNSModelColor> mMaterialDetailView = mMaterialDetailManagerView.GetColor(((UserView)Session["UserLogin"]).CompanyConnect, pModelCode);

            return View("_ColorList", Tuple.Create(mMaterialDetailView, pColorCellId));
        }

        [HttpPost]
        public JsonResult Update(MaterialDetailView pMaterialDetail)
        {
            return Json(mMaterialDetailManagerView.Update(((UserView)Session["UserLogin"]).CompanyConnect, pMaterialDetail));

        }

        [HttpPost]
        public JsonResult Add(MaterialDetailView pMaterialDetail)
        {
            return Json(mMaterialDetailManagerView.Add(((UserView)Session["UserLogin"]).CompanyConnect, pMaterialDetail));

        }
    }
}