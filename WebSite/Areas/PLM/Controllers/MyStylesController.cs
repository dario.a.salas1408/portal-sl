﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.Attribute;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class MyStylesController : Controller
    {
        private MyStylesManagerView mMSManagerView;

        public MyStylesController()
        {
            mMSManagerView = new MyStylesManagerView();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.MyStyleList, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<ARGNSModelView> ListModel = new List<ARGNSModelView>();
                PDMSAPCombo mSAPCombo = new PDMSAPCombo();
                return View(Tuple.Create(ListModel, mSAPCombo));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.MyStyleGrid, Enums.Actions.List)]
        public ActionResult PDMGrid()
        {
            try
            {
                List<ARGNSModelView> mListModel = new List<ARGNSModelView>();
                PDMSAPCombo mSAPCombo = new PDMSAPCombo();
                return View("IndexGrid", Tuple.Create(mListModel, mSAPCombo));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("IndexGrid", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.MyCostSheet, Enums.Actions.List)]
        public ActionResult MyCostSheet()
        {
            try
            {
                List<ARGNSCostSheetView> ListCostSheet = new List<ARGNSCostSheetView>();
                return View("IndexCostSheet", ListCostSheet);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("IndexCostSheet", "Error");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtCodeModel"></param>
        /// <param name="txtNameModel"></param>
        /// <param name="txtSeasonModel"></param>
        /// <param name="txtGroupModel"></param>
        /// <param name="txtBrand"></param>
        /// <param name="txtCollection"></param>
        /// <param name="txtSubCollection"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <returns></returns>
        //[HttpPost]
        //public JsonObjectResult MyStyleGrid(string txtCodeModel = "", string txtNameModel = "",
        //	string txtSeasonModel = "", string txtGroupModel = "",
        //	string txtBrand = "", string txtCollection = "",
        //	string txtSubCollection = "",
        //	int pStart = 0, int pLength = 100)
        //{
        //	JsonObjectResult jsonObjectResult = new JsonObjectResult(); 
        //	PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();

        //	if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
        //	{
        //		jsonObjectResult = mMSManagerView.GetBPPDMListSearch((
        //			(UserView)Session["UserLogin"]).CompanyConnect,
        //			((UserView)Session["UserLogin"]).BPCode, ref mPDMSAPCombo, txtCodeModel,
        //			txtNameModel, txtSeasonModel, txtGroupModel, txtBrand, txtCollection,
        //			txtSubCollection, pStart, pLength);
        //	}

        //	if (((UserView)Session["UserLogin"]).IsUser)
        //	{
        //		jsonObjectResult = mMSManagerView.GetUserPDMListSearch(
        //			((UserView)Session["UserLogin"]).CompanyConnect,
        //			((UserView)Session["UserLogin"]).IdUser, ref mPDMSAPCombo,
        //			txtCodeModel, txtNameModel, txtSeasonModel, txtGroupModel,
        //			txtBrand, txtCollection, txtSubCollection,
        //			pStart, pLength);
        //	}
        //	return null;
        //	//return PartialView("_PDMGrid", Tuple.Create(mListPDM, mPDMSAPCombo));
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtCodeModel"></param>
        /// <param name="txtNameModel"></param>
        /// <param name="txtSeasonModel"></param>
        /// <param name="txtGroupModel"></param>
        /// <param name="txtBrand"></param>
        /// <param name="txtCollection"></param>
        /// <param name="txtSubCollection"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult MyStyleGrid(AdvancedSearchStylesView searchViewModel)
        {
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            Util.Miscellaneous commonFunctions = new Miscellaneous();

            IMapper mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<Column, OrderColumn>(); }
                ).CreateMapper();

            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                mJsonObjectResult.ARGNSModelList = Mapper.Map<List<ARGNSModel>>(mMSManagerView.GetBPPDMListSearch((
                    (UserView)Session["UserLogin"]).CompanyConnect,
                    ((UserView)Session["UserLogin"]).BPCode, ref mPDMSAPCombo,
                    searchViewModel.CodeModel == null ? "" : searchViewModel.CodeModel,
                    searchViewModel.NameModel == null ? "" : searchViewModel.NameModel,
                    searchViewModel.SeasonModel == null ? "" : searchViewModel.SeasonModel,
                    searchViewModel.GroupModel == null ? "" : searchViewModel.GroupModel,
                    searchViewModel.BrandModel == null ? "" : searchViewModel.BrandModel,
                    searchViewModel.CollectionModel == null ? "" : searchViewModel.CollectionModel,
                    searchViewModel.SubCollectionModel == null ? "" : searchViewModel.SubCollectionModel,
                    searchViewModel.Start,
                    searchViewModel.Length));
            }

            if (((UserView)Session["UserLogin"]).IsUser)
            {
                mJsonObjectResult = mMSManagerView.GetUserPDMListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
                    ((UserView)Session["UserLogin"]).IdUser, ref mPDMSAPCombo,
                    searchViewModel.CodeModel == null ? "" : searchViewModel.CodeModel,
                    searchViewModel.NameModel == null ? "" : searchViewModel.NameModel,
                    searchViewModel.SeasonModel == null ? "" : searchViewModel.SeasonModel,
                    searchViewModel.GroupModel == null ? "" : searchViewModel.GroupModel,
                    searchViewModel.BrandModel == null ? "" : searchViewModel.BrandModel,
                    searchViewModel.CollectionModel == null ? "" : searchViewModel.CollectionModel,
                    searchViewModel.SubCollectionModel == null ? "" : searchViewModel.SubCollectionModel,
                    searchViewModel.Start,
                    searchViewModel.Length);
            }

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ARGNSModel, ARGNSModelView>();
                cfg.CreateMap<ARGNSSegmentation, ARGNSSegmentationView>();
            }).CreateMapper();

            commonFunctions.ReplaceCodesByDescripcionInModel(mPDMSAPCombo, mJsonObjectResult);

            return PartialView("_PDMGrid", Tuple.Create(mapper.Map<List<ARGNSModelView>>(mJsonObjectResult.ARGNSModelList),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                    searchViewModel.Length));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtCodeModel"></param>
        /// <param name="txtNameModel"></param>
        /// <param name="txtSeasonModel"></param>
        /// <param name="txtGroupModel"></param>
        /// <param name="txtBrand"></param>
        /// <param name="txtCollection"></param>
        /// <param name="txtSubCollection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MyStyleList([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            AdvancedSearchStylesView searchViewModel)
        {
            List<ARGNSModelView> mListPDM = new List<ARGNSModelView>();
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();
            Util.Miscellaneous commonFunctions = new Miscellaneous();

            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                mJsonObjectResult.ARGNSModelList = Mapper.Map<List<ARGNSModel>>(mMSManagerView.GetBPPDMListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
                    ((UserView)Session["UserLogin"]).BPCode, ref mPDMSAPCombo,
                    searchViewModel.CodeModel == null ? "" : searchViewModel.CodeModel,
                    searchViewModel.NameModel == null ? "" : searchViewModel.NameModel,
                    searchViewModel.SeasonModel == null ? "" : searchViewModel.SeasonModel,
                    searchViewModel.GroupModel == null ? "" : searchViewModel.GroupModel,
                    searchViewModel.BrandModel == null ? "" : searchViewModel.BrandModel,
                    searchViewModel.CollectionModel == null ? "" : searchViewModel.CollectionModel,
                    searchViewModel.SubCollectionModel == null ? "" : searchViewModel.SubCollectionModel,
                    searchViewModel.Start,
                    searchViewModel.Length));
            }

            if (((UserView)Session["UserLogin"]).IsUser)
            {
                mJsonObjectResult = mMSManagerView.GetUserPDMListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
                    ((UserView)Session["UserLogin"]).IdUser,
                    ref mPDMSAPCombo,
                    searchViewModel.CodeModel == null ? "" : searchViewModel.CodeModel,
                    searchViewModel.NameModel == null ? "" : searchViewModel.NameModel,
                    searchViewModel.SeasonModel == null ? "" : searchViewModel.SeasonModel,
                    searchViewModel.GroupModel == null ? "" : searchViewModel.GroupModel,
                    searchViewModel.BrandModel == null ? "" : searchViewModel.BrandModel,
                    searchViewModel.CollectionModel == null ? "" : searchViewModel.CollectionModel,
                    searchViewModel.SubCollectionModel == null ? "" : searchViewModel.SubCollectionModel,
                    searchViewModel.Start,
                    searchViewModel.Length);
            }

            commonFunctions.ReplaceCodesByDescripcionInModel(mPDMSAPCombo, mJsonObjectResult);

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ARGNSModel, ARGNSModelView>();
                cfg.CreateMap<ARGNSSegmentation, ARGNSSegmentationView>();
            }).CreateMapper();

            return Json(new DataTablesResponse(requestModel.Draw,
                mapper.Map<List<ARGNSModelView>>(mJsonObjectResult.ARGNSModelList),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtStyle"></param>
        /// <param name="txtCodeCostSheet"></param>
        /// <param name="txtDescription"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult MyCostSheetList(string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            List<ARGNSCostSheetView> listCostSheet = new List<ARGNSCostSheetView>();

            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                listCostSheet = mMSManagerView.GetBPCostSheetListSearch(
                    ((UserView)Session["UserLogin"]).CompanyConnect,
                    ((UserView)Session["UserLogin"]).BPCode, txtStyle,
                    txtCodeCostSheet, txtDescription);
            }

            if (((UserView)Session["UserLogin"]).IsUser)
            {
                listCostSheet = mMSManagerView.GetUserCostSheetListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
                    ((UserView)Session["UserLogin"]).IdUser, txtStyle,
                    txtCodeCostSheet, txtDescription);
            }

            return PartialView("_CostSheetList", listCostSheet);
        }
    }
}