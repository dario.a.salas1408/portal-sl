﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class CriticalPathController : Controller
    {
        private PDMManagerView mPDMMan = new PDMManagerView();

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.PLMCPath, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<ARGNSCrPathView> ListModel = new List<ARGNSCrPathView>();
                return View(ListModel);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }


        [HttpPost]
        public PartialViewResult CrPathList(string txtSeasonCode = "", string txtCollCode = "", string txtSubCollCode = "", string txtModelCode = "", string txtProjectCode = "", string txtCustomerCode = "", string txtVendorCode = "")
        {
            List<ARGNSCrPathView> CrPathList = mPDMMan.GetCriticalPathList(((UserView)Session["UserLogin"]).CompanyConnect, txtCollCode, txtSubCollCode, txtSeasonCode, txtModelCode, txtProjectCode, txtVendorCode, txtCustomerCode);
            return PartialView("_CriticalPathList", CrPathList);
        }
    }
}