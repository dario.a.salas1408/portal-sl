﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.PLM.ColorMaster;
using ARGNS.WebSite.Resources.Views.PLM.PDM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class ColorMasterController : Controller
    {
        private ColorManagerView mColorMan = new ColorManagerView();

        // GET: PLM/ColorMaster
        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.ColorMaster, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<ColorView> ListModel = new List<ColorView>();
                return View(ListModel);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult ColorList(string txtColorCode = "", string txtColorName = "")
        {
            List<ColorView> ColorList = mColorMan.GetColorListSearch(((UserView)Session["UserLogin"]).CompanyConnect, txtColorCode, txtColorName);
            return PartialView("_ColorList", ColorList);
        }

        public ActionResult ActionColorMaster(string pActionColorMaster, string pColorCode)
        {
            try
            {
                ColorView mColorView = null;

                switch (pActionColorMaster)
                {
                    case "Add":
                        ViewBag.Tite = Color.Addnew;
                        ViewBag.FormMode = pActionColorMaster;
                        mColorView = mColorMan.GetColorByCode(((UserView)Session["UserLogin"]).CompanyConnect, pColorCode);                     
                        break;
                    case "Update":
                        ViewBag.Tite = Color.Editnew;
                        ViewBag.FormMode = pActionColorMaster;
                        mColorView = mColorMan.GetColorByCode(((UserView)Session["UserLogin"]).CompanyConnect, pColorCode);
                        break;
                    case "View":
                        ViewBag.Tite = PDM.View;
                        ViewBag.FormMode = pActionColorMaster;
                        mColorView = mColorMan.GetColorByCode(((UserView)Session["UserLogin"]).CompanyConnect, pColorCode);
                        break;

                }

                return View("_Color", mColorView);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public JsonResult UploadFile(FormCollection pData)
        {
            string mFileName = string.Empty;
            //string mUModCode = pData["mUModCode"].ToString();
            //mPageKey = pData["mPageKey"].ToString();
            try
            {
                //List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
                //if (ListaImg == null)
                //{
                //    ListaImg = new List<ARGNSModelImg>();
                //}
                //string mFolder = mUModCode;
                string mFullPath = string.Empty;
                string mName = string.Empty;
                string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;
                    foreach (string mKey in mFiles)
                    {
                        //ARGNSModelImg mObj = new ARGNSModelImg();
                        HttpPostedFileBase mFilePost = mFiles[mKey];
                        mName = mFilePost.FileName;
                        mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

                        if (!Directory.Exists(Server.MapPath("~/images")))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/images"));
                        }
                        mFileName = Server.MapPath("~/images/" + mFileName);
                        //mObj.U_File = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;
                        //mObj.U_Path = mFileName;
                        //mObj.Object = "ARGNS_MODEL";
                        //mObj.Object = "ARGNS_MODEL";
                        //mObj.U_FType = "HDR";
                        mFullPath = mFileName;
                        mFilePost.SaveAs(mFileName);
                        //ListaImg.Add(mObj);
                    }
                }

                //if (Request.Files.Count > 0)
                //{
                //    HttpFileCollectionBase mFiles = Request.Files;

                //    for (int i = 0; i < mFiles.Count; i++)
                //    {
                //        HttpPostedFileBase mFilePost = mFiles[i];

                //        mName = mFilePost.FileName;

                //        if (mTechPack.Lines.Where(c => c.ImageName == mName).FirstOrDefault().ImgChanged == true)
                //        {
                //            mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

                //            if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
                //            {
                //                Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
                //            }

                //            mFullPath = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

                //            mFilePost.SaveAs(mFullPath);

                //            mTechPack.Lines.Where(c => c.ImageName == mName).FirstOrDefault().U_Imag = mFullPath;
                //        }
                //    }
                //}

                //TempData[mPageKey + "ModelImageList"] = ListaImg;
                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult Add(ColorView pColorView)
        {
            pColorView.U_Argb = pColorView.getColorARGB(pColorView.Alpha, pColorView.Red, pColorView.Green, pColorView.Blue).ToString();

            return Json(mColorMan.Add(((UserView)Session["UserLogin"]).CompanyConnect, pColorView));

        }

        [HttpPost]
        public JsonResult Update(ColorView pColorView)
        {
            pColorView.U_Argb = pColorView.getColorARGB(pColorView.Alpha, pColorView.Red, pColorView.Green, pColorView.Blue).ToString();

            return Json(mColorMan.Update(((UserView)Session["UserLogin"]).CompanyConnect, pColorView));
        }

        //[HttpPost]
        //public JsonResult UpdateDesign(ARGNSModelView design)
        //{
        //    mPageKey = design.PageKey;
        //    List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
        //    design.ModelImageList = ListaImg;
        //    if (ListaImg.Count > 0 && (design.U_Pic == null || design.U_Pic == ""))
        //    {
        //        design.U_Pic = ListaImg.FirstOrDefault().U_Path;
        //        design.U_PicR = ListaImg.FirstOrDefault().U_Path;
        //    }
        //    TempData[mPageKey + "ModelImageList"] = ListaImg;

        //    return Json(mDesignManagerView.UpdateDesign(design, ((UserView)Session["UserLogin"]).CompanyConnect));
        //}

        //[HttpPost]
        //public JsonResult AddDesign(ARGNSModelView design, FormCollection pData)
        //{
        //    mPageKey = design.PageKey;
        //    List<ARGNSModelImg> ListaImg = ((List<ARGNSModelImg>)TempData[mPageKey + "ModelImageList"]);
        //    design.ModelImageList = ListaImg;
        //    if (ListaImg.Count > 0)
        //    {
        //        design.U_Pic = ListaImg.FirstOrDefault().U_Path;
        //        design.U_PicR = ListaImg.FirstOrDefault().U_Path;
        //    }
        //    TempData[mPageKey + "ModelImageList"] = ListaImg;

        //    return Json(mDesignManagerView.AddDesign(design, ((UserView)Session["UserLogin"]).CompanyConnect));
        //}
    }
}