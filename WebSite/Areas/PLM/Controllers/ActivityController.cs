﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PLM.Activity;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class ActivityController : Controller
    {
        private ActivitiesManagerView mManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;

        public ActivityController()
        {
            mManagerView = new ActivitiesManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.Activity, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                ActivitiesSAPComboView mActivityCombo = new ActivitiesSAPComboView();

                List<ActivityStatusSAP> mActivityStatus = new List<ActivityStatusSAP>();
                mActivityStatus.Add(new ActivityStatusSAP(-1, ""));
                mActivityStatus.AddRange(mManagerView.GetActivityStatusList(((UserView)Session["UserLogin"]).CompanyConnect));
                mActivityCombo.ActivityStatusList = mActivityStatus;
                mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
                {
                    new ActivityUserType(-1,""),
                    new ActivityUserType(0,"User"),
                    new ActivityUserType(1,"Employee")
                };
                return View(mActivityCombo);
            }

            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.PLMMyActivities, Enums.Actions.List)]
        public ActionResult IndexMyActivity()
        {
            try
            {
                ActivitiesSAPComboView mActivityCombo = new ActivitiesSAPComboView();
                List<ActivityStatusSAP> mActivityStatus = new List<ActivityStatusSAP>();
                mActivityStatus.Add(new ActivityStatusSAP(-1, ""));
                mActivityStatus.AddRange(mManagerView.GetActivityStatusList(((UserView)Session["UserLogin"]).CompanyConnect));
                mActivityCombo.ActivityStatusList = mActivityStatus;
                mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
                {
                    new ActivityUserType(-1,""),
                    new ActivityUserType(0,"User"),
                    new ActivityUserType(1,"Employee")
                };

                return View("MyActivityIndex", mActivityCombo);
            }

            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("MyActivityIndex", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult ActivityList(int UserType, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            if (txtStatus == "-1")
                txtStatus = null;
            List<ActivitiesView> mActivityList = mManagerView.GetActivitiesSeach(((UserView)Session["UserLogin"]).CompanyConnect, UserType, txtModel, txtProject, txtStatus, txtRemarks);
            ActivitiesSAPComboView mActivityCombo = new ActivitiesSAPComboView();
            mActivityCombo.ActivityStatusList = mManagerView.GetActivityStatusList(((UserView)Session["UserLogin"]).CompanyConnect);
            mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
            {
                new ActivityUserType(-1,""),
                new ActivityUserType(0,"User"),
                new ActivityUserType(1,"Employee")
            };
            return PartialView("_ActivityList", Tuple.Create(mActivityList, mActivityCombo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserType"></param>
        /// <param name="txtModel"></param>
        /// <param name="txtProject"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult MyActivityList(int UserType, string txtModel = "", string txtProject = "", string txtStatus = "", string txtRemarks = "")
        {
            if (txtStatus == "-1")
                txtStatus = null;
            List<ActivitiesView> mActivityList = new List<ActivitiesView>();
            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                mActivityList = mManagerView.GetBPMyActivitiesSeach(((UserView)Session["UserLogin"]).CompanyConnect, UserType, ((UserView)Session["UserLogin"]).BPCode, txtModel, txtProject, txtStatus, txtRemarks);
            }
            if (((UserView)Session["UserLogin"]).IsUser)
            {
                mActivityList = mManagerView.GetUserMyActivitiesSearch(((UserView)Session["UserLogin"]).CompanyConnect, UserType, ((UserView)Session["UserLogin"]).IdUser, txtModel, txtProject, txtStatus, txtRemarks);
            }
            ActivitiesSAPComboView mActivityCombo = new ActivitiesSAPComboView();
            mActivityCombo.ActivityStatusList = mManagerView.GetActivityStatusList(((UserView)Session["UserLogin"]).CompanyConnect);
            mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
            {
                new ActivityUserType(-1,""),
                new ActivityUserType(0,"User"),
                new ActivityUserType(1,"Employee")
            };
            return PartialView("_MyActivityList", Tuple.Create(mActivityList, mActivityCombo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pAction"></param>
        /// <param name="pCode"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        public ActionResult ActivityAction(string pAction, string pCode, string pFrom)
        {
            try
            {
                ActivitiesView mView = null;

                switch (pAction)
                {
                    case "Add":
                        ViewBag.Tite = Activity.Addnew;
                        ViewBag.FormMode = pAction;
                        ViewBag.FromPage = pFrom;
                        mView = mManagerView.GetActivity(0, ((UserView)Session["UserLogin"]).CompanyConnect);
                        mView.Recontact = System.DateTime.Now;
                        mView.endDate = System.DateTime.Now;
                        break;

                    case "Update":
                        ViewBag.Tite = Activity.Editnew;
                        ViewBag.FormMode = pAction;
                        ViewBag.FromPage = pFrom;
                        mView = mManagerView.GetActivity(Convert.ToInt32(pCode), ((UserView)Session["UserLogin"]).CompanyConnect);
                        break;

                    case "View":
                        ViewBag.Tite = Activity.ViewActivity;
                        ViewBag.FormMode = pAction;
                        ViewBag.FromPage = pFrom;
                        mView = mManagerView.GetActivity(Convert.ToInt32(pCode), ((UserView)Session["UserLogin"]).CompanyConnect);
                        break;

                }

                return View("_Activity", Tuple.Create(mView, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.Activity, ((UserView)Session["UserLogin"]).CompanyConnect)));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(ActivitiesView model)
        {
            if (model.status == -1)
                model.status = null;
            return Json(mManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(ActivitiesView model)
        {
            if (model.status == -1)
                model.status = null;
            return Json(mManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Vendors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.All, true, 
                    searchViewModel.pCardCode, searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, 
                    ((UserView)Session["UserLogin"]).SECode, ((UserView)Session["UserLogin"]).BPGroupId, true,
                    requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.All, true, 
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, false,
                    null, null, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="LocalCurrency"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBp(string Id, string LocalCurrency)
        {
            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, LocalCurrency);

            if (mBP != null)
            {
                return Json(mBP, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json("", JsonRequestBehavior.AllowGet); }

        }

         [HttpPost]
        public JsonResult GetActivityUserList(string pUserType)
        {
            List<ActivityUser> ret = mManagerView.GetActivityUserList(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pUserType));

            if (ret != null)
            {
                return Json(ret, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json("", JsonRequestBehavior.AllowGet); }

        }

    }
}