﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PDC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class PDCController : Controller
    {
        private PDCManagerView mPDCManagerView;

        public PDCController()
        {
            mPDCManagerView = new PDCManagerView();
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.PDC, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                PDCSAPCombo combo = new PDCSAPCombo();
                combo = mPDCManagerView.GetPDCCombo(((UserView)Session["UserLogin"]).CompanyConnect);
                return View(Tuple.Create(new List<ARGNSPDCView>(), combo));
            }

            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult PDCList(DateTime? pPD, string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "")
        {
            PDCSAPCombo combo = new PDCSAPCombo();
            List<ARGNSPDCView> mPDCList = mPDCManagerView.GetPDCList(((UserView)Session["UserLogin"]).CompanyConnect, ref combo, pPD, pCode, pEmployee, pShift, pVendor);
            return PartialView("_PDCList", Tuple.Create(mPDCList, combo));
        }

        public ActionResult PDCAction(string PDCAction, string Code)
        {
            try
            {
                ARGNSPDCView mView = null;
                PDCSAPCombo combo = new PDCSAPCombo();
                switch (PDCAction)
                {
                    case "Add":
                        ViewBag.Tite = PDC.AddNew;
                        ViewBag.FormMode = PDCAction;
                        mView = mPDCManagerView.GetPDCById(((UserView)Session["UserLogin"]).CompanyConnect, "0", ref combo);
                        break;

                    case "View":
                        ViewBag.Tite = PDC.ViewPDC;
                        ViewBag.FormMode = PDCAction;
                        mView = mPDCManagerView.GetPDCById(((UserView)Session["UserLogin"]).CompanyConnect, Code, ref combo);
                        break;

                }
                BusinessPartnerSAP mBP = new BusinessPartnerSAP();
                mBP.CardType = "S";
                combo.ListBusinessPartner.Insert(0, mBP);

                TempData["PDCObject"] = mView;
                TempData["Combo"] = combo;
                return View("_PDC", Tuple.Create(mView, combo));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult _GetPDCLineCodeBar(string pCodeBar)
        {
            ARGNSPDCView mPDCObject = (ARGNSPDCView)TempData["PDCObject"];
            PDCSAPCombo mCombo = (PDCSAPCombo)TempData["Combo"];
            ARGNSPDCLine returnLine = mPDCManagerView.GetPDCLineCodeBar(((UserView)Session["UserLogin"]).CompanyConnect, pCodeBar);
            if (returnLine != null)
                mPDCObject.Lines.Add(returnLine);
            TempData["PDCObject"] = mPDCObject;
            TempData["Combo"] = mCombo;
            return PartialView("_PDCLines", Tuple.Create(mPDCObject.Lines, mCombo));
        }

        [HttpPost]
        public PartialViewResult _GetPDCLinesCutTick(string pCutTick)
        {
            ARGNSPDCView mPDCObject = (ARGNSPDCView)TempData["PDCObject"];
            PDCSAPCombo mCombo = (PDCSAPCombo)TempData["Combo"];
            List<ARGNSPDCLine> returnLine = mPDCManagerView.GetPDCLinesCutTick(((UserView)Session["UserLogin"]).CompanyConnect, pCutTick);
            mPDCObject.Lines.AddRange(returnLine);
            TempData["PDCObject"] = mPDCObject;
            TempData["Combo"] = mCombo;
            return PartialView("_PDCLines", Tuple.Create(mPDCObject.Lines, mCombo));
        }


        [HttpPost]
        public JsonResult Add(ARGNSPDCView model)
        {
            return Json(mPDCManagerView.Add(((UserView)Session["UserLogin"]).CompanyConnect, model));
        }

        [HttpPost]
        public JsonResult DeleteRow(string RowId)
        {
            try
            {
                ARGNSPDCView mPDCObject = (ARGNSPDCView)TempData["PDCObject"];
                mPDCObject.Lines.RemoveAll(c => c.LineId == RowId);
                TempData["PDCObject"] = mPDCObject;
                return Json("Ok");
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

    }
}