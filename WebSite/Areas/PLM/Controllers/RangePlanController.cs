﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.PLM.PDM;
using ARGNS.WebSite.Resources.Views.PLM.RangePlan;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    public class RangePlanController : Controller
    {
        private PDMManagerView mPDMManagerView;
        private RangePlanManagerView mRangePlanManagerView;

        public RangePlanController()
        {
            mPDMManagerView = new PDMManagerView();
            mRangePlanManagerView = new RangePlanManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.PLMRangePlan, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                RangePlanCombo mCombo = new RangePlanCombo();
                mCombo = mRangePlanManagerView.GetRangePlanCombo(((UserView)Session["UserLogin"]).CompanyConnect);
                mCombo.ListSeason.Insert(0, new ARGNSSeason("-1", PDM.Season));
                mCombo.ListCollection.Insert(0, new ARGNSCollection("-1", PDM.Coll));
                mCombo.ListEmployee.Insert(0, new EmployeeSAP(-1, RangePlan.Employee, ""));
                return View(mCombo);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionRangePlan"></param>
        /// <param name="pRangePlanCode"></param>
        /// <returns></returns>
        public ActionResult ActionRangePlan(string ActionRangePlan, string pRangePlanCode)
        {
            ARGNSRangePlanView mRangePlanView = null;
            string mPageKey = PageKeyGenerator.GetPageKey();

            switch (ActionRangePlan)
            {
                case "Update":
                    ViewBag.FormMode = ActionRangePlan;
                    ViewBag.PageKey = mPageKey;
                    mRangePlanView = mRangePlanManagerView.GetRangePlanById(((UserView)Session["UserLogin"]).CompanyConnect, pRangePlanCode);
                    ViewBag.Tite = RangePlan.RP;
                    break;
            }

            return View("_RangePlan", mRangePlanView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtCode"></param>
        /// <param name="txtSeason"></param>
        /// <param name="txtCollection"></param>
        /// <param name="txtEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ListRangePlan(string txtCode = "", string txtSeason = "", string txtCollection = "", string txtEmployee = "")
        {
            RangePlanCombo mRPCombo = new RangePlanCombo();
            List<ARGNSRangePlanView> mListRangePlan = mRangePlanManagerView.GetRangePlanListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ref mRPCombo, txtCode, txtSeason, txtCollection, txtEmployee);

            return PartialView("_RangePlanList", Tuple.Create(mListRangePlan,mRPCombo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetModelList(AdvancedSearchStylesView searchViewModel)
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            IMapper mapper = new MapperConfiguration(
               cfg => { cfg.CreateMap<Column, OrderColumn>(); }
               ).CreateMapper();

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ModelDesc, ModelDescView>();
            }).CreateMapper();

            RangePlanCombo mRPCombo = new RangePlanCombo();
            mRPCombo = mRangePlanManagerView.GetRangePlanCombo(((UserView)Session["UserLogin"]).CompanyConnect);

            bool permissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
            .Where(c => c.PageInternalKey == Enums.Pages.PLMRangePlan.ToDescriptionString()
            && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
            .FirstOrDefault() != null ? true : false;

            JsonObjectResult mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), permissionToViewStock,
                searchViewModel.Start, searchViewModel.Length);

            return PartialView("_ModelList", Tuple.Create(
             mapper.Map<List<ModelDescView>>(mJsonObjectResult.ModelDescViewList),
                 Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                 searchViewModel.Length, mRPCombo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pRangePlan"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateRangePlan(ARGNSRangePlanView pRangePlan)
        {
            return Json(mRangePlanManagerView.Update(pRangePlan, ((UserView)Session["UserLogin"]).CompanyConnect));
        }

    }
}