﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.PLM.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    [CustomerAuthorize(Enums.Areas.PLM)]
    public class LogoController : Controller
    {
        private LogoManagerView mLogoManagerView = new LogoManagerView();

        public ActionResult ActionLogo(string ActionLogo, string pCode)
        {
            LogoView mLogoView = null;

            switch (ActionLogo)
            {
                case "View":
                    ViewBag.FormMode = ActionLogo;
                    mLogoView = mLogoManagerView.GetLogoById(((UserView)Session["UserLogin"]).CompanyConnect,pCode);
                    ViewBag.Tite = PDM.View  + " Logo " + pCode;
                    break;

            }

            return View("_Logo", mLogoView);
        }

        [HttpPost]
        public PartialViewResult GetLogoList(string pCode)
        {
            List<ARGNSModelLogoView> ScaleList = mLogoManagerView.GetLogoList(((UserView)Session["UserLogin"]).CompanyConnect, pCode);
            return PartialView("_LogoList", ScaleList);
        }


    }
}