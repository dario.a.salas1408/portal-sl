﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.PLM.Activity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    public class TechPackController : Controller
    {
        private TechPackManagerView mTechPackManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;

        public TechPackController()
        {
            mTechPackManagerView = new TechPackManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.PLMTechPack)]
        public ActionResult Index()
        {
            return View();
        }

        [CustomerAuthorize(Enums.Areas.PLM, Enums.Pages.PLMTechPack)]
        public ActionResult TechPackAction(string pAction, int pCode)
        {
            try
            {
                string mPageKey = PageKeyGenerator.GetPageKey();
                ViewBag.PageKey = mPageKey;

                ARGNSTechPackView mView = null;

                switch (pAction)
                {
                    case "Update":
                        ViewBag.Tite = Activity.Editnew;
                        ViewBag.FormMode = pAction;
                        mView = mTechPackManagerView.GetTechPack(((UserView)Session["UserLogin"]).CompanyConnect, pCode);
                        break;

                    case "View":
                        ViewBag.Tite = Activity.ViewActivity;
                        ViewBag.FormMode = pAction;
                        mView = mTechPackManagerView.GetTechPack(((UserView)Session["UserLogin"]).CompanyConnect, pCode);
                        break;
                }
                TempData[mPageKey + "SectionList"] = mView.Sections;

                return View("_TechPack", Tuple.Create(mView, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.PLMTechPack, ((UserView)Session["UserLogin"]).CompanyConnect)));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult _GetRow(int pNextId, string pPageKey)
        {
            List<ARGNSTechPackSectionView> mListSections = ((List<ARGNSTechPackSectionView>)TempData[pPageKey + "SectionList"]);
            TempData[pPageKey + "SectionList"] = mListSections;

            return PartialView("_RowSection", Tuple.Create(new ARGNSTechPackLineView(pNextId), mListSections));
        }

        public JsonResult UpdateTechPack(FormCollection pForm)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ARGNSTechPackView mTechPack = Newtonsoft.Json.JsonConvert.DeserializeObject<ARGNSTechPackView>(pForm["ARGNSTechPackView"]);
            
            string mFolder = mTechPack.ModelCode + "_TeckPack";
            string mFullPath = string.Empty;
            string mName = string.Empty;
            string mFileName = string.Empty;
            string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase mFiles = Request.Files;

                for (int i = 0; i < mFiles.Count; i++)
                {
                    HttpPostedFileBase mFilePost = mFiles[i];

                    mName = mFilePost.FileName;

                    if(mTechPack.Lines.Where(c => c.ImageName == mName).FirstOrDefault().ImgChanged == true)
                    { 
                        mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

                        if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
                        }

                        mFullPath = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

                        mFilePost.SaveAs(mFullPath);

                        mTechPack.Lines.Where(c => c.ImageName == mName).FirstOrDefault().U_Imag = mFullPath;
                    }
                }
            }

            return Json(mTechPackManagerView.UpdateTechPack(mTechPack, ((UserView)Session["UserLogin"]).CompanyConnect));
        }

        public JsonResult DeleteDragAndDropImg()
        {
            return Json("Ok");
        }

        }
}