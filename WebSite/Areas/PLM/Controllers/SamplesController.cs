﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ARGNS.WebSite.Areas.PLM.Controllers
{
    public class SamplesController : Controller
    {
        private SampleManagerView mSampleManagerView;
        private ScaleMasterManagerView mScaleMasterManagerView;
        private PDMManagerView mPDMManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private string mPageKey;

        public SamplesController()
        {
            mSampleManagerView = new SampleManagerView();
            mScaleMasterManagerView = new ScaleMasterManagerView();
            mPDMManagerView = new PDMManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        }
        // GET: PLM/Samples
        public ActionResult Index()
        {

            ViewBag.Title = "Add Samples";

            return View("_Samples");
        }

        public ActionResult ViewAdd(string pModCode)
        {
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.Title = "Add Samples";
            ViewBag.FormMode = "Add";
            ViewBag.PageKey = mPageKey;

            ARGNSModelSampleView mReturn = mSampleManagerView.GetSample(((UserView)Session["UserLogin"]).CompanyConnect, "0", pModCode);

            TempData[mPageKey + "ListPOM"] = mReturn.ListPOM;

            TempData[mPageKey + "POMModel"] = mPDMManagerView.GetModelPomList(((UserView)Session["UserLogin"]).CompanyConnect, pModCode);

            if (mReturn.ListScale.FirstOrDefault() != null)
            {
                mReturn.ListPOM = mReturn.ListPOM.Where(c => c.U_Scale == mReturn.ListScale.FirstOrDefault().U_SclCode).ToList();

                mReturn.ListSizes = mScaleMasterManagerView.GetSizeByScale(((UserView)Session["UserLogin"]).CompanyConnect, mReturn.ListScale.FirstOrDefault().U_SclCode, pModCode);
            }

            mReturn.ListSizes.Select(c => { c.U_Selected = "N"; return c; }).ToList();

            mReturn.U_ModCode = pModCode;

            return View("_Samples", mReturn);
        }

        public ActionResult ViewUpdate(string pModCode, string pSampleCode)
        {
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.Title = "Update Samples";
            ViewBag.FormMode = "Update";
            ViewBag.PageKey = mPageKey;

            ARGNSModelSampleView mReturn = mSampleManagerView.GetSample(((UserView)Session["UserLogin"]).CompanyConnect, pSampleCode, pModCode);

            string[] mSizeSelected = mReturn.ListLine.Select(c => c.U_SizeCode).Distinct().ToArray();

            foreach (var item in mReturn.ListLine)
            {
                mReturn.ListModelPomTab.Add(new ARGNSModelPomView { U_Desc = item.U_Desc, U_Value = item.U_Target, U_SizeCode = item.U_SizeCode, U_Actual = item.U_Actual, U_Revision = item.U_Revision });
            }

            TempData[mPageKey + "ListPOM"] = mReturn.ListPOM;

            TempData[mPageKey + "POMModel"] = mPDMManagerView.GetModelPomList(((UserView)Session["UserLogin"]).CompanyConnect, pModCode);

            mReturn.ListPOM = mReturn.ListPOM.Where(c => c.U_Scale == mReturn.U_SclCode).ToList();

            mReturn.ListSizes = mScaleMasterManagerView.GetSizeByScale(((UserView)Session["UserLogin"]).CompanyConnect, mReturn.U_SclCode, pModCode);

            mReturn.ListSizes.Select(c => { c.U_Selected = "N"; return c; }).ToList();

            foreach (var item in mReturn.ListSizes.Where(c => mSizeSelected.Contains(c.U_SizeCod)).ToList())
            {
                item.U_Selected = "Y";
            }

            mReturn.U_ModCode = pModCode;

            return View("_Samples", mReturn);
        }

        [HttpPost]
        public JsonResult GetPOM(string pSclCode, string pPageKey)
        {
            mPageKey = pPageKey;
            List<ARGNSModelPomTemplate> ListPOM = (List<ARGNSModelPomTemplate>)TempData[mPageKey + "ListPOM"];

            TempData[mPageKey + "ListPOM"] = ListPOM;

            if (ListPOM != null)
            {
                return Json(ListPOM.Where(c => c.U_Scale == pSclCode).ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            { return Json("", JsonRequestBehavior.AllowGet); }

        }

        [HttpPost]
        public PartialViewResult GetSizes(string pSclCode, string pModeCode)
        {
            List<ARGNSSize> mReturn = mScaleMasterManagerView.GetSizeByScale(((UserView)Session["UserLogin"]).CompanyConnect, pSclCode, pModeCode);

            mReturn.Select(c => { c.U_Selected = "N"; return c; }).ToList();

            return PartialView("_Sizes", mReturn);
        }

        [HttpPost]
        public PartialViewResult GenerateTab(string pU_PomCode, string pSizes, string pPageKey)
        {
            mPageKey = pPageKey;
            string[] pArrSizes = pSizes.ToString().Split(',');


            List<ARGNSModelPomView> mReturn = (List<ARGNSModelPomView>)TempData[mPageKey + "POMModel"];

            TempData[mPageKey + "POMModel"] = mReturn;

            return PartialView("_TabScalePom", mReturn.Where(c => c.U_PomCode == pU_PomCode && pArrSizes.Contains(c.U_SizeCode)).OrderBy(c => c.U_SizeCode).ToList());
        }

        [HttpPost]
        public JsonResult AddModel(FormCollection pForm)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ARGNSModelSampleView mARGNSModelSampleView = serializer.Deserialize<ARGNSModelSampleView>(pForm["ARGNSModelSampleView"]);

            //Verifico si existe
            ARGNSModelSampleView mReturnValidate = mSampleManagerView.GetSampleByCode(((UserView)Session["UserLogin"]).CompanyConnect, mARGNSModelSampleView.U_SampCode, mARGNSModelSampleView.U_ModCode);

            if (mReturnValidate != null)
            {
                return Json("The Sample not Exit");
            }

            string mFolder = mARGNSModelSampleView.U_ModCode + "_Samples";
            string mFullPath = string.Empty;
            string mName = string.Empty;
            string mFileName = string.Empty;
            //string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

            if (Request.Files.Count > 0)
            {
                HttpFileCollectionBase mFiles = Request.Files;
                for (int i = 0; i < mFiles.Count; i++)
                {
                    HttpPostedFileBase mFilePost = mFiles[i];

                    mName = mFilePost.FileName;

                    mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

                    if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
                    }

                    mFullPath = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

                    mFilePost.SaveAs(mFullPath);
                }
            }

            mARGNSModelSampleView.U_Picture = mFullPath;

            string mResult = mSampleManagerView.AddSample(mARGNSModelSampleView, ((UserView)Session["UserLogin"]).CompanyConnect);

            if (mResult == "Ok")
            {
                ARGNSModelSampleView mReturnAdd = mSampleManagerView.GetSampleByCode(((UserView)Session["UserLogin"]).CompanyConnect, mARGNSModelSampleView.U_SampCode, mARGNSModelSampleView.U_ModCode);
                return Json("Ok," + mReturnAdd.Code.ToString());
            }
            else
            { return Json(mResult); }

        }

        [HttpPost]
        public JsonResult UpdateModel(FormCollection pForm)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            ARGNSModelSampleView mARGNSModelSampleView = serializer.Deserialize<ARGNSModelSampleView>(pForm["ARGNSModelSampleView"]);

            if (Request.Files.Count > 0)
            {


                string mFolder = mARGNSModelSampleView.U_ModCode + "_Samples";
                string mFullPath = string.Empty;
                string mName = string.Empty;
                string mFileName = string.Empty;
                //string mCode = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString();

                HttpFileCollectionBase mFiles = Request.Files;
                for (int i = 0; i < mFiles.Count; i++)
                {
                    HttpPostedFileBase mFilePost = mFiles[i];

                    mName = mFilePost.FileName;

                    mFileName = System.DateTime.Now.Day.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + mFilePost.FileName;

                    if (!Directory.Exists(Server.MapPath("~/images/" + mFolder)))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/images/" + mFolder));
                    }

                    mFullPath = Server.MapPath("~/images/" + mFolder + "/" + mFileName);

                    mFilePost.SaveAs(mFullPath);

                    mARGNSModelSampleView.U_Picture = mFullPath;
                }
            }

            return Json(mSampleManagerView.UpdateSample(mARGNSModelSampleView, ((UserView)Session["UserLogin"]).CompanyConnect));

        }

        [HttpPost]
        public PartialViewResult CustomerSA(string pVendorCode = "", string pVendorName = "")
        {
            pVendorCode = pVendorCode.TrimStart().TrimEnd();
            pVendorName = pVendorName.TrimStart().TrimEnd();
            List<BusinessPartnerView> ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer).Where(c => (c.CardCode.ToUpper().Contains(pVendorCode.ToUpper())) && (c.CardName.ToUpper().Contains(pVendorName.ToUpper()))).ToList();

            return PartialView("_CustomerSA", ListReturn);
        }

        [HttpPost]
        public PartialViewResult CustomerSUP(string pVendorCode = "", string pVendorName = "")
        {
            pVendorCode = pVendorCode.TrimStart().TrimEnd();
            pVendorName = pVendorName.TrimStart().TrimEnd();
            List<BusinessPartnerView> ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor).Where(c => (c.CardCode.ToUpper().Contains(pVendorCode.ToUpper())) && (c.CardName.ToUpper().Contains(pVendorName.ToUpper()))).ToList();

            return PartialView("_CustomerSUP", ListReturn);
        }


    }
}