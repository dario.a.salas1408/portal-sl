﻿$(document).ready(function () {
    $("#btnSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/VarMaster/VariableList',
            contextType: 'application/html;charset=utf-8',
            data: { txtVarCode: $("#txtVarCode").val(), txtVarName: $("#txtVarName").val(), txtVarSegmentation: $("#txtVarSegmentation").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyVariable").html(data);

            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});