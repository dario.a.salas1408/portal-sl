﻿$(document).ready(function () {
    $('.input-group.date').datepicker({});

    $("#btnSearch").click(function () {
        dateASD = $("#txtASD").datepicker("getDate");
        dateADA = $("#txtADA").datepicker("getDate");

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/Shipment/_GetShipmentList',
            contextType: 'application/html;charset=utf-8',
            data: { txtShipmentNum: $("#txtShipmentNum").val(), txtStatus: $("#txtStatus").val(), txtPOE: $("#txtPOE").val(), dateASD: (isNaN(dateASD) == true ? null : (dateASD.getMonth() + 1) + "/" + dateASD.getDate() + "/" + dateASD.getFullYear()), dateADA: (isNaN(dateADA) == true ? null : (dateADA.getMonth() + 1) + "/" + dateADA.getDate() + "/" + dateADA.getFullYear()) },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyShipment").html(data);

            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});