﻿var mObjType;

$(document).ready(function () {
    $('.input-group.date').datepicker({});

    $('#UpdateCorrectly').hide();

    $("#btnOK").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        switch (formMode) {
            case "Add":
                Add();
                break;
            case "Update":
                Update();
                break;
            default:
                alert('Imposible realizar una acción')
                break;
        }

    });

    $("#LoadVendor").hide();

    $("#btnDocumentChoose").click(function () {
        LoadDocumentLinesModal(mObjType);
    });

    $("#btnDocumentLinesChoose").click(function () {
        AddDocumentLines();
    });

    $("#ListShipmentDocument").click(function () {
        //if ($("#LiMaterial").attr('class') == "active")
        //    ListMaterialData = $("#divpartil").html();
        //if ($("#LiOp").attr('class') == "active")
        //    ListOpData = $("#divpartil").html();
        //if ($("#LiSh").attr('class') == "active")
        //    ListSchemaData = $("#divpartil").html();

        //$("#LiOp").removeClass("active");
        //$("#LiSh").removeClass("active");
        //$("#LiMaterial").addClass("active");
        //if (ListMaterialClicked == false) {
        //    $("#LoadingComment").show();
            $.ajax({
                url: '/PLM/Shipment/_GetShipmentDocument',
                contextType: 'application/html;charset=utf-8',
                data: {pShipmentID: $("#DocEntry").val(), pPageKey: $("#Pagekey").val()},
                type: 'POST',
                dataType: 'html'
            }).done(function (data) {
                //$("#LoadingComment").hide();

                $("#tabShipmentDocument").empty().html(data);

            }).fail(function () {
                alert("error");
            });
        //}
        //else {
        //    $("#divpartil").empty().html(ListMaterialData);
        //}

    });


    $("#SearchBP").click(function () {

        var mVendorCode;
        var mVendorName;

        if ($('#ckCodeVendor').is(':checked')) {
            mVendorCode = $('#txtSearchVendor').val();
        }

        if ($('#ckNameVendor').is(':checked')) {
            mVendorName = $('#txtSearchVendor').val();
        }

        $("#LoadVendor").show('slow');
        $.ajax({
            url: '/PLM/Shipment/_BusinessPartner',
            contextType: 'application/html;charset=utf-8',
            data: { pVendorCode: mVendorCode, pVendorName: mVendorName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadVendor").hide();
            $("#ModalBodyVendor").html(data);
        });

    });

});

function LoadContainerModalPackages() {
    $.ajax({
        url: '/PLM/Shipment/_ContainerModalPackage',
        contextType: 'application/html;charset=utf-8',
        data: { pShipmentID: $("#DocEntry").val(), pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#ContainerModalTable').html(data);

        $.ajax({
            url: '/PLM/Shipment/_ContainerModalAvailableItems',
            contextType: 'application/html;charset=utf-8',
            data: { pShipmentID: $("#DocEntry").val(), pPageKey: $("#Pagekey").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#ContainerModalAvailableItemsTable').html(data);
        }).fail(function () {
            alert("error");
        });

    }).fail(function () {
        alert("error");
    });

}

function LoadContainerModalPackageItems(pContainer) {
    $.ajax({
        url: '/PLM/Shipment/_ContainerModalPackageItems',
        contextType: 'application/html;charset=utf-8',
        data: { pContainer: pContainer, pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#ContainerModalPackageItemsTable').html(data);
    }).fail(function () {
        alert("error");
    });
}

function AddItemToPackage() {
    var mSelectedItems = [];
    var mFailFlag = true;
    $("#ContainerModalAvailableItemsTable :input[type=checkbox]").each(function () {
        var input = $(this);
        if (input.prop('checked') == true) {
            mSelectedItems.push({
                LineId: input.attr('value'),
                Quantity: $("#Selected" + input.attr('value')).val()
            });
        }
    });

    if (mSelectedItems.length <= 0) {
        alert($("#msgSelectItem").val());
        mFailFlag = false;
    }

    if ($('input[name=containerRadio]:checked').val() == "" || $('input[name=containerRadio]:checked').val() == null) {
        alert($("#msgSelectPackage").val());
        mFailFlag = false;
    }

    $("#ContainerModalAvailableItemsTable :input[type=hidden]").each(function () {
        var input = $(this);
        if ($("#Selected" + input.attr('id')).val() == "" || $("#Selected" + input.attr('id')).val() == null) {
            alert($("#msgSelectItemQuantity").val());
            mFailFlag = false;
        }
        else {
            if ($("#Selected" + input.attr('id')).val() > $("#LineQty" + input.attr('id')).val()) {
                alert($("#msgSelectItemQuantityGter").val());
                mFailFlag = false;
            }
        }
    });

    if (mFailFlag == true) {
        var ItemToPackageView = {
            ItemsToAdd: mSelectedItems,
            PackageNum: $('input[name=containerRadio]:checked').val(),
            pPageKey: $("#Pagekey").val()
        }

        $.ajax({
            url: "/PLM/Shipment/_AddItemToPackage",
            async: false,
            type: "POST",
            data: JSON.stringify(ItemToPackageView),
            dataType: "html",
            contentType: "application/json; charset=utf-8",
        }).done(function (data) {
            $('#ContainerModalPackageItemsTable').html(data);
            $.ajax({
                url: "/PLM/Shipment/_UpdateAvailableItem",
                async: false,
                type: "POST",
                data: { pPageKey: $("#Pagekey").val() },
                dataType: "html",
                contextType: 'application/html;charset=utf-8',
            }).done(function (data) {
                $('#ContainerModalAvailableItemsTable').html(data);
            }).fail(function (data) {
                alert("error");
            }); 
        }).fail(function (data) {
            alert("error");
        });
    }
}

function Update() {

    var dateU_ESD = $("#U_ESD").datepicker("getDate");
    var dateU_EDA = $("#U_EDA").datepicker("getDate");
    var dateU_EDW = $("#U_EDW").datepicker("getDate");
    var dateU_ASD = $("#U_ASD").datepicker("getDate");
    var dateU_ADA = $("#U_ADA").datepicker("getDate");
    var dateU_ADW = $("#U_ADW").datepicker("getDate");

    var ShipmentView = {
        DocEntry: $("#DocEntry").val(),
        U_Shipment: $("#U_Shipment").val(),
        U_Broker: $("#U_Broker").val(),
        U_Status: $("#U_Status").val(),
        U_ShipVia: $("#U_ShipVia").val(),
        U_POE: $("#U_POE").val(),
        U_UserTxt1: $("#U_UserTxt1").val(),
        U_UserTxt2: $("#U_UserTxt2").val(),
        U_UserTxt3: $("#U_UserTxt3").val(),
        U_UserTxt4: $("#U_UserTxt4").val(),
        U_ESD: (isNaN(dateU_ESD) == true ? null : (dateU_ESD.getMonth() + 1) + "/" + dateU_ESD.getDate() + "/" + dateU_ESD.getFullYear()),
        U_EDA: (isNaN(dateU_EDA) == true ? null : (dateU_EDA.getMonth() + 1) + "/" + dateU_EDA.getDate() + "/" + dateU_EDA.getFullYear()),
        U_EDW: (isNaN(dateU_EDW) == true ? null : (dateU_EDW.getMonth() + 1) + "/" + dateU_EDW.getDate() + "/" + dateU_EDW.getFullYear()),
        U_ASD: (isNaN(dateU_ASD) == true ? null : (dateU_ASD.getMonth() + 1) + "/" + dateU_ASD.getDate() + "/" + dateU_ASD.getFullYear()),
        U_ADA: (isNaN(dateU_ADA) == true ? null : (dateU_ADA.getMonth() + 1) + "/" + dateU_ADA.getDate() + "/" + dateU_ADA.getFullYear()),
        U_ADW: (isNaN(dateU_ADW) == true ? null : (dateU_ADW.getMonth() + 1) + "/" + dateU_ADW.getDate() + "/" + dateU_ADW.getFullYear()),
        PageKey: $('#Pagekey').val()
    };

    $.ajax({
        url: "/PLM/Shipment/Update",
        async: false,
        type: "POST",
        data: JSON.stringify(ShipmentView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
                location.reload();
            }
            else {
                $('#Loading').modal('hide');
                ErrorPLM(data);
            }
        }
    });

}

function AddPackage()
{
    $.ajax({
        url: '/PLM/Shipment/_AddNewContainer',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#ContainerModalTable').html(data);
    }).fail(function () {
        alert("error");
    });
}

function LoadPackaginSlipModal(pContainerID) {
    $.ajax({
        url: '/PLM/Shipment/_PSlipModalPackage',
        contextType: 'application/html;charset=utf-8',
        data: { pContainerID: pContainerID, pShipmentID: $("#DocEntry").val(), pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#PSlipModalTable').html(data);

        $.ajax({
            url: '/PLM/Shipment/_PSlipModalAvailableItems',
            contextType: 'application/html;charset=utf-8',
            data: { pContainerID: pContainerID, pPageKey: $("#Pagekey").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#PSlipModalAvailableItemsTable').html(data);
        }).fail(function () {
            alert("error");
        });

    }).fail(function () {
        alert("error");
    });

}

function LoadPSlipModalPackageItems(pContainer, pPackageNum) {
    $.ajax({
        url: '/PLM/Shipment/_PSlipModalPackageItems',
        contextType: 'application/html;charset=utf-8',
        data: { pPackageNum: pPackageNum, pContainer: pContainer, pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#PSlipModalPackageItemsTable').html(data);
    }).fail(function () {
        alert("error");
    });
}

function AddItemToPSlip() {
    var mSelectedItems = [];
    var mFailFlag = true;
    $("#PSlipModalAvailableItemsTable :input[type=checkbox]").each(function () {
        var input = $(this);
        if (input.prop('checked') == true) {
            mSelectedItems.push({
                LineId: input.attr('value'),
                Quantity: $("#Selected_PS" + input.attr('value')).val()
            });
        }
    });

    if (mSelectedItems.length <= 0) {
        alert($("#msgSelectItem").val());
        mFailFlag = false;
    }

    if ($('input[name=packageRadio]:checked').val() == "" || $('input[name=packageRadio]:checked').val() == null) {
        alert($("#msgSelectPackage").val());
        mFailFlag = false;
    }

    $("#PSlipModalAvailableItemsTable :input[type=hidden]").each(function () {
        var input = $(this);
        if ($("#Selected_PS" + input.attr('id')).val() == "" || $("#Selected_PS" + input.attr('id')).val() == null) {
            alert($("#msgSelectItemQuantity").val());
            mFailFlag = false;
        }
        else {
            if ($("#Selected_PS" + input.attr('id')).val() > $("#Line_PS_Qty" + input.attr('id')).val()) {
                alert($("#msgSelectItemQuantityGter").val());
                mFailFlag = false;
            }
        }
    });

    if (mFailFlag == true) {
        var ItemToPackageView = {
            ItemsToAdd: mSelectedItems,
            ContainerNum: $('#U_PSlipContainerNum').val(),
            PackageNum: $('input[name=packageRadio]:checked').val(),
            pPageKey: $("#Pagekey").val()
        }

        $.ajax({
            url: "/PLM/Shipment/_AddItemToPSlip",
            async: false,
            type: "POST",
            data: JSON.stringify(ItemToPackageView),
            dataType: "html",
            contentType: "application/json; charset=utf-8",
        }).done(function (data) {
            $('#PSlipModalPackageItemsTable').html(data);
            $.ajax({
                url: "/PLM/Shipment/_UpdatePSlipAvailableItem",
                async: false,
                type: "POST",
                data: { pPageKey: $("#Pagekey").val() },
                dataType: "html",
                contextType: 'application/html;charset=utf-8',
            }).done(function (data) {
                $('#PSlipModalAvailableItemsTable').html(data);
            }).fail(function (data) {
                alert("error");
            });
        }).fail(function (data) {
            alert("error");
        });
    }
}

function AddPSlipPackage() {
    $.ajax({
        url: '/PLM/Shipment/_AddNewPackageSlip',
        contextType: 'application/html;charset=utf-8',
        data: { pContainerID : $('#U_PSlipContainerNum').val(), pPageKey: $("#Pagekey").val()},
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#PSlipModalTable').html(data);
    }).fail(function () {
        alert("error");
    });
}

function LoadDocumentModal(pObjType) {
    mObjType = pObjType;

    $.ajax({
        url: '/PLM/Shipment/_GetDocumentWhitOpenLines',
        contextType: 'application/html;charset=utf-8',
        data: { pObjType: pObjType, pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#DocumentModalTable').html(data);
        if(pObjType == "17")
            $('#LiPO').remove();
        if (pObjType == "22")
            $('#LiSO').remove();
    }).fail(function () {
        alert("error");
    });

}

function LoadDocumentLinesModal(pObjType) {
    
    var mSelectedDocuments = [];

    $("#DocumentModalTable :input[type=checkbox]").each(function () {
        var input = $(this);
        if (input.prop('checked') == true) {
            mSelectedDocuments.push(
                input.attr('value')
            );
        }
    });

    $.ajax({
        url: '/PLM/Shipment/_GetDocumentOpenLines',
        contextType: 'application/html;charset=utf-8',
        data: { pObjType: pObjType, pDocEntryList: mSelectedDocuments, pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#DocumentLinesModalTable').html(data);
        $('#myModalDocument').modal('hide');
        $('#myModalDocumentLines').modal('show');
    }).fail(function () {
        alert("error");
    });

}

function AddDocumentLines()
{
    var mARGNSContainerLineView = [];

    $("#DocumentLinesModalTable :input[type=checkbox]").each(function () {
        var input = $(this);
        if (input.prop('checked') == true) {
            mARGNSContainerLineView.push(
                {
                    U_BaseEntry: $("#DocEntry_"+ input.attr('value')).val(),
                    U_BaseLine: $("#LineNum_" + input.attr('value')).val(),
                    U_ItemCode: $("#ItemCode_" + input.attr('value')).val(),
                    U_ObjType: mObjType,
                    U_LineStatus: "O",
                    DraftLine: { Quantity: $("#Quantity_" + input.attr('value')).val() }
                }  
            );
        }
    });

    mContainerObj = { PageKey: $("#Pagekey").val(), Lines: mARGNSContainerLineView }

    $.ajax({
        url: '/PLM/Shipment/_AddDocumentLines',
        async: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(mContainerObj),
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#TableSPD').html(data);
        $('#myModalDocumentLines').modal('hide');
    }).fail(function () {
        alert("error");
    });
}

function RemoveItemToPackage() {
    var mSelectedItems = [];
    var mFailFlag = true;
    $("#ContainerModalPackageItemsTable :input[type=checkbox]").each(function () {
        var input = $(this);
        if (input.prop('checked') == true) {
            mSelectedItems.push({
                LineId: input.attr('value')
            });
        }
    });

    if (mSelectedItems.length <= 0) {
        alert($("#msgSelectItem").val());
        mFailFlag = false;
    }

    if (mFailFlag == true) {
        var ItemToPackageView = {
            ItemsToAdd: mSelectedItems,
            PackageNum: $('input[name=containerRadio]:checked').val(),
            pPageKey: $("#Pagekey").val()
        }

        $.ajax({
            url: "/PLM/Shipment/_RemoveItemToPackage",
            async: false,
            type: "POST",
            data: JSON.stringify(ItemToPackageView),
            dataType: "html",
            contentType: "application/json; charset=utf-8",
        }).done(function (data) {
            $('#ContainerModalPackageItemsTable').html(data);
            $.ajax({
                url: "/PLM/Shipment/_UpdateAvailableItem",
                async: false,
                type: "POST",
                data: { pPageKey: $("#Pagekey").val() },
                dataType: "html",
                contextType: 'application/html;charset=utf-8',
            }).done(function (data) {
                $('#ContainerModalAvailableItemsTable').html(data);
            }).fail(function (data) {
                alert("error");
            });
        }).fail(function (data) {
            alert("error");
        });
    }
}

function RemoveItemToPSlip() {
    var mSelectedItems = [];
    var mFailFlag = true;
    $("#PSlipModalPackageItemsTable :input[type=checkbox]").each(function () {
        var input = $(this);
        if (input.prop('checked') == true) {
            mSelectedItems.push({
                LineId: input.attr('value')
            });
        }
    });

    if (mSelectedItems.length <= 0) {
        alert($("#msgSelectItem").val());
        mFailFlag = false;
    }

    if (mFailFlag == true) {
        var ItemToPackageView = {
            ItemsToAdd: mSelectedItems,
            ContainerNum: $('#U_PSlipContainerNum').val(),
            PackageNum: $('input[name=packageRadio]:checked').val(),
            pPageKey: $("#Pagekey").val()
        }

        $.ajax({
            url: "/PLM/Shipment/_RemoveItemToPSlip",
            async: false,
            type: "POST",
            data: JSON.stringify(ItemToPackageView),
            dataType: "html",
            contentType: "application/json; charset=utf-8",
        }).done(function (data) {
            $('#PSlipModalPackageItemsTable').html(data);
            $.ajax({
                url: "/PLM/Shipment/_UpdatePSlipAvailableItem",
                async: false,
                type: "POST",
                data: { pPageKey: $("#Pagekey").val() },
                dataType: "html",
                contextType: 'application/html;charset=utf-8',
            }).done(function (data) {
                $('#PSlipModalAvailableItemsTable').html(data);
            }).fail(function (data) {
                alert("error");
            });
        }).fail(function (data) {
            alert("error");
        });
    }
}

function Add() {

    var dateU_ESD = $("#U_ESD").datepicker("getDate");
    var dateU_EDA = $("#U_EDA").datepicker("getDate");
    var dateU_EDW = $("#U_EDW").datepicker("getDate");
    var dateU_ASD = $("#U_ASD").datepicker("getDate");
    var dateU_ADA = $("#U_ADA").datepicker("getDate");
    var dateU_ADW = $("#U_ADW").datepicker("getDate");

    var ShipmentView = {
        DocEntry: $("#DocEntry").val(),
        U_Shipment: $("#U_Shipment").val(),
        U_Broker: $("#U_Broker").val(),
        U_Status: $("#U_Status").val(),
        U_ShipVia: $("#U_ShipVia").val(),
        U_POE: $("#U_POE").val(),
        U_UserTxt1: $("#U_UserTxt1").val(),
        U_UserTxt2: $("#U_UserTxt2").val(),
        U_UserTxt3: $("#U_UserTxt3").val(),
        U_UserTxt4: $("#U_UserTxt4").val(),
        U_ESD: (isNaN(dateU_ESD) == true ? null : (dateU_ESD.getMonth() + 1) + "/" + dateU_ESD.getDate() + "/" + dateU_ESD.getFullYear()),
        U_EDA: (isNaN(dateU_EDA) == true ? null : (dateU_EDA.getMonth() + 1) + "/" + dateU_EDA.getDate() + "/" + dateU_EDA.getFullYear()),
        U_EDW: (isNaN(dateU_EDW) == true ? null : (dateU_EDW.getMonth() + 1) + "/" + dateU_EDW.getDate() + "/" + dateU_EDW.getFullYear()),
        U_ASD: (isNaN(dateU_ASD) == true ? null : (dateU_ASD.getMonth() + 1) + "/" + dateU_ASD.getDate() + "/" + dateU_ASD.getFullYear()),
        U_ADA: (isNaN(dateU_ADA) == true ? null : (dateU_ADA.getMonth() + 1) + "/" + dateU_ADA.getDate() + "/" + dateU_ADA.getFullYear()),
        U_ADW: (isNaN(dateU_ADW) == true ? null : (dateU_ADW.getMonth() + 1) + "/" + dateU_ADW.getDate() + "/" + dateU_ADW.getFullYear()),
        PageKey: $('#Pagekey').val()
    };

    $.ajax({
        url: "/PLM/Shipment/Add",
        async: false,
        type: "POST",
        data: JSON.stringify(ShipmentView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
                location.href = urlRedirect;
            }
            else {
                $('#Loading').modal('hide');
                ErrorPLM(data);
            }
        }
    });

}

function SetBP(pCode) {

    $('#U_Broker').val(pCode);
    $('#VendorModal').modal('hide');

    return true;
}

function ErrorPLM(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}