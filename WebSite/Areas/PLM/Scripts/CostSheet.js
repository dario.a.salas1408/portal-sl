﻿var ListRates;
var RateCurrency = null;

$(document).ready(function () {

    $('#U_Currency').attr("disabled", true);
    $('#U_Description').attr("disabled", true);

    switch (formMode) {
        case "Update":
            $('#Name').attr("disabled", true);
            break;
        case "View":
            $('#Name').attr("disabled", true);
            $("#btnOK").hide();
            break;
        case "Add":
            $('#U_Currency').attr("disabled", false);
            $('#U_Description').attr("disabled", false);
            break;
    }

    $("#UpdateCorrectly").hide();
    $('#alertRates').hide();
    /* Obtengo los rates y setteo el precio de las lineas (Por primera vez en el documento)*/
    updateRates();

    $("#SearchItems").click(function () {

        if (typeof assetListVM_ListItems.dt === 'undefined' || assetListVM_ListItems.dt == null) {
            assetListVM_ListItems.init();
        }
        else {
            assetListVM_ListItems.refresh();
        }

    });

    $("#SearchOperTemplate").click(function () {
        $('#Loading').modal('show');

        $.ajax({
            url: '/PLM/CostSheet/_GetOperationsTemplateTable',
            contextType: 'application/html;charset=utf-8',
            data: { pCode: $("#CodeOperTemplate").val(), pName: $("#NameOperTemplate").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ModalOPTemplateTable").empty().html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            $('#Loading').modal('hide');
            alert("error");
        });
    });

    $("#SearchSchema").click(function () {
        $('#Loading').modal('show');

        $.ajax({
            url: '/PLM/CostSheet/_GetCostSchemaTable',
            contextType: 'application/html;charset=utf-8',
            data: { pCode: $("#CodeSchema").val(), pName: $("#NameSchema").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ModalSchemaTable").empty().html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            $('#Loading').modal('hide');
            alert("error");
        });
    });

    $("#SearchPattern").click(function () {
        $('#Loading').modal('show');

        $.ajax({
            url: '/PLM/CostSheet/_GetPatternTemplateTable',
            contextType: 'application/html;charset=utf-8',
            data: { pCode: $("#CodePattern").val(), pName: $("#NamePattern").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ModalPatternTable").empty().html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            $('#Loading').modal('hide');
            alert("error");
        });
    });

    $("#btnSearchModelList").click(function () {
        $('#Loading').modal('show');

        $.ajax({
            url: '/PLM/CostSheet/_GetModelListSearch',
            contextType: 'application/html;charset=utf-8',
            data: { pCodeModel: $("#txtCodeModel").val(), pNameModel: $("#txtNameModel").val(), pSeasonModel: $("#txtSeasonModel").val(), pGroupModel: $("#txtGroupModel").val(), pBrand: $("#txtBrand").val(), pCollection: $("#txtCollection").val(), pSubCollection: $("#txtSubCollection").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ModalModelList").empty().html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            $('#Loading').modal('hide');
            alert("error");
        });
    });
    
    $("#btnOK").click(function () {
        $('#Loading').modal('show');
        var data = new FormData();
        var mARGNSCostSheetView = {
            Code: $("#Code").val(),
            Name: $("#Name").val(),
            DocEntry: $("#DocEntry").val(),
            U_ModCode: $("#U_ModCode").val(),
            U_Currency: $("#U_Currency").val(),
            U_Description: $("#U_Description").val(),
            U_OTCode: $("#OTm").val(),
            U_SchCode: $("#Cst").val(),
            U_CodeTmpl: $("#PatternTempCode").val(),
            U_PurchPrice: $("#Summary_PurchPrice").val(),
            U_TMaterials: $("#Summary_TMaterials").val(),
            U_TOperations: $("#Summary_TOperations").val(),
            U_IndrCost: $("#Summary_IndrCost").val(),
            U_TCost: $("#Summary_TCost").val(),
            U_Duty: $("#Summary_Duty").val(),
            U_IntTransp: $("#Summary_IntTransp").val(),
            U_Freight: $("#Summary_Freight").val(),
            U_Margin: $("#Summary_Margin").val(),
            U_DDP: $("#Summary_DDP").val(),
            U_CostPercent: $("#Summary_CostPercent").val(),
            U_SlsPercent: $("#Summary_SlsPercent").val(),
            U_StrFactor: $("#Summary_StrFactor").val(),
            U_TMargin: $("#Summary_TMargin").val(),
            U_CIF: $("#Summary_CIF").val(),
            U_FPrice: $("#Summary_FPrice").val(),
            U_UserText1: $("#Summary_UserText1").val(),
            U_UserText2: $("#Summary_UserText2").val(),
            U_UserText3: $("#Summary_UserText3").val(),
            ListMaterial: [],
            ListOperation: [],
            ListSchema: [],
            ListPattern: []
        };

        $("#TableMaterials :input[type=hidden]").each(function () {
            var input = $(this);
            mARGNSCostSheetView.ListMaterial.push({
                "LineId": input.attr('value'),
                "U_ItemCode": $("#Material_ItemCode" + input.attr('value')).val(),
                "U_ItemName": $("#Material_ItemName" + input.attr('value')).val(),
                "U_Quantity": $("#Material_Quantity" + input.attr('value')).val(),
                "U_CxT": ($('#Material_CxT' + input.attr('value')).prop('checked') == true ? "Y" : "N"),
                "U_CxS": ($('#Material_CxS' + input.attr('value')).prop('checked') == true ? "Y" : "N"),
                "U_CxC": ($('#Material_CxC' + input.attr('value')).prop('checked') == true ? "Y" : "N"),
                "U_CxGT": ($('#Material_CxGT' + input.attr('value')).prop('checked') == true ? "Y" : "N"),
                "U_UoM": $("#Material_UoM" + input.attr('value')).val(),
                "U_Whse": $("#Material_Wh" + input.attr('value')).val(),
                "U_OcrCode": $("#Material_DistRule" + input.attr('value')).val(),
                "U_PList": $("#Material_PList" + input.attr('value')).val(),
                "U_Currency": $("#Material_Currency" + input.attr('value')).val(),
                "U_UPrice": $("#Material_UnitP" + input.attr('value')).val(),
                "U_PVendor": $("#Material_Pvendor" + input.attr('value')).val(),
                "U_Comments": $("#Material_Remarks" + input.attr('value')).val(),
                "IsNew": $("#Material_IsNew" + input.attr('value')).val()
            });
        });

        $("#TableSchemas :input[type=hidden]").each(function () {
            var input = $(this);
            mARGNSCostSheetView.ListSchema.push({
                "LineId": input.attr('value'),
                "U_ItemCode": $("#Schema_ItemCode" + input.attr('value')).val(),
                "U_ItemName": $("#Schema_ItemName" + input.attr('value')).val(),
                "U_Percent": $("#Schema_Percent" + input.attr('value')).val(),
                "U_PList": $("#Schema_PList" + input.attr('value')).val(),
                "U_Currency": $("#Schema_Currency" + input.attr('value')).val(),
                "U_Amount": $("#Schema_Amount" + input.attr('value')).val(),
                "IsNew": $("#Schema_IsNew" + input.attr('value')).val()

            });
        });

        $("#TableOperations :input[type=hidden]").each(function () {
            var input = $(this);
            mARGNSCostSheetView.ListOperation.push({
                "LineId": input.attr('value'),
                "U_ItemCode": $("#Operation_ItemCode" + input.attr('value')).val(),
                "U_ItemName": $("#Operation_ItemName" + input.attr('value')).val(),
                "U_Quantity": $("#Operation_Quantity" + input.attr('value')).val(),
                "U_UoM": $("#Operation_UoM" + input.attr('value')).val(),
                "U_PList": $("#Operation_PList" + input.attr('value')).val(),
                "U_Currency": $("#Operation_Currency" + input.attr('value')).val(),
                "U_Price": $("#Operation_UnitP" + input.attr('value')).val(),
                "IsNew": $("#Operation_IsNew" + input.attr('value')).val()

            });
        });

        $("#TablePattern :input[type=hidden]").each(function () {
            var input = $(this);
            mARGNSCostSheetView.ListPattern.push({
                "LineId": input.attr('value'),
                "U_PattCode": $("#Pattern_Code" + input.attr('value')).val(),
                "U_Desc": $("#Pattern_Desc" + input.attr('value')).val(),
                "U_ItemCode": $("#Pattern_ItemCode" + input.attr('value')).val(),
                "U_ItemName": $("#Pattern_ItemName" + input.attr('value')).val(),
                "U_Quantity": $("#Pattern_Quantity" + input.attr('value')).val(),
                "U_UoM": $("#Pattern_UoM" + input.attr('value')).val(),
                "U_PattQty": $("#Pattern_PattQty" + input.attr('value')).val(),
                "IsNew": $("#Pattern_IsNew" + input.attr('value')).val()
            });
        });

        var mPageKey = $('#Pagekey').val();

        data.append('ARGNSCostSheetView', JSON.stringify(mARGNSCostSheetView));
        data.append('pPageKey', JSON.stringify(mPageKey));

        switch (formMode) {
            case "Update":
                $.ajax({
                    url: "/PLM/CostSheet/UpdateCostSheet",
                    async: false,
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data, textStatus, jqXHR) {
                        if (data == "Ok") {
                            $('#Loading').modal('hide');
                            $("#UpdateCorrectly").show("slow");
                            $("#UpdateCorrectly").delay(1500).slideUp(1000);
                        }
                        else {
                            $('#Loading').modal('hide');
                            ErrorPDM(data);
                        }
                    }
                });
                break;
            case "Add":
                $.ajax({
                    url: "/PLM/CostSheet/AddCostSheet",
                    async: false,
                    type: "POST",
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data, textStatus, jqXHR) {
                        if (data.ServiceAnswer == "Ok") {
                            location.href = data.RedirectUrl;
                            $('#Loading').modal('hide');
                        }
                        else {
                            $('#Loading').modal('hide');
                            ErrorPDM(data.ErrorMsg);
                        }
                    }
                });
                break;
        }
    });
});

function ErrorPDM(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function updateRates() {
    var dateNow = new Date();
    //Obtengo los rates para el dia del DocDate
    $.ajax({
        url: '/PLM/CostSheet/UpdateRateList',
        data: { DocDate: (dateNow.getMonth() + 1) + "/" + dateNow.getDate() + "/" + dateNow.getFullYear(), pPageKey: $('#Pagekey').val() },
        type: 'POST',
        dataType: 'json'
    }).done(function (data) {
        ListRates = data;
        $.grep(ListRates, function (v) {
            if (v.Currency.replace(/\s+/g, '') == $("#U_Currency").val() && v.Local == false && v.System == false) {
                if (v.Rate == 0) {
                    total = 0;
                    $('#alertRates').show();
                    $('#btnOk').attr("disabled", true);
                }
                else {
                    RateCurrency = v.Rate;
                    $("#TableMaterials :input[type=hidden]").each(function () {
                        var input = $(this);
                        ChangeValueMaterial(input.attr('value'));
                    });
                    $("#TableOperations :input[type=hidden]").each(function () {
                        var input = $(this);
                        ChangeValueOperation(input.attr('value'));
                    });
                    $('#alertRates').hide();
                    $('#btnOk').attr("disabled", false);
                }
            }
        });
    });
}

function ChangeValueMaterial(Id) {

    var count = 0;
    var price = 0;
    var total = 0;
    if ($('#Material_Quantity' + Id).val() != "") {
        count = $('#Material_Quantity' + Id).val();
    }

    if ($('#Material_UnitP' + Id).val() != "") {
        price = $('#Material_UnitP' + Id).val();
    }

    $.grep(ListRates, function (v) {
        if (v.Currency.replace(/\s+/g, '') == $('#Material_Currency' + Id).val() && v.Local == false && v.System == false) {
            if (v.Rate == 0) {
                total = 0;
                $('#alertRates').show();
                $('#btnOk').attr("disabled", true);
            }
            else {
                total = ((price / RateCurrency) * v.Rate) * count;
                $('#alertRates').hide();
                $('#btnOk').attr("disabled", false);
            }
        }
    });

    $("#Material_Total" + Id).html("");
    $("#Material_Total" + Id).val(total.toFixed(2));
    ChangeValueSummary();
}

function ChangeValueOperation(Id) {

    var count = 0;
    var price = 0;
    var total = 0;
    if ($('#Operation_Quantity' + Id).val() != "") {
        count = $('#Operation_Quantity' + Id).val();
    }

    if ($('#Operation_UnitP' + Id).val() != "") {
        price = $('#Operation_UnitP' + Id).val();
    }

    $.grep(ListRates, function (v) {
        if (v.Currency.replace(/\s+/g, '') == $('#Operation_Currency' + Id).val() && v.Local == false && v.System == false) {
            if (v.Rate == 0) {
                total = 0;
                $('#alertRates').show();
                $('#btnOk').attr("disabled", true);
            }
            else {
                total = ((price / RateCurrency) * v.Rate) * count;
                $('#alertRates').hide();
                $('#btnOk').attr("disabled", false);
            }
        }
    });

    $("#Operation_Total" + Id).html("");
    $("#Operation_Total" + Id).val(total.toFixed(2));
    ChangeValueSummary();
}

function ChangeValueSummary() {
    var totalMaterial = 0;
    var totalOperation = 0;
    $("#TableMaterials :input[type=hidden]").each(function () {
        var input = $(this);
        totalMaterial += Number($("#Material_Total" + input.attr('value')).val());
    });
    $('#Summary_TMaterials').val(totalMaterial.toFixed(2));

    $("#TableOperations :input[type=hidden]").each(function () {
        var input = $(this);
        totalOperation += Number($("#Operation_Total" + input.attr('value')).val());
    });
    $('#Summary_TOperations').val(totalOperation.toFixed(2));

    var totalCost = 0;
    totalCost += Number($('#Summary_PurchPrice').val());
    totalCost += Number($('#Summary_TMaterials').val());
    totalCost += Number($('#Summary_TOperations').val());
    totalCost += Number($('#Summary_IndrCost').val());
    $('#Summary_TCost').val(totalCost.toFixed(2));

    var totalSugestedPrice = totalCost.toFixed(2);
    if(Number($('#Summary_CostPercent').val()) != 0)
    {
        totalSugestedPrice = Number(totalSugestedPrice) + ((totalSugestedPrice * Number($('#Summary_CostPercent').val())) / 100)
    }
    if (Number($('#Summary_SlsPercent').val()) != 0) {
        totalSugestedPrice = totalSugestedPrice / (1 - (Number($('#Summary_SlsPercent').val()) / 100))
    }
    if (Number($('#Summary_StrFactor').val()) != 0) {
        totalSugestedPrice = totalSugestedPrice * Number($('#Summary_StrFactor').val())
    }
    $('#Summary_SP').val(totalSugestedPrice.toFixed(2));
}

function AddRow(pTableId, pTableName) {
    $('#Loading').modal('show');

    var nextId = NextRowId("TR_" + pTableName);
    $.ajax({
        url: '/PLM/CostSheet/_GetRow',
        contextType: 'application/html;charset=utf-8',
        data: { pTable: pTableName, pNextId: nextId, pPageKey: $('#Pagekey').val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#Loading').modal('hide');
        $('#' + pTableId + ' tr:last').after(data);
    }).fail(function () {
        $('#Loading').modal('hide');
        alert("error");
    });
}

function NextRowId(pSearchId) {
    var mListTr = $("tr[id^=" + pSearchId + "]");
    auxMax = 0;
    for (i = 0; i < mListTr.length ; i++) {
        if (mListTr[i].id.substring(pSearchId.length) > auxMax) {
            auxMax = mListTr[i].id.substring(pSearchId.length);
        }
    }

    return parseInt(auxMax) + 1;
}

function DeleteRow(pTrId) {
    $("#" + pTrId).remove();
}

function GetItemsModel(pRowNum, pFromTab) {
    if (pFromTab == "Materials") {
        $.ajax({
            url: '/PLM/CostSheet/GetItemsModel',
            contextType: 'application/html;charset=utf-8',
            data: { pRowNum: pRowNum },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ModalItemsTable").html(data);
        });
    }
    if (pFromTab == "Patterns") {
        $("#ModalItemsPatternTable").html("");
        $('#LoadItemsPattern').modal('show');
        mListItemsInMaterialsRow = [];
        $("#TableMaterials :input[type=hidden]").each(function () {
            var input = $(this);
            mListItemsInMaterialsRow.push($("#Material_ItemCode" + input.attr('value')).val());
        });
        $.ajax({
            url: '/PLM/CostSheet/GetItemsPattern',
            contextType: 'application/html;charset=utf-8',
            data: { pRowNum: pRowNum, pListItemsInMaterialsRow: mListItemsInMaterialsRow },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#LoadItemsPattern').modal('hide');
            $("#ModalItemsPatternTable").html(data);
        });
    }
}

function SetItemInRow(pRowNum, pItemCode, pItemName) {
    $('#Material_ItemCode' + pRowNum).val(pItemCode);
    $('#Material_ItemName' + pRowNum).val(pItemName);
    $('#ModalItems').modal('hide');
}

function SetModel(pModCode) {
    $('#U_ModCode').val(pModCode);
    $('#ModelTemplate').modal('hide');
} 

function SetItemInPatternRow(pRowNum, pItemCode, pItemName) {
    $('#Pattern_ItemCode' + pRowNum).val(pItemCode);
    $('#Pattern_ItemName' + pRowNum).val(pItemName);
    $('#ModalItemsPattern').modal('hide');
}

function SetOperationTemplate(pOperTemplateCode, pU_OTCode) {

    $('#Loading').modal('show');

    $.ajax({
        url: '/PLM/CostSheet/_GetOperTemplateLines',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $('#Pagekey').val(), pCode: pOperTemplateCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#OTm").val(pU_OTCode);
        $("#TableOperTemplateBody").empty().html(data);
        $('#Loading').modal('hide');
        $('#ModalOPTemplate').modal('hide');
    }).fail(function () {
        $('#Loading').modal('hide');
        alert("error");
    });
}

function SetSchema(pSchemaCode, pU_SchCode) {

    $('#Loading').modal('show');

    $.ajax({
        url: '/PLM/CostSheet/_GetSchemaLines',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $('#Pagekey').val(), pCode: pSchemaCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#Cst").val(pU_SchCode);
        $("#TableSchemasBody").empty().html(data);
        $('#Loading').modal('hide');
        $('#ModalSchema').modal('hide');
    }).fail(function () {
        $('#Loading').modal('hide');
        alert("error");
    });
}

function SetPatternTemplate(pPatternCode, pU_CodePatternTemp) {

    $('#Loading').modal('show');

    $.ajax({
        url: '/PLM/CostSheet/_GetPatternTemplateLines',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $('#Pagekey').val(), pCode: pPatternCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#PatternTempCode").val(pU_CodePatternTemp);
        $("#TablePatternBody").empty().html(data);
        $('#Loading').modal('hide');
        $('#ModalPattern').modal('hide');
    }).fail(function () {
        $('#Loading').modal('hide');
        alert("error");
    });
}