﻿var datePostingDate;
$(document).ready(function () {
    $('.input-group.date').datepicker({});

    $("#btnSearch").click(function () {
        datePostingDate = $("#txtPostingDate").datepicker("getDate");
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
        $.ajax({
            url: '/PLM/PDC/PDCList',
            contextType: 'application/html;charset=utf-8',
            data: { pPD: (isNaN(datePostingDate) == true ? null : (datePostingDate.getMonth() + 1) + "/" + datePostingDate.getDate() + "/" + datePostingDate.getFullYear()), pCode: $("#txtCodePDC").val(), pEmployee: $("#txtEmployee").val(), pShift: $("#txtShifts").val(), pVendor: $("#txtVendor").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#BodyPDC").html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            alert("error");
        });
    });

});