﻿var repeatedCodeFlag = false;
$(document).ready(function () {
    $('.input-group.date').datepicker({});
    $("#Code").attr("disabled", "disabled");

    $("#btnOk").click(function () {

        var mDate = $("#U_Date").val();
        if (mDate === "") {          
            ErrorPO("Please, enter Posting Date.");
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
        $("#Loading").show('slow');  
        var dateU_Date = $("#U_Date").datepicker("getDate");
        var ARGNSPDCView = {
            U_Date: (isNaN(dateU_Date) == true ? null : (dateU_Date.getMonth() + 1) + "/" + dateU_Date.getDate() + "/" + dateU_Date.getFullYear()),
            U_empID: $("#U_empID").val(),
            U_ShiftCode: $("#U_ShiftCode").val(),
            U_Vendor: $("#U_Vendor").val(),
            U_MinPres: $("#U_MinPres").val(),
            U_UnprodMins: $('#U_UnprodMins').val(),
            Lines: []
        };

        var rowCounter = 0;
        $("#TableLines :input[type=hidden]").each(function () {
            var input = $(this); // This is the jquery object of the input, do what you will
            ARGNSPDCView.Lines.push({
                "LineId": input.attr('id'),
                "U_BadQty": $("#bq" + input.attr('id')).val(),
                "U_CodeBars": $("#cb" + input.attr('id')).val(),
                "U_Color": $("#color" + input.attr('id')).val(),
                "U_Model": $("#mc" + input.attr('id')).val(),
                "U_OperCode": $("#oc" + input.attr('id')).val(),
                "U_ProdOrd": $("#ct" + input.attr('id')).val(),
                "U_Qty": $("#qty" + input.attr('id')).val(),
                "U_ResName": $("#resource" + input.attr('id')).val(),
                "U_Scale": $("#scale" + input.attr('id')).val(),
                "U_Size": $("#size" + input.attr('id')).val(),
                "U_Stage": $("#stage" + input.attr('id')).val(),
                "U_Variable": $("#variable" + input.attr('id')).val(),
                "U_WorkCtr": $("#wc" + input.attr('id')).val()
            });
            rowCounter++;
        });

        if (rowCounter > 0) {
            $.ajax({
                url: "/PLM/PDC/Add",
                async: false,
                type: "POST",
                data: JSON.stringify(ARGNSPDCView),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, textStatus, jqXHR) {
                    if (data == "Ok") {
                        $('#Loading').modal('hide');
                        location.href = '/PLM/PDC/Index';
                    }
                    else {
                        $('#Loading').modal('hide');
                        ErrorPO(data);
                    }
                }
            });
        }
        else {
            $('#Loading').modal('hide');
            $("#EnterCode").val("");
            $("#EnterCode").focus();
            $("#errorMessage").empty().append("<strong>  You have not entered any line </strong>");
            $("#errorMessage").show('slow');
            $('#errorBox').modal({
                backdrop: 'static',
                keyboard: true
            });
        }
    });
});

$(document).keypress(function (e) {
    if (e.which == 13 && $("#EnterCode").is(':focus')) {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
        var selectedVal = "";
        var selected = $("input[type='radio'][name='inputMethod']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
        }
        if (selectedVal == "CB") {
            repeatedCodeFlag = false;
            $("#TableLines :input[type=hidden]").each(function () {
                var input = $(this); // This is the jquery object of the input, do what you will
                if ($("#cb" + input.attr('id')).val() == $("#EnterCode").val())
                    repeatedCodeFlag = true;
            });
            if (repeatedCodeFlag == false) {
                $.ajax({
                    url: '/PLM/PDC/_GetPDCLineCodeBar',
                    traditional: true,
                    contextType: 'application/html;charset=utf-8',
                    data: { pCodeBar: $("#EnterCode").val() },
                    type: 'POST',
                    dataType: 'html'
                }).done(function (data) {
                    $('#Loading').modal('hide');
                    $("#EnterCode").val("");
                    $("#EnterCode").focus();
                    $("#ItemsSelect").html(data);
                });
            }
            else {
                $('#Loading').modal('hide');
                $("#EnterCode").val("");
                $("#EnterCode").focus();
                $("#errorMessage").empty().append("<strong>  This Code Bar was already entered </strong>");
                $("#errorMessage").show('slow');
                $('#errorBox').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            }
        }

        if (selectedVal == "CT") {
            var rowCounter = 0;
            $("#TableLines :input[type=hidden]").each(function () {
                var input = $(this); // This is the jquery object of the input, do what you will
                rowCounter++;
            });
            if (rowCounter == 0) {
                $.ajax({
                    url: '/PLM/PDC/_GetPDCLinesCutTick',
                    contextType: 'application/html;charset=utf-8',
                    data: { pCutTick: $("#EnterCode").val() },
                    type: 'POST',
                    dataType: 'html'
                }).done(function (data) {
                    $('#Loading').modal('hide');
                    $("#EnterCode").val("");
                    $("#EnterCode").focus();
                    if (data != "") {
                        $("#ItemsSelect").html(data);
                    }
                });
            }
            else {
                $('#Loading').modal('hide');
                $("#EnterCode").val("");
                $("#EnterCode").focus();
                $("#errorMessage").empty().append("<strong> There already entered lines </strong>");
                $("#errorMessage").show('slow');
                $('#errorBox').modal({
                    backdrop: 'static',
                    keyboard: true
                });
            }
        }
    }
});

//function Save() {
//    $('#Loading').modal({
//        backdrop: 'static',
//        keyboard: true
//    });

//    var dateU_Date = $("#U_Date").datepicker("getDate");

//    var ARGNSPDCView = {
//        U_Date: (isNaN(dateU_Date) == true ? null : (dateU_Date.getMonth() + 1) + "/" + dateU_Date.getDate() + "/" + dateU_Date.getFullYear()),
//        U_empID: $("#U_empID").val(),
//        U_ShiftCode: $("#U_ShiftCode").val(),
//        U_Vendor: $("#U_Vendor").val(),
//        U_MinPres: $("#U_MinPres").val(),
//        U_UnprodMins: $('#U_UnprodMins').val(),
//        Lines: [],
//    };

//    $("#TableItemsForm :input[type=hidden]").each(function () {
//        var input = $(this); // This is the jquery object of the input, do what you will

//        ARGNSPDCView.Lines.push({
//            "LineId": input.attr('id'),
//            "U_BadQty": $("#bq" + input.attr('id')).val(),
//            "U_CodeBars": $("#cb" + input.attr('id')).val(),
//            "U_Color": $("#color" + input.attr('id')).val(),
//            "U_Model": $("#mc" + input.attr('id')).val(),
//            "U_OperCode": $("#oc" + input.attr('id')).val(),
//            "U_ProdOrd": $("#ct" + input.attr('id')).val(),
//            "U_Qty": $("#qty" + input.attr('id')).val(),
//            "U_ResName": $("#resource" + input.attr('id')).val(),
//            "U_Scale": $("#scale" + input.attr('id')).val(),
//            "U_Size": $("#size" + input.attr('id')).val(),
//            "U_Stage": $("#stage" + input.attr('id')).val(),
//            "U_Variable": $("#variable" + input.attr('id')).val(),
//            "U_WorkCtr": $("#wc" + input.attr('id')).val()
//        });

//    });

//    $.ajax({
//        url: "/PLM/PDC/Add",
//        async: false,
//        type: "POST",
//        data: JSON.stringify(ARGNSPDCView),
//        dataType: "json",
//        contentType: "application/json; charset=utf-8",
//        success: function (data, textStatus, jqXHR) {
//            if (data == "Ok") {
//                $('#Loading').modal('hide');
//                location.href = '/PLM/PDC/Index';
//            }
//            else {
//                ErrorPO(data);
//            }
//        }
//    });
//}

function ErrorPO(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function DeleteRow(Id) {

    $.post("/PLM/PDC/DeleteRow", {
        RowId: Id
    })
       .success(function (data) {
           if (data == "Ok") {
               $("#tr" + Id).remove();
           }
           else {
               ErrorPO(data);
           }
       });

}