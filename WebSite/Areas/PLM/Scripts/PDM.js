﻿$(document).ready(function () {
    $("#LoadingComment").show();

    $('#file-1').fileinput('clear');
    $('#UpdateFile').fileinput('clear');

    $("#alertLog").hide();
    $("#alertAtt").hide();
    $("#UpdateCorrectly").hide();


    //Inicializar PopUp  ----------------------------------------------------------------
    $('#ColorModal').on('show.bs.modal', function (e) {
        $("#ModalBodyColor").html("");
        $("#LoadColor").hide();
    });

    $('#ScaleModal').on('show.bs.modal', function (e) {
        $("#ModalBodyScale").html("");
        $("#LoadScale").hide();
    });

    $('#VarModal').on('show.bs.modal', function (e) {
        $("#ModalBodyVar").html("");
        $("#LoadVar").hide();
    });

    $('#WhModal').on('show.bs.modal', function (e) {
        $("#ModalBodyWh").html("");
        $("#LoadWh").hide();
    });

    $('#SecondaryWhModal').on('show.bs.modal', function (e) {
        $("#ModalBodySecondaryWh").html("");
        $("#LoadSecondaryWh").hide();
    });
    //------------------------------------------------------------------------------------



    //-------------- Search Color -----------------------------------------------------
    $("#SearchColor").click(function () {

        var mCode;
        var mName;

        if ($('#ckCode').is(':checked')) {
            mCode = $('#txtSearchColor').val();
        }

        if ($('#ckName').is(':checked')) {
            mName = $('#txtSearchColor').val();
        }

        $("#ModalBodyColor").show('slow');

        $.ajax({
            url: '/PLM/PDM/ColorList',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $('#Pagekey').val(), txtColorCode: mCode, txtColorName: mName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadColor").hide();
            $("#ModalBodyColor").html(data);

        }).fail(function () {
            alert("error");
        });

    });

    $("#btnChooseColor").click(function () {
        var data = new FormData();
        ARGNSModelColor = [];
        $("#ModalBodyColor :input[type=hidden]").each(function () {
            var input = $(this);

            if ($("#active" + input.attr('id')).prop('checked')) {

                ARGNSModelColor.push({
                    "U_ColCode": $("#U_ColCode" + input.attr('id')).html(),
                    "ColorDesc": $("#U_ColDesc" + input.attr('id')).html(),
                    "U_Argb": $("#U_Argb" + input.attr('id')).val()
                });
            }
        });

        if (ARGNSModelColor.length == 0) {
            return;
        }

        var mPageKey = $('#Pagekey').val();
        data.append('ARGNSModelColor', JSON.stringify(ARGNSModelColor));
        data.append('pPageKey', JSON.stringify(mPageKey));

        $.ajax({
            url: '/PLM/PDM/AddColor',
            type: 'POST',
            contentType: false,
            processData: false,
            data: data
        }).done(function (data) {

            $("#ColorTableItems").html(data);
            $('#ColorModal').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });


    //-------------- Search Escala -----------------------------------------------------
    $("#SearchScale").click(function () {

        var mCode;
        var mName;

        if ($('#ckCode').is(':checked')) {
            mCode = $('#txtSearchColor').val();
        }

        if ($('#ckName').is(':checked')) {
            mName = $('#txtSearchScale').val();
        }

        $("#ModalBodyScale").show('slow');

        $.ajax({
            url: '/PLM/PDM/ScaleList',
            contextType: 'application/html;charset=utf-8',
            data: { txtScaleCode: mCode, txtScaleName: mName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadScale").hide();
            $("#ModalBodyScale").html(data);

        }).fail(function () {
            alert("error");
        });

    });

    $("#btnChooseScale").click(function () {
        var data = new FormData();
       var  ARGNSModelSize = [];
       var mScale = [];

        $("#ModalBodyScale").find("input:checked").each(function (i, ob) {
            ARGNSModelSize.push($(ob).val());
            mScale.push($("#scalecode" + $(ob).val()).val());            
        });

        if (ARGNSModelSize.length == 0) {
            return;
        }

        $("#U_SclCode").val(mScale);       
        data.append('ARGNSModelSize', JSON.stringify(ARGNSModelSize));
        data.append('pPageKey', JSON.stringify($('#Pagekey').val()));
        data.append('pScaleCode', JSON.stringify(mScale));

        $.ajax({
            url: '/PLM/PDM/AddScale',
            type: 'POST',
            contentType: false,
            processData: false,
            data: data
        }).done(function (data) {

            $("#ScaleTableItems").html(data);
            $('#ScaleModal').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });



    //-------------- Search Variable -----------------------------------------------------
    $("#SearchVar").click(function () {

        var mCode;
        var mName;

        if ($('#ckCode').is(':checked')) {
            mCode = $('#txtSearchVar').val();
        }

        if ($('#ckName').is(':checked')) {
            mName = $('#txtSearchVar').val();
        }

        $("#ModalBodyVar").show('slow');

        $.ajax({
            url: '/PLM/PDM/VariableList',
            contextType: 'application/html;charset=utf-8',
            data: { txtVarCode: mCode, txtVarName: mName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadVar").hide();
            $("#ModalBodyVar").html(data);

        }).fail(function () {
            alert("error");
        });

    });

    $("#btnChooseVar").click(function () {
        var data = new FormData();
        ARGNSModelVar = [];
        $("#ModalBodyVar :input[type=hidden]").each(function () {
            var input = $(this);

            if ($("#varactive" + input.attr('id')).prop('checked')) {

                ARGNSModelVar.push({
                    "U_VarCode": $("#U_VarCode" + input.attr('id')).html(),
                    "U_VarDesc": $("#U_VarDesc" + input.attr('id')).html()
                });
            }
        });

        if (ARGNSModelVar.length == 0) {
            return;
        }

        var mPageKey = $('#Pagekey').val();
        data.append('ARGNSModelVar', JSON.stringify(ARGNSModelVar));
        data.append('pPageKey', JSON.stringify(mPageKey));

        $.ajax({
            url: '/PLM/PDM/AddVariable',
            type: 'POST',
            contentType: false,
            processData: false,
            data: data
        }).done(function (data) {

            $("#VariableTableItems").html(data);
            $('#VarModal').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

    //-------------- Search Warehouse -----------------------------------------------------
    $("#SearchWh").click(function () {

        var mCode;
        var mName;

        if ($('#ckCodeWh').is(':checked')) {
            mCode = $('#txtSearchWh').val();
        }

        if ($('#ckNameWh').is(':checked')) {
            mName = $('#txtSearchWh').val();
        }

        $("#ModalBodyWh").show('slow');

        $.ajax({
            url: '/PLM/PDM/_GetWarehouseListSearch',
            contextType: 'application/html;charset=utf-8',
            data: { pWhsCode: mCode, pWhsName: mName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadWh").hide();
            $("#ModalBodyWh").html(data);

        }).fail(function () {
            alert("error");
        });

    });

    $("#btnChooseWh").click(function () {
        $("#U_MainWhs").val($('input[name=WhRadio]:checked').val());
        $('#WhModal').modal('hide');
    });

    //-------------- Search Secondary Warehouse -----------------------------------------------------

    $("#SearchSecondaryWh").click(function () {

        var mCode;
        var mName;

        if ($('#ckCodeSecondaryWh').is(':checked')) {
            mCode = $('#txtSearchSecondaryWh').val();
        }

        if ($('#ckNameSecondaryWh').is(':checked')) {
            mName = $('#txtSearchSecondaryWh').val();
        }

        $("#ModalBodySecondaryWh").show('slow');

        $.ajax({
            url: '/PLM/PDM/_GetSecondaryWarehouseListSearch',
            contextType: 'application/html;charset=utf-8',
            data: { pWhsCode: mCode, pWhsName: mName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadSecondaryWh").hide();
            $("#ModalBodySecondaryWh").html(data);

        }).fail(function () {
            alert("error");
        });

    });

    $("#btnChooseSecondaryWh").click(function () {
        var mSelectedWh = "";
        var counter = 0;
        $("#SecondaryWhTable :input[type=checkbox]").each(function () {
            var input = $(this);
            if (input.prop('checked') == true) {
                if (counter == 0)
                {
                    mSelectedWh = input.attr('value');
                    counter = 1;
                }
                else
                {
                    mSelectedWh += "," + input.attr('value');
                }
            }
        });
        $("#U_WhsNewI").val(mSelectedWh);
        $('#SecondaryWhModal').modal('hide');
    });

    //-------------- Control De visibilidad -----------------------------------------------------
    $("#U_ATGrp").change(function () {

        $.ajax({
            url: '/PLM/PDM/GetSegmentgation',
            contextType: 'application/html;charset=utf-8',
            data: { pCode: $("#U_ATGrp").val(), pPageKey: $('#Pagekey').val() },
            type: 'POST',
            dataType: 'json'
        }).done(function (data) {

            VisibilityControl(data);

        }).fail(function () {
            alert("error");
        });

    });

    //-----------------------------------------------------------------------------------

    $("#UpdateFileImg").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-lg",
        fileType: "any",
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
    });

    //Emp-------------------------------------------------------------------------------

    var EmpItems = [];


    $.ajax({
        url: '/PLM/PDM/_GetEmployeeList',
        type: 'POST',
        data: {},
        traditional: true,
        cache: false,
        dataType: 'json',
        success: function (data) {
            $.each(data, function (key) {
                EmpItems.push({
                    value: data[key].Value,
                    label: data[key].Text
                });

                $("#U_Designer").autocomplete({
                    source: EmpItems
                });

            });
        }, error: function (xhr) {
            ErrorCalls(xhr.status)

        },
    });

    //----------------------------------------------------------------------------------

    //WHS-------------------------------------------------------------------------------

    //var WhItems = [];

    //function split(val) {
    //    return val.split(/,\s*/);
    //}

    //function extractLast(term) {
    //    return split(term).pop();
    //}

    //$.ajax({
    //    url: '/PLM/PDM/_GetWarehouseList',
    //    type: 'POST',
    //    data: {},
    //    traditional: true,
    //    cache: false,
    //    dataType: 'json',
    //    success: function (data) {
    //        $.each(data, function (key) {
    //            WhItems.push({
    //                value: data[key].Value,
    //                label: data[key].Text
    //            });

    //            $("#U_WhsNewI").autocomplete({
    //                minLength: 0,
    //                source: function (request, response) {
    //                    response($.ui.autocomplete.filter(
    //                      WhItems, extractLast(request.term)));
    //                },
    //                focus: function () {
    //                    return false;
    //                },
    //                select: function (event, ui) {
    //                    var terms = split(this.value);
    //                    terms.pop();
    //                    terms.push(ui.item.value);
    //                    terms.push("");
    //                    this.value = terms.join(", ");
    //                    return false;
    //                }
    //            });

    //            $("#U_MainWhs").autocomplete({
    //                source: WhItems
    //            });

    //        });
    //    }, error: function (xhr) {
    //        ErrorCalls(xhr.status)

    //    },
    //});



    //----------------------------------------------------------------------------------


    $("#UploadFiles").click(function () {

        if ($("#txtLogComment").val() == "") {
            $("#alertLog").show();
            $("#txtLogComment").focus();
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var fileInput = document.getElementById("UpdateFile");
        var mAttachList = [];
        var data = new FormData();
        var mModCode = $("#Code").val();
        var mUModCode = $("#U_ModCode").val();

        if (fileInput.files.length > 0) {
            for (var i = 0; i < fileInput.files.length; i++) {
                data.append(fileInput.files[i].name, fileInput.files[i]);
            }
        }

        data.append('mCode', mModCode);
        data.append('mUModCode', mUModCode);
        data.append('mComment', $("#txtLogComment").val());

        $.ajax({
            type: 'POST',
            url: '/PLM/PDM/UploadFile',
            contentType: false,
            processData: false,
            data: data,
        }).done(function (result) {

            if (result == "Ok") {
                $('#UpdateFile').fileinput('clear');

                $("#LoadingComment").show();

                $.ajax({
                    url: '/PLM/PDM/ListComment',
                    contextType: 'application/html;charset=utf-8',
                    data: { txtCodeModel: $("#U_ModCode").val() },
                    type: 'POST',
                    dataType: 'html'
                }).done(function (data) {
                    $('#UpdateFile').fileinput('clear');
                    $("#alertLog").hide();
                    $("#txtLogComment").val("");
                    $("#LoadingComment").hide();
                    $("#PanelComments").html(data);
                    $('#Loading').modal('hide');

                }).fail(function () {
                    alert("error");
                });
            }
            else {
                $('#Loading').modal('hide');
                ErrorPDM(result);
            }

        }).fail(function () {
            ErrorPDM("Error....");
        });

    });

    $("#UploadAttach").click(function () {

        var fileInput = document.getElementById("file-1");
        var mAttachList = [];
        var data = new FormData();
        var mModCode = $("#Code").val();
        var mUModCode = $("#U_ModCode").val();

        $("#alertAtt").hide();
        if (fileInput.files.length == 0) {
            $("#alertAtt").show();
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        if (fileInput.files.length > 0) {
            for (var i = 0; i < fileInput.files.length; i++) {
                data.append(fileInput.files[i].name, fileInput.files[i]);
            }
        }

        var mPageKey = $('#Pagekey').val();
        
        data.append('pPageKey', mPageKey);
        data.append('mCode', mModCode);
        data.append('mUModCode', mUModCode);

        $.ajax({
            type: 'POST',
            url: '/PLM/PDM/UploadAttachment',
            contentType: false,
            processData: false,
            data: data,
        }).done(function (result) {
            $('#file-1').fileinput('clear');
            $("#FilesItems").html(result);
            $('#Loading').modal('hide');
            $("#alertAtt").hide();

        }).fail(function () {
            alert("error");
        });

    });

    $('.input-group.date').datepicker({});

    $("#formPDM").validate({
        rules: {
            U_DocNumb: "required",
            U_ModCode: "required",
            U_ModDesc: "required",
            U_ATGrp: "required",
        },
        /*messages: {
            *CardCode: $("#CC").text() + $("#REQ").text(),
            CardName: $("#Name").text() + $("#REQ").text(),
            CardType: $("#Type").text() + $("#REQ").text(),
            Address: $("#ADDRS").text() + $("#REQ").text(),
            ZipCode: $("#ZC").text() + $("#REQ").text(),
            MailAddres: $("#MA").text() + $("#REQ").text()
        },*/
        submitHandler: function (form) {
            switch (formMode) {
                case "AddModel":
                    Save();
                    break;
                case "UpdateModel":

                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    //$("#Linkcollapseone").click(function () {
    //    $('#UpdateFile').fileinput('clear');
    //    $("#alertLog").hide();
    //    $("#txtLogComment").val("");

    //    $("#LoadingComment").show();

    //    $.ajax({
    //        url: '/PLM/PDM/ListComment',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { txtCodeModel: $("#U_ModCode").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingComment").hide();

    //        $("#PanelComments").html(data);

    //    }).fail(function () {
    //        alert("error");
    //    });


    //});

    //$("#Linkcollapseone").click(function () {
    //    $('#UpdateFile').fileinput('clear');
    //    $("#alertLog").hide();
    //    $("#txtLogComment").val("");

    //    $("#LoadingComment").show();

    //    $.ajax({
    //        url: '/PLM/PDM/ListComment',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { txtCodeModel: $("#U_ModCode").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingComment").hide();

    //        $("#PanelComments").html(data);

    //    }).fail(function () {
    //        alert("error");
    //    });


    //});

    //$("#projectsAccordion").click(function () {

    //    $("#LoadingProjects").show();

    //    $.ajax({
    //        url: '/PLM/PDM/ListProjects',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { txtModelCode: $("#U_ModCode").val(), txtModelId: $("#Code").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingProjects").hide();

    //        $("#ProjectTable").html(data);

    //    }).fail(function () {
    //        alert("error");
    //    });


    //});


    //$("#SamplesAccordion").click(function () {


    //    $("#LoadingSamples").show();

    //    $.ajax({
    //        url: '/PLM/PDM/ListSamples',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { txtCodeModel: $("#U_ModCode").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingSamples").hide();

    //        $("#SamplesTable").html(data);

    //    }).fail(function () {
    //        alert("error");
    //    });


    //});

    //$("#MDAccordion").click(function () {


    //    $("#LoadingMD").show();

    //    $.ajax({
    //        url: '/PLM/PDM/ListMD',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { txtCodeModel: $("#U_ModCode").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingMD").hide();
    //        $("#MDTable").html(data);
    //    }).fail(function () {
    //        alert("error");
    //    });


    //});

    //Show Change Log (History)
    $("#HistoryAccordion").click(function () {

        $("#LoadingHistory").show();
        $.ajax({
            url: '/PLM/PDM/ListModelLog',
            contextType: 'application/html;charset=utf-8',
            data: { pModelCode: $("#U_ModCode").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadingHistory").hide();

            $("#ModelHistoryTable").html(data);

        }).fail(function () {
            alert("error");
        });
    });

    //Show Differences
    $("#btnHistory").click(function () {      

        var mSelectItems = [];
        $("#HistoryTable").find("input:checked").each(function (i, ob) {            
            mSelectItems.push($(ob).val());
        });

    });


    //$("#logosAccordion").click(function () {

    //    $("#LoadingLogos").show();
    //    $.ajax({
    //        url: '/PLM/Logo/GetLogoList',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { pCode: $("#Code").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingLogos").hide();
    //        $("#LogosTable").html(data);
    //    }).fail(function () {
    //        alert("error");
    //    });

    //});

    //$("#pomAccordion").click(function () {
    //    $("#LoadingPom").show();

    //    $.ajax({
    //        url: '/PLM/PDM/GetModelPom',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { txtModelCode: $("#U_ModCode").val(), pPageKey: $("#Pagekey").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingPom").hide();

    //        $("#PomTable").html(data);

    //    }).fail(function () {
    //        alert("error");
    //    });
    //});

    //$("#LinkcollapseFive").click(function () {

    //    $("#LoadingCostSheet").show();

    //    $.ajax({
    //        url: '/PLM/PDM/ListCostSheet',
    //        contextType: 'application/html;charset=utf-8',
    //        data: { txtCodeModel: $("#U_ModCode").val(), Code: $("#Code").val() },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {
    //        $("#LoadingCostSheet").hide();

    //        $("#PanelCostSheet").html(data);

    //    }).fail(function () {
    //        alert("error");
    //    });


    //});


    function Save() {

        var data = new FormData();
        var fileInput = $("#file-ImgUpload").fileinput('getFileStack');

        if (fileInput.length > 0) {
            for (var i = 0; i < fileInput.length; i++) {
                data.append(fileInput[i].name, fileInput[i]);
            }
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var mColorTableItems = [];
        var mScaleTableItems = [];
        var mVariableTableItems = [];
        var mSegmentation =
        {
            Code: $("#SegmCode").val(),
            U_UseMod: $("#U_UseMod").val(),
            U_UseCol: $("#U_UseCol").val(),
            U_UseScl: $("#U_UseScl").val(),
            U_UseVar: $("#U_UseVar").val()
        };  

        $("#ColorTableItems").find("input:checked").each(function (i, ob) {
            mColorTableItems.push($(ob).val());
        });
        $("#ScaleTableItems").find("input:checked").each(function (i, ob) {
            mScaleTableItems.push($(ob).val());
        });
        $("#VariableTableItems").find("input:checked").each(function (i, ob) {
            mVariableTableItems.push($(ob).val());
        });

        if ($("#U_WhsNewI").val().substring($("#U_WhsNewI").val().length, $("#U_WhsNewI").val().length - 1) == " ") {
            if ($("#U_WhsNewI").val().substring($("#U_WhsNewI").val().length, $("#U_WhsNewI").val().length - 2) == ", ") {
                $("#U_WhsNewI").val($("#U_WhsNewI").val().substring(0, $("#U_WhsNewI").val().length - 2))
            }
        }

        var test = {
            Code: $("#Code").val(),
            U_Pic: $("#U_Pic").val(),
            U_PicR: $("#U_PicR").val(),
            U_DocNumb: $("#U_DocNumb").val(),
            U_ModCode: $("#U_ModCode").val(),
            U_ModDesc: $("#U_ModDesc").val(),
            U_FrgnDesc: $("#U_FrgnDesc").val(),
            U_Status: $("#U_Status").val(),
            U_LineCode: $("#U_LineCode").val(),
            U_ATGrp: $("#U_ATGrp").val(),
            U_ModGrp: $("#U_ModGrp").val(),
            U_SapGrp: $("#U_SapGrp").val(),
            U_Season: $("#U_Season").val(),
            U_CollCode: $("#U_CollCode").val(),
            U_AmbCode: $("#U_AmbCode").val(),
            U_CompCode: $("#U_CompCode").val(),
            U_GrpSCod: $("#U_GrpSCod").val(),
            U_Brand: $("#U_Brand").val(),
            U_Designer: $("#U_Designer").val(),
            U_Division: $("#U_Division").val(),
            U_Owner: $("#U_Owner").val(),
            U_Year: $("#U_Year").val(),
            U_COO: $("#U_COO").val(),
            U_SSDate: $("#U_SSDate").val(),
            U_SCDate: $("#U_SCDate").val(),
            U_Customer: $("#U_Customer").val(),
            U_Vendor: $("#U_Vendor").val(),
            U_MainWhs: $("#U_MainWhs").val(),
            U_WhsNewI: $("#U_WhsNewI").val(),
            U_Currency: $("#U_Currency").val(),
            U_Price: $("#U_Price").val(),
            U_Comments: $("#U_Comments").val(),
            U_SclCode: $("#U_SclCode").val(),
            U_ChartCod: $("#U_ChartCod").val(),
            U_CodePOM: $("#U_CodePOM").val(),
            U_SclPOM: $("#U_SclPOM").val(),
            U_PList: $("#U_PList").val(),
            PageKey: $('#Pagekey').val(),
            ColorTableItems: mColorTableItems,
            ScaleTableItems: mScaleTableItems,
            VariableTableItems: mVariableTableItems,
            ModelSegmentation: mSegmentation,
            MappedUdf: []
        };

        var listUDF = $("input[id^=UDF_]");
        var listUDFSelect = $("select[id^=UDF_]");
        for (i = 0; i < listUDF.length; i++) {
            test.MappedUdf.push({
                UDFName: listUDF[i].id.substring(4),
                Value: $("#" + listUDF[i].id).val(),
            });
        }
        for (i = 0; i < listUDFSelect.length; i++) {
            test.MappedUdf.push({
                UDFName: listUDFSelect[i].id.substring(4),
                Value: $("#" + listUDFSelect[i].id).val(),
            });
        }

        var mPageKey = $('#Pagekey').val();

        data.append('ARGNSModelView', JSON.stringify(test));
        data.append('pPageKey', JSON.stringify(mPageKey));
        data.append('pFrom', JSON.stringify(pFrom));

        $.ajax({
            url: "/PLM/PDM/AddModel",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.ServiceAnswer == "Ok") {
                    $('#Loading').modal('hide');
                    location.href = data.RedirectUrl;
                }
                else
                {
                    $('#Loading').modal('hide');
                    ErrorPDM(data.ErrorMsg);
                }
            }
        });
    }

    function Update() {

        var data = new FormData();
        var fileInput = $("#file-ImgUpload").fileinput('getFileStack');

        if (fileInput.length > 0) {
            for (var i = 0; i < fileInput.length; i++) {
                data.append(fileInput[i].name, fileInput[i]);
            }
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var mColorTableItems = [];
        var mScaleTableItems = [];
        var mVariableTableItems = [];
        var mPomLines = [];

        $("#ColorTableItems").find("input:checked").each(function (i, ob) {
            mColorTableItems.push($(ob).val());
        });
        $("#ScaleTableItems").find("input:checked").each(function (i, ob) {
            mScaleTableItems.push($(ob).val());
        });
        $("#VariableTableItems").find("input:checked").each(function (i, ob) {
            mVariableTableItems.push($(ob).val());
        });


        if ($("#U_WhsNewI").val().substring($("#U_WhsNewI").val().length, $("#U_WhsNewI").val().length - 1) == " ") {
            if ($("#U_WhsNewI").val().substring($("#U_WhsNewI").val().length, $("#U_WhsNewI").val().length - 2) == ", ") {
                $("#U_WhsNewI").val($("#U_WhsNewI").val().substring(0, $("#U_WhsNewI").val().length - 2))
            }

        }

        var test = {
            Code: $("#Code").val(),
            U_Pic: $("#U_Pic").val(),
            U_PicR: $("#U_PicR").val(),
            U_DocNumb: $("#U_DocNumb").val(),
            U_ModCode: $("#U_ModCode").val(),
            U_ModDesc: $("#U_ModDesc").val(),
            U_FrgnDesc: $("#U_FrgnDesc").val(),
            U_Status: $("#U_Status").val(),
            U_LineCode: $("#U_LineCode").val(),
            U_ATGrp: $("#U_ATGrp").val(),
            U_ModGrp: $("#U_ModGrp").val(),
            U_SapGrp: $("#U_SapGrp").val(),
            U_Season: $("#U_Season").val(),
            U_CollCode: $("#U_CollCode").val(),
            U_AmbCode: $("#U_AmbCode").val(),
            U_CompCode: $("#U_CompCode").val(),
            U_GrpSCod: $("#U_GrpSCod").val(),
            U_Brand: $("#U_Brand").val(),
            U_Designer: $("#U_Designer").val(),
            U_Division: $("#U_Division").val(),
            U_Owner: $("#U_Owner").val(),
            U_Year: $("#U_Year").val(),
            U_COO: $("#U_COO").val(),
            U_SSDate: $("#U_SSDate").val(),
            U_SCDate: $("#U_SCDate").val(),
            U_Customer: $("#U_Customer").val(),
            U_Vendor: $("#U_Vendor").val(),
            U_MainWhs: $("#U_MainWhs").val(),
            U_WhsNewI: $("#U_WhsNewI").val(),
            U_Currency: $("#U_Currency").val(),
            U_Price: $("#U_Price").val(),
            U_Comments: $("#U_Comments").val(),
            U_SclCode: $("#U_SclCode").val(),
            U_ChartCod: $("#U_ChartCod").val(),
            U_CodePOM: $("#U_CodePOM").val(),
            U_SclPOM: $("#U_SclPOM").val(),
            U_PList: $("#U_PList").val(),
            PageKey: $('#Pagekey').val(),
            ColorTableItems: mColorTableItems,
            ScaleTableItems: mScaleTableItems,
            VariableTableItems: mVariableTableItems,
            ModelPomList: [],
            MappedUdf: []
            };

        var listUDF = $("input[id^=UDF_]");
        var listUDFSelect = $("select[id^=UDF_]");
        for (i = 0; i < listUDF.length; i++) {
            test.MappedUdf.push({
                UDFName: listUDF[i].id.substring(4),
                Value: $("#" + listUDF[i].id).val(),
            });
        }
        for (i = 0; i < listUDFSelect.length; i++) {
            test.MappedUdf.push({
                UDFName: listUDFSelect[i].id.substring(4),
                Value: $("#" + listUDFSelect[i].id).val(),
            });
        }

        $("#myTab li").each(function (index) {
            $("#TableItemsPom-" + $.trim($(this).text()) + " :input[name=valueInfo]").each(function () {
                var input = $(this); // This is the jquery object of the input, do what you will
                arrayPomLine = input.attr('id').split('-');
                test.ModelPomList.push({
                    "Code": $("#Code").val(),
                    "LineId": arrayPomLine[1],
                    "U_Value": parseFloat(input.val()),
                    "U_TolPosit": parseFloat($("#TotPos-" + arrayPomLine[0]).val()),
                    "U_TolNeg": parseFloat($("#TotNeg-" + arrayPomLine[0]).val()),
                    "U_QAPoint": ($("#QAPoint-" + arrayPomLine[0]).is(':checked') ? "Y" : "N"),
                    "U_SclPom": $("#SclCode-" + arrayPomLine[0]).val()
                });
            });
        });

        var mPageKey = $('#Pagekey').val();       

        data.append('ARGNSModelView', JSON.stringify(test));
        data.append('pPageKey', JSON.stringify(mPageKey));

        $.ajax({
            url: "/PLM/PDM/UpdateModel",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data == "Ok") {
                    $('#Loading').modal('hide');

                    $("#UpdateCorrectly").show("slow");
                    $("#UpdateCorrectly").delay(1500).slideUp(1000);
                }
                else {
                    $('#Loading').modal('hide');
                    ErrorPDM(data);
                }
            }
        });
    }

});

function ErrorPDM(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function VisibilityControl(data) {


    if (data.U_UseCol == "Y" && data.U_UseScl == "Y" && data.U_UseVar == "Y") {
        $("#AddColor").removeAttr('disabled');
        $("#AddSize").removeAttr('disabled');
        $("#AddVar").removeAttr('disabled');
    }
    else if (data.U_UseCol == "Y" && data.U_UseScl == "Y") {

        $("#AddColor").removeAttr('disabled');
        $("#AddSize").removeAttr('disabled');
        $("#AddVar").attr('disabled', 'disabled');

        $("#VariableTableItems").html("");
    }
    else {

        $("#AddColor").removeAttr('disabled');
        $("#AddSize").attr('disabled', 'disabled');
        $("#AddVar").attr('disabled', 'disabled');

        $("#ScaleTableItems").html("");
        $("#VariableTableItems").html("");

    }

    $("#SegmCode").val(data.Code);
    $("#U_UseMod").val(data.U_UseMod);
    $("#U_UseCol").val(data.U_UseCol);
    $("#U_UseScl").val(data.U_UseScl);
    $("#U_UseVar").val(data.U_UseVar);

}

function GetPOMTemplateList() {
    $("#ModalPOMTemplateTable").html("");
    $('#LoadPOMTemplate').modal('show');

    $.ajax({
        url: '/PLM/PDM/_GetModelPomTemplateList',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $('#Pagekey').val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#LoadPOMTemplate').modal('hide');
        $("#ModalPOMTemplateTable").html(data);
    });
}

function SetPOMTemplate(pPOMCode, pSclCode, pU_CodeTmp) {
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });
    $("#LoadingPom").show();

    $.ajax({
        url: '/PLM/PDM/_AddModelPom',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $('#Pagekey').val(), pPOMCode: pPOMCode, pSclCode: pSclCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#Loading').modal('hide');
        $("#LoadingPom").hide();
        $("#ModalPOMTemplate").modal('hide');
        $("#U_SclPOM").val(pSclCode);
        $("#U_CodePOM").val(pU_CodeTmp);
        $("#PomTable").html(data);
    });
}

function LogCommentAccordionShow() {
    //$('#UpdateFile').fileinput('clear');
    $("#alertLog").hide();
    $("#txtLogComment").val("");

    $("#LoadingComment").show();

    $.ajax({
        url: '/PLM/PDM/ListComment',
        contextType: 'application/html;charset=utf-8',
        data: { txtCodeModel: $("#U_ModCode").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingComment").hide();

        $("#PanelComments").html(data);

    }).fail(function () {
        alert("error");
    });
}


function SamplesAccordionShow() {
    $("#LoadingSamples").show();

    $.ajax({
        url: '/PLM/PDM/ListSamples',
        contextType: 'application/html;charset=utf-8',
        data: { txtCodeModel: $("#U_ModCode").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingSamples").hide();

        $("#SamplesTable").html(data);

    }).fail(function () {
        alert("error");
    });
}

function MaterialDetailAccordionShow() {
    $("#LoadingMD").show();

    $.ajax({
        url: '/PLM/PDM/ListMD',
        contextType: 'application/html;charset=utf-8',
        data: { txtCodeModel: $("#U_ModCode").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingMD").hide();
        $("#MDTable").html(data);
    }).fail(function () {
        alert("error");
    });
}

function ProjectAccordionShow() {

    $("#LoadingProjects").show();

    $.ajax({
        url: '/PLM/PDM/ListProjects',
        contextType: 'application/html;charset=utf-8',
        data: { txtModelCode: $("#U_ModCode").val(), txtModelId: $("#Code").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingProjects").hide();

        $("#ProjectTable").html(data);

    }).fail(function () {
        alert("error");
    });

}

function LogosAccordionShow() {

    $("#LoadingLogos").show();
    $.ajax({
        url: '/PLM/Logo/GetLogoList',
        contextType: 'application/html;charset=utf-8',
        data: { pCode: $("#Code").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingLogos").hide();
        $("#LogosTable").html(data);
    }).fail(function () {
        alert("error");
    });

}

function CostSheetAccordionShow() {

    $("#LoadingCostSheet").show();

    $.ajax({
        url: '/PLM/PDM/ListCostSheet',
        contextType: 'application/html;charset=utf-8',
        data: { txtCodeModel: $("#U_ModCode").val(), Code: $("#Code").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingCostSheet").hide();

        $("#PanelCostSheet").html(data);

    }).fail(function () {
        alert("error");
    });

}

function PomAccordionShow() {

    $("#LoadingPom").show();

    $.ajax({
        url: '/PLM/PDM/GetModelPom',
        contextType: 'application/html;charset=utf-8',
        data: { txtModelCode: $("#U_ModCode").val(), pPageKey: $("#Pagekey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingPom").hide();

        $("#PomTable").html(data);

    }).fail(function () {
        alert("error");
    });
}