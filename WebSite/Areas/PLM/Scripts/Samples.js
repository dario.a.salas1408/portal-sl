﻿$(document).ready(function () {
    $("#LoadVendor").hide();
    $("#LoadSupplier").hide();
    

    $("#SearchVendor").click(function () {

        var mVendorCode;
        var mVendorName;

        if ($('#ckCodeVendor').is(':checked')) {
            mVendorCode = $('#txtSearchVendor').val();
        }

        if ($('#ckNameVendor').is(':checked')) {
            mVendorName = $('#txtSearchVendor').val();
        }

        $("#LoadVendor").show('slow');

        $.ajax({
            url: '/PLM/Samples/CustomerSA',
            contextType: 'application/html;charset=utf-8',
            data: { pVendorCode: mVendorCode, pVendorName: mVendorName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadVendor").hide('slow');
            $("#ModalBodyVendor").html(data);


        }).fail(function () {
            alert("error");
        }).always(function () {

        });

    });

    $("#SearchSupplier").click(function () {

        var mVendorCode;
        var mVendorName;

        if ($('#ckCodeSupplier').is(':checked')) {
            mVendorCode = $('#txtSearchSupplier').val();
        }

        if ($('#ckNameSupplier').is(':checked')) {
            mVendorName = $('#txtSearchSupplier').val();
        }

        $("#LoadSupplier").show('slow');

        $.ajax({
            url: '/PLM/Samples/CustomerSUP',
            contextType: 'application/html;charset=utf-8',
            data: { pVendorCode: mVendorCode, pVendorName: mVendorName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadSupplier").hide('slow');
            $("#ModalBodySupplier").html(data);


        }).fail(function () {
            alert("error");
        }).always(function () {

        });

    });

    $("#Comment").val(Comment);
    document.getElementById("U_ModCode").disabled = true;

    if (formMode == "Update") {
        document.getElementById("U_SampCode").disabled = true;
        document.getElementById("U_POM").disabled = true;
        document.getElementById("U_SclCode").disabled = true;
        document.getElementById("btGenerate").disabled = true;
    }

    $("#AddUpdateCorrectly").hide();
    $("#AddUpdateFail").hide();

    $("#ListEmpty").hide();

    //$("#UpdateFile").fileinput({
    //    showUpload: false,
    //    showCaption: false,
    //    browseClass: "btn btn-primary btn-lg",
    //    fileType: "any",
    //    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
    //});

    $('#LoadingSize').hide();

    $('#LoadingPom').hide();

    $("#U_SclCode").change(function () {

        $('#ScaleTableItems').empty();

        GetPOM($("#U_SclCode").val(), $("#Pagekey").val());

        GetSizes($("#U_SclCode").val());

        $('#PomTable').empty();

    });

    $('.input-group.date').datepicker({ dateFormat: 'MM/dd/yyyy' });

    $("#btGenerate").click(function () {

        $('#LoadingPom').show();

        var mSize = "";

        $("#tablesize").find("input:checked").each(function (i, ob) {

            if (mSize == "") {
                mSize = $(ob).val();
            }
            else {
                mSize = mSize + "," + $(ob).val();
            }

        });        
        $.ajax({
            url: '/PLM/Samples/GenerateTab',
            contextType: 'application/html;charset=utf-8',
            data: { pU_PomCode: $('#U_POM').val(), pSizes: mSize, pPageKey: $('#Pagekey').val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $('#PomTable').empty();

            $('#PomTable').html(data);

            $('#LoadingPom').hide();

        }).fail(function () {
            alert("error");
        });

    });

    $("#formSamples").validate({
        rules: {
            U_ModCode: "required",
            U_SampCode: "required",
            U_Date: "required",
            U_ApproDate: "required"
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Add();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

});

function GetPOM(pScale,pPageKeyParam) {
    $.ajax({
        url: '/PLM/Samples/GetPOM',
        type: 'Post',
        data: { pSclCode: pScale, pPageKey: pPageKeyParam },
        traditional: true,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data == "ErrorSecurity") {
                location.href = "../Error/ErrorAccess?Error=Error to Access, User Corrupt";
            }
            else {
                if (data) {

                    $('#U_POM').empty();

                    $.each(data, function (key) {

                        $('#U_POM').append($('<option>', {
                            value: data[key].U_CodeTmpl,
                            text: data[key].U_Desc
                        }));

                    });
                }
            }

        }, error: function (xhr) {
            alert(xhr.status);

        },
    });

}

function GetSizes(pScale) {

    $('#LoadingSize').show();

    $.ajax({
        url: '/PLM/Samples/GetSizes',
        contextType: 'application/html;charset=utf-8',
        data: { pSclCode: pScale, pModeCode: $('#U_ModCode').val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $('#ScaleTableItems').empty();

        $('#ScaleTableItems').html(data);

        $('#LoadingSize').hide();

    }).fail(function () {
        alert("error");
    });
}

function Add() {

    var data = new FormData();
    //var fileInput = document.getElementById("UpdateFile");

    //if (fileInput.files.length > 0) {
    //    for (var i = 0; i < fileInput.files.length; i++) {
    //        data.append(fileInput.files[i].name, fileInput.files[i]);
    //    }
    //}

    var fileInput = $("#file-ImgUpload").fileinput('getFileStack');

    if (fileInput.length > 0) {
        for (var i = 0; i < fileInput.length; i++) {
            data.append(fileInput[i].name, fileInput[i]);
        }
    }

    var Sample = {
        U_ModCode: $("#U_ModCode").val(),
        U_Type: $("#U_Type").val(),
        U_SampCode: $("#U_SampCode").val(),
        U_Status: $("#Status").val(),
        U_SclCode: $("#U_SclCode").val(),
        U_POM: $("#U_POM").val(),
        U_Date: $("#U_Date").val(),
        U_User: $("#U_User").val(),
        U_ApproDate: $("#U_ApproDate").val(),
        U_Active: "Y",
        U_Comments: $("#Comment").val(),
        U_BPCustomer: $("#txtCustomer").val(),
        U_BPSupp: $("#txtSupplier").val(),
        ListLine: []
    };

    $('.tbodyTr tr').each(function (i, ob) {

        Sample.ListLine.push({
            "LineId": i + 1,
            "U_SclCode": $("#U_SclCode").val(),
            "U_POM": $(ob).find(".LineaId").html(),
            "U_Desc": $(ob).find(".U_Desc").html(),
            "U_SizeCode": $(ob).attr("id"),
            "U_Target": parseFloat($(ob).find(".U_Value").html()),
            "U_Actual": parseFloat($(ob).find(".Actual input").val()),
            "U_Revision": $(ob).find(".Revision input").val()
        });

    });

    if (Sample.ListLine.length == 0) {
        $("#ListEmpty").show("slow");
        $("#ListEmpty").delay(1500).slideUp(1000);
        return;
    }

    $('#Loading').modal('show');

    data.append('ARGNSModelSampleView', JSON.stringify(Sample));

    $.ajax({
        url: "/PLM/Samples/AddModel",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.split(',')[0] == "Ok") {
                document.getElementById("U_ModCode").disabled = true;
                document.getElementById("U_SampCode").disabled = true;
                document.getElementById("U_POM").disabled = true;
                document.getElementById("U_SclCode").disabled = true;
                document.getElementById("btGenerate").disabled = true;
                $('#Loading').modal('hide');
                $("#AddUpdateCorrectly").show("slow");
                $("#AddUpdateCorrectly").delay(2500).slideUp(1000);
                setTimeout(function () { location.href = "/PLM/Samples/ViewUpdate?pModCode=" + $("#U_ModCode").val() + "&pSampleCode=" + data.split(',')[1]; }, 500);
            }
            else {
                $('#Loading').modal('hide');
                //$("#AddUpdateFail").show("slow");
                //$("#AddUpdateFail").delay(1500).slideUp(1000);

                ErrorAction(data);
            }
        }
    });

}

function Update() {

    var data = new FormData();
    //var fileInput = document.getElementById("UpdateFile");

    //if (fileInput.files.length > 0) {
    //    for (var i = 0; i < fileInput.files.length; i++) {
    //        data.append(fileInput.files[i].name, fileInput.files[i]);
    //    }
    //}

    var fileInput = $("#file-ImgUpload").fileinput('getFileStack');

    if (fileInput.length > 0) {
        for (var i = 0; i < fileInput.length; i++) {
            data.append(fileInput[i].name, fileInput[i]);
        }
    }

    var Sample = {
        Code: $("#Code").val(),
        U_ModCode: $("#U_ModCode").val(),
        U_Type: $("#U_Type").val(),
        U_SampCode: $("#U_SampCode").val(),
        U_Status: $("#Status").val(),
        U_SclCode: $("#U_SclCode").val(),
        U_POM: $("#U_POM").val(),
        U_Date: $("#U_Date").val(),
        U_User: $("#U_User").val(),
        U_ApproDate: $("#U_ApproDate").val(),
        U_Active: "Y",
        U_Comments: $("#Comment").val(),
        U_BPCustomer: $("#txtCustomer").val(),
        U_BPSupp: $("#txtSupplier").val(),
        ListLine: []
    };

    $('.tbodyTr tr').each(function (i, ob) {

        Sample.ListLine.push({
            "LineId": i + 1,
            "U_SclCode": $("#U_SclCode").val(),
            "U_POM": $(ob).find(".LineaId").html(),
            "U_Desc": $(ob).find(".U_Desc").html(),
            "U_SizeCode": $(ob).attr("id"),
            "U_Target": parseFloat($(ob).find(".U_Value").html()),
            "U_Actual": parseFloat($(ob).find(".Actual input").val()),
            "U_Revision": $(ob).find(".Revision input").val()
        });

    });

    if (Sample.ListLine.length == 0) {
        $("#ListEmpty").show("slow");
        $("#ListEmpty").delay(1500).slideUp(1000);
        return;
    }

    $('#Loading').modal('show');

    data.append('ARGNSModelSampleView', JSON.stringify(Sample));

    $.ajax({
        url: "/PLM/Samples/UpdateModel",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#AddUpdateCorrectly").show("slow");
                $("#AddUpdateCorrectly").delay(2500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorAction(data);
            }
        }
    });

}

function SetCustomer(pCode, pName) {

    $('#txtCustomer').val(pCode);
    $('#VendorModal').modal('hide');

}

function SetSupplier(pCode, pName) {

    $('#txtSupplier').val(pCode);
    $('#SuppModal').modal('hide');

}

function ErrorAction(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}




