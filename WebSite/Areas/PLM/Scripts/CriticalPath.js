﻿$(document).ready(function () {
    $("#btnSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/CriticalPath/CrPathList',
            contextType: 'application/html;charset=utf-8',
            data: { txtSeasonCode: $("#txtSeasonCode").val(), txtCollCode: $("#txtCollCode").val(), txtSubCollCode: $("#txtSubCollCode").val(), txtModelCode: $("#txtModelCode").val(), txtProjectCode: $("#txtProjectCode").val(), txtCustomerCode: $("#txtCustomerCode").val(), txtVendorCode: $("#txtVendorCode").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyCriticalPath").html(data);

            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});