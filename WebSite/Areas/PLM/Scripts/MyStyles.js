﻿$(document).ready(function () {
	var mStartItemsGrid, mLengthItemsGrid;
	var searchViewModel = {};

	$("#btnSearchList").click(function () {
		if (typeof dt === 'undefined') {
			assetListVM.init();
		}
		else {
			assetListVM.refresh();
		}
	});

	$("#SearchItems").click(function () {

		if (typeof assetListVM_ListItems.dt === 'undefined' || assetListVM_ListItems.dt == null) {
			assetListVM_ListItems.init();
			$("#btnChoose").prop('disabled', true);
			updateLinesChanged(true);
		}
		else {
			assetListVM_ListItems.refresh();
			$("#btnChoose").prop('disabled', true);
			updateLinesChanged(true);
		}
		pSelectedItems = [];

	});

	$("#btnSearchGrid").click(function () {

		var mListSearchUDF = $("input[id^=SearchItemGridUDF_]");
		var mListSearchUDFSelect = $("select[id^=SearchItemGridUDF_]");
		var mMappedUDFList = [];
		for (i = 0; i < mListSearchUDF.length; i++) {
			mMappedUDFList.push({
				UDFName: mListSearchUDF[i].id.substring(18),
				Value: $("#" + mListSearchUDF[i].id).val(),
			});
		}
		for (i = 0; i < mListSearchUDFSelect.length; i++) {
			mMappedUDFList.push({
				UDFName: mListSearchUDFSelect[i].id.substring(18),
				Value: $("#" + mListSearchUDFSelect[i].id).val(),
			});
		}

		searchViewModel = {};
		searchViewModel.pMappedUdf = mMappedUDFList;
		searchViewModel.CodeModel = $("#txtCodeModel").val();
		searchViewModel.NameModel = $("#txtNameModel").val();
		searchViewModel.SeasonModel = $("#txtSeasonModel").val();
		searchViewModel.GroupModel = $("#txtGroupModel").val();
		searchViewModel.BrandModel = $("#txtBrand").val();
		searchViewModel.CollectionModel = $("#txtCollection").val();
		searchViewModel.SubCollectionModel = $("#txtSubCollection").val();
		mStartItemsGrid = 0;
		GetItemsGrid();

	});

	function GetItemsGrid() {
		mLengthItemsGrid = ($("#PageQtyItemsGrid").val() == undefined ? 10 : Number($("#PageQtyItemsGrid").val()));
		mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

		searchViewModel.Start = mStartItemsGrid;
		searchViewModel.Length = mLengthItemsGrid;

		$("#LoadGrid").show('slow');

		$.ajax({
			url: '/PLM/MyStyles/MyStyleGrid',
			contextType: 'application/html;charset=utf-8',
			data: { searchViewModel },
			type: 'POST',
			dataType: 'html'
		}).done(function (data) {

			$("#LoadGrid").hide();
			$("#BodyPDMGrid").html(data);
			$("#btnChooseGrid").prop('disabled', true);

			if (mStartItemsGrid <= 0) {
				$("#PreviousItemsGrid").attr('disabled', 'disabled');
			}
			else {
				$("#PreviousItemsGrid").removeAttr('disabled');;
			}

			if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityItemsGrid").val()) {
				$("#NextItemsGrid").attr('disabled', 'disabled');
			}
			else {
				$("#NextItemsGrid").removeAttr('disabled');;
			}

			updateLinesChanged(true);
		});

	}

	$("#PDMList").bind("click", function () {
		$('#products .item').addClass('list-group-item');
	});

	$("#PDMGrid").bind("click", function () {
		$('#products .item').removeClass('list-group-item');
		$('#products .item').addClass('grid-group-item');
	});

});