﻿$(document).ready(function () {
    $("#btnSearch").click(function () {
        
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
              
        $.ajax({
            url: '/PLM/ColorMaster/ColorList',
            contextType: 'application/html;charset=utf-8',
            data: { txtColorCode: $("#txtColorCode").val(), txtColorName: $("#txtColorName").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyColor").html(data);

            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});