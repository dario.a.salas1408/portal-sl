﻿$(document).ready(function () {
    
    $("#UpdateCorrectly").hide();

    if (formMode == "Update")
    {
        $("#U_ColCode").attr("disabled", true);
    }

    $("#formColor").validate({
        rules: {
            U_ColCode: "required",
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Add();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    function Add() {
        $('#Loading').modal('show');
        var datos = new FormData();
        var fileInput = $("#UpdateFile").fileinput('getFileStack');
        if (fileInput.length > 0) {
            for (var j = 0; j < fileInput.length; j++) {
                datos.append(fileInput[j].name, fileInput[j]);
            }
        }

        //var mUModCode = $("#U_ModCode").val();
        //datos.append('mUModCode', mUModCode);
        //datos.append('mPageKey', $("#Pagekey").val());


        var ColorObj = {
            Code: $("#Code").val(),
            U_ColCode: $("#U_ColCode").val(),
            U_ColDesc: $("#U_ColDesc").val(),
            U_Shade: $("#U_Shade").val(),
            U_NRFCode: $("#U_NRFCode").val(),
            Alpha: 255,
            Red: $('#ColorPicker').data('colorpicker').color.toRGB().r,
            Green: $('#ColorPicker').data('colorpicker').color.toRGB().g,
            Blue: $('#ColorPicker').data('colorpicker').color.toRGB().b,
            U_Attrib1: $("#U_Attrib1").val(),
            U_Attrib2: $("#U_Attrib2").val(),
            U_Active: ($("#U_Active").prop('checked') ? "Y" : "N"),
            U_Pic: $("#U_Pic").val()
        };

        $.ajax({
            type: 'POST',
            url: '/PLM/ColorMaster/UploadFile',
            contentType: false,
            processData: false,
            data: datos,
        }).done(function (result) {
            $.ajax({
                url: "/PLM/ColorMaster/Add",
                async: false,
                type: "POST",
                data: JSON.stringify(ColorObj),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data == "Ok") {
                        $('#Loading').modal('hide');
                        location.href = urlRedirect;
                    }
                    else {
                        $('#Loading').modal('hide');
                        ErrorPDM(data);
                    }
                }
            });
        }).fail(function () {
            alert("error");
        });
    }

    function Update() {

        $('#Loading').modal('show');
        var datos = new FormData();
        var fileInput = $("#UpdateFile").fileinput('getFileStack');
        if (fileInput.length > 0) {
            for (var j = 0; j < fileInput.length; j++) {
                datos.append(fileInput[j].name, fileInput[j]);
            }
        }

        //var mUModCode = $("#U_ModCode").val();
        //datos.append('mUModCode', mUModCode);
        //datos.append('mPageKey', $("#Pagekey").val());


        var ColorObj = {
            Code: $("#Code").val(),
            U_ColCode: $("#U_ColCode").val(),
            U_ColDesc: $("#U_ColDesc").val(),
            U_Shade: $("#U_Shade").val(),
            U_NRFCode: $("#U_NRFCode").val(),
            Alpha: 255,
            Red: $('#ColorPicker').data('colorpicker').color.toRGB().r,
            Green: $('#ColorPicker').data('colorpicker').color.toRGB().g,
            Blue: $('#ColorPicker').data('colorpicker').color.toRGB().b,
            U_Attrib1: $("#U_Attrib1").val(),
            U_Attrib2: $("#U_Attrib2").val(),
            U_Active: ($("#U_Active").prop('checked') ? "Y" : "N"),
            U_Pic: $("#U_Pic").val()
        };

        $.ajax({
            type: 'POST',
            url: '/PLM/ColorMaster/UploadFile',
            contentType: false,
            processData: false,
            data: datos,
        }).done(function (result) {
            $.ajax({
                url: "/PLM/ColorMaster/Update",
                async: false,
                type: "POST",
                data: JSON.stringify(ColorObj),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data == "Ok") {
                        $('#Loading').modal('hide');
                        $("#UpdateCorrectly").show("slow");
                        $("#UpdateCorrectly").delay(1500).slideUp(1000);
                    }
                    else {
                        $('#Loading').modal('hide');
                        ErrorPDM(data);
                    }
                }
            });
        }).fail(function () {
            alert("error");
        });
    }

});

function ErrorPDM(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}