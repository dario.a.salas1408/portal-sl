﻿$(document).ready(function () {
    $("#btnSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/ScaleMaster/ScaleList',
            contextType: 'application/html;charset=utf-8',
            data: { txtScaleCode: $("#txtScaleCode").val(), txtScaleName: $("#txtScaleName").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyScale").html(data);

            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });
});

function showSizes(code) {

    $("#LoadingSizes").show();

    $.ajax({
        url: '/PLM/ScaleMaster/GetScaleDescriptionListSearch',
        contextType: 'application/html;charset=utf-8',
        data: { pScaleCode: code },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadingSizes").hide();

        $("#tdInfo-" + code).html(data);
        $("#collapse-" + code).show();
        $('#link-'+ code).attr('onclick','').unbind('click');

    }).fail(function () {
        alert("error");
    });
}