﻿$(document).ready(function () {

    $('.input-group.date').datepicker({});
    $("#UpdateCorrectly").hide();

    switch (formMode) {
        case "Add":
            ChangeWorkflow();
            break;
        case "Update":
            $("#U_Workflow").attr('disabled', 'disabled');
            $("#U_Planning").attr('disabled', 'disabled');
            break;
        case "View":
            $("#U_Workflow").attr('disabled', 'disabled');
            $("#U_Planning").attr('disabled', 'disabled');
            $("#U_Desc").attr('disabled', 'disabled');
            $("#U_Status").attr('disabled', 'disabled');
            $("#U_Calendar").attr('disabled', 'disabled');
            $("#U_SDate").attr('disabled', 'disabled');
            $("#U_SalesO").attr('disabled', 'disabled');
            $("#TableActivities :input[type=hidden]").each(function () {
                var input = $(this); // This is the jquery object of the input, do what you will
                $("#U_Depart" + input.val()).attr('disabled', 'disabled');
                $("#U_Role" + input.val()).attr('disabled', 'disabled');
            });
            break;
        default:
            break;
    }

    $("#btnOk").click(function () {

        $('#Loading').modal('show');
        var dateSDate = $("#U_SDate").datepicker("getDate");

        var mProjectView = {
            Code: $("#Code").val(),
            U_Desc: $("#U_Desc").val(),
            U_Model: $("#U_Model").val(),
            U_Workflow: $("#U_Workflow").val(),
            U_Status: $("#U_Status").val(),
            U_Calendar: $("#U_Calendar").val(),
            U_Planning: $("#U_Planning").val(),
            U_SDate: (isNaN(dateSDate) == true ? null : (dateSDate.getMonth() + 1) + "/" + dateSDate.getDate() + "/" + dateSDate.getFullYear()),
            U_SalesO: $("#U_SalesO").val(),
            ModelID: $("#ModelID").val(),
            ActivitiesList: []
        };

        $("#TableActivities :input[type=hidden]").each(function () {
            var input = $(this);
            var datePlSDate = $("#U_PlSDate" + input.attr('value')).datepicker("getDate");
            var datePlCDate = $("#U_PlCDate" + input.attr('value')).datepicker("getDate");
            mProjectView.ActivitiesList.push({
                "LineId": input.attr('value'),
                "U_Updated": ($('#U_Updated' + input.attr('value')).prop('checked') == true ? "Y" : "N"),
                "U_PhaseId": $("#U_PhaseId" + input.attr('value')).val(),
                "U_Desc": $("#U_Desc" + input.attr('value')).val(),
                "U_Depart": $("#U_Depart" + input.attr('value')).val(),
                "U_Role": $("#U_Role" + input.attr('value')).val(),
                "U_Manager": $("#U_Manager" + input.attr('value')).val(),
                "U_User": $("#U_User" + input.attr('value')).val(),
                "U_Bp": $("#U_Bp" + input.attr('value')).val(),
                "U_PlanLead": $("#U_PlanLead" + input.attr('value')).val(),
                "U_PlSDate": (isNaN(datePlSDate) == true ? null : (datePlSDate.getMonth() + 1) + "/" + datePlSDate.getDate() + "/" + datePlSDate.getFullYear()),
                "U_PlCDate": (isNaN(datePlCDate) == true ? null : (datePlCDate.getMonth() + 1) + "/" + datePlCDate.getDate() + "/" + datePlCDate.getFullYear()),
                "U_ActSAP": $("#U_ActSAP" + input.attr('value')).val()
            });
        });

        switch (formMode) {
            case "Add":
                $.ajax({
                    url: '/PLM/Project/AddProject',
                    contextType: 'application/html;charset=utf-8',
                    data: { pProject: mProjectView },
                    type: 'POST',
                    dataType: 'html'
                }).done(function (data) {
                    var jsonObj = $.parseJSON(data);
                    if (jsonObj.ServiceAnswer == "Ok") {
                        location.href = jsonObj.RedirectUrl;
                        $('#Loading').modal('hide');
                    }
                    else {
                        $('#Loading').modal('hide');
                        ErrorProject(jsonObj.ErrorMsg);
                    }
                });
                break;
            case "Update":
                $.ajax({
                    url: '/PLM/Project/UpdateProject',
                    contextType: 'application/html;charset=utf-8',
                    data: { pProject: mProjectView },
                    type: 'POST',
                    dataType: 'html'
                }).done(function (data) {
                    if (data === "Ok") {
                        $('#Loading').modal('hide');
                        $("#UpdateCorrectly").show("slow");
                        $("#UpdateCorrectly").delay(1500).slideUp(1000);
                    }
                    else {
                        $('#Loading').modal('hide');
                        ErrorProject(data);
                    }
                }).fail(function (data) {
                    ErrorProject(data);
                });
                break;
            default:
                break;
        }

    });

    $("#SearchUser").click(function () {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/Project/_GetUserList',
            contextType: 'application/html;charset=utf-8',
            data: { pCode: $("#CodeUser").val(), pName: $("#NameUser").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#Loading').modal('hide');
            $("#ModalUserTable").html(data);
        });

    });

    $("#SearchEmployee").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/Project/_GetEmployeeList',
            contextType: 'application/html;charset=utf-8',
            data: { pCode: $("#CodeEmployee").val(), pName: $("#NameEmployee").val(), pDepartment: $("#U_Depart" + $("#EmployeeRowId").val()).val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#Loading').modal('hide');
            $("#ModalEmployeeTable").html(data);
        });

    });

    $("#SearchBP").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/Project/_GetBPList',
            contextType: 'application/html;charset=utf-8',
            data: { pCode: $("#CodeBP").val(), pName: $("#NameBP").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#Loading').modal('hide');
            $("#ModalBPTable").html(data);
        });

    });


    $("#CreateActivityButton").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
        mActivitiesList = [];

        $("#TableActivities :input[type=hidden]").each(function () {
            var input = $(this);
            var datePlSDate = $("#U_PlSDate" + input.attr('value')).datepicker("getDate");
            var datePlCDate = $("#U_PlCDate" + input.attr('value')).datepicker("getDate");
            mActivitiesList.push({
                "Code": $("#Code").val(),
                "LineId": input.attr('value'),
                "U_Updated": ($('#U_Updated' + input.attr('value')).prop('checked') == true ? "Y" : "N"),
                "U_PhaseId": $("#U_PhaseId" + input.attr('value')).val(),
                "U_Desc": $("#U_Desc" + input.attr('value')).val(),
                "U_Depart": $("#U_Depart" + input.attr('value')).val(),
                "U_Role": $("#U_Role" + input.attr('value')).val(),
                "U_Manager": $("#U_Manager" + input.attr('value')).val(),
                "U_User": $("#U_User" + input.attr('value')).val(),
                "U_Bp": $("#U_Bp" + input.attr('value')).val(),
                "U_PlanLead": $("#U_PlanLead" + input.attr('value')).val(),
                "U_PlSDate": (isNaN(datePlSDate) == true ? null : (datePlSDate.getMonth() + 1) + "/" + datePlSDate.getDate() + "/" + datePlSDate.getFullYear()),
                "U_PlCDate": (isNaN(datePlCDate) == true ? null : (datePlCDate.getMonth() + 1) + "/" + datePlCDate.getDate() + "/" + datePlCDate.getFullYear()),
                "U_ActSAP": $("#U_ActSAP" + input.attr('value')).val()
            });
        });


        $.ajax({
            url: '/PLM/Project/_CreateActivities',
            contextType: 'application/html;charset=utf-8',
            data: { pActivityList: mActivitiesList, pModelCode: $("#U_Model").val(), pModelId: $("#ModelID").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            location.reload();
            $('#Loading').modal('hide');
        });
    });

});

function CalculateLineDates()
{
    var dateU_SDate = $("#U_SDate").datepicker("getDate");
    var pNextLineDate = $("#U_SDate").datepicker("getDate");

    //Foward Plan
    if($("#U_Planning").val() == "1")
    {
        $("#TableActivities :input[type=hidden]").each(function () {
            var input = $(this); // This is the jquery object of the input, do what you will
            $("#U_PlSDate" + input.val()).datepicker("setDate", pNextLineDate);
            var U_LTimeAux = Number($("#U_PlanLead" + input.val()).val());
            pNextLineDate = new Date(pNextLineDate.setDate(pNextLineDate.getDate() + U_LTimeAux));
            $("#U_PlCDate" + input.val()).datepicker("setDate", pNextLineDate);
        });
        
    }

    //Backward Plan
    if ($("#U_Planning").val() == "2")
    {
        $($("#TableActivities :input[type=hidden]").get().reverse()).each(function () {
            var input = $(this); // This is the jquery object of the input, do what you will
            $("#U_PlCDate" + input.val()).datepicker("setDate", pNextLineDate);
            var U_LTimeAux = Number($("#U_PlanLead" + input.val()).val());
            pNextLineDate = new Date(pNextLineDate.setDate(pNextLineDate.getDate() - U_LTimeAux));
            $("#U_PlSDate" + input.val()).datepicker("setDate", pNextLineDate);
        });
    }
}

function ChangeWorkflow()
{
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    $.ajax({
        url: '/PLM/Project/_GetWorkflowLines',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $('#Pagekey').val(), pWorkflow: $("#U_Workflow").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#Loading').modal('hide');
        $("#tabActivities").html(data);
        CalculateLineDates();
    });
}

function SetUserInLine(pCode)
{
    $("#U_Manager" + $("#UserRowId").val()).val(pCode);
    $("#U_User" + $("#UserRowId").val()).val("");
    $('#ModalUsers').modal('hide');
}

function SetEmployeeInLine(pCode) {
    $("#U_User" + $("#EmployeeRowId").val()).val(pCode);
    $("#U_Manager" + $("#EmployeeRowId").val()).val("");
    $('#ModalEmployee').modal('hide');
}

function SetBPInLine(pCode) {
    $("#U_Bp" + $("#BPRowId").val()).val(pCode);
    $('#ModalBP').modal('hide');
}

function ErrorProject(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}