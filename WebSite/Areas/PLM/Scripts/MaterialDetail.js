﻿$(document).ready(function () {

    $('#UpdateCorrectly').hide();

    $("#btnOK").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        switch (formMode) {
            case "Add":
                Add();
                break;
            case "Update":
                Update();
                break;
            default:
                alert('Imposible realizar una acción')
                break;
        }

    });


});

function btnChooseColor(mColorCellId) {
    if ($('input[name=colorRadio]:checked').val() != "" && $('input[name=colorRadio]:checked').val() != null) {
        $(mColorCellId).val($('input[name=colorRadio]:checked').val());
        $("#alertify-ok").click();
    }
}

function AddRow(pTableId, pTableName) {
    var nextId = NextRowId("TR_" + pTableName);
    $.ajax({
        url: '/PLM/MaterialDetail/_GetRow',
        contextType: 'application/html;charset=utf-8',
        data: { pTable: pTableName, pNextId: nextId },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#' + pTableId + ' tr:last').after(data);
    }).fail(function () {
        alert("error");
    });
}

function SearchColor(mColorCellId) {
    $.ajax({
        url: '/PLM/MaterialDetail/_GetColor',
        contextType: 'application/html;charset=utf-8',
        data: { pModelCode: $("#U_ModCode").val(), pColorCellId: mColorCellId },
        async: true,
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        alertify.alert(data);

    });
}

function NextRowId(pSearchId) {
    var mListTr = $("tr[id^=" + pSearchId + "]");
    auxMax = 0;
    for (i = 0; i < mListTr.length ; i++) {
        if (mListTr[i].id.substring(pSearchId.length) > auxMax) {
            auxMax = mListTr[i].id.substring(pSearchId.length);
        }
    }

    return parseInt(auxMax) + 1;
}

function DeleteRow(pTrId) {
    $("#" + pTrId).remove();
}

function Add() {

    var MaterialDetailView = {
        Code: $("#Code").val(),
        U_ModCode: $("#U_ModCode").val(),
        U_Description: $("#U_Description").val(),
        PageKey: $('#Pagekey').val(),
        ListFabric: [],
        ListAccessories: [],
        ListCareInstructions: [],
        ListLabelling: [],
        ListPackaging: [],
        ListFootwearMaterial: [],
        ListFootwearDetail: [],
        ListFootwearPackaging: [],
        ListFootwearPictogram: [],
        ListFootwearSizeRange: [],
        ListFootwearAccessories: [],
    };

    //------------------------------ Fabric ------------------------------

    var listUDFLines = $("input[id^=LINEUDF_Fabric_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Fabric_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(15))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(15))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFabric :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Fabric_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFabric.push({
            "U_ColCode": $("#FabricColor" + input.attr('id')).val(),
            "U_CompCode": $("#Comp" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Fabric_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Accessories ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Accessories_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Accessories_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(20))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(20))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableAccessories :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Accessories_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListAccessories.push({
            "U_Buttons": $("#Buttons" + input.attr('id')).val(),
            "U_Trims": $("#Trims" + input.attr('id')).val(),
            "U_Zippers": $("#Zippers" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Accessories_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Care Instructions ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_CI_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_CI_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(11))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(11))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableCI :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_CI_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListCareInstructions.push({
            "U_ColCode": $("#CI_Color" + input.attr('id')).val(),
            "U_CareText": $("#CI_CT" + input.attr('id')).val(),
            "U_CareCode": $("#CI_CC" + input.attr('id')).val(),
            "U_CareLbl": $("#CI_CL2" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#CI_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Labelling ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Labelling_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Labelling_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(18))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(18))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableLabelling :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Labelling_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListLabelling.push({
            "U_MLabel": $("#L_ML" + input.attr('id')).val(),
            "U_LImage": $("#L_LI" + input.attr('id')).val(),
            "U_LSize": $("#L_LS" + input.attr('id')).val(),
            "U_Position": $("#L_Position" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Labelling_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Packaging ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Packaging_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Packaging_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(18))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(18))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TablePackaging :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Packaging_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListPackaging.push({
            "U_Hangtag": $("#P_HangTag" + input.attr('id')).val(),
            "U_Polybag": $("#P_Polybag" + input.attr('id')).val(),
            "U_Others": $("#P_Others" + input.attr('id')).val(),
            "U_OutPack": $("#P_OP2" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Packaging_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Material ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Material_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Material_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(17))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(17))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearMaterial :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Material_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearMaterial.push({
            "U_Sfabric": $("#FMaterial_SF" + input.attr('id')).val(),
            "U_Lining": $("#FMaterial_Lining" + input.attr('id')).val(),
            "U_Insole": $("#FMaterial_Insole" + input.attr('id')).val(),
            "U_Outsole": $("#FMaterial_Outsole" + input.attr('id')).val(),
            "U_Heel": $("#FMaterial_Heel" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearMaterial_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Detail ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Detail_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Detail_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(15))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(15))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearDetail :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Detail_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearDetail.push({
            "U_HHeight": $("#FDetail_HH" + input.attr('id')).val(),
            "U_BHeight": $("#FDetail_BH" + input.attr('id')).val(),
            "U_BWidth": $("#FDetail_BW" + input.attr('id')).val(),
            "U_Strap": $("#FDetail_Strap" + input.attr('id')).val(),
            "U_Seams": $("#FDetail_Seams" + input.attr('id')).val(),
            "U_Others": $("#FDetail_Others" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearDetail_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Packaging ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_FPackaging_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_FPackaging_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearPackaging :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_FPackaging_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearPackaging.push({
            "U_Hangtag": $("#FPackaging_Hangtag" + input.attr('id')).val(),
            "U_PolybagSB": $("#FPackaging_PSB" + input.attr('id')).val(),
            "U_Others": $("#FPackaging_Others" + input.attr('id')).val(),
            "U_OutPack": $("#FPackaging_OthersPacking" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearPackaging_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Pictogram ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Pictogram_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Pictogram_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearPictogram :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Pictogram_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearPictogram.push({
            "U_Pictogram": $("#FPictogram_Pictogram" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearPictogram_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Size Range ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_SRange_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_SRange_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearSRange :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_SRange_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearSizeRange.push({
            "U_SRange": $("#FSRange_SizeRange" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearSizeRange_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Accessories ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_FAccessories_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_FAccessories_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearAccessories :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_FAccessories_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearAccessories.push({
            "U_SLaces": $("#FAccess_SL" + input.attr('id')).val(),
            "U_Eyelets": $("#FAccess_Eyelets" + input.attr('id')).val(),
            "U_Buckles": $("#FAccess_Buckles" + input.attr('id')).val(),
            "U_Enclosures": $("#FAccess_Enclosures" + input.attr('id')).val(),
            "U_Labelling": $("#FAccess_Labelling" + input.attr('id')).val(),
            "U_Others": $("#FAccess_Others" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearAccessories_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    $.ajax({
        url: "/PLM/MaterialDetail/Add",
        async: false,
        type: "POST",
        data: JSON.stringify(MaterialDetailView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                location.href = '/PLM/MaterialDetail/ActionMD?ActionMD=Update&pModelCode='+$("#U_ModCode").val();
            }
            else {
                $('#Loading').modal('hide');
                ErrorPO(data);
            }
        }
    });

}

function Update() {

    var MaterialDetailView = {
        Code: $("#Code").val(),
        U_ModCode: $("#U_ModCode").val(),
        U_Description: $("#U_Description").val(),
        PageKey: $('#Pagekey').val(),
        ListFabric: [],
        ListAccessories: [],
        ListCareInstructions: [],
        ListLabelling: [],
        ListPackaging: [],
        ListFootwearMaterial: [],
        ListFootwearDetail: [],
        ListFootwearPackaging: [],
        ListFootwearPictogram: [],
        ListFootwearSizeRange: [],
        ListFootwearAccessories: [],
    };

    //------------------------------ Fabric ------------------------------

    var listUDFLines = $("input[id^=LINEUDF_Fabric_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Fabric_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(15))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(15))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFabric :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Fabric_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFabric.push({
            "U_ColCode": $("#FabricColor" + input.attr('id')).val(),
            "U_CompCode": $("#Comp" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Fabric_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Accessories ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Accessories_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Accessories_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(20))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(20))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableAccessories :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Accessories_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListAccessories.push({
            "U_Buttons": $("#Buttons" + input.attr('id')).val(),
            "U_Trims": $("#Trims" + input.attr('id')).val(),
            "U_Zippers": $("#Zippers" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Accessories_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Care Instructions ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_CI_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_CI_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(11))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(11))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableCI :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_CI_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListCareInstructions.push({
            "U_ColCode": $("#CI_Color" + input.attr('id')).val(),
            "U_CareText": $("#CI_CT" + input.attr('id')).val(),
            "U_CareCode": $("#CI_CC" + input.attr('id')).val(),
            "U_CareLbl": $("#CI_CL2" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#CI_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Labelling ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Labelling_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Labelling_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(18))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(18))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableLabelling :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Labelling_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListLabelling.push({
            "U_MLabel": $("#L_ML" + input.attr('id')).val(),
            "U_LImage": $("#L_LI" + input.attr('id')).val(),
            "U_LSize": $("#L_LS" + input.attr('id')).val(),
            "U_Position": $("#L_Position" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Labelling_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Packaging ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Packaging_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Packaging_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(18))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(18))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TablePackaging :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Packaging_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListPackaging.push({
            "U_Hangtag": $("#P_HangTag" + input.attr('id')).val(),
            "U_Polybag": $("#P_Polybag" + input.attr('id')).val(),
            "U_Others": $("#P_Others" + input.attr('id')).val(),
            "U_OutPack": $("#P_OP2" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#Packaging_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Material ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Material_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Material_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(17))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(17))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearMaterial :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Material_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearMaterial.push({
            "U_Sfabric": $("#FMaterial_SF" + input.attr('id')).val(),
            "U_Lining": $("#FMaterial_Lining" + input.attr('id')).val(),
            "U_Insole": $("#FMaterial_Insole" + input.attr('id')).val(),
            "U_Outsole": $("#FMaterial_Outsole" + input.attr('id')).val(),
            "U_Heel": $("#FMaterial_Heel" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearMaterial_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Detail ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Detail_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Detail_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(15))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(15))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearDetail :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Detail_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearDetail.push({
            "U_HHeight": $("#FDetail_HH" + input.attr('id')).val(),
            "U_BHeight": $("#FDetail_BH" + input.attr('id')).val(),
            "U_BWidth": $("#FDetail_BW" + input.attr('id')).val(),
            "U_Strap": $("#FDetail_Strap" + input.attr('id')).val(),
            "U_Seams": $("#FDetail_Seams" + input.attr('id')).val(),
            "U_Others": $("#FDetail_Others" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearDetail_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Packaging ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_FPackaging_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_FPackaging_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearPackaging :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_FPackaging_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearPackaging.push({
            "U_Hangtag": $("#FPackaging_Hangtag" + input.attr('id')).val(),
            "U_PolybagSB": $("#FPackaging_PSB" + input.attr('id')).val(),
            "U_Others": $("#FPackaging_Others" + input.attr('id')).val(),
            "U_OutPack": $("#FPackaging_OthersPacking" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearPackaging_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Pictogram ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_Pictogram_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_Pictogram_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearPictogram :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_Pictogram_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearPictogram.push({
            "U_Pictogram": $("#FPictogram_Pictogram" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearPictogram_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Size Range ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_SRange_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_SRange_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearSRange :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_SRange_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearSizeRange.push({
            "U_SRange": $("#FSRange_SizeRange" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearSizeRange_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    //------------------------------ Footwear Accessories ------------------------------
    var listUDFLines = $("input[id^=LINEUDF_FAccessories_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_FAccessories_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(19))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(19))
    }
    linesUDFNames = $.unique(linesUDFNames);
    $("#TableFootwearAccessories :input[type=hidden]").each(function () {
        var input = $(this);
        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_FAccessories_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }

        MaterialDetailView.ListFootwearAccessories.push({
            "U_SLaces": $("#FAccess_SL" + input.attr('id')).val(),
            "U_Eyelets": $("#FAccess_Eyelets" + input.attr('id')).val(),
            "U_Buckles": $("#FAccess_Buckles" + input.attr('id')).val(),
            "U_Enclosures": $("#FAccess_Enclosures" + input.attr('id')).val(),
            "U_Labelling": $("#FAccess_Labelling" + input.attr('id')).val(),
            "U_Others": $("#FAccess_Others" + input.attr('id')).val(),
            "LineId": input.attr('id'),
            "IsNew": $("#FootwearAccessories_IsNew" + input.attr('id')).val(),
            "MappedUdf": MappedUdf
        });
    });

    $.ajax({
        url: "/PLM/MaterialDetail/Update",
        async: false,
        type: "POST",
        data: JSON.stringify(MaterialDetailView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
                location.reload();
            }
            else {
                $('#Loading').modal('hide');
                ErrorPO(data);
            }
        }
    });

}