﻿$(document).ready(function () {
    //Show Difference
    $("#btnHistory").click(function () {

        var mSelectItems = [];
        $("#HistoryTable").find("input:checked").each(function (i, ob) {
            mSelectItems.push($(ob).val());
        });

        if (mSelectItems.length == 0) {
            ErrorMessage("Please, Select an element.");
            return;
        }

        if (mSelectItems.length > 2) {
            ErrorMessage("Please, Select until two element.");
            return;
        }

        $("#LoadingHistory").show();

        $.ajax({
            url: '/PLM/PDM/LogDifferenceList',
            contextType: 'application/html;charset=utf-8',
            data: { pModelCode: $('#Code').val(), pSelected: mSelectItems.join() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadingHistory").hide();

            $("#ModelHistoryTable").html(data);

        }).fail(function () {
            alert("error");
        });
    });
});

function ErrorMessage(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}