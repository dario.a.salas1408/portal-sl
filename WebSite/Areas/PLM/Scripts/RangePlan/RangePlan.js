﻿$(document).ready(function () {
    $('.input-group.date').datepicker({});
    $("#UpdateCorrectly").hide();

    $("#ModelSearch").click(function () {
        mStartItemsGrid = 0;
        GetModelsList();
    });



    $("#formRangePlan").validate({
        rules: {
            Code: "required"
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    
});

function Update() {

    $('#Loading').modal('show');
    var dateSDate = $("#U_SDate").datepicker("getDate");
    var dateDTo = $("#U_DTo").datepicker("getDate");

    var ARGNSRangePlanView = {
        Code: $("#Code").val(),
        U_Season: $("#U_Season").val(),
        U_SDate: (isNaN(dateSDate) == true ? null : (dateSDate.getMonth() + 1) + "/" + dateSDate.getDate() + "/" + dateSDate.getFullYear()),
        U_DTo: (isNaN(dateDTo) == true ? null : (dateDTo.getMonth() + 1) + "/" + dateDTo.getDate() + "/" + dateDTo.getFullYear()),
        U_RangeDesc: $("#U_RangeDesc").val(),
        U_Coll: $("#U_Coll").val(),
        U_PrjCode: $("#U_PrjCode").val(),
        U_RPEmpl: $("#U_RPEmpl").val(),
        Lines: []
    };

    $("#RPLines").find('li').each(function () {
        var input = $(this); 
        ARGNSRangePlanView.Lines.push({
            "Code": $("#Code_" + input.attr('data-id')).val(),
            "LineId": $("#LineId_" + input.attr('data-id')).val(),
            "U_ModCode": input.attr('data-id'),
            "U_ModPic": $("#PicPath_" + input.attr('data-id')).val(),
            "U_ModDesc": $("#ModDesc_" + input.attr('data-id')).val(),
            "U_Comments": $("#Comment_" + input.attr('data-id')).val()
        });
    });

    $.ajax({
        url: '/PLM/RangePlan/UpdateRangePlan',
        async: false,
        type: "POST",
        data: JSON.stringify(ARGNSRangePlanView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data == "Ok") {
                $('#Loading').modal('hide');

                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorRP(data);
            }
        }
    });

}

function ErrorRP(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function GetNextModelList() {
    mStartItemsGrid = mStartItemsGrid + Number($('#PageQtyModelList').val());
    GetModelsList();
};

function GetPreviousModelList() {
    mStartItemsGrid = mStartItemsGrid - Number($('#PageQtyModelList').val());
    GetModelsList();
};

function GetModelsList() {
    $("#LoadSearchModel").show('slow');
    $('#Loading').modal({
        backdrop: 'static'
    });

    var mModelCode;
    var mModelName;

    if ($('#ckCodeModel').is(':checked')) {
        mModelCode = $('#txtSearchModel').val();
    }

    if ($('#ckNameModel').is(':checked')) {
        mModelName = $('#txtSearchModel').val();
    }

    searchViewModel = {};

    mLengthItemsGrid = ($("#PageQtyModelList").val() == undefined ? 10 : Number($("#PageQtyModelList").val()));
    mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

    searchViewModel.Start = mStartItemsGrid;
    searchViewModel.Length = mLengthItemsGrid;

    $("#LoadGrid").show('slow');

    $.ajax({
        url: '/PLM/RangePlan/_GetModelList',
        contextType: 'application/html;charset=utf-8',
        data: { searchViewModel },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $("#ListModel").html(data);
        initDragAndDrop();

        $('#Loading').modal('hide');

        if (mStartItemsGrid <= 0) {
            $("#PreviousModelList").attr('disabled', 'disabled');
        }
        else {
            $("#PreviousModelList").removeAttr('disabled');;
        }

        if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityModelList").val()) {
            $("#NextModelList").attr('disabled', 'disabled');
        }
        else {
            $("#NextModelList").removeAttr('disabled');;
        }
    });
};