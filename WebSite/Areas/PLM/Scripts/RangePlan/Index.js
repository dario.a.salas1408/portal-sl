﻿$(document).ready(function () {
    $("#btSearch").click(function () {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
        $.ajax({
            url: '/PLM/RangePlan/ListRangePlan',
            contextType: 'application/html;charset=utf-8',
            data: { txtCode: $("#txtCode").val(), txtSeason: $("#txtSeason").val(), txtCollection: $("#txtCollection").val(), txtEmployee: $("#txtEmployee").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#BodyRangePlan").html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            alert("error");
        });
    });
});