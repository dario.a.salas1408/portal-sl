﻿$(document).ready(function () {

    $("#btnSearch").click(function () {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
        $.ajax({
            url: '/PLM/MyStyles/MyCostSheetList',
            contextType: 'application/html;charset=utf-8',
            data: { txtStyle: $("#txtStyle").val(), txtCodeCostSheet: $("#txtCodeCostSheet").val(), txtDescription: $("#txtDescription").val()},
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyCS").html(data);

            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });
    });
});