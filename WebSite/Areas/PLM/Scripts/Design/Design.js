﻿$(document).ready(function () {
    $("#UpdateCorrectly").hide();
    if (formMode == "UpdateDesign")
    {
        $("#U_ModCode").prop('disabled', true);
        $("#U_ATGrp").prop('disabled', true);
    }
    if (formMode == "AddDesign") {
        $('#U_Status').val("-1");
        $("#U_Status").prop('disabled', true);
    }

    //$("#UpdateFile").fileinput({
    //    showUpload: false,
    //    showCaption: false,
    //    browseClass: "btn btn-primary btn-lg",
    //    fileType: "any",
    //    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
    //});

    $("#formDesign").validate({
        rules: {
            U_ModCode: "required",
            U_ModDesc: "required",
            U_ATGrp: "required",
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "AddDesign":
                    Save();
                    break;
                case "UpdateDesign":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    function Save() {
        $('#Loading').modal('show');
        var datos = new FormData();
        var fileInput = $("#UpdateFile").fileinput('getFileStack');
        if (fileInput.length > 0) {
            for (var j = 0; j < fileInput.length; j++) {
                datos.append(fileInput[j].name, fileInput[j]);
            }
        }

        var mUModCode = $("#U_ModCode").val();
        datos.append('mUModCode', mUModCode);
        datos.append('mPageKey', $("#Pagekey").val());


        var test = {
            Code: $("#Code").val(),
            U_Pic: $("#U_Pic").val(),
            U_PicR: $("#U_PicR").val(),
            U_ModCode: $("#U_ModCode").val(),
            U_ModDesc: $("#U_ModDesc").val(),
            U_FrgnDesc: $("#U_FrgnDesc").val(),
            U_Status: $("#U_Status").val(),
            U_LineCode: $("#U_LineCode").val(),
            U_ATGrp: $("#U_ATGrp").val(),
            U_ModGrp: $("#U_ModGrp").val(),
            U_SapGrp: $("#U_SapGrp").val(),
            U_Season: $("#U_Season").val(),
            U_CollCode: $("#U_CollCode").val(),
            U_AmbCode: $("#U_AmbCode").val(),
            U_CompCode: $("#U_CompCode").val(),
            U_GrpSCod: $("#U_GrpSCod").val(),
            U_Brand: $("#U_Brand").val(),
            U_Designer: $("#U_Designer").val(),
            U_Division: $("#U_Division").val(),
            PageKey: $("#Pagekey").val(),
            U_Comments: $("#U_Comments").val()
        };

        $.ajax({
            type: 'POST',
            url: '/PLM/Design/UploadFile',
            contentType: false,
            processData: false,
            data: datos,
        }).done(function (result) {
            $.ajax({
                url: "/PLM/Design/AddDesign",
                async: false,
                type: "POST",
                data: JSON.stringify(test),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data == "Ok") {
                        $('#Loading').modal('hide');
                        location.href = urlRedirect;
                    }
                    else {
                        $('#Loading').modal('hide');
                        ErrorPDM(data);
                    }
                }
            });
        }).fail(function () {
            alert("error");
        });
    }

    function Update() {

        $('#Loading').modal('show');
        var datos = new FormData();
        var fileInput = $("#UpdateFile").fileinput('getFileStack');
        if (fileInput.length > 0) {
            for (var j = 0; j < fileInput.length; j++) {
                datos.append(fileInput[j].name, fileInput[j]);
            }
        }
        var mUModCode = $("#U_ModCode").val();
        datos.append('mUModCode', mUModCode);
        datos.append('mPageKey', $("#Pagekey").val());

        var test = {
            Code: $("#Code").val(),
            U_Pic: $("#U_Pic").val(),
            U_Pic: $("#U_PicR").val(),
            U_ModCode: $("#U_ModCode").val(),
            U_ModDesc: $("#U_ModDesc").val(),
            U_FrgnDesc: $("#U_FrgnDesc").val(),
            U_Status: $("#U_Status").val(),
            U_LineCode: $("#U_LineCode").val(),
            U_ATGrp: $("#U_ATGrp").val(),
            U_ModGrp: $("#U_ModGrp").val(),
            U_SapGrp: $("#U_SapGrp").val(),
            U_Season: $("#U_Season").val(),
            U_CollCode: $("#U_CollCode").val(),
            U_AmbCode: $("#U_AmbCode").val(),
            U_CompCode: $("#U_CompCode").val(),
            U_GrpSCod: $("#U_GrpSCod").val(),
            U_Brand: $("#U_Brand").val(),
            U_Designer: $("#U_Designer").val(),
            U_Division: $("#U_Division").val(),
            PageKey: $("#Pagekey").val(),
            U_Comments: $("#U_Comments").val()
        };

        $.ajax({
            type: 'POST',
            url: '/PLM/Design/UploadFile',
            contentType: false,
            processData: false,
            data: datos,
        }).done(function (result) {
            $.ajax({
                url: "/PLM/Design/UpdateDesign",
                async: false,
                type: "POST",
                data: JSON.stringify(test),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data == "Ok") {
                        $('#Loading').modal('hide');

                        $("#UpdateCorrectly").show("slow");
                        $("#UpdateCorrectly").delay(1500).slideUp(1000);
                    }
                    else {
                        $('#Loading').modal('hide');
                        ErrorPDM(data);
                    }
                }
            });
        }).fail(function () {
            alert("error");
        });
    }

});

function ErrorPDM(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}