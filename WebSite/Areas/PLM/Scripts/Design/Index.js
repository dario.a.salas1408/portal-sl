﻿$(document).ready(function () {

	$("#btSearch").click(function () {
		if (typeof dt === 'undefined') {
			assetListVM.init();
		}
		else {
			assetListVM.refresh();
		}
	});

	$("#btnSearch").click(function () {

		var mListSearchUDF = $("input[id^=SearchItemGridUDF_]");
		var mListSearchUDFSelect = $("select[id^=SearchItemGridUDF_]");
		var mMappedUDFList = [];
		for (i = 0; i < mListSearchUDF.length; i++) {
			mMappedUDFList.push({
				UDFName: mListSearchUDF[i].id.substring(18),
				Value: $("#" + mListSearchUDF[i].id).val(),
			});
		}
		for (i = 0; i < mListSearchUDFSelect.length; i++) {
			mMappedUDFList.push({
				UDFName: mListSearchUDFSelect[i].id.substring(18),
				Value: $("#" + mListSearchUDFSelect[i].id).val(),
			});
		}

		searchViewModel = {};
		searchViewModel.pMappedUdf = mMappedUDFList;
		searchViewModel.CodeModel = $("#txtCodeModel").val();
		searchViewModel.NameModel = $("#txtNameModel").val();
		searchViewModel.SeasonModel = $("#txtSeasonModel").val();
		searchViewModel.GroupModel = $("#txtGroupModel").val();
		searchViewModel.BrandModel = $("#txtBrand").val();
		searchViewModel.CollectionModel = $("#txtCollection").val();
		searchViewModel.SubCollectionModel = $("#txtSubCollection").val();
		mStartItemsGrid = 0;
		GetItemsGrid();

	});

	function GetItemsGrid() {
		mLengthItemsGrid = ($("#PageQtyItemsGrid").val() == undefined ? 10 : Number($("#PageQtyItemsGrid").val()));
		mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

		searchViewModel.Start = mStartItemsGrid;
		searchViewModel.Length = mLengthItemsGrid;

		$("#LoadGrid").show('slow');

		$.ajax({
			url: '/PLM/Design/ListDesignGrid',
			contextType: 'application/html;charset=utf-8',
			data: { searchViewModel },
			type: 'POST',
			dataType: 'html'
		}).done(function (data) {

			$("#LoadGrid").hide();
			$("#BodyDesignGrid").html(data);
			$("#btnChooseGrid").prop('disabled', true);

			if (mStartItemsGrid <= 0) {
				$("#PreviousItemsGrid").attr('disabled', 'disabled');
			}
			else {
				$("#PreviousItemsGrid").removeAttr('disabled');;
			}

			if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityItemsGrid").val()) {
				$("#NextItemsGrid").attr('disabled', 'disabled');
			}
			else {
				$("#NextItemsGrid").removeAttr('disabled');;
			}

			updateLinesChanged(true);
		});

	}

	//$("#btnSearch").click(function () {
    //    $('#Loading').modal({
    //        backdrop: 'static',
    //        keyboard: true
    //    });

    //    $.ajax({
    //        url: '/PLM/Design/ListDesignGrid',
    //        contextType: 'application/html;charset=utf-8',
    //        data: {
    //        	txtCodeModel: $("#txtCodeModel").val(),
    //        	txtNameModel: $("#txtNameModel").val(),
    //        	txtSeasonModel: $("#txtSeasonModel").val(),
    //        	txtGroupModel: $("#txtGroupModel").val(),
    //        	txtBrand: $("#txtBrand").val(),
    //        	txtCollection: $("#txtCollection").val(),
    //        	txtSubCollection: $("#txtSubCollection").val(),
    //        	pStart: 0,
    //        	pLength: 10
    //        },
    //        type: 'POST',
    //        dataType: 'html'
    //    }).done(function (data) {


    //        $("#BodyDesignGrid").empty().append(data);

    //        $('#Loading').modal('hide');

    //    }).fail(function () {
    //        alert("error");
    //    });

    //});

    $("#DesignList").bind("click", function () {
        $('#products .item').addClass('list-group-item');
    });

    $("#DesignGrid").bind("click", function () {
        $('#products .item').removeClass('list-group-item'); $('#products .item').addClass('grid-group-item');
    });

});