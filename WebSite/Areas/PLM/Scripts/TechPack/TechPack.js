﻿$(document).ready(function () {

    $('#UpdateCorrectly').hide();

    $("#btnOK").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        switch (formMode) {
            case "Add":
                Add();
                break;
            case "Update":
                Update();
                break;
            default:
                alert('Imposible realizar una acción');
                break;
        }

    });


});

function AddRow() {
    var nextId = NextRowId("DivLineId");
    $.ajax({
        url: '/PLM/TechPack/_GetRow',
        contextType: 'application/html;charset=utf-8',
        data: { pNextId: nextId, pPageKey: $("#PageKey").val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        //$('#' + pTableId + ' tr:last').after(data);
        $('#RowContainer').append(data);
    }).fail(function () {
        alert("error");
    });
}

function NextRowId(pSearchId) {
    var mListTr = $("div[id^=" + pSearchId + "]");
    auxMax = 0;
    for (i = 0; i < mListTr.length ; i++) {
        if (mListTr[i].id.substring(pSearchId.length) > auxMax) {
            auxMax = mListTr[i].id.substring(pSearchId.length);
        }
    }

    return parseInt(auxMax) + 1;
}

function DeleteRow(pDivId)
{
    $("#" + pDivId).remove();
}


function Add()
{
    
}

function Update()
{
    var data = new FormData();

    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    var mTechPack = {
        Code: $("#Code").val(),
        ModelCode: $("#ModelCode").val(),
        ModelDesc: $("#ModelDesc").val(),
        Lines: []
    };

    var listTechPackLines = $("div[id^=DivLineId]");
    for (i = 0; i < listTechPackLines.length; i++) {
        var mTechPackId = listTechPackLines[i].id.substring(9);
        var fileInput = $("#file-ImgUpload" + mTechPackId).fileinput('getFileStack');
        mTechPack.Lines.push({
            "Code": $("#Code").val(),
            "LineId": mTechPackId,
            "U_InstCod": $("#Section" + mTechPackId).val(),
            "U_Sector": $("#Section" + mTechPackId + " option:selected").text(),
            "U_Instuct": $("#SectionName" + mTechPackId).val(),
            "U_Descrip": $("#Description" + mTechPackId).val(),
            "U_Active": (($("#mCheckBox" + mTechPackId).is(':checked')) ? "Y" : "N"),
            "ImageName": ((fileInput.length > 0) ? fileInput[0].name : ""),
            "ImgChanged": $("#fileImgUploadChange" + mTechPackId).val(),
            "IsNew" : $("#IsNew" + mTechPackId).val()
        });
        if (fileInput.length > 0) {
            for (var j = 0; j < fileInput.length; j++) {
                data.append(fileInput[j].name, fileInput[j]);
            }
        }

    }

    //var mPageKey = $('#Pagekey').val();

    data.append('ARGNSTechPackView', JSON.stringify(mTechPack));
    //data.append('pPageKey', JSON.stringify(mPageKey));

    $.ajax({
        url: "/PLM/TechPack/UpdateTechPack",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "Ok") {
                $('#Loading').modal('hide');

                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorPDM(data);
            }
        }
    });
}

function ErrorPDM(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}