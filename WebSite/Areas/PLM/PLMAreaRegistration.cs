﻿using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.PLM
{
    public class PLMAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PLM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PLM_default",
                "PLM/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new string[] { "ARGNS.WebSite.Areas.PLM.Controllers" }
            );
        }
    }
}