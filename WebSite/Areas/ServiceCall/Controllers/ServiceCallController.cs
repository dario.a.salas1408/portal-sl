﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARGNS.WebSite.Resources.Views.ServiceCall;
using ARGNS.View;
using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Attribute;
using ARGNS.Model.Implementations.View;
using AutoMapper;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.PDM;
using DataTables.Mvc;
using ARGNS.Model.Implementations.Portal;

namespace ARGNS.WebSite.Areas.ServiceCall.Controllers
{
    [CustomerAuthorize(Enums.Areas.ServiceCall)]
    public class ServiceCallController : Controller
    {
        private ServiceCallManagerView mServCall;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private ItemMasterManagerView mItemMasterManagerView;
        private UserManagerView mUserManagerView;

        public ServiceCallController()
        {
            mServCall = new ServiceCallManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mItemMasterManagerView = new ItemMasterManagerView();
            mUserManagerView = new UserManagerView();
        }


        [CustomerAuthorize(Enums.Areas.ServiceCall, Enums.Pages.ServiceCallList, Enums.Actions.List)]
        public ActionResult Index()
        {
            ServiceCallView mServiceCallView = new ServiceCallView();
            mServiceCallView.ComboList.ServiceStatusList.Add(new ServiceStatus(-99, ""));
            mServiceCallView.ComboList.ServiceStatusList.AddRange(mServCall.GetServiceCallStatusList(((UserView)Session["UserLogin"]).CompanyConnect));

            return View(mServiceCallView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActivityAction"></param>
        /// <param name="ActivityCode"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        public ActionResult Activities(string ActivityAction, string ActivityCode, string pFrom)
        {
            return RedirectToAction("ActivityAction", "/PLM/Activity", new { Areas = "PLM", pAction = ActivityAction, pCode = ActivityCode });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pAction"></param>
        /// <param name="pCode"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        public ActionResult Action(string pAction, string pCode, string pFrom)
        {
            ServiceCallView mView = null;
            ViewBag.Decimalformat = "{0:0.00}";
            ViewBag.URLRedirect = (pFrom == "MyService") ? "/ServiceCall/MyServiceCall/Index" : "/ServiceCall/ServiceCall/Index";

            switch (pAction)
            {
                case "Update":
                    ViewBag.FormMode = pAction;
                    mView = mServCall.GetServiceCallById(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pCode));
                    ViewBag.Tite = ARGNS.WebSite.Resources.Views.ServiceCall.ServiceCall.ServiceCallTitle + " " + mView.subject;
                    break;
                case "Add":
                    ViewBag.FormMode = pAction;
                    mView = mServCall.GetServiceCallById(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pCode));
                    ViewBag.Tite = ARGNS.WebSite.Resources.Views.ServiceCall.ServiceCall.ServiceCallTitle + " " + mView.subject;
                    if (!string.IsNullOrEmpty(((UserView)Session["UserLogin"]).BPCode))
                    {
                        mView.PortalBPCode = ((UserView)Session["UserLogin"]).BPCode;
                        mView.PortalBPName = mBusinessPartnerManagerView.GetBusinessPartnerListSearch(
                            ((UserView)Session["UserLogin"]).CompanyConnect,
                            ((UserView)Session["UserLogin"]).BPCode).Where(c => c.CardCode == mView.PortalBPCode).FirstOrDefault().CardName;
                    }
                    break;
                case "View":
                    ViewBag.FormMode = pAction;
                    mView = mServCall.GetServiceCallById(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pCode));
                    ViewBag.Tite = ARGNS.WebSite.Resources.Views.ServiceCall.ServiceCall.ServiceCallTitle + " " + mView.subject;
                    break;
            }

            mView.Action = pAction;
            mView.FromPage = pFrom;

            TempData["ServiceCallView"] = mView;
            List<UDF_ARGNS> mOITMUDFList = mItemMasterManagerView.GetItemMasterUDF(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);
            return View("_ServiceCall", Tuple.Create(mView, mOITMUDFList));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(ServiceCallView model)
        {
            short? userId = null;
            ServiceCallView mModelTemp = (ServiceCallView)TempData["ServiceCallView"];
            model.AttachmentList = mModelTemp.AttachmentList;
            
            TempData["ServiceCallView"] = model;
            string mResult = mServCall.UpdateServiceCall(((UserView)Session["UserLogin"]).CompanyConnect, model, userId);

            return Json(mResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(ServiceCallView model)
        {
            short? userId = null;
            ServiceCallView mModelTemp = (ServiceCallView)TempData["ServiceCallView"];
            model.AttachmentList = mModelTemp.AttachmentList;
           
            TempData["ServiceCallView"] = model;
            JsonObjectResult mResult = mServCall.AddServiceCall(((UserView)Session["UserLogin"]).CompanyConnect, model, userId);
            mResult.AddUpdateMsg = Resources.Views.ServiceCall.ServiceCall.AddResultMsg.Replace("{Code}", mResult.Code);
            return Json(mResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBpCode"></param>
        /// <param name="pPriority"></param>
        /// <param name="pSubject"></param>
        /// <param name="pRefNum"></param>
        /// <param name="pDate"></param>
        /// <param name="pStatus"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ServiceCallList(string pBpCode = "", string pPriority = "", string pSubject = "", string pRefNum = "", DateTime? pDate = null, short? pStatus = null)
        {
            DateTime? mDate = null;

            if (pDate != null)
            {
                mDate = Convert.ToDateTime(pDate);
            }

            List<ServiceCallView> ListOppor = mServCall.GetServiceCallListSearch(((UserView)Session["UserLogin"]).CompanyConnect, pBpCode, pPriority, pSubject, pRefNum, mDate, pStatus);

            List<ServiceStatus> mServiceStatusList = new List<ServiceStatus>();
            mServiceStatusList.Add(new ServiceStatus(-99, ""));
            mServiceStatusList.AddRange(mServCall.GetServiceCallStatusList(((UserView)Session["UserLogin"]).CompanyConnect));

            return PartialView("_ServiceCallList", Tuple.Create(ListOppor, mServiceStatusList));
        }

        //[HttpPost]
        //public PartialViewResult _Customers(string pVendorCode = "", string pVendorName = "")
        //{
        //    pVendorCode = pVendorCode.TrimStart().TrimEnd();
        //    pVendorName = pVendorName.TrimStart().TrimEnd();
        //    List<BusinessPartnerView> ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer).Where(c => (c.CardCode.ToUpper().Contains(pVendorCode.ToUpper())) && ((c.CardName == null ? false : c.CardName.ToUpper().Contains(pVendorName.ToUpper())))).ToList();

        //    return PartialView("_Customers", ListReturn);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Customers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, true,
                    searchViewModel.pCardCode, searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, 
                    ((UserView)Session["UserLogin"]).SECode, ((UserView)Session["UserLogin"]).BPGroupId, 
                    true, requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, 
                    Enums.BpType.Customer, true, ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, 
                    false, null, null, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult _EquipmentCard(string pItemCode = "", string pItemName = "")
        {
            List<EquipmentCardView> ListReturn = mServCall.GetEquipmentCardListSearch(((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, pItemName);

            return PartialView("_EquipmentCard", ListReturn);
        }

        [HttpPost]
        public JsonResult GetBp(string Id)
        {
            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);

            if (mBP != null)
            {
                mBP.ErrorResponse = "Ok";
                return Json(mBP, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new BusinessPartnerView { ErrorResponse = @Error.WebServError }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = GetItems(searchViewModel.pMappedUdf, "", searchViewModel.pItemCode, searchViewModel.pItemName, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult AddActivity()
        {
            Session["ServiceCallViewSession"] = (ServiceCallView)TempData["ServiceCallView"];
            return RedirectToAction("ActivityAction", "CRMActivity", new { Area = "CRM", pAction = "Add", pCode = "0", pFrom = "ServiceCall" });
        }

        public ActionResult NewServiceCallActivity(string pId)
        {
            try
            {
                ServiceCallView pModel = (ServiceCallView)Session["ServiceCallViewSession"];

                ServiceCallActivitiesView mNew = pModel.ActivitiesList.Find(c => c.ClgID == Convert.ToInt32(pId));
                if (mNew == null)
                {
                    ActivitiesManagerView mManagerView = new ActivitiesManagerView();
                    mNew = new ServiceCallActivitiesView();
                    mNew.ClgID = Convert.ToInt32(pId);
                    mNew.SrvcCallId = pModel.callID;

                    if (pModel.ActivitiesList.Count > 0)
                    {
                        mNew.Line = (short)(pModel.ActivitiesList.Max(c => c.Line) + 1);
                    }
                    else
                    {
                        mNew.Line = 1;
                    }

                    mNew.Activity = mManagerView.GetActivity(Convert.ToInt32(pId), ((UserView)Session["UserLogin"]).CompanyConnect);
                    pModel.ActivitiesList.Add(mNew);
                    TempData["ServiceCallView"] = pModel;
                }

                ViewBag.FormMode = pModel.Action;
                ViewBag.URLRedirect = (pModel.FromPage == "MyService") ? "/ServiceCall/MyServiceCall/Index" : "/ServiceCall/ServiceCall/Index";

                List<UDF_ARGNS> mOITMUDFList = mItemMasterManagerView.GetItemMasterUDF(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);

                return View("_ServiceCall", Tuple.Create(pModel, mOITMUDFList));
            }
            catch (Exception)
            {
                throw;
            }

        }

        [HttpPost]
        public JsonResult _ItemGroup(string Id, int pUserId)
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            return Json(mItemMasterManagerView.GetItemGroup(((UserView)Session["UserLogin"]).CompanyConnect,
                "Y", "Y", "", Id, pUserId, false, Mapper.Map<List<WarehousePortal>>(pWarehousePortalList)),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pMappedUdf"></param>
        /// <param name="pModeCode"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        private JsonObjectResult GetItems(List<UDF_ARGNS> pMappedUdf, string pModeCode = "",
            string pItemCode = "", string pItemName = "", int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(
                    ((UserView)Session["UserLogin"]).CompanyConnect, "Y", "", 
                    "","N","", 
                    pMappedUdf, pModeCode, 
                    ((UserView)Session["UserLogin"]).IdUser, pItemCode, pItemName,
                    false, Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                    false, "", "", pStart, pLength, pOrderColumn);

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pData"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult UploadAttachment(FormCollection pData)
        {
            string mFileName = string.Empty;
            ServiceCallView mModel = (ServiceCallView)TempData["ServiceCallView"];
            string mFolder = "~/images";
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;
                    AttachmentView mObj = new AttachmentView();
                    foreach (string mKey in mFiles)
                    {
                        HttpPostedFileBase mFilePost = mFiles[mKey];
                        mFileName = mFilePost.FileName;
                        string path = mFolder + "/" + mFileName;
                        mFileName = Server.MapPath(path);
                        mFilePost.SaveAs(mFileName);

                        mObj.FileExt = System.IO.Path.GetExtension(mFileName).Split('.')[1];
                        mObj.FileName = System.IO.Path.GetFileNameWithoutExtension(mFileName);
                        mObj.srcPath = Server.MapPath(mFolder);
                        mObj.Date = DateTime.Now;
                        //mObj.AbsEntry = mModel.callID;
                        mObj.IsNew = true;

                        if (mModel.AttachmentList.Count > 0)
                        {
                            mObj.Line = mModel.AttachmentList.Max(c => c.Line) + 1;
                        }
                        else
                        {
                            mObj.Line = 1;
                        }

                        mModel.AttachmentList.Add(mObj);
                    }
                }

                TempData["ServiceCallView"] = mModel;
                return PartialView("_AttachmentList", mModel.AttachmentList);
            }
            catch (Exception)
            {
                //Limpio la lista y vuelvo a guardar el modelo en los temporales por si necesita subir nuevamente un attachemente sin salir de la pagina.
                mModel.AttachmentList = new List<AttachmentView>();
                TempData["ServiceCallView"] = mModel;
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetItemsModel()
        {
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return PartialView("_Items", mOITMUDFList);
        }
    }
}