﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.ServiceCall.Controllers
{
    [CustomerAuthorize(Enums.Areas.ServiceCall)]
    public class MyServiceCallController : Controller
    {
        private ServiceCallManagerView mServCall;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private ItemMasterManagerView mItemMasterManagerView;

        public MyServiceCallController()
        {
            mServCall = new ServiceCallManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mItemMasterManagerView = new ItemMasterManagerView();
        }

        [CustomerAuthorize(Enums.Areas.ServiceCall, Enums.Pages.MyServiceCallList, Enums.Actions.List)]
        public ActionResult Index()
        {
            ServiceCallView mServiceCallView = new ServiceCallView();
            mServiceCallView.ComboList.ServiceStatusList.Add(new ServiceStatus(-99, ""));
            mServiceCallView.ComboList.ServiceStatusList.AddRange(mServCall.GetServiceCallStatusList(((UserView)Session["UserLogin"]).CompanyConnect));

            return View(mServiceCallView);
        }

        [HttpPost]
        public PartialViewResult ServiceCallList(string pBpCode = "", string pPriority = "", string pSubject = "", string pRefNum = "", DateTime? pDate = null, short? pStatus = null)
        {
            DateTime? mDate = null;
            List<ServiceCallView> ServiceCallList = null;

            if (pDate != null)
            {
                mDate = Convert.ToDateTime(pDate);
            }

            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                ServiceCallList = mServCall.GetServiceCallListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).BPCode, pPriority, pSubject, pRefNum, mDate, pStatus, null);
            }

            if (((UserView)Session["UserLogin"]).IsUser)
            {
                ServiceCallList = mServCall.GetServiceCallListSearchByUser(((UserView)Session["UserLogin"]).CompanyConnect, "", pPriority, pSubject, pRefNum, mDate, pStatus, (short)((UserView)Session["UserLogin"]).IdUser);
            }
            List<ServiceStatus> mServiceStatusList = new List<ServiceStatus>();
            mServiceStatusList.Add(new ServiceStatus(-99, ""));
            mServiceStatusList.AddRange(mServCall.GetServiceCallStatusList(((UserView)Session["UserLogin"]).CompanyConnect));

            return PartialView("_ServiceCallList", Tuple.Create(ServiceCallList, mServiceStatusList));
        }


        [HttpPost]
        public PartialViewResult _Customers(string pVendorCode = "", string pVendorName = "")
        {
            pVendorCode = pVendorCode.TrimStart().TrimEnd();
            pVendorName = pVendorName.TrimStart().TrimEnd();
            List<BusinessPartnerView> ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer).Where(c => (c.CardCode.ToUpper().Contains(pVendorCode.ToUpper())) && ((c.CardName == null ? false : c.CardName.ToUpper().Contains(pVendorName.ToUpper())))).ToList();

            return PartialView("_Customers", ListReturn);
        }
    }
}