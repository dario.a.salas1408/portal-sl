﻿$(document).ready(function () {

    $('.input-group.date').datepicker({}); 

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    $("#btSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var dateTxtDate = $("#txtDate").datepicker("getDate");
        $.ajax({
            url: '/ServiceCall/ServiceCall/ServiceCallList',
            contextType: 'application/html;charset=utf-8',
            data: { pBpCode: $("#txtCodeVendor").val(), pPriority: $("#cmbPriority").val(), pSubject: $("#txtSubject").val(), pRefNum: $("#txtRefNumber").val(), pDate: (isNaN(dateTxtDate) == true ? null : ((dateTxtDate.getMonth() + 1) + "/" + dateTxtDate.getDate() + "/" + dateTxtDate.getFullYear())), pStatus: $("#SelectStatus").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyServiceCall").html(data);
            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');

}

