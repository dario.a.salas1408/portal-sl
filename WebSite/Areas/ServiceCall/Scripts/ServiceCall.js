﻿$(document).ready(function () {

    //Inicializar los controles de la pagina---------------------------------------------------
    $('.input-group.date').datepicker({});   
    $("#UpdateCorrectly").hide();
    $("#file-1").fileinput({ 'showUpload': false, 'previewFileType': 'any' });

    //Inicializar POPUP   
    $("#Load").hide();
    $("#LoadEquip").hide();
    
    $('#myModal').on('show.bs.modal', function (e) {
        $("#ItemsBody").html("");
    });
    $('#myModalEquipCard').on('show.bs.modal', function (e) {
        $("#EquipCardBody").html("");
    });

    switch (formMode) {
        case "Update":
            $("#customer").attr('disabled', 'disabled');           
            break;
    }

    //------------------------------------------------------------------------------------------

    //Upload Attachment
    $("#UploadAttach").click(function () {

        var fileInput = document.getElementById("file-1");
        var mAttachList = [];
        var data = new FormData();
        var mCallID = $("#Code").val();
       
        $("#alertAtt").hide();
        if (fileInput.files.length == 0) {
            $("#alertAtt").show();
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        if (fileInput.files.length > 0) {
            for (var i = 0; i < fileInput.files.length; i++) {
                data.append(fileInput.files[i].name, fileInput.files[i]);
            }
        }

        data.append('mCode', mCallID);      

        $.ajax({
            type: 'POST',
            url: '/ServiceCall/ServiceCall/UploadAttachment',
            contentType: false,
            processData: false,
            data: data,
        }).done(function (result) {
            $('#file-1').fileinput('clear');
            $("#FilesItems").html(result);
            $('#Loading').modal('hide');
            $("#alertAtt").hide();

        }).fail(function () {
            alert("error");
        });

    });

    //Buscar Equipment Card
    $("#SearchEquipCard").click(function () {

        var mItemCode;
        var mItemName;

        if ($('#ckCode').is(':checked')) {
            mItemCode = $('#txtSearch').val();
        }

        if ($('#ckName').is(':checked')) {
            mItemName = $('#txtSearch').val();
        }

        $("#LoadEquip").show('slow');

        $.ajax({
            url: '/ServiceCall/ServiceCall/_EquipmentCard',
            contextType: 'application/html;charset=utf-8',
            data: { pItemCode: mItemCode, pItemName: mItemName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadEquip").hide();
            $("#EquipCardBody").html(data);           
        });

    });

    //Buscar Items
    $("#SearchItems").click(function () {

        if (typeof assetListVM_ListItems.dt === 'undefined' || assetListVM_ListItems.dt == null) {
            assetListVM_ListItems.init();
            $("#btnChoose").prop('disabled', true);
        }
        else {
            assetListVM_ListItems.refresh();
            $("#btnChoose").prop('disabled', true);
        }

    });
    
    //Buscar BusinessPartner
    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });  

    //Guardar/Actualizar Service Call
    $("#btnOk").click(function () {
        var form = $("#formServiceCall");
        form.submit();
    });

    $("#formServiceCall").validate({
        rules: {
            customer: "required",
            subject: "required"
        },
        messages: {
            customer: "Please insert Business Partner",
            subject: "Please insert Subject"
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Add();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });
});

function Update() {

    $('#Loading').modal({
        backdrop: 'static'
    })
    var CreateDate = $("#createDate").datepicker("getDate");
  
    var ServiceCallView = {
        customer: $("#customer").val(),
        custmrName: $("#custmrName").val(),
        createDate: (isNaN(CreateDate) == true ? null : (CreateDate.getMonth() + 1) + "/" + CreateDate.getDate() + "/" + CreateDate.getFullYear()),
        CustomerPhone: $("#CustomerPhone").val(),
        NumAtCard: $("#NumAtCard").val(),
        subject: $("#subject").val(),
        itemCode: $("#itemCode").val(),
        itemName: $('#itemName').val(),
        itemGroup: $('#itemGroup').val(),
        contractID: $('#contractID').val(),
        callID: $('#callID').val(),
        status: $('#status').val(),
        priority: $('#priority').val(),
        origin: $('#origin').val(),
        problemTyp: $('#problemTyp').val(),
        ProSubType: $('#ProSubType').val(),
        callType: $('#callType').val(),
        technician: $('#technician').val(),
        assignee: $('#assignee').val(),
        descrption: $('#descrption').val(),
        resolution: $('#resolution').val(),
        contctCode: $('#contctCode').val(),
        ActivitiesList:[]
    };

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);

        ServiceCallView.ActivitiesList.push({
            "Line": input.attr('id'),           
            "ClgID": $("#ClgID" + input.attr('id')).html()
        });
    });

    $('#Loading').modal({
        backdrop: 'static'
    })

    $.ajax({
        url: "/ServiceCall/ServiceCall/Update",
        async: false,
        type: "POST",
        data: JSON.stringify(ServiceCallView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorMessage(data);
            }
        }
    });
}

function Add() {

    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    var CreateDate = $("#createDate").datepicker("getDate");

    var ServiceCallView = {
        customer: $("#customer").val(),
        custmrName: $("#custmrName").val(),
        createDate: (isNaN(CreateDate) == true ? null : (CreateDate.getMonth() + 1) + "/" + CreateDate.getDate() + "/" + CreateDate.getFullYear()),
        CustomerPhone: $("#CustomerPhone").val(),
        NumAtCard: $("#NumAtCard").val(),
        subject: $("#subject").val(),
        itemCode: $("#itemCode").val(),
        itemName: $('#itemName').val(),
        itemGroup: $('#itemGroup').val(),
        contractID: $('#contractID').val(),
        callID: $('#callID').val(),
        status: $('#status').val(),
        priority: $('#priority').val(),
        origin: $('#origin').val(),
        problemTyp: $('#problemTyp').val(),
        ProSubType: $('#ProSubType').val(),
        callType: $('#callType').val(),
        technician: $('#technician').val(),
        assignee: $('#assignee').val(),
        descrption: $('#descrption').val(),
        resolution: $('#resolution').val(),
        contctCode: $('#contctCode').val(),
        ActivitiesList:[]
    };

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);     

        ServiceCallView.ActivitiesList.push({
            "Line": input.attr('id'),
            "ClgID": $("#ClgID" + input.attr('id')).html()
        });
    });


    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    $.ajax({
        url: "/ServiceCall/ServiceCall/Add",
        async: false,
        type: "POST",
        data: JSON.stringify(ServiceCallView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data.ServiceAnswer == "Ok") {
                $('#Loading').modal('hide');
                DocumentAddUpdateMessage(data.AddUpdateMsg, window.location.href, urlRedirect);
            }
            else {
                $('#Loading').modal('hide');
                ErrorMessage(data.ErrorMsg);
            }
        }
    });
}

function ErrorMessage(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function DocumentAddUpdateMessage(message, urlAddNew, urlGoBack) {
    $("#DocumentAddUpdateMessageContent").empty().append("<strong>" + message + "</strong>");
    $("#DocumentAddUpdateMessageContent").show('slow');
    $('#DocumentAddUpdateMessageBox').modal({
        backdrop: 'static',
        keyboard: true
    });
    $("#AddNewDocumentAddUpdateMessage").click(function () {
        $('#Loading').modal({
            backdrop: 'static'
        })
        location.href = urlAddNew;
    });
    $("#GoBackDocumentAddUpdateMessage").click(function () {
        $('#Loading').modal({
            backdrop: 'static'
        })
        location.href = urlGoBack;
    });
}

function SetVendor(pCode, pName) {
    var mName = pName.replace("##", "'");

    $('#customer').val(pCode);
    $('#custmrName').val(mName);
    $('#VendorModal').modal('hide');

    $.ajax({
        url: "/ServiceCall/ServiceCall/GetBp",
        type: "POST",
        data: { Id: pCode },
        dataType: 'json',
        success: function (data, text) {
            if (data.ErrorResponse == "Ok") {

                $("#contctCode").empty();
                $.each(data.ListContact, function (key) {
                    $('#contctCode').append($('<option>', {
                        value: data.ListContact[key].CntctCode,
                        text: data.ListContact[key].Name
                    }));
                });

                $("#CustomerPhone").val(data.Phone1);
            }
            else {
                ErrorPO(data.ErrorResponse);
            }

        }
    });

}

function SetItems(pCode, pName,pGroup) {
   
    $('#itemCode').val(pCode);
    $('#itemName').val(pName);
    $('#itemGroup').val(pGroup);
    $('#manufSN').val("");
    $('#internalSN').val("");
    $('#myModal').modal('hide');

}

function SetEquipment(pCode, pName, pSerialNumManu,pInternalSerial) {

    var mName = pName.replace("##", "'");

    $('#itemCode').val(pCode);
    $('#itemName').val(mName);   
    $('#manufSN').val(pSerialNumManu);
    $('#internalSN').val(pInternalSerial);

    $.ajax({
        url: "/ServiceCall/ServiceCall/_ItemGroup",
        type: "POST",
        data: { Id: pCode },
        dataType: 'json',
        success: function (data, text) {            
            $('#itemGroup').val(data);
        }
    });

    $('#myModalEquipCard').modal('hide');

}

function GetItemsModel() {
    $.ajax({
        url: '/ServiceCall/ServiceCall/GetItemsModel',
        contextType: 'application/html;charset=utf-8',
        data: {},
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#ModalItemsTable").html(data);
    });
}