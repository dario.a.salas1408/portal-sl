﻿$(document).ready(function () {

    $('.input-group.date').datepicker({});
    $("#LoadVendor").hide();


    $('#VendorModal').on('show.bs.modal', function (e) {
        $("#ModalBodyVendor").html("");
    });

    $("#SearchVendor").click(function () {

        var mVendorCode;
        var mVendorName;

        if ($('#ckCodeVendor').is(':checked')) {
            mVendorCode = $('#txtSearchVendor').val();
        }

        if ($('#ckNameVendor').is(':checked')) {
            mVendorName = $('#txtSearchVendor').val();
        }

        $("#LoadVendor").show('slow');

        $.ajax({
            url: '/ServiceCall/MyServiceCall/_Customers',
            contextType: 'application/html;charset=utf-8',
            data: { pVendorCode: mVendorCode, pVendorName: mVendorName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadVendor").hide('slow');
            $("#ModalBodyVendor").html(data);


        }).fail(function () {
            alert("error");
        }).always(function () {

        });

    });

    $("#btSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var dateTxtDate = $("#txtDate").datepicker("getDate");
        $.ajax({
            url: '/ServiceCall/MyServiceCall/ServiceCallList',
            contextType: 'application/html;charset=utf-8',
            data: { pBpCode: $("#txtCodeVendor").val(), pPriority: $("#cmbPriority").val(), pSubject: $("#txtSubject").val(), pRefNum: $("#txtRefNumber").val(), pDate: (isNaN(dateTxtDate) == true ? null : ((dateTxtDate.getMonth() + 1) + "/" + dateTxtDate.getDate() + "/" + dateTxtDate.getFullYear())), pStatus: $("#SelectStatus").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyServiceCall").html(data);
            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');

}

