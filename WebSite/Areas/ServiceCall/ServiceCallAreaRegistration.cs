﻿using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.ServiceCall
{
    public class ServiceCallAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ServiceCall";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ServiceCall_default",
                "ServiceCall/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}