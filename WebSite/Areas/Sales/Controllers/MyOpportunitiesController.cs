﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
    [CustomerAuthorize(Enums.Areas.Sales)]
    public class MyOpportunitiesController : Controller
    {
        private OpportunityManagerView mOpporService;
        private SaleOrderManagerView mPSOManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private UserManagerView mUserManagerView;

        public MyOpportunitiesController()
        {
            mOpporService = new OpportunityManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mUserManagerView = new UserManagerView();
            mPSOManagerView = new SaleOrderManagerView();

        }

         [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.MyOpportunitiesList, Enums.Actions.List)]
        public ActionResult Index()
        {
            return View(new List<OpportunitiesView>());
        }


        [HttpPost]
        public PartialViewResult ListOpportunities(string txtCodeVendor = "", string txtDate = "", string pOpporName = "", string txtDocStatus = "", string pOwnerCode = "", string pSECode = "")
        {

            string Vendor = txtCodeVendor;
            DateTime? Date = null;
            int? mOwner = null;
            int? SalesEmpl = null;
            List<OpportunitiesView> ListOppor = null;

            if (txtDate != "")
            {
                Date = Convert.ToDateTime(txtDate);
            }

            if (pOwnerCode != "")
            {
                mOwner = Convert.ToInt32(pOwnerCode);
            }

            if (pSECode != "")
            {
                SalesEmpl = Convert.ToInt32(pSECode);
            }

            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                ListOppor = mOpporService.GetOpportunityListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).BPCode, SalesEmpl, pOpporName, mOwner, Date, txtDocStatus);
            }

            if (((UserView)Session["UserLogin"]).IsUser)
            {
                ListOppor = mOpporService.GetOpportunityListSearch(((UserView)Session["UserLogin"]).CompanyConnect, Vendor, SalesEmpl, pOpporName, mOwner, Date, txtDocStatus);
            }

            return PartialView("_OpportunitiesList", ListOppor);
        }

        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mPSOManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }
    }
}