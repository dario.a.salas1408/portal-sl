﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Draft;
using ARGNS.WebSite.Resources.Views.SalesInvoices;
using AutoMapper;
using CrystalDecisions.Shared;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
	[CustomerAuthorize(Enums.Areas.Sales)]
	public class SalesInvoiceController : Controller
	{
		private BusinessPartnerManagerView mBusinessPartnerManagerView;
		private SalesInvoiceManagerView mSIManagerView;
		private DraftManagerView mDraftManagerView;
		private CRPageMapManagerView mCRPageMapManagerView;
		private UserManagerView mUserManagerView;

		public SalesInvoiceController()
		{
			mSIManagerView = new SalesInvoiceManagerView();
			mBusinessPartnerManagerView = new BusinessPartnerManagerView();
			mDraftManagerView = new DraftManagerView();
			mCRPageMapManagerView = new CRPageMapManagerView();
			mUserManagerView = new UserManagerView();

			Mapper.CreateMap<Draft, SalesInvoiceView>();
			Mapper.CreateMap<DraftLine, SalesInvoiceLineView>();
		}

		[CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.SalesInvoice, Enums.Actions.List)]
		public ActionResult Index()
		{
			try
			{
				List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
				List<SalesInvoiceView> ListSI = new List<SalesInvoiceView>();
				string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

				if (System.Web.HttpRuntime.Cache["SIList" + mUser] != null)
				{
					ListSI = (List<SalesInvoiceView>)System.Web.HttpRuntime.Cache["SIList" + mUser];
				}

				return View(Tuple.Create(ListSI, ListBp));
			}
			catch (Exception ex)
			{
				TempData["Error"] = ex.InnerException;
				return RedirectToAction("Index", "Error");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ActionSalesInvoice"></param>
		/// <param name="IdSI"></param>
		/// <param name="fromController"></param>
		/// <returns></returns>
		public ActionResult ActionSalesInvoice(string ActionSalesInvoice, int IdSI, string fromController)
		{
			SalesInvoiceView mSIView = null;
			switch (fromController)
			{
				case "SalesInvoice":
					ViewBag.URLRedirect = "/Sales/SalesInvoice/Index";
					ViewBag.controllerName = "SalesInvoice";
					break;
				case "MyDocuments":
					ViewBag.URLRedirect = "/Sales/SalesDraft/Index";
					ViewBag.controllerName = "SalesDraft";
					break;
				default:
					ViewBag.URLRedirect = "/Home";
					ViewBag.controllerName = "Home";
					break;
			}
			switch (ActionSalesInvoice)
			{
				case "View":
					ViewBag.Tite = SalesInvoices.VSI;
					ViewBag.FormMode = ActionSalesInvoice;
					mSIView = mSIManagerView.GetSalesInvoice(IdSI, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
					mSIView.Lines.Select(c => { c.ListCurrency = mSIView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();
					break;
				case "ViewDraft":
					ViewBag.Tite = Drafts.SID;
					ViewBag.FormMode = "View";
					ViewBag.From = fromController;
					DraftView draft = mDraftManagerView.GetDraftById(IdSI, ((UserView)Session["UserLogin"]).CompanyConnect);
					mSIView = new SalesInvoiceView();
					mSIView = Mapper.Map<SalesInvoiceView>(draft);
					mSIView.Lines = Mapper.Map<List<SalesInvoiceLineView>>(draft.Lines);

					//mSO = mPSOManagerView.GetSOInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mPSOView);
					//mPSOView = Mapper.Map<SaleOrderView>(mSO);
					//mPSOView.Lines = Mapper.Map<List<SaleOrderLineView>>(mSO.Lines);
					mSIView.Lines.Select(c => { c.ListCurrency = mSIView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();
					break;
			}

			return View("_SalesInvoice", Tuple.Create(mSIView, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesInvoice, ((UserView)Session["UserLogin"]).CompanyConnect)));
		}


		//[HttpPost]
		//public PartialViewResult _Vendors(string pVendorCode = "", string pVendorName = "")
		//{
		//    IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

		//    List<BusinessPartnerView> ListReturn;
		//    pVendorCode = pVendorCode.TrimStart().TrimEnd();
		//    pVendorName = pVendorName.TrimStart().TrimEnd();

		//    if (((UserView)Session["UserLogin"]).BPCode == null)
		//    {
		//        //ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer).Where(c => (c.CardCode.ToUpper().Contains(pVendorCode.ToUpper())) && ((c.CardName == null ? false : c.CardName.ToUpper().Contains(pVendorName.ToUpper())))).ToList();
		//        ListReturn = mapper.Map<List<BusinessPartnerView>>(mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, pVendorCode, pVendorName, ((UserView)Session["UserLogin"]).IsSalesEmployee, ((UserView)Session["UserLogin"]).SECode).BusinessPartnerSAPList);
		//    }
		//    else
		//    {
		//        //ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer).Where(c => c.CardCode == ((UserView)Session["UserLogin"]).BPCode).ToList();
		//        ListReturn = mapper.Map<List<BusinessPartnerView>>(mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, ((UserView)Session["UserLogin"]).BPCode, pVendorName, false, null).BusinessPartnerSAPList);
		//    } 

		//    return PartialView("_Vendors", ListReturn);
		//}

            /// <summary>
            /// 
            /// </summary>
            /// <param name="requestModel"></param>
            /// <param name="searchViewModel"></param>
            /// <returns></returns>
		[HttpPost]
		public ActionResult _Customers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
		{
			IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

			List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
			JsonObjectResult mJsonObjectResult = new JsonObjectResult();

			if (((UserView)Session["UserLogin"]).BPCode == null)
			{
				mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, 
                    Enums.BpType.Customer, false, 
                    searchViewModel.pCardCode, searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, 
                    ((UserView)Session["UserLogin"]).SECode, 
                    ((UserView)Session["UserLogin"]).BPGroupId, true, requestModel.Start, 
                    requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
			}
			else
			{
				mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, 
                    Enums.BpType.Customer, false, 
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, 
                    false, null, null, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
			}

			return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult GetListSI([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
		{
			IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();

			List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

			string vendor = searchViewModel.pCodeVendor;
			DateTime? date = null;
			int? docNum = null;
			string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

			if (searchViewModel.pDate != "")
			{
				date = Convert.ToDateTime(searchViewModel.pDate);
			}

			if (searchViewModel.pNro != "")
			{
				docNum = Convert.ToInt32(searchViewModel.pNro);
			}

			JsonObjectResult mJsonObjectResult = mSIManagerView.GetSalesInvoiceListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
				vendor, date, docNum, searchViewModel.pDocStatus,
				searchViewModel.pOwnerCode, searchViewModel.pSECode,
				requestModel.Start, requestModel.Length,
				mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

			mapper = new MapperConfiguration(cfg => { cfg.CreateMap<SalesInvoiceSAP, SalesInvoiceView>(); }).CreateMapper();

			return Json(new DataTablesResponse(requestModel.Draw,
				mapper.Map<List<SalesInvoiceView>>(mJsonObjectResult.SalesInvoiceSAPList),
				Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
				Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
				JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
		{
			List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
			return PartialView("_ListSE", listSE);
		}

		[HttpPost]
		public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
		{
			return PartialView("_EmployeesSap", mSIManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
		}

        /// <summary>
		/// 
		/// </summary>
		/// <param name="reportUrl"></param>
		/// <param name="docEntry"></param>
		public void DownloadPDF(string reportUrl, int docEntry)
        {
            ARGNS.Util.CrystalReports crystalReports = new ARGNS.Util.CrystalReports();
            var report = crystalReports.GeneratePDFFile(docEntry, 13,
                System.IO.Path.Combine(Server.MapPath("~/Reports"), reportUrl),
                ((UserView)Session["UserLogin"]).CompanyConnect.Server,
                ((UserView)Session["UserLogin"]).CompanyConnect.DbUserName,
                ((UserView)Session["UserLogin"]).CompanyConnect.DbPassword,
                ((UserView)Session["UserLogin"]).CompanyConnect.CompanyDB,
                ((UserView)Session["UserLogin"]).CompanyConnect.ServerType == (int)Enums.eServerType.dst_HANA);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";


            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat,
            System.Web.HttpContext.Current.Response,
            true,
            "SalesInvoice_" + docEntry.ToString());
        }
    }
}