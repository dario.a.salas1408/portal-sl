﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.Opportunities;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
    [CustomerAuthorize(Enums.Areas.Sales)]
    public class OpportunitiesController : Controller
    {
        private OpportunityManagerView mOpporService;
        private SaleOrderManagerView mPSOManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private UserManagerView mUserManagerView;
        private ActivitiesManagerView mActServ;

        public OpportunitiesController()
        {
            mOpporService = new OpportunityManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mUserManagerView = new UserManagerView();
            mPSOManagerView = new SaleOrderManagerView();
            mActServ = new ActivitiesManagerView();
        }

        [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.OpportunitiesList, Enums.Actions.List)]
        public ActionResult Index()
        {
            return View(new List<OpportunitiesView>());
        }

        public ActionResult Action(string pAction, string pCode, string pFrom)
        {
            OpportunitiesView mView = null;
            ViewBag.Decimalformat = "{0:0.00}";
            ViewBag.URLRedirect = (pFrom == "MyOpportunity") ? "/Sales/MyOpportunities/Index" : "/Sales/Opportunities/Index";

            switch (pAction)
            {
                case "Update":
                    ViewBag.FormMode = pAction;
                    mView = mOpporService.GetOportunityById(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pCode));
                    ViewBag.Tite = Opportunities.Opportinity + " " + mView.Name;
                    break;
                case "Add":
                    ViewBag.FormMode = pAction;
                    mView = mOpporService.GetOportunityById(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pCode));
                    ViewBag.Tite = Opportunities.Opportinity + " " + mView.Name;
                    break;
            }

            mView.Action = pAction;
            mView.FromPage = pFrom;

            TempData["Opportunities"] = mView;

            return View("_Opportunities", mView);
        }

        [HttpPost]
        public ActionResult _AddNewLine()
        {
            ViewBag.Decimalformat = "{0:0.00}";
            int mLastLine = 0;
            OpportunitiesView model = (OpportunitiesView)TempData["Opportunities"];
            model.OpportunitiesLines.ForEach(c => c.Status = "C");
            mLastLine = model.OpportunitiesLines.Max(c => c.Line);

            OpportunitiesLineView mLine = mOpporService.GetOpportunityLine(((UserView)Session["UserLogin"]).CompanyConnect);
            mLine.Line = (short)(mLastLine + 1);
            mLine.MaxSumLoc = model.OpportunitiesLines.Where(c => c.Line == mLastLine).Single().MaxSumLoc;
            mLine.WtSumLoc = model.OpportunitiesLines.Where(c => c.Line == mLastLine).Single().WtSumLoc;

            model.OpportunitiesLines.Add(mLine);
            TempData["Opportunities"] = model;
            return PartialView("_OpportunitiesLines", model.OpportunitiesLines);
        }

        [HttpPost]
        public ActionResult _DeleteRow(string pId)
        {
            ViewBag.Decimalformat = "{0:0.00}";
            int mLastLine;
            int mLineId = Convert.ToInt32(pId);
            OpportunitiesView model = (OpportunitiesView)TempData["Opportunities"];

            if (model.OpportunitiesLines.Count > 1)
            {
                //elimino la fila
                model.OpportunitiesLines.Remove(model.OpportunitiesLines.Where(c => c.Line == mLineId).Single());

                //Obtengo la ultima fila para setearla como OPEN
                mLastLine = model.OpportunitiesLines.Max(c => c.Line);
                model.OpportunitiesLines.Where(c => c.Line == mLastLine).Single().Status = "O";
            }

            TempData["Opportunities"] = model;
            return PartialView("_OpportunitiesLines", model.OpportunitiesLines);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Customers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>();
                cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); })
                .CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, true, 
                    searchViewModel.pCardCode, searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, 
                    ((UserView)Session["UserLogin"]).SECode, 
                    ((UserView)Session["UserLogin"]).BPGroupId, true, 
                    requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, true,
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, 
                    false, null, null, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, 
                mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetListDocuments([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            if(!string.IsNullOrEmpty(searchViewModel.pDocumentType) && searchViewModel.pClearTable == false)
            { 
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();
                List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

                DateTime? pDate = null;
                if (searchViewModel.pDate != "")
                {
                    pDate = Convert.ToDateTime(searchViewModel.pDate);
                }

                GenericDocumentManagerView mDocService = new GenericDocumentManagerView();

                JsonObjectResult mJsonObjectResult = mDocService.GetDocuments(
                    ((UserView)Session["UserLogin"]).CompanyConnect, searchViewModel.pDocumentType, false,
                    pDate, searchViewModel.pBPDocCheckBox, searchViewModel.pCardCode, 
                    requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                switch (searchViewModel.pDocumentType)
                {
                    case "13":
                        mapper = new MapperConfiguration(cfg => { cfg.CreateMap<SalesInvoiceSAP, GenericDocumentView>(); }).CreateMapper();
                        return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<GenericDocumentView>>(mJsonObjectResult.SalesInvoiceSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
                    case "17":
                        mapper = new MapperConfiguration(cfg => { cfg.CreateMap<SaleOrderSAP, GenericDocumentView>(); }).CreateMapper();
                        return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<GenericDocumentView>>(mJsonObjectResult.SalesOrderSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
                    case "23":
                        mapper = new MapperConfiguration(cfg => { cfg.CreateMap<SalesQuotationSAP, GenericDocumentView>(); }).CreateMapper();
                        return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<GenericDocumentView>>(mJsonObjectResult.SalesQuotationSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
                    default:
                        return null;
                }
            }

            return Json(new DataTablesResponse(requestModel.Draw, new List<GenericDocumentView>(), 0, 0), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pEmployeeName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
        {
            List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
            return PartialView("_ListSE", listSE);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pEmployeeCode"></param>
        /// <param name="pEmployeeName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mPSOManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtCodeVendor"></param>
        /// <param name="txtDate"></param>
        /// <param name="pOpporName"></param>
        /// <param name="txtDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ListOpportunities(string txtCodeVendor = "", string txtDate = "", string pOpporName = "", string txtDocStatus = "", string pOwnerCode = "", string pSECode = "")
        {

            string mVendor = txtCodeVendor;
            DateTime? mDate = null;
            int? mOwner = null;
            int? mSalesEmpl = null;

            if (txtDate != "")
            {
                mDate = Convert.ToDateTime(txtDate);
            }

            if (pOwnerCode != "")
            {
                mOwner = Convert.ToInt32(pOwnerCode);
            }

            if (pSECode != "")
            {
                mSalesEmpl = Convert.ToInt32(pSECode);
            }

            List<OpportunitiesView> mListOppor = mOpporService.GetOpportunityListSearch(
                ((UserView)Session["UserLogin"]).CompanyConnect, mVendor, 
                mSalesEmpl, pOpporName, mOwner, mDate, txtDocStatus);

            return PartialView("_OpportunitiesList", mListOppor);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(OpportunitiesView model)
        {
            OpportunitiesView pModelAux = (OpportunitiesView)TempData["Opportunities"];
            model.OpportunitiesLines.ForEach(c => c.ActivitiesList = pModelAux.OpportunitiesLines.Where(x => x.Line == c.Line).Single().ActivitiesList);

            TempData["Opportunities"] = model;
            string mResult = mOpporService.UpdateOportunity(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(OpportunitiesView model)
        {
            OpportunitiesView pModelAux = (OpportunitiesView)TempData["Opportunities"];
            model.OpportunitiesLines.ForEach(c => c.ActivitiesList = pModelAux.OpportunitiesLines.Where(x => x.Line == c.Line).Single().ActivitiesList);

            TempData["Opportunities"] = model;
            string mResult = mOpporService.AddOportunity(model, ((UserView)Session["UserLogin"]).CompanyConnect);
            return Json(mResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetStageById(string pId)
        {
            OpportunityStageView mResult = mOpporService.GetStageById(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pId));
            return Json(mResult.CloPrcnt);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBp(string Id)
        {
            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);

            if (mBP != null)
            {
                mBP.ErrorResponse = "Ok";
                return Json(mBP, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new BusinessPartnerView { ErrorResponse = @Error.WebServError }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLine"></param>
        /// <returns></returns>
        public ActionResult ViewActivity(string pLine)
        {
            OpportunitiesView pModel = (OpportunitiesView)TempData["Opportunities"];
            TempData["Opportunities"] = pModel;
            return View("_ActivitiesList", pModel.OpportunitiesLines.Where(x => x.Line == Convert.ToInt32(pLine)).Single().ActivitiesList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLine"></param>
        /// <returns></returns>
        public ActionResult AddActivity(string pLine)
        {
            OpportunitiesView pModel = (OpportunitiesView)TempData["Opportunities"];
            pModel.CurrentLine = Convert.ToInt32(pLine);
            Session["OpportunitiesSession"] = pModel;
            return RedirectToAction("ActivityAction", "CRMActivity", new { Area = "CRM", pAction = "Add", pCode = "0", pFrom = "Opportunity" });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public ActionResult NewOpportunitiesActivity(string pId)
        {
            try
            {
                OpportunitiesView pModel = (OpportunitiesView)Session["OpportunitiesSession"];
                ActivitiesView mNew = pModel.OpportunitiesLines.Where(x => x.Line == pModel.CurrentLine).Single().ActivitiesList.Find(c => c.ClgCode == Convert.ToInt32(pId));
                if (mNew == null)
                {
                    ActivitiesManagerView mManagerView = new ActivitiesManagerView();
                    mNew = mManagerView.GetActivity(Convert.ToInt32(pId), ((UserView)Session["UserLogin"]).CompanyConnect);
                    pModel.OpportunitiesLines.Where(x => x.Line == pModel.CurrentLine).Single().ActivitiesList.Add(mNew);
                }

                TempData["Opportunities"] = pModel;
                ViewBag.FormMode = pModel.Action;
                ViewBag.URLRedirect = (pModel.FromPage == "MyOpportunity") ? "/Sales/MyOpportunities/Index" : "/Sales/Opportunities/Index";

                return View("_Opportunities", pModel);
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLines"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateLinesChanged(List<OpportunitiesLineView>  pLines)
        {
            OpportunitiesView pModel = (OpportunitiesView)TempData["Opportunities"];

            foreach (OpportunitiesLineView Line in pLines)
            {
                OpportunitiesLineView LineToUpdate = pModel.OpportunitiesLines.Where(c => c.Line == Line.Line).FirstOrDefault();
                LineToUpdate.OpenDate = Line.OpenDate;
                LineToUpdate.CloseDate = Line.CloseDate;
                LineToUpdate.SlpCode = Line.SlpCode;
                LineToUpdate.Step_Id = Line.Step_Id;
                LineToUpdate.ClosePrcnt = Line.ClosePrcnt;
                LineToUpdate.MaxSumLoc = Line.MaxSumLoc;
                LineToUpdate.WtSumLoc = Line.WtSumLoc;
                LineToUpdate.DocNumber = Line.DocNumber;
                LineToUpdate.ObjType = Line.ObjType;
            }
            TempData["Opportunities"] = pModel;
            return Json("Ok");
        }
    }
}