﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.Attribute;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
    [CustomerAuthorize(Enums.Areas.Sales)]
    public class SalesDraftController : Controller
    {
        private DraftManagerView mDraftManagerView;
        private LocalDraftsManagerView mLocalDraftsManagerView;
        private UserManagerView mUserManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        public SalesDraftController()
        {
            mDraftManagerView = new DraftManagerView();
            mUserManagerView = new UserManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mLocalDraftsManagerView = new LocalDraftsManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.SalesDraft, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<DraftPortalView> listDC = new List<DraftPortalView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                if (System.Web.HttpRuntime.Cache["DraftSalesList" + mUser] != null)
                {
                    listDC = (List<DraftPortalView>)System.Web.HttpRuntime.Cache["DraftSalesList" + mUser];
                }

                return View(listDC);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        public ActionResult GetListDraftSales([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            AdvanceSearchMyPurchaseDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();
            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            try
            {
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();
                DateTime? mDocDateFrom = null, mDocDateTo = null;
                DateTime? mReqDate = null;
                int? mDocNum = null;
                List<DraftPortalView> ListDraft = new List<DraftPortalView>();

                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                if (searchViewModel.pDocDateFrom != "")
                {
                    mDocDateFrom = Convert.ToDateTime(searchViewModel.pDocDateFrom);
                }
                if (searchViewModel.pDocDateTo != "")
                {
                    mDocDateTo = Convert.ToDateTime(searchViewModel.pDocDateTo);
                }
                if (searchViewModel.pDocNum != "")
                {
                    mDocNum = Convert.ToInt32(searchViewModel.pDocNum);
                }

                if (searchViewModel.pDocType.Equals("LD"))
                {
                    mJsonObjectResult = mLocalDraftsManagerView.ListAllDrafts(((UserView)Session["UserLogin"]).CompanyConnect,
                        searchViewModel.pCodeVendor,null, mDocNum, "O", "", "", requestModel.Start, requestModel.Length,
                        mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                    mapper = new MapperConfiguration(cfg => { cfg.CreateMap<LocalDrafts, DraftPortalView>(); }).CreateMapper();
                    ListDraft = mapper.Map<List<DraftPortalView>>(mJsonObjectResult.LocalDraftsList);
                }
                else
                {
                    if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
                    {
                        mJsonObjectResult = mDraftManagerView.GetSalesCustomerDraftListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
                            ((UserView)Session["UserLogin"]).BPCode, mDocDateFrom,
                            mDocDateTo, searchViewModel.pDocType, searchViewModel.pDocStatus,
                            searchViewModel.pCodeVendor, mDocNum,
                            requestModel.Start, requestModel.Length,
                            mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                    }

                    if (((UserView)Session["UserLogin"]).IsSalesEmployee && ((UserView)Session["UserLogin"]).SECode != null)
                    {
                        mJsonObjectResult = mDraftManagerView.GetSalesSalesEmployeeDraftListSearch(
                            ((UserView)Session["UserLogin"]).CompanyConnect, 
                            ((UserView)Session["UserLogin"]).SECode.Value, mDocDateFrom, mDocDateTo, 
                            searchViewModel.pDocType, searchViewModel.pDocStatus, 
                            searchViewModel.pCodeVendor, mDocNum, requestModel.Start, 
                            requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                    }

                    if (((UserView)Session["UserLogin"]).IsUser && ((UserView)Session["UserLogin"]).IdUser != null)
                    {
                        mJsonObjectResult = mDraftManagerView.GetSalesUserDraftListSearch(((UserView)Session["UserLogin"]).CompanyConnect, 
                            ((UserView)Session["UserLogin"]).IdUser, mDocDateFrom, mDocDateTo, 
                            searchViewModel.pDocType, searchViewModel.pDocStatus, searchViewModel.pCodeVendor, 
                            mDocNum, requestModel.Start, requestModel.Length, 
                            mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                    }

                    mapper = new MapperConfiguration(cfg => { cfg.CreateMap<DraftPortal, DraftPortalView>(); }).CreateMapper();
                    ListDraft = mapper.Map<List<DraftPortalView>>(mJsonObjectResult.DraftPortalList);
                }
                
                return Json(new DataTablesResponse(requestModel.Draw, 
                    ListDraft, Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), 
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), 
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesDraftController --> GetListDraftSales: " + ex.Message);
                Logger.WriteError("SalesDraftController --> GetListDraftSales: " + ex.InnerException);
                return null; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DraftDocEntry"></param>
        public void CreateDocument(string DraftDocEntry)
        {
            mDraftManagerView.CreateDocument(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(DraftDocEntry));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDocEntry"></param>
        /// <param name="pObjType"></param>
        /// <returns></returns>
        public PartialViewResult _GetHistory(int pDocEntry, string pObjType)
        {
            List<DocumentConfirmationLineView> listReturn = mDraftManagerView.GetApprovalListByDocumentId(((UserView)Session["UserLogin"]).CompanyConnect, pDocEntry, pObjType);
            if (listReturn != null)
            {
                List<UserSAP> userList = mUserManagerView.GetUserSapList(((UserView)Session["UserLogin"]).CompanyConnect);
                List<StageView> ListStage = mDraftManagerView.GetStageList(((UserView)Session["UserLogin"]).CompanyConnect);
                listReturn = listReturn.Select(c => { c.UserName = userList.Where(j => j.USERID == c.UserID).FirstOrDefault().U_NAME; c.StageName = ListStage.Where(k => k.WstCode == c.StepCode).FirstOrDefault().Name; return c; }).ToList();
            }
            return PartialView("_HistoryView", listReturn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="draftId"></param>
        /// 
        public ActionResult DeleteLocalDraft(int draftId)
        {
            mLocalDraftsManagerView.Delete(draftId);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Customers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Customer, true, searchViewModel.pCardCode,
                    searchViewModel.pCardName, ((UserView)Session["UserLogin"]).IsSalesEmployee,
                    ((UserView)Session["UserLogin"]).SECode, ((UserView)Session["UserLogin"]).BPGroupId,
                    true, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Customer, true, ((UserView)Session["UserLogin"]).BPCode,
                    searchViewModel.pCardName, false, null, null, true, requestModel.Start,
                    requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }
    }
}