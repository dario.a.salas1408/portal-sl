﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Draft;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.SaleQuotations;
using AutoMapper;
using CrystalDecisions.Shared;
using DataTables.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
    [CustomerAuthorize(Enums.Areas.Sales)]
    public class SalesQuotationController : Controller
    {
        private SaleQuotationManagerView mSQManagerView;
        private SaleOrderManagerView mSOManagerView;
        private ItemMasterManagerView mItemMasterManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private DraftManagerView mDraftManagerView;
        private PDMManagerView mPDMManagerView;
        private CompaniesManagerView mCompaniesManagerView;
        private UserManagerView mUserManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;

        public SalesQuotationController()
        {
            mSQManagerView = new SaleQuotationManagerView();
            mSOManagerView = new SaleOrderManagerView();
            mItemMasterManagerView = new ItemMasterManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mDraftManagerView = new DraftManagerView();
            mPDMManagerView = new PDMManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
            mUserManagerView = new UserManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();


            Mapper.CreateMap<DraftView, SaleQuotationView>();
            Mapper.CreateMap<DraftLineView, SaleQuotationLineView>();
            Mapper.CreateMap<UserUDFView, UDF_ARGNS>();
            Mapper.CreateMap<UDF_ARGNS, UserUDFView>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.SalesQuotation, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
                List<SaleQuotationView> ListPO = new List<SaleQuotationView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                if (System.Web.HttpRuntime.Cache["SQList" + mUser] != null)
                {
                    ListPO = (List<SaleQuotationView>)System.Web.HttpRuntime.Cache["SQList" + mUser];
                }

                return View(Tuple.Create(ListPO, ListBp));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionPurchaseOrder"></param>
        /// <param name="IdPO"></param>
        /// <param name="fromController"></param>
        /// <returns></returns>
        public ActionResult ActionPurchaseOrder(string ActionPurchaseOrder, int IdPO, string fromController)
        {
            SaleQuotationView mSQView = null;
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            TempData["ListItems" + mPageKey] = null;

            List<ItemGroupSAP> mItemGroupList = mItemMasterManagerView
                .GetItemGroupList(((UserView)Session["UserLogin"]).CompanyConnect);

            switch (fromController)
            {
                case "SalesQuotation":
                    ViewBag.URLRedirect = "/Sales/SalesQuotation/Index";
                    ViewBag.controllerName = "SalesQuotation";
                    break;
                case "MyDocuments":
                    ViewBag.URLRedirect = "/Sales/SalesDraft/Index";
                    ViewBag.controllerName = "SalesDraft";
                    break;
                case "MyApprovals":
                    ViewBag.URLRedirect = "/Sales/SalesApproval/Index";
                    ViewBag.controllerName = "SalesApproval";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            switch (ActionPurchaseOrder)
            {
                case "Add":
                    ViewBag.Tite = SaleQuotations.ASQ;
                    ViewBag.FormMode = ActionPurchaseOrder;
                    mSQView = mSQManagerView.GetSaleQuotation(0, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mSQView.MappedUdf = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OQUT");

                    mSQView.DocDate = System.DateTime.Now;
                    mSQView.DocDueDate = System.DateTime.Now;
                    mSQView.TaxDate = System.DateTime.Now;
                    if (((UserView)Session["UserLogin"]).IsSalesEmployee == true && ((UserView)Session["UserLogin"]).SECode != null)
                    {
                        mSQView.SlpCode = ((UserView)Session["UserLogin"]).SECode.Value;
                    }
                    TempData["ListItemsExit" + mPageKey] = null;
                    break;

                case "View":
                case "Update":
                    if (ActionPurchaseOrder == "View")
                        ViewBag.Tite = SaleQuotations.VSQ;
                    else
                        ViewBag.Tite = SaleQuotations.USQ;
                    ViewBag.FormMode = ActionPurchaseOrder;
                    mSQView = mSQManagerView.GetSaleQuotation(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);

                    List<StockModelView> mLIstStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, mSQView.Lines.Select(c => c.ItemCode).ToList());

                    mSQView.Lines.Select(c => { c.ListCurrency = mSQView.ListDocumentSAPCombo.ListCurrency; c.hasStock = (mLIstStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();

                    //seteo el ocrcode = "" en caso de tenga null, para que luego en el items form no le ponga el warehouse por defecto dado que no es una linea nueva
                    mSQView.Lines = mSQView.Lines.Select(c => { c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); return c; }).ToList();
                    TempData["ListItemsExit" + mPageKey] = mSQView.Lines;

                    break;
                case "ViewDraft":
                    ViewBag.Tite = Drafts.SQD;
                    ViewBag.FormMode = "View";
                    DraftView draft = mDraftManagerView.GetDraftById(IdPO, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mSQView = new SaleQuotationView();
                    mSQView = Mapper.Map<SaleQuotationView>(draft);
                    mSQView.Lines = Mapper.Map<List<SaleQuotationLineView>>(draft.Lines);

                    SalesQuotationSAP mSQ = mSQManagerView.GetSQInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mSQView);
                    mSQView = Mapper.Map<SaleQuotationView>(mSQ);
                    mSQView.Lines = Mapper.Map<List<SaleQuotationLineView>>(mSQ.Lines);
                    mSQView.Lines.Select(c => { c.ListCurrency = mSQView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();

                    TempData["ListItemsExit" + mPageKey] = mSQView.Lines;
                    break;

            }

            Session["LocalCurrency"] = mSQView.LocalCurrency;
            Session["SystemCurrency"] = mSQView.SystemCurrency;
            Session["DistrRuleSAP"] = mSQView.DistrRuleSAP;
            Session["Warehouse"] = mSQView.Warehouse;
            Session["GLAccountSAP"] = mSQView.GLAccountSAP;

            TempData["ListCurrency" + mPageKey] = mSQView.ListDocumentSAPCombo.ListCurrency;
            List<RatesSystem> ListRates = mSQManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mSQView.ListDocumentSAPCombo.ListCurrency, mSQView.DocDate, mSQView.LocalCurrency, mSQView.SystemCurrency);
            TempData["Rates" + mPageKey] = ListRates;
            mSQView.SystemCurrency = ListRates.Where(c => c.System == true).FirstOrDefault().Currency;
            mSQView.LocalCurrency = ListRates.Where(c => c.Local == true).FirstOrDefault().Currency;
            TempData["Rates" + mPageKey] = mSQView.ListSystemRates;
            TempData["FreightListSQ" + mPageKey] = mSQView.ListFreight;
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");

            TempData["Attachments" + mPageKey] = mSQView.AttachmentList;

            mItemGroupList.Add(new ItemGroupSAP() { ItmsGrpCod = 0, ItmsGrpNam = "All" });
            mSQView.ItemGroups = mItemGroupList.OrderBy(o => o.ItmsGrpCod).ToList();

            return View("_SalesQuotation", Tuple.Create(mSQView, ((UserView)Session["UserLogin"]).CompanyConnect, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesQuotation, ((UserView)Session["UserLogin"]).CompanyConnect), mOITMUDFList));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(SaleQuotationView model)
        {
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];

            List<SaleQuotationLineView> mListSaleQuotationLineView = (
                (List<SaleQuotationLineView>)TempData["ListItemsExit" + model.PageKey]);

            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];

            model.AttachmentList = mAttachmentList;
            model.Lines.Select(
                c => 
                    { c.ItemCode = ((List<SaleQuotationLineView>)TempData["ListItemsExit" + model.PageKey])
                        .Where(d => d.LineNum == c.LineNum)
                        .Select(d => d.ItemCode).FirstOrDefault();

                        c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["ListItemsExit" + model.PageKey] = mListSaleQuotationLineView;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            return Json(mSQManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(SaleQuotationView model)
        {
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];
            List<SaleQuotationLineView> mListSaleQuotationLineView = (List<SaleQuotationLineView>)TempData["ListItemsExit" + model.PageKey];
            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];

            model.AttachmentList = mAttachmentList;
            model.Lines.Select(c => { c.ItemCode = mListSaleQuotationLineView.Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["ListItemsExit" + model.PageKey] = mListSaleQuotationLineView;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            string mResult = mSQManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = GetItems(searchViewModel.pPageKey, 
                    searchViewModel.pMappedUdf, 
                    "", searchViewModel.pItemCode, 
                    searchViewModel.pItemName,
                    searchViewModel.pInventoryItem,
                    searchViewModel.pItemWithStock,
                    searchViewModel.pItemGroup,
                    requestModel.Start, 
                    requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemData"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsGrid(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pItemCode = "", string pItemData = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

            OrderColumn mOrderColumn = new OrderColumn();
            mOrderColumn.Name = "ItemCode";
            mOrderColumn.IsOrdered = true;
            mOrderColumn.SortDirection = OrderColumn.OrderDirection.Ascendant;
            pOrderColumn = mOrderColumn;

            JsonObjectResult mJsonObjectResult = GetItems(pPageKey, pMappedUdf, "", pItemCode, pItemData, "", "", "", pStart, pLength, pOrderColumn);
            return PartialView("_ItemsGrid", Tuple.Create(mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), pLength));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLines"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateLinesChanged(List<SaleQuotationLineView> pLines, string pPageKey)
        {
            List<SaleQuotationLineView> ListReturnExit = (List<SaleQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            foreach (SaleQuotationLineView Line in pLines)
            {
                SaleQuotationLineView LineToUpdate = ListReturnExit.Where(c => c.LineNum == Line.LineNum).FirstOrDefault();
                LineToUpdate.Dscription = Line.Dscription;
                LineToUpdate.WhsCode = Line.WhsCode;
                LineToUpdate.OcrCode = Line.OcrCode;
                LineToUpdate.GLAccount = Line.GLAccount;
                LineToUpdate.FreeTxt = Line.FreeTxt;
                LineToUpdate.ShipDate = Line.ShipDate;
                LineToUpdate.Quantity = Line.Quantity;
                LineToUpdate.Price = Line.Price;
                LineToUpdate.PriceBefDi = Line.PriceBefDi;
                LineToUpdate.Currency = Line.Currency;
                LineToUpdate.UomCode = Line.UomCode;
                LineToUpdate.TaxCode = Line.TaxCode;
                LineToUpdate.DiscPrcnt = Line.DiscPrcnt;
                LineToUpdate.VatPrcnt = Line.VatPrcnt;
                if (LineToUpdate.Freight.Count > 0)
                {
                    LineToUpdate.Freight.FirstOrDefault().ExpnsCode = Line.Freight.FirstOrDefault().ExpnsCode;
                    LineToUpdate.Freight.FirstOrDefault().LineTotal = Line.Freight.FirstOrDefault().LineTotal;
                }
                else
                {
                    LineToUpdate.Freight = Line.Freight;
                }
                foreach (UDF_ARGNS mUDF in LineToUpdate.MappedUdf)
                {
                    UDF_ARGNS mUDFAux = Line.MappedUdf.Where(c => c.UDFName == mUDF.UDFName).FirstOrDefault();
                    mUDF.Value = mUDFAux.Value;
                }
            }
            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json("Ok");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItems"></param>
        /// <param name="pCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsForm(string[] pItems, string pCurrency, string CardCode, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mSQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;

            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }
            
            List<SaleQuotationLineView> ListReturn = null;
            List<SaleQuotationLineView> ListExistingItems = new List<SaleQuotationLineView>();
            List<SaleQuotationLineView> ListReturnExit = (List<SaleQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            
            if (ListReturnExit != null)
            {
                ListReturnExit.ForEach(c => c.CalcTax = "N");
            }            

            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
            {
                //Esta lista contiene los items que ya fueron agregados a una linea del documento (solo debo sumarle +1 a la quantity)
                ListExistingItems = ListReturnExit.Where(c => pItems.Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).ToList();
                //Esta lista contiene los items que no fueron agregados a una linea del documento (debo crear una nueva linea)
                pItems = pItems.Where(j => !ListExistingItems.Select(c => c.ItemCode).ToList().Contains(j)).ToArray();
            }
            if (pItems.Length > 0)
            {
                int mLine = 0;

                if (ListReturnExit != null)
                {
                    mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max() + 1;
                }
                List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);
                List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, ListItems.Select(c => c.ItemCode).ToList());

                //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
                List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "QUT1");
                
                List<SaleQuotationLineView> ListOitm = new List<SaleQuotationLineView>();
                foreach (ItemMasterView mItemAux in ListItems)
                {
                    SaleQuotationLineView mLineAux = new SaleQuotationLineView();
                    UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                    mLineAux.hasStock = (ItemsStock.Where(d => d.ItemCode == mItemAux.ItemCode).Count() > 0 ? true : false);
                    mLineAux.Dscription = mItemAux.ItemName;
                    mLineAux.ItemCode = mItemAux.ItemCode;
                    mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                    mLineAux.LineNum = (mLine++);
                    mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);
                    mLineAux.Quantity = 1;
                    mLineAux.ListCurrency = ListCurrency;
                    //Clone the list of udf to a new list in mLineAux.MappedUdf
                    mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                    mListUserUDF.ForEach((item) =>
                    {
                        mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                    });
                    mLineAux.WhsCode = mItemAux.DfltWH;
                    mLineAux.TaxCode = mDefTAxCode.Code;
                    mLineAux.VatPrcnt = mDefTAxCode.Rate;
                    mLineAux.PriceBefDi = mLineAux.Price;
                    mLineAux.UomCode = mDefaultUOM.UomCode;
                    mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                    mLineAux.ItemPrices = mItemAux.ItemPrices;
                    mLineAux.CalcTax = "Y";
                    mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                    
                    if (mItemAux.TaxCodeAR != null)
                    {
                        mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                        mLineAux.VatPrcnt = mTaxCodeList.Where(t => t.Code == mItemAux.TaxCodeAR).Single().Rate;
                    }


                    ListOitm.Add(mLineAux);
                }

                if (pItems != null)
                { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
                else
                { ListReturn = new List<SaleQuotationLineView>(); }

                if (ListReturnExit != null)
                { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }
            }
            if (ListExistingItems.Count > 0)
            {
                //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                ListExistingItems = ListReturnExit.Where(c => ListExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + 1; return k; }).ToList();
                if (ListReturn != null)
                { ListReturn = ListReturn.Union(ListExistingItems).ToList(); }
                else
                { ListReturn = ListReturnExit.Union(ListExistingItems).ToList(); }
            }
            
            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
            TempData["Rates" + pPageKey] = ListRate;

            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

            SalesQuotationItemsFormView mSalesQuotationItemsFormView = new SalesQuotationItemsFormView();
            mSalesQuotationItemsFormView.Lines = ListReturn;
            mSalesQuotationItemsFormView.DocCur = pCurrency;
            mSalesQuotationItemsFormView.UsersSetting = userSetting;
            mSalesQuotationItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSQ" + pPageKey];
            mSalesQuotationItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            //TempData["ListItems" + pPageKey] = ListReturn;
            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            TempData["FreightListSQ" + pPageKey] = mSalesQuotationItemsFormView.FreightList;
            ViewBag.CalculateRate = "Y";

            return PartialView("_ItemsForm", mSalesQuotationItemsFormView);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItems"></param>
        /// <param name="pCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsFormCopyPaste(string[] pItems, string pCurrency, string CardCode, string pPageKey)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mSQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<SaleQuotationLineView> ListReturn = null;
            List<SaleQuotationLineView> ListExistingItems = new List<SaleQuotationLineView>();
            List<SaleQuotationLineView> ListReturnExit = (List<SaleQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, pItems.ToList());
            List<ItemMasterView> ListItemsclipboardData = new List<ItemMasterView>();

            if (ListReturnExit != null)
            {
                ListReturnExit.ForEach(c => c.CalcTax = "N");
            }

            if (ItemsStock == null || ItemsStock.Count == 0)
            {
                ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect);
            }

            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            foreach (string item in pItems)
            {
                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(
                    ((UserView)Session["UserLogin"]).CompanyConnect, "", "Y", "",
                    "N", "", null, "", 
                    ((UserView)Session["UserLogin"]).IdUser, item, "", true, 
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

                ItemMasterView mItemMasterView = mapper.Map<ItemMasterView>(
                    mJsonObjectResult.ItemMasterSAPList.FirstOrDefault());

                if (mItemMasterView == null)
                {
                    mItemMasterView = new ItemMasterView() { ItemCode = item };
                }

                ListItemsclipboardData.Add(mItemMasterView);

                ListItemsclipboardData.Select(c => { c.ListCurrency = ListCurrency; c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();
            }

            if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
            {
                //Esta lista contiene los items que ya fueron agregados a una linea del documento (solo debo sumarle +1 a la quantity)
                ListExistingItems = ListReturnExit.Where(c => pItems.Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).ToList();
                //Esta lista contiene los items que no fueron agregados a una linea del documento (debo crear una nueva linea)
                pItems = pItems.Where(j => !ListExistingItems.Select(c => c.ItemCode).ToList().Contains(j)).ToArray();
            }
            
            if (pItems.Length > 0)
            {
                int mLine = 0;

                if (ListReturnExit != null)
                {
                    //mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Count;
                    mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max() + 1;

                }
                List<ItemMasterView> ListItems = ((List<ItemMasterView>)ListItemsclipboardData).Where(c => pItems.Contains(c.ItemCode)).ToList();
                ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection, false);
                
                //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
                List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "QUT1");

                List<SaleQuotationLineView> ListOitm = new List<SaleQuotationLineView>();
                foreach (ItemMasterView mItemAux in ListItems)
                {
                    SaleQuotationLineView mLineAux = new SaleQuotationLineView();
                    UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                    mLineAux.hasStock = (ItemsStock.Where(d => d.ItemCode == mItemAux.ItemCode).Count() > 0 ? true : false);
                    mLineAux.Dscription = mItemAux.ItemName;
                    mLineAux.ItemCode = mItemAux.ItemCode;
                    mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                    mLineAux.LineNum = (mLine++);
                    mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);
                    mLineAux.Quantity = 1;
                    mLineAux.ListCurrency = ListCurrency;
                    //Clone the list of udf to a new list in mLineAux.MappedUdf
                    mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                    mListUserUDF.ForEach((item) =>
                    {
                        mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                    });
                    mLineAux.WhsCode = mItemAux.DfltWH;
                    mLineAux.TaxCode = mDefTAxCode.Code;
                    mLineAux.VatPrcnt = mDefTAxCode.Rate;
                    mLineAux.PriceBefDi = mLineAux.Price;
                    mLineAux.UomCode = mDefaultUOM.UomCode;
                    mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                    mLineAux.ItemPrices = mItemAux.ItemPrices;
                    mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                    mLineAux.CalcTax = "Y";

                    ListOitm.Add(mLineAux);
                }

                if (pItems != null)
                { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
                else
                { ListReturn = new List<SaleQuotationLineView>(); }

                if (ListReturnExit != null)
                { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }
            }
            if (ListExistingItems.Count > 0)
            {
                //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                ListExistingItems = ListReturnExit.Where(c => ListExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + 1; return k; }).ToList();
                if (ListReturn != null)
                { ListReturn = ListReturn.Union(ListExistingItems).ToList(); }
                else
                { ListReturn = ListReturnExit.Union(ListExistingItems).ToList(); }
            }

            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
            TempData["Rates" + pPageKey] = ListRate;
            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

            SalesQuotationItemsFormView mSalesQuotationItemsFormView = new SalesQuotationItemsFormView();
            mSalesQuotationItemsFormView.Lines = ListReturn;
            mSalesQuotationItemsFormView.DocCur = pCurrency;
            mSalesQuotationItemsFormView.UsersSetting = userSetting;
            mSalesQuotationItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSQ" + pPageKey];
            mSalesQuotationItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            TempData["FreightListSQ" + pPageKey] = mSalesQuotationItemsFormView.FreightList;
            ViewBag.CalculateRate = "Y";

            return PartialView("_ItemsForm", mSalesQuotationItemsFormView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTaxCodeList()
        {
            try
            {
                List<SalesTaxCodesSAP> mTaxCodeList = mSQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
                return Json(mTaxCodeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Customers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, 
                    Enums.BpType.Customer, true, searchViewModel.pCardCode, searchViewModel.pCardName,
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, ((UserView)Session["UserLogin"]).SECode, 
                    ((UserView)Session["UserLogin"]).BPGroupId, true, requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, true, 
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, false, 
                    null, null, true, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pEmployeeCode"></param>
        /// <param name="pEmployeeName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mSQManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="LocalCurrency"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBp(string Id, string LocalCurrency)
        {
            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, LocalCurrency);

            if (mBP != null)
            {
                mBP.ErrorResponse = "Ok";
                return Json(mBP, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new BusinessPartnerView { ErrorResponse = @Error.WebServError }, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocDate"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        public JsonResult UpdateRateList(DateTime DocDate, string pPageKey)
        {
            List<CurrencySAP> mListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<RatesSystem> ListRates = mSQManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mListCurrency, DocDate, (string)Session["LocalCurrency"], (string)Session["SystemCurrency"]);
            TempData["Rates" + pPageKey] = ListRates;
            TempData["ListCurrency" + pPageKey] = mListCurrency;
            try
            {
                if (ListRates != null)
                {
                    return Json(ListRates, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteRow(string ItemCode, string pPageKey)
        {
            try
            {
                List<SaleQuotationLineView> ListReturnExit = (List<SaleQuotationLineView>)TempData["ListItemsExit" + pPageKey];

                ListReturnExit.RemoveAll(c => c.LineNum == Convert.ToInt32(ItemCode));

                TempData["ListItemsExit" + pPageKey] = ListReturnExit;

                return Json("Ok");
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pModeCode"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        private JsonObjectResult GetItems(string pPageKey, List<UDF_ARGNS> pMappedUdf, 
            string pModeCode = "", string pItemCode = "", string pItemName = "",
            string pInventoryItem = "",
            string pItemWithStock = "",
            string pItemGroup = "",
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                    cfg.CreateMap<ItemMasterView, ItemMasterSAP>();
                    cfg.CreateMap<StockModel, StockModelView>();
                    cfg.CreateMap<StockModelView, StockModel>();
                }).CreateMapper();

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(
                    ((UserView)Session["UserLogin"]).CompanyConnect,
                    "", "Y", pInventoryItem, pItemWithStock,
                    pItemGroup, 
                    pMappedUdf, pModeCode, ((UserView)Session["UserLogin"]).IdUser,
                    pItemCode, pItemName, true, 
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                    false, "", "", pStart, pLength, pOrderColumn);

                List<ItemMasterView> ListReturn = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);

                List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(
                    ((UserView)Session["UserLogin"]).CompanyConnect, ListReturn.Select(c => c.ItemCode).ToList());

                List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                //if pLength is greatter than 0, i need to page the result, then i need to add the actual page to the previous ListItems
                if (pLength > 0)
                {
                    List<ItemMasterView> mListItemsAux = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]);
                    //If its the first time that i enter to GetItems and the TempData of ListItems is null i inizialice the variable.
                    mListItemsAux = (mListItemsAux == null ? new List<ItemMasterView>() : mListItemsAux);
                    //if the item not exist in mListItemsAux List i add it, else i do nothing
                    mListItemsAux.AddRange(ListReturn.Where(c => mListItemsAux.Select(x => x.ItemCode).Contains(c.ItemCode) == false).Select(c => { c.ListCurrency = ListCurrency; c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList());
                    TempData["ListItems" + pPageKey] = mListItemsAux;
                }
                else
                {
                    TempData["ListItems" + pPageKey] = ListReturn.Select(c => { c.ListCurrency = ListCurrency; c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();
                }
                //Save the ListCurrency and hasStock to ListReturn Items
                ListReturn = ListReturn.Select(c => { c.ListCurrency = ListCurrency; c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();
                mJsonObjectResult.ItemMasterSAPList = mapper.Map<List<ItemMasterSAP>>(ListReturn);

                TempData["ListCurrency" + pPageKey] = ListCurrency;

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetListSQ(
            [ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            string mVendor = searchViewModel.pCodeVendor;
            DateTime? mDate = null;
            int? mDocNum = null;
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

            if (searchViewModel.pDate != "")
            {
                mDate = Convert.ToDateTime(searchViewModel.pDate);
            }

            if (searchViewModel.pNro != "")
            {
                int intDocNum; 
                int.TryParse(searchViewModel.pNro, out intDocNum);
                if (intDocNum !=  0)
                {
                    mDocNum = intDocNum;
                }
            }

            JsonObjectResult mJsonObjectResult = mSQManagerView.GetSalesQuotationListSearch(
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                mVendor, mDate, mDocNum, searchViewModel.pDocStatus, 
                searchViewModel.pOwnerCode, searchViewModel.pSECode, 
                requestModel.Start, requestModel.Length, 
                mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<SalesQuotationSAP, SaleQuotationView>(); }).CreateMapper();
            
            return Json(new DataTablesResponse(requestModel.Draw, 
                mapper.Map<List<SaleQuotationView>>(mJsonObjectResult.SalesQuotationSAPList), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords")
                    .FirstOrDefault().Value), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords")
                    .FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDocEntry"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _FreightTable(int pDocEntry)
        {
            List<FreightView> FreightList = new List<FreightView>();
            FreightList = mSQManagerView.GetFreights(((UserView)Session["UserLogin"]).CompanyConnect, pDocEntry);

            return PartialView("_FreightTable", FreightList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdPO"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        public ActionResult CopyOrder(int IdPO, string pPageKey)
        {
            SaleOrderView mSOView = null;
            SaleQuotationView mSQView = null;
            Mapper.CreateMap<SaleOrderView, SaleQuotationView>();
            Mapper.CreateMap<SaleOrderLineView, SaleQuotationLineView>();

            mSOView = mSOManagerView.GetSaleOrder(IdPO, 
                ((UserView)Session["UserLogin"]).IdUser, 
                ((UserView)Session["UserLogin"]).CompanyConnect, false);

            mSQView = Mapper.Map<SaleQuotationView>(mSOView);

            mSQView.DocDate = System.DateTime.Now;
            mSQView.DocDueDate = System.DateTime.Now;
            mSQView.TaxDate = System.DateTime.Now;
            mSQView.ReqDate = null;
            mSQView.CancelDate = null;
            mSQView.DocEntry = 0;

            mSQView.Comments = "Based On Sales Order " + IdPO.ToString();

            mSQView.Lines.Select(c => 
                { c.ListCurrency = mSQView.ListDocumentSAPCombo.ListCurrency; return c; })
                .ToList();

            TempData["ListItemsExit" + pPageKey] = mSQView.Lines;

            ViewBag.Tite = "Add";
            ViewBag.FormMode = "Add";

            TempData["ListCurrency" + pPageKey] = mSQView.ListDocumentSAPCombo.ListCurrency;
            TempData["Rates" + pPageKey] = mSQView.ListSystemRates;

            return View("_SalesQuotation", Tuple.Create(mSQView, 
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesQuotation, 
                ((UserView)Session["UserLogin"]).CompanyConnect)));
        }

        #region Matrix Model

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModelCode"></param>
        /// <param name="pModelName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelListSearch(AdvancedSearchStylesView searchViewModel)
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            bool hasPermissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
            .Where(c => c.PageInternalKey == Enums.Pages.SalesQuotation.ToDescriptionString()
            && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
            .FirstOrDefault() != null ? true : false;

            IMapper mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<Column, OrderColumn>(); }
                ).CreateMapper();

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ModelDesc, ModelDescView>();
            }).CreateMapper();

            JsonObjectResult mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                hasPermissionToViewStock, searchViewModel.Start, 
                searchViewModel.Length, searchViewModel.CodeModel, searchViewModel.NameModel);

            return PartialView("_ModelList", Tuple.Create(
             mapper.Map<List<ModelDescView>>(mJsonObjectResult.ModelDescViewList),
                 Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                 searchViewModel.Length));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pModelCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelDetail(string pPageKey, string pModelCode = "")
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            MatrixModelView mModelObj = mPDMManagerView.GetMatrixModelView(
                ((UserView)Session["UserLogin"]).CompanyConnect, pModelCode,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

            GetItems(pPageKey, null, pModelCode, "", "");
            return PartialView("_ModelMatrixDetail", mModelObj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemsQty"></param>
        /// <param name="pItems"></param>
        /// <param name="pCurrency"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        //Verificar este metodo para implementarlo Mejor.
        [HttpPost]
        public PartialViewResult _ItemsFormMatrixModel(string pItemsQty, string[] pItems, string pCurrency, string pPageKey, string pCardCode)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mSQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            ICollection<JsonObject> jsonModel = JsonConvert.DeserializeObject<ICollection<JsonObject>>(pItemsQty);
            List<SaleQuotationLineView> ListReturn;
            List<SaleQuotationLineView> ListReturnExit = (List<SaleQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

            int mLine = 0;

            if (ListReturnExit != null)
            {
                mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max() + 1;
            }

            List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
            ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, pCardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

            //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
            List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "QUT1");

            List<SaleQuotationLineView> ListOitm = new List<SaleQuotationLineView>();
            foreach (ItemMasterView mItemAux in ListItems)
            {
                SaleQuotationLineView mLineAux = new SaleQuotationLineView();
                UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                mLineAux.Dscription = mItemAux.ItemName;
                mLineAux.ItemCode = mItemAux.ItemCode;
                mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                mLineAux.LineNum = (mLine++);
                mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);
                mLineAux.Quantity = 1;
                mLineAux.ListCurrency = ListCurrency;
                //Clone the list of udf to a new list in mLineAux.MappedUdf
                mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                mListUserUDF.ForEach((item) =>
                {
                    mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                });
                mLineAux.WhsCode = mItemAux.DfltWH;
                mLineAux.TaxCode = mDefTAxCode.Code;
                mLineAux.VatPrcnt = mDefTAxCode.Rate;
                mLineAux.PriceBefDi = mLineAux.Price;
                mLineAux.UomCode = mDefaultUOM.UomCode;
                mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                mLineAux.ItemPrices = mItemAux.ItemPrices;
                mLineAux.CalcTax = "Y";
                mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                mLineAux.ItemPrices = mItemAux.ItemPrices;

                ListOitm.Add(mLineAux);
            }

            foreach (SaleQuotationLineView item in ListOitm)
            {
                foreach (var mQty in jsonModel)
                {
                    if (item.ItemCode.Contains(mQty.Code))
                    {
                        item.Quantity = Convert.ToDecimal(mQty.Qty);
                    }
                }
            }

            if (pItems != null)
            { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
            else
            { ListReturn = new List<SaleQuotationLineView>(); }

            if (ListReturnExit != null)
            { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }

            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();

            TempData["Rates" + pPageKey] = ListRate;

            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

            SalesQuotationItemsFormView mSalesQuotationItemsFormView = new SalesQuotationItemsFormView();
            mSalesQuotationItemsFormView.Lines = ListReturn;
            mSalesQuotationItemsFormView.DocCur = pCurrency;
            mSalesQuotationItemsFormView.UsersSetting = userSetting;
            mSalesQuotationItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSQ" + pPageKey];
            mSalesQuotationItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            //TempData["ListItems" + pPageKey] = ListReturn;
            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            TempData["FreightListSQ" + pPageKey] = mSalesQuotationItemsFormView.FreightList;
            ViewBag.CalculateRate = "Y";

            return PartialView("_ItemsForm", mSalesQuotationItemsFormView);

        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _GetDistributionRuleList()
        {
            try
            {
                List<DistrRuleSAP> mDistrRuleSAP = (List<DistrRuleSAP>)Session["DistrRuleSAP"];

                List<ListItem> DistributionRuleList = mDistrRuleSAP.Select(c => new ListItem { Value = c.OcrCode.ToString(), Text = c.OcrName }).ToList();

                return Json(DistributionRuleList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesQuotationController -> _GetDistributionRuleList: " + ex.Message);
                Logger.WriteError("SalesQuotationController -> _GetDistributionRuleList: " + ex.InnerException);
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetWHwithStock(string ItemCode)
        {
            try
            {
                List<string> mItemsCode = new List<string>();

                mItemsCode.Add(ItemCode);

                List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, mItemsCode.ToArray());
                return Json(ItemsStock, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetItemStock(string ItemCode = "")
        {
            List<string> mItemsCode = new List<string>();

            mItemsCode.Add(ItemCode);

            List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, mItemsCode.ToArray());
            return PartialView("_ItemsStock", ItemsStock);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUnitOfMeasure(string ItemCode)
        {
            try
            {
                List<UnitOfMeasureView> mUOM = mItemMasterManagerView.GeUnitOfMeasureByItem(((UserView)Session["UserLogin"]).CompanyConnect, ItemCode);
                return Json(mUOM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _GetGLAccountList(string pPageKey)
        {
            try
            {
                List<GLAccountSAP> mGLAccountSAP = (List<GLAccountSAP>)Session["GLAccountSAP"];

                TempData["ListGLAccount" + pPageKey] = mGLAccountSAP;
                List<ListItem> ListReturn = mGLAccountSAP.Select(c => new ListItem { Value = c.FormatCode, Text = c.AcctName }).ToList();

                return Json(ListReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPCurrency"></param>
        /// <param name="pPageKey"></param>
        /// <param name="pItemCode"></param>
        /// <param name="CardCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetHandheldItem(string pBPCurrency, string pPageKey, string pItemCode = "", string CardCode = "")
        {
            //En el caso del 1 es BarCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 1)
            {
                ItemMasterView oItemMaster = mItemMasterManagerView.GeItemByCodeBar(((UserView)Session["UserLogin"]).CompanyConnect, "N", "Y", pItemCode);

                if (oItemMaster != null)
                {
                    GetItems(pPageKey, null, "", oItemMaster.ItemCode);
                    string[] pItems = new string[1];
                    pItems[0] = oItemMaster.ItemCode;
                    try
                    {
                        return _ItemsForm(pItems, pBPCurrency, CardCode, pPageKey);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            //En el caso del 2 es QRCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 2)
            {
                QRConfigView mQrConfig = mCompaniesManagerView.GetQRConfig(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
                string[] stringSplit = pItemCode.Split(new char[] { mQrConfig.Separator.ToCharArray().FirstOrDefault() });
                if (pItemCode != "")
                {
                    try
                    {
                        return _ItemsFormQRCode(stringSplit, pBPCurrency, CardCode, mQrConfig, pPageKey);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pEmployeeName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
        {
            List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
            return PartialView("_ListSE", listSE);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItems"></param>
        /// <param name="pBPCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="mQrConfig"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        public PartialViewResult _ItemsFormQRCode(string[] pItems, string pBPCurrency, string CardCode, QRConfigView mQrConfig, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mSQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<SaleQuotationLineView> ListReturn = null;
            SaleQuotationLineView ListExistingItem = null;
            List<SaleQuotationLineView> ListReturnExit = (List<SaleQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

            try
            {
                string pItemsAux = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];

                if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
                {
                    ListExistingItem = ListReturnExit.Where(c => c.ItemCode == pItemsAux).FirstOrDefault();
                }
                //Si esta lista es distinta de null solo debo sumarle + 1 a la quantity puesto que el item ya existe en las lineas de la orden
                if (ListExistingItem != null && ListReturnExit != null)
                {
                    //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                    ListReturnExit.Where(c => c.ItemCode == ListExistingItem.ItemCode).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + (mQrConfig.QuantityPosition != null ? Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]) : 1); return k; }).ToList();
                    ListReturn = ListReturnExit;
                }
                //Debo agregar la linea nueva para ese item
                else
                {
                    int mLine = 0;

                    if (ListReturnExit != null)
                    {
                        mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max() + 1;
                    }

                    SaleQuotationLineView mSQLine = new SaleQuotationLineView();
                    mSQLine.ItemCode = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];
                    GetItems(pPageKey, null, "", mSQLine.ItemCode);
                    List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                    List<string> mListItemCode = new List<string>();
                    mListItemCode.Add(mSQLine.ItemCode);
                    List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, mListItemCode);
                    mSQLine.hasStock = (ItemsStock.Where(d => d.ItemCode == mSQLine.ItemCode).Count() > 0 ? true : false);
                    ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

                    if (mQrConfig.CurrencyPosition != null)
                        mSQLine.Currency = pItems[(mQrConfig.CurrencyPosition ?? 0) - 1];
                    //Descripcion
                    if (mQrConfig.DscriptionPosition != null)
                        mSQLine.Dscription = pItems[(mQrConfig.DscriptionPosition ?? 0) - 1];
                    else
                        mSQLine.Dscription = ListItems.FirstOrDefault().ItemName;

                    if (mQrConfig.FreeTxtPosition != null)
                        mSQLine.FreeTxt = pItems[(mQrConfig.FreeTxtPosition ?? 0) - 1];
                    if (mQrConfig.GLAccountPosition != null)
                    {
                        mSQLine.GLAccount = new GLAccountView();
                        mSQLine.GLAccount.FormatCode = pItems[(mQrConfig.GLAccountPosition ?? 0) - 1];
                    }
                    if (mQrConfig.OcrCodePosition != null)
                        mSQLine.OcrCode = pItems[(mQrConfig.OcrCodePosition ?? 0) - 1];

                    //Price
                    if (mQrConfig.PricePosition != null)
                    {
                        mSQLine.Price = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                        mSQLine.PriceBefDi = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                    }
                    else
                    {
                        UnitOfMeasure mDefaultUOM = ListItems.FirstOrDefault().UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();
                        mSQLine.Price = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0);
                        mSQLine.PriceBefDi = mSQLine.Price;
                        mSQLine.Currency = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                        mSQLine.UnitOfMeasureList = ListItems.FirstOrDefault().UnitOfMeasureList;
                        mSQLine.ItemPrices = ListItems.FirstOrDefault().ItemPrices;
                    }

                    //Rates
                    mSQLine.TaxCode = mDefTAxCode.Code;
                    mSQLine.VatPrcnt = mDefTAxCode.Rate;

                    //Quantity
                    if (mQrConfig.QuantityPosition != null)
                        mSQLine.Quantity = Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]);
                    else
                        mSQLine.Quantity = 1;

                    if (mQrConfig.WhsCodePosition != null)
                    {
                        mSQLine.WhsCode = pItems[(mQrConfig.WhsCodePosition ?? 0) - 1];
                    }
                    else
                    {   //Default Warehouse
                        mSQLine.WhsCode = ListItems.FirstOrDefault().DfltWH;
                    }

                    if (mQrConfig.UomCodePosition != null)
                        mSQLine.UomCode = pItems[(mQrConfig.UomCodePosition ?? 0) - 1];

                    if (mQrConfig.SerialPosition != null || mQrConfig.BatchPosition != null)
                    {
                        if (!string.IsNullOrEmpty(pItems[(mQrConfig.SerialPosition ?? 0) - 1]))
                            mSQLine.SerialBatch = pItems[(mQrConfig.SerialPosition ?? 0) - 1];
                        if (!string.IsNullOrEmpty(pItems[(mQrConfig.BatchPosition ?? 0) - 1]))
                            mSQLine.SerialBatch = pItems[(mQrConfig.BatchPosition ?? 0) - 1];
                    }
                    mSQLine.LineNum = (mLine++);
                    mSQLine.ListCurrency = ListCurrency;
                    if (ListReturnExit == null)
                        ListReturnExit = new List<SaleQuotationLineView>();
                    ListReturnExit.Add(mSQLine);
                    ListReturn = ListReturnExit;
                }
                List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
                double mRateGl = ListRate.Where(c => c.Currency == pBPCurrency).Select(c => c.Rate).FirstOrDefault();
                TempData["Rates" + pPageKey] = ListRate;
                ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pBPCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

                SalesQuotationItemsFormView mSalesQuotationItemsFormView = new SalesQuotationItemsFormView();
                mSalesQuotationItemsFormView.Lines = ListReturn;
                mSalesQuotationItemsFormView.DocCur = pBPCurrency;
                mSalesQuotationItemsFormView.UsersSetting = userSetting;
                mSalesQuotationItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSQ" + pPageKey];
                mSalesQuotationItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                //TempData["ListItems" + pPageKey] = ListReturn;
                TempData["ListItemsExit" + pPageKey] = ListReturn;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                TempData["FreightListSQ" + pPageKey] = mSalesQuotationItemsFormView.FreightList;
                ViewBag.CalculateRate = "Y";

                return PartialView("_ItemsForm", mSalesQuotationItemsFormView);
            }
            catch (Exception ex)
            {
                TempData["ListItemsExit" + pPageKey] = ListReturnExit;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetATPByWarehouse(string pItemCode, string pWarehouse)
        {

            List<AvailableToPromiseView> mATPList = mItemMasterManagerView.GetATPByWarehouse(((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, pWarehouse);
            return PartialView("_ATPTable", mATPList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Country"></param>
        /// <param name="SelectedState"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetStateList(string Country, string SelectedState)
        {
            List<StateSAP> StateList = mBusinessPartnerManagerView.GetStateList(((UserView)Session["UserLogin"]).CompanyConnect, Country);
            return PartialView("_States", Tuple.Create(StateList, SelectedState));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLineNum"></param>
        /// <param name="pUOMCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateUOM(int pLineNum, string pUOMCode, string pPageKey)
        {
            List<SaleQuotationLineView> ListReturnExit = (List<SaleQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            SaleQuotationLineView mSQLine = ListReturnExit.Where(c => c.LineNum == pLineNum).FirstOrDefault();
            UnitOfMeasure mUOMAux = mSQLine.UnitOfMeasureList.Where(c => c.UomCode == pUOMCode).FirstOrDefault();
            if (mUOMAux != null)
            {
                ItemPrices mItemPriceAux = mSQLine.ItemPrices.Where(c => c.UOMCode == mUOMAux.UomEntry.ToString()).FirstOrDefault();
                mSQLine.UomCode = mUOMAux.UomCode;
                if (mItemPriceAux != null)
                {
                    mSQLine.Price = mItemPriceAux.Price.Value;
                }
                else
                {
                    if (mUOMAux.AltQty != null)
                    {
                        if (mUOMAux.AltQty.Value != 0 && mUOMAux.AltQty.Value != 1)
                        {
                            mSQLine.Price = mSQLine.ItemPrices.FirstOrDefault().Price.Value / mUOMAux.AltQty.Value;
                        }
                        else
                        {
                            mSQLine.Price = mSQLine.ItemPrices.FirstOrDefault().Price.Value * mUOMAux.BaseQty.Value;
                        }
                    }
                    else
                    {
                        mSQLine.Price = mSQLine.ItemPrices.FirstOrDefault().Price.Value * mUOMAux.BaseQty.Value;
                    }
                }
            }

            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json(mSQLine.Price);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetItemsModel()
        {
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return PartialView("_Items", mOITMUDFList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pData"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult UploadAttachment(FormCollection pData)
        {
            string mFileName = string.Empty;
            string mPageKey = pData["pPageKey"].ToString();
            AttachmentView mAttachment = new AttachmentView();
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + mPageKey];
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;
                    AttachmentView mObj = new AttachmentView();
                    foreach (string mKey in mFiles)
                    {
                        HttpPostedFileBase mFilePost = mFiles[mKey];
                        mFileName = mFilePost.FileName;
                        string path = "~/images/" + mFileName;
                        mFileName = Server.MapPath(path);

                        if (System.IO.File.Exists(mFileName))
                        {
                            throw new Exception(SaleQuotations.TFAE);
                        }

                        mObj.FileName = Path.GetFileNameWithoutExtension(mFilePost.FileName);
                        mObj.FileExt = Path.GetExtension(mFilePost.FileName).Replace(".", "");
                        mObj.srcPath = Server.MapPath("~/images");
                        mObj.Date = DateTime.Today;

                        if (mFileList.Count == 0)
                        {
                            mObj.Line = 0;
                        }
                        else
                        {
                            mObj.Line = mFileList.Max(c => c.Line) + 1;
                        }

                        mFilePost.SaveAs(mFileName);
                        mFileList.Add(mObj);
                    }
                }

                TempData["Attachments" + mPageKey] = mFileList;
                return PartialView("_FilesList", mFileList);
            }
            catch (Exception ex)
            {
                TempData["Attachments" + mPageKey] = mFileList;
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult RemovedAtt(int pId, string pPageKey)
        {
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + pPageKey];
            mFileList.RemoveAll(c => c.Line == pId);

            TempData["Attachments" + pPageKey] = mFileList;
            return PartialView("_FilesList", mFileList);
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="reportUrl"></param>
		/// <param name="docEntry"></param>
		public void DownloadPDF(string reportUrl, int docEntry)
        {
            ARGNS.Util.CrystalReports crystalReports = new ARGNS.Util.CrystalReports();
            var report = crystalReports.GeneratePDFFile(docEntry, 23,
                System.IO.Path.Combine(Server.MapPath("~/Reports"), reportUrl),
                ((UserView)Session["UserLogin"]).CompanyConnect.Server,
                ((UserView)Session["UserLogin"]).CompanyConnect.DbUserName,
                ((UserView)Session["UserLogin"]).CompanyConnect.DbPassword,
                ((UserView)Session["UserLogin"]).CompanyConnect.CompanyDB,
                ((UserView)Session["UserLogin"]).CompanyConnect.ServerType == (int)Enums.eServerType.dst_HANA);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";


            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat,
            System.Web.HttpContext.Current.Response,
            true,
            "SalesQuotation_" + docEntry.ToString());
        }
    }
}