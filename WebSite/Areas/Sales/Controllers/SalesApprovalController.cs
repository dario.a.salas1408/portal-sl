﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
    [CustomerAuthorize(Enums.Areas.Sales)]
    public class SalesApprovalController : Controller
    {
        private ApprovalManagerView mApprovalManagerView;
        private UserManagerView mUserManagerView;
        private DraftManagerView mDraftManagerView;
        public SalesApprovalController()
        {
            mApprovalManagerView = new ApprovalManagerView();
            mUserManagerView = new UserManagerView();
            mDraftManagerView = new DraftManagerView();
        }

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
        [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.SalesApproval, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<DocumentConfirmationLineView> listDC = new List<DocumentConfirmationLineView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                return View(Tuple.Create(listDC, listDC));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="txtDocDate"></param>
		/// <param name="txtDocStatus"></param>
		/// <returns></returns>
        public PartialViewResult ListApproval(string txtDocDate = "", string txtDocStatus = "", string txtObjectId = "")
        {
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();
            DateTime? mDocDate = null;
            if (txtDocDate != "")
            {
                mDocDate = Convert.ToDateTime(txtDocDate);
            }
            List<DocumentConfirmationLineView> listDocuments = mApprovalManagerView.GetSalesApprovalListSearch(
				((UserView)Session["UserLogin"]).CompanyConnect, 
				((UserView)Session["UserLogin"]).IdUser, mDocDate,txtDocStatus, txtObjectId);

			List<UserSAP> userList = mUserManagerView.GetUserSapList(((UserView)Session["UserLogin"]).CompanyConnect);
            listDocuments = listDocuments.Select(c => { c.UserSignName = (userList.Where(j => j.USERID == c.UserSign).FirstOrDefault() != null ? userList.Where(j => j.USERID == c.UserSign).FirstOrDefault().U_NAME : ""); return c; }).ToList();

			TempData["ListSDC"] = listDocuments;
            TempData["ListUsers"] = userList;

            return PartialView("_ApprovalList", listDocuments);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="txtDocDate"></param>
		/// <param name="txtDocStatus"></param>
		/// <returns></returns>
        public PartialViewResult ListApprovalByOriginator(string txtDocDate = "", string txtDocStatus = "", string txtObjectId = "")
        {
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();
            DateTime? mDocDate = null;
            if (txtDocDate != "")
            {
                mDocDate = Convert.ToDateTime(txtDocDate);
            }

            List<DocumentConfirmationLineView> confirmationList = mApprovalManagerView.GetSalesApprovalByOriginator(((UserView)Session["UserLogin"]).CompanyConnect, 
				((UserView)Session["UserLogin"]).IdUser, mDocDate, txtDocStatus, txtObjectId);

			List<UserSAP> userList = mUserManagerView.GetUserSapList(((UserView)Session["UserLogin"]).CompanyConnect);
            confirmationList = confirmationList.Select(c => { c.UserSignName = (userList.Where(j => j.USERID == c.UserSign).FirstOrDefault() != null ? userList.Where(j => j.USERID == c.UserSign).FirstOrDefault().U_NAME : ""); c.ByOriginator = true; return c; }).ToList();

            return PartialView("_ApprovalList", confirmationList);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="WddCode"></param>
		/// <returns></returns>
        public PartialViewResult GetApprovalDesicion(string WddCode)
        {   
            int mWddCode = Convert.ToInt32(WddCode);
            List<DocumentConfirmationLineView> ListDC = (List<DocumentConfirmationLineView>)TempData["ListSDC"];
            TempData["ListSDC"] = ListDC;
            return PartialView("_ApprovalTemplate", ListDC.Where(c => c.WddCode == mWddCode).FirstOrDefault());
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="owwdCode"></param>
		/// <param name="remark"></param>
		/// <param name="approvalCode"></param>
        public void SaveApprovalResponse(string owwdCode, string remark, string approvalCode)
        {
            UsersSetting userPortal = mApprovalManagerView.GetUserPortal(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);
            mApprovalManagerView.SaveApprovalResponse(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(owwdCode), remark, userPortal, approvalCode);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="WddCode"></param>
		/// <returns></returns>
        public PartialViewResult _GetHistory(int WddCode)
        {
            List<UserSAP> ListUserSAP = (List<UserSAP>)TempData["ListUsers"];
            List<StageView> ListStage = mApprovalManagerView.GetStageList(((UserView)Session["UserLogin"]).CompanyConnect);

            List<DocumentConfirmationLineView> listReturn = mApprovalManagerView.GetApprovalListByID(((UserView)Session["UserLogin"]).CompanyConnect, WddCode);
            listReturn = listReturn.Select(c => { c.UserName = ListUserSAP.Where(j => j.USERID == c.UserID).FirstOrDefault().U_NAME; c.StageName = ListStage.Where(k => k.WstCode == c.StepCode).FirstOrDefault().Name; return c; }).ToList();

            TempData["ListUsers"] = ListUserSAP;

            return PartialView("_ApprovalHistory", listReturn);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="DraftDocEntry"></param>
        public void CreateDocument(string DraftDocEntry)
        {
            mDraftManagerView.CreateDocument(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(DraftDocEntry));
        }
    }
}