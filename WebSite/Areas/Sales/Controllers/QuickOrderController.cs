﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.ItemMaster;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
    [CustomerAuthorize(Enums.Areas.Sales)]
    public class QuickOrderController : Controller
    {
        private ItemMasterManagerView mItemMasterManagerView;
        private QuickOrderManagerView mQuickOrderManagerView;
        private UserManagerView mUserManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        public QuickOrderController()
        {
            mItemMasterManagerView = new ItemMasterManagerView();
            mQuickOrderManagerView = new QuickOrderManagerView();
            mUserManagerView = new UserManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SalesOrderId"></param>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.SOQuickOrder, Enums.Actions.Add)]
        public ActionResult Index(int SalesOrderId = 0)
        {
            List<ItemMasterView> mListItemMaster = new List<ItemMasterView>();
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            List<QuickOrderView> mListQuickOrder = new List<QuickOrderView>();
            mListQuickOrder = mQuickOrderManagerView.GetQuickOrders(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, (((UserView)Session["UserLogin"]).BPCode != null ? ((UserView)Session["UserLogin"]).BPCode : ""), true);
            List<ItemGroupSAP> mListItemGroup = new List<ItemGroupSAP>();
            mListItemGroup.Add(new ItemGroupSAP(-1,"Item Group"));
            mListItemGroup.AddRange(mItemMasterManagerView.GetItemGroupList(((UserView)Session["UserLogin"]).CompanyConnect));
            List<WarehouseView> mListWarehouseView = new List<WarehouseView>();
            mListWarehouseView = mItemMasterManagerView.GetAvailableWarehousesByCompany(((UserView)Session["UserLogin"]).CompanyConnect);


            QuickOrderParamsView mQuickOrderParamsView = new QuickOrderParamsView();
            mQuickOrderParamsView.ListItemMaster = mListItemMaster;
            mQuickOrderParamsView.ListOITMUDF = mOITMUDFList;
            mQuickOrderParamsView.ListQuickOrder = mListQuickOrder;
            mQuickOrderParamsView.SalesOrderId = SalesOrderId;
            mQuickOrderParamsView.ListItemGroup = mListItemGroup;
            mQuickOrderParamsView.ListWarehouse = mListWarehouseView;

            return View(mQuickOrderParamsView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUDF_Code"></param>
        /// <param name="pUDF_Name"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pTxtCodeBP"></param>
        /// <param name="pTileSearch"></param>
        /// <param name="pDoubleSearch"></param>
        /// <param name="pZeroStock"></param>
        /// <param name="pUDFList"></param>
        /// <param name="pExecuteSearch"></param>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.SOQuickOrder, Enums.Actions.Add)]
        public ActionResult IndexWithParams(string pUDF_Code = "", string pUDF_Name = "", string pItemGroup = "", string pTxtCodeBP = "", bool pTileSearch = false, bool pDoubleSearch = false, bool pZeroStock = false, string pUDFList = "", bool pExecuteSearch = false)
        {
            List<ItemMasterView> mListItemMaster = new List<ItemMasterView>();
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            List<QuickOrderView> mListQuickOrder = new List<QuickOrderView>();
            mListQuickOrder = mQuickOrderManagerView.GetQuickOrders(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, (((UserView)Session["UserLogin"]).BPCode != null ? ((UserView)Session["UserLogin"]).BPCode : ""), true);
            List<ItemGroupSAP> mListItemGroup = new List<ItemGroupSAP>();
            mListItemGroup.Add(new ItemGroupSAP(-1, "Item Group"));
            mListItemGroup.AddRange(mItemMasterManagerView.GetItemGroupList(((UserView)Session["UserLogin"]).CompanyConnect));
            List<WarehouseView> mListWarehouseView = new List<WarehouseView>();
            mListWarehouseView = mItemMasterManagerView.GetAvailableWarehousesByCompany(((UserView)Session["UserLogin"]).CompanyConnect);

            Dictionary<string, string> mDictionaryUDF = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(pUDFList))
            {
                string[] mUDFList = pUDFList.Split(';');
                foreach (string mUDF in mUDFList)
                {
                    mDictionaryUDF.Add(mUDF.Split(',')[0], mUDF.Split(',')[1]);
                }
            }

            QuickOrderParamsView mQuickOrderParamsView = new QuickOrderParamsView();
            mQuickOrderParamsView.ListItemMaster = mListItemMaster;
            mQuickOrderParamsView.ListOITMUDF = mOITMUDFList;
            mQuickOrderParamsView.ListQuickOrder = mListQuickOrder;
            mQuickOrderParamsView.ListItemGroup = mListItemGroup;
            mQuickOrderParamsView.ListWarehouse = mListWarehouseView;

            mQuickOrderParamsView.UDF_Code = pUDF_Code;
            mQuickOrderParamsView.UDF_Name = pUDF_Name;
            mQuickOrderParamsView.ItemGroup = pItemGroup;
            mQuickOrderParamsView.TxtCodeBP = pTxtCodeBP;
            mQuickOrderParamsView.TileSearch = pTileSearch;
            mQuickOrderParamsView.DoubleSearch = pDoubleSearch;
            mQuickOrderParamsView.ZeroStock = pZeroStock;
            mQuickOrderParamsView.UDFListValues = mDictionaryUDF;
            mQuickOrderParamsView.ExecuteSearch = pExecuteSearch;

            return View("Index", mQuickOrderParamsView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListItemQuickOrder([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();
                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
                
                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetQuickOrderListBy(((UserView)Session["UserLogin"]).CompanyConnect, 
                    "Y", "N", searchViewModel.pMappedUdf, 
                    "", ((UserView)Session["UserLogin"]).IdUser, 
                    searchViewModel.pItemCode, searchViewModel.pItemName, 
                    searchViewModel.pCardCode, 
                    Convert.ToInt16(searchViewModel.pItemGroupCode), 
                    true,true,
                    searchViewModel.pNoItemsWithZeroStockCk, 
                    searchViewModel.pSupplierCardCode, null, 
                    requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                List<ItemGroupSAP> mItemGroupList = mItemMasterManagerView.GetItemGroupList(((UserView)Session["UserLogin"]).CompanyConnect);
                mJsonObjectResult.ItemMasterSAPList = mJsonObjectResult.ItemMasterSAPList.Select(c => { c.ItmsGrpNam = mItemGroupList.Where(j => j.ItmsGrpCod == c.ItmsGrpCod).FirstOrDefault().ItmsGrpNam; return c; }).ToList();

                return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ListItemQuickOrderTile(AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();
                
                QuickOrderParamsView mQuickOrderParamsView = new QuickOrderParamsView();
                mQuickOrderParamsView.TilesSearchCk = searchViewModel.pTilesSearchCk;
                mQuickOrderParamsView.DoubleTilesSearchCk = searchViewModel.pDoubleTilesSearchCk;
                mQuickOrderParamsView.ColumnRadio = searchViewModel.pColumnRadio;
                mQuickOrderParamsView.FirstDoubleSearch = searchViewModel.pFirstDoubleSearch;
                mQuickOrderParamsView.TileToModify = searchViewModel.pTileToModify;

                OrderColumn mOrderColumn = new OrderColumn();
                mOrderColumn.Name = "ItemCode";
                mOrderColumn.IsOrdered = true;
                mOrderColumn.SortDirection = OrderColumn.OrderDirection.Ascendant;

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetQuickOrderListBy(((UserView)Session["UserLogin"]).CompanyConnect, 
                    "Y", "N", searchViewModel.pMappedUdf, "", 
                    ((UserView)Session["UserLogin"]).IdUser, 
                    searchViewModel.pItemCode, searchViewModel.pItemName, 
                    searchViewModel.pCardCode, Convert.ToInt16(searchViewModel.pItemGroupCode), 
                    true,true,
                    searchViewModel.pNoItemsWithZeroStockCk, searchViewModel.pSupplierCardCode, 
                    null, searchViewModel.pStart, searchViewModel.pLength, mOrderColumn);

                mQuickOrderParamsView.TotalQuantity = Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value);

                return PartialView("_QuickOrderDoubleTiles", Tuple.Create(mJsonObjectResult.ItemMasterSAPList, mQuickOrderParamsView));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetQuickOrderList(AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
                return PartialView("_QuickOrderList", mOITMUDFList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCustomerCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ListItemCarrito(string pCustomerCode)
        {
            List<QuickOrderView> mListQuickOrder = new List<QuickOrderView>();
            mListQuickOrder = mQuickOrderManagerView.GetQuickOrders(
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                ((UserView)Session["UserLogin"]).IdUser, 
                ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, 
                pCustomerCode, true);

            List<WarehouseView> mListWarehouseView = new List<WarehouseView>();
            mListWarehouseView = mItemMasterManagerView.GetAvailableWarehousesByCompany(
                ((UserView)Session["UserLogin"]).CompanyConnect);

            return PartialView("_QuickOrderHeader", Tuple.Create(mListQuickOrder, mListWarehouseView));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemCode"></param>
        /// <param name="pQuantity"></param>
        /// <param name="pCustomerCode"></param>
        /// <returns></returns>
        [HttpPost]
        public bool AddToCart(string pItemCode, int pQuantity, string pCustomerCode)
        {
            QuickOrderView tryQuickOrderView = new QuickOrderView();
            tryQuickOrderView = mQuickOrderManagerView.GetQuickOrderLine(((UserView)Session["UserLogin"]).IdUser, pItemCode, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
            QuickOrderView QuickOrderView = new QuickOrderView();

            if (tryQuickOrderView != null)
            {
                QuickOrderView.IdQuickOrder = tryQuickOrderView.IdQuickOrder;
                QuickOrderView.IdUser = tryQuickOrderView.IdUser;
                QuickOrderView.IdCompany = tryQuickOrderView.IdCompany;
                QuickOrderView.IdBp = tryQuickOrderView.IdBp;
                QuickOrderView.ItemCode = tryQuickOrderView.ItemCode;
                QuickOrderView.Quantity = tryQuickOrderView.Quantity + pQuantity;

                return mQuickOrderManagerView.Update(QuickOrderView);
            }
            else
            {
                List<WarehouseView> mListWarehouseView = new List<WarehouseView>();
                mListWarehouseView = mItemMasterManagerView.GetAvailableWarehousesByCompany(((UserView)Session["UserLogin"]).CompanyConnect);
                QuickOrderView.IdUser = ((UserView)Session["UserLogin"]).IdUser;
                QuickOrderView.IdCompany = ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany;
                QuickOrderView.IdBp = pCustomerCode;
                QuickOrderView.ItemCode = pItemCode;
                QuickOrderView.Quantity = pQuantity;
                if(mListWarehouseView.FirstOrDefault() != null)
                { 
                    QuickOrderView.WarehouseCode = mListWarehouseView.FirstOrDefault().WhsCode;
                }

                return mQuickOrderManagerView.Add(QuickOrderView);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemCode"></param>
        /// <param name="pQuantity"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        [HttpPost]
        public bool UpdateCart(string pItemCode, int pQuantity, string pWarehouse)
        {
            QuickOrderView tryQuickOrderView = new QuickOrderView();
            tryQuickOrderView = mQuickOrderManagerView.GetQuickOrderLine(((UserView)Session["UserLogin"]).IdUser, pItemCode, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
            QuickOrderView QuickOrderView = new QuickOrderView();

            if (tryQuickOrderView != null)
            {
                QuickOrderView.IdQuickOrder = tryQuickOrderView.IdQuickOrder;
                QuickOrderView.IdUser = tryQuickOrderView.IdUser;
                QuickOrderView.IdBp = tryQuickOrderView.IdBp;
                QuickOrderView.ItemCode = tryQuickOrderView.ItemCode;
                QuickOrderView.Quantity = pQuantity;
                QuickOrderView.WarehouseCode = pWarehouse;

                return mQuickOrderManagerView.Update(QuickOrderView);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public bool DeleteLineFromCart(string pItemCode)
        {
            QuickOrderView QuickOrderView = new QuickOrderView();

            QuickOrderView.IdUser = ((UserView)Session["UserLogin"]).IdUser;
            QuickOrderView.ItemCode = pItemCode;
            QuickOrderView.IdCompany = ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany;

            return mQuickOrderManagerView.Delete(QuickOrderView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public bool DeleteCart()
        {
            mQuickOrderManagerView.DeleteCart(((UserView)Session["UserLogin"]).IdUser, 
                ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Suppliers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, 
                OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); })
                .CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor, true, 
                searchViewModel.pCardCode, searchViewModel.pCardName, 
                false, null, null, false, requestModel.Start, requestModel.Length, 
                mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            return Json(new DataTablesResponse(requestModel.Draw, 
                mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), 
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Customers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>();
                cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); })
                .CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, true, 
                    searchViewModel.pCardCode, 
                    searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, 
                    ((UserView)Session["UserLogin"]).SECode, 
                    ((UserView)Session["UserLogin"]).BPGroupId, 
                    true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, true,
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, 
                    false, null, null, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, 
                mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), 
                JsonRequestBehavior.AllowGet);
        }
    }
}