﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.DiverSO;
using ARGNS.Model.Implementations.DiverSO.Catalog;
using ARGNS.Model.Implementations.DiverSO.SalesDocProcess;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.App_Start;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Draft;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.SaleOrders;
using AutoMapper;
using CrystalDecisions.Shared;
using DataTables.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using static ARGNS.Util.Enums;

namespace ARGNS.WebSite.Areas.Sales.Controllers
{
    [CustomerAuthorize(Enums.Areas.Sales)]
    public class SalesOrderController : Controller
    {
        private SaleOrderManagerView mPSOManagerView;
        private LocalDraftsManagerView mLocalDraftsManagerView;
        private SaleQuotationManagerView mPSQManagerView;
        private ItemMasterManagerView mItemMasterManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private DraftManagerView mDraftManagerView;
        private PDMManagerView mPDMManagerView;
        private CompaniesManagerView mCompaniesManagerView;
        private UserManagerView mUserManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;
        private QuickOrderManagerView mQuickOrderManagerView;
        private ProjectSAPManagerView mProjectSAPManagerView;
        private SerialManagerView serialManagerView;
        private PaymentMeanManagerView paymentMeanManagerView;
        private bool mSplitOrder;
        public static List<PaymentMeanOrder> paymentMeanOrders;
        private static string surchargeConfig = WebConfigurationManager.AppSettings["Recargo"];

        public SalesOrderController()
        {
            mLocalDraftsManagerView = new LocalDraftsManagerView();
            mPSOManagerView = new SaleOrderManagerView();
            mPSQManagerView = new SaleQuotationManagerView();
            mItemMasterManagerView = new ItemMasterManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mDraftManagerView = new DraftManagerView();
            mPDMManagerView = new PDMManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
            mUserManagerView = new UserManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
            mQuickOrderManagerView = new QuickOrderManagerView();
            mProjectSAPManagerView = new ProjectSAPManagerView();
            serialManagerView = new SerialManagerView();
            paymentMeanManagerView = new PaymentMeanManagerView();
            mSplitOrder = WebConfigurationManager.AppSettings["SplitOrder"] == "true" ? true : false;


            Mapper.CreateMap<DraftView, SaleOrderView>();
            Mapper.CreateMap<DraftLineView, SaleOrderLineView>();
            Mapper.CreateMap<SaleQuotationView, SaleOrderView>();
            Mapper.CreateMap<SaleQuotationLineView, SaleOrderLineView>();
            Mapper.CreateMap<UserUDFView, UDF_ARGNS>();
            Mapper.CreateMap<UDF_ARGNS, UserUDFView>();
            Mapper.CreateMap<SaleOrderSAPLine, ItemMasterView>();
            //Mapper.CreateMap<ItemMasterView, ItemMasterSAP>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.Sales, Enums.Pages.SalesOrder, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
                List<SaleOrderView> ListPO = new List<SaleOrderView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();


                if (System.Web.HttpRuntime.Cache["SOList" + mUser] != null)
                {
                    ListPO = (List<SaleOrderView>)System.Web.HttpRuntime.Cache["SOList" + mUser];
                }

                return View(Tuple.Create(ListPO, ListBp));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="reportUrl"></param>
		/// <param name="docEntry"></param>
		public void DownloadPDF(string reportUrl, int docEntry)
        {
            ARGNS.Util.CrystalReports crystalReports = new ARGNS.Util.CrystalReports();
            var report = crystalReports.GeneratePDFFile(docEntry, 17,
                Path.Combine(Server.MapPath("~/Reports"), reportUrl),
                ((UserView)Session["UserLogin"]).CompanyConnect.Server,
                ((UserView)Session["UserLogin"]).CompanyConnect.DbUserName,
                ((UserView)Session["UserLogin"]).CompanyConnect.DbPassword,
                ((UserView)Session["UserLogin"]).CompanyConnect.CompanyDB,
                ((UserView)Session["UserLogin"]).CompanyConnect.ServerType == (int)Enums.eServerType.dst_HANA);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/pdf";

            try
            {
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat,
            System.Web.HttpContext.Current.Response,
            true,
            "SalesOrder_" + docEntry.ToString());
            }
            finally
            {
                report.Close();
                report.Dispose();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionPurchaseOrder"></param>
        /// <param name="IdPO"></param>
        /// <param name="fromController"></param>
        /// <param name="pQuickOrderId"></param>
        /// <param name="pCustomerCode"></param>
        /// <returns></returns>
        public ActionResult ActionPurchaseOrder(string ActionPurchaseOrder, int IdPO,
            string fromController,
            string pQuickOrderId = "",
            string pCustomerCode = "")
        {
            SaleOrderView mPSOView = null;
            SaleOrderSAP mSO = null;
            List<UDF_ARGNS> mListUDFCRD1 = null;
            List<string> crystalReportsList = new List<string>();

            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            TempData["ListItems" + mPageKey] = null;
            TempData["FreeSerials"] = null;
            TempData["UsedSerials"] = null;
            paymentMeanOrders = new List<PaymentMeanOrder>();


            bool hasPermissionToViewOpenQuantity = ((UserView)Session["UserLogin"]).ListUserPageAction
                .Where(c => c.PageInternalKey == Enums.Pages.SalesOrder.ToDescriptionString()
                    && c.InternalKey == Enums.Actions.ShowOpenQuantity.ToDescriptionString())
                .FirstOrDefault() != null && fromController == "SalesOrder" ? true : false;

            List<ItemGroupSAP> mItemGroupList = mItemMasterManagerView
                .GetItemGroupList(((UserView)Session["UserLogin"]).CompanyConnect);

            switch (fromController)
            {
                case "SalesOrder":
                case "LocalDrafts":
                    ViewBag.URLRedirect = "/Sales/SalesOrder/Index";
                    ViewBag.controllerName = "SalesOrder";
                    break;
                case "SalesQuotation":
                    ViewBag.URLRedirect = "/Sales/SalesQuotation/Index";
                    ViewBag.controllerName = "SalesQuotation";
                    break;
                case "MyDocuments":
                    ViewBag.URLRedirect = "/Sales/SalesDraft/Index";
                    ViewBag.controllerName = "SalesDraft";
                    break;
                case "MyApprovals":
                    ViewBag.URLRedirect = "/Sales/SalesApproval/Index";
                    ViewBag.controllerName = "SalesApproval";
                    break;
                case "QuickOrder":
                    ViewBag.URLRedirect = "/Sales/QuickOrder/Index";
                    ViewBag.controllerName = "QuickOrder";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            switch (ActionPurchaseOrder)
            {
                case "Add":
                    crystalReportsList = mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesOrder,
                        ((UserView)Session["UserLogin"]).CompanyConnect);
                    ViewBag.Tite = SaleOrders.ASO;
                    ViewBag.FormMode = ActionPurchaseOrder;
                    ViewBag.QuickOrderId = pQuickOrderId;
                    ViewBag.From = fromController;

                    mPSOView = mPSOManagerView.GetSaleOrder(0,
                        ((UserView)Session["UserLogin"]).IdUser,
                        ((UserView)Session["UserLogin"]).CompanyConnect, false);

                    mPSOView.MappedUdf = mUserManagerView.GetUserUDFSearch(
                        ((UserView)Session["UserLogin"]).CompanyConnect,
                        ((UserView)Session["UserLogin"]).IdUser,
                        ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "ORDR");

                    mListUDFCRD1 = mUserManagerView.GetUserUDFSearch(
                        ((UserView)Session["UserLogin"]).CompanyConnect,
                        ((UserView)Session["UserLogin"]).IdUser,
                        ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "CRD1");

                    mPSOView.SOAddress.MappedUdfShipTo = mListUDFCRD1;
                    mPSOView.SOAddress.MappedUdfBillTo = mListUDFCRD1;
                    mPSOView.DocDate = System.DateTime.Now;
                    mPSOView.DocDueDate = System.DateTime.Now;
                    mPSOView.TaxDate = System.DateTime.Now;
                    mPSOView.ReqDate = null;
                    mPSOView.CancelDate = null;

                    if (((UserView)Session["UserLogin"]).IsSalesEmployee == true && ((UserView)Session["UserLogin"]).SECode != null)
                    {
                        mPSOView.SlpCode = ((UserView)Session["UserLogin"]).SECode.Value;
                    }
                    TempData["ListItemsExit" + mPageKey] = null;
                    break;
                case "View":
                case "Update":
                    ViewBag.FormMode = ActionPurchaseOrder;
                    ViewBag.QuickOrderId = pQuickOrderId;
                    ViewBag.From = fromController;

                    if (fromController.Equals("LocalDrafts"))
                    {
                        crystalReportsList = new List<string>();
                        ViewBag.Tite = SaleOrders.ULD;

                        mPSOView = mLocalDraftsManagerView.GetDraftDocument(IdPO,
                            ((UserView)Session["UserLogin"]).IdUser,
                            ((UserView)Session["UserLogin"]).CompanyConnect);

                        mSO = mPSOManagerView.GetSOInternalObjects(
                            ((UserView)Session["UserLogin"]).CompanyConnect, mPSOView);

                        mPSOView = Mapper.Map<SaleOrderView>(mSO);
                        mPSOView.Lines = Mapper.Map<List<SaleOrderLineView>>(mSO.Lines);
                    }
                    else
                    {
                        crystalReportsList = mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesOrder,
                        ((UserView)Session["UserLogin"]).CompanyConnect);
                        if (ActionPurchaseOrder == "View")
                            ViewBag.Tite = SaleOrders.VSO;
                        else
                            ViewBag.Tite = SaleOrders.USO;

                        mPSOView = mPSOManagerView.GetSaleOrder(IdPO,
                            ((UserView)Session["UserLogin"]).IdUser,
                            ((UserView)Session["UserLogin"]).CompanyConnect,
                            hasPermissionToViewOpenQuantity);
                    }

                    List<StockModelView> mListStock = mItemMasterManagerView.GetItemWithStock(
                        ((UserView)Session["UserLogin"]).CompanyConnect,
                        mPSOView.Lines.Select(c => c.ItemCode).ToList());

                    mPSOView.Lines.Select(c =>
                    {
                        c.ListCurrency = mPSOView.ListDocumentSAPCombo.ListCurrency;
                        c.hasStock = (mListStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                        c.anoSelected = "0";
                        return c;
                    }).ToList();

                    SerialSAP serialSAP = serialManagerView.GetSerialsByDocument(((UserView)Session["UserLogin"]).CompanyConnect, mPSOView.DocEntry, -1, string.Empty, DocumentsType.SO, "", ((UserView)Session["UserLogin"]).IdUser, 0);

                    TempData["UsedSerials"] = serialSAP.SerialsInDocument;

                    foreach (var item in mPSOView.Lines)
                    {
                        if (WebConfigurationManager.AppSettings["udfWarranty"] != string.Empty)
                        {
                            if (item.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfWarranty"]).Count() > 0)
                            {
                                if (item.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfWarranty"]).FirstOrDefault().Value == "S")
                                {
                                    item.isWarranty = true;
                                    if (item.Dscription.Split('-').Count() > 2)
                                    {
                                        item.fromWarrantyItem = item.Dscription.Split('-')[1].TrimStart().TrimEnd();
                                    }
                                }
                            }
                        }
                    }



                    TempData["ListItemsExit" + mPageKey] = mPSOView.Lines;
                    break;
                case "ViewDraft":
                    crystalReportsList = mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesOrder,
                        ((UserView)Session["UserLogin"]).CompanyConnect);
                    ViewBag.Tite = Drafts.SOD;
                    ViewBag.FormMode = "View";
                    ViewBag.QuickOrderId = pQuickOrderId;
                    ViewBag.From = fromController;
                    DraftView draft = mDraftManagerView.GetDraftById(IdPO,
                        ((UserView)Session["UserLogin"]).CompanyConnect);

                    var GetDefaults = mPSOManagerView.GetSaleOrder(0,
                      ((UserView)Session["UserLogin"]).IdUser,
                      ((UserView)Session["UserLogin"]).CompanyConnect, false);

                    mPSOView = new SaleOrderView();
                    mPSOView = Mapper.Map<SaleOrderView>(draft);
                    mPSOView.Lines = Mapper.Map<List<SaleOrderLineView>>(draft.Lines);
                    mPSOView.Lines.ForEach(c => c.CalcTax = "Y");
                    List<SalesTaxCodesSAP> mTaxCodeListDraft = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);

                    foreach (var item in mPSOView.Lines)
                    {
                        if (!string.IsNullOrEmpty(item.TaxCode))
                        {
                            item.TaxCodeAR = item.TaxCode;
                            item.VatPrcnt = mTaxCodeListDraft.Where(t => t.Code == item.TaxCode).Single().Rate;
                        }
                    }

                    mSO = mPSOManagerView.GetSOInternalObjects(
                         ((UserView)Session["UserLogin"]).CompanyConnect, mPSOView);

                    mSO.PaymentMeanOrder = paymentMeanManagerView.GetForDraft(((UserView)Session["UserLogin"]).CompanyConnect, IdPO);

                    mPSOView = Mapper.Map<SaleOrderView>(mSO);
                    mPSOView.Lines = Mapper.Map<List<SaleOrderLineView>>(mSO.Lines);
                    mPSOView.Lines.Select(c => { c.ListCurrency = mPSOView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();

                    mPSOView.Warehouse = GetDefaults.Warehouse;
                    mPSOView.LocalCurrency = GetDefaults.LocalCurrency;
                    mPSOView.GLAccountSAP = GetDefaults.GLAccountSAP;
                    mPSOView.SystemCurrency = GetDefaults.SystemCurrency;
                    mPSOView.DistrRuleSAP = GetDefaults.DistrRuleSAP;

                    TempData["ListItemsExit" + mPageKey] = mPSOView.Lines;
                    break;

                case "CopyFromSQ":
                    crystalReportsList = mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesOrder,
                        ((UserView)Session["UserLogin"]).CompanyConnect);
                    ViewBag.Tite = SaleOrders.ASO;
                    ViewBag.FormMode = "Add";
                    ViewBag.QuickOrderId = pQuickOrderId;
                    ViewBag.From = fromController;
                    SaleQuotationView mSQView = mPSQManagerView.GetSaleQuotation(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPSOView = new SaleOrderView();
                    mPSOView = Mapper.Map<SaleOrderView>(mSQView);
                    mPSOView.SOAddress = mSQView.SQAddress;
                    mPSOView.Lines = Mapper.Map<List<SaleOrderLineView>>(mSQView.Lines);

                    mSO = mPSOManagerView.GetSOInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mPSOView);
                    mPSOView = Mapper.Map<SaleOrderView>(mSO);
                    mPSOView.Lines = Mapper.Map<List<SaleOrderLineView>>(mSO.Lines);

                    List<StockModelView> mLIstStockSQ = mItemMasterManagerView.
                        GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect,
                        mPSOView.Lines.Select(c => c.ItemCode).ToList());

                    mPSOView.Lines.Select(c =>
                    {
                        c.ListCurrency = mPSOView.ListDocumentSAPCombo.ListCurrency;
                        c.BaseType = 23;
                        c.hasStock = (mLIstStockSQ.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                        c.BaseEntry = mPSOView.DocEntry;
                        c.BaseLine = c.LineNum;
                        c.BaseRef = mPSOView.DocNum.ToString();
                        return c;
                    }).ToList();

                    //Actualizo estas 2 fechas a la fecha actual tal como hace SAP y setteo el DocNumber en 0 para evitar equivocaciones por parte del usuario
                    mPSOView.DocDate = DateTime.Today;
                    mPSOView.TaxDate = DateTime.Today;
                    mPSOView.DocNum = 0;
                    mPSOView.ListDocumentSAPCombo.ListPaymentMethod = mSQView.ListDocumentSAPCombo.ListPaymentMethod;
                    TempData["ListItemsExit" + mPageKey] = mPSOView.Lines;
                    break;
                case "CopyFromLD":
                    crystalReportsList = mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesOrder,
                        ((UserView)Session["UserLogin"]).CompanyConnect);
                    ViewBag.Tite = SaleOrders.ASO;
                    ViewBag.FormMode = "Add";
                    ViewBag.QuickOrderId = pQuickOrderId;
                    ViewBag.From = fromController;

                    SaleOrderView mLocalDraft = mLocalDraftsManagerView.GetDraftDocument(IdPO,
                           ((UserView)Session["UserLogin"]).IdUser,
                           ((UserView)Session["UserLogin"]).CompanyConnect);

                    mSO = mPSOManagerView.GetSOInternalObjects(
                        ((UserView)Session["UserLogin"]).CompanyConnect, mLocalDraft);

                    mPSOView = new SaleOrderView();
                    mPSOView = Mapper.Map<SaleOrderView>(mSO);
                    mPSOView.SOAddress = mPSOView.SOAddress;
                    mPSOView.Lines = Mapper.Map<List<SaleOrderLineView>>(mSO.Lines);

                    mPSOView.MappedUdf = mUserManagerView.GetUserUDFSearch(
                        ((UserView)Session["UserLogin"]).CompanyConnect,
                        ((UserView)Session["UserLogin"]).IdUser,
                        ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "ORDR");

                    mListUDFCRD1 = mUserManagerView.GetUserUDFSearch(
                        ((UserView)Session["UserLogin"]).CompanyConnect,
                        ((UserView)Session["UserLogin"]).IdUser,
                        ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "CRD1");

                    mPSOView.SOAddress.MappedUdfShipTo = mListUDFCRD1;
                    mPSOView.SOAddress.MappedUdfBillTo = mListUDFCRD1;

                    List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
                    UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
                    List<SalesTaxCodesSAP> mTaxCodeList = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
                    SalesTaxCodesSAP mDefTAxCode;
                    if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
                    {
                        mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
                    }
                    else
                    {//TODO AutomaticTest ALSO IN MKT DOCUMENTS
                        mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
                    }

                    List<StockModelView> mLIstStockLD = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect,
                        mPSOView.Lines.Select(c => c.ItemCode).ToList());

                    List<ItemMasterView> mListIMAux = mItemMasterManagerView.GeUOMByItemList(((UserView)Session["UserLogin"]).CompanyConnect,
                        Mapper.Map<List<ItemMasterView>>(mSO.Lines), "Y", "N");

                    mListIMAux = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, mListIMAux, mPSOView.CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

                    int mLine = 0;
                    foreach (SaleOrderLineView mLineAux in mPSOView.Lines)
                    {
                        mLineAux.ListCurrency = mPSOView.ListDocumentSAPCombo.ListCurrency;
                        mLineAux.hasStock = (mLIstStockLD.Where(d => d.ItemCode == mLineAux.ItemCode).Count() > 0 ? true : false);
                        mLineAux.UnitOfMeasureList = mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().UnitOfMeasureList;
                        mLineAux.UgpEntry = mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().UgpEntry;
                        if (mLineAux.UomCode == null)
                            mLineAux.UomCode = mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault().UomCode;
                        mLineAux.UomGroupName = mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().UomGroupName;
                        mLineAux.ItemPrices = mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().ItemPrices;
                        mLineAux.LineNum = mLine;
                        mLineAux.TaxCode = mDefTAxCode.Code;
                        mLineAux.VatPrcnt = mDefTAxCode.Rate;
                        if (mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().TaxCodeAR != null)
                        {
                            mLineAux.TaxCodeAR = mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().TaxCodeAR;
                            mLineAux.VatPrcnt = mTaxCodeList.Where(t => t.Code == mListIMAux.Where(j => j.ItemCode == mLineAux.ItemCode).FirstOrDefault().TaxCodeAR).Single().Rate;
                        }
                        mLine++;
                    }

                    //Actualizo estas 2 fechas a la fecha actual tal como hace SAP y setteo el DocNumber en 0 para evitar equivocaciones por parte del usuario
                    mPSOView.DocDate = DateTime.Today;
                    mPSOView.TaxDate = DateTime.Today;
                    mPSOView.DocNum = 0;
                    mPSOView.ListDocumentSAPCombo.ListPaymentMethod = mPSOView.ListDocumentSAPCombo.ListPaymentMethod;
                    TempData["ListItemsExit" + mPageKey] = mPSOView.Lines;
                    break;
                case "QuickOrder":
                    crystalReportsList = mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.SalesOrder,
                        ((UserView)Session["UserLogin"]).CompanyConnect);
                    ViewBag.Tite = SaleOrders.ASO;
                    ViewBag.FormMode = "Add";
                    ViewBag.QuickOrderId = pQuickOrderId;
                    ViewBag.From = fromController;


                    mPSOView = mPSOManagerView.GetSaleOrder(0,
                        ((UserView)Session["UserLogin"]).IdUser,
                        ((UserView)Session["UserLogin"]).CompanyConnect,
                        hasPermissionToViewOpenQuantity);

                    mPSOView.CardCode = pCustomerCode;
                    mPSOView.CardName = mBusinessPartnerManagerView.GetBusinessPartner(pCustomerCode, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser).CardName;
                    mPSOView.MappedUdf = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "ORDR");
                    mPSOView.DocDate = System.DateTime.Now;
                    mPSOView.DocDueDate = System.DateTime.Now;
                    mPSOView.TaxDate = System.DateTime.Now;
                    mPSOView.ReqDate = null;
                    mPSOView.CancelDate = null;
                    if (((UserView)Session["UserLogin"]).IsSalesEmployee == true && ((UserView)Session["UserLogin"]).SECode != null)
                    {
                        mPSOView.SlpCode = ((UserView)Session["UserLogin"]).SECode.Value;
                    }
                    TempData["ListItemsExit" + mPageKey] = null;
                    break;

            }

            Session["LocalCurrency"] = mPSOView.LocalCurrency;
            Session["SystemCurrency"] = mPSOView.SystemCurrency;
            Session["DistrRuleSAP"] = mPSOView.DistrRuleSAP;
            Session["Warehouse"] = mPSOView.Warehouse;
            Session["GLAccountSAP"] = mPSOView.GLAccountSAP;

            AppGlobal.PaymentMeans = paymentMeanManagerView.GetPaymentMeans(((UserView)Session["UserLogin"]).CompanyConnect, "");
            AppGlobal.PaymentMeanTypes = paymentMeanManagerView.GetPaymentMeanType(((UserView)Session["UserLogin"]).CompanyConnect);

            if (mPSOView.PaymentMeanOrder != null && mPSOView.PaymentMeanOrder.Count() > 0)
            {
                paymentMeanOrders = mPSOView.PaymentMeanOrder;
            }

            TempData["PaymentMeanOrder" + mPageKey] = paymentMeanOrders;

            TempData["ListCurrency" + mPageKey] = mPSOView.ListDocumentSAPCombo.ListCurrency;
            List<RatesSystem> ListRates = mPSOManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mPSOView.ListDocumentSAPCombo.ListCurrency, mPSOView.DocDate, mPSOView.LocalCurrency, mPSOView.SystemCurrency);
            TempData["Rates" + mPageKey] = ListRates;
            mPSOView.SystemCurrency = ListRates.Where(c => c.System == true).FirstOrDefault().Currency;
            mPSOView.LocalCurrency = ListRates.Where(c => c.Local == true).FirstOrDefault().Currency;
            TempData["FreightListSO" + mPageKey] = mPSOView.ListFreight;
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");

            TempData["Attachments" + mPageKey] = mPSOView.AttachmentList;

            mPSOView.ShowOpenQuantity = hasPermissionToViewOpenQuantity;
            mItemGroupList.Add(new ItemGroupSAP() { ItmsGrpCod = 0, ItmsGrpNam = "All" });
            mPSOView.ItemGroups = mItemGroupList.OrderBy(o => o.ItmsGrpCod).ToList();

            //ViewBag.DiverSoUrl = WebConfigurationManager.AppSettings["DiverSoUrl"];

            return View(mSplitOrder == true ? "_SalesOrderSplit" : "_SalesOrder", Tuple.Create(mPSOView,
                ((UserView)Session["UserLogin"]).CompanyConnect,
                crystalReportsList,
                mOITMUDFList));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(SaleOrderView model)
        {
            List<SaleOrderLineView> SOLinesTemp = (List<SaleOrderLineView>)TempData["ListItemsExit" + model.PageKey];
            //En caso de que se hayan copiado lineas de otro documento y tengan freights aca se los agrego a la orden
            List<FreightView> FreightListTemp = (List<FreightView>)TempData["FreightList" + model.PageKey];
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];
            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];
            List<Serial> serials = (List<Serial>)TempData["UsedSerials"];
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<SplitOrderResultView, SplitOrderResult>(); }).CreateMapper();

            model.PaymentMeanOrder = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + model.PageKey];
            TempData["PaymentMeanOrder" + model.PageKey] = model.PaymentMeanOrder;

            model.AttachmentList = mAttachmentList;
            if (FreightListTemp != null)
                model.ListFreight.AddRange(FreightListTemp);
            model.Lines.Select(c =>
            {
                c.Serials = serials != null ? serials.Where(d => d.DocLine == c.LineNum).ToList() : null;

                c.ItemCode = ((List<SaleOrderLineView>)SOLinesTemp)
                    .Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault();

                c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode)
                    .FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null);

                return c;
            }).ToList();

            TempData["UsedSerials"] = serials;
            TempData["FreightList" + model.PageKey] = FreightListTemp;
            TempData["ListItemsExit" + model.PageKey] = SOLinesTemp;
            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            ReplaceUTFInAddress(model);

            if (mSplitOrder && !model.SaveAsDraft)
            {
                FolioManagerView folioManagerView = new FolioManagerView();
                int folioNum = 0;

                List<short> paymentTermCodeList = model.Lines.Select(c => c.PaymentTerm).Distinct().ToList();
                var modelLinesOrig = model.Lines;
                JsonResult jsonRet = new JsonResult();
                JsonObjectResult ret = new JsonObjectResult();

                folioNum = folioManagerView.New(EnumDesc.GetDescription(ARGNS.Util.Enums.FolioType.SaleOrder));
                List<SplitOrderResultView> generatedOrderList = new List<SplitOrderResultView>();

                foreach (var method in paymentTermCodeList)
                {
                    model.Lines = modelLinesOrig.Where(c => c.PaymentTerm == method).ToList();
                    model.GroupNum = method;

                    foreach (var udf in model.MappedUdf)
                    {
                        if (udf.UDFName == "U_ARGNS_PORTAL_FOLIO")
                        {
                            udf.Value = folioNum.ToString();
                        }
                    }
                    DiverSOSalesDocProcessResult mPromotionResult = mPSOManagerView.GetPromotionItemsByOrder(model);
                    if (mPromotionResult.succeed == true)
                    {
                        foreach (DiverSOPromotions mPromotion in mPromotionResult.value.Promotions)
                        {
                            SaleOrderLineView mLineAux = new SaleOrderLineView((model.Lines.Select(c => c.LineNum).Max() + 1), WebConfigurationManager.AppSettings["PromoItem"], mPromotion.Notes, (mPromotion.DiscountTotal * -1), 1);
                            model.Lines.Add(mLineAux);
                        }
                    }

                    ret = mPSOManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, model.From, model.UrlFrom, model.QuickOrderId);

                    ret.SplitOrderFolioNumber = folioNum;

                    if (string.IsNullOrEmpty(ret.Code) == false)
                    {
                        var order = mPSOManagerView.GetSaleOrder(Convert.ToInt32(ret.Code),
                        ((UserView)Session["UserLogin"]).IdUser,
                        ((UserView)Session["UserLogin"]).CompanyConnect,
                        false);

                        generatedOrderList.Add(new SplitOrderResultView { Code = order.DocNum.ToString(), OrderType = "SO" });
                    }
                    else
                    {
                        if (ret.ServiceAnswer == "Error")
                        {
                            string mErrorAux = ret.ErrorMsg;
                            ret = mLocalDraftsManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, model.UrlFrom, "LocalDrafts");
                            generatedOrderList.Add(new SplitOrderResultView { Code = ret.Code, OrderType = "LD", ErrorMsg = mErrorAux });
                        }
                    }
                }

                ret.SplitOrderFolioNumber = folioNum;
                ret.SplitOrders = mapper.Map<List<SplitOrderResult>>(generatedOrderList);
                jsonRet = Json(ret);

                return jsonRet;
            }
            else
            {
                //if (model.SaveAsDraft)
                //{
                //    return Json(mLocalDraftsManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, model.UrlFrom, "LocalDrafts"));
                //}
                //else
                //{

                //    return Json(mPSOManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, model.From, model.UrlFrom, model.QuickOrderId));
                //}
                JsonObjectResult ret = new JsonObjectResult();
                JsonResult jsonRet = new JsonResult();

                if (model.SaveAsDraft)
                {
                    ret = mLocalDraftsManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, model.UrlFrom, "LocalDrafts");
                    jsonRet = Json(ret);
                    return jsonRet;
                }
                else
                {
                    ret = mPSOManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, model.From, model.UrlFrom, model.QuickOrderId);
                    jsonRet = Json(ret);
                    ret.AddUpdateMsg = Resources.Views.SaleOrders.SaleOrders.ADDOSO;
                    return jsonRet;
                }
            }



        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(SaleOrderView model)
        {
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];
            List<SaleOrderLineView> mListSaleOrderLineView = (List<SaleOrderLineView>)TempData["ListItemsExit" + model.PageKey];
            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];
            List<Serial> serials = (List<Serial>)TempData["UsedSerials"];

            model.AttachmentList = mAttachmentList;
            model.Lines.Select(c => { c.Serials = serials.Where(d => d.DocLine == c.LineNum).ToList(); c.ItemCode = mListSaleOrderLineView.Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["UsedSerials"] = serials;
            TempData["ListItemsExit" + model.PageKey] = mListSaleOrderLineView;
            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            //model.PaymentMeanOrder = paymentMeanOrders;
            model.PaymentMeanOrder = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + model.PageKey];
            TempData["PaymentMeanOrder" + model.PageKey] = model.PaymentMeanOrder;

            ReplaceUTFInAddress(model);

            string mResult = "";
            if (model.SaveAsDraft)
            {
                return Json(mLocalDraftsManagerView.Update(model,
                    ((UserView)Session["UserLogin"]).CompanyConnect,
                    model.UrlFrom, "LocalDrafts"));
            }
            else
            {
                mResult = mPSOManagerView.Update(model,
                    ((UserView)Session["UserLogin"]).CompanyConnect);
            }

            return Json(mResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private static void ReplaceUTFInAddress(SaleOrderView model)
        {
            try
            {
                if (model.SOAddress != null)
                {
                    model.SOAddress.StreetB = Miscellaneous.ReplaceUTF(model.SOAddress.StreetB);
                    model.SOAddress.StreetS = Miscellaneous.ReplaceUTF(model.SOAddress.StreetS);

                    model.SOAddress.CityB = Miscellaneous.ReplaceUTF(model.SOAddress.CityB);
                    model.SOAddress.CityS = Miscellaneous.ReplaceUTF(model.SOAddress.CityS);

                    model.SOAddress.CountyB = Miscellaneous.ReplaceUTF(model.SOAddress.CountyB);
                    model.SOAddress.CountyS = Miscellaneous.ReplaceUTF(model.SOAddress.CountyS);

                    model.SOAddress.BlockB = Miscellaneous.ReplaceUTF(model.SOAddress.BlockB);
                    model.SOAddress.BlockS = Miscellaneous.ReplaceUTF(model.SOAddress.BlockS);

                    model.SOAddress.GlbLocNumS = Miscellaneous.ReplaceUTF(model.SOAddress.GlbLocNumS);
                    model.SOAddress.GlbLocNumB = Miscellaneous.ReplaceUTF(model.SOAddress.GlbLocNumB);

                    model.SOAddress.BuildingB = Miscellaneous.ReplaceUTF(model.SOAddress.BuildingB);
                    model.SOAddress.BuildingS = Miscellaneous.ReplaceUTF(model.SOAddress.BuildingS);

                    model.SOAddress.StreetNoB = Miscellaneous.ReplaceUTF(model.SOAddress.StreetNoB);
                    model.SOAddress.StreetNoS = Miscellaneous.ReplaceUTF(model.SOAddress.StreetNoS);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesController -> ReplaceUTFInAddress :" + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Column, OrderColumn>();
                    cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                    cfg.CreateMap<StockModel, StockModelView>();
                }).CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = GetItems(searchViewModel.pPageKey,
                    searchViewModel.pMappedUdf, true, "",
                    searchViewModel.pItemCode, searchViewModel.pItemName,
                    searchViewModel.pCkCatalogueNum,
                    searchViewModel.pCardCode,
                    searchViewModel.pBPCatalogCode,
                    searchViewModel.pInventoryItem,
                    searchViewModel.pItemWithStock,
                    searchViewModel.pItemGroup,
                    requestModel.Start,
                    requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw,
                    mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemData"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsGrid(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pItemCode = "", string pItemData = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

            OrderColumn mOrderColumn = new OrderColumn();
            mOrderColumn.Name = "ItemCode";
            mOrderColumn.IsOrdered = true;
            mOrderColumn.SortDirection = OrderColumn.OrderDirection.Ascendant;
            pOrderColumn = mOrderColumn;

            JsonObjectResult mJsonObjectResult = GetItems(pPageKey, pMappedUdf, true, "", pItemCode,
                pItemData, false, "", "", "", "", "", pStart, pLength, pOrderColumn);

            return PartialView("_ItemsGrid", Tuple.Create(mapper.Map<List<ItemMasterView>>(
                mJsonObjectResult.ItemMasterSAPList),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), pLength));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLines"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateLinesChanged(List<SaleOrderLineView> pLines, string pPageKey)
        {
            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pPageKey];
            foreach (SaleOrderLineView Line in pLines)
            {
                SaleOrderLineView LineToUpdate = ListReturnExit.Where(c => c.LineNum == Line.LineNum).FirstOrDefault();
                LineToUpdate.Dscription = Line.Dscription;
                LineToUpdate.WhsCode = Line.WhsCode;
                LineToUpdate.OcrCode = Line.OcrCode;
                LineToUpdate.GLAccount = Line.GLAccount;
                LineToUpdate.FreeTxt = Line.FreeTxt;
                LineToUpdate.ShipDate = Line.ShipDate;
                LineToUpdate.Quantity = Line.Quantity;
                LineToUpdate.Price = Line.Price;
                LineToUpdate.PriceBefDi = Line.PriceBefDi;
                LineToUpdate.Currency = Line.Currency;
                LineToUpdate.UomCode = Line.UomCode;
                LineToUpdate.TaxCode = Line.TaxCode;
                LineToUpdate.DiscPrcnt = Line.DiscPrcnt;
                LineToUpdate.VatPrcnt = Line.VatPrcnt;
                LineToUpdate.SerialBatch = Line.SerialBatch;
                LineToUpdate.PaymentTerm = Line.PaymentTerm;
                if (LineToUpdate.Freight.Count > 0)
                {
                    LineToUpdate.Freight.FirstOrDefault().ExpnsCode = Line.Freight.FirstOrDefault().ExpnsCode;
                    LineToUpdate.Freight.FirstOrDefault().LineTotal = Line.Freight.FirstOrDefault().LineTotal;
                }
                else
                {
                    LineToUpdate.Freight = Line.Freight;
                }
                foreach (UDF_ARGNS mUDF in LineToUpdate.MappedUdf)
                {
                    UDF_ARGNS mUDFAux = Line.MappedUdf.Where(c => c.UDFName == mUDF.UDFName).FirstOrDefault();
                    mUDF.Value = mUDFAux.Value;
                }
            }
            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json("Ok");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItems"></param>
        /// <param name="pCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pPageKey"></param>
        /// <param name="pCkCatalogNum"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsForm(string[] pItems, string pCurrency, string CardCode, DateTime? pReqDate, string pPageKey, bool pCkCatalogNum = false, int? pUomEntry = null, decimal surcharge = 0, bool fromPayment = false, string ano = "")
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;

            if (Session["Warehouse"] != null)
            {
                var listWhs = ((IEnumerable<Warehouse>)(Session["Warehouse"])).ToList();

                if (listWhs.Count() > 0)
                {

                    userSetting.DftWhs = (listWhs).Where(c => c.BPLid == ((UserView)Session["UserLogin"]).CompanyConnect.IdBranchSelect).FirstOrDefault().WhsCode ?? "";
                }
            }

            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {//TODO AutomaticTest ALSO IN MKT DOCUMENTS
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<SaleOrderLineView> ListReturn = null;
            List<SaleOrderLineView> ListExistingItems = new List<SaleOrderLineView>();

            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pPageKey];

            if (fromPayment)
            {
                pItems = new string[1];
                pItems[0] = surchargeConfig;
                GetItems(pPageKey, null, true, "", surchargeConfig);

            }

            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
            {
                //Si el uomentry es distinto de null, entonces busco los items que ya existan en la orden y ademas tengan la misma unidad de medida.
                if (pUomEntry != null)
                {
                    List<SaleOrderLineView> ListExistingItemsAux = new List<SaleOrderLineView>();
                    ListExistingItems = ListReturnExit.Where(c => pItems.Contains(c.ItemCode)).ToList();

                    List<ItemMasterView> ListItemsAux = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]);
                    TempData["ListItems" + pPageKey] = ListItemsAux;

                    ListItemsAux = ListItemsAux.Where(c => pItems.Contains(c.ItemCode)).ToList();

                    foreach (ItemMasterView mItemAux in ListItemsAux)
                    {
                        UnitOfMeasure mDefaultUOMAux = new UnitOfMeasure();
                        mDefaultUOMAux = mItemAux.UnitOfMeasureList.Where(k => k.UomEntry == pUomEntry).FirstOrDefault();
                        SaleOrderLineView mLineAux = ListExistingItems.Where(c => c.ItemCode == mItemAux.ItemCode && c.UomCode == mDefaultUOMAux.UomCode).FirstOrDefault();
                        if (mLineAux != null)
                        {
                            ListExistingItemsAux.Add(mLineAux);
                        }
                    }

                    ListExistingItems = ListExistingItemsAux;
                }
                else
                {
                    //Esta lista contiene los items que ya fueron agregados a una linea del documento (solo debo sumarle +1 a la quantity)
                    ListExistingItems = ListReturnExit.Where(c => pItems.Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).ToList();
                }
                //Esta lista contiene los items que no fueron agregados a una linea del documento (debo crear una nueva linea)
                pItems = pItems.Where(j => !ListExistingItems.Select(c => c.ItemCode).ToList().Contains(j)).ToArray();
            }
            if (pItems.Length > 0)
            {
                int mLine = 0;

                if (ListReturnExit != null)
                {
                    //mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Count;

                    mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max() + 1;

                }

                List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection, false, ano);
                List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, ListItems.Select(c => c.ItemCode).ToList());

                //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
                List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "RDR1");

                List<SaleOrderLineView> ListOitm = new List<SaleOrderLineView>();
                foreach (ItemMasterView mItemAux in ListItems)
                {
                    SaleOrderLineView mLineAux = new SaleOrderLineView();
                    UnitOfMeasure mDefaultUOM = new UnitOfMeasure();
                    if (pUomEntry != null)
                    {
                        mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.UomEntry == pUomEntry).FirstOrDefault();
                    }
                    else
                    {
                        mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();
                    }

                    mLineAux.hasStock = (ItemsStock.Where(d => d.ItemCode == mItemAux.ItemCode).Count() > 0 ? true : false);
                    mLineAux.Dscription = mItemAux.ItemName;
                    mLineAux.ItemCode = mItemAux.ItemCode;
                    mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                    mLineAux.LineNum = (mLine++);
                    mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);
                    mLineAux.Quantity = 1;
                    mLineAux.ListCurrency = ListCurrency;
                    //Clone the list of udf to a new list in mLineAux.MappedUdf
                    mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                    mListUserUDF.ForEach((item) => { mLineAux.MappedUdf.Add(new UDF_ARGNS(item)); });
                    mLineAux.WhsCode = mItemAux.DfltWH;
                    mLineAux.TaxCode = mDefTAxCode.Code;
                    mLineAux.VatPrcnt = mDefTAxCode.Rate;
                    mLineAux.PriceBefDi = mLineAux.Price;
                    mLineAux.UomCode = mDefaultUOM.UomCode;
                    mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                    mLineAux.ItemPrices = mItemAux.ItemPrices;
                    mLineAux.Substitute = mItemAux.Substitute;
                    mLineAux.ManBtchNum = mItemAux.ManBtchNum;
                    mLineAux.ManSerNum = mItemAux.ManSerNum;
                    mLineAux.anoSelected = ano;

                    //Para Garantia
                    if (WebConfigurationManager.AppSettings["udfWarranty"] != string.Empty)
                    {
                        foreach (var item in mLineAux.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfWarranty"]).ToList())
                        {
                            item.Value = "N";
                        }
                    }




                    if (mItemAux.TaxCodeAR != null)
                    {
                        mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                        mLineAux.VatPrcnt = mTaxCodeList.Where(t => t.Code == mItemAux.TaxCodeAR).Single().Rate;
                    }


                    mLineAux.UomGroupName = mItemAux.UomGroupName;
                    ListOitm.Add(mLineAux);
                }


                if (pItems != null)
                { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
                else
                { ListReturn = new List<SaleOrderLineView>(); }

                if (ListReturnExit != null)
                { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }
            }
            if (ListExistingItems.Count > 0)
            {
                if (pUomEntry != null)
                {
                    //Si el uomentry es distinto de null, entonces busco los items que ya existan en la orden y ademas tengan la misma unidad de medida.
                    ListExistingItems = ListReturnExit.Where(c => ListExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode) && ListExistingItems.Select(j => j.UomCode).Contains(c.UomCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + 1; return k; }).ToList();
                }
                else
                {
                    //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                    ListExistingItems = ListReturnExit.Where(c => ListExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + 1; return k; }).ToList();
                }

                if (ListReturn != null)
                { ListReturn = ListReturn.Union(ListExistingItems).ToList(); }
                else
                { ListReturn = ListReturnExit.Union(ListExistingItems).ToList(); }
            }
            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
            TempData["Rates" + pPageKey] = ListRate;

            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

            SalesOrderItemsFormView mSalesOrderItemsFormView = new SalesOrderItemsFormView();
            mSalesOrderItemsFormView.Lines = ListReturn;
            mSalesOrderItemsFormView.DocCur = pCurrency;
            mSalesOrderItemsFormView.UsersSetting = userSetting;
            mSalesOrderItemsFormView.ReqDate = pReqDate;
            mSalesOrderItemsFormView.CkCatalogNum = pCkCatalogNum;
            mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pPageKey];
            mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;


            //Esto es para items de garantia
            foreach (var item in ListReturn.Where(c => c.isWarranty).ToList())
            {
                var percWarranty = (WebConfigurationManager.AppSettings["WarrantyPercentage"] == null ? 0 : (Convert.ToDecimal(WebConfigurationManager.AppSettings["WarrantyPercentage"]) / 100));

                var fromWarrantyItem = ListReturn.Where(c => c.ItemCode == item.fromWarrantyItem).FirstOrDefault();

                if (fromWarrantyItem != null)
                {
                    item.Price = (fromWarrantyItem.Price * fromWarrantyItem.Quantity) * percWarranty;
                    item.PriceBefDi = item.Price;
                }

            }


            if (WebConfigurationManager.AppSettings["udfanioSeleted"] != string.Empty)
            {
                foreach (var item in ListReturn.ToList())
                {
                    if (item.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfanioSeleted"]).Count() > 0)
                    {
                        item.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfanioSeleted"]).FirstOrDefault().Value = ano;
                    }
                }
            }



            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            TempData["FreightListSO" + pPageKey] = mSalesOrderItemsFormView.FreightList;
            ViewBag.CalculateRate = "Y";

            if (fromPayment)
            {

                foreach (var item in mSalesOrderItemsFormView.Lines.Where(c => c.ItemCode == surchargeConfig).ToList())
                {
                    var localSurcharge = (surcharge < 0 ? surcharge * -1 : surcharge);

                    item.PriceBefDi = localSurcharge;
                    item.Price = localSurcharge;

                    if (item.TaxCode != null)
                    {
                        item.Price = item.Price / (((item.VatPrcnt ?? 0) / 100) + 1);
                        item.PriceBefDi = item.PriceBefDi / (((item.VatPrcnt ?? 0) / 100) + 1);
                    }

                    item.Quantity = (surcharge < 0 ? -1 : 1);
                }

                //if (fromPayment && mLineAux.ItemCode == surchargeConfig)
                //{
                //    if (mItemAux.TaxCodeAR != null)
                //    {
                //        mLineAux.Price = mLineAux.Price / (mLineAux.VatPrcnt ?? 0 / 100 + 1);
                //    }
                //}



                //mSalesOrderItemsFormView.Lines.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault().PriceBefDi = (surcharge < 0 ? surcharge * -1 : surcharge);
                //mSalesOrderItemsFormView.Lines.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault().Price = (surcharge < 0 ? surcharge * -1 : surcharge);
                //mSalesOrderItemsFormView.Lines.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault().Quantity = (surcharge < 0 ? -1 : 1);
            }


            mSalesOrderItemsFormView.Lines = mSalesOrderItemsFormView.Lines.OrderBy(c => c.LineNum).ToList();


            return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTaxCodeList()
        {
            try
            {
                List<SalesTaxCodesSAP> mTaxCodeList = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
                return Json(mTaxCodeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        #region Matrix Model

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModelCode"></param>
        /// <param name="pModelName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelListSearch(AdvancedSearchStylesView searchViewModel)
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            bool permissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
                    .Where(c => c.PageInternalKey == Enums.Pages.SalesOrder.ToDescriptionString()
                    && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
                    .FirstOrDefault() != null ? true : false;

            IMapper mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<Column, OrderColumn>(); }
                ).CreateMapper();

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ModelDesc, ModelDescView>();
            }).CreateMapper();

            JsonObjectResult mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList),
                permissionToViewStock, searchViewModel.Start, searchViewModel.Length,
                searchViewModel.CodeModel, searchViewModel.NameModel);

            return PartialView("_ModelList", Tuple.Create(
             mapper.Map<List<ModelDescView>>(mJsonObjectResult.ModelDescViewList),
                 Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                 searchViewModel.Length));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pModelCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelDetail(string pPageKey,
            string pModelCode = "")
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;


            MatrixModelView mModelObj = mPDMManagerView.GetMatrixModelView(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                pModelCode,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

            GetItems(pPageKey, null, true, pModelCode, "", "");
            return PartialView("_ModelMatrixDetail", mModelObj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemsQty"></param>
        /// <param name="pItems"></param>
        /// <param name="pCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        //Verificar este metodo para implementarlo Mejor.
        [HttpPost]
        public PartialViewResult _ItemsFormMatrixModel(string pItemsQty, string[] pItems, string pCurrency, string CardCode, DateTime? pReqDate, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            ICollection<JsonObject> jsonModel = JsonConvert.DeserializeObject<ICollection<JsonObject>>(pItemsQty);
            List<SaleOrderLineView> ListReturn;
            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

            int mLine = 0;

            if (ListReturnExit != null)
            {
                mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max() + 1;
            }

            List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
            ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

            //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
            List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "RDR1");

            //List<SaleOrderLineView> ListOitm = ListItems.Where(c => pItems.Contains(c.ItemCode)).Select(c => new SaleOrderLineView { Dscription = c.ItemName, ItemCode = c.ItemCode, Currency = (c.ItemPrices.Currency ?? ""), LineNum = (mLine = mLine + 1), Price = (c.ItemPrices.Price ?? 0), Quantity = 1, ListCurrency = ListCurrency, MappedUdf = mListUserUDF, WhsCode = c.DfltWH, TaxCode = mDefTAxCode.Code, VatPrcnt = mDefTAxCode.Rate, PriceBefDi = (c.ItemPrices.Price ?? 0) }).ToList();
            List<SaleOrderLineView> ListOitm = new List<SaleOrderLineView>();
            foreach (ItemMasterView mItemAux in ListItems)
            {

                SaleOrderLineView mLineAux = new SaleOrderLineView();
                UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();
                mLineAux.Dscription = mItemAux.ItemName;
                mLineAux.ItemCode = mItemAux.ItemCode;
                mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                mLineAux.LineNum = (mLine++);
                mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);
                mLineAux.Quantity = 1;
                mLineAux.ListCurrency = ListCurrency;
                //Clone the list of udf to a new list in mLineAux.MappedUdf
                mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                mListUserUDF.ForEach((item) =>
                {
                    mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                });
                mLineAux.WhsCode = mItemAux.DfltWH;
                mLineAux.TaxCode = mDefTAxCode.Code;
                mLineAux.VatPrcnt = mDefTAxCode.Rate;
                mLineAux.PriceBefDi = mLineAux.Price;
                mLineAux.UomCode = mDefaultUOM.UomCode;
                mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                mLineAux.ItemPrices = mItemAux.ItemPrices;
                if (mItemAux.TaxCodeAR != null)
                {
                    mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                    mLineAux.VatPrcnt = mTaxCodeList.Where(t => t.Code == mItemAux.TaxCodeAR).Single().Rate;
                }
                mLineAux.UomGroupName = mItemAux.UomGroupName;

                ListOitm.Add(mLineAux);
            }

            foreach (SaleOrderLineView item in ListOitm)
            {
                foreach (var mQty in jsonModel)
                {
                    if (item.ItemCode.Contains(mQty.Code))
                    {
                        item.Quantity = Convert.ToDecimal(mQty.Qty);
                    }
                }
            }

            if (pItems != null)
            { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
            else
            { ListReturn = new List<SaleOrderLineView>(); }

            if (ListReturnExit != null)
            { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }

            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
            TempData["Rates" + pPageKey] = ListRate;
            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

            SalesOrderItemsFormView mSalesOrderItemsFormView = new SalesOrderItemsFormView();
            mSalesOrderItemsFormView.Lines = ListReturn;
            mSalesOrderItemsFormView.DocCur = pCurrency;
            mSalesOrderItemsFormView.UsersSetting = userSetting;
            mSalesOrderItemsFormView.ReqDate = pReqDate;
            mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pPageKey];
            mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            //TempData["ListItems" + pPageKey] = ListReturn;
            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            TempData["FreightListSO" + pPageKey] = mSalesOrderItemsFormView.FreightList;
            ViewBag.CalculateRate = "Y";

            //return PartialView("_ItemsFormSplit", Tuple.Create(ListReturn, pCurrency, userSetting, pReqDate));
            return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);

        }
        #endregion

        #region Catalog Model
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCatalogCode"></param>
        /// <param name="pCatalogName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelCatalogListSearch(string pCatalogCode = "", string pCatalogName = "")
        {
            List<ARGNSCatalog> ListReturn = mPDMManagerView.GetCatalogListSearch(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                pCatalogCode, pCatalogName, ((UserView)Session["UserLogin"]).SECode);
            return PartialView("_ModelCatalogList", ListReturn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCatalogCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _CatalogLines(AdvancedSearchStylesView searchViewModel)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            bool permissionToViewStock = ((UserView)Session["UserLogin"])
                    .ListUserPageAction
                    .Where(c => c.PageInternalKey == Enums.Pages.SalesOrder.ToDescriptionString()
                            && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
                    .FirstOrDefault() != null ? true : false;

            IMapper mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<Column, OrderColumn>(); }
                ).CreateMapper();

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ModelDesc, ModelDescView>();
            }).CreateMapper();


            mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList),
                permissionToViewStock, searchViewModel.Start, searchViewModel.Length,
                "", "", "", "", "", "", "", searchViewModel.CatalogCode);

            return PartialView("_ModelCatalogLines", Tuple.Create(
              mapper.Map<List<ModelDescView>>(mJsonObjectResult.ModelDescViewList),
                  Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                  searchViewModel.Length));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pModelCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelDetailFromCatalog(string pPageKey,
            string pModelCode = "")
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            bool permissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
            .Where(c => c.PageInternalKey == Enums.Pages.SalesOrder.ToDescriptionString()
            && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
            .FirstOrDefault() != null ? true : false;

            MatrixModelView mModelObj = mPDMManagerView.GetModelFromCatalog(
                ((UserView)Session["UserLogin"]).CompanyConnect, pModelCode,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), permissionToViewStock);

            ModelDescView mModelDesc = mPDMManagerView.GetModelDesc(((UserView)Session["UserLogin"]).CompanyConnect, pModelCode);
            mModelObj.ModelCode = pModelCode;
            TempData["ListSizeRun" + pPageKey] = mModelObj.SizeRunList;
            GetItems(pPageKey, null, true, pModelCode, "", "");
            return PartialView("_ModelCatalogMatrix", Tuple.Create(mModelObj, mModelDesc));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pSizeRunCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSizeRunByCode(string pPageKey, string pSizeRunCode = "")
        {
            try
            {
                List<ARGNSSizeRunView> ListSizeRun = ((List<ARGNSSizeRunView>)TempData["ListSizeRun" + pPageKey]);
                TempData["ListSizeRun" + pPageKey] = ListSizeRun;
                return Json(ListSizeRun.Where(c => c.U_SizeRunCode == pSizeRunCode).FirstOrDefault(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Customers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Customer, true, searchViewModel.pCardCode,
                    searchViewModel.pCardName, ((UserView)Session["UserLogin"]).IsSalesEmployee,
                    ((UserView)Session["UserLogin"]).SECode, ((UserView)Session["UserLogin"]).BPGroupId,
                    true, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), licTracNum: searchViewModel.LicTracNum);
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Customer, true,
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, false, null,
                    null, true, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true, searchViewModel.LicTracNum);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _ItemsComparatives([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            DiverSOResultItems mJsonObjectResult = new DiverSOResultItems();
            mJsonObjectResult = mItemMasterManagerView.GetDiverSOItems(searchViewModel.pItemCode, searchViewModel.pCardCode, DiverSOItemMethod.Comparatives);
            return Json(new DataTablesResponse(requestModel.Draw, mJsonObjectResult.value, mJsonObjectResult.TotalRecords, mJsonObjectResult.TotalRecords), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _ItemsSubstitute([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            DiverSOResultItems mJsonObjectResult = new DiverSOResultItems();
            mJsonObjectResult = mItemMasterManagerView.GetDiverSOItems(searchViewModel.pItemCode, searchViewModel.pCardCode, DiverSOItemMethod.Substitutes);
            return Json(new DataTablesResponse(requestModel.Draw, mJsonObjectResult.value, mJsonObjectResult.TotalRecords, mJsonObjectResult.TotalRecords), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _ItemsComplementaries([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            DiverSOResultItems mJsonObjectResult = new DiverSOResultItems();
            //TempData["ItemComplementary"] = searchViewModel.pItemCode;
            //TempData["ItemComplementary"] = "TestItem01";
            //searchViewModel.pItemCode = "TestItem01";
            //searchViewModel.pCardCode = "C000001";



            mJsonObjectResult = mItemMasterManagerView.GetDiverSOItems(searchViewModel.pItemCode, searchViewModel.pCardCode, DiverSOItemMethod.Complementaries);
            return Json(new DataTablesResponse(requestModel.Draw, mJsonObjectResult.value, mJsonObjectResult.TotalRecords, mJsonObjectResult.TotalRecords), JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="LocalCurrency"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBp(string Id, string LocalCurrency)
        {
            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, LocalCurrency);

            if (mBP != null)
            {
                mBP.ErrorResponse = "Ok";
                return Json(mBP, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new BusinessPartnerView { ErrorResponse = @Error.WebServError }, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocDate"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        public JsonResult UpdateRateList(DateTime DocDate, string pPageKey)
        {
            List<CurrencySAP> mListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<RatesSystem> ListRates = mPSOManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mListCurrency, DocDate, (string)Session["LocalCurrency"], (string)Session["SystemCurrency"]);
            TempData["Rates" + pPageKey] = ListRates;
            TempData["ListCurrency" + pPageKey] = mListCurrency;
            try
            {
                if (ListRates != null)
                {
                    return Json(ListRates, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteRow(string ItemCode, string pPageKey)
        {
            try
            {
                List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pPageKey];

                var itemCode = ListReturnExit.FirstOrDefault(c => c.LineNum == Convert.ToInt32(ItemCode)).ItemCode;

                ListReturnExit.RemoveAll(c => c.LineNum == Convert.ToInt32(ItemCode));

                //Esto es para items de garantia
                ListReturnExit.RemoveAll(c => c.isWarranty == true && c.fromWarrantyItem == itemCode);

                TempData["ListItemsExit" + pPageKey] = ListReturnExit;

                var tempUsedSerials = (List<Serial>)TempData["UsedSerials"];

                if (tempUsedSerials != null)
                {
                    foreach (var item in tempUsedSerials.Where(c => c.DocLine == Convert.ToInt32(ItemCode)).ToList())
                    {
                        tempUsedSerials.Remove(item);
                    }
                }

                TempData["UsedSerials"] = tempUsedSerials;

                return Json("Ok");
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pModeCode"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        private JsonObjectResult GetItems(string pPageKey, List<UDF_ARGNS> pMappedUdf, bool pOnlyActiveItems, string pModeCode = "", string pItemCode = "",
            string pItemName = "", bool pCkCatalogueNum = false, string pBPCode = "", string pBPCatalogCode = "", string pInventoryItem = "", string pItemWithStock = "",
            string pItemGroup = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {

                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                IMapper mapper = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ItemMasterSAP,
                    ItemMasterView>(); cfg.CreateMap<ItemMasterView, ItemMasterSAP>();
                    cfg.CreateMap<StockModel, StockModelView>();
                    cfg.CreateMap<StockModelView, StockModel>();
                })
                    .CreateMapper();

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView
                    .GetOITMListBy(((UserView)Session["UserLogin"]).CompanyConnect,
                    "", "Y", pInventoryItem, pItemWithStock,
                    pItemGroup, pMappedUdf, pModeCode,
                    ((UserView)Session["UserLogin"]).IdUser, pItemCode,
                    pItemName, pOnlyActiveItems,
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList),
                    pCkCatalogueNum, pBPCode, pBPCatalogCode,
                    pStart, pLength, pOrderColumn);

                List<ItemMasterView> listReturn = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);

                List<StockModelView> itemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect,
                    listReturn.Select(c => c.ItemCode).ToList());

                List<CurrencySAP> listCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                //if pLength is greatter than 0, i need to page the result, then i need to add the actual page to the previous ListItems
                if (pLength > 0)
                {
                    List<ItemMasterView> mListItemsAux = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]);
                    //If its the first time that i enter to GetItems and the TempData of ListItems is null i inizialice the variable.
                    mListItemsAux = (mListItemsAux == null ? new List<ItemMasterView>() : mListItemsAux);

                    //if the item not exist in mListItemsAux List i add it, else i do nothing
                    mListItemsAux.AddRange(listReturn.Where(
                        c => mListItemsAux.Select(x => x.ItemCode).Contains(c.ItemCode) == false)
                        .Select(c =>
                        {
                            c.ListCurrency = listCurrency;
                            c.hasStock = (itemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c;
                        })
                            .ToList());

                    TempData["ListItems" + pPageKey] = mListItemsAux;
                }
                else
                {
                    TempData["ListItems" + pPageKey] = listReturn.Select(c =>
                    {
                        c.ListCurrency = listCurrency;
                        c.hasStock = (itemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                        return c;
                    }).ToList();
                }

                //Save the ListCurrency and hasStock to ListReturn Items
                listReturn = listReturn.Select(c =>
                {
                    c.ListCurrency = listCurrency;
                    c.hasStock = (itemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                    return c;
                }).ToList();

                mJsonObjectResult.ItemMasterSAPList = mapper.Map<List<ItemMasterSAP>>(listReturn);
                TempData["ListCurrency" + pPageKey] = listCurrency;

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesOrderController -> GetOITMListBy :" + ex.Message);
                Logger.WriteError("SalesOrderController -> GetOITMListBy :" + ex.InnerException.Message);
                return new JsonObjectResult();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetListSO([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            bool hasPermissionToViewOpenQuantity = ((UserView)Session["UserLogin"]).ListUserPageAction
                            .Where(c => c.PageInternalKey == Enums.Pages.SalesOrder.ToDescriptionString()
                            && c.InternalKey == Enums.Actions.ShowOpenQuantity.ToDescriptionString())
                            .FirstOrDefault() != null ? true : false;

            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            string mVendor = searchViewModel.pCodeVendor;
            DateTime? mDate = null;
            int? mDocNum = null;
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

            if (searchViewModel.pDate != "")
            {
                mDate = Convert.ToDateTime(searchViewModel.pDate);
            }

            if (searchViewModel.pNro != "")
            {
                mDocNum = Convert.ToInt32(searchViewModel.pNro);
            }

            JsonObjectResult mJsonObjectResult = mPSOManagerView.GetSalesOrderListSearch(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                hasPermissionToViewOpenQuantity,
                mVendor, mDate, mDocNum, searchViewModel.pDocStatus,
                searchViewModel.pOwnerCode, searchViewModel.pSECode,
                requestModel.Start, requestModel.Length,
                mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            mapper = new MapperConfiguration(cfg => { cfg.CreateMap<SaleOrderSAP, SaleOrderView>(); }).CreateMapper();

            return Json(new DataTablesResponse(requestModel.Draw,
                    mapper.Map<List<SaleOrderView>>(mJsonObjectResult.SalesOrderSAPList),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                    JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetItemStock(string ItemCode = "")
        {
            List<string> mItemsCode = new List<string>();
            mItemsCode.Add(ItemCode);

            List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemStock(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                mItemsCode.ToArray());

            return PartialView("_ItemsStock", ItemsStock);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetWHwithStock(string ItemCode)
        {
            try
            {
                List<string> mItemsCode = new List<string>();

                mItemsCode.Add(ItemCode);

                List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, mItemsCode.ToArray());
                return Json(ItemsStock, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUnitOfMeasure(string ItemCode)
        {
            try
            {
                List<UnitOfMeasureView> mUOM = mItemMasterManagerView.GeUnitOfMeasureByItem(((UserView)Session["UserLogin"]).CompanyConnect, ItemCode);
                return Json(mUOM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDocEntry"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>

        [HttpPost]
        public PartialViewResult _FreightTable(int pDocEntry, string pPageKey)
        {
            List<FreightView> FreightList = new List<FreightView>();
            FreightList = mPSOManagerView.GetFreights(((UserView)Session["UserLogin"]).CompanyConnect, pDocEntry);
            List<FreightView> FreightListTemp = (List<FreightView>)TempData["FreightList" + pPageKey];
            if (FreightListTemp != null)
                FreightList.AddRange(FreightListTemp);
            return PartialView("_FreightTable", FreightList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListCopyFrom([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            string cardCode = searchViewModel.pCardCode;

            switch (searchViewModel.pDocumentType)
            {
                case "SQ":
                    mJsonObjectResult = mPSQManagerView.GetSalesQuotationListSearch(((UserView)Session["UserLogin"]).CompanyConnect,
                        cardCode, null, null, "O", "", "", requestModel.Start, requestModel.Length,
                        mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                    mapper = new MapperConfiguration(cfg => { cfg.CreateMap<SalesQuotationSAP, SaleQuotationView>(); }).CreateMapper();

                    return Json(new DataTablesResponse(requestModel.Draw,
                        mapper.Map<List<SaleQuotationView>>(mJsonObjectResult.SalesQuotationSAPList),
                        Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                        Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                        JsonRequestBehavior.AllowGet);
                case "LD":
                    mapper = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<LocalDrafts, SaleQuotationView>();
                        cfg.CreateMap<LocalDraftsLine, SaleQuotationLineView>(); cfg.CreateMap<Column, OrderColumn>();
                    }).CreateMapper();

                    mJsonObjectResult = mLocalDraftsManagerView.ListAllDrafts(((UserView)Session["UserLogin"]).CompanyConnect,
                        cardCode, null, null, "O", "", "", requestModel.Start, requestModel.Length,
                        mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                    return Json(new DataTablesResponse(requestModel.Draw,
                        mapper.Map<List<SaleQuotationView>>(mJsonObjectResult.LocalDraftsList),
                        Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                        Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                        JsonRequestBehavior.AllowGet);
                default:
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDocumentType"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetCopyFromModel(string pDocumentType)
        {
            switch (pDocumentType)
            {
                case "SQ":
                    return PartialView("_ListSQ_Model");
                case "LD":
                    return PartialView("_ListLocalDrafts");
                default:
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetItemsModel()
        {
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return PartialView("_Items", mOITMUDFList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocumentType"></param>
        /// <param name="pDocuments"></param>
        /// <param name="pCurrency"></param>
        /// <param name="pPageKey"></param>
        /// <param name="pCustomerCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ListLinesCopyFrom(string DocumentType, string[] pDocuments, string pCurrency, string pPageKey, string pCustomerCode = "")
        {
            List<RatesSystem> ListRate;
            List<CurrencySAP> ListCurrency;
            List<StockModelView> ItemsStock;
            List<UDF_ARGNS> ListUserUDF;
            List<UsersSettingView> UserSettingsList;
            UsersSettingView UserSetting;
            SalesOrderItemsFormView mSalesOrderItemsFormView = new SalesOrderItemsFormView();

            try
            {
                switch (DocumentType)
                {
                    case "SQ":
                        List<SaleQuotationLineView> linesList = mPSQManagerView.GetSalesQuotationLinesSearch(((UserView)Session["UserLogin"]).CompanyConnect, pDocuments);

                        List<SaleOrderLineView> lineList = Mapper.Map<List<SaleOrderLineView>>(linesList);

                        ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

                        ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                        ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, linesList.Select(c => c.ItemCode).ToArray());

                        double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();

                        TempData["Rates" + pPageKey] = ListRate;

                        List<FreightView> freightList = new List<FreightView>();
                        foreach (int docEntry in lineList.Select(c => c.DocEntry).Distinct().ToList())
                        {
                            freightList.AddRange(mPSQManagerView.GetFreights(((UserView)Session["UserLogin"]).CompanyConnect, docEntry));
                        }
                        if (freightList.Count > 0)
                        {
                            if (freightList.FirstOrDefault().DocEntry != 0)
                            {
                                freightList.Select(c => { c.BaseLnNum = c.LineNum; c.BaseAbsEnt = c.DocEntry; c.BaseType = 23; return c; }).ToList();
                            }
                            else
                            {
                                freightList = null;
                            }
                        }

                        TempData["FreightList" + pPageKey] = freightList;

                        //Tengo que usar este counter para poner el linenum desde 0 sta la cantidad de lineas que haya, previo a setear el baseline con el que se van a linkear las lines del documento con el linenum original
                        int counter = 0;
                        lineList = lineList.Select(c => { c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); c.Currency = pCurrency; c.RateGl = mRateGl; c.ListCurrency = ListCurrency; c.BaseType = 23; c.BaseEntry = c.DocEntry; c.BaseLine = c.LineNum; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); c.LineNum = counter; counter++; return c; }).ToList();

                        UserSettingsList = mCompaniesManagerView.GetUserSettings();
                        UserSetting = UserSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();

                        mSalesOrderItemsFormView = new SalesOrderItemsFormView();
                        mSalesOrderItemsFormView.Lines = lineList;
                        mSalesOrderItemsFormView.DocCur = pCurrency;
                        mSalesOrderItemsFormView.UsersSetting = UserSetting;
                        mSalesOrderItemsFormView.ReqDate = new DateTime?();
                        mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pPageKey];
                        mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                        //TempData["ListItems" + pPageKey] = ListReturn;
                        TempData["ListItemsExit" + pPageKey] = lineList;
                        TempData["ListCurrency" + pPageKey] = ListCurrency;
                        TempData["FreightListSO" + pPageKey] = mSalesOrderItemsFormView.FreightList;
                        ViewBag.CalculateRate = "Y";

                        return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);
                    case "LD":
                        IMapper myMapper = new MapperConfiguration(cfg => { cfg.CreateMap<SaleOrderLineView, ItemMasterView>(); }).CreateMapper();

                        linesList = mLocalDraftsManagerView.ListLineDocuments(((UserView)Session["UserLogin"]).CompanyConnect, pDocuments);

                        lineList = Mapper.Map<List<SaleOrderLineView>>(linesList);

                        ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

                        ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                        ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, linesList.Select(c => c.ItemCode).ToArray());

                        mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();

                        List<ItemMasterView> mListIMAux = mItemMasterManagerView.GeUOMByItemList(((UserView)Session["UserLogin"]).CompanyConnect,
                        myMapper.Map<List<ItemMasterView>>(lineList), "Y", "N");

                        mListIMAux = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, mListIMAux, pCustomerCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

                        counter = 0;
                        lineList = lineList.Select(
                            c =>
                            {
                                c.ListCurrency = ListCurrency;
                                c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                                c.UnitOfMeasureList = mListIMAux.Where(j => j.ItemCode == c.ItemCode).FirstOrDefault().UnitOfMeasureList;
                                c.UgpEntry = mListIMAux.Where(j => j.ItemCode == c.ItemCode).FirstOrDefault().UgpEntry;
                                c.UomCode = mListIMAux.Where(j => j.ItemCode == c.ItemCode).FirstOrDefault().UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault().UomCode;
                                c.UomGroupName = mListIMAux.Where(j => j.ItemCode == c.ItemCode).FirstOrDefault().UomGroupName;
                                c.ItemPrices = mListIMAux.Where(j => j.ItemCode == c.ItemCode).FirstOrDefault().ItemPrices;
                                c.Currency = pCurrency;
                                c.RateGl = mRateGl;
                                c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault();
                                c.LineNum = counter; counter++; return c;
                            })
                                .ToList();

                        UserSettingsList = mCompaniesManagerView.GetUserSettings();
                        UserSetting = UserSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();

                        mSalesOrderItemsFormView = new SalesOrderItemsFormView();
                        mSalesOrderItemsFormView.Lines = lineList;
                        mSalesOrderItemsFormView.DocCur = pCurrency;
                        mSalesOrderItemsFormView.UsersSetting = UserSetting;
                        mSalesOrderItemsFormView.ReqDate = new DateTime?();
                        mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pPageKey];
                        mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                        TempData["Rates" + pPageKey] = ListRate;
                        TempData["ListItemsExit" + pPageKey] = lineList;
                        TempData["ListCurrency" + pPageKey] = ListCurrency;
                        TempData["FreightListSO" + pPageKey] = mSalesOrderItemsFormView.FreightList;
                        ViewBag.CalculateRate = "Y";

                        return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);

                    case "QuickOrder":
                        IMapper mapper = new MapperConfiguration(cfg =>
                        {
                            cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                            cfg.CreateMap<StockModel, StockModelView>();
                        }).CreateMapper();

                        List<QuickOrderView> ListQuickOrder = mQuickOrderManagerView.GetQuickOrders(
                            ((UserView)Session["UserLogin"]).CompanyConnect,
                            ((UserView)Session["UserLogin"]).IdUser,
                            ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany,
                            pCustomerCode, true);

                        JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetQuickOrderListBy(((UserView)Session["UserLogin"]).CompanyConnect,
                            "N", "Y", null, "", ((UserView)Session["UserLogin"]).IdUser, null, null, pCustomerCode, -1, true, true, false,
                            "", ListQuickOrder.Select(c => c.ItemCode).ToList(), 0, 0, null);

                        List<ItemMasterView> ListItemMaster = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);

                        ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect,
                            ListQuickOrder.Select(c => c.ItemCode).ToArray());

                        //ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, ListQuickOrder.FirstOrDefault().CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);
                        ListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "RDR1");
                        UserSettingsList = mCompaniesManagerView.GetUserSettings();
                        UserSetting = UserSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();

                        List<SalesTaxCodesSAP> mTaxCodeList = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
                        SalesTaxCodesSAP mDefTAxCode;
                        if (!string.IsNullOrEmpty(UserSetting.SalesTaxCodeDef))
                        {
                            mDefTAxCode = mTaxCodeList.Where(t => t.Code == UserSetting.SalesTaxCodeDef).Single();
                        }
                        else
                        {
                            mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
                        }

                        // Controlar que existan los temporales estos 
                        ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
                        ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

                        int mLine = 0;
                        List<SaleOrderLineView> ListOitm = new List<SaleOrderLineView>();
                        foreach (ItemMasterView mItemAux in ListItemMaster)
                        {
                            SaleOrderLineView mLineAux = new SaleOrderLineView();
                            UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                            mLineAux.hasStock = (ItemsStock.Where(d => d.ItemCode == mItemAux.ItemCode).Count() > 0 ? true : false);
                            mLineAux.Dscription = mItemAux.ItemName;
                            mLineAux.ItemCode = mItemAux.ItemCode;
                            mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                            mLineAux.LineNum = (mLine++);
                            mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);
                            mLineAux.Quantity = ListQuickOrder.Where(j => j.ItemCode == mItemAux.ItemCode).FirstOrDefault().Quantity;
                            mLineAux.ListCurrency = ListCurrency;
                            //Clone the list of udf to a new list in mLineAux.MappedUdf
                            mLineAux.MappedUdf = new List<UDF_ARGNS>(ListUserUDF.Count);
                            ListUserUDF.ForEach((item) =>
                            {
                                mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                            });
                            mLineAux.WhsCode = ListQuickOrder.Where(j => j.ItemCode == mItemAux.ItemCode).FirstOrDefault().WarehouseCode;
                            mLineAux.TaxCode = mDefTAxCode.Code;
                            mLineAux.VatPrcnt = mDefTAxCode.Rate;
                            mLineAux.PriceBefDi = mLineAux.Price;
                            mLineAux.UomCode = mDefaultUOM.UomCode;
                            mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                            mLineAux.ItemPrices = mItemAux.ItemPrices;
                            if (mItemAux.TaxCodeAR != null)
                            {
                                mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                                mLineAux.VatPrcnt = mTaxCodeList.Where(t => t.Code == mItemAux.TaxCodeAR).Single().Rate;
                            }

                            ListOitm.Add(mLineAux);
                        }
                        List<SaleOrderLineView> mListReturn = Mapper.Map<List<SaleOrderLineView>>(ListOitm);
                        double RateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
                        lineList = mListReturn.Select(c => { c.RateGl = RateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

                        mSalesOrderItemsFormView = new SalesOrderItemsFormView();
                        mSalesOrderItemsFormView.Lines = lineList;
                        mSalesOrderItemsFormView.DocCur = pCurrency;
                        mSalesOrderItemsFormView.UsersSetting = UserSetting;
                        mSalesOrderItemsFormView.ReqDate = new DateTime?();
                        mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pPageKey];
                        mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                        //TempData["ListItems" + pPageKey] = ListReturn;
                        TempData["ListItemsExit" + pPageKey] = lineList;
                        TempData["ListCurrency" + pPageKey] = ListCurrency;
                        TempData["FreightListSO" + pPageKey] = mSalesOrderItemsFormView.FreightList;
                        TempData["Rates" + pPageKey] = ListRate;
                        ViewBag.CalculateRate = "Y";

                        return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);

                    default:
                        return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _GetDistributionRuleList()
        {
            try
            {
                //List<ListItem> DistributionRuleList = mUserManagerView.GetDistributionRuleList(((UserView)Session["UserLogin"]).CompanyConnect);

                List<DistrRuleSAP> mDistrRuleSAP = (List<DistrRuleSAP>)Session["DistrRuleSAP"];

                List<ListItem> DistributionRuleList = mDistrRuleSAP.Select(c => new ListItem { Value = c.OcrCode.ToString(), Text = c.OcrName }).ToList();

                return Json(DistributionRuleList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _GetGLAccountList(string pPageKey)
        {
            try
            {
                List<GLAccountSAP> mGLAccountSAP = (List<GLAccountSAP>)Session["GLAccountSAP"];

                TempData["ListGLAccount" + pPageKey] = mGLAccountSAP;
                List<ListItem> ListReturn = mGLAccountSAP.Select(c => new ListItem { Value = c.FormatCode, Text = c.AcctName }).ToList();

                return Json(ListReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pPageKey"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetHandheldItem(string pBPCurrency, string CardCode, DateTime? pReqDate, string pPageKey, string pItemCode = "")
        {
            //En el caso del 1 es BarCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 1)
            {
                ItemMasterView oItemMaster = mItemMasterManagerView.GeItemByCodeBar(((UserView)Session["UserLogin"]).CompanyConnect, "N", "Y", pItemCode);

                if (oItemMaster != null)
                {
                    GetItems(pPageKey, null, true, "", oItemMaster.ItemCode);
                    string[] pItems = new string[1];
                    pItems[0] = oItemMaster.ItemCode;
                    try
                    {
                        return _ItemsForm(pItems, pBPCurrency, CardCode, pReqDate, pPageKey, false, oItemMaster.UomEntry);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            //En el caso del 2 es QRCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 2)
            {
                QRConfigView mQrConfig = mCompaniesManagerView.GetQRConfig(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
                string[] stringSplit = pItemCode.Split(new char[] { mQrConfig.Separator.ToCharArray().FirstOrDefault() });
                if (pItemCode != "")
                {
                    try
                    {
                        return _ItemsFormQRCode(stringSplit, pBPCurrency, CardCode, pReqDate, mQrConfig, pPageKey);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pEmployeeName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
        {
            List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
            return PartialView("_ListSE", listSE);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pEmployeeCode"></param>
        /// <param name="pEmployeeName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mPSOManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItems"></param>
        /// <param name="pBPCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pReqDate"></param>
        /// <param name="mQrConfig"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        public PartialViewResult _ItemsFormQRCode(string[] pItems, string pBPCurrency, string CardCode, DateTime? pReqDate, QRConfigView mQrConfig, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<SaleOrderLineView> ListReturn = null;
            SaleOrderLineView ListExistingItem = null;
            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<ItemMasterView> ListItems = null;
            try
            {
                string pItemsAux = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];

                if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
                {
                    ListExistingItem = ListReturnExit.Where(c => c.ItemCode == pItemsAux).FirstOrDefault();
                }
                //Si esta lista es distinta de null solo debo sumarle + 1 a la quantity puesto que el item ya existe en las lineas de la orden
                if (ListExistingItem != null && ListReturnExit != null)
                {
                    //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                    ListReturnExit.Where(c => c.ItemCode == ListExistingItem.ItemCode).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + (mQrConfig.QuantityPosition != null ? Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]) : 1); return k; }).ToList();
                    ListReturn = ListReturnExit;
                }
                //Debo agregar la linea nueva para ese item
                else
                {
                    int mLine = 0;

                    if (ListReturnExit != null)
                    {
                        //mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Count;
                        mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max() + 1;

                    }

                    SaleOrderLineView mSOLine = new SaleOrderLineView();
                    mSOLine.ItemCode = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];
                    GetItems(pPageKey, null, true, "", mSOLine.ItemCode);
                    ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                    List<string> mListItemCode = new List<string>();
                    mListItemCode.Add(mSOLine.ItemCode);
                    List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, mListItemCode);
                    mSOLine.hasStock = (ItemsStock.Where(d => d.ItemCode == mSOLine.ItemCode).Count() > 0 ? true : false);

                    //Obtengo Los Precios y el Warehouse Por Defecto
                    ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

                    if (mQrConfig.CurrencyPosition != null)
                        mSOLine.Currency = pItems[(mQrConfig.CurrencyPosition ?? 0) - 1];
                    //Descripcion
                    if (mQrConfig.DscriptionPosition != null)
                        mSOLine.Dscription = pItems[(mQrConfig.DscriptionPosition ?? 0) - 1];
                    else
                        mSOLine.Dscription = ListItems.FirstOrDefault().ItemName;

                    if (mQrConfig.FreeTxtPosition != null)
                        mSOLine.FreeTxt = pItems[(mQrConfig.FreeTxtPosition ?? 0) - 1];
                    if (mQrConfig.GLAccountPosition != null)
                    {
                        mSOLine.GLAccount = new GLAccountView();
                        mSOLine.GLAccount.FormatCode = pItems[(mQrConfig.GLAccountPosition ?? 0) - 1];
                    }
                    if (mQrConfig.OcrCodePosition != null)
                        mSOLine.OcrCode = pItems[(mQrConfig.OcrCodePosition ?? 0) - 1];

                    //Price
                    if (mQrConfig.PricePosition != null)
                    {
                        mSOLine.Price = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                        mSOLine.PriceBefDi = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                    }
                    else
                    {
                        UnitOfMeasure mDefaultUOM = new UnitOfMeasure();
                        if (mQrConfig.UomCodePosition != null)
                            mDefaultUOM = ListItems.FirstOrDefault().UnitOfMeasureList.Where(k => k.UomCode == pItems[(mQrConfig.UomCodePosition ?? 0) - 1]).FirstOrDefault();
                        else
                            mDefaultUOM = ListItems.FirstOrDefault().UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();
                        mSOLine.Price = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0);
                        mSOLine.PriceBefDi = mSOLine.Price;
                        mSOLine.Currency = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                        mSOLine.ItemPrices = ListItems.FirstOrDefault().ItemPrices;
                    }

                    //Unit of Measure
                    mSOLine.UomGroupName = ListItems.FirstOrDefault().UomGroupName;
                    mSOLine.UnitOfMeasureList = ListItems.FirstOrDefault().UnitOfMeasureList;

                    //Rates
                    mSOLine.TaxCode = mDefTAxCode.Code;
                    mSOLine.VatPrcnt = mDefTAxCode.Rate;


                    //Quantity
                    if (mQrConfig.QuantityPosition != null)
                        mSOLine.Quantity = Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]);
                    else
                        mSOLine.Quantity = 1;

                    if (mQrConfig.WhsCodePosition != null)
                    {
                        mSOLine.WhsCode = pItems[(mQrConfig.WhsCodePosition ?? 0) - 1];
                    }
                    else
                    {
                        //Default Warehouse
                        mSOLine.WhsCode = ListItems.FirstOrDefault().DfltWH;
                    }

                    if (mQrConfig.UomCodePosition != null)
                        mSOLine.UomCode = pItems[(mQrConfig.UomCodePosition ?? 0) - 1];

                    if (mQrConfig.SerialPosition != null || mQrConfig.BatchPosition != null)
                    {
                        if (!string.IsNullOrEmpty(pItems[(mQrConfig.SerialPosition ?? 0) - 1]))
                            mSOLine.SerialBatch = pItems[(mQrConfig.SerialPosition ?? 0) - 1];
                        if (!string.IsNullOrEmpty(pItems[(mQrConfig.BatchPosition ?? 0) - 1]))
                            mSOLine.SerialBatch = pItems[(mQrConfig.BatchPosition ?? 0) - 1];
                    }
                    mSOLine.LineNum = (mLine++);
                    mSOLine.ListCurrency = ListCurrency;
                    if (ListReturnExit == null)
                        ListReturnExit = new List<SaleOrderLineView>();
                    ListReturnExit.Add(mSOLine);
                    ListReturn = ListReturnExit;
                }

                List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
                double mRateGl = ListRate.Where(c => c.Currency == pBPCurrency).Select(c => c.Rate).FirstOrDefault();
                TempData["Rates" + pPageKey] = ListRate;
                ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pBPCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

                SalesOrderItemsFormView mSalesOrderItemsFormView = new SalesOrderItemsFormView();
                mSalesOrderItemsFormView.Lines = ListReturn;
                mSalesOrderItemsFormView.DocCur = pBPCurrency;
                mSalesOrderItemsFormView.UsersSetting = userSetting;
                mSalesOrderItemsFormView.ReqDate = pReqDate;
                mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pPageKey];
                mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                //TempData["ListItems" + pPageKey] = ListReturn;
                TempData["ListItemsExit" + pPageKey] = ListReturn;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                TempData["FreightListSO" + pPageKey] = mSalesOrderItemsFormView.FreightList;
                ViewBag.CalculateRate = "Y";

                //return PartialView("_ItemsFormSplit", Tuple.Create(ListReturn, pBPCurrency, userSetting, pReqDate));
                return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);
            }
            catch (Exception ex)
            {
                TempData["ListItemsExit" + pPageKey] = ListReturnExit;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItems"></param>
        /// <param name="pCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pPageKey"></param>
        /// <param name="pLineToReplace"></param> Solo es Llamado Cuando se necesita Reemplazar UNA Linea
        /// 
        /// 
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsFormCopyPaste(string[] pItems, string pCurrency, string CardCode, DateTime? pReqDate, string pPageKey, int pLineToReplace = -1, string pItemsQty = null, int lineWarranty = 0)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); cfg.CreateMap<UnitOfMeasureView, UnitOfMeasure>(); }).CreateMapper();

            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPSOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            ICollection<JsonObject> jsonModel = null;
            if (!string.IsNullOrEmpty(pItemsQty))
            {
                jsonModel = JsonConvert.DeserializeObject<ICollection<JsonObject>>(pItemsQty);
            }

            if (!string.IsNullOrEmpty(userSetting.SalesTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.SalesTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            if (Session["Warehouse"] != null)
            {
                var listWhs = ((IEnumerable<Warehouse>)(Session["Warehouse"])).ToList();

                userSetting.DftWhs = (listWhs).Where(c => c.BPLid == ((UserView)Session["UserLogin"]).CompanyConnect.IdBranchSelect).FirstOrDefault().WhsCode ?? "";
            }

            List<SaleOrderLineView> listReturn = null;
            List<SaleOrderLineView> listExistingItems = new List<SaleOrderLineView>();
            List<StockModelView> itemsStock = mItemMasterManagerView.GetItemWithStock(
                ((UserView)Session["UserLogin"]).CompanyConnect, pItems.ToList());
            List<SaleOrderLineView> listReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> listCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<ItemMasterView> listItemsclipboardData = new List<ItemMasterView>();

            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            foreach (string item in pItems)
            {
                JsonObjectResult mJsonObjectResult = mItemMasterManagerView
                    .GetOITMListBy(((UserView)Session["UserLogin"]).CompanyConnect,
                    "", "Y", "", "N", "", null, "",
                    ((UserView)Session["UserLogin"]).IdUser, item, "", true,
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

                ItemMasterView mItemMasterView = mapper.Map<ItemMasterView>(
                    mJsonObjectResult.ItemMasterSAPList.Where(c => c.ItemCode == item).FirstOrDefault());

                if (mItemMasterView == null)
                {
                    mItemMasterView = new ItemMasterView() { ItemCode = item };
                }

                listItemsclipboardData.Add(mItemMasterView);

                listItemsclipboardData.Select(c =>
                {
                    c.ListCurrency = listCurrency;
                    c.hasStock = (itemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false);
                    return c;
                })
                    .ToList();


            }

            if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && listReturnExit != null)
            {
                //Esta lista contiene los items que ya fueron agregados a una linea del documento (solo debo sumarle +1 a la quantity)
                listExistingItems = listReturnExit.Where(c => pItems.Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).ToList();
                //Esta lista contiene los items que no fueron agregados a una linea del documento (debo crear una nueva linea)
                pItems = pItems.Where(j => !listExistingItems.Select(c => c.ItemCode).ToList().Contains(j)).ToArray();
            }

            //Esta variable es para Garantias
            var itemCode = TempData["ItemComplementary"];

            if (pItems.Length > 0)
            {
                int mLine = 0;

                if (listReturnExit != null)
                {
                    //mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Count;
                    mLine = listReturnExit.Count == 0 ? 0 : listReturnExit.Select(c => c.LineNum).Max() + 1;
                }

                List<ItemMasterView> listItems = ((List<ItemMasterView>)listItemsclipboardData).Where(c => pItems.Contains(c.ItemCode)).ToList();
                listItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, listItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection, false);

                //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
                List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "RDR1");

                List<SaleOrderLineView> ListOitm = new List<SaleOrderLineView>();
                foreach (ItemMasterView mItemAux in listItems)
                {
                    try
                    {
                        SaleOrderLineView mLineAux = new SaleOrderLineView();
                        mItemAux.UnitOfMeasureList = mapper.Map<List<UnitOfMeasure>>(mItemMasterManagerView.GeUnitOfMeasureByItem(((UserView)Session["UserLogin"]).CompanyConnect, mItemAux.ItemCode));
                        UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                        mLineAux.hasStock = (itemsStock.Where(d => d.ItemCode == mItemAux.ItemCode).Count() > 0 ? true : false);
                        mLineAux.Dscription = mItemAux.ItemName;
                        mLineAux.ItemCode = mItemAux.ItemCode;
                        mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                        mLineAux.LineNum = (mLine++);
                        mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);

                        if (jsonModel != null)
                        {
                            foreach (var mQty in jsonModel)
                            {
                                if (mItemAux.ItemCode.Contains(mQty.Code))
                                {
                                    mLineAux.Quantity = (mQty.Qty != "0" ? Convert.ToDecimal(mQty.Qty) : 1);
                                }
                            }
                        }
                        else
                        {
                            mLineAux.Quantity = 1;
                        }

                        mLineAux.ListCurrency = listCurrency;
                        //Clone the list of udf to a new list in mLineAux.MappedUdf
                        mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                        mListUserUDF.ForEach((item) =>
                        {
                            mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                        });
                        mLineAux.WhsCode = mItemAux.DfltWH;
                        mLineAux.TaxCode = mDefTAxCode.Code;
                        mLineAux.VatPrcnt = mDefTAxCode.Rate;
                        mLineAux.PriceBefDi = mLineAux.Price;
                        mLineAux.UomCode = mDefaultUOM.UomCode;
                        mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                        mLineAux.ItemPrices = mItemAux.ItemPrices;
                        if (mItemAux.TaxCodeAR != null)
                        {
                            mLineAux.TaxCodeAR = mItemAux.TaxCodeAR;
                            mLineAux.VatPrcnt = mTaxCodeList.Where(t => t.Code == mItemAux.TaxCodeAR).Single().Rate;
                        }
                        mLineAux.UomGroupName = mItemAux.UomGroupName;

                        if (WebConfigurationManager.AppSettings["udfWarranty"] != string.Empty)
                        {
                            if (mItemAux.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfWarranty"]).Count() > 0)
                            {
                                if (mItemAux.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfWarranty"]).FirstOrDefault().Value == "S")
                                {
                                    //var percWarranty = (WebConfigurationManager.AppSettings["WarrantyPercentage"] == null ? 0 : (Convert.ToDecimal(WebConfigurationManager.AppSettings["WarrantyPercentage"]) / 100));
                                    mLineAux.isWarranty = true;
                                    mLineAux.Dscription = mLineAux.Dscription + " - " + itemCode;
                                    mLineAux.fromWarrantyItem = itemCode.ToString();
                                    foreach (var item in mLineAux.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfWarranty"]).ToList())
                                    {
                                        item.Value = "S";
                                    }
                                }
                                else
                                {
                                    foreach (var item in mLineAux.MappedUdf.Where(c => c.UDFName == WebConfigurationManager.AppSettings["udfWarranty"]).ToList())
                                    {
                                        item.Value = "N";
                                    }
                                }
                            }
                        }



                        ListOitm.Add(mLineAux);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteError("SalesOrderController -> _ItemsFormCopyPaste" + ex.Message);
                        Logger.WriteError("SalesOrderController -> _ItemsFormCopyPaste" + ex.InnerException);
                    }

                }

                if (pItems != null)
                { listReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
                else
                { listReturn = new List<SaleOrderLineView>(); }

                //Added by Jose
                if (pLineToReplace == -1)
                {
                    if (listReturnExit != null)
                    {
                        listReturn = listReturn.Union(listReturnExit).ToList();
                    }
                }
                else
                {
                    var oNewItemReplace = listReturn.Single();
                    oNewItemReplace.LineNum = pLineToReplace;

                    var oldItemIndex = listReturnExit.FindIndex(c => c.LineNum == pLineToReplace);
                    listReturnExit[oldItemIndex] = oNewItemReplace;
                    listReturn = listReturnExit;
                }

            }
            if (listExistingItems.Count > 0)
            {
                //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                listExistingItems = listReturnExit.Where(c => listExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + (jsonModel != null ? (jsonModel.Where(c => c.Code == k.ItemCode).FirstOrDefault() != null ? Convert.ToDecimal(jsonModel.Where(c => c.Code == k.ItemCode).FirstOrDefault().Qty) : 1) : 1); return k; }).ToList();
                if (listReturn != null)
                { listReturn = listReturn.Union(listExistingItems).ToList(); }
                else
                { listReturn = listReturnExit.Union(listExistingItems).ToList(); }
            }

            List<RatesSystem> listRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
            double mRateGl = listRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
            TempData["Rates" + pPageKey] = listRate;

            listReturn = listReturn.Select(c =>
            {
                c.LineStatus = "O";
                c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency);
                c.RateGl = mRateGl;
                c.RateLine = listRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault();
                return c;
            })
                .ToList();

            //Esto es para items de garantia
            foreach (var item in listReturn.Where(c => c.isWarranty).ToList())
            {
                var percWarranty = (WebConfigurationManager.AppSettings["WarrantyPercentage"] == null ? 0 : (Convert.ToDecimal(WebConfigurationManager.AppSettings["WarrantyPercentage"]) / 100));

                var fromWarrantyItem = listReturn.Where(c => c.ItemCode == item.fromWarrantyItem).FirstOrDefault();

                item.Price = (fromWarrantyItem.Price * fromWarrantyItem.Quantity) * percWarranty;
                item.PriceBefDi = item.Price;

            }

            SalesOrderItemsFormView mSalesOrderItemsFormView = new SalesOrderItemsFormView();
            mSalesOrderItemsFormView.Lines = listReturn;
            mSalesOrderItemsFormView.DocCur = pCurrency;
            mSalesOrderItemsFormView.UsersSetting = userSetting;
            mSalesOrderItemsFormView.ReqDate = pReqDate;
            mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pPageKey];
            mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            TempData["ListItemsExit" + pPageKey] = listReturn;
            TempData["ListCurrency" + pPageKey] = listCurrency;
            TempData["FreightListSO" + pPageKey] = mSalesOrderItemsFormView.FreightList;
            ViewBag.CalculateRate = "Y";

            return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetATPByWarehouse(string pItemCode, string pWarehouse)
        {

            List<AvailableToPromiseView> mATPList = mItemMasterManagerView.GetATPByWarehouse(((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, pWarehouse);
            return PartialView("_ATPTable", mATPList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Country"></param>
        /// <param name="SelectedState"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetStateList(string Country, string SelectedState)
        {
            List<StateSAP> StateList = mBusinessPartnerManagerView.GetStateList(((UserView)Session["UserLogin"]).CompanyConnect, Country);
            return PartialView("_States", Tuple.Create(StateList, SelectedState));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLineNum"></param>
        /// <param name="pUOMCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateUOM(int pLineNum, string pUOMCode, string pPageKey)
        {
            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pPageKey];
            SaleOrderLineView mSOLine = ListReturnExit.Where(c => c.LineNum == pLineNum).FirstOrDefault();
            UnitOfMeasure mUOMAux = mSOLine.UnitOfMeasureList.Where(c => c.UomCode == pUOMCode).FirstOrDefault();
            if (mUOMAux != null)
            {
                ItemPrices mItemPriceAux = mSOLine.ItemPrices.Where(c => c.UOMCode == mUOMAux.UomEntry.ToString()).FirstOrDefault();
                mSOLine.UomCode = mUOMAux.UomCode;
                if (mItemPriceAux != null)
                {
                    mSOLine.Price = mItemPriceAux.Price.Value;
                }
                else
                {
                    if (mUOMAux.AltQty != null)
                    {
                        if (mUOMAux.AltQty.Value != 0 && mUOMAux.AltQty.Value != 1)
                        {
                            mSOLine.Price = mSOLine.ItemPrices.FirstOrDefault().Price.Value / mUOMAux.AltQty.Value;
                        }
                        else
                        {
                            mSOLine.Price = mSOLine.ItemPrices.FirstOrDefault().Price.Value * mUOMAux.BaseQty.Value;
                        }
                    }
                    else
                    {
                        mSOLine.Price = mSOLine.ItemPrices.FirstOrDefault().Price.Value * mUOMAux.BaseQty.Value;
                    }
                }
            }

            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json(mSOLine.Price);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pData"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult UploadAttachment(FormCollection pData)
        {
            string mFileName = string.Empty;
            string mPageKey = pData["pPageKey"].ToString();
            AttachmentView mAttachment = new AttachmentView();
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + mPageKey];
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;
                    AttachmentView mObj = new AttachmentView();
                    foreach (string mKey in mFiles)
                    {
                        HttpPostedFileBase mFilePost = mFiles[mKey];
                        mFileName = mFilePost.FileName;
                        string path = "~/images/" + mFileName;
                        mFileName = Server.MapPath(path);

                        if (System.IO.File.Exists(mFileName))
                        {
                            throw new Exception(SaleOrders.TFAE);
                        }

                        mObj.FileName = Path.GetFileNameWithoutExtension(mFilePost.FileName);
                        mObj.FileExt = Path.GetExtension(mFilePost.FileName).Replace(".", "");
                        mObj.srcPath = Server.MapPath("~/images");
                        mObj.Date = DateTime.Today;

                        if (mFileList.Count == 0)
                        {
                            mObj.Line = 0;
                        }
                        else
                        {
                            mObj.Line = mFileList.Max(c => c.Line) + 1;
                        }

                        mFilePost.SaveAs(mFileName);
                        mFileList.Add(mObj);
                    }

                }

                TempData["Attachments" + mPageKey] = mFileList;

                return PartialView("_FilesList", mFileList);
            }
            catch (Exception ex)
            {
                TempData["Attachments" + mPageKey] = mFileList;
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult RemovedAtt(int pId, string pPageKey)
        {
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + pPageKey];

            mFileList.RemoveAll(c => c.Line == pId);

            TempData["Attachments" + pPageKey] = mFileList;

            return PartialView("_FilesList", mFileList);


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pProjectCode"></param>
        /// <param name="pProjectName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetProjectsSAPList(string pProjectCode, string pProjectName)
        {
            return PartialView("_ModalProjectsList", mProjectSAPManagerView.GetProjectsSAPList(((UserView)Session["UserLogin"]).CompanyConnect, pProjectCode, pProjectName));
        }

        [HttpPost]
        public PartialViewResult _GetDSOCatalogs(string pCatalogCode, string pCatalogName, string pCardCode, string pSlpCode, DateTime pDate, bool pIncludeItems, string pItemCode, string pItemName, string pItemGroup)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<DiverSOCatalogValue, DSOCatalogView>(); }).CreateMapper();

            List<DSOCatalogView> mListCatalogs = mapper.Map<List<DSOCatalogView>>(mPSOManagerView.GetDSOCatalogs(pCatalogCode, pCatalogName, pCardCode, pSlpCode, pDate, pIncludeItems, pItemCode, pItemName, pItemGroup).value.Data);
            return PartialView("_DSOCatalogList", mListCatalogs);
        }

        [HttpPost]
        public PartialViewResult _GetDSOCatalogItems(string pCatalogCode, string pCatalogName, string pCardCode, string pSlpCode, DateTime pDate, bool pIncludeItems, string pItemCode, string pItemName, string pItemGroup, int pStart = 0, int pLength = 0)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterDSO, DSOCatalogItemView>(); }).CreateMapper();
            List<DSOCatalogItemView> mListCatalogs = new List<DSOCatalogItemView>();

            DiverSOCatalogResult mResult = mPSOManagerView.GetDSOCatalogs(pCatalogCode, pCatalogName, pCardCode, pSlpCode, pDate, pIncludeItems, pItemCode, pItemName, pItemGroup, pStart, pLength);
            if (mResult.value.Data.FirstOrDefault() != null)
            {
                mListCatalogs = mapper.Map<List<DSOCatalogItemView>>(mResult.value.Data.FirstOrDefault().Items);
            }
            return PartialView("_DSOCatalogItems", Tuple.Create(mListCatalogs, mResult.value.Count, pLength));
        }



        [HttpPost]
        public ActionResult _ListSerials(int docNum, int line, string whs, string itemCode, int anio)
        {
            SerialSAP serialSAP = serialManagerView.GetSerialsByDocument(((UserView)Session["UserLogin"]).CompanyConnect, docNum, line, whs, DocumentsType.SO, itemCode, ((UserView)Session["UserLogin"]).IdUser, anio);
            List<Serial> tempSerialsUsed = new List<Serial>();

            if (TempData["UsedSerials"] == null)
            {
                TempData["UsedSerials"] = serialSAP.SerialsInDocument;
            }
            else
            {
                tempSerialsUsed = (List<Serial>)TempData["UsedSerials"];
                serialSAP.SerialsInDocument = tempSerialsUsed.Where(c => c.DocLine == line).ToList();
                TempData["UsedSerials"] = tempSerialsUsed;
            }

            if (TempData["FreeSerials"] == null)
            {
                TempData["FreeSerials"] = serialSAP.SerialsSAP;
            }
            else
            {
                var tempSerialsFree = (List<Serial>)TempData["FreeSerials"];
                var arraySerials = tempSerialsUsed.Select(c => c.DistNumber).ToArray();
                serialSAP.SerialsSAP = serialSAP.SerialsSAP.Union(serialSAP.SerialsInDocument).Where(c => !arraySerials.Contains(c.DistNumber)).ToList();
                TempData["FreeSerials"] = serialSAP.SerialsSAP;
            }

            var jsonSerialsSAP = $"[{GetDataSerials(serialSAP.SerialsSAP)}]";

            var jsonSerialsInDoc = $"[{GetDataSerials(serialSAP.SerialsInDocument)}]";

            return PartialView(Tuple.Create(jsonSerialsInDoc, jsonSerialsSAP, GetColummnsSerials((serialSAP.SerialsSAP.Count == 0 ? serialSAP.SerialsInDocument : serialSAP.SerialsSAP)), line));
        }

        [HttpPost]
        public JsonResult AddSerial(int AbsEntry, string DistNumber, int line)
        {

            var removed = ((List<Serial>)TempData["FreeSerials"]).Where(c => c.AbsEntry == AbsEntry).FirstOrDefault();

            ((List<Serial>)TempData["FreeSerials"]).Remove(removed);

            ((List<Serial>)TempData["UsedSerials"]).Add(new Serial { AbsEntry = AbsEntry, DistNumber = DistNumber, DocLine = line, UDFs = removed.UDFs });

            var tempFreeSerials = (List<Serial>)TempData["FreeSerials"];

            var tempUsedSerials = (List<Serial>)TempData["UsedSerials"];

            JavaScriptSerializer jSS = new JavaScriptSerializer();

            var objetcSerialsSAP = jSS.Deserialize($"[{GetDataSerials(((List<Serial>)TempData["FreeSerials"]))}]", typeof(object));
            var jsonSerialsSAP = jSS.Serialize(objetcSerialsSAP);

            var objetcSerialInDoc = jSS.Deserialize($"[{GetDataSerials(((List<Serial>)TempData["UsedSerials"]).Where(c => c.DocLine == line).ToList())}]", typeof(object));
            var jsonSerialsInDoc = jSS.Serialize(objetcSerialInDoc); ;

            TempData["FreeSerials"] = tempFreeSerials;

            TempData["UsedSerials"] = tempUsedSerials;

            List<string> jsonReturn = new List<string>();

            jsonReturn.Add(jsonSerialsSAP);

            jsonReturn.Add(jsonSerialsInDoc);

            return Json(jsonReturn);
        }

        [HttpPost]
        public JsonResult DeleteSerial(int AbsEntry, string DistNumber)
        {
            var removed = ((List<Serial>)TempData["UsedSerials"]).Where(c => c.AbsEntry == AbsEntry).FirstOrDefault();

            ((List<Serial>)TempData["FreeSerials"]).Add(new Serial { AbsEntry = AbsEntry, DistNumber = DistNumber, UDFs = removed.UDFs });

            ((List<Serial>)TempData["UsedSerials"]).Remove(removed);

            var tempFreeSerials = (List<Serial>)TempData["FreeSerials"];

            var tempUsedSerials = (List<Serial>)TempData["UsedSerials"];

            JavaScriptSerializer jSS = new JavaScriptSerializer();

            var objetcSerialsSAP = jSS.Deserialize($"[{GetDataSerials(((List<Serial>)TempData["FreeSerials"]))}]", typeof(object));
            var jsonSerialsSAP = jSS.Serialize(objetcSerialsSAP);

            var objetcSerialInDoc = jSS.Deserialize($"[{GetDataSerials(((List<Serial>)TempData["UsedSerials"]).Where(c => c.DocLine == removed.DocLine).ToList())}]", typeof(object));
            var jsonSerialsInDoc = jSS.Serialize(objetcSerialInDoc); ;

            TempData["FreeSerials"] = tempFreeSerials;

            TempData["UsedSerials"] = tempUsedSerials;

            List<string> jsonReturn = new List<string>();

            jsonReturn.Add(jsonSerialsSAP);

            jsonReturn.Add(jsonSerialsInDoc);

            return Json(jsonReturn);
        }

        private string GetDataSerials(List<Serial> serials)
        {
            var data = "";

            foreach (var item in serials)
            {
                string udfs = "";
                if (item.UDFs != null)
                {
                    foreach (var itemUDF in item.UDFs)
                    {
                        if (udfs == "")
                        {
                            udfs = $",'{itemUDF.UDFDesc}':'{itemUDF.Value}'";
                        }
                        else
                        {
                            udfs = udfs + $",'{itemUDF.UDFDesc}':'{itemUDF.Value}'";
                        }

                    }
                }
                if (data == "")
                { data = "{" + $"'DistNumber':'{item.DistNumber}','AbsEntry':{item.AbsEntry},'DocLine':{item.DocLine} {udfs}" + "}"; }
                else
                {
                    data = data + ",{" + $"'DistNumber':'{item.DistNumber}','AbsEntry':{item.AbsEntry},'DocLine':{item.DocLine} {udfs}" + "}";
                }
            }

            return data;
        }

        private List<SerialsColumnsView> GetColummnsSerials(List<Serial> serials)
        {
            List<SerialsColumnsView> returnColumns = new List<SerialsColumnsView>();

            returnColumns.Add(new SerialsColumnsView { data = "DistNumber", title = "Serial" });
            returnColumns.Add(new SerialsColumnsView { data = "AbsEntry", title = "Id" });

            if (serials.Count() > 0)
            {
                if (serials.FirstOrDefault().UDFs != null)
                {
                    foreach (var itemUDF in serials.FirstOrDefault().UDFs)
                    {
                        returnColumns.Add(new SerialsColumnsView { data = itemUDF.UDFDesc, title = itemUDF.UDFDesc });
                    }
                }
            }

            return returnColumns;
        }

        [HttpPost]
        public PartialViewResult GetPaymentMeanByOrder(decimal Overall, string pageKey, decimal Desc = 0, decimal TotalTax = 0, decimal TotalExpns = 0, decimal TotalDoc = 0)
        {
            decimal SurchargeAfter = 0;
            decimal amount = 0;

            Overall = Math.Round(Overall, 2);

            paymentMeanOrders = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + pageKey];

            foreach (var item in paymentMeanOrders)
            {
                decimal surcharge = @AppGlobal.PaymentMeans.Where(c => c.Code == item.U_PaymentCode).FirstOrDefault().U_Surcharge ?? 0;
                SurchargeAfter = SurchargeAfter + (item.U_Amount + (item.U_Amount * (surcharge / 100)));
                amount = amount + item.U_Amount;
            }

            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pageKey];

            var lineaWithSurcharge = ListReturnExit.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault();

            if (lineaWithSurcharge != null)
            {
                decimal taxAmount = 1;

                if (lineaWithSurcharge.TaxCode != null)
                {
                    taxAmount = taxAmount + ((lineaWithSurcharge.VatPrcnt ?? 0) / 100);
                }


                TotalDoc = TotalDoc - (lineaWithSurcharge.Quantity * lineaWithSurcharge.Price * taxAmount);

                Overall = TotalDoc + TotalTax + TotalExpns;

                decimal DiscPrcnt = Desc / 100;

                Overall = Overall - (TotalDoc * DiscPrcnt);

                var rounding = amount - Overall;

                Overall = Overall + rounding;

            }

            TempData["ListItemsExit" + pageKey] = ListReturnExit;

            //Overall = Overall - diff;
            TempData["PaymentMeanOrder" + pageKey] = paymentMeanOrders;

            var fix = decimal.Truncate(Overall * 100m) / 100m;

            return PartialView("_ListPaymentMeansByOrder", Tuple.Create(paymentMeanOrders, fix));
        }

        [HttpPost]
        public PartialViewResult AddPaymentMean(PaymentMeanOrder paymentMeanOrder, decimal Overall, string pageKey)
        {
            paymentMeanOrder.U_AmountSurcharger = paymentMeanOrder.U_Amount + (paymentMeanOrder.U_Amount * (AppGlobal.PaymentMeans.Where(c => c.Code == paymentMeanOrder.U_PaymentCode).FirstOrDefault().U_Surcharge / 100));

            paymentMeanOrders = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + pageKey];

            paymentMeanOrders.Add(paymentMeanOrder);

            TempData["PaymentMeanOrder" + pageKey] = paymentMeanOrders;

            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pageKey];

            var item = ListReturnExit.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault();

            if (item != null)
            {
                ListReturnExit.Remove(item);
            }

            TempData["ListItemsExit" + pageKey] = ListReturnExit;

            return PartialView("_ListPaymentMeansByOrder", Tuple.Create(paymentMeanOrders, Overall));
        }

        [HttpPost]
        public PartialViewResult DeletePaymentMean(int id, decimal Overall, string pageKey)
        {
            paymentMeanOrders = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + pageKey];

            paymentMeanOrders.Remove(paymentMeanOrders.Where(c => c.DocEntry == id).FirstOrDefault());

            TempData["PaymentMeanOrder" + pageKey] = paymentMeanOrders;

            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pageKey];

            var item = ListReturnExit.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault();

            if (item != null)
            {
                ListReturnExit.Remove(item);
            }

            TempData["ListItemsExit" + pageKey] = ListReturnExit;

            return PartialView("_ListPaymentMeansByOrder", Tuple.Create(paymentMeanOrders, Overall));
        }

        [HttpPost]
        public JsonResult GetPaymentMean(string code)
        {
            var type = AppGlobal.PaymentMeans.Where(c => c.Code == code).FirstOrDefault();

            return Json(type);
        }

        [HttpPost]
        public JsonResult DeleteAllPaymentMeanByOrder(string pageKey)
        {
            string returnJson = "OK";

            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pageKey];

            var lineaWithSurcharge = ListReturnExit.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault();

            if (lineaWithSurcharge != null)
            {
                ListReturnExit.RemoveAll(c => c.LineNum == lineaWithSurcharge.LineNum);

                returnJson = returnJson + "-" + lineaWithSurcharge.LineNum;
            }

            TempData["ListItemsExit" + pageKey] = ListReturnExit;
            paymentMeanOrders = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + pageKey];

            if (paymentMeanOrders.Count() == 0)
            {
                returnJson = "--";
            }

            TempData["PaymentMeanOrder" + pageKey] = paymentMeanOrders;

            //paymentMeanOrders = new List<PaymentMeanOrder>();

            return Json(returnJson);
        }

        [HttpPost]
        public PartialViewResult GetPaymentMeansByType(string code)
        {
            return PartialView("_ListPaymentMeans", AppGlobal.PaymentMeans.Where(c => c.U_Type == code).ToList());
        }

        [HttpPost]
        public JsonResult ValidateSaveWithPaymentMeans(decimal Overall, string pageKey, decimal Desc = 0, decimal TotalTax = 0, decimal TotalExpns = 0, decimal TotalDoc = 0)
        {
            string returnJson = "OK";
            decimal amount = 0;

            Overall = Math.Round(Overall, 2, MidpointRounding.AwayFromZero);

            paymentMeanOrders = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + pageKey];

            foreach (var item in paymentMeanOrders)
            {
                amount = amount + item.U_Amount;
            }

            amount = decimal.Round(amount, 2, MidpointRounding.AwayFromZero);

            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pageKey];

            var lineaWithSurcharge = ListReturnExit.Where(c => c.ItemCode == surchargeConfig).FirstOrDefault();

            if (lineaWithSurcharge != null)
            {
                decimal taxAmount = 1;

                if (lineaWithSurcharge.TaxCode != null)
                {
                    taxAmount = taxAmount + ((lineaWithSurcharge.VatPrcnt ?? 0) / 100);
                }

                TotalDoc = TotalDoc - (lineaWithSurcharge.Quantity * (lineaWithSurcharge.Price * taxAmount));

                Overall = TotalDoc + TotalTax + TotalExpns;

                decimal DiscPrcnt = Desc / 100;

                Overall = Overall - (TotalDoc * DiscPrcnt);

                var rounding = amount - Overall;

                Overall = Overall + rounding;

            }

            Overall = decimal.Round(Overall, 2, MidpointRounding.AwayFromZero);

            TempData["ListItemsExit" + pageKey] = ListReturnExit;

            if ((Overall - decimal.Round(amount, 2, MidpointRounding.AwayFromZero)) != 0)
            {
                returnJson = "Fail -" + (Overall - decimal.Round(amount, 2)).ToString();
            }

            TempData["PaymentMeanOrder" + pageKey] = paymentMeanOrders;

            return Json(returnJson);
        }

        [HttpPost]
        public JsonResult UpdatePaymentMean(int index, string voucher, string pageKey)
        {
            string returnJson = "OK";

            paymentMeanOrders = (List<PaymentMeanOrder>)TempData["PaymentMeanOrder" + pageKey];

            paymentMeanOrders.Where(c => c.DocEntry == index).FirstOrDefault().U_Voucher = voucher;

            TempData["PaymentMeanOrder" + pageKey] = paymentMeanOrders;

            return Json(returnJson);
        }

        public PartialViewResult _ReloadItemsForm(string pageKey, bool ckCatalogNum = false)
        {
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();

            if (Session["Warehouse"] != null)
            {
                var listWhs = ((IEnumerable<Warehouse>)(Session["Warehouse"])).ToList();

                userSetting.DftWhs = (listWhs).Where(c => c.BPLid == ((UserView)Session["UserLogin"]).CompanyConnect.IdBranchSelect).FirstOrDefault().WhsCode ?? "";
            }

            List<SaleOrderLineView> ListReturnExit = (List<SaleOrderLineView>)TempData["ListItemsExit" + pageKey];

            //Esto es para items de garantia
            foreach (var item in ListReturnExit.Where(c => c.isWarranty).ToList())
            {
                var percWarranty = (WebConfigurationManager.AppSettings["WarrantyPercentage"] == null ? 0 : (Convert.ToDecimal(WebConfigurationManager.AppSettings["WarrantyPercentage"]) / 100));

                var fromWarrantyItem = ListReturnExit.Where(c => c.ItemCode == item.fromWarrantyItem).FirstOrDefault();

                if (fromWarrantyItem != null)
                {
                    item.Price = (fromWarrantyItem.Price * fromWarrantyItem.Quantity) * percWarranty;
                    item.PriceBefDi = item.Price;
                }

            }

            SalesOrderItemsFormView mSalesOrderItemsFormView = new SalesOrderItemsFormView();
            mSalesOrderItemsFormView.Lines = ListReturnExit;
            mSalesOrderItemsFormView.CkCatalogNum = ckCatalogNum;
            mSalesOrderItemsFormView.UsersSetting = userSetting;
            mSalesOrderItemsFormView.FreightList = (List<FreightView>)TempData["FreightListSO" + pageKey];
            mSalesOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            TempData["ListItemsExit" + pageKey] = ListReturnExit;
            TempData["FreightListSO" + pageKey] = mSalesOrderItemsFormView.FreightList;
            ViewBag.CalculateRate = "Y";

            return PartialView(mSplitOrder == true ? "_ItemsFormSplit" : "_ItemsForm", mSalesOrderItemsFormView);
        }



    }
}