﻿var ListLinesChanged = [];
var clearTable = true;
$(document).ready(function () {

    //Inicializar los controles de la pagina---------------------------------------------------
    $('.input-group.date').datepicker({});
    $("#PotentialAlert").hide();
    $("#UpdateCorrectly").hide();
    $('#txtSearchDoc').datepicker({});

    //Inicializar POPUP
    $("#LoadEmployee").hide();
    $("#LoadDocument").hide();
    $('#EmployeeModal').on('show.bs.modal', function (e) {
        $("#ModalBodyEmployee").html("");
    });
    $('#DocumentModal').on('show.bs.modal', function (e) {
        if (typeof dt === 'undefined') {
            assetListVM_ListDocument.init();
        }
        else {
            clearTable = true;
            dt.clear().draw();
        } 
    });
    //Habilitar solo las rows que estan OPEN
    EnableRowTable();

    switch (formMode) {
        case "Update":
            $("#OpprType").attr('disabled', 'disabled');
            $("#btnSearchBP").attr('disabled', 'disabled');
            break;
        case "Add":
            $("#OpprType").removeAttr('disabled');
            $("#btnSearchBP").removeAttr('disabled');
            break;
    }

    //------------------------------------------------------------------------------------------

    //Buscar Documents
    $("#SearchDocument").click(function () {
        clearTable = false;
        if (typeof dt === 'undefined') {
            assetListVM_ListDocument.init();
        }
        else {
            assetListVM_ListDocument.refresh();
        }

    });

    //Buscar Owner
    $("#SearchEmployee").click(function () {

        var mEmployeeCode;
        var mEmployeeName;

        if ($('#ckCodeEmployee').is(':checked')) {
            mEmployeeCode = $('#txtSearchEmployee').val();
        }

        if ($('#ckNameEmployee').is(':checked')) {
            mEmployeeName = $('#txtSearchEmployee').val();
        }
        $("#LoadEmployee").show('slow');
        $("#ModalBodyEmployee").show('slow');

        $.ajax({
            url: '/Sales/Opportunities/_Employee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeCode: mEmployeeCode, pEmployeeName: mEmployeeName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadEmployee").hide('slow');
            $("#ModalBodyEmployee").html(data);
        });

    });

    //Buscar BusinessPartner
    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    //Agregar Nueva Linea
    $("#btNew").click(function () {
        updateLinesChanged(false);
        $.ajax({
            url: '/Sales/Opportunities/_AddNewLine',
            contextType: 'application/html;charset=utf-8',
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#TableItemsForm").html(data);
            EnableRowTable();
            CalcularPonderado($("#ActualLine").val());
            $('.input-group.date').datepicker({});
        });

    });

    //Guardar/Actualizar las Oportunidades
    $("#btnOk").click(function () {
        var form = $("#formOpportunity");
        form.submit();
    });

    $("#formOpportunity").validate({
        rules: {
            CardCode: "required",
            MaxSumLoc: "required"
        },
        messages: {
            CardCode: "Please insert Business Partner",
            MaxSumLoc: "Please insert potential amount"
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Add();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });
});

function Update() {

    var mPotentialAmount = $("#MaxSumLoc").val();

    var mSelector;



    if (parseFloat(mPotentialAmount) <= 0) {
        $("#PotentialAlert").show("slow");
        $("#PotentialAlert").delay(1500).slideUp(1000);
        return;
    }


    $('#Loading').modal({
        backdrop: 'static'
    })
    var PredDatetxt = $("#PredDate").datepicker("getDate");
    var OpenDatetxt = $("#OpenDate").datepicker("getDate");
    var CloseDatetxt = $("#CloseDate").datepicker("getDate");

    var OpportunitiesView = {
        OpprId: $("#OpprId").val(),
        OpprType: $("#OpprType").val(),
        PredDate: (isNaN(PredDatetxt) == true ? null : (PredDatetxt.getMonth() + 1) + "/" + PredDatetxt.getDate() + "/" + PredDatetxt.getFullYear()),
        OpenDate: (isNaN(OpenDatetxt) == true ? null : (OpenDatetxt.getMonth() + 1) + "/" + OpenDatetxt.getDate() + "/" + OpenDatetxt.getFullYear()),
        CloseDate: (isNaN(CloseDatetxt) == true ? null : (CloseDatetxt.getMonth() + 1) + "/" + CloseDatetxt.getDate() + "/" + CloseDatetxt.getFullYear()),
        CardCode: $("#CardCode").val(),
        CardName: $("#CardName").val(),
        CprCode: $("#CprCode").val(),
        SlpCode: $("#SlpCode").val(),
        Owner: $('#Owner').val(),
        Name: $('#Name').val(),
        MaxSumLoc: $('#MaxSumLoc').val(),
        WtSumLoc: $('#WtSumLoc').val(),
        PrcnProf: $('#PrcnProf').val(),
        SumProfL: $('#SumProfL').val(),
        Memo: $('#Memo').val(),
        Status: $('#StatusList').val(),
        OpportunitiesLines: []
    };

    $("#TableItemsForm :input[type=hidden]").each(function () {

        var input = $(this);
        var OpenDatetxt = $("#OpenDateLine" + input.attr('id')).datepicker("getDate");
        var CloseDatetxt = $("#CloseDateLine" + input.attr('id')).datepicker("getDate");

        var mDocTypeList = [];
        var mStageList = [];
        var mSalesEmpl = [];

        //Document Type
        mSelector = "ObjTypeLine" + input.attr('id') + " option";
        $('#' + mSelector).each(function () {
            mDocTypeList.push({
                "Code": this.value,
                "Name": this.text
            });
        });

        //Stages
        mSelector = "Step_IdLine" + input.attr('id') + " option";
        $('#' + mSelector).each(function () {

            mStageList.push({
                "StepId": this.value,
                "Descript": this.text
            });
        });

        //Sales Employee
        mSelector = "SlpCodeLine" + input.attr('id') + " option";
        $('#' + mSelector).each(function () {

            mSalesEmpl.push({
                "SlpCode": this.value,
                "SlpName": this.text
            });
        });

        OpportunitiesView.OpportunitiesLines.push({

            "Line": input.attr('id'),
            "OpenDate": (isNaN(OpenDatetxt) == true ? null : (OpenDatetxt.getMonth() + 1) + "/" + OpenDatetxt.getDate() + "/" + OpenDatetxt.getFullYear()),
            "CloseDate": (isNaN(CloseDatetxt) == true ? null : (CloseDatetxt.getMonth() + 1) + "/" + CloseDatetxt.getDate() + "/" + CloseDatetxt.getFullYear()),
            "SlpCode": $("#SlpCodeLine" + input.attr('id')).val(),
            "Step_Id": $("#Step_IdLine" + input.attr('id')).val(),
            "ObjType": $("#ObjTypeLine" + input.attr('id')).val(),
            "DocNumber": $("#DocNumberLine" + input.attr('id')).val(),
            "Status": $("#StatusLine" + input.attr('id')).val(),
            "MaxSumLoc": $("#MaxSumLocLine" + input.attr('id')).val(),
            "WtSumLoc": $("#WtSumLocLine" + input.attr('id')).val(),
            "ClosePrcnt": $("#ClosePrcntLine" + input.attr('id')).val(),
            DocumentTypeList: mDocTypeList,
            SalesEmployeeList: mSalesEmpl,
            StageList: mStageList,
            ContactPersonList: []
        });
    });

    $('#Loading').modal({
        backdrop: 'static'
    })

    $.ajax({
        url: "/Sales/Opportunities/Update",
        async: false,
        type: "POST",
        data: JSON.stringify(OpportunitiesView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorMessage(data);
            }
        }
    });
}

function Add() {

    $('#Loading').modal({
        backdrop: 'static'
    })
    var PredDatetxt = $("#PredDate").datepicker("getDate");
    var OpenDatetxt = $("#OpenDate").datepicker("getDate");
    var CloseDatetxt = $("#CloseDate").datepicker("getDate");

    var OpportunitiesView = {
        OpprId: $("#OpprId").val(),
        OpprType: $("#OpprType").val(),
        PredDate: (isNaN(PredDatetxt) == true ? null : (PredDatetxt.getMonth() + 1) + "/" + PredDatetxt.getDate() + "/" + PredDatetxt.getFullYear()),
        OpenDate: (isNaN(OpenDatetxt) == true ? null : (OpenDatetxt.getMonth() + 1) + "/" + OpenDatetxt.getDate() + "/" + OpenDatetxt.getFullYear()),
        CloseDate: (isNaN(CloseDatetxt) == true ? null : (CloseDatetxt.getMonth() + 1) + "/" + CloseDatetxt.getDate() + "/" + CloseDatetxt.getFullYear()),
        CardCode: $("#CardCode").val(),
        CardName: $("#CardName").val(),
        CprCode: $("#CprCode").val(),
        SlpCode: $("#SlpCode").val(),
        Owner: $('#Owner').val(),
        Name: $('#Name').val(),
        MaxSumLoc: $('#MaxSumLoc').val(),
        WtSumLoc: $('#WtSumLoc').val(),
        PrcnProf: $('#PrcnProf').val(),
        SumProfL: $('#SumProfL').val(),
        Memo: $('#Memo').val(),
        OpportunitiesLines: []
    };

    $("#TableItemsForm :input[type=hidden]").each(function () {

        var input = $(this);
        var OpenDatetxt = $("#OpenDateLine" + input.attr('id')).datepicker("getDate");
        var CloseDatetxt = $("#CloseDateLine" + input.attr('id')).datepicker("getDate");

        OpportunitiesView.OpportunitiesLines.push({

            "Line": input.attr('id'),
            "OpenDate": (isNaN(OpenDatetxt) == true ? null : (OpenDatetxt.getMonth() + 1) + "/" + OpenDatetxt.getDate() + "/" + OpenDatetxt.getFullYear()),
            "CloseDate": (isNaN(CloseDatetxt) == true ? null : (CloseDatetxt.getMonth() + 1) + "/" + CloseDatetxt.getDate() + "/" + CloseDatetxt.getFullYear()),
            "SlpCode": $("#SlpCodeLine" + input.attr('id')).val(),
            "Step_Id": $("#Step_IdLine" + input.attr('id')).val(),
            "ObjType": $("#ObjTypeLine" + input.attr('id')).val(),
            "DocNumber": $("#DocNumberLine" + input.attr('id')).val(),
            "Status": $("#StatusLine" + input.attr('id')).val(),
            "MaxSumLoc": $("#MaxSumLocLine" + input.attr('id')).val(),
            "WtSumLoc": $("#WtSumLocLine" + input.attr('id')).val(),
            "ClosePrcnt": $("#ClosePrcntLine" + input.attr('id')).val()
        });

    });

    $('#Loading').modal({
        backdrop: 'static'
    })

    $.ajax({
        url: "/Sales/Opportunities/Add",
        async: false,
        type: "POST",
        data: JSON.stringify(OpportunitiesView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                location.href = urlRedirect;
            }
            else {
                $('#Loading').modal('hide');
                ErrorMessage(data);
            }
        }
    });
}

function CalcularPonderado(Id) {

    var mPorcentaje = $('#ClosePrcntLine' + Id).val();
    var mPotencial = 0;

    var mTotalPonderado = 0;

    if ($('#MaxSumLocLine' + Id).val() != "") {
        mPotencial = $('#MaxSumLocLine' + Id).val();
    }

    mTotalPonderado = (parseFloat(mPotencial) * parseFloat(mPorcentaje)) / 100;

    $('#WtSumLocLine' + Id).val(parseFloat(mTotalPonderado).toFixed(2));
    $('#MaxSumLoc').val(parseFloat(mPotencial).toFixed(2));
    $('#WtSumLoc').val(parseFloat(mTotalPonderado).toFixed(2));

    //Progress Bar Header    
    $('#CloPrcnt').val(parseFloat(mPorcentaje).toFixed(2));
    $('#spanprogbarheader').html(parseFloat(mPorcentaje).toFixed(2) + '%');
    $('#progbarheader').css({
        'width': parseFloat(mPorcentaje).toFixed(2) + "%"
    });

    //Progres Bar Line
    $('#spanline' + Id).html(parseFloat(mPorcentaje).toFixed(2) + '%');
    $('#progbarline' + Id).css({
        'width': parseFloat(mPorcentaje).toFixed(2) + "%"
    });


    CalculateTotalProfit();

}

function CalcularPotencial(Id) {

    var mPorcentaje = $('#ClosePrcntLine' + Id).val();
    var mPonderado = 0;
    var mTotalPotencial = 0;

    if ($('#WtSumLocLine' + Id).val() != "") {
        mPonderado = $('#WtSumLocLine' + Id).val();
    }

    mTotalPotencial = (parseFloat(mPonderado) * parseFloat(mPorcentaje)) / 100;

    $('#MaxSumLocLine' + Id).val(parseFloat(mTotalPotencial).toFixed(2));
    $('#MaxSumLoc').val(parseFloat(mTotalPotencial).toFixed(2));
    $('#WtSumLoc').val(parseFloat(mPonderado).toFixed(2));

    CalculateTotalProfit();
}

function GetPercent(Id) {

    $.ajax({
        url: "/Sales/Opportunities/GetStageById",
        contextType: 'application/html;charset=utf-8',
        async: false,
        type: "POST",
        data: { "pId": $('#Step_IdLine' + Id).val() },
        dataType: "json",
        success: function (result) {
            $('#ClosePrcntLine' + Id).val(parseFloat(result).toFixed(2))
            CalcularPonderado(Id);
        }
    });

}

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#CardCode').val(pCode);
    $('#CardName').val(mName);
    $('#VendorModal').modal('hide');


    $.ajax({
        url: "/Sales/Opportunities/GetBp",
        type: "POST",
        data: { Id: pCode },
        dataType: 'json',
        success: function (data, text) {
            if (data.ErrorResponse == "Ok") {

                $("#CprCode").empty();
                $.each(data.ListContact, function (key) {
                    $('#CprCode').append($('<option>', {
                        value: data.ListContact[key].CrtctCode,
                        text: data.ListContact[key].Name
                    }));
                });

            }
            else {
                ErrorPO(data.ErrorResponse);
            }

        }
    });
}

function SetOwner(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#Owner').val(pCode);
    $('#OwnerName').val(mName);
    $('#EmployeeModal').modal('hide');
}

function EnableRowTable() {
    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);
        var mPercent = 0;

        if ($("#StatusLine" + input.attr('id')).val() == 'O') {
            $("#btnDocSearch" + input.attr('id')).removeAttr('disabled');
            $("#btnDel" + input.attr('id')).removeAttr('disabled');
            $("#OpenDateLine" + input.attr('id')).removeAttr('disabled');
            $("#CloseDateLine" + input.attr('id')).removeAttr('disabled');
            $("#SlpCodeLine" + input.attr('id')).removeAttr('disabled');
            $("#Step_IdLine" + input.attr('id')).removeAttr('disabled');
            $("#ObjTypeLine" + input.attr('id')).removeAttr('disabled');
            $("#MaxSumLocLine" + input.attr('id')).removeAttr('disabled');
            $("#WtSumLocLine" + input.attr('id')).removeAttr('disabled');
            $("#ClosePrcntLine" + input.attr('id')).removeAttr('disabled');

            mPercent = $("#ClosePrcntLine" + input.attr('id')).val();
            //Progress Bar Header
            $('#spanprogbarheader').html(parseFloat(mPercent).toFixed(2) + '%');
            $('#progbarheader').css({
                'width': parseFloat(mPercent).toFixed(2) + "%"
            });

            $("#ActualLine").val(input.attr('id'));
            $("#CloPrcnt").val(parseFloat(mPercent).toFixed(2));
        }
    });
}

function DeleteRow(Id, pCount) {

    if (parseInt(pCount) > 0) {
        ErrorMessage("The Line cannot be deleted because it has Activities linked.");
        return;
    }
    $.ajax({
        url: '/Sales/Opportunities/_DeleteRow',
        contextType: 'application/html;charset=utf-8',
        type: 'POST',
        dataType: 'html',
        data: { pId: Id },
    }).done(function (data) {
        ListLinesChanged = jQuery.unique(ListLinesChanged);
        ListLinesChanged.splice($.inArray(Id, ListLinesChanged), 1);
        $("#TableItemsForm").html(data);
        EnableRowTable();

        CalcularPonderado($("#ActualLine").val());

    });

}

function CalcularPonderadoHeader() {


    var mPorcentaje = $('#CloPrcnt').val();
    var mPotencial = 0;
    var mTotalPonderado = 0;
    var Id = $('#ActualLine').val();
    var mGanPercent = $('#PrcnProf').val();
    var mGanTtoal = 0;

    if ($('#MaxSumLoc').val() != "") {
        mPotencial = $('#MaxSumLoc').val();
    }

    mTotalPonderado = (parseFloat(mPotencial) * parseFloat(mPorcentaje)) / 100;

    //Lineas
    $('#WtSumLocLine' + Id).val(parseFloat(mTotalPonderado).toFixed(2));
    $('#MaxSumLocLine' + Id).val(parseFloat(mPotencial).toFixed(2));

    //header
    $('#WtSumLoc').val(parseFloat(mTotalPonderado).toFixed(2));

    //Calcular Ganancias
    if (parseFloat(mGanPercent) > 0) {
        mGanTtoal = (parseFloat(mPotencial) * parseFloat(mGanPercent)) / 100;
        $('#SumProfL').val(mGanTtoal);
    }
    else { $('#SumProfL').val(0) }


}

function CalcularPotencialHeader() {


    var mPorcentaje = $('#CloPrcnt').val();
    var mPonderado = 0;
    var mTotalPotencial = 0;
    var Id = $('#ActualLine').val();
    var mGanPercent = $('#PrcnProf').val();
    var mGanTtoal = 0;

    if ($('#WtSumLoc').val() != "") {
        mPonderado = $('#WtSumLoc').val();
    }

    mTotalPotencial = (parseFloat(mPonderado) * 100) / parseFloat(mPorcentaje);

    //Lineas
    $('#WtSumLocLine' + Id).val(parseFloat(mPonderado).toFixed(2));
    $('#MaxSumLocLine' + Id).val(parseFloat(mTotalPotencial).toFixed(2));

    //header
    $('#MaxSumLoc').val(parseFloat(mTotalPotencial).toFixed(2));

    //Calcular Ganancias
    if (parseFloat(mGanPercent) > 0) {
        mGanTtoal = (parseFloat(mTotalPotencial) * parseFloat(mGanPercent)) / 100;
        $('#SumProfL').val(mGanTtoal);
    }
    else { $('#SumProfL').val(0) }


}

function CalculateTotalProfit() {

    var mPercentaje = $('#PrcnProf').val();
    var mPotential = $('#MaxSumLoc').val();
    var mTotalProfit = 0;

    //Calcular Ganancias
    if (parseFloat(mPercentaje) > 0) {
        mTotalProfit = (parseFloat(mPotential) * parseFloat(mPercentaje)) / 100;
        $('#SumProfL').val(mTotalProfit);
    }
    else { $('#SumProfL').val(0) }
}

function CalculateTotalPercent() {

    var mProfit = $('#SumProfL').val();
    var mPotential = $('#MaxSumLoc').val();
    var mTotalPercent = 0;

    //Calcular Ganancias
    if (parseFloat(mProfit) > 0) {
        mTotalPercent = (parseFloat(mProfit) * 100) / parseFloat(mPotential);
        $('#PrcnProf').val(mTotalPercent);
    }
    else { $('#PrcnProf').val(0) }


}

function ErrorMessage(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function SetDocument(pDocNum) {

    var Id = $('#ActualLine').val();

    $('#DocNumberLine' + Id).val(pDocNum);

    $('#DocumentModal').modal('hide');

}

function LineWasChanged(lineNum) {
    ListLinesChanged.push(lineNum);
}

function updateLinesChanged(sendAsync) {

    ListLinesChanged = jQuery.unique(ListLinesChanged);
    if (ListLinesChanged.length > 0) {
        var Lines = [];
        for (i = 0; i < ListLinesChanged.length; i++) {
            var mLineNum = parseInt(ListLinesChanged[i]);
            var dateOpenDate = $("#OpenDateLine" + mLineNum).datepicker("getDate");
            var dateCloseDate = $("#CloseDateLine" + mLineNum).datepicker("getDate");

            Lines.push({
                "Line": mLineNum,
                "OpenDate": (isNaN(dateOpenDate) == true ? null : (dateOpenDate.getMonth() + 1) + "/" + dateOpenDate.getDate() + "/" + dateOpenDate.getFullYear()),
                "CloseDate": (isNaN(dateCloseDate) == true ? null : (dateCloseDate.getMonth() + 1) + "/" + dateCloseDate.getDate() + "/" + dateCloseDate.getFullYear()),
                "SlpCode": $("#SlpCodeLine" + mLineNum).val(),
                "Step_Id": $("#Step_IdLine" + mLineNum).val(),
                "ClosePrcnt": $("#ClosePrcntLine" + mLineNum).val(),
                "MaxSumLoc": $("#MaxSumLocLine" + mLineNum).val(),
                "WtSumLoc": $("#WtSumLocLine" + mLineNum).val(),
                "DocNumber": $("#DocNumberLine" + mLineNum).val(),
                "ObjType": $("#ObjTypeLine" + mLineNum).val()
            });
        }

        $.ajax({
            url: '/Sales/Opportunities/_UpdateLinesChanged',
            async: sendAsync,
            type: "POST",
            data: JSON.stringify(Lines),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, jqXHR) {
                ListLinesChanged = [];
            }
        });

    }
    else {
        $("#btnChoose").prop('disabled', false);
        $("#ModelMatrixAddButton").prop('disabled', false);
    }


}

function OpenDoc(pLineId) {

    var mDocType = $("#ObjTypeLine" + pLineId).val();
    var mDocNum = $("#DocNumberLine" + pLineId).val();

    if (mDocNum == "" || mDocType == "")
    { return;}

    location.target = "_blank";

    switch (mDocType) {
        case "17":
            window.open("/Sales/SalesOrder/ActionPurchaseOrder?IdPO=" + mDocNum + "&ActionPurchaseOrder=View", '_blank');
            break;
        case "23":
            window.open("/Sales/SalesQuotation/ActionPurchaseOrder?IdPO=" + mDocNum + "&ActionPurchaseOrder=View", '_blank');
            break;
        case "13":
            window.open("/Sales/SalesInvoice/ActionSalesInvoice?IdSI=" + mDocNum + "&ActionSalesInvoice=View", '_blank');
            break;
        default:
            break;
    }

}