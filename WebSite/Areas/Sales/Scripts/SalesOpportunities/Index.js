﻿$(document).ready(function () {
    $('.input-group.date').datepicker({});

    $("#txtNameVendor").prop('disabled', true);

    $("#LoadSE").hide();
    $("#LoadEmployee").hide();

    $('#SEModal').on('show.bs.modal', function (e) {
        $("#ModalBodySE").html("");
    });
    $('#EmployeeModal').on('show.bs.modal', function (e) {
        $("#ModalBodyEmployee").html("");
    });

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    $("#SearchEmployee").click(function () {

        var mEmployeeCode;
        var mEmployeeName;

        if ($('#ckCodeEmployee').is(':checked')) {
            mEmployeeCode = $('#txtSearchEmployee').val();
        }

        if ($('#ckNameEmployee').is(':checked')) {
            mEmployeeName = $('#txtSearchEmployee').val();
        }
        $("#LoadEmployee").show('slow');
        $("#ModalBodyEmployee").show('slow');

        $.ajax({
            url: '/Sales/Opportunities/_Employee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeCode: mEmployeeCode, pEmployeeName: mEmployeeName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadEmployee").hide('slow');
            $("#ModalBodyEmployee").html(data);
        });

    });

    $("#SearchSE").click(function () {
        var mSEName;
        mSEName = $('#txtSE').val();

        $("#LoadSE").show('slow');

        $.ajax({
            url: '/Sales/Opportunities/_GetSalesEmployee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeName: mSEName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadSE").hide('slow');
            $("#ModalBodySE").html(data);


        }).fail(function () {
            alert("error");
        }).always(function () {

        });

    });

    $("#btSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var dateTxtDate = $("#txtDate").datepicker("getDate");
        $.ajax({
            url: '/Sales/Opportunities/ListOpportunities',
            contextType: 'application/html;charset=utf-8',
            data: { txtCodeVendor: $("#txtCodeVendor").val(), txtDate: (isNaN(dateTxtDate) == true ? null : ((dateTxtDate.getMonth() + 1) + "/" + dateTxtDate.getDate() + "/" + dateTxtDate.getFullYear())), pOpporName: $("#txtOpporName").val(), txtDocStatus: $("#txtDocStatus").val(), pOwnerCode: $('#txtOwnerCode').val(), pSECode: $('#SalesEmployeeNumber').val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyOpportunity").html(data);
            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');

}

function SetOwner(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtOwnerCode').val(pCode);
    $('#txtOwner').val(mName);
    $('#EmployeeModal').modal('hide');
}

function SetSE(pCode, pName) {

    $('#SalesEmployeeNumber').val(pCode);
    $('#txtSalesEmployee').val(pName);
    $('#SEModal').modal('hide');
}