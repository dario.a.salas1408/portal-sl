﻿var ListRates;
var LastDocDate;
var ListLinesChanged = [];
//var used to set the items that will be added to the actual order
var pSelectedItems = [];
//vars used to pagination in items grid
var mStartItemsGrid, mLengthItemsGrid;
var searchViewModel = {};
$(document).ready(function () {

    $('#file-1').fileinput('clear');
    $('.input-group.date').datepicker({});
    $("#txtDocNum").prop('disabled', true);
    $("#OwnerName").prop('disabled', true);
    $("#TotalExpns").prop('disabled', true);
    $("#TotalTax").prop('disabled', true);
    $('#alertRates').hide();
    $('#alertLectura').hide();
    $('#alertItems').hide();
    $("#UpdateCorrectly").hide();
    $("#LoadSearchModel").hide();

    
    var freightClicked = false;
    LastDocDate = $("#DocDate").datepicker("getDate");

    CountryBChange();
    CountrySChange();

    switch (formMode) {
        case "Update":
            SetVendor($('#txtCodeVendor').val(), $('#txtNameVendor').val(), false);
            $("#btnCopy").hide();
            break;
        case "Add":
            if ($('#txtCodeVendor').val() != "")
            { SetVendor($('#txtCodeVendor').val(), $('#txtNameVendor').val(), false); }
            break;
        case "View":
            SetVendor($('#txtCodeVendor').val(), $('#txtNameVendor').val(), false);
            $("#btnOk").hide();
            $("#btnCopy").hide();
            break;

    }
    /* Obtengo los rates y setteo el precio de las lineas (Por primera vez en el documento)*/
    updateRates();
    /*Cuando cambia el DocDate actualizo los rates para ese dia*/
    $("#DocDate").change(function () {
        if ((LastDocDate.getTime() != $("#DocDate").datepicker("getDate").getTime()) && (!(isNaN($("#DocDate").datepicker("getDate"))))) {
            LastDocDate = $("#DocDate").datepicker("getDate");
            updateRates();
        }
    });

    $("#TypeCurrency").change(function () {
        var mTypeCurrency = "";
        var mRow = 0;

        switch ($("#TypeCurrency").val()) {
            case "C":
                $("#txtCurrencySg").show();
                mTypeCurrency = $("#txtCurrencySg").val();
                break;
            case "L":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#LocalCurrency").val();
                break;
            case "S":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#SystemCurrency").val();
                break;
        }

        if (ListRates !== undefined) {
            $.grep(ListRates, function (v) {
                if (v.Currency.replace(/\s+/g, '') == mTypeCurrency && v.Local == false && v.System == false) {
                    if (v.Rate == 0) {
                        total = 0;
                        $('#alertRates').show();
                        $('#btnOk').attr("disabled", true);
                    }
                    else {
                        $("#RateCurrency").val(v.Rate);
                        $('#alertRates').hide();
                        $('#btnOk').attr("disabled", false);
                    }
                }
            });
        }
        CalculateTaxForAllLine();

    });

    //if (formMode == "Add")
    //    $('#Buyer').val("-1");

    $("#formPurchaseOrder").validate({
        rules: {
            txtCodeVendor: "required",
            DocDate: "required",
            DocDueDate: "required",
            TaxDate: "required"
        },
        messages: {
            txtCodeVendor: "Please select the Vendor",
            DocDate: "Please select the Posting Date",
            DocDueDate: "Please select the Delivery Date",
            TaxDate: "Please select the Document Date"
        },
        errorPlacement: function (error, element) {
            $('#Loading').modal('hide');
            if (element.attr("name") == "fname" || element.attr("name") == "lname") {
                error.insertAfter("#lastname");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    $("#btnOk").click(function () {
        if ($("#CheckBasketID").val() == "True" && $('#NumAtCard').val() == "") {
            //Llamar al modal del basket field
            $('#BasketIDModal').modal('show');
            //$("#txtBasketID").focus();
            setTimeout(function () {
                $("#txtBasketID").focus();
            }, 500);

        }
        else {
            $('#Loading').modal({
                backdrop: 'static',
                keyboard: true
            });
            var form = $("#formPurchaseOrder");
            form.validate();
            if (form.valid()) {
                setTimeout(function () {
                    form.submit();
                }, 5000);
            }
        }
    });

    $("#btnNewList").attr("disabled", "disabled");

    $("#btnNewDropDown").attr("disabled", "disabled");

    $("#txtCodeVendor").attr("disabled", "disabled");

    $("#txtNameVendor").attr("disabled", "disabled");

    $("#TypeCurrency").attr("disabled", "disabled");

    $("#txtCurrencySg").hide();

    $("#LoadEmployee").hide();


    //Inicio Matrix 


    $("#btnMatrix").attr("disabled", "disabled");

    $("#SearchModel").click(function () {
        mStartItemsGrid = 0;
        GetModelsList();
    });

    function GetModelsList() {
        $("#LoadSearchModel").show('slow');
        $('#Loading').modal({
            backdrop: 'static'
        });

        var mModelCode;
        var mModelName;

        if ($('#ckCodeModel').is(':checked')) {
            mModelCode = $('#txtSearchModel').val();
        }

        if ($('#ckNameModel').is(':checked')) {
            mModelName = $('#txtSearchModel').val();
        }

        searchViewModel = {};
        searchViewModel.CodeModel = mModelCode;
        searchViewModel.NameModel = mModelName;

        mLengthItemsGrid = ($("#PageQtyModelList").val() == undefined ? 10 : Number($("#PageQtyModelList").val()));
        mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

        searchViewModel.Start = mStartItemsGrid;
        searchViewModel.Length = mLengthItemsGrid;

        $("#LoadGrid").show('slow');

        $.ajax({
            url: '/Sales/SalesQuotation/_ModelListSearch',
            contextType: 'application/html;charset=utf-8',
            data: { searchViewModel },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadGrid").hide();
            $("#BodyPDMGrid").html(data);
            $("#btnChooseGrid").prop('disabled', true);

            $("#modelListSearch").html(data);
            $("#ModelMatrixAddButton").prop('disabled', true);
            $("#LoadSearchModel").hide();
            updateLinesChanged(true);

            $('#Loading').modal('hide');

            if (mStartItemsGrid <= 0) {
                $("#PreviousModelList").attr('disabled', 'disabled');
            }
            else {
                $("#PreviousModelList").removeAttr('disabled');;
            }

            if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityModelList").val()) {
                $("#NextModelList").attr('disabled', 'disabled');
            }
            else {
                $("#NextModelList").removeAttr('disabled');;
            }
        });
    };

    $("#PDMList").bind("click", function () {

        $('#products .item').addClass('list-group-item');
    });

    $("#PDMGrid").bind("click", function () {
        $(this).addClass("active").siblings().removeClass("active");
        $('#products .item').removeClass('list-group-item'); $('#products .item').addClass('grid-group-item');
    });

    $('#PDMGrid').addClass("active");

    $(".btn-group > .btn").click(function () {
        $(this).addClass("active").siblings().removeClass("active");
    });


    //Fin Matrix 

    $("#SearchItems").click(function () {

        if (typeof assetListVM_ListItems.dt === 'undefined' || assetListVM_ListItems.dt == null) {
            assetListVM_ListItems.init();
            $("#btnChoose").prop('disabled', true);
            updateLinesChanged(true);
        }
        else {
            assetListVM_ListItems.refresh();
            $("#btnChoose").prop('disabled', true);
            updateLinesChanged(true);
        }
        pSelectedItems = [];

    });

    $("#SearchItemsGrid").click(function () {

        var mListSearchUDF = $("input[id^=SearchItemGridUDF_]");
        var mListSearchUDFSelect = $("select[id^=SearchItemGridUDF_]");
        var mMappedUDFList = [];
        for (i = 0; i < mListSearchUDF.length; i++) {
            mMappedUDFList.push({
                UDFName: mListSearchUDF[i].id.substring(18),
                Value: $("#" + mListSearchUDF[i].id).val(),
            });
        }
        for (i = 0; i < mListSearchUDFSelect.length; i++) {
            mMappedUDFList.push({
                UDFName: mListSearchUDFSelect[i].id.substring(18),
                Value: $("#" + mListSearchUDFSelect[i].id).val(),
            });
        }

        searchViewModel = {};
        searchViewModel.pMappedUdf = mMappedUDFList;
        searchViewModel.pPageKey = $('#Pagekey').val();
        searchViewModel.pItemCode = $("#UDFGrid_Code").val();
        searchViewModel.pItemData = $("#UDFGrid_Name").val();
        mStartItemsGrid = 0;
        GetItemsGrid();

    });

    $("#Load").hide();
    $("#LoadGrid").hide();

    $('#myModal').on('show.bs.modal', function (e) {
        $("#ModalBody").html("");
    });
    $('#myModalGrid').on('show.bs.modal', function (e) {
        $("#ModalGridBody").html("");
    });

    $('#myModalModel').on('show.bs.modal', function (e) {
        $("#modelListSearch").html("");
    });

    $('#EmployeeModal').on('show.bs.modal', function (e) {
        $("#ModalBodyEmployee").html("");
    });

    $("#btnChoose").click(function () {
        $('#UDF_Code').val('');
        $('#UDF_Name').val('');

        var mListItems = pSelectedItems;

        if (mListItems.length == 0) {
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var mTypeCurrency = "";

        switch ($("#TypeCurrency").val()) {
            case "C":
                $("#txtCurrencySg").show();
                mTypeCurrency = $("#txtCurrencySg").val();
                break;
            case "L":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#LocalCurrency").val();
                break;
            case "S":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#SystemCurrency").val();
                break;
        }

        $.ajax({
            url: '/Sales/SalesQuotation/_ItemsForm',
            traditional: true,
            contextType: 'application/html;charset=utf-8',
            data: { pItems: mListItems, pCurrency: mTypeCurrency, CardCode: $("#txtCodeVendor").val(), pPageKey: $('#Pagekey').val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#myModal').modal('hide');
            $("#ItemsSelect").html(data).trigger('change');
            $('#Loading').modal('hide');
            pSelectedItems = [];
        });


    });

    $("#btnChooseGrid").click(function () {
        var mListItems = [];
        $('#UDFGrid_Code').val('');
        $('#UDFGrid_Name').val('');

        $("#TableItemsGrid").find("input:checked").each(function (i, ob) {
            mListItems.push($(ob).val());
        });

        if (mListItems.length == 0) {
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var mTypeCurrency = "";

        switch ($("#TypeCurrency").val()) {
            case "C":
                $("#txtCurrencySg").show();
                mTypeCurrency = $("#txtCurrencySg").val();
                break;
            case "L":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#LocalCurrency").val();
                break;
            case "S":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#SystemCurrency").val();
                break;
        }

        $.ajax({
            url: '/Sales/SalesQuotation/_ItemsForm',
            traditional: true,
            contextType: 'application/html;charset=utf-8',
            data: { pItems: mListItems, pCurrency: mTypeCurrency, CardCode: $("#txtCodeVendor").val(), pPageKey: $('#Pagekey').val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#myModalGrid').modal('hide');
            $("#ItemsSelect").html(data).trigger('change');
            $('#Loading').modal('hide');
        });


    });

    $("#btnFreightsOk").click(function () {

        var tax = Number($("#TotalTax").val());
        var freigth = Number(0);
        var counter = 0;
        $("#TableFreight :input[type=hidden]").each(function () {
            tax += Number($("#VatSum" + counter).val());
            freigth += Number($("#LineTotal" + counter).val());

            counter++;
        });
        $("#TotalTax").val(tax);
        $("#TotalExpns").val(freigth);

        $("#DiscPrcnt").change();

        $('#FreightModal').modal('hide');
    });


    $("#FreightButton").click(function () {
        if (freightClicked == false) {
            $("#LoadFreight").show('slow');
            $.ajax({
                url: '/Sales/SalesQuotation/_FreightTable',
                contextType: 'application/html;charset=utf-8',
                data: { pDocEntry: $("#DocEntry").val() },
                type: 'POST',
                dataType: 'html'
            }).done(function (data) {
                freightClicked = true;
                $("#LoadFreight").hide('slow');
                $("#ModalBodyFreight").html(data);
            });
        }
    });

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    $("#SearchEmployee").click(function () {

        var mEmployeeCode;
        var mEmployeeName;

        if ($('#ckCodeEmployee').is(':checked')) {
            mEmployeeCode = $('#txtSearchEmployee').val();
        }

        if ($('#ckNameEmployee').is(':checked')) {
            mEmployeeName = $('#txtSearchEmployee').val();
        }
        $("#LoadEmployee").show('slow');
        $("#ModalBodyEmployee").show('slow');

        $.ajax({
            url: '/Sales/SalesQuotation/_Employee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeCode: mEmployeeCode, pEmployeeName: mEmployeeName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadEmployee").hide('slow');
            $("#ModalBodyEmployee").html(data);
        });

    });

    $("#DiscPrcnt").change(function () {
        CalculateTaxwithDiscount();
        CalculateDiscount();
    });

    $("#TotalDisc").change(function () {
        CalculateTaxwithDiscount();
        CalculateDiscountTotal();
    });

    $('#BpAddressSelect').change(function () {
        setAddress();
    });

    $('#BpAddressSelectShipTo').change(function () {
        setAddressShipTo();
    });

    $("#CountryS").change(function () {
        CountrySChange();
    });

    $("#CountryB").change(function () {
        CountryBChange();
    });

    //Cleanning of the array pSelectedItems when the modal is closed
    $('#myModal').on('hide.bs.modal', function (e) {
        pSelectedItems = [];
    });

    $("#UploadAttach").click(function () {

        var fileInput = document.getElementById("file-1");
        var mAttachList = [];
        var data = new FormData();
        var mModCode = $("#Code").val();

        $("#alertAtt").hide();
        if (fileInput.files.length == 0) {
            $("#alertAtt").show();
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        if (fileInput.files.length > 0) {
            for (var i = 0; i < fileInput.files.length; i++) {
                data.append(fileInput.files[i].name, fileInput.files[i]);
            }
        }

        var mPageKey = $('#Pagekey').val();

        data.append('pPageKey', mPageKey);
        data.append('mCode', mModCode);

        $.ajax({
            type: 'POST',
            url: '/Sales/SalesQuotation/UploadAttachment',
            contentType: false,
            processData: false,
            data: data,
        }).done(function (result) {
            $('#file-1').fileinput('clear');
            $("#FilesItems").html(result);
            $('#Loading').modal('hide');
            $("#alertAtt").hide();

        }).fail(function (error) {
            var startMessageString = error.responseText.search("<title>") + 7;
            var endMessageString = error.responseText.search("</title>");
            ErrorPO(error.responseText.substring(startMessageString, endMessageString));
            $('#file-1').fileinput('clear');
            $('#Loading').modal('hide');
            $("#alertAtt").hide();
        });

    });

    $.getScript("../../Scripts/ExternalJS/CustomizeSQ.js", function (data, textStatus, jqxhr) { });
});

function SetVendor(pCode, pName, refreshAddress) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode).trigger('change');
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');

    $.ajax({
        url: "/Sales/SalesQuotation/GetBp",
        type: "POST",
        data: { Id: pCode, LocalCurrency: $('#LocalCurrency').val() },
        dataType: 'json',
        success: function (data, text) {
            if (data.ErrorResponse == "Ok") {
                $("#HandHeldField").val("");
                $("#HandHeldField").focus();
                $("#ListContact").empty();

                $.each(data.ListContact, function (key) {
                    $('#ListContact').append($('<option>', {
                        value: data.ListContact[key].CrtctCode,
                        text: data.ListContact[key].Name
                    }));
                });

                $("#VatStatus").val(data.VatStatus);
                $("#btnNewList").removeAttr("disabled");
                $("#btnNewDropDown").removeAttr("disabled");
                $("#btnMatrix").removeAttr("disabled");
                $('#RateCurrency').val(data.RateCurrency);
                if (data.Currency == "##")
                {
                    if (formMode == 'Add')
                    {
                        $('#txtCurrencySg').val($("#LocalCurrency").val());
                    }
                    $("#txtCurrencySg").removeAttr("disabled");
                }
                else {
                    $('#txtCurrencySg').val(data.Currency);
                    $("#txtCurrencySg").attr("disabled", "disabled");
                }
                $("#TypeCurrency").removeAttr("disabled");
                $("#TypeCurrency").val("C");
                $("#TypeCurrency").change();
                $("#txtCurrencySg").show();

                //Payment  
                if (refreshAddress == true) {
                    $("#PeyMethod").html("");
                    $.each(data.PaymentMethodList, function (key) {
                        $('#PeyMethod').append($('<option>', {
                            value: data.PaymentMethodList[key].PayMethCod,
                            text: data.PaymentMethodList[key].Descript
                        }));
                    });

                    $("#PaymentTerm").val(data.GroupNum);
                    $("#PeyMethod").val(data.PymCode);
                }


                addressesJS = data.Addresses;
                $("#BpAddressSelect").html("");

                if (data.BillToDef != $('#hPayTo').val() && $('#hPayTo').val() != "") {
                    data.BillToDef = $('#hPayTo').val();
                }

                if (data.ShipToDef != $('#hShipTo').val() && $('#hShipTo').val() != "") {
                    data.ShipToDef = $('#hShipTo').val();
                }

                $.grep(data.Addresses, function (v) {
                    if (v.AdresType == "B") {
                        if (v.Address == data.BillToDef)
                            $("#BpAddressSelect").append('<option value=' + v.Address.replace(/\s+/g, '') + ' selected = "selected">' + v.Address + '</option>');
                        else
                            $("#BpAddressSelect").append('<option value=' + v.Address.replace(/\s+/g, '') + '>' + v.Address + '</option>');
                    }
                });

                $("#BpAddressSelectShipTo").html("");
                $.grep(data.Addresses, function (v) {
                    if (v.AdresType == "S") {
                        if (v.Address == data.ShipToDef)
                            $("#BpAddressSelectShipTo").append('<option value=' + v.Address.replace(/\s+/g, '') + ' selected = "selected">' + v.Address + '</option>');
                        else
                            $("#BpAddressSelectShipTo").append('<option value=' + v.Address.replace(/\s+/g, '') + '>' + v.Address + '</option>');
                    }
                });

                var found_names;
                if ((data.BillToDef != null || data.BillToDef != "") && refreshAddress == true) {
                    found_names = $.grep(data.Addresses, function (v) {
                        if (v.AdresType == "B" && v.Address == data.BillToDef)
                            return v;
                    });
                    if (found_names[0] != null) {
                        $("#StreetB").val(found_names[0].Street);
                        $("#StreetNoB").val(found_names[0].StreetNo);
                        $("#BlockB").val(found_names[0].Block);
                        $("#CityB").val(found_names[0].City);
                        $("#ZipCodeB").val(found_names[0].ZipCode);
                        $("#CountyB").val(found_names[0].County);
                        $("#StateB").val(found_names[0].State);
                        $("#CountryB").val(found_names[0].Country);
                        $("#BuildingB").val(found_names[0].Building);
                        $("#GlbLocNumB").val(found_names[0].GlbLocNum);
                    }
                }


                var found_names;
                if ((data.ShipToDef != null || data.ShipToDef != "") && refreshAddress == true) {
                    found_names = $.grep(data.Addresses, function (v) {
                        if (v.AdresType == "S" && v.Address == data.ShipToDef)
                            return v;
                    });
                    if (found_names[0] != null) {
                        $("#StreetS").val(found_names[0].Street);
                        $("#StreetNoS").val(found_names[0].StreetNo);
                        $("#BlockS").val(found_names[0].Block);
                        $("#CityS").val(found_names[0].City);
                        $("#ZipCodeS").val(found_names[0].ZipCode);
                        $("#CountyS").val(found_names[0].County);
                        $("#StateS").val(found_names[0].State);
                        $("#CountryS").val(found_names[0].Country);
                        $("#BuildingS").val(found_names[0].Building);
                        $("#GlbLocNumS").val(found_names[0].GlbLocNum);
                    }
                }

                if (data.SlpCode != $('#hSlpCode').val() && $('#hSlpCode').val() != "" && $('#hSlpCode').val() != "0") {
                    data.SlpCode = $('#hSlpCode').val();
                }

                if (data.SlpCode != "") {
                    $("#Buyer").val(data.SlpCode);
                }

                CountryBChange();
                CountrySChange();
            }
            else {
                ErrorPO(data.ErrorResponse);
            }

        }
    });

}

function CalculateTotalDiscount() {
    var TotalWDisc = 0;
    if ($("#TotalDoc").val() > 0) {

        if ($("#DiscPrcnt").val() > 100)
        { $("#DiscPrcnt").val(100); }

        TotalWDisc = $("#TotalDoc").val() * ($("#DiscPrcnt").val() / 100);
        $("#TotalDisc").val(parseFloat(TotalWDisc).toFixed(2));
    } else { $("#TotalDisc").val(0) }

}

function CalculatePercentDiscount() {
    var TotalWDisc = 0;
    if ($("#TotalDoc").val() > 0) {

        TotalWDisc = ($("#TotalDisc").val() * 100) / $("#TotalDoc").val();
        $("#DiscPrcnt").val(parseFloat(TotalWDisc).toFixed(4));
    }
    else { $("#DiscPrcnt").val(0); }
}

function ChangeValue(Id, pCal) {

    var count = 0;
    var price = 0;
    var total = 0;
    var mPriceAfterDisc = 0;
    var mDiscPercent = 0;

    var mPercent = 0;
    var mGrossPrice = 0;
    var TaxAmount = 0;
    var mGrossTotal = 0;
    var mDescTotal = 0;

    if ($('#txtcount' + Id).val() != "") {
        count = $('#txtcount' + Id).val();
    }

    if ($('#txt' + Id).val() != "") {
        price = $('#txt' + Id).val();
    }

    if ($('#DiscPrcnt' + Id).val() != "") {
        mDiscPercent = $('#DiscPrcnt' + Id).val();
    }

    if ($('#VatPrcnt' + Id).html() != "") {
        mPercent = $('#VatPrcnt' + Id).html();
    }

    if ($("#DiscPrcnt").val() != "") {

        mDescTotal = $("#DiscPrcnt").val();
    }

    if (ListRates !== undefined) {
        $.grep(ListRates, function (v) {
            if (v.Currency.replace(/\s+/g, '') == $('#dp' + Id).val() && v.Local == false && v.System == false) {
                if (v.Rate == 0) {
                    total = 0;
                    $('#alertRates').show();
                    $('#btnOk').attr("disabled", true);
                }
                else {

                    //Price
                    total = ((price / $('#RateCurrency').val()) * v.Rate) * count;
                    total = parseFloat(total) - (parseFloat(total) * (parseFloat(mDiscPercent) / 100));


                    //Price After Discount
                    // mPriceAfterDisc = ((price / $('#RateCurrency').val()) * v.Rate);
                    mPriceAfterDisc = parseFloat(price) - (parseFloat(price) * (parseFloat(mDiscPercent) / 100));

                    //Gross Price
                    if (mPercent != 0) {

                        if (price != 0) {

                            //Calcular TAX
                            mGrossPrice = parseFloat(((price * mPercent)) / 100) + parseFloat(price);
                            mGrossTotal = ((roundcustomized(mGrossPrice.toFixed(5), 2) / $('#RateCurrency').val()) * v.Rate) * count;

                            //Calcular Descuento por Linea
                            mGrossPrice = parseFloat(mGrossPrice) - (parseFloat(mGrossPrice) * (parseFloat(mDiscPercent) / 100));
                            mGrossTotal = parseFloat(mGrossTotal) - (parseFloat(mGrossTotal) * (parseFloat(mDiscPercent) / 100));

                            TaxAmount = (parseFloat(total) * mPercent) / 100;

                            //Descuento por Cabecera
                            if (mDescTotal != 0) {
                                TaxAmount = parseFloat(TaxAmount) - (parseFloat(TaxAmount) * (parseFloat(mDescTotal) / 100));
                            }
                        }
                        else {

                            TaxAmount = 0;
                            mGrossPrice = total;
                            mGrossTotal = 0;
                        }
                    }
                    else {

                        //Descuento Por Linea
                        mGrossPrice = parseFloat(price) - (parseFloat(price) * (parseFloat(mDiscPercent) / 100));
                        mGrossTotal = parseFloat(total)/* - (parseFloat(total) * (parseFloat(mDiscPercent) / 100))*/;

                        if ($('#TaxCode' + Id).val() != "") {

                            TaxAmount = (total * mPercent) / 100;
                            //Descuento Total
                            if (mDescTotal != 0) {
                                TaxAmount = parseFloat(TaxAmount) - (parseFloat(TaxAmount) * (parseFloat(mDescTotal) / 100));
                            }
                        }
                        else {
                            TaxAmount = 0;
                        }
                    }

                    $('#alertRates').hide();
                    $('#btnOk').attr("disabled", false);
                }
            }
        });
    }

    $("#td" + Id).html("");
    $("#td" + Id).append(total.toFixed(2));

    if ($("#VatStatus").val() == "N") {
        TaxAmount = 0;
    }

    //Tax Code Values 
    $("#PriceAfVAT" + Id).html(mGrossPrice.toFixed(2));
    $("#VatSum" + Id).html(TaxAmount.toFixed(2));
    $("#GTotal" + Id).html(mGrossTotal.toFixed(2));
    $("#Price" + Id).html(mPriceAfterDisc.toFixed(2));

    if (pCal === true) {
        CalculatedTotal();
    }

}

function CalculatedTotal() {
    var TotalDoc = 0;
    var mTotalTax = 0;
    var mTax = 0;
    var value = 0;
    var TotalDocDesc = 0;

    // Calculo el Total del Documento y Total Tax -------------------------------------
    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);

        value = parseFloat($("#td" + input.attr('id')).html());
        mTax = parseFloat($("#VatSum" + input.attr('id')).html());

        TotalDoc = parseFloat(TotalDoc) + parseFloat(value);
        mTotalTax = parseFloat(mTotalTax) + parseFloat(mTax);

    });
    $("#TotalTax").val(parseFloat(mTotalTax).toFixed(2));
    $("#TotalDoc").val(parseFloat(TotalDoc).toFixed(2));
    //-------------------------------------------------------------------------------------

    //Calculo el Descuento
    CalculateTotalDiscount();

    //Calculo el Total Con Descuentos  
    TotalDocDesc = Number($("#TotalDoc").val()) - Number($("#TotalDisc").val()) + Number($("#TotalExpns").val()) + parseFloat($("#TotalTax").val());
    $("#TotalDocPD").val(TotalDocDesc.toFixed(2));
    $("#TotalDocPD").change();
}

function roundcustomized(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

function Save() {
    var mTypeCurrency = "";
    var mTotal = (Number($("#TotalDocPD").val()));
    var mTotalRow = 0;

    switch ($("#TypeCurrency").val()) {
        case "C":
            $("#txtCurrencySg").show();
            mTypeCurrency = $("#txtCurrencySg").val();
            break;
        case "L":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#LocalCurrency").val();
            break;
        case "S":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#SystemCurrency").val();
            break;
    }


    var mDocumentAddress = {
        DocEntry: $("#DocEntry").val(),
        StreetS: $("#StreetS").val(),
        StreetB: $("#StreetB").val(),
        StreetNoS: $("#StreetNoS").val(),
        StreetNoB: $("#StreetNoB").val(),
        BlockS: $("#BlockS").val(),
        BlockB: $("#BlockB").val(),
        CityS: $("#CityS").val(),
        CityB: $("#CityB").val(),
        ZipCodeS: $("#ZipCodeS").val(),
        ZipCodeB: $("#ZipCodeB").val(),
        CountyS: $("#CountyS").val(),
        CountyB: $("#CountyB").val(),
        StateS: $("#StateS").val(),
        StateB: $("#StateB").val(),
        CountryS: $("#CountryS").val(),
        CountryB: $("#CountryB").val(),
        BuildingS: $("#BuildingS").val(),
        BuildingB: $("#BuildingB").val(),
        GlbLocNumS: $("#GlbLocNumS").val(),
        GlbLocNumB: $("#GlbLocNumB").val()

    }
    var dateDocDate = $("#DocDate").datepicker("getDate");
    var dateDocDueDate = $("#DocDueDate").datepicker("getDate");
    var dateTaxDate = $("#TaxDate").datepicker("getDate");

    var PurchaseOrderView = {
        DocDate: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
        DocDueDate: (isNaN(dateDocDueDate) == true ? null : (dateDocDueDate.getMonth() + 1) + "/" + dateDocDueDate.getDate() + "/" + dateDocDueDate.getFullYear()),
        TaxDate: (isNaN(dateTaxDate) == true ? null : (dateTaxDate.getMonth() + 1) + "/" + dateTaxDate.getDate() + "/" + dateTaxDate.getFullYear()),
        CardCode: $("#txtCodeVendor").val(),
        DocCur: mTypeCurrency,
        DocTotal: mTotal,
        CardName: $("#txtNameVendor").val(),
        DiscPrcnt: $("#DiscPrcnt").val(),
        CntctCode: $('#ListContact').val(),
        CurSource: $('#CurSource').val(),
        SlpCode: $('#Buyer').val(),
        TrnspCode: $('#ShipType').val(),
        NumAtCard: ($('#NumAtCard').val() != "" ? $('#NumAtCard').val() : $("#txtBasketID").val()),
        OwnerCode: $('#OwnerCode').val(),
        Comments: $('#Comments').val(),
        PageKey: $('#Pagekey').val(),
        ShipToCode: $("#BpAddressSelectShipTo option[value='" + $("#BpAddressSelectShipTo").val() + "']").text(),
        PayToCode: $("#BpAddressSelect option[value='" + $("#BpAddressSelect").val() + "']").text(),
        SQAddress: mDocumentAddress,
        GroupNum: $("#PaymentTerm").val(),
        PeyMethod: $("#PeyMethod").val(),
        ListItem: [],
        Lines: [],
        ListFreight: [],
        MappedUdf: []
    };

    var listUDFHEAD = $("input[id^=HEADUDF_]");
    var listUDFHEADSelect = $("select[id^=HEADUDF_]");
    for (i = 0; i < listUDFHEAD.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEAD[i].id.substring(8),
            Value: $("#" + listUDFHEAD[i].id).val(),
        });
    }
    for (i = 0; i < listUDFHEADSelect.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEADSelect[i].id.substring(8),
            Value: $("#" + listUDFHEADSelect[i].id).val(),
        });
    }

    //Obtengo los names de las lineas los meto en un array y luego hago un distinct para evitar los repetidos
    var listUDFLines = $("input[id^=LINEUDF_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(8))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(8))
    }
    linesUDFNames = unique(linesUDFNames);

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        var price = $("#txt" + input.attr('id')).val();
        var quantity = $("#txtcount" + input.attr('id')).val();
        var UomCode = $("#UOMAuto" + input.attr('id')).val();
        var whCode = "";

        if ($("#AutoComplete" + input.attr('id')).val() != "") {
            whCode = $("#AutoComplete" + input.attr('id')).val();
        }
        else {
            whCode = null;
        }

        var DistRule = "";

        if ($("#AutoCompleteDR" + input.attr('id')).val() != "") {
            DistRule = $("#AutoCompleteDR" + input.attr('id')).val();
        }
        else {
            DistRule = null;
        }

        var DistRule2 = "";

        if ($("#AutoCompleteDR_2" + input.attr('id')).val() != "") {
            DistRule2 = $("#AutoCompleteDR_2" + input.attr('id')).val();
        }
        else {
            DistRule2 = null;
        }

        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }
        var dateDelDate = $("#DelDate" + input.attr('id')).datepicker("getDate");
        PurchaseOrderView.Lines.push({
            "ItemCode": input.attr('id'),
            "Dscription": $("#DescItem" + input.attr('id')).val(),
            "Quantity": parseFloat(quantity),
            "Price": parseFloat(price),
            "PriceBefDi": parseFloat(price),
            "Currency": $("#dp" + input.attr('id')).val(),
            "LineNum": input.attr('id'),
            "WhsCode": whCode,
            "UomCode": UomCode,
            "OcrCode": DistRule,
            "OcrCode2": DistRule2,
            "FreeTxt": $("#FreeTxt" + input.attr('id')).val(),
            "GLAccount": { FormatCode: $("#AutoCompleteGL" + input.attr('id')).val() },
            "ShipDate": (isNaN(dateDelDate) == true ? null : (dateDelDate.getMonth() + 1) + "/" + dateDelDate.getDate() + "/" + dateDelDate.getFullYear()),
            "MappedUdf": MappedUdf,
            "TaxCode": $("#TaxCode" + input.attr('id')).val(),
            "DiscPrcnt": $("#DiscPrcnt" + input.attr('id')).val(),
            "VatPrcnt": $("#VatPrcnt" + input.attr('id')).val()
        });

        mTotalRow++;

    });

    var counter = 0;
    $("#TableFreight :input[type=hidden]").each(function () {
        PurchaseOrderView.ListFreight.push({
            LineNum: $("#itemLineNum_" + counter).val(),
            Comments: $("#Comments" + counter).val(),
            TaxCode: $("#TaxCode" + counter).val(),
            DistrbMthd: $("#DistrMethod" + counter).val(),
            LineTotal: $("#LineTotal" + counter).val(),
            OcrCode: $("#DistrRule" + counter).val(),
            Project: $("#Project" + counter).val(),
            ExpnsCode: $("#ExpenseCode_" + counter).val()
        });

        counter++;
    });

    if (mTotalRow == 0) {
        $('#alertItems').show();
        $('#Loading').modal('hide');
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return;
    }

    $('#Loading').modal({
        backdrop: 'static'
    });

    $.ajax({
        url: "/Sales/SalesQuotation/Add",
        async: false,
        type: "POST",
        data: JSON.stringify(PurchaseOrderView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            if (data.ServiceAnswer == "Ok") {
                $('#Loading').modal('hide');
                location.href = data.RedirectUrl;
            }
            else {
                $('#Loading').modal('hide');
                ErrorPO(data.ErrorMsg);
            }
        }
    });
}

function Update() {
    var mTypeCurrency = "";
    var mTotal = (Number($("#TotalDocPD").val()));
    var mTotalRow = 0;

    $('#Loading').modal({
        backdrop: 'static'
    })

    switch ($("#TypeCurrency").val()) {
        case "C":
            $("#txtCurrencySg").show();
            mTypeCurrency = $("#txtCurrencySg").val();
            break;
        case "L":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#LocalCurrency").val();
            break;
        case "S":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#SystemCurrency").val();
            break;
    }

    var mDocumentAddress = {
        DocEntry: $("#DocEntry").val(),
        StreetS: $("#StreetS").val(),
        StreetB: $("#StreetB").val(),
        StreetNoS: $("#StreetNoS").val(),
        StreetNoB: $("#StreetNoB").val(),
        BlockS: $("#BlockS").val(),
        BlockB: $("#BlockB").val(),
        CityS: $("#CityS").val(),
        CityB: $("#CityB").val(),
        ZipCodeS: $("#ZipCodeS").val(),
        ZipCodeB: $("#ZipCodeB").val(),
        CountyS: $("#CountyS").val(),
        CountyB: $("#CountyB").val(),
        StateS: $("#StateS").val(),
        StateB: $("#StateB").val(),
        CountryS: $("#CountryS").val(),
        CountryB: $("#CountryB").val(),
        BuildingS: $("#BuildingS").val(),
        BuildingB: $("#BuildingB").val(),
        GlbLocNumS: $("#GlbLocNumS").val(),
        GlbLocNumB: $("#GlbLocNumB").val()

    }
    var dateDocDate = $("#DocDate").datepicker("getDate");
    var dateDocDueDate = $("#DocDueDate").datepicker("getDate");
    var dateTaxDate = $("#TaxDate").datepicker("getDate");

    var PurchaseOrderView = {
        DocEntry: $("#DocEntry").val(),
        DocDate: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
        DocDueDate: (isNaN(dateDocDueDate) == true ? null : (dateDocDueDate.getMonth() + 1) + "/" + dateDocDueDate.getDate() + "/" + dateDocDueDate.getFullYear()),
        TaxDate: (isNaN(dateTaxDate) == true ? null : (dateTaxDate.getMonth() + 1) + "/" + dateTaxDate.getDate() + "/" + dateTaxDate.getFullYear()),
        CardCode: $("#txtCodeVendor").val(),
        DocCur: mTypeCurrency,
        DocTotal: mTotal,
        CardName: $("#txtNameVendor").val(),
        DiscPrcnt: $("#DiscPrcnt").val(),
        CntctCode: $('#ListContact').val(),
        CurSource: $('#CurSource').val(),
        SlpCode: $('#Buyer').val(),
        TrnspCode: $('#ShipType').val(),
        NumAtCard: ($('#NumAtCard').val() != "" ? $('#NumAtCard').val() : $("#txtBasketID").val()),
        OwnerCode: $('#OwnerCode').val(),
        Comments: $('#Comments').val(),
        PageKey: $('#Pagekey').val(),
        ShipToCode: $("#BpAddressSelectShipTo option[value='" + $("#BpAddressSelectShipTo").val() + "']").text(),
        PayToCode: $("#BpAddressSelect option[value='" + $("#BpAddressSelect").val() + "']").text(),
        SQAddress: mDocumentAddress,
        GroupNum: $("#PaymentTerm").val(),
        PeyMethod: $("#PeyMethod").val(),
        ListItem: [],
        Lines: [],
        ListFreight: [],
        MappedUdf: []
    };

    var listUDFHEAD = $("input[id^=HEADUDF_]");
    var listUDFHEADSelect = $("select[id^=HEADUDF_]");
    for (i = 0; i < listUDFHEAD.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEAD[i].id.substring(8),
            Value: $("#" + listUDFHEAD[i].id).val(),
        });
    }
    for (i = 0; i < listUDFHEADSelect.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEADSelect[i].id.substring(8),
            Value: $("#" + listUDFHEADSelect[i].id).val(),
        });
    }

    //Obtengo los names de las lineas los meto en un array y luego hago un distinct para evitar los repetidos
    var listUDFLines = $("input[id^=LINEUDF_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(8))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(8))
    }
    linesUDFNames = unique(linesUDFNames);

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        var price = $("#txt" + input.attr('id')).val();
        var quantity = $("#txtcount" + input.attr('id')).val();
        var whCode = "";
        var UomCode = $("#UOMAuto" + input.attr('id')).val();

        if ($("#AutoComplete" + input.attr('id')).val() != "") {
            whCode = $("#AutoComplete" + input.attr('id')).val();
        }
        else {
            whCode = null;
        }

        var DistRule = "";

        if ($("#AutoCompleteDR" + input.attr('id')).val() != "") {
            DistRule = $("#AutoCompleteDR" + input.attr('id')).val();
        }
        else {
            DistRule = null;
        }

        var DistRule2 = "";

        if ($("#AutoCompleteDR_2" + input.attr('id')).val() != "") {
            DistRule2 = $("#AutoCompleteDR_2" + input.attr('id')).val();
        }
        else {
            DistRule2 = null;
        }

        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }
        var dateDelDate = $("#DelDate" + input.attr('id')).datepicker("getDate");
        PurchaseOrderView.Lines.push({
            "ItemCode": input.attr('id'),
            "Dscription": $("#DescItem" + input.attr('id')).val(),
            "Quantity": parseFloat(quantity),
            "Price": parseFloat(price),
            "PriceBefDi": parseFloat(price),
            "Currency": $("#dp" + input.attr('id')).val(),
            "LineNum": input.attr('id'),
            "WhsCode": whCode,
            "UomCode": UomCode,
            "OcrCode": DistRule,
            "OcrCode2": DistRule2,
            "FreeTxt": $("#FreeTxt" + input.attr('id')).val(),
            "GLAccount": { FormatCode: $("#AutoCompleteGL" + input.attr('id')).val() },
            "ShipDate": (isNaN(dateDelDate) == true ? null : (dateDelDate.getMonth() + 1) + "/" + dateDelDate.getDate() + "/" + dateDelDate.getFullYear()),
            "MappedUdf": MappedUdf,
            "TaxCode": $("#TaxCode" + input.attr('id')).val(),
            "DiscPrcnt": $("#DiscPrcnt" + input.attr('id')).val(),
            "VatPrcnt": $("#VatPrcnt" + input.attr('id')).val()
        });

        mTotalRow++;

    });

    var counter = 0;
    $("#TableFreight :input[type=hidden]").each(function () {
        PurchaseOrderView.ListFreight.push({
            LineNum: $("#itemLineNum_" + counter).val(),
            Comments: $("#Comments" + counter).val(),
            TaxCode: $("#TaxCode" + counter).val(),
            DistrbMthd: $("#DistrMethod" + counter).val(),
            LineTotal: $("#LineTotal" + counter).val(),
            OcrCode: $("#DistrRule" + counter).val(),
            Project: $("#Project" + counter).val(),
            ExpnsCode: $("#ExpenseCode_" + counter).val()
        });

        counter++;
    });

    if (mTotalRow == 0) {
        $('#alertItems').show();
        return;
    }

    $('#Loading').modal({
        backdrop: 'static'
    })

    $.ajax({
        url: "/Sales/SalesQuotation/Update",
        async: false,
        type: "POST",
        data: JSON.stringify(PurchaseOrderView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorPO(data);
            }
        }
    });
}

function DeleteRow(Id) {

    $.post("/Sales/SalesQuotation/DeleteRow", {
        ItemCode: Id,
        pPageKey: $('#Pagekey').val()
    })
       .success(function (data) {
           if (data == "Ok") {
               //Deleting items from  ListLinesChanged array who contains the deleted row id.
               ListLinesChanged = unique(ListLinesChanged);
               var index = ListLinesChanged.indexOf(Id);
               if (index > -1) {
                   ListLinesChanged.splice(index, 1);
               }
               $("#tr" + Id).remove();
               $("#TypeCurrency").change();
           }
           else {
               ErrorPO(data);
           }
       });

}

function ErrorPO(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function SetOwner(pCode, pName) {

    $('#OwnerCode').val(pCode);
    $('#OwnerName').val(pName);
    $('#EmployeeModal').modal('hide');
}


function setAddress() {
    $.grep(addressesJS, function (v) {
        if (v.Address.replace(/\s+/g, '') == $("#BpAddressSelect").val()) {
            $("#StreetB").val(v.Street);
            $("#StreetNoB").val(v.StreetNo);
            $("#BlockB").val(v.Block);
            $("#CityB").val(v.City);
            $("#ZipCodeB").val(v.ZipCode);
            $("#CountyB").val(v.County);
            $("#StateB").val(v.State);
            $("#CountryB").val(v.Country);
            $("#BuildingB").val(v.Building);
            $("#GlbLocNumB").val(v.GlbLocNum);
        }
    });
}

function setAddressShipTo() {
    $.grep(addressesJS, function (v) {
        if (v.Address.replace(/\s+/g, '') == $("#BpAddressSelectShipTo").val()) {
            $("#StreetS").val(v.Street);
            $("#StreetNoS").val(v.StreetNo);
            $("#BlockS").val(v.Block);
            $("#CityS").val(v.City);
            $("#ZipCodeS").val(v.ZipCode);
            $("#CountyS").val(v.County);
            $("#StateS").val(v.State);
            $("#CountryS").val(v.Country);
            $("#BuildingS").val(v.Building);
            $("#GlbLocNumS").val(v.GlbLocNum);
        }
    });
}

//Inicio Matrix 
function ShowModel(pCode) {

    $('#Loading').modal({
        backdrop: 'static'
    });

    $.ajax({
        url: '/Sales/SalesQuotation/_ModelDetail',
        contextType: 'application/html;charset=utf-8',
        data: {  pPageKey: $('#Pagekey').val(),pModelCode: pCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $("#MatrixDetail").html(data);

        $('#Loading').modal('hide');

        $('#myModal2').modal({
            backdrop: 'static',
            keyboard: true
        });
    });

}

function PasteItems() {
    var mItemsQty = [];
    var mListItems = [];
    var mTypeCurrency = "";
    var mQty = "";


    $('.MatrixItems').each(function () {

        mQty = $(this).val();

        if (mQty != "" && parseFloat(mQty) > 0) {
            mItemsQty.push({ Code: $(this).attr("name"), Qty: $(this).val() });
            mListItems.push($(this).attr("name"));
        }
    })

    if (mItemsQty.length == 0) {
        return;
    }

    switch ($("#TypeCurrency").val()) {
        case "C":
            $("#txtCurrencySg").show();
            mTypeCurrency = $("#txtCurrencySg").val();
            break;
        case "L":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#LocalCurrency").val();
            break;
        case "S":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#SystemCurrency").val();
            break;
    }

    $.ajax({
        url: '/Sales/SalesQuotation/_ItemsFormMatrixModel',
        traditional: true,
        contextType: 'application/html;charset=utf-8',
        data: { pItems: mListItems, pCurrency: mTypeCurrency, pItemsQty: JSON.stringify(mItemsQty), pPageKey: $('#Pagekey').val(), pCardCode: $('#txtCodeVendor').val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#myModal').modal('hide');
        $("#ItemsSelect").html(data).trigger('change');
        $('#myModal2').modal('hide');
        $("#RightLateralMessage").show("slow");
        $("#RightLateralMessage").delay(2000).slideUp(1000);
    });
}

//Fin Matrix 

function updateRates() {
    var dateDocDate = $("#DocDate").datepicker("getDate");
    //Obtengo los rates para el dia del DocDate
    $.ajax({
        url: '/Sales/SalesQuotation/UpdateRateList',
        data: { DocDate: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()), pPageKey: $('#Pagekey').val() },
        type: 'POST',
        dataType: 'json'
    }).done(function (data) {
        ListRates = data;
        var mTypeCurrency = "";
        var mRow = 0;

        switch ($("#TypeCurrency").val()) {
            case "C":
                $("#txtCurrencySg").show();
                mTypeCurrency = $("#txtCurrencySg").val();
                break;
            case "L":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#LocalCurrency").val();
                break;
            case "S":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#SystemCurrency").val();
                break;
        }

        if (ListRates !== undefined) {
            $.grep(ListRates, function (v) {
                if (v.Currency.replace(/\s+/g, '') == mTypeCurrency && v.Local == false && v.System == false) {
                    if (v.Rate == 0) {
                        total = 0;
                        $('#alertRates').show();
                        $('#btnOk').attr("disabled", true);
                    }
                    else {
                        $("#RateCurrency").val(v.Rate);
                        $('#alertRates').hide();
                        $('#btnOk').attr("disabled", false);
                    }
                }
            });
        }

        CalculateTaxForAllLine();
        $("#DiscPrcnt").val(parseFloat($("#DiscPrcnt").val()).toFixed(4));
    });
}

$(document).keypress(function (e) {
    if (e.which == 13 && $("#HandHeldField").is(':focus')) {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var mTypeCurrency = "";
        switch ($("#TypeCurrency").val()) {
            case "C":
                $("#txtCurrencySg").show();
                mTypeCurrency = $("#txtCurrencySg").val();
                break;
            case "L":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#LocalCurrency").val();
                break;
            case "S":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#SystemCurrency").val();
                break;
        }
        updateLinesChanged(false);
        $.ajax({
            url: '/Sales/SalesQuotation/_GetHandheldItem',
            contextType: 'application/html;charset=utf-8',
            data: { pBPCurrency: mTypeCurrency, pPageKey: $('#Pagekey').val(), pItemCode: $("#HandHeldField").val(), CardCode: $("#txtCodeVendor").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $('#Loading').modal('hide');
            $("#HandHeldField").val("");
            $("#HandHeldField").focus();
            if (data != "") {
                $("#ItemsSelect").html(data).trigger('change');
            } else {
                $("#alertLectura").show("slow");
                $("#alertLectura").delay(1500).slideUp(1000);
            }
        });

    }

    if (e.which == 13 && $("#txtBasketID").is(':focus')) {
        $("#CheckBasketID").val("False");
        $('#BasketIDModal').modal('hide');
        $('#btnOk').click();
    }
});

function LineWasChanged(lineNum) {
    ListLinesChanged.push(lineNum);
}

function ChangeQuantityValue(lineNum, message) {
    if ($("#txtcount" + lineNum).val() < 0 || $("#txtcount" + lineNum).val().trim() == "") {
        ErrorPO(message);
        $("#txtcount" + lineNum).val(0);
    }
}

function updateLinesChanged(sendAsync) {
    ListLinesChanged = unique(ListLinesChanged);
    if (ListLinesChanged.length > 0) {
        var Lines = [];

        //I Obtain the names of the lines udf and y put it into an array, then i apply a distinct to the array to avoid repeated udfs
        var listUDFLines = $("input[id^=LINEUDF_]");
        var listUDFLinesSelect = $("select[id^=LINEUDF_]");
        var linesUDFNames = [];
        for (i = 0; i < listUDFLines.length; i++) {
            linesUDFNames.push(listUDFLines[i].name.substring(8))
        }
        for (i = 0; i < listUDFLinesSelect.length; i++) {
            linesUDFNames.push(listUDFLinesSelect[i].name.substring(8))
        }
        linesUDFNames = unique(linesUDFNames);

        for (i = 0; i < ListLinesChanged.length; i++) {
            var price = $("#txt" + ListLinesChanged[i]).val();
            var quantity = $("#txtcount" + ListLinesChanged[i]).val();
            var whCode = "";

            if ($("#AutoComplete" + ListLinesChanged[i]).val() != "") {
                whCode = $("#AutoComplete" + ListLinesChanged[i]).val();
            }
            else {
                whCode = null;
            }

            var DistRule = "";
            if ($("#AutoCompleteDR" + ListLinesChanged[i]).val() != "") {
                DistRule = $("#AutoCompleteDR" + ListLinesChanged[i]).val();
            }
            else {
                DistRule = null;
            }

            var DistRule2 = "";
            if ($("#AutoCompleteDR_2" + ListLinesChanged[i]).val() != "") {
                DistRule2 = $("#AutoCompleteDR_2" + ListLinesChanged[i]).val();
            }
            else {
                DistRule2 = null;
            }

            var UOM = "";
            if ($("#UOMAuto" + ListLinesChanged[i]).val() != "") {
                UOM = $("#UOMAuto" + ListLinesChanged[i]).val();
            }
            else {
                UOM = null;
            }

            var dateDelDate = $("#DelDate" + ListLinesChanged[i]).datepicker("getDate");

            var MappedUdf = [];
            for (j = 0; j < linesUDFNames.length; j++) {
                MappedUdf.push({
                    UDFName: linesUDFNames[j],
                    Value: $("#LINEUDF_" + linesUDFNames[j] + ListLinesChanged[i]).val(),
                });
            }

            Lines.push({
                "ItemCode": ListLinesChanged[i],
                "Dscription": $("#DescItem" + ListLinesChanged[i]).val(),
                "Quantity": parseFloat(quantity),
                "Price": parseFloat($("#Price" + ListLinesChanged[i]).html()),
                "PriceBefDi": parseFloat(price),
                "Currency": $("#dp" + ListLinesChanged[i]).val(),
                "LineNum": ListLinesChanged[i],
                "WhsCode": whCode,
                "OcrCode": DistRule,
                "OcrCode2": DistRule2,
                "UomCode": UOM,
                "FreeTxt": $("#FreeTxt" + ListLinesChanged[i]).val(),
                "GLAccount": { FormatCode: $("#AutoCompleteGL" + ListLinesChanged[i]).val() },
                "ShipDate": (isNaN(dateDelDate) == true ? null : (dateDelDate.getMonth() + 1) + "/" + dateDelDate.getDate() + "/" + dateDelDate.getFullYear()),
                "TaxCode": $("#TaxCode" + ListLinesChanged[i]).val(),
                "DiscPrcnt": parseFloat($("#DiscPrcnt" + ListLinesChanged[i]).val()),
                "VatPrcnt": parseFloat($("#VatPrcnt" + ListLinesChanged[i]).html()),
                "MappedUdf": MappedUdf,
                "Freight": [{ "ExpnsCode": $("#Freight1" + ListLinesChanged[i]).val(), "LineTotal": $("#FreightLC1" + ListLinesChanged[i]).val() }]
            });
        }

        $.ajax({
            url: "/Sales/SalesQuotation/_UpdateLinesChanged?pPageKey=" + $('#Pagekey').val(),
            async: sendAsync,
            type: "POST",
            data: JSON.stringify(Lines),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, jqXHR) {
                $("#btnChoose").prop('disabled', false);
                $("#btnChooseGrid").prop('disabled', false);
                $("#ModelMatrixAddButton").prop('disabled', false);
                ListLinesChanged = [];
            }
        });

    }
    else {
        $("#btnChoose").prop('disabled', false);
        $("#btnChooseGrid").prop('disabled', false);
        $("#ModelMatrixAddButton").prop('disabled', false);
    }

}

function CountrySChange() {
    $.post("/Sales/SalesQuotation/GetStateList", {
        Country: $("#CountryS").val(),
        SelectedState: $("#DefaultStateS").val()
    })
    .success(function (data) {
        $("#StateS").html(data);
    })
    .error(function (err) { });
}

function CountryBChange() {
    $.post("/Sales/SalesQuotation/GetStateList", {
        Country: $("#CountryB").val(),
        SelectedState: $("#DefaultStateB").val()
    })
    .success(function (data) {
        $("#StateB").html(data);
    })
    .error(function (err) { });
}

function showATP(pItemCode, pWarehouse) {
    $.ajax({
        url: '/Sales/SalesQuotation/_GetATPByWarehouse',
        contextType: 'application/html;charset=utf-8',
        data: { pItemCode: pItemCode, pWarehouse: pWarehouse },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#tdInfo-" + pItemCode + "-" + pWarehouse).html("");
        $("#tdInfo-" + pItemCode + "-" + pWarehouse).html(data);
        $("#collapse-" + pItemCode + "-" + pWarehouse).show();
    }).fail(function () {
        alert("error");
    });
}

function UpdateUOM(pLineNum) {

    $.post("/Sales/SalesQuotation/_UpdateUOM", {
        pLineNum: pLineNum,
        pUOMCode: $('#UOMAuto' + pLineNum).val(),
        pPageKey: $('#Pagekey').val()
    })
       .success(function (data) {
           $('#txt' + pLineNum).val(data).trigger('change');
       })
       .error(function (err) { Close(); });

}

function GetItemsModel() {
    $.ajax({
        url: '/Sales/SalesQuotation/GetItemsModel',
        contextType: 'application/html;charset=utf-8',
        data: {},
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#ModalItemsTable").html(data);
    });
}

function AddOrRemoveSelectedItem(ItemCode, pCheckBoxId) {
    var mChecked = $("input[id='" + pCheckBoxId + "']").is(':checked');
    if (mChecked == true) {
        pSelectedItems.push(ItemCode);
        $("input[id='" + pCheckBoxId + "']").parent().parent().addClass('selected');
    }
    else {
        var index = pSelectedItems.indexOf(ItemCode);
        if (index > -1) {
            pSelectedItems.splice(index, 1);
            $("input[id='" + pCheckBoxId + "']").parent().parent().removeClass('selected');
        }
    }
}

function unique(array) {
    return $.grep(array, function (el, index) {
        return index == $.inArray(el, array);
    });
}

function GetItemsGrid() {
    mLengthItemsGrid = ($("#PageQtyItemsGrid").val() == undefined ? 10 : Number($("#PageQtyItemsGrid").val()));
    mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

    $("#LoadGrid").show('slow');

    $.ajax({
        url: '/Sales/SalesQuotation/_ItemsGrid',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: searchViewModel.pPageKey, pMappedUdf: searchViewModel.pMappedUdf, pItemCode: searchViewModel.pItemCode, pItemData: searchViewModel.pItemData, pStart: mStartItemsGrid, pLength: mLengthItemsGrid },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $("#LoadGrid").hide();
        $("#ModalGridBody").html(data);
        $("#btnChooseGrid").prop('disabled', true);

        if (mStartItemsGrid <= 0) {
            $("#PreviousItemsGrid").attr('disabled', 'disabled');
        }
        else {
            $("#PreviousItemsGrid").removeAttr('disabled');;
        }

        if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityItemsGrid").val()) {
            $("#NextItemsGrid").attr('disabled', 'disabled');
        }
        else {
            $("#NextItemsGrid").removeAttr('disabled');;
        }

        updateLinesChanged(true);
    });

}

function DeleteAtt(pId) {

    $.post("/Sales/SalesQuotation/RemovedAtt", {
        pId: pId,
        pPageKey: $('#Pagekey').val()
    })
    .success(function (data) {
        $('#file-1').fileinput('clear');
        $("#FilesItems").html(data);
        $('#Loading').modal('hide');
        $("#alertAtt").hide();
    })
    .error(function (err) { Close(); });
}

function GetNextModelList() {
    mStartItemsGrid = mStartItemsGrid + Number($('#PageQtyModelList').val());
    GetModelsList();
};

function GetPreviousModelList() {
    mStartItemsGrid = mStartItemsGrid - Number($('#PageQtyModelList').val());
    GetModelsList();
};

$("#SearchModel").click(function () {
    mStartItemsGrid = 0;
    GetModelsList();
});

function GetModelsList() {
    $("#LoadSearchModel").show('slow');
    $('#Loading').modal({
        backdrop: 'static'
    });

    var mModelCode;
    var mModelName;

    if ($('#ckCodeModel').is(':checked')) {
        mModelCode = $('#txtSearchModel').val();
    }

    if ($('#ckNameModel').is(':checked')) {
        mModelName = $('#txtSearchModel').val();
    }

    searchViewModel = {};
    searchViewModel.CodeModel = mModelCode;
    searchViewModel.NameModel = mModelName;

    mLengthItemsGrid = ($("#PageQtyModelList").val() == undefined ? 10 : Number($("#PageQtyModelList").val()));
    mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

    searchViewModel.Start = mStartItemsGrid;
    searchViewModel.Length = mLengthItemsGrid;

    $("#LoadGrid").show('slow');

    $.ajax({
        url: '/Sales/SalesQuotation/_ModelListSearch',
        contextType: 'application/html;charset=utf-8',
        data: { searchViewModel },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $("#LoadGrid").hide();
        //$("#BodyPDMGrid").html(data);
        $("#btnChooseGrid").prop('disabled', true);

        $("#modelListSearch").html(data);
        $("#ModelMatrixAddButton").prop('disabled', true);
        $("#LoadSearchModel").hide();
        updateLinesChanged(true);

        $('#Loading').modal('hide');

        if (mStartItemsGrid <= 0) {
            $("#PreviousModelList").attr('disabled', 'disabled');
        }
        else {
            $("#PreviousModelList").removeAttr('disabled');;
        }

        if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityModelList").val()) {
            $("#NextModelList").attr('disabled', 'disabled');
        }
        else {
            $("#NextModelList").removeAttr('disabled');;
        }
    });

};