﻿var mFirstDoubleSearch = true;
var mStartColumn, mLengthColumn, mStartColumn1, mLengthColumn1, mStartColumn2, mLengthColumn2;
var searchViewModelColumn = {};
var searchViewModelColumn1 = {};
var searchViewModelColumn2 = {};
var searchViewModel = {};
var mGetTable = false;

$(document).ready(function () {

    $("#SearchCustomer").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });
    
    $("#DoubleTilesSearchCk").click(function () {
        if($("#DoubleTilesSearchCk").is(':checked'))
        {
            $("#ColumnRadio1").prop('checked', true);
        }
        else
        {
            $("#ColumnRadio1").prop('checked', false);
            $("#ColumnRadio2").prop('checked', false);
        }
    });

    $("#btSearch").click(function () {

        if ($('#TilesSearchCk').is(':checked') == false)
        {
            if (mGetTable == true)
            {
                $('#Loading').modal({
                    backdrop: 'static',
                    keyboard: true
                });

                $.ajax({
                    url: '/Sales/QuickOrder/_GetQuickOrderList',
                    contextType: 'application/html;charset=utf-8',
                    data: {},
                    type: 'POST',
                    dataType: 'html',
                    async: false
                }).done(function (data) {
                    $("#QuickOrderListPartial").html(data);
                    $('#Loading').modal('hide');
                }).fail(function () {
                    alert("error");
                    $('#Loading').modal('hide');
                });

                mGetTable = false;
                mFirstDoubleSearch = true;
            }

            if (typeof assetListVM_ListItems.dt === 'undefined' || assetListVM_ListItems.dt == null) {
                assetListVM_ListItems.init();
            }
            else {
                assetListVM_ListItems.refresh();
            }
        }
        else
        {
            mGetTable = true;

            var mListSearchUDF = $("input[id^=SearchItemUDF_]");
            var mListSearchUDFSelect = $("select[id^=SearchItemUDF_]");
            var mMappedUDFList = [];
            for (i = 0; i < mListSearchUDF.length; i++) {
                mMappedUDFList.push({
                    UDFName: mListSearchUDF[i].id.substring(14),
                    Value: $("#" + mListSearchUDF[i].id).val(),
                });
            }
            for (i = 0; i < mListSearchUDFSelect.length; i++) {
                mMappedUDFList.push({
                    UDFName: mListSearchUDFSelect[i].id.substring(14),
                    Value: $("#" + mListSearchUDFSelect[i].id).val(),
                });
            }
            searchViewModel = {};
            searchViewModel.pMappedUdf = mMappedUDFList;
            searchViewModel.pItemGroupCode = $('#ItemGroup').val();
            searchViewModel.pItemCode = $("#UDF_Code").val();
            searchViewModel.pItemName = $("#UDF_Name").val();
            searchViewModel.pTilesSearchCk = $('#TilesSearchCk').is(':checked');
            searchViewModel.pDoubleTilesSearchCk = $('#DoubleTilesSearchCk').is(':checked');
            searchViewModel.pColumnRadio = $('input[name=ColumnRadio]:checked').val();
            searchViewModel.pFirstDoubleSearch = mFirstDoubleSearch;
            searchViewModel.pNoItemsWithZeroStockCk = $('#NoItemsWithZeroStockCk').is(':checked');
            searchViewModel.pSupplierCardCode = $('#txtCodeBP').val();
            searchViewModel.pCardCode = $('#txtCodeCustomer').val();

            if ($("#DoubleTilesSearchCk").is(':checked'))
            {
                if ($('input[name=ColumnRadio]:checked').val() == "1") {
                    //Setting start numbers to do a new search
                    mStartColumn1 = 0;
                    mLengthColumn1 = 10;
                    searchViewModelColumn1 = searchViewModel;
                    GetItemsTile(1);
                }
                if ($('input[name=ColumnRadio]:checked').val() == "2") {
                    //Setting start numbers to do a new search
                    mStartColumn2 = 0;
                    mLengthColumn2 = 10;
                    searchViewModelColumn2 = searchViewModel;
                    GetItemsTile(2);
                }
            }
            else
            {
                mStartColumn = 0;
                mLengthColumn = 10;
                searchViewModelColumn = searchViewModel;
                GetItemsTile(0);
            }
        }
    });

    $("#TilesSearchCk").change(function () {
        if ($('#TilesSearchCk').is(':checked')) {
            $("#DoubleTilesSearchCk").removeAttr('disabled');
            if ($('#DoubleTilesSearchCk').is(':checked')) {
                $("input[name=ColumnRadio]").removeAttr('disabled');
            }
        }
        else {
            $("#DoubleTilesSearchCk").attr('disabled', 'disabled');
            $("input[name=ColumnRadio]").attr('disabled', 'disabled');
            $('#DoubleTilesSearchCk').removeAttr('checked');
        }
    });

    $("#DoubleTilesSearchCk").change(function () {
        if ($('#DoubleTilesSearchCk').is(':checked')) {
            $("input[name=ColumnRadio]").removeAttr('disabled');
        }
        else {
            $("input[name=ColumnRadio]").attr('disabled', 'disabled');
        }
    });

    $("#txtNameBP").attr("disabled", "disabled");

    $("#SearchSupplier").click(function () {
        if (typeof dtSupplier === 'undefined') {
            assetListSupplier.init();
        }
        else {
            assetListSupplier.refresh();
        }

    });

    if ($("#ExecuteSearch").val() == "True")
        $("#btSearch").click();
        
});

function addtoCart(ItemCode) {
    var qty = $("input[id='qty_" + ItemCode + "']").val();
    $.ajax({
        url: '/Sales/QuickOrder/AddToCart',
        contextType: 'application/html;charset=utf-8',
        data: { pItemCode: ItemCode, pQuantity: qty, pCustomerCode: $('#txtCodeCustomer').val()},
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        refreshCart();
    }).fail(function () {
    });

}

function refreshCart() {
    $.ajax({
        url: '/Sales/QuickOrder/ListItemCarrito',
        contextType: 'application/html;charset=utf-8',
        data: { pCustomerCode: $('#txtCodeCustomer').val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#QuickOrderCabeceraPartial").html(data);
        $(".modal-header button").click();
        $('.modal-backdrop').remove();
    }).fail(function () {
        $(".modal-header button").click();
        $('.modal-backdrop').remove();
    });

}

function updateCart(ItemCodejs) {
    var qty = $("input[id='qtyc_" + ItemCodejs + "']").val();
    var mWarehouse = $("select[id='Warehouse_" + ItemCodejs + "']").val();
    $.ajax({
        url: '/Sales/QuickOrder/UpdateCart',
        contextType: 'application/html;charset=utf-8',
        data: { pItemCode: ItemCodejs, pQuantity: qty, pWarehouse: mWarehouse },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        refreshCart();
    }).fail(function () {
    });
}

function deleteLineFromCart(ItemCodejs) {
    $.ajax({
        url: '/Sales/QuickOrder/DeleteLineFromCart',
        contextType: 'application/html;charset=utf-8',
        data: { pItemCode: ItemCodejs },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        refreshCart();
    }).fail(function () {
    });
}

function SetBP(pCode, pName, refreshAddress) {

    var mName = pName.replace("##", "'");

    $('#txtCodeBP').val(pCode).trigger('change');
    $('#txtNameBP').val(mName);
    $('#BPModal').modal('hide');
}

function SetCustomer(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeCustomer').val(pCode);
    $('#txtNameCustomer').val(mName);
    $('#CustomerModal').modal('hide');
    refreshCart();
}

function PlaceOrder()
{
    if($('#txtCodeCustomer').val() == "")    
    {
        $("#errorMessage").empty().append("<strong>" + $('#SelectCustomer').val() + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }
    else
    {
        location.href = '/Sales/SalesOrder/ActionPurchaseOrder?ActionPurchaseOrder=QuickOrder&IdPO=0&fromController=QuickOrder&pQuickOrderId=1&pCustomerCode=' + $('#txtCodeCustomer').val();
    }
}

function GetItemsTile(pColumnToSearch)
{
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    switch(pColumnToSearch)
    {
        case 0:
            searchViewModel = searchViewModelColumn;
            searchViewModel.pStart = mStartColumn;
            searchViewModel.pLength = mLengthColumn;
            searchViewModel.pTileToModify = pColumnToSearch;

            break;
        case 1:
            searchViewModel = searchViewModelColumn1;
            searchViewModel.pStart = mStartColumn1;
            searchViewModel.pLength = mLengthColumn1;
            searchViewModel.pTileToModify = pColumnToSearch;

            break;
        case 2:
            searchViewModel = searchViewModelColumn2;
            searchViewModel.pStart = mStartColumn2;
            searchViewModel.pLength = mLengthColumn2;
            searchViewModel.pTileToModify = pColumnToSearch;

            break;

        default:
            break;
    }

    $.ajax({
        url: '/Sales/QuickOrder/ListItemQuickOrderTile',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(searchViewModel),
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        if ($('#DoubleTilesSearchCk').is(':checked') == false || mFirstDoubleSearch == true || $('#TilesSearchCk').is(':checked') == false)
        {
            $("#QuickOrderListPartial").html(data);
            if ($('#DoubleTilesSearchCk').is(':checked') == false) {
                mFirstDoubleSearch = true;
            }
            else {
                mFirstDoubleSearch = false;
            }
            if (pColumnToSearch == 0) {
                if (mStartColumn - mLengthColumn < 0) {
                    $("#PreviousColumn").attr('disabled', 'disabled');
                }
                else {
                    $("#PreviousColumn").removeAttr('disabled');;
                }

                if (mStartColumn + mLengthColumn > $("#TotalQuantityColumn").val()) {
                    $("#NextColumn").attr('disabled', 'disabled');
                }
                else {
                    $("#NextColumn").removeAttr('disabled');;
                }

            }
            if (pColumnToSearch == 1) {
                searchViewModelColumn1.pFirstDoubleSearch = false;
            }
            if (pColumnToSearch == 2) {
                searchViewModelColumn2.pFirstDoubleSearch = false;
            }
        }
        else {
            if (pColumnToSearch  == 1) {
                $("#ListTilesDiv1").html(data);
                if (mStartColumn1 - mLengthColumn1 < 0)
                {
                    $("#PreviousColumn1").attr('disabled', 'disabled');
                }
                else
                {
                    $("#PreviousColumn1").removeAttr('disabled');;
                }

                if(mStartColumn1 + mLengthColumn1 > $("#TotalQuantityColumn1").val())
                {
                    $("#NextColumn1").attr('disabled', 'disabled');
                }
                else {
                    $("#NextColumn1").removeAttr('disabled');;
                }

            }
            if (pColumnToSearch == 2) {
                $("#ListTilesDiv2").html(data);
                if (mStartColumn2 - mLengthColumn2 < 0) {
                    $("#PreviousColumn2").attr('disabled', 'disabled');
                }
                else {
                    $("#PreviousColumn2").removeAttr('disabled');;
                }

                if (mStartColumn2 + mLengthColumn2 > $("#TotalQuantityColumn2").val()) {
                    $("#NextColumn2").attr('disabled', 'disabled');
                }
                else {
                    $("#NextColumn2").removeAttr('disabled');;
                }

            }
        }
        $('#Loading').modal('hide');
    }).fail(function (data) {
        alert("error");
        $('#Loading').modal('hide');
    });
}