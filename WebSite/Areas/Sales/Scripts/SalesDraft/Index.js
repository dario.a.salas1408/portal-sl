﻿$(document).ready(function () {
    jQuery('#txtNro').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $("#txtDocDateFrom").datepicker({});
    $("#txtDocDateTo").datepicker({});

    $("#btSearch").click(function () {
        if (typeof dt === 'undefined') {
            assetListVM.init();
        }
        else {
            assetListVM.refresh();
        }
    });

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

});

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');
}