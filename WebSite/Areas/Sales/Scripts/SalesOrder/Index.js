﻿$(document).ready(function () {
    jQuery('#txtNro').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $('.input-group.date').datepicker({});

    $("#txtNameVendor").prop('disabled', true);

    $("#LoadSE").hide();
    $("#LoadEmployee").hide();

    $('#SEModal').on('show.bs.modal', function (e) {
        $("#ModalBodySE").html("");
    });
    $('#EmployeeModal').on('show.bs.modal', function (e) {
        $("#ModalBodyEmployee").html("");
    });

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    $("#SearchEmployee").click(function () {

        var mEmployeeCode;
        var mEmployeeName;

        if ($('#ckCodeEmployee').is(':checked')) {
            mEmployeeCode = $('#txtSearchEmployee').val();
        }

        if ($('#ckNameEmployee').is(':checked')) {
            mEmployeeName = $('#txtSearchEmployee').val();
        }
        $("#LoadEmployee").show('slow');
        $("#ModalBodyEmployee").show('slow');

        $.ajax({
            url: '/Sales/SalesOrder/_Employee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeCode: mEmployeeCode, pEmployeeName: mEmployeeName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadEmployee").hide('slow');
            $("#ModalBodyEmployee").html(data);
        });

    });

    $("#SearchSE").click(function () {
        var mSEName;
        mSEName = $('#txtSE').val();

        $("#LoadSE").show('slow');

        $.ajax({
            url: '/Sales/SalesOrder/_GetSalesEmployee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeName: mSEName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadSE").hide('slow');
            $("#ModalBodySE").html(data);


        }).fail(function () {
            alert("error");
        }).always(function () {

        });

    });

    $("#btSearch").click(function () {  
        if (typeof dt === 'undefined') {
            assetListVM.init();
        }
        else {
            assetListVM.refresh();
        }
    });

});

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');

}

function SetOwner(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#OwnerNumber').val(pCode);
    $('#txtOwner').val(mName);
    $('#EmployeeModal').modal('hide');
}

function SetSE(pCode, pName) {

    $('#SalesEmployeeNumber').val(pCode);
    $('#txtSalesEmployee').val(pName);
    $('#SEModal').modal('hide');
}