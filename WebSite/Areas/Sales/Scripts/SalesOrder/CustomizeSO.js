﻿// El trigger de la tasa de referencia y la fecha de entrega se ejecutara solamente cuando el formulario este en modo add
// para el modo update se entiende que estos campos ya contienen información por lo que no debe ejecutarse
if (formMode == "Add") {
    // Tasa de referencia

    var QueryObject = {
        pQueryIdentifier: 'TasaReferencia',
        pQueryParams: []
    }
    var dateDocDate = $("#DocDate").datepicker("getDate");

    QueryObject.pQueryParams.push({
        Key: 'DocDate',
        Value: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
    });

    $.ajax({
        url: "/QueryManager/GetQueryResult",
        async: false,
        type: "POST",
        data: JSON.stringify(QueryObject),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            var jsonObj = $.parseJSON(data);
            if (data != "[]")
                $("#HEADUDF_U_TasaUSDREF").val(jsonObj[0].Rate);
        }
    });

    // Fecha de entrega

    QueryObject = {
        pQueryIdentifier: 'FechaEntrega',
        pQueryParams: []
    }

    $.ajax({
        url: "/QueryManager/GetQueryResult",
        async: false,
        type: "POST",
        data: JSON.stringify(QueryObject),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            var jsonObj = $.parseJSON(data);
            if (data != "[]")
                $('#DocDueDate').datepicker("setDate", new Date(jsonObj[0].FechaEntrega));
        }
    });
}
// Exento ITC y Nro de DJ (se ejecuta cuando se asigna un nuevo vendedor a la orden, 
// para ordenes ya creadas con vendedor asignado no se ejecutará)

$("#txtCodeVendor").change(function () {
    var QueryObject = {
        pQueryIdentifier: 'ExentoITC',
        pQueryParams: []
    }

    QueryObject.pQueryParams.push({
        Key: 'CardCode',
        Value: $('#txtCodeVendor').val(),
    });

    $.ajax({
        url: "/QueryManager/GetQueryResult",
        async: false,
        type: "POST",
        data: JSON.stringify(QueryObject),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            var jsonObj = $.parseJSON(data);
            if (data != "[]") {
                $("#HEADUDF_U_ExentoITC").val(jsonObj[0].QryGroup1);
                if (jsonObj[0].QryGroup1 == "N") {
                    $("#HEADUDF_U_NroDJ").val("N/A");
                }
                else {
                    $("#HEADUDF_U_NroDJ").val("");
                }
            }
        }
    });
});

//Este trigger se ejecutará al terminar de carga la orden en el caso que el formulario se encuentre en modo Update puesto que
//hay que añadir los eventos a las lineas ya existentes
if (formMode == "Update") {
    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);
        $("#txtcount" + input.attr('id')).unbind('blur');

        $("#txtcount" + input.attr('id')).blur(function () {
            var QueryObject = {
                pQueryIdentifier: 'FamiliaITC',
                pQueryParams: []
            }

            QueryObject.pQueryParams.push({
                Key: 'ExentoITC',
                Value: $("#HEADUDF_U_ExentoITC").val(),
            });
            var dateDocDate = $("#DocDate").datepicker("getDate");
            QueryObject.pQueryParams.push({
                Key: 'DocDate',
                Value: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
            });
            QueryObject.pQueryParams.push({
                Key: 'ItemCode',
                Value: $("#ItemCode" + input.attr('id')).val(),
            });

            $.ajax({
                url: "/QueryManager/GetQueryResult",
                async: false,
                type: "POST",
                data: JSON.stringify(QueryObject),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, textStatus, jqXHR) {
                    var jsonObj = $.parseJSON(data);
                    if (data != "[]" && jsonObj[0].FLIAITC != null) {
                        $("#Freight1" + input.attr('id')).val(jsonObj[0].FLIAITC);
                    }
                }
            });
        });

        $("#txtcount" + input.attr('id')).blur(function () {
            var QueryObject = {
                pQueryIdentifier: 'MontoITC',
                pQueryParams: []
            }

            QueryObject.pQueryParams.push({
                Key: 'ExentoITC',
                Value: $("#HEADUDF_U_ExentoITC").val(),
            });
            var dateDocDate = $("#DocDate").datepicker("getDate");
            QueryObject.pQueryParams.push({
                Key: 'DocDate',
                Value: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
            });
            QueryObject.pQueryParams.push({
                Key: 'ItemCode',
                Value: $("#ItemCode" + input.attr('id')).val(),
            });
            QueryObject.pQueryParams.push({
                Key: 'ItemQuantity',
                Value: $("#txtcount" + input.attr('id')).val(),
            });
            QueryObject.pQueryParams.push({
                Key: 'UomCode',
                Value: $("#UOMAuto" + input.attr('id')).val(),
            });
            $.ajax({
                url: "/QueryManager/GetQueryResult",
                async: false,
                type: "POST",
                data: JSON.stringify(QueryObject),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, textStatus, jqXHR) {
                    var jsonObj = $.parseJSON(data);
                    if (data != "[]" && jsonObj[0].itcMonto != null) {
                        $("#FreightLC1" + input.attr('id')).val(jsonObj[0].itcMonto);
                    }
                }
            });
        });

    });
}

//Este trigger se ejecutará en el momento en que se agregue una nueva linea a la orden
$("#ItemsSelect").change(function () {
    // Familia ITC y Monto ITC
    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);
        $("#txtcount" + input.attr('id')).unbind('blur');

        $("#txtcount" + input.attr('id')).blur(function () {
            var QueryObject = {
                pQueryIdentifier: 'FamiliaITC',
                pQueryParams: []
            }

            QueryObject.pQueryParams.push({
                Key: 'ExentoITC',
                Value: $("#HEADUDF_U_ExentoITC").val(),
            });
            var dateDocDate = $("#DocDate").datepicker("getDate");
            QueryObject.pQueryParams.push({
                Key: 'DocDate',
                Value: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
            });
            QueryObject.pQueryParams.push({
                Key: 'ItemCode',
                Value: $("#ItemCode" + input.attr('id')).val(),
            });

            $.ajax({
                url: "/QueryManager/GetQueryResult",
                async: false,
                type: "POST",
                data: JSON.stringify(QueryObject),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, textStatus, jqXHR) {
                    var jsonObj = $.parseJSON(data);
                    if (data != "[]" && jsonObj[0].FLIAITC != null) {
                        $("#Freight1" + input.attr('id')).val(jsonObj[0].FLIAITC);
                    }
                }
            });
        });

        $("#txtcount" + input.attr('id')).blur(function () {
            var QueryObject = {
                pQueryIdentifier: 'MontoITC',
                pQueryParams: []
            }

            QueryObject.pQueryParams.push({
                Key: 'ExentoITC',
                Value: $("#HEADUDF_U_ExentoITC").val(),
            });
            var dateDocDate = $("#DocDate").datepicker("getDate");
            QueryObject.pQueryParams.push({
                Key: 'DocDate',
                Value: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
            });
            QueryObject.pQueryParams.push({
                Key: 'ItemCode',
                Value: $("#ItemCode" + input.attr('id')).val(),
            });
            QueryObject.pQueryParams.push({
                Key: 'ItemQuantity',
                Value: $("#txtcount" + input.attr('id')).val(),
            });
            QueryObject.pQueryParams.push({
                Key: 'UomCode',
                Value: $("#UOMAuto" + input.attr('id')).val(),
            });
            $.ajax({
                url: "/QueryManager/GetQueryResult",
                async: false,
                type: "POST",
                data: JSON.stringify(QueryObject),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data, textStatus, jqXHR) {
                    var jsonObj = $.parseJSON(data);
                    if (data != "[]" && jsonObj[0].itcMonto != null) {
                        $("#FreightLC1" + input.attr('id')).val(jsonObj[0].itcMonto);
                    }
                }
            });
        });

    });

});

