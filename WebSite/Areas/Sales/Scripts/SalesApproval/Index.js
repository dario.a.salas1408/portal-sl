﻿$(document).ready(function () {

    $('.input-group.date').datepicker({

    });

    $("#btSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var dateTxDoctDate = $("#txtDocDate").datepicker("getDate");
        $.ajax({
            url: '/Sales/SalesApproval/ListApproval',
            contextType: 'application/html;charset=utf-8',
            data: {
            	txtDocDate: (isNaN(dateTxDoctDate) == true ? null : ((dateTxDoctDate.getMonth() + 1) + "/" + dateTxDoctDate.getDate() + "/" + dateTxDoctDate.getFullYear())),
            	txtDocStatus: $("#txtDocStatus").val(),
            	txtObjectId: $("#txtObjectId").val()
            },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#BodyApproval").html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            alert("error");
        });

    });

    $("#btSearch2").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var dateTxDoctDate = $("#txtDocDate2").datepicker("getDate");
        $.ajax({
            url: '/Sales/SalesApproval/ListApprovalByOriginator',
            contextType: 'application/html;charset=utf-8',
            data: {
            	txtDocDate: (isNaN(dateTxDoctDate) == true ? null : ((dateTxDoctDate.getMonth() + 1) + "/" + dateTxDoctDate.getDate() + "/" + dateTxDoctDate.getFullYear())),
            	txtDocStatus: $("#txtDocStatus2").val(),
            	txtObjectId: $("#txtObjectId2").val()
            },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#BodyApproval2").html(data);
            $('#Loading').modal('hide');
        }).fail(function () {
            alert("error");
        });

    });

});

function SelectDesicion() {

    $("#LoadApprovalTemplate").show('slow');

    $.ajax({
        url: '/Sales/SalesApproval/SaveApprovalResponse',
        contextType: 'application/html;charset=utf-8',
        data: { owwdCode: $("#WddCode").val(), remark: $("#Remarks").val(), approvalCode: $("#ApproveDesicion").val()},
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#ModalBodyApprovalTemplate").html("");
        window.location.reload(true);
    }).fail(function () {
        alert("error");
    });
}

function GetApprovalTemplate(wddCode) {

    $('#ApproveModal').show();
    $("#LoadApprovalTemplate").show('slow');

    $.ajax({
        url: '/Sales/SalesApproval/GetApprovalDesicion',
        contextType: 'application/html;charset=utf-8',
        data: { WddCode: wddCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadApprovalTemplate").hide('slow');
        $("#ModalBodyApprovalTemplate").html(data);
    }).fail(function () {
        alert("error");
    });

}

function GetHistory(wddCode) {
    $("#LoadHistory").show('slow');
    $.ajax({
        url: '/Sales/SalesApproval/_GetHistory',
        contextType: 'application/html;charset=utf-8',
        data: { WddCode: wddCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        freightClicked = true;
        $("#LoadHistory").hide('slow');
        $("#ModalBodyHistory").html(data);
    });

}

function ClearHistoryTable() {
    $("#ModalBodyHistory").html("");
}

function CreateDocument(docEntry) {
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    $.ajax({
        url: '/Sales/SalesApproval/CreateDocument',
        contextType: 'application/html;charset=utf-8',
        data: { DraftDocEntry: docEntry },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#Loading').modal('hide');
        window.location.reload(true);
    }).fail(function () {
        alert("error");
    });
}