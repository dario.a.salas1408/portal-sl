﻿$(document).ready(function () {

    $('.input-group.date').datepicker({});

    $('#txtDate').datepicker({});
    $("#txtNameVendor").prop('disabled', true);

    $("#LoadVendor").hide();
    $("#LoadSE").hide();
    $("#LoadEmployee").hide();
    
    $('#EmployeeModal').on('show.bs.modal', function (e) {
        $("#ModalBodyEmployee").html("");
    });  

    $("#SearchEmployee").click(function () {

        var mEmployeeCode;
        var mEmployeeName;

        if ($('#ckCodeEmployee').is(':checked')) {
            mEmployeeCode = $('#txtSearchEmployee').val();
        }

        if ($('#ckNameEmployee').is(':checked')) {
            mEmployeeName = $('#txtSearchEmployee').val();
        }
        $("#LoadEmployee").show('slow');
        $("#ModalBodyEmployee").show('slow');

        $.ajax({
            url: '/Sales/MyOpportunities/_Employee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeCode: mEmployeeCode, pEmployeeName: mEmployeeName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadEmployee").hide('slow');
            $("#ModalBodyEmployee").html(data);
        });

    });   

    $("#btSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var dateTxtDate = $("#txtDate").datepicker("getDate");
        $.ajax({
            url: '/Sales/MyOpportunities/ListOpportunities',
            contextType: 'application/html;charset=utf-8',
            data: { txtCodeVendor: $("#txtCodeVendor").val(), txtDate: (isNaN(dateTxtDate) == true ? null : ((dateTxtDate.getMonth() + 1) + "/" + dateTxtDate.getDate() + "/" + dateTxtDate.getFullYear())), pOpporName: $("#txtOpporName").val(), txtDocStatus: $("#txtDocStatus").val(), pOwnerCode: $('#txtOwnerCode').val(), pSECode: $('#SalesEmployeeNumber').val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyOpportunity").html(data);
            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });

});


function SetOwner(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtOwnerCode').val(pCode);
    $('#txtOwner').val(mName);
    $('#EmployeeModal').modal('hide');
}

