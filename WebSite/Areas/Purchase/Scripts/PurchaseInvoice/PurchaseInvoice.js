﻿$(document).ready(function () {

    $('.input-group.date').datepicker({});
    $('#alertRates').hide();
    $('#alertItems').hide();

    switch (formMode) {
        case "Update":
            SetVendor($('#txtCodeVendor').val(), $('#txtNameVendor').val(), false);
            $("#btnCopy").hide();
            break;
        case "Add":
            if ($('#txtCodeVendor').val() != "")
            { SetVendor($('#txtCodeVendor').val(), $('#txtNameVendor').val(), false); }
            break;
        case "View":
            SetVendor($('#txtCodeVendor').val(), $('#txtNameVendor').val(), false);
            $("#btnOk").hide();
            $("#btnCopy").hide();
            break;

    }

    var TotalDoc = 0;
    var mIntRow = 0;

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);

        var value = parseFloat($("#td" + input.attr('id')).html());

        TotalDoc = TotalDoc + value;

        mIntRow++;
    });

    $("#TotalDoc").val(TotalDoc.toFixed(2));
    $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));
    

    if (formMode == "Add")
        $('#Buyer').val("-1");

    $("#Load").hide();

    $('#myModal').on('show.bs.modal', function (e) {
        $("#ModalBody").html("");
    });

    $('#VendorModal').on('show.bs.modal', function (e) {
        $("#ModalBodyVendor").html("");
    });

    $('#BpAddressSelect').change(function () {
        setAddress();
    });

    $.getScript("../../Scripts/ExternalJS/CustomizePI.js", function (data, textStatus, jqxhr) { });
});

function SetVendor(pCode, pName, refreshAddress) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');

    $.ajax({
        url: "/Purchase/PurchaseOrder/GetBp",
        type: "POST",
        data: { Id: pCode, LocalCurrency: $('#LocalCurrency').val() },
        dataType: 'json',
        success: function (data, text) {
            if (data.ErrorResponse == "Ok") {
                $("#ListContact").empty();

                $.each(data.ListContact, function (key) {
                    $('#ListContact').append($('<option>', {
                        value: data.ListContact[key].CrtctCode,
                        text: data.ListContact[key].Name
                    }));
                });

                addressesJS = data.Addresses;
                $("#BpAddressSelect").html("");
                $.grep(data.Addresses, function (v) {
                    if (v.AdresType == "B") {
                        if (v.Address == data.BillToDef)
                            $("#BpAddressSelect").append('<option value=' + v.Address.replace(/\s+/g, '') + ' selected = "selected">' + v.Address + '</option>');
                        else
                            $("#BpAddressSelect").append('<option value=' + v.Address.replace(/\s+/g, '') + '>' + v.Address + '</option>');
                    }
                });
                var found_names;
                if ((data.BillToDef != null || data.BillToDef != "") && refreshAddress == true) {
                    found_names = $.grep(data.Addresses, function (v) {
                        if (v.AdresType == "B" && v.Address == data.BillToDef)
                            return v;
                    });
                    if (found_names[0] != null) {
                        $("#StreetB").val(found_names[0].Street);
                        $("#StreetNoB").val(found_names[0].StreetNo);
                        $("#BlockB").val(found_names[0].Block);
                        $("#CityB").val(found_names[0].City);
                        $("#ZipCodeB").val(found_names[0].ZipCode);
                        $("#CountyB").val(found_names[0].County);
                        $("#StateB").val(found_names[0].State);
                        $("#CountryB").val(found_names[0].Country);
                        $("#BuildingB").val(found_names[0].Building);
                        $("#GlbLocNumB").val(found_names[0].GlbLocNum);
                    }
                }
            }
            else {
                ErrorPO(data.ErrorResponse);
            }

        }
    });

}

function ErrorPO(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function SetOwner(pCode, pName) {

    $('#OwnerCode').val(pCode);
    $('#OwnerName').val(pName);
    $('#EmployeeModal').modal('hide');
}

function setAddress() {
    $.grep(addressesJS, function (v) {
        if (v.Address.replace(/\s+/g, '') == $("#BpAddressSelect").val()) {
            $("#StreetB").val(v.Street);
            $("#StreetNoB").val(v.StreetNo);
            $("#BlockB").val(v.Block);
            $("#CityB").val(v.City);
            $("#ZipCodeB").val(v.ZipCode);
            $("#CountyB").val(v.County);
            $("#StateB").val(v.State);
            $("#CountryB").val(v.Country);
            $("#BuildingB").val(v.Building);
            $("#GlbLocNumB").val(v.GlbLocNum);
        }
    });
}

function ChangeQuantityValue(lineNum, message) {
    if ($("#txtcount" + lineNum).val() < 0 || $("#txtcount" + lineNum).val().trim() == "") {
        ErrorPO(message);
        $("#txtcount" + lineNum).val(0);
    }
}