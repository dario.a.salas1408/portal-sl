﻿$(document).ready(function () {
    jQuery('#txtNro').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $("#txtDocDateFrom").datepicker({});
    $("#txtDocDateTo").datepicker({});
    $("#txtReqDate").datepicker({});

    $("#btSearch").click(function () {

        if (typeof dt === 'undefined') {
            assetListVM.init();
        }
        else {
            assetListVM.refresh();
        }
    });

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });
});



        if ($('#ckNameVendor').is(':checked')) {
            mVendorName = $('#txtSearchVendor').val();
        }



//function CreateDocument(docEntry)
//{
//    $('#Loading').modal({
//        backdrop: 'static',
//        keyboard: true
//    });

//    $.ajax({
//        url: '/Purchase/Draft/CreateDocument',
//        contextType: 'application/html;charset=utf-8',
//        data: { DraftDocEntry: docEntry },
//        type: 'POST',
//        dataType: 'html'
//    }).done(function (data) {
//        $('#Loading').modal('hide');
//        window.location.reload(true);
//    }).fail(function () {
//        alert("error");
//    });
//}

//function CloseModal()
//{
//    $('.modal-backdrop').remove();
//    $("#ApprovalTemplate").html("");
//    $('#ApproveModal').remove();
//}

//function GetHistory(pDocEntry,pObjType) {
//    $("#LoadHistory").show('slow');
//    $.ajax({
//        url: '/Purchase/Draft/_GetHistory',
//        contextType: 'application/html;charset=utf-8',
//        data: { pDocEntry: pDocEntry, pObjType: pObjType },
//        type: 'POST',
//        dataType: 'html'
//    }).done(function (data) {
//        freightClicked = true;
//        $("#LoadHistory").hide('slow');
//        $("#ModalBodyHistory").html(data);
//    });

//}

//function ClearHistoryTable() {
//    $("#ModalBodyHistory").html("");
//}

function SetVendor(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(mName);
    $('#VendorModal').modal('hide');
}