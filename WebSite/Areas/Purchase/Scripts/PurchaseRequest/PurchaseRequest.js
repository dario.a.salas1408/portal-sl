﻿var ListRates;
var LastDocDate;
var ListLinesChanged = [];
//var used to set the items that will be added to the actual order
var pSelectedItems = [];
//vars used to pagination in items grid
var mStartItemsGrid, mLengthItemsGrid;
var searchViewModel = {};

$(document).ready(function () {

    $('.input-group.date').datepicker({ });
    $("#TotalExpns").prop('disabled', true);
    $("#TotalTax").prop('disabled', true);
    $('#alertRates').hide();
    $('#alertItems').hide();
    $("#UpdateCorrectly").hide();
    $("#LoadSearchModel").hide();
    LastDocDate = $("#DocDate").datepicker("getDate");

    switch (formMode) {
        case "Update":
            $("#btnCopy").hide();
            break;
        case "Add":
            break;
        case "View":
            $("#btnOk").hide();
            $("#btnCopy").hide();
            break;

    }

    /* Obtengo los rates y setteo el precio de las lineas (Por primera vez en el documento)*/
    updateRates();
    /*Cuando cambia el DocDate actualizo los rates para ese dia*/
    $("#DocDate").change(function () {
        if ((LastDocDate.getTime() != $("#DocDate").datepicker("getDate").getTime()) && (!(isNaN($("#DocDate").datepicker("getDate"))))) {
            LastDocDate = $("#DocDate").datepicker("getDate");
            updateRates();
        }
    });

    $("#LoadRequester").hide();

    $('#RequesterModal').on('show.bs.modal', function (e) {
        $("#ModalBodyRequester").html("");
    });

    $("#SearchRequester").click(function () {

        var mRequesterCode;
        var mRequesterName;

        if ($('#ckCodeRequester').is(':checked')) {
            mRequesterCode = $('#txtSearchRequester').val();
        }

        if ($('#ckNameRequester').is(':checked')) {
            mRequesterName = $('#txtSearchRequester').val();
        }

        $("#LoadRequester").show('slow');

        $.ajax({
            url: '/Purchase/PurchaseRequest/_Requesters',
            contextType: 'application/html;charset=utf-8',
            data: { pUserType: $('#ReqType').val(), pRequesterCode: mRequesterCode, pRequesterName: mRequesterName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadRequester").hide();
            $("#ModalBodyRequester").html(data);


        }).fail(function () {
            alert("error");
        });

    });


    $("#formPurchaseOrder").validate({
        rules: {
            txtRequester: "required",
            DocDate: "required",
            DocDueDate: "required",
            TaxDate: "required",
            ReqDate: "required"
        },
        messages: {
            txtRequester: "Please select the Vendor",
            DocDate: "Please select the Posting Date",
            DocDueDate: "Please select the Delivery Date",
            TaxDate: "Please select the Document Date",
            ReqDate: "Please select the Required Date"
        },
        errorPlacement: function (error, element) {
            $('#Loading').modal('hide');
            if (element.attr("name") == "fname" || element.attr("name") == "lname") {
                error.insertAfter("#lastname");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                case "ApprovePR":
                    ApprovePR();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    if (formMode != "Add")
    {
        $("#ReqType").attr("disabled", "disabled");
    }

    $("#txtRequesterName").attr("disabled", "disabled");


    $("#SearchItems").click(function () {

        if (typeof assetListVM_ListItems.dt === 'undefined' || assetListVM_ListItems.dt == null) {
            assetListVM_ListItems.init();
            $("#btnChoose").prop('disabled', true);
            updateLinesChanged(true);
        }
        else {
            assetListVM_ListItems.refresh();
            $("#btnChoose").prop('disabled', true);
            updateLinesChanged(true);
        }
        pSelectedItems = [];

    });

    $("#SearchItemsGrid").click(function () {

        var mListSearchUDF = $("input[id^=SearchItemGridUDF_]");
        var mListSearchUDFSelect = $("select[id^=SearchItemGridUDF_]");
        var mMappedUDFList = [];
        for (i = 0; i < mListSearchUDF.length; i++) {
            mMappedUDFList.push({
                UDFName: mListSearchUDF[i].id.substring(18),
                Value: $("#" + mListSearchUDF[i].id).val(),
            });
        }
        for (i = 0; i < mListSearchUDFSelect.length; i++) {
            mMappedUDFList.push({
                UDFName: mListSearchUDFSelect[i].id.substring(18),
                Value: $("#" + mListSearchUDFSelect[i].id).val(),
            });
        }

        searchViewModel = {};
        searchViewModel.pMappedUdf = mMappedUDFList;
        searchViewModel.pPageKey = $('#Pagekey').val();
        searchViewModel.pItemCode = $("#UDFGrid_Code").val();
        searchViewModel.pItemData = $("#UDFGrid_Name").val();
        mStartItemsGrid = 0;
        GetItemsGrid();

    });

    //Inicio Matrix 

    $("#SearchModel").click(function () {
        mStartItemsGrid = 0;
        GetModelsList();
    });

    function GetModelsList() {
        $("#LoadSearchModel").show('slow');
        $('#Loading').modal({
            backdrop: 'static'
        });

        var mModelCode;
        var mModelName;

        if ($('#ckCodeModel').is(':checked')) {
            mModelCode = $('#txtSearchModel').val();
        }

        if ($('#ckNameModel').is(':checked')) {
            mModelName = $('#txtSearchModel').val();
        }

        searchViewModel = {};
        searchViewModel.CodeModel = mModelCode;
        searchViewModel.NameModel = mModelName;

        mLengthItemsGrid = ($("#PageQtyModelList").val() == undefined ? 10 : Number($("#PageQtyModelList").val()));
        mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

        searchViewModel.Start = mStartItemsGrid;
        searchViewModel.Length = mLengthItemsGrid;

        $("#LoadGrid").show('slow');

        $.ajax({
            url: '/Purchase/PurchaseRequest/_ModelListSearch',
            contextType: 'application/html;charset=utf-8',
            data: { searchViewModel },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadGrid").hide();
            $("#BodyPDMGrid").html(data);
            $("#btnChooseGrid").prop('disabled', true);

            $("#modelListSearch").html(data);
            $("#ModelMatrixAddButton").prop('disabled', true);
            $("#LoadSearchModel").hide();
            updateLinesChanged(true);

            $('#Loading').modal('hide');

            if (mStartItemsGrid <= 0) {
                $("#PreviousModelList").attr('disabled', 'disabled');
            }
            else {
                $("#PreviousModelList").removeAttr('disabled');;
            }

            if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityModelList").val()) {
                $("#NextModelList").attr('disabled', 'disabled');
            }
            else {
                $("#NextModelList").removeAttr('disabled');;
            }
        })
    };

    $("#PDMList").bind("click", function () {

        $('#products .item').addClass('list-group-item');
    });

    $("#PDMGrid").bind("click", function () {
        $(this).addClass("active").siblings().removeClass("active");
        $('#products .item').removeClass('list-group-item'); $('#products .item').addClass('grid-group-item');
    });

    $('#PDMGrid').addClass("active");

    $(".btn-group > .btn").click(function () {
        $(this).addClass("active").siblings().removeClass("active");
    });


    //Fin Matrix 

    $("#Load").hide();
    $("#LoadGrid").hide();

    $('#myModal').on('show.bs.modal', function (e) {
        $("#ModalBody").html("");
    });

    $('#myModalGrid').on('show.bs.modal', function (e) {
        $("#ModalGridBody").html("");
    });

    $("#LoadRequester").hide();

    $('#RequesterModal').on('show.bs.modal', function (e) {
        $("#ModalBodyRequester").html("");
    });   

    $('#myModalModel').on('show.bs.modal', function (e) {
        $("#modelListSearch").html("");
    });

    $("#btnChoose").click(function () {
        var mListItems = pSelectedItems;
        var pReqDate;

        $('#UDF_Code').val('');
        $('#UDF_Name').val('');

        if (isNaN($("#ReqDate").datepicker("getDate")))
        {
            pReqDate == null;
        }
        else
        {
            var dateReqDate = $("#ReqDate").datepicker("getDate");
            pReqDate = (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear());
        }

        if (mListItems.length == 0) {
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/Purchase/PurchaseRequest/_ItemsForm',
            traditional: true,
            contextType: 'application/html;charset=utf-8',
            data: { pItems: mListItems, pReqDate: pReqDate, pPageKey: $('#Pagekey').val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#myModal').modal('hide');
            $("#ItemsSelect").html(data);

            var TotalDoc = 0;
            var mIntRow = 0;


            $("#TableItemsForm :input[type=hidden]").each(function () {
                var input = $(this);

                var value = parseFloat($("#td" + input.attr('id')).html());

                TotalDoc = TotalDoc + value;

                mIntRow++;
            });

            $("#TotalDoc").val(TotalDoc);
            $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));

            $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));
            $("#TotalDocPD").val(parseFloat($("#TotalDocPD").val()).toFixed(2));
            $("#TotalDocPD").change();

            if (mIntRow > 0) {
                $('#alertItems').hide();
            }

            $('.input-group.date').datepicker({

            });

            $('#Loading').modal('hide');

            pSelectedItems = [];
        });


    });

    $("#btnChooseGrid").click(function () {
        var mListItems = [];
        $('#UDFGrid_Code').val('');
        $('#UDFGrid_Name').val('');

        if (isNaN($("#ReqDate").datepicker("getDate"))) {
            pReqDate == null;
        }
        else {
            var dateReqDate = $("#ReqDate").datepicker("getDate");
            pReqDate = (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear());
        }
        $("#TableItemsGrid").find("input:checked").each(function (i, ob) {
            mListItems.push($(ob).val());
        });

        if (mListItems.length == 0) {
            return;
        }

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/Purchase/PurchaseRequest/_ItemsForm',
            traditional: true,
            contextType: 'application/html;charset=utf-8',
            data: { pItems: mListItems, pReqDate: pReqDate, pPageKey: $('#Pagekey').val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#myModalGrid').modal('hide');
            $("#ItemsSelect").html(data);

            var TotalDoc = 0;
            var mIntRow = 0;


            $("#TableItemsForm :input[type=hidden]").each(function () {
                var input = $(this);

                var value = parseFloat($("#td" + input.attr('id')).html());

                TotalDoc = TotalDoc + value;

                mIntRow++;
            });

            $("#TotalDoc").val(TotalDoc);
            $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));

            $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));
            $("#TotalDocPD").val(parseFloat($("#TotalDocPD").val()).toFixed(2));
            $("#TotalDocPD").change();

            if (mIntRow > 0) {
                $('#alertItems').hide();
            }

            $('.input-group.date').datepicker({

            });

            $('#Loading').modal('hide');
        });

    });

    $("#btnOk").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var form = $("#formPurchaseOrder");
        form.validate();

        if (form.valid()) {
            setTimeout(function () {
                form.submit();
            }, 5000);

        }

    });

    //Cleanning of the array pSelectedItems when the modal is closed
    $('#myModal').on('hide.bs.modal', function (e) {
        pSelectedItems = [];
    });

    $.getScript("../../Scripts/ExternalJS/CustomizePR.js", function (data, textStatus, jqxhr) { });
});

function ChangeValue(Id) {
    
    var count = 0;
    var PriceBefDi = 0;
    var total = 0;
    if ($('#txtcount' + Id).val() != "") {
        count = $('#txtcount' + Id).val();
    }
    if ($('#txt' + Id).val() != "") {
        PriceBefDi = $('#txt' + Id).val();
    }
   
    $.grep(ListRates, function (v) {
        if (v.Currency.replace(/\s+/g, '') == $('#dp' + Id).val() && v.Local == false && v.System == false) {
            if (v.Rate == 0) {
                total = 0;
                $('#alertRates').show();
                $('#btnOk').attr("disabled", true);
            }
            else {
                total = (PriceBefDi * v.Rate) * count;
                $('#alertRates').hide();
                $('#btnOk').attr("disabled", false);
            }
        }
    });

    $("#td" + Id).html("");
    $("#td" + Id).append(total.toFixed(2));
    var TotalDoc = 0;
    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);
        var value = parseFloat($("#td" + input.attr('id')).html());
        TotalDoc = TotalDoc + value;
    });
    $("#TotalDoc").val(TotalDoc.toFixed(2));
    $("#TotalDocPD").val((Number($("#TotalDoc").val()) + Number($("#TotalTax").val()) + Number($("#TotalExpns").val())).toFixed(2));

    $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));
    $("#TotalDocPD").val(parseFloat($("#TotalDocPD").val()).toFixed(2));
    $("#TotalDocPD").change();
}

function Save() {
    $('#Loading').modal({ backdrop: 'static', keyboard: false, show: true });

    var mTotal = parseFloat($("#TotalDoc").val());
    var mTotalRow = 0;

    var dateDocDate = $("#DocDate").datepicker("getDate");
    var dateDocDueDate = $("#DocDueDate").datepicker("getDate");
    var dateTaxDate = $("#TaxDate").datepicker("getDate");
    var dateReqDate = $("#ReqDate").datepicker("getDate");

    var PurchaseRequestView = {
        Requester: $("#txtRequesterCode").val(),
        ReqType: $("#ReqType").val(),
        Branch: $("#selectBranch").val(),
        Department: $("#selectDepartment").val(),
        DocEntry: $("#DocEntry").val(),
        DocNum: $("#DocNum").val(),
        DocDate: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
        DocDueDate: (isNaN(dateDocDueDate) == true ? null : (dateDocDueDate.getMonth() + 1) + "/" + dateDocDueDate.getDate() + "/" + dateDocDueDate.getFullYear()),
        TaxDate: (isNaN(dateTaxDate) == true ? null : (dateTaxDate.getMonth() + 1) + "/" + dateTaxDate.getDate() + "/" + dateTaxDate.getFullYear()),
        ReqDate: (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear()),
        Comments: $('#Comments').val(),
        PageKey: $('#Pagekey').val(),
        DocTotal: mTotal,
        ListItem: [],
        Lines: [],
        MappedUdf: []
    };

    var listUDFHEAD = $("input[id^=HEADUDF_]");
    var listUDFHEADSelect = $("select[id^=HEADUDF_]");
    for (i = 0; i < listUDFHEAD.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEAD[i].id.substring(8),
            Value: $("#" + listUDFHEAD[i].id).val(),
        });
    }
    for (i = 0; i < listUDFHEADSelect.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEADSelect[i].id.substring(8),
            Value: $("#" + listUDFHEADSelect[i].id).val(),
        });
    }

    //Obtengo los names de las lineas los meto en un array y luego hago un distinct para evitar los repetidos
    var listUDFLines = $("input[id^=LINEUDF_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(8))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(8))
    }
    linesUDFNames = unique(linesUDFNames);

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        var PriceBefDi = $("#txt" + input.attr('id')).val();
        var quantity = $("#txtcount" + input.attr('id')).val();
        var UomCode = $("#UOMAuto" + input.attr('id')).val();
        if ($("#AutoComplete" + input.attr('id')).val() != "") {
            whCode = $("#AutoComplete" + input.attr('id')).val();
        }
        else {
            whCode = null;
        }

        var DistRule = "";

        if ($("#AutoCompleteDR" + input.attr('id')).val() != "") {
            DistRule = $("#AutoCompleteDR" + input.attr('id')).val();
        }
        else {
            DistRule = null;
        }

        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }
        var dateReqDate = $("#ReqDate" + input.attr('id')).datepicker("getDate");
        PurchaseRequestView.Lines.push({
            "ItemCode": input.attr('id'),
            "Dscription": $("#DescItem" + input.attr('id')).val(),
            "PQTReqDate": (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear()),
            "Quantity": parseFloat(quantity),
            "PriceBefDi": parseFloat(PriceBefDi),
            "Currency": $("#dp" + input.attr('id')).val(),
            "LineNum": input.attr('id'),
            "WhsCode": whCode,
            "UomCode": UomCode,
            "OcrCode": DistRule,
            "FreeTxt": $("#FreeTxt" + input.attr('id')).val(),
            "MappedUdf": MappedUdf,
            "GLAccount": { FormatCode: $("#AutoCompleteGL" + input.attr('id')).val() }
        });

        mTotalRow++;

    });

    if (mTotalRow == 0) {
        $('#alertItems').show();
        $('#Loading').modal('hide');
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return;
    }

    $.ajax({
        url: "/Purchase/PurchaseRequest/Add",
        async: false,
        type: "POST",
        data: JSON.stringify(PurchaseRequestView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                location.href = urlRedirect;
            }
            else {
                $('#Loading').modal('hide');
                ErrorPR(data);
            }
        }
    });

}

function Update() {
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    var mTotal = parseFloat($("#TotalDoc").val());
    var mTotalRow = 0;

    var dateDocDate = $("#DocDate").datepicker("getDate");
    var dateDocDueDate = $("#DocDueDate").datepicker("getDate");
    var dateTaxDate = $("#TaxDate").datepicker("getDate");
    var dateReqDate = $("#ReqDate").datepicker("getDate");

    var PurchaseRequestView = {
        Requester: $("#txtRequesterCode").val(),
        ReqType: $("#ReqType").val(),
        Branch: $("#selectBranch").val(),
        Department: $("#selectDepartment").val(),
        DocEntry: $("#DocEntry").val(),
        DocNum: $("#DocNum").val(),
        DocDate: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
        DocDueDate: (isNaN(dateDocDueDate) == true ? null : (dateDocDueDate.getMonth() + 1) + "/" + dateDocDueDate.getDate() + "/" + dateDocDueDate.getFullYear()),
        TaxDate: (isNaN(dateTaxDate) == true ? null : (dateTaxDate.getMonth() + 1) + "/" + dateTaxDate.getDate() + "/" + dateTaxDate.getFullYear()),
        ReqDate: (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear()),
        Comments: $('#Comments').val(),
        PageKey: $('#Pagekey').val(),
        DocTotal: mTotal,
        ListItem: [],
        Lines: [],
        MappedUdf: []
    };

    var listUDFHEAD = $("input[id^=HEADUDF_]");
    var listUDFHEADSelect = $("select[id^=HEADUDF_]");
    for (i = 0; i < listUDFHEAD.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEAD[i].id.substring(8),
            Value: $("#" + listUDFHEAD[i].id).val(),
        });
    }
    for (i = 0; i < listUDFHEADSelect.length; i++) {
        PurchaseOrderView.MappedUdf.push({
            UDFName: listUDFHEADSelect[i].id.substring(8),
            Value: $("#" + listUDFHEADSelect[i].id).val(),
        });
    }

    //Obtengo los names de las lineas los meto en un array y luego hago un distinct para evitar los repetidos
    var listUDFLines = $("input[id^=LINEUDF_]");
    var listUDFLinesSelect = $("select[id^=LINEUDF_]");
    var linesUDFNames = [];
    for (i = 0; i < listUDFLines.length; i++) {
        linesUDFNames.push(listUDFLines[i].name.substring(8))
    }
    for (i = 0; i < listUDFLinesSelect.length; i++) {
        linesUDFNames.push(listUDFLinesSelect[i].name.substring(8))
    }
    linesUDFNames = unique(linesUDFNames);

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        var PriceBefDi = $("#txt" + input.attr('id')).val();
        var quantity = $("#txtcount" + input.attr('id')).val();
        var UomCode = $("#UOMAuto" + input.attr('id')).val();
        if ($("#AutoComplete" + input.attr('id')).val() != "") {
            whCode = $("#AutoComplete" + input.attr('id')).val();
        }
        else {
            whCode = null;
        }

        var DistRule = "";

        if ($("#AutoCompleteDR" + input.attr('id')).val() != "") {
            DistRule = $("#AutoCompleteDR" + input.attr('id')).val();
        }
        else {
            DistRule = null;
        }

        var MappedUdf = [];
        for (i = 0; i < linesUDFNames.length; i++) {
            MappedUdf.push({
                UDFName: linesUDFNames[i],
                Value: $("#LINEUDF_" + linesUDFNames[i] + input.attr('id')).val(),
            });
        }
        var dateReqDate = $("#ReqDate" + input.attr('id')).datepicker("getDate");
        PurchaseRequestView.Lines.push({
            "ItemCode": input.attr('id'),
            "Dscription": $("#DescItem" + input.attr('id')).val(),
            "PQTReqDate": (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear()),
            "Quantity": parseFloat(quantity),
            "PriceBefDi": parseFloat(PriceBefDi),
            "Currency": $("#dp" + input.attr('id')).val(),
            "LineNum": input.attr('id'),
            "WhsCode": whCode,
            "UomCode": UomCode,
            "OcrCode": DistRule,
            "FreeTxt": $("#FreeTxt" + input.attr('id')).val(),
            "MappedUdf": MappedUdf,
            "GLAccount": { FormatCode: $("#AutoCompleteGL" + input.attr('id')).val() }
        });

        mTotalRow++;

    });

    if (mTotalRow == 0) {
        $('#alertItems').show();
        return;
    }

    $.ajax({
        url: "/Purchase/PurchaseRequest/Update",
        async: false,
        type: "POST",
        data: JSON.stringify(PurchaseRequestView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorPR(data);
            }
        }
    });
}


function ApprovePR() {
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    var mTotal = parseFloat($("#TotalDoc").val());
    var mTotalRow = 0;

    var dateDocDate = $("#DocDate").datepicker("getDate");
    var dateDocDueDate = $("#DocDueDate").datepicker("getDate");
    var dateTaxDate = $("#TaxDate").datepicker("getDate");
    var dateReqDate = $("#ReqDate").datepicker("getDate");

    var PurchaseRequestView = {
        Requester: $("#txtRequesterCode").val(),
        ReqType: $("#ReqType").val(),
        Branch: $("#selectBranch").val(),
        Department: $("#selectDepartment").val(),
        DocEntry: $("#DocEntry").val(),
        DocNum: $("#DocNum").val(),
        Comments: $('#Comments').val(),
        DocDate: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()),
        DocDueDate: (isNaN(dateDocDueDate) == true ? null : (dateDocDueDate.getMonth() + 1) + "/" + dateDocDueDate.getDate() + "/" + dateDocDueDate.getFullYear()),
        TaxDate: (isNaN(dateTaxDate) == true ? null : (dateTaxDate.getMonth() + 1) + "/" + dateTaxDate.getDate() + "/" + dateTaxDate.getFullYear()),
        ReqDate: (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear()),
        DocTotal: mTotal,
        ListItem: [],
        Lines: []
    };

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        var PriceBefDi = $("#txt" + input.attr('id')).val();
        var quantity = $("#txtcount" + input.attr('id')).val();
        if ($("#AutoComplete" + input.attr('id')).val() != "") {
            whCode = $("#AutoComplete" + input.attr('id')).val();
        }
        else {
            whCode = null;
        }

        var DistRule = "";

        if ($("#AutoCompleteDR" + input.attr('id')).val() != "") {
            DistRule = $("#AutoCompleteDR" + input.attr('id')).val();
        }
        else {
            DistRule = null;
        }

        PurchaseRequestView.Lines.push({
            "ItemCode": input.attr('id'),
            "Dscription": $("#DescItem" + input.attr('id')).val(),
            "PQTReqDate": $("#ReqDate" + input.attr('id')).val(),
            "Quantity": parseFloat(quantity),
            "PriceBefDi": parseFloat(PriceBefDi),
            "Currency": $("#dp" + input.attr('id')).val(),
            "LineNum": input.attr('id'),
            "WhsCode": whCode,
            "OcrCode": DistRule,
            "FreeTxt": $("#FreeTxt" + input.attr('id')).val(),
            "GLAccount": { FormatCode: $("#AutoCompleteGL" + input.attr('id')).val() }
        });

        mTotalRow++;

    });

    if (mTotalRow == 0) {
        $('#alertItems').show();
        return;
    }

    $.ajax({
        url: "/Purchase/PurchaseRequest/SaveDraftToDocument",
        async: false,
        type: "POST",
        data: JSON.stringify(PurchaseRequestView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                location.href = "/Purchase/PurchaseRequest/Index";
            }
            else {
                $('#Loading').modal('hide');
                ErrorPR(data);
            }
        }
    });
}


function DeleteRow(Id) {

    $.post("/Purchase/PurchaseRequest/DeleteRow", {
        ItemCode: Id,
        pPageKey: $('#Pagekey').val()
    })
       .success(function (data) {
           if (data == "Ok") {
           //Deleting items from  ListLinesChanged array who contains the deleted row id.
               ListLinesChanged = unique(ListLinesChanged);
               var index = ListLinesChanged.indexOf(Id);
               if (index > -1) {
                   ListLinesChanged.splice(index, 1);
               }
               $("#tr" + Id).remove();
           }
           else {
               ErrorPO(data);
           }
       });

}

function updateRates() {
    var dateDocDate = $("#DocDate").datepicker("getDate");
    var mRow = 0;
    //Obtengo los rates para el dia del DocDate
    $.ajax({
        url: '/Purchase/PurchaseRequest/UpdateRateList',
        data: { DocDate: (isNaN(dateDocDate) == true ? null : (dateDocDate.getMonth() + 1) + "/" + dateDocDate.getDate() + "/" + dateDocDate.getFullYear()), pPageKey: $('#Pagekey').val() },
        type: 'POST',
        dataType: 'json'
    }).done(function (data) {
        ListRates = data;
        $("#TableItemsForm :input[type=hidden]").each(function () {
            var input = $(this); // This is the jquery object of the input, do what you will
            ChangeValue(input.attr('id'));

            mRow++;
        });

        if (mRow == 0) {
            $("#TotalDoc").val(0);
            $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));
        }

        
    });
}

function SetRequester(pCode, pName) {

    $('#txtRequesterCode').val(pCode);
    $('#txtRequesterName').val(pName);
    $('#RequesterModal').modal('hide');
    $("#HandHeldField").val("");
    $("#HandHeldField").focus();
}

function ErrorPR(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

//Inicio Matrix 
function ShowModel(pCode) {

    $('#Loading').modal({
        backdrop: 'static'
    });

    $.ajax({
        url: '/Purchase/PurchaseRequest/_ModelDetail',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $('#Pagekey').val(), pModelCode: pCode },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $("#MatrixDetail").html(data);

        $('#Loading').modal('hide');

        $('#myModal2').modal({
            backdrop: 'static',
            keyboard: true
        });
    });

}

function PasteItems() {
    var mItemsQty = [];
    var mListItems = [];
    var mTypeCurrency = "";
    var mQty = "";


    $('.MatrixItems').each(function () {

        mQty = $(this).val();

        if (mQty != "" && parseFloat(mQty) > 0) {
            mItemsQty.push({ Code: $(this).attr("name"), Qty: $(this).val() });
            mListItems.push($(this).attr("name"));
        }
    })

    if (mItemsQty.length == 0) {
        return;
    }

    switch ($("#TypeCurrency").val()) {
        case "C":
            $("#txtCurrencySg").show();
            mTypeCurrency = $("#txtCurrencySg").val();
            break;
        case "L":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#LocalCurrency").val();
            break;
        case "S":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#SystemCurrency").val();
            break;
    }

    var pReqDate;
    if (isNaN($("#ReqDate").datepicker("getDate"))) {
        pReqDate == null;
    }
    else {
        var dateReqDate = $("#ReqDate").datepicker("getDate");
        pReqDate = (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear());
    }

    $.ajax({
        url: '/Purchase/PurchaseRequest/_ItemsFormMatrixModel',
        traditional: true,
        contextType: 'application/html;charset=utf-8',
        data: { pItems: mListItems, pCurrency: mTypeCurrency, pItemsQty: JSON.stringify(mItemsQty), pReqDate: pReqDate, pPageKey: $('#Pagekey').val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $('#myModal').modal('hide');
        $("#ItemsSelect").html(data);

        var TotalDoc = 0;
        var Disc = 0;
        var mIntRow = 0;

        if ($("#TotalDisc").val() != "") {
            Disc = $("#TotalDisc").val();
        }

        $("#TableItemsForm :input[type=hidden]").each(function () {
            var input = $(this);

            var value = parseFloat($("#td" + input.attr('id')).html());

            TotalDoc = TotalDoc + value;

            mIntRow++;
        });

        $("#TotalDoc").val(TotalDoc);
        $("#TotalDocPD").val(TotalDoc - Disc);

        $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));
        $("#TotalDocPD").val(parseFloat($("#TotalDocPD").val()).toFixed(2));

        if (mIntRow > 0) {
            $('#alertItems').hide();
        }

        $('#myModal2').modal('hide');
        $("#RightLateralMessage").show("slow");
        $("#RightLateralMessage").delay(2000).slideUp(1000);
    });
}

//Fin Matrix 

$(document).keypress(function (e) {
    if (e.which == 13 && $("#HandHeldField").is(':focus'))
    {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });
        var pReqDate;
        if (isNaN($("#ReqDate").datepicker("getDate"))) {
            pReqDate == null;
        }
        else {
            var dateReqDate = $("#ReqDate").datepicker("getDate");
            pReqDate = (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear());
        }
        updateLinesChanged(false);
        $.ajax({
            url: '/Purchase/PurchaseRequest/_GetHandheldItem',
            contextType: 'application/html;charset=utf-8',
            data: { pReqDate: pReqDate, pPageKey: $('#Pagekey').val(), pItemCode: $("#HandHeldField").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $('#Loading').modal('hide');
            $("#HandHeldField").val("");
            $("#HandHeldField").focus();
            if (data != "") {
                $("#ItemsSelect").html(data);
                var TotalDoc = 0;
                var mIntRow = 0;
                
                $("#TableItemsForm :input[type=hidden]").each(function () {
                    var input = $(this);

                    var value = parseFloat($("#td" + input.attr('id')).html());

                    TotalDoc = TotalDoc + value;

                    mIntRow++;
                });

                $("#TotalDoc").val(TotalDoc);
                $("#TotalDoc").val(parseFloat($("#TotalDoc").val()).toFixed(2));
                

                if (mIntRow > 0) {
                    $('#alertItems').hide();
                }

                $('.input-group.date').datepicker({

                });   
            }
        });
    }
});

function ErrorPO(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function LineWasChanged(lineNum) {
    ListLinesChanged.push(lineNum);
}

function ChangeQuantityValue(lineNum, message) {
    if ($("#txtcount" + lineNum).val() < 0 || $("#txtcount" + lineNum).val().trim() == "") {
        ErrorPO(message);
        $("#txtcount" + lineNum).val(0);
    }
}

function updateLinesChanged(sendAsync) {
    ListLinesChanged = unique(ListLinesChanged);
    if (ListLinesChanged.length > 0) {
        var Lines = [];

        //I Obtain the names of the lines udf and y put it into an array, then i apply a distinct to the array to avoid repeated udfs
        var listUDFLines = $("input[id^=LINEUDF_]");
        var listUDFLinesSelect = $("select[id^=LINEUDF_]");
        var linesUDFNames = [];
        for (i = 0; i < listUDFLines.length; i++) {
            linesUDFNames.push(listUDFLines[i].name.substring(8))
        }
        for (i = 0; i < listUDFLinesSelect.length; i++) {
            linesUDFNames.push(listUDFLinesSelect[i].name.substring(8))
        }
        linesUDFNames = unique(linesUDFNames);

        for (i = 0; i < ListLinesChanged.length; i++) {
            var price = $("#txt" + ListLinesChanged[i]).val();
            var quantity = $("#txtcount" + ListLinesChanged[i]).val();
            var whCode = "";

            if ($("#AutoComplete" + ListLinesChanged[i]).val() != "") {
                whCode = $("#AutoComplete" + ListLinesChanged[i]).val();
            }
            else {
                whCode = null;
            }

            var DistRule = "";

            if ($("#AutoCompleteDR" + ListLinesChanged[i]).val() != "") {
                DistRule = $("#AutoCompleteDR" + ListLinesChanged[i]).val();
            }
            else {
                DistRule = null;
            }

            var UOM = "";
            if ($("#UOMAuto" + ListLinesChanged[i]).val() != "") {
                UOM = $("#UOMAuto" + ListLinesChanged[i]).val();
            }
            else {
                UOM = null;
            }

            var dateReqDate = $("#ReqDate" + ListLinesChanged[i]).datepicker("getDate");

            var MappedUdf = [];
            for (j = 0; j < linesUDFNames.length; j++) {
                MappedUdf.push({
                    UDFName: linesUDFNames[j],
                    Value: $("#LINEUDF_" + linesUDFNames[j] + ListLinesChanged[i]).val(),
                });
            }

            Lines.push({
                "ItemCode": ListLinesChanged[i],
                "Dscription": $("#DescItem" + ListLinesChanged[i]).val(),
                "Quantity": parseFloat(quantity),
                "Price": parseFloat(price),
                "PriceBefDi": parseFloat(price),
                "Currency": $("#dp" + ListLinesChanged[i]).val(),
                "LineNum": ListLinesChanged[i],
                "WhsCode": whCode,
                "OcrCode": DistRule,
                "UomCode": UOM,
                "FreeTxt": $("#FreeTxt" + ListLinesChanged[i]).val(),
                "GLAccount": { FormatCode: $("#AutoCompleteGL" + ListLinesChanged[i]).val() },
                "PQTReqDate": (isNaN(dateReqDate) == true ? null : (dateReqDate.getMonth() + 1) + "/" + dateReqDate.getDate() + "/" + dateReqDate.getFullYear()),
                "MappedUdf": MappedUdf
            });
        }

        $.ajax({
            url: "/Purchase/PurchaseRequest/_UpdateLinesChanged?pPageKey=" + $('#Pagekey').val(),
            async: sendAsync,
            type: "POST",
            data: JSON.stringify(Lines),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, jqXHR) {
                $("#btnChoose").prop('disabled', false);
                $("#btnChooseGrid").prop('disabled', false);
                $("#ModelMatrixAddButton").prop('disabled', false);
                ListLinesChanged = [];
            }
        });

    }
    else
    {
        $("#btnChoose").prop('disabled', false);
        $("#btnChooseGrid").prop('disabled', false);
        $("#ModelMatrixAddButton").prop('disabled', false);
    }

}

function AddOrRemoveSelectedItem(ItemCode, pChecked, pCheckBoxId) {
    var mChecked = $("input[id='" + pCheckBoxId + "']").is(':checked');
    if (mChecked == true) {
        pSelectedItems.push(ItemCode);
        $("input[id='" + pCheckBoxId + "']").parent().parent().addClass('selected');
    }
    else {
        var index = pSelectedItems.indexOf(ItemCode);
        if (index > -1) {
            pSelectedItems.splice(index, 1);
            $("input[id='" + pCheckBoxId + "']").parent().parent().removeClass('selected');
        }
    }
}

function GetItemsModel() {
    $.ajax({
        url: '/Purchase/PurchaseRequest/GetItemsModel',
        contextType: 'application/html;charset=utf-8',
        data: {},
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#ModalItemsTable").html(data);
    });
}

function unique(array) {
    return $.grep(array, function (el, index) {
        return index == $.inArray(el, array);
    });
}

function GetItemsGrid() {
    mLengthItemsGrid = ($("#PageQtyItemsGrid").val() == undefined ? 10 : Number($("#PageQtyItemsGrid").val()));
    mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

    $("#LoadGrid").show('slow');

    $.ajax({
        url: '/Purchase/PurchaseRequest/_ItemsGrid',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: searchViewModel.pPageKey, pMappedUdf: searchViewModel.pMappedUdf, pItemCode: searchViewModel.pItemCode, pItemData: searchViewModel.pItemData, pStart: mStartItemsGrid, pLength: mLengthItemsGrid },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $("#LoadGrid").hide();
        $("#ModalGridBody").html(data);
        $("#btnChooseGrid").prop('disabled', true);

        if (mStartItemsGrid <= 0) {
            $("#PreviousItemsGrid").attr('disabled', 'disabled');
        }
        else {
            $("#PreviousItemsGrid").removeAttr('disabled');;
        }

        if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityItemsGrid").val()) {
            $("#NextItemsGrid").attr('disabled', 'disabled');
        }
        else {
            $("#NextItemsGrid").removeAttr('disabled');;
        }

        updateLinesChanged(true);
    });

}

function GetNextModelList() {
    mStartItemsGrid = mStartItemsGrid + Number($('#PageQtyModelList').val());
    GetModelsList();
};

function GetPreviousModelList() {
    mStartItemsGrid = mStartItemsGrid - Number($('#PageQtyModelList').val());
    GetModelsList();
};

$("#SearchModel").click(function () {
    mStartItemsGrid = 0;
    GetModelsList();
});

function GetModelsList() {
    $("#LoadSearchModel").show('slow');
    $('#Loading').modal({
        backdrop: 'static'
    });

    var mModelCode;
    var mModelName;

    if ($('#ckCodeModel').is(':checked')) {
        mModelCode = $('#txtSearchModel').val();
    }

    if ($('#ckNameModel').is(':checked')) {
        mModelName = $('#txtSearchModel').val();
    }

    searchViewModel = {};
    searchViewModel.CodeModel = mModelCode;
    searchViewModel.NameModel = mModelName;

    mLengthItemsGrid = ($("#PageQtyModelList").val() == undefined ? 10 : Number($("#PageQtyModelList").val()));
    mStartItemsGrid = (mStartItemsGrid <= 0 ? 0 : mStartItemsGrid);

    searchViewModel.Start = mStartItemsGrid;
    searchViewModel.Length = mLengthItemsGrid;

    $("#LoadGrid").show('slow');

    $.ajax({
        url: '/Purchase/PurchaseRequest/_ModelListSearch',
        contextType: 'application/html;charset=utf-8',
        data: { searchViewModel },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {

        $("#LoadGrid").hide();
        //$("#BodyPDMGrid").html(data);
        $("#btnChooseGrid").prop('disabled', true);

        $("#modelListSearch").html(data);
        $("#ModelMatrixAddButton").prop('disabled', true);
        $("#LoadSearchModel").hide();
        updateLinesChanged(true);

        $('#Loading').modal('hide');

        if (mStartItemsGrid <= 0) {
            $("#PreviousModelList").attr('disabled', 'disabled');
        }
        else {
            $("#PreviousModelList").removeAttr('disabled');;
        }

        if (mStartItemsGrid + mLengthItemsGrid > $("#TotalQuantityModelList").val()) {
            $("#NextModelList").attr('disabled', 'disabled');
        }
        else {
            $("#NextModelList").removeAttr('disabled');;
        }
    });

};