﻿$(document).ready(function () {
    jQuery('#txtDocNum').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    if (window.history.state != null) {

        $("#BodyPR").html(window.history.state);

    }

    $('.input-group.date').datepicker({ });

    $("#txtRequesterName").prop('disabled', true);

    $("#LoadRequester").hide();
    $("#LoadSE").hide();
    $("#LoadEmployee").hide();

    $('#RequesterModal').on('show.bs.modal', function (e) {
        $("#ModalBodyRequester").html("");
    });
    $('#SEModal').on('show.bs.modal', function (e) {
        $("#ModalBodySE").html("");
    });
    $('#EmployeeModal').on('show.bs.modal', function (e) {
        $("#ModalBodyEmployee").html("");
    });

    $("#SearchRequester").click(function () {

        var mRequesterCode;
        var mRequesterName;

        if ($('#ckCodeRequester').is(':checked')) {
            mRequesterCode = $('#txtSearchRequester').val();
        }

        if ($('#ckNameRequester').is(':checked')) {
            mRequesterName = $('#txtSearchRequester').val();
        }

        $("#LoadRequester").show('slow');

        $.ajax({
            url: '/Purchase/PurchaseRequest/_Requesters',
            contextType: 'application/html;charset=utf-8',
            data: { pUserType: $('#ReqType').val(), pRequesterCode: mRequesterCode, pRequesterName: mRequesterName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadRequester").hide('slow');
            $("#ModalBodyRequester").html(data);


        }).fail(function () {
            alert("error");
        }).always(function () {

        });

    });

    $("#SearchEmployee").click(function () {

        var mEmployeeCode;
        var mEmployeeName;

        if ($('#ckCodeEmployee').is(':checked')) {
            mEmployeeCode = $('#txtSearchEmployee').val();
        }

        if ($('#ckNameEmployee').is(':checked')) {
            mEmployeeName = $('#txtSearchEmployee').val();
        }
        $("#LoadEmployee").show('slow');
        $("#ModalBodyEmployee").show('slow');

        $.ajax({
            url: '/Purchase/PurchaseRequest/_Employee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeCode: mEmployeeCode, pEmployeeName: mEmployeeName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadEmployee").hide('slow');
            $("#ModalBodyEmployee").html(data);
        });

    });

    $("#SearchSE").click(function () {
        var mSEName;
        mSEName = $('#txtSE').val();

        $("#LoadSE").show('slow');

        $.ajax({
            url: '/Purchase/PurchaseRequest/_GetSalesEmployee',
            contextType: 'application/html;charset=utf-8',
            data: { pEmployeeName: mSEName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadSE").hide('slow');
            $("#ModalBodySE").html(data);


        }).fail(function () {
            alert("error");
        }).always(function () {

        });

    });

    $("#btSearch").click(function () {
        if (typeof dt === 'undefined') {
            assetListVM.init();
        }
        else {
            assetListVM.refresh();
        }
    });

});

function SetRequester(pCode, pName) {

    var mName = pName.replace("##", "'");

    $('#txtRequesterCode').val(pCode);
    $('#txtRequesterName').val(mName);
    $('#RequesterModal').modal('hide');
}

function SetOwner(pCode, pName) {
    $('#OwnerCode').val(pCode);
    $('#OwnerName').val(pName);
    $('#EmployeeModal').modal('hide');
}

function SetSE(pCode, pName) {

    $('#SalesEmployeeNumber').val(pCode);
    $('#txtSalesEmployee').val(pName);
    $('#SEModal').modal('hide');
}