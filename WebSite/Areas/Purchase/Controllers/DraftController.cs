﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.Attribute;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.Purchase.Controllers
{
    [CustomerAuthorize(Enums.Areas.Purchase)]
    public class DraftController : Controller
    {
        private DraftManagerView mDraftManagerView;
        private UserManagerView mUserManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;

        public DraftController()
        {
            mDraftManagerView = new DraftManagerView();
            mUserManagerView = new UserManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        }

        [CustomerAuthorize(Enums.Areas.Purchase, Enums.Pages.PurchaseDraft, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<DraftPortalView> listDC = new List<DraftPortalView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                if (System.Web.HttpRuntime.Cache["PurDraft" + mUser] != null)
                {
                    listDC = (List<DraftPortalView>)System.Web.HttpRuntime.Cache["PurDraft" + mUser];
                }

                return View(listDC);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        public ActionResult ListDraft([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, 
            AdvanceSearchMyPurchaseDocumentView searchViewModel)
        {
            Mapper.CreateMap<OrderColumn, Column>();
            Mapper.CreateMap<Column, OrderColumn>();
            Mapper.CreateMap<DraftPortalView, DraftPortal>();
            Mapper.CreateMap<DraftPortal, DraftPortalView>();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();
            DateTime? mDocDateFrom = null, mDocDateTo = null;
            DateTime? mReqDate = null;
            int? mDocNum = null;
            List<DraftPortalView> mListDraft = new List<DraftPortalView>();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (searchViewModel.pDocDateFrom != "")
            {
                mDocDateFrom = Convert.ToDateTime(searchViewModel.pDocDateFrom);
            }

            if (searchViewModel.pDocDateTo != "")
            {
                mDocDateTo = Convert.ToDateTime(searchViewModel.pDocDateTo);
            }

            if (searchViewModel.pReqDate != "")
            {
                mReqDate = Convert.ToDateTime(searchViewModel.pReqDate);
            }

            if (searchViewModel.pDocNum != "")
            {
                mDocNum = Convert.ToInt32(searchViewModel.pDocNum);
            }

            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                mJsonObjectResult = mDraftManagerView.GetPurchaseCustomerDraftListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).BPCode, mDocDateFrom, mDocDateTo, mReqDate, searchViewModel.pDocType, searchViewModel.pDocStatus, searchViewModel.pCodeVendor, mDocNum, requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                mListDraft = Mapper.Map<List<DraftPortalView>>(mJsonObjectResult.DraftPortalList);
            }
            if (((UserView)Session["UserLogin"]).IsSalesEmployee && ((UserView)Session["UserLogin"]).SECode != null)
            {
                mJsonObjectResult = mDraftManagerView.GetPurchaseSalesEmployeeDraftListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).SECode.Value, mDocDateFrom, mDocDateTo, mReqDate, searchViewModel.pDocType, searchViewModel.pDocStatus, searchViewModel.pCodeVendor, mDocNum, requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                mListDraft = Mapper.Map<List<DraftPortalView>>(mJsonObjectResult.DraftPortalList);
            }
            if (((UserView)Session["UserLogin"]).IsUser && ((UserView)Session["UserLogin"]).IdUser != null)
            {
                mJsonObjectResult = mDraftManagerView.GetPurchaseUserDraftListSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, mDocDateFrom, mDocDateTo, mReqDate, searchViewModel.pDocType, searchViewModel.pDocStatus, searchViewModel.pCodeVendor, mDocNum, requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                mListDraft = Mapper.Map<List<DraftPortalView>>(mJsonObjectResult.DraftPortalList);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mListDraft, 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), 
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DraftDocEntry"></param>
        public void CreateDocument(string DraftDocEntry)
        {
            mDraftManagerView.CreateDocument(((UserView)Session["UserLogin"]).CompanyConnect, 
                Convert.ToInt32(DraftDocEntry));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pDocEntry"></param>
        /// <param name="pObjType"></param>
        /// <returns></returns>
        public PartialViewResult _GetHistory(int pDocEntry, string pObjType)
        {
            List<DocumentConfirmationLineView> listReturn = mDraftManagerView.GetApprovalListByDocumentId(((UserView)Session["UserLogin"]).CompanyConnect, pDocEntry, pObjType);
            if(listReturn != null)
            { 
                List<UserSAP> userList = mUserManagerView.GetUserSapList(((UserView)Session["UserLogin"]).CompanyConnect);
                List<StageView> ListStage = mDraftManagerView.GetStageList(((UserView)Session["UserLogin"]).CompanyConnect);
                listReturn = listReturn.Select(c => { c.UserName = userList.Where(j => j.USERID == c.UserID).FirstOrDefault().U_NAME; c.StageName = ListStage.Where(k => k.WstCode == c.StepCode).FirstOrDefault().Name; return c; }).ToList();
            }
            return PartialView("_HistoryView", listReturn);
        }

        [HttpPost]
        public ActionResult _Vendors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor, true, 
                    searchViewModel.pCardCode, searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, ((UserView)Session["UserLogin"]).SECode,
                    ((UserView)Session["UserLogin"]).BPGroupId, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor, true, 
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, false, null, 
                    null, true, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }
    }
}