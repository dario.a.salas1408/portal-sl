﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Draft;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PurchaseOrders;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using ARGNS.Model.Implementations.View;
using System.Web.Caching;
using System.IO;
using DataTables.Mvc;
using ARGNS.Model.Implementations.Portal;
using ARGNS.View.AdvanceSearchViews;

namespace ARGNS.WebSite.Areas.Purchase.Controllers
{
    [CustomerAuthorize(Enums.Areas.Purchase)]
    public class PurchaseOrderController : Controller
    {
        private PurchaseOrderManagerView mPOManagerView;

        private PurchaseQuotationManagerView mPQManagerView;

        private PurchaseRequestManagerView mPRManagerView;

        private ItemMasterManagerView mItemMasterManagerView;

        private BusinessPartnerManagerView mBusinessPartnerManagerView;

        private DraftManagerView mDraftManagerView;

        private PDMManagerView mPDMManagerView;

        private UserManagerView mUserManagerView;

        private CompaniesManagerView mCompaniesManagerView;

        private CRPageMapManagerView mCRPageMapManagerView;

        public PurchaseOrderController()
        {
            mPOManagerView = new PurchaseOrderManagerView();
            mPQManagerView = new PurchaseQuotationManagerView();
            mPRManagerView = new PurchaseRequestManagerView();
            mItemMasterManagerView = new ItemMasterManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mDraftManagerView = new DraftManagerView();
            mPDMManagerView = new PDMManagerView();
            mUserManagerView = new UserManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();

            Mapper.CreateMap<DraftView, PurchaseOrderView>();
            Mapper.CreateMap<DraftLineView, PurchaseOrderLineView>();
            Mapper.CreateMap<PurchaseRequestView, PurchaseOrderView>();
            Mapper.CreateMap<PurchaseRequestLineView, PurchaseOrderLineView>();
            Mapper.CreateMap<PurchaseQuotationView, PurchaseOrderView>();
            Mapper.CreateMap<PurchaseQuotationLineView, PurchaseOrderLineView>();
            Mapper.CreateMap<UserUDFView, UDF_ARGNS>();
            Mapper.CreateMap<UDF_ARGNS, UserUDFView>();
        }

        [CustomerAuthorize(Enums.Areas.Purchase, Enums.Pages.PurchaseOrder, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
                List<PurchaseOrderView> ListPO = new List<PurchaseOrderView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                if (System.Web.HttpRuntime.Cache["POList" + mUser] != null)
                {
                    ListPO = (List<PurchaseOrderView>)System.Web.HttpRuntime.Cache["POList" + mUser];
                }

                return View(Tuple.Create(ListPO, ListBp));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult ActionPurchaseOrder(string ActionPurchaseOrder, int IdPO, string fromController)
        {
            PurchaseOrderView mPOView = null;
            PurchaseOrderSAP mPO = null;
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            TempData["ListItems" + mPageKey] = null;
            switch (fromController)
            {
                case "PurchaseOrder":
                    ViewBag.URLRedirect = "/Purchase/PurchaseOrder/Index";
                    ViewBag.controllerName = "PurchaseOrder";
                    break;
                case "PurchaseQuotation":
                    ViewBag.URLRedirect = "/Purchase/PurchaseQuotation/Index";
                    ViewBag.controllerName = "PurchaseQuotation";
                    break;
                case "PurchaseRequest":
                    ViewBag.URLRedirect = "/Purchase/PurchaseRequest/Index";
                    ViewBag.controllerName = "PurchaseRequest";
                    break;
                case "MyDocuments":
                    ViewBag.URLRedirect = "/Purchase/Draft/Index";
                    ViewBag.controllerName = "Draft";
                    break;
                case "MyApprovals":
                    ViewBag.URLRedirect = "/Purchase/Approval/Index";
                    ViewBag.controllerName = "Approval";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            switch (ActionPurchaseOrder)
            {
                case "Add":
                    ViewBag.Tite = PurchaseOrders.APO;
                    ViewBag.FormMode = ActionPurchaseOrder;
                    mPOView = mPOManagerView.GetPurchaseOrder(0, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPOView.MappedUdf = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OPOR");

                    mPOView.DocDate = System.DateTime.Now;
                    mPOView.DocDueDate = System.DateTime.Now;
                    mPOView.TaxDate = System.DateTime.Now;
                    mPOView.ReqDate = null;
                    mPOView.CancelDate = null;
                    if (((UserView)Session["UserLogin"]).IsSalesEmployee == true && ((UserView)Session["UserLogin"]).SECode != null)
                    {
                        mPOView.SlpCode = ((UserView)Session["UserLogin"]).SECode.Value;
                    }
                    TempData["ListItemsExit" + mPageKey] = null;
                    break;

                case "View":
                case "Update":
                    if (ActionPurchaseOrder == "View")
                        ViewBag.Tite = PurchaseOrders.VPO;
                    else
                        ViewBag.Tite = PurchaseOrders.UPO;
                    ViewBag.FormMode = ActionPurchaseOrder;

                    mPOView = mPOManagerView.GetPurchaseOrder(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);

                    List<StockModelView> mLIstStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, mPOView.Lines.Select(c => c.ItemCode).ToList());

                    mPOView.Lines = mPOView.Lines.Select(c => { c.ListCurrency = mPOView.ListDocumentSAPCombo.ListCurrency; c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); c.hasStock = (mLIstStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();

                    TempData["ListItemsExit" + mPageKey] = mPOView.Lines;

                    break;
                case "ViewDraft":
                    ViewBag.Tite = Drafts.POD;
                    ViewBag.FormMode = "View";
                    DraftView draft = mDraftManagerView.GetDraftById(IdPO, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPOView = new PurchaseOrderView();
                    mPOView = Mapper.Map<PurchaseOrderView>(draft);
                    mPOView.Lines = Mapper.Map<List<PurchaseOrderLineView>>(draft.Lines);

                    mPO = mPOManagerView.GetPOInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mPOView);
                    mPOView = Mapper.Map<PurchaseOrderView>(mPO);
                    mPOView.Lines = Mapper.Map<List<PurchaseOrderLineView>>(mPO.Lines);
                    //seteo el ocrcode = "" en caso de tenga null, para que luego en el items form no le ponga el warehouse por defecto dado que no es una linea nueva
                    mPOView.Lines = mPOView.Lines.Select(c => { c.ListCurrency = mPOView.ListDocumentSAPCombo.ListCurrency; c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); return c; }).ToList();
                    TempData["ListItemsExit" + mPageKey] = mPOView.Lines;
                    break;

                case "CopyFromPR":
                    ViewBag.Tite = PurchaseOrders.APO;
                    ViewBag.FormMode = "Add";
                    PurchaseRequestView mPRView = mPRManagerView.GetPurchaseRequest(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPOView = new PurchaseOrderView();
                    mPOView = Mapper.Map<PurchaseOrderView>(mPRView);
                    mPOView.Lines = Mapper.Map<List<PurchaseOrderLineView>>(mPRView.Lines);

                    mPO = mPOManagerView.GetPOInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mPOView);
                    mPOView = Mapper.Map<PurchaseOrderView>(mPO);
                    mPOView.Lines = Mapper.Map<List<PurchaseOrderLineView>>(mPO.Lines);

                    List<StockModelView> mLIstStockPR = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, mPOView.Lines.Select(c => c.ItemCode).ToList());

                    mPOView.Lines.Select(c => { c.ListCurrency = mPOView.ListDocumentSAPCombo.ListCurrency; c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); c.BaseType = 1470000113; c.BaseEntry = mPOView.DocEntry; c.BaseLine = c.LineNum; c.BaseRef = mPOView.DocNum.ToString(); c.hasStock = (mLIstStockPR.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();

                    //Actualizo estas 2 fechas a la fecha actual tal como hace SAP y setteo el DocNumber en 0 para evitar equivocaciones por parte del usuario
                    mPOView.DocDate = DateTime.Today;
                    mPOView.TaxDate = DateTime.Today;
                    mPOView.DocNum = 0;
                    TempData["ListItemsExit" + mPageKey] = mPOView.Lines;
                    break;

                case "CopyFromPQ":
                    ViewBag.Tite = PurchaseOrders.APO;
                    ViewBag.FormMode = "Add";
                    PurchaseQuotationView mPQView = mPQManagerView.GetPurchaseQuotation(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPOView = new PurchaseOrderView();
                    mPOView = Mapper.Map<PurchaseOrderView>(mPQView);
                    mPOView.POAddress = mPQView.PQAddress;
                    mPOView.Lines = Mapper.Map<List<PurchaseOrderLineView>>(mPQView.Lines);

                    mPO = mPOManagerView.GetPOInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mPOView);
                    mPOView = Mapper.Map<PurchaseOrderView>(mPO);
                    mPOView.Lines = Mapper.Map<List<PurchaseOrderLineView>>(mPO.Lines);

                    List<StockModelView> mLIstStockPQ = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, mPOView.Lines.Select(c => c.ItemCode).ToList());

                    mPOView.Lines.Select(c => { c.ListCurrency = mPOView.ListDocumentSAPCombo.ListCurrency; c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); c.BaseType = 540000006; c.BaseEntry = mPOView.DocEntry; c.BaseLine = c.LineNum; c.BaseRef = mPOView.DocNum.ToString(); c.hasStock = (mLIstStockPQ.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();

                    //Actualizo estas 2 fechas a la fecha actual tal como hace SAP y setteo el DocNumber en 0 para evitar equivocaciones por parte del usuario
                    mPOView.DocDate = DateTime.Today;
                    mPOView.TaxDate = DateTime.Today;
                    mPOView.DocNum = 0;
                    TempData["ListItemsExit" + mPageKey] = mPOView.Lines;
                    break;

            }
            
            Session["LocalCurrency"] = mPOView.LocalCurrency;
            Session["SystemCurrency"] = mPOView.SystemCurrency;
            Session["DistrRuleSAP"] = mPOView.DistrRuleSAP;
            Session["Warehouse"] = mPOView.Warehouse;
            Session["GLAccountSAP"] = mPOView.GLAccountSAP;

            TempData["Attachments" + mPageKey] = mPOView.AttachmentList;
            TempData["ListCurrency" + mPageKey] = mPOView.ListDocumentSAPCombo.ListCurrency;
            List<RatesSystem> ListRates = mPOManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mPOView.ListDocumentSAPCombo.ListCurrency, mPOView.DocDate, mPOView.LocalCurrency, mPOView.SystemCurrency);
            TempData["Rates" + mPageKey] = ListRates;
            mPOView.SystemCurrency = ListRates.Where(c => c.System == true).FirstOrDefault().Currency;
            mPOView.LocalCurrency = ListRates.Where(c => c.Local == true).FirstOrDefault().Currency;
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return View("_PurchaseOrder", Tuple.Create(mPOView, ((UserView)Session["UserLogin"]).CompanyConnect, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.PurchaseOrder, ((UserView)Session["UserLogin"]).CompanyConnect), mOITMUDFList));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(PurchaseOrderView model)
        {

            List<PurchaseOrderLineView> POLinesTemp = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + model.PageKey];
            List<FreightView> FreightListTemp = (List<FreightView>)TempData["FreightList" + model.PageKey];
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];
            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];

            model.AttachmentList = mAttachmentList;
            if (FreightListTemp != null)
                model.ListFreight.AddRange(FreightListTemp);
            model.Lines.Select(c => { c.ItemCode = (POLinesTemp).Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListItemsExit" + model.PageKey] = POLinesTemp;
            TempData["FreightList" + model.PageKey] = FreightListTemp;
            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            string mResult = mPOManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(PurchaseOrderView model)
        {
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];
            List<PurchaseOrderLineView> mListPurchaseOrderLineView = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + model.PageKey];
            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];

            model.AttachmentList = mAttachmentList;
            model.Lines.Select(c => { c.ItemCode = ((List<PurchaseOrderLineView>)TempData["ListItemsExit" + model.PageKey]).Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["ListItemsExit" + model.PageKey] = mListPurchaseOrderLineView;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            string mResult = mPOManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = GetItems(searchViewModel.pPageKey, searchViewModel.pMappedUdf, "", searchViewModel.pItemCode, searchViewModel.pItemName, searchViewModel.pCkCatalogueNum, searchViewModel.pCardCode, searchViewModel.pBPCatalogCode, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemData"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsGrid(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pItemCode = "", string pItemData = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

            OrderColumn mOrderColumn = new OrderColumn();
            mOrderColumn.Name = "ItemCode";
            mOrderColumn.IsOrdered = true;
            mOrderColumn.SortDirection = OrderColumn.OrderDirection.Ascendant;
            pOrderColumn = mOrderColumn;

            JsonObjectResult mJsonObjectResult = GetItems(pPageKey, pMappedUdf, "", pItemCode, pItemData, false, "", "", pStart, pLength, pOrderColumn);

            return PartialView("_ItemsGrid", Tuple.Create(mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), pLength));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLines"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateLinesChanged(List<PurchaseOrderLineView> pLines, string pPageKey)
        {
            List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + pPageKey];
            foreach (PurchaseOrderLineView Line in pLines)
            {
                PurchaseOrderLineView LineToUpdate = ListReturnExit.Where(c => c.LineNum == Line.LineNum).FirstOrDefault();
                LineToUpdate.Dscription = Line.Dscription;
                LineToUpdate.WhsCode = Line.WhsCode;
                LineToUpdate.OcrCode = Line.OcrCode;
                LineToUpdate.GLAccount = Line.GLAccount;
                LineToUpdate.FreeTxt = Line.FreeTxt;
                LineToUpdate.ShipDate = Line.ShipDate;
                LineToUpdate.Quantity = Line.Quantity;
                LineToUpdate.Price = Line.Price;
                LineToUpdate.PriceBefDi = Line.PriceBefDi;
                LineToUpdate.Currency = Line.Currency;
                LineToUpdate.UomCode = Line.UomCode;
                LineToUpdate.TaxCode = Line.TaxCode;
                LineToUpdate.DiscPrcnt = Line.DiscPrcnt;
                LineToUpdate.VatPrcnt = Line.VatPrcnt;
                foreach (UDF_ARGNS mUDF in LineToUpdate.MappedUdf)
                {
                    UDF_ARGNS mUDFAux = Line.MappedUdf.Where(c => c.UDFName == mUDF.UDFName).FirstOrDefault();
                    mUDF.Value = mUDFAux.Value;
                }
            }
            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json("Ok");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItems"></param>
        /// <param name="pBPCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pPageKey"></param>
        /// <param name="pCkCatalogNum"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsForm(string[] pItems, string pBPCurrency, string CardCode, string pPageKey, bool pCkCatalogNum = false)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;

            if (Session["Warehouse"] != null)
            {
                var listWhs = ((IEnumerable<Warehouse>)(Session["Warehouse"])).ToList();

                userSetting.DftWhs = (listWhs).Where(c => c.BPLid == ((UserView)Session["UserLogin"]).CompanyConnect.IdBranchSelect).FirstOrDefault().WhsCode ?? "";
            }

            if (!string.IsNullOrEmpty(userSetting.PurchaseTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.PurchaseTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<PurchaseOrderLineView> ListReturn = null;
            List<PurchaseOrderLineView> ListExistingItems = new List<PurchaseOrderLineView>();
            List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + pPageKey];

            if (ListReturnExit != null)
            {
                ListReturnExit.ForEach(c => c.CalcTax = "N");
            }

            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

            if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
            {
                //Esta lista contiene los items que ya fueron agregados a una linea del documento (solo debo sumarle +1 a la quantity)
                ListExistingItems = ListReturnExit.Where(c => pItems.Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).ToList();
                //Esta lista contiene los items que no fueron agregados a una linea del documento (debo crear una nueva linea)
                pItems = pItems.Where(j => !ListExistingItems.Select(c => c.ItemCode).ToList().Contains(j)).ToArray();
            }
            if (pItems.Length > 0)
            {
                int mLine = 0;

                if (ListReturnExit != null)
                {
                    mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
                }

                List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);
                List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, ListItems.Select(c => c.ItemCode).ToList());

                //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
                List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "POR1");

                List<PurchaseOrderLineView> ListOitm = new List<PurchaseOrderLineView>();
                foreach (ItemMasterView mItemAux in ListItems)
                {
                    PurchaseOrderLineView mLineAux = new PurchaseOrderLineView();
                    UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                    mLineAux.hasStock = (ItemsStock.Where(d => d.ItemCode == mItemAux.ItemCode).Count() > 0 ? true : false);
                    mLineAux.Dscription = mItemAux.ItemName;
                    mLineAux.ItemCode = mItemAux.ItemCode;
                    mLineAux.Currency = (string.IsNullOrEmpty(mItemAux.LastPurCur) ? (ListItems.Where(d => mItemAux.ItemCode == mItemAux.ItemCode).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency : mItemAux.LastPurCur) : mItemAux.LastPurCur) : mItemAux.LastPurCur);
                    mLineAux.LineNum = (mLine = mLine + 1);
                    mLineAux.Price = ((mItemAux.LastPurPrc == 0 || mItemAux.LastPurPrc == null) ? (ListItems.Where(d => mItemAux.ItemCode == mItemAux.ItemCode).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price : mItemAux.LastPurPrc) : mItemAux.LastPurPrc) : mItemAux.LastPurPrc) ?? 0;
                    mLineAux.Quantity = 1;
                    mLineAux.ListCurrency = ListCurrency;
                    //Clone the list of udf to a new list in mLineAux.MappedUdf
                    mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                    mListUserUDF.ForEach((item) =>
                    {
                        mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                    });
                    mLineAux.WhsCode = mItemAux.DfltWH;
                    mLineAux.TaxCode = mDefTAxCode.Code;
                    mLineAux.VatPrcnt = mDefTAxCode.Rate;
                    mLineAux.PriceBefDi = mLineAux.Price;
                    mLineAux.UomCode = mDefaultUOM.UomCode;
                    mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                    mLineAux.ItemPrices = mItemAux.ItemPrices;
                    mLineAux.CalcTax = "Y";
                    mLineAux.Substitute = mItemAux.Substitute;
                    mLineAux.TaxCodeAP = mItemAux.TaxCodeAP;

                    ListOitm.Add(mLineAux);
                }

                if (pItems != null)
                { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
                else
                { ListReturn = new List<PurchaseOrderLineView>(); }

                if (ListReturnExit != null)
                { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }
            }
            if (ListExistingItems.Count > 0)
            {
                //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                ListExistingItems = ListReturnExit.Where(c => ListExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + 1; return k; }).ToList();
                if (ListReturn != null)
                { ListReturn = ListReturn.Union(ListExistingItems).ToList(); }
                else
                { ListReturn = ListReturnExit.Union(ListExistingItems).ToList(); }
            }
            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
            double mRateGl = ListRate.Where(c => c.Currency == pBPCurrency).Select(c => c.Rate).FirstOrDefault();
            TempData["Rates" + pPageKey] = ListRate;

            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pBPCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();
            PurchaseOrderItemsFormView mPurchaseOrderItemsFormView = new PurchaseOrderItemsFormView(ListReturn, pBPCurrency, userSetting, pPageKey);
            mPurchaseOrderItemsFormView.CkCatalogNum = pCkCatalogNum;
            mPurchaseOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            //TempData["ListItems" + pPageKey] = ListReturn;
            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            ViewBag.CalculateRate = "Y";

            return PartialView("_ItemsForm", mPurchaseOrderItemsFormView);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTaxCodeList()
        {
            try
            {
                List<SalesTaxCodesSAP> mTaxCodeList = mPOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
                return Json(mTaxCodeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Vendors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Vendor, true, searchViewModel.pCardCode,
                    searchViewModel.pCardName, ((UserView)Session["UserLogin"]).IsSalesEmployee,
                    ((UserView)Session["UserLogin"]).SECode,
                    ((UserView)Session["UserLogin"]).BPGroupId, true,
                    requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Vendor, true,
                    ((UserView)Session["UserLogin"]).BPCode,
                    searchViewModel.pCardName, false, null, null, true, requestModel.Start,
                    requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListCopyFrom([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            Mapper.CreateMap<OrderColumn, Column>();
            Mapper.CreateMap<Column, OrderColumn>();
            Mapper.CreateMap<PurchaseRequestView, PurchaseRequestSAP>();
            Mapper.CreateMap<PurchaseRequestSAP, PurchaseRequestView>();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            string Vendor = searchViewModel.pCodeVendor;

            switch (searchViewModel.pDocumentType)
            {
                case "PR":
                    mJsonObjectResult = mPRManagerView.GetPurchaseRequestListSearch(((UserView)Session["UserLogin"]).CompanyConnect, "", null, null, null, "O", "", "", requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                    return Json(new DataTablesResponse(requestModel.Draw, Mapper.Map<List<PurchaseRequestView>>(mJsonObjectResult.PurchaseRequestSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
                case "PQ":
                    mJsonObjectResult = mPQManagerView.GetPurchaseQuotationListSearch(((UserView)Session["UserLogin"]).CompanyConnect, searchViewModel.pCodeVendor, null, null, "O", "", "", requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                    return Json(new DataTablesResponse(requestModel.Draw, Mapper.Map<List<PurchaseQuotationView>>(mJsonObjectResult.PurchaseQuotationSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
                default:
                    return null;
            }
        }

        [HttpPost]
        public PartialViewResult GetCopyFromModel(string pDocumentType)
        {
            switch (pDocumentType)
            {
                case "PR":
                    return PartialView("_ListPR_Model");
                case "PQ":
                    return PartialView("_ListPQ_Model");
                default:
                    return null;
            }
        }

        [HttpPost]
        public PartialViewResult _ListLinesCopyFrom(string DocumentType, string[] pDocuments, string pCurrency, string pPageKey)
        {
            List<PurchaseOrderLineView> ListReturn = null;
            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
            TempData["Rates" + pPageKey] = ListRate;
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
            int counter = 0;
            List<FreightView> FreightList = null;
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            PurchaseOrderItemsFormView mPurchaseOrderItemsFormView;
            switch (DocumentType)
            {
                case "PR":
                    List<PurchaseRequestLineView> ListPR = mPRManagerView.GetPurchaseRequestLinesSearch(((UserView)Session["UserLogin"]).CompanyConnect, pDocuments);

                    ListReturn = Mapper.Map<List<PurchaseOrderLineView>>(ListPR);

                    FreightList = new List<FreightView>();
                    foreach (int docEntry in ListReturn.Select(c => c.DocEntry).Distinct().ToList())
                    {
                        FreightList.AddRange(mPRManagerView.GetFreights(((UserView)Session["UserLogin"]).CompanyConnect, docEntry));
                    }
                    FreightList.Select(c => { c.BaseLnNum = c.LineNum; c.BaseAbsEnt = c.DocEntry; c.BaseType = 1470000113; return c; }).ToList();
                    TempData["FreightList" + pPageKey] = FreightList;

                    //Tengo que usar este counter para poner el linenum desde 0 hasta la cantidad de lineas que haya, previo a setear el baseline con el que se van a linkear las lines del documento con el linenum original
                    counter = 0;
                    ListReturn = ListReturn.Select(c => { c.Currency = pCurrency; c.RateGl = mRateGl; c.ListCurrency = ListCurrency; c.BaseType = 1470000113; c.BaseEntry = c.DocEntry; c.BaseLine = c.LineNum; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); c.LineNum = counter; counter++; return c; }).ToList();
                    mPurchaseOrderItemsFormView = new PurchaseOrderItemsFormView(ListReturn, pCurrency, userSetting, pPageKey);
                    mPurchaseOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                    TempData["ListItems" + pPageKey] = ListReturn;
                    TempData["ListItemsExit" + pPageKey] = ListReturn;
                    TempData["ListCurrency" + pPageKey] = ListCurrency;
                    ViewBag.CalculateRate = "Y";

                    return PartialView("_ItemsForm", mPurchaseOrderItemsFormView);

                case "PQ":
                    List<PurchaseQuotationLineView> ListPQ = mPQManagerView.GetPurchaseQuotationLinesSearch(((UserView)Session["UserLogin"]).CompanyConnect, pDocuments);

                    ListReturn = Mapper.Map<List<PurchaseOrderLineView>>(ListPQ);

                    FreightList = new List<FreightView>();
                    foreach (int docEntry in ListReturn.Select(c => c.DocEntry).Distinct().ToList())
                    {
                        FreightList.AddRange(mPQManagerView.GetFreights(((UserView)Session["UserLogin"]).CompanyConnect, docEntry));
                    }
                    FreightList.Select(c => { c.BaseLnNum = c.LineNum; c.BaseAbsEnt = c.DocEntry; c.BaseType = 540000006; return c; }).ToList();
                    TempData["FreightList" + pPageKey] = FreightList;

                    //Tengo que usar este counter para poner el linenum desde 0 hasta la cantidad de lineas que haya, previo a setear el baseline con el que se van a linkear las lines del documento con el linenum original
                    counter = 0;
                    ListReturn = ListReturn.Select(c => { c.Currency = pCurrency; c.RateGl = mRateGl; c.ListCurrency = ListCurrency; c.BaseType = 540000006; c.BaseEntry = c.DocEntry; c.BaseLine = c.LineNum; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); c.LineNum = counter; counter++; return c; }).ToList();
                    mPurchaseOrderItemsFormView = new PurchaseOrderItemsFormView(ListReturn, pCurrency, userSetting, pPageKey);
                    mPurchaseOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                    //TempData["ListItems" + pPageKey] = ListReturn;
                    TempData["ListItemsExit" + pPageKey] = ListReturn;
                    TempData["ListCurrency" + pPageKey] = ListCurrency;
                    ViewBag.CalculateRate = "Y";

                    return PartialView("_ItemsForm", mPurchaseOrderItemsFormView);

                default:
                    return null;
            }
        }

        #region Matrix Model
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModelCode"></param>
        /// <param name="pModelName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelListSearch(AdvancedSearchStylesView searchViewModel)
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            IMapper mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<Column, OrderColumn>(); }
                ).CreateMapper();

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ModelDesc, ModelDescView>();
            }).CreateMapper();

            bool permissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
            .Where(c => c.PageInternalKey == Enums.Pages.PurchaseOrder.ToDescriptionString()
            && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
            .FirstOrDefault() != null ? true : false;

            JsonObjectResult mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList),
                permissionToViewStock, searchViewModel.Start, searchViewModel.Length,
                searchViewModel.CodeModel, searchViewModel.NameModel);

            return PartialView("_ModelList", Tuple.Create(
            mapper.Map<List<ModelDescView>>(mJsonObjectResult.ModelDescViewList),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                searchViewModel.Length));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pModelCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelDetail(string pPageKey, string pModelCode = "")
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            MatrixModelView mModelObj = mPDMManagerView.GetMatrixModelView(
                ((UserView)Session["UserLogin"]).CompanyConnect, pModelCode,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

            GetItems(pPageKey, null, pModelCode, "", "");

            return PartialView("_ModelMatrixDetail", mModelObj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pItemsQty"></param>
        /// <param name="pItems"></param>
        /// <param name="pBPCurrency"></param>
        /// <param name="CardCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ItemsFormMatrixModel(string pItemsQty, string[] pItems, string pBPCurrency, string CardCode, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.PurchaseTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.PurchaseTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            ICollection<JsonObject> jsonModel = JsonConvert.DeserializeObject<ICollection<JsonObject>>(pItemsQty);
            List<PurchaseOrderLineView> ListReturn;
            List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

            int mLine = 0;

            if (ListReturnExit != null)
            {
                mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
            }

            List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
            ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

            //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
            List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "POR1");

            List<PurchaseOrderLineView> ListOitm = new List<PurchaseOrderLineView>();
            foreach (ItemMasterView mItemAux in ListItems)
            {
                PurchaseOrderLineView mLineAux = new PurchaseOrderLineView();
                UnitOfMeasure mDefaultUOM = mItemAux.UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();

                mLineAux.Dscription = mItemAux.ItemName;
                mLineAux.ItemCode = mItemAux.ItemCode;
                mLineAux.Currency = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                mLineAux.LineNum = (mLine = mLine + 1);
                mLineAux.Price = (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (mItemAux.ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0) : 0);
                mLineAux.Quantity = 1;
                mLineAux.ListCurrency = ListCurrency;
                //Clone the list of udf to a new list in mLineAux.MappedUdf
                mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                mListUserUDF.ForEach((item) =>
                {
                    mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                });
                mLineAux.WhsCode = mItemAux.DfltWH;
                mLineAux.TaxCode = mDefTAxCode.Code;
                mLineAux.VatPrcnt = mDefTAxCode.Rate;
                mLineAux.PriceBefDi = mLineAux.Price;
                mLineAux.UomCode = mDefaultUOM.UomCode;
                mLineAux.UnitOfMeasureList = mItemAux.UnitOfMeasureList;
                mLineAux.ItemPrices = mItemAux.ItemPrices;
                mLineAux.CalcTax = "Y";
                mLineAux.TaxCodeAP = mItemAux.TaxCodeAP;

                ListOitm.Add(mLineAux);
            }

            foreach (PurchaseOrderLineView item in ListOitm)
            {
                foreach (var mQty in jsonModel)
                {
                    if (item.ItemCode.Contains(mQty.Code))
                    {
                        item.Quantity = Convert.ToDecimal(mQty.Qty);
                    }
                }
            }


            if (pItems != null)
            { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
            else
            { ListReturn = new List<PurchaseOrderLineView>(); }

            if (ListReturnExit != null)
            { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }

            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

            double mRateGl = ListRate.Where(c => c.Currency == pBPCurrency).Select(c => c.Rate).FirstOrDefault();

            TempData["Rates" + pPageKey] = ListRate;

            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pBPCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();
            PurchaseOrderItemsFormView mPurchaseOrderItemsFormView = new PurchaseOrderItemsFormView(ListReturn, pBPCurrency, userSetting, pPageKey);
            mPurchaseOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

            //TempData["ListItems" + pPageKey] = ListReturn;
            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            ViewBag.CalculateRate = "Y";

            return PartialView("_ItemsForm", mPurchaseOrderItemsFormView);

        }
        #endregion

        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mPOManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }

        [HttpPost]
        public JsonResult GetBp(string Id, string LocalCurrency, string pPageKey)
        {
            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, LocalCurrency);

            TempData["BusinessPartner" + pPageKey] = mBP;

            if (mBP != null)
            {
                mBP.ErrorResponse = "Ok";
                return Json(mBP, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new BusinessPartnerView { ErrorResponse = @Error.WebServError }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult UpdateRateList(DateTime DocDate, string pPageKey)
        {
            // Llamar a esta funcion GetListRatesSystem
            string mLocalCurrency = ((string)Session["LocalCurrency"] == string.Empty ? "" : (string)Session["LocalCurrency"]);
            string mSystemCurrency = ((string)Session["LocalCurrency"] == string.Empty ? "" : (string)Session["LocalCurrency"]);

            List<CurrencySAP> mListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<RatesSystem> ListRates = mPOManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mListCurrency, DocDate, mLocalCurrency, mSystemCurrency);
            TempData["Rates" + pPageKey] = ListRates;
            TempData["ListCurrency" + pPageKey] = mListCurrency;
            try
            {
                if (ListRates != null)
                {
                    return Json(ListRates, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult DeleteRow(string ItemCode, string pPageKey)
        {
            try
            {
                List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + pPageKey];

                ListReturnExit.RemoveAll(c => c.LineNum == Convert.ToInt32(ItemCode));

                TempData["ListItemsExit" + pPageKey] = ListReturnExit;

                return Json("Ok");
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        private JsonObjectResult GetItems(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pModeCode = "", string pItemCode = "", string pItemName = "", bool pCkCatalogueNum = false, string pBPCode = "", string pBPCatalogCode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<ItemMasterView, ItemMasterSAP>(); cfg.CreateMap<StockModel, StockModelView>(); cfg.CreateMap<StockModelView, StockModel>(); }).CreateMapper();
                BusinessPartnerView mBP = (BusinessPartnerView)TempData["BusinessPartner" + pPageKey];

                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(((UserView)Session["UserLogin"]).CompanyConnect,
                    "Y", "", "", "N", "", pMappedUdf, pModeCode,
                    ((UserView)Session["UserLogin"]).IdUser, pItemCode, pItemName,
                    true, Mapper.Map<List<WarehousePortal>>(pWarehousePortalList),
                    pCkCatalogueNum,
                    pBPCode, pBPCatalogCode, pStart, pLength, pOrderColumn);

                List<ItemMasterView> ListReturn = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);
                List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemWithStock(((UserView)Session["UserLogin"]).CompanyConnect, ListReturn.Select(c => c.ItemCode).ToList());

                List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                //if pLength is greatter than 0, i need to page the result, then i need to add the actual page to the previous ListItems
                if (pLength > 0)
                {
                    List<ItemMasterView> mListItemsAux = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]);
                    //If its the first time that i enter to GetItems and the TempData of ListItems is null i inizialice the variable.
                    mListItemsAux = (mListItemsAux == null ? new List<ItemMasterView>() : mListItemsAux);
                    //if the item not exist in mListItemsAux List i add it, else i do nothing
                    mListItemsAux.AddRange(ListReturn.Where(c => mListItemsAux.Select(x => x.ItemCode).Contains(c.ItemCode) == false).Select(c => { c.ListCurrency = ListCurrency; c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList());
                    TempData["ListItems" + pPageKey] = mListItemsAux;
                }
                else
                {
                    TempData["ListItems" + pPageKey] = ListReturn.Select(c => { c.ListCurrency = ListCurrency; c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();
                }
                //Save the ListCurrency and hasStock to ListReturn Items
                ListReturn = ListReturn.Select(c => { c.ListCurrency = ListCurrency; c.hasStock = (ItemsStock.Where(d => d.ItemCode == c.ItemCode).Count() > 0 ? true : false); return c; }).ToList();
                mJsonObjectResult.ItemMasterSAPList = mapper.Map<List<ItemMasterSAP>>(ListReturn);

                TempData["ListCurrency" + pPageKey] = ListCurrency;

                TempData["BusinessPartner" + pPageKey] = mBP;

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public ActionResult GetListPO([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            Mapper.CreateMap<OrderColumn, Column>();
            Mapper.CreateMap<Column, OrderColumn>();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            string Vendor = searchViewModel.pCodeVendor;
            DateTime? Date = null;
            int? DocNum = null;
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

            if (searchViewModel.pDate != "")
            {
                Date = Convert.ToDateTime(searchViewModel.pDate);
            }

            if (searchViewModel.pNro != "")
            {
                DocNum = Convert.ToInt32(searchViewModel.pNro);
            }

            JsonObjectResult mJsonObjectResult = mPOManagerView.GetPurchaseOrderListSearch(((UserView)Session["UserLogin"]).CompanyConnect, Vendor, Date, DocNum, searchViewModel.pDocStatus, searchViewModel.pOwnerCode, searchViewModel.pSECode, requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            return Json(new DataTablesResponse(requestModel.Draw, Mapper.Map<List<PurchaseOrderView>>(mJsonObjectResult.PurchaseOrderSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult _FreightTable(int pDocEntry, string pPageKey)
        {
            List<FreightView> FreightList = new List<FreightView>();
            FreightList = mPOManagerView.GetFreights(((UserView)Session["UserLogin"]).CompanyConnect, pDocEntry);
            List<FreightView> FreightListTemp = (List<FreightView>)TempData["FreightList" + pPageKey];
            if (FreightListTemp != null)
                FreightList.AddRange(FreightListTemp);
            return PartialView("_FreightTable", FreightList);
        }

        public ActionResult CopyOrder(int IdPO, string pPageKey)
        {
            PurchaseOrderView mPOView = null;
            PurchaseQuotationView mPQView = null;

            mPQView = mPQManagerView.GetPurchaseQuotation(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);

            mPOView = Mapper.Map<PurchaseOrderView>(mPQView);

            mPOView.DocDate = System.DateTime.Now;
            mPOView.DocDueDate = System.DateTime.Now;
            mPOView.TaxDate = System.DateTime.Now;
            mPOView.ReqDate = null;
            mPOView.CancelDate = null;
            mPOView.DocEntry = 0;

            mPOView.Comments = "Based On Purchase Quotation " + IdPO.ToString();

            mPOView.Lines.Select(c => { c.ListCurrency = mPOView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();

            TempData["ListItemsExit" + pPageKey] = mPOView.Lines;

            ViewBag.Tite = PurchaseOrders.APO;
            ViewBag.FormMode = "Add";

            TempData["ListCurrency" + pPageKey] = mPOView.ListDocumentSAPCombo.ListCurrency;
            TempData["Rates" + pPageKey] = mPOView.ListSystemRates;

            return View("_PurchaseOrder", Tuple.Create(mPOView, ((UserView)Session["UserLogin"]).CompanyConnect, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.PurchaseOrder, ((UserView)Session["UserLogin"]).CompanyConnect)));
        }

        [HttpPost]
        public JsonResult _GetDistributionRuleList()
        {
            try
            {
                List<DistrRuleSAP> mDistrRuleSAP = (List<DistrRuleSAP>)Session["DistrRuleSAP"];

                List<ListItem> DistributionRuleList = mDistrRuleSAP.Select(c => new ListItem { Value = c.OcrCode.ToString(), Text = c.OcrName }).ToList();

                return Json(DistributionRuleList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public JsonResult _GetWarehouseList()
        {
            try
            {
                List<Warehouse> mWarehouse = (List<Warehouse>)Session["Warehouse"];
                List<ListItem> WhList = mWarehouse.Select(c => new ListItem { Value = c.WhsCode.ToString(), Text = c.WhsName }).ToList();

                return Json(WhList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public JsonResult _GetGLAccountList(string pPageKey)
        {
            try
            {
                List<GLAccountSAP> mGLAccountSAP = (List<GLAccountSAP>)Session["GLAccountSAP"];
                TempData["ListGLAccount" + pPageKey] = mGLAccountSAP;
                List<ListItem> ListReturn = mGLAccountSAP.Select(c => new ListItem { Value = c.FormatCode, Text = c.AcctName }).ToList();

                return Json(ListReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public PartialViewResult _GetHandheldItem(string pBPCurrency, string CardCode, string pPageKey, string pItemCode = "")
        {
            //En el caso del 1 es BarCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 1)
            {
                ItemMasterView oItemMaster = mItemMasterManagerView.GeItemByCodeBar(((UserView)Session["UserLogin"]).CompanyConnect, "Y", "N", pItemCode);

                if (oItemMaster != null)
                {
                    GetItems(pPageKey, null, "", oItemMaster.ItemCode);
                    string[] pItems = new string[1];
                    pItems[0] = oItemMaster.ItemCode;
                    try
                    {
                        return _ItemsForm(pItems, pBPCurrency, CardCode, pPageKey);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            //En el caso del 2 es QRCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 2)
            {
                QRConfigView mQrConfig = mCompaniesManagerView.GetQRConfig(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
                string[] stringSplit = pItemCode.Split(new Char[] { mQrConfig.Separator.ToCharArray().FirstOrDefault() });
                if (pItemCode != "")
                {
                    try
                    {
                        return _ItemsFormQRCode(stringSplit, pBPCurrency, CardCode, mQrConfig, pPageKey);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        [HttpPost]
        public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
        {
            List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
            return PartialView("_ListSE", listSE);
        }

        public PartialViewResult _ItemsFormQRCode(string[] pItems, string pBPCurrency, string CardCode, QRConfigView mQrConfig, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPOManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.PurchaseTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.PurchaseTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<PurchaseOrderLineView> ListReturn = null;
            PurchaseOrderLineView ListExistingItem = null;
            List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            try
            {
                string pItemsAux = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];

                if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
                {
                    ListExistingItem = ListReturnExit.Where(c => c.ItemCode == pItemsAux).FirstOrDefault();
                }
                //Si esta lista es distinta de null solo debo sumarle + 1 a la quantity puesto que el item ya existe en las lineas de la orden
                if (ListExistingItem != null && ListReturnExit != null)
                {
                    //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                    ListReturnExit.Where(c => c.ItemCode == ListExistingItem.ItemCode).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]); return k; }).ToList();
                    ListReturn = ListReturnExit;
                }
                //Debo agregar la linea nueva para ese item
                else
                {
                    int mLine = 0;

                    if (ListReturnExit != null)
                    {
                        mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
                    }

                    PurchaseOrderLineView mPOLine = new PurchaseOrderLineView();
                    mPOLine.ItemCode = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];

                    GetItems(pPageKey, null, "", mPOLine.ItemCode);
                    List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                    ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, CardCode, ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

                    if (mQrConfig.CurrencyPosition != null)
                        mPOLine.Currency = pItems[(mQrConfig.CurrencyPosition ?? 0) - 1];
                    if (mQrConfig.DscriptionPosition != null)
                        mPOLine.Dscription = pItems[(mQrConfig.DscriptionPosition ?? 0) - 1];
                    if (mQrConfig.FreeTxtPosition != null)
                        mPOLine.FreeTxt = pItems[(mQrConfig.FreeTxtPosition ?? 0) - 1];
                    if (mQrConfig.GLAccountPosition != null)
                    {
                        mPOLine.GLAccount = new GLAccountView();
                        mPOLine.GLAccount.FormatCode = pItems[(mQrConfig.GLAccountPosition ?? 0) - 1];
                    }
                    if (mQrConfig.OcrCodePosition != null)
                        mPOLine.OcrCode = pItems[(mQrConfig.OcrCodePosition ?? 0) - 1];

                    if (mQrConfig.PricePosition != null)
                    {
                        mPOLine.Price = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                        mPOLine.PriceBefDi = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                    }
                    else
                    {
                        UnitOfMeasure mDefaultUOM = ListItems.FirstOrDefault().UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();
                        mPOLine.Price = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0);
                        mPOLine.PriceBefDi = mPOLine.Price;
                        mPOLine.Currency = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                        mPOLine.UnitOfMeasureList = ListItems.FirstOrDefault().UnitOfMeasureList;
                        mPOLine.ItemPrices = ListItems.FirstOrDefault().ItemPrices;
                    }

                    //Rates
                    mPOLine.TaxCode = mDefTAxCode.Code;
                    mPOLine.VatPrcnt = mDefTAxCode.Rate;

                    if (mQrConfig.QuantityPosition != null)
                        mPOLine.Quantity = Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]);

                    if (mQrConfig.WhsCodePosition != null)
                    {
                        mPOLine.WhsCode = pItems[(mQrConfig.WhsCodePosition ?? 0) - 1];
                    }
                    else
                    {
                        mPOLine.WhsCode = ListItems.FirstOrDefault().DfltWH;
                    }

                    mPOLine.LineNum = mLine + 1;
                    mPOLine.ListCurrency = ListCurrency;
                    if (ListReturnExit == null)
                        ListReturnExit = new List<PurchaseOrderLineView>();
                    ListReturnExit.Add(mPOLine);
                    ListReturn = ListReturnExit;
                }
                List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
                double mRateGl = ListRate.Where(c => c.Currency == pBPCurrency).Select(c => c.Rate).FirstOrDefault();
                TempData["Rates" + pPageKey] = ListRate;
                ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pBPCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();
                PurchaseOrderItemsFormView mPurchaseOrderItemsFormView = new PurchaseOrderItemsFormView(ListReturn, pBPCurrency, userSetting, pPageKey);
                mPurchaseOrderItemsFormView.LawsSet = ((UserView)Session["UserLogin"]).CompanyConnect.CompanySAPConfig.DocumentSettingsSAP.LawsSet;

                //TempData["ListItems" + pPageKey] = ListReturn;
                TempData["ListItemsExit" + pPageKey] = ListReturn;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                ViewBag.CalculateRate = "Y";

                return PartialView("_ItemsForm", mPurchaseOrderItemsFormView);
            }
            catch (Exception ex)
            {
                TempData["ListItemsExit" + pPageKey] = ListReturnExit;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                throw ex;
            }
        }

        [HttpPost]
        public PartialViewResult GetItemStock(string ItemCode = "")
        {
            List<string> mItemsCode = new List<string>();

            mItemsCode.Add(ItemCode);

            List<StockModelView> ItemsStock = mItemMasterManagerView.GetItemStock(((UserView)Session["UserLogin"]).CompanyConnect, mItemsCode.ToArray());
            return PartialView("_ItemsStock", ItemsStock);
        }

        [HttpPost]
        public JsonResult GetUnitOfMeasure(string ItemCode)
        {
            try
            {
                List<UnitOfMeasureView> mUOM = mItemMasterManagerView.GeUnitOfMeasureByItem(((UserView)Session["UserLogin"]).CompanyConnect, ItemCode);
                return Json(mUOM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        [HttpPost]
        public JsonResult _UpdateUOM(int pLineNum, string pUOMCode, string pPageKey)
        {
            List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit" + pPageKey];
            PurchaseOrderLineView mPOLine = ListReturnExit.Where(c => c.LineNum == pLineNum).FirstOrDefault();
            UnitOfMeasure mUOMAux = mPOLine.UnitOfMeasureList.Where(c => c.UomCode == pUOMCode).FirstOrDefault();
            if (mUOMAux != null)
            {
                ItemPrices mItemPriceAux = mPOLine.ItemPrices.Where(c => c.UOMCode == mUOMAux.UomEntry.ToString()).FirstOrDefault();
                mPOLine.UomCode = mUOMAux.UomCode;
                if (mItemPriceAux != null)
                {
                    mPOLine.Price = mItemPriceAux.Price.Value;
                }
                else
                {
                    if (mUOMAux.AltQty != null)
                    {
                        if (mUOMAux.AltQty.Value != 0 && mUOMAux.AltQty.Value != 1)
                        {
                            mPOLine.Price = mPOLine.ItemPrices.FirstOrDefault().Price.Value / mUOMAux.AltQty.Value;
                        }
                        else
                        {
                            mPOLine.Price = mPOLine.ItemPrices.FirstOrDefault().Price.Value * mUOMAux.BaseQty.Value;
                        }
                    }
                    else
                    {
                        mPOLine.Price = mPOLine.ItemPrices.FirstOrDefault().Price.Value * mUOMAux.BaseQty.Value;
                    }
                }
            }

            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json(mPOLine.Price);
        }

        [HttpPost]
        public PartialViewResult UploadAttachment(FormCollection pData)
        {
            string mFileName = string.Empty;
            string mPageKey = pData["pPageKey"].ToString();
            AttachmentView mAttachment = new AttachmentView();
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + mPageKey];
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;
                    AttachmentView mObj = new AttachmentView();
                    foreach (string mKey in mFiles)
                    {
                        HttpPostedFileBase mFilePost = mFiles[mKey];
                        mFileName = mFilePost.FileName;
                        string path = "~/images/" + mFileName;
                        mFileName = Server.MapPath(path);

                        if (System.IO.File.Exists(mFileName))
                        {
                            throw new Exception(PurchaseOrders.TFAE);
                        }

                        mObj.FileName = Path.GetFileNameWithoutExtension(mFilePost.FileName);
                        mObj.FileExt = Path.GetExtension(mFilePost.FileName).Replace(".", "");
                        mObj.srcPath = Server.MapPath("~/images");
                        mObj.Date = DateTime.Today;

                        if (mFileList.Count == 0)
                        {
                            mObj.Line = 0;
                        }
                        else
                        {
                            mObj.Line = mFileList.Max(c => c.Line) + 1;
                        }

                        mFilePost.SaveAs(mFileName);
                        mFileList.Add(mObj);
                    }

                }

                TempData["Attachments" + mPageKey] = mFileList;

                return PartialView("_FilesList", mFileList);
            }
            catch (Exception ex)
            {
                TempData["Attachments" + mPageKey] = mFileList;
                throw ex;
            }

        }


        [HttpPost]
        public PartialViewResult RemovedAtt(int pId, string pPageKey)
        {
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + pPageKey];

            mFileList.RemoveAll(c => c.Line == pId);

            TempData["Attachments" + pPageKey] = mFileList;

            return PartialView("_FilesList", mFileList);


        }

        [HttpPost]
        public PartialViewResult GetItemsModel()
        {
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return PartialView("_Items", mOITMUDFList);
        }
    }
}