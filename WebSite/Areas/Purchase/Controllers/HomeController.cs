﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.Purchase.Controllers
{
    [CustomerAuthorize(Enums.Areas.Purchase)]
    public class HomeController : Controller
    {
        private ChartManagerView mChartManagerView;
        private AreaKeyManagerView mAreasKeyManagerView;

        public HomeController()
        {
            mChartManagerView = new ChartManagerView();
            mAreasKeyManagerView = new AreaKeyManagerView();
        }

        // GET: /Purchase/Home/
        public ActionResult Index()
        {
            AreaKeyView mAreaKey = mAreasKeyManagerView.GetAreasKeyList().Where(c => c.Area.ToUpper() == Util.Enums.Areas.Purchase.ToString().ToUpper()).FirstOrDefault();
            List<PortalChartView> mListPortalChartView = mChartManagerView.GetPortalChartListSearch(mAreaKey.IdAreaKeys.ToString(), ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, ((UserView)Session["UserLogin"]).UserGroupObj);
            return View(mListPortalChartView);
        }
	}
}