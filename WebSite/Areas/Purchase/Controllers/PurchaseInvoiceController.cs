﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Draft;
using ARGNS.WebSite.Resources.Views.PurchaseInvoices;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.Purchase.Controllers
{
    [CustomerAuthorize(Enums.Areas.Purchase)]
    public class PurchaseInvoiceController : Controller
    {
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private PurchaseInvoiceManagerView mPIManagerView;
        private DraftManagerView mDraftManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;
        private UserManagerView mUserManagerView;

        public PurchaseInvoiceController()
        {
            mPIManagerView = new PurchaseInvoiceManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mDraftManagerView = new DraftManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
            mUserManagerView = new UserManagerView();

            Mapper.CreateMap<Draft, PurchaseInvoiceView>();
            Mapper.CreateMap<DraftLine, PurchaseInvoiceLineView>();
        }

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
        [CustomerAuthorize(Enums.Areas.Purchase, Enums.Pages.PurchaseInvoice, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
                List<PurchaseInvoiceView> ListPI = new List<PurchaseInvoiceView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                if (System.Web.HttpRuntime.Cache["PIList" + mUser] != null)
                {
                    ListPI = (List<PurchaseInvoiceView>)System.Web.HttpRuntime.Cache["PIList" + mUser];
                }

                return View(Tuple.Create(ListPI, ListBp));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ActionPurchaseInvoice"></param>
		/// <param name="IdPI"></param>
		/// <param name="fromController"></param>
		/// <returns></returns>
        public ActionResult ActionPurchaseInvoice(string ActionPurchaseInvoice, int IdPI, string fromController)
        {
            PurchaseInvoiceView mPIView = null;
            PurchaseInvoiceSAP mPI = null;
            switch (fromController)
            {
                case "PurchaseInvoice":
                    ViewBag.URLRedirect = "/Purchase/PurchaseInvoice/Index";
                    ViewBag.controllerName = "PurchaseInvoice";
                    break;
                case "MyDocuments":
                    ViewBag.URLRedirect = "/Purchase/Draft/Index";
                    ViewBag.controllerName = "Draft";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            switch (ActionPurchaseInvoice)
            {
                case "View":
                    ViewBag.Tite = PurchaseInvoices.VPI;
                    ViewBag.FormMode = ActionPurchaseInvoice;
                    mPIView = mPIManagerView.GetPurchaseInvoice(IdPI, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPIView.Lines.Select(c => { c.ListCurrency = mPIView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();
                    break;
                case "ViewDraft":
                    ViewBag.Tite = Drafts.PID;
                    ViewBag.FormMode = "View";
                    DraftView draft = mDraftManagerView.GetDraftById(IdPI, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPIView = new PurchaseInvoiceView();
                    mPIView = Mapper.Map<PurchaseInvoiceView>(draft);
                    mPIView.Lines = Mapper.Map<List<PurchaseInvoiceLineView>>(draft.Lines);
                    //seteo el ocrcode = "" en caso de tenga null, para que luego en el items form no le ponga el warehouse por defecto dado que no es una linea nueva
                    mPIView.Lines = mPIView.Lines.Select(c => { c.ListCurrency = mPIView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();
                    break;
            }

            return View("_PurchaseInvoice", Tuple.Create(mPIView, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.PurchaseInvoice, ((UserView)Session["UserLogin"]).CompanyConnect)));
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="requestModel"></param>
		/// <param name="searchViewModel"></param>
		/// <returns></returns>
        [HttpPost]
        public ActionResult _Vendors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Vendor, true, searchViewModel.pCardCode, 
                    searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, 
                    ((UserView)Session["UserLogin"]).SECode, 
                    ((UserView)Session["UserLogin"]).BPGroupId, true, 
                    requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, 
                    Enums.BpType.Vendor, true, 
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, 
                    false, null, null, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="requestModel"></param>
		/// <param name="searchViewModel"></param>
		/// <returns></returns>
        [HttpPost]
        public ActionResult GetListPI([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            Mapper.CreateMap<OrderColumn, Column>();
            Mapper.CreateMap<Column, OrderColumn>();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            string Vendor = searchViewModel.pCodeVendor;
            DateTime? Date = null;
            int? DocNum = null;
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

            if (searchViewModel.pDate != "")
            {
                Date = Convert.ToDateTime(searchViewModel.pDate);
            }

            if (searchViewModel.pNro != "")
            {
                DocNum = Convert.ToInt32(searchViewModel.pNro);
            }

            JsonObjectResult mJsonObjectResult = mPIManagerView.GetPurchaseInvoiceListSearch(((UserView)Session["UserLogin"]).CompanyConnect, Vendor, Date, DocNum, searchViewModel.pDocStatus, searchViewModel.pOwnerCode, searchViewModel.pSECode, requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            return Json(new DataTablesResponse(requestModel.Draw, Mapper.Map<List<PurchaseInvoiceView>>(mJsonObjectResult.PurchaseInvoiceSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pEmployeeName"></param>
		/// <returns></returns>
        [HttpPost]
        public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
        {
            List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
            return PartialView("_ListSE", listSE);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pEmployeeCode"></param>
		/// <param name="pEmployeeName"></param>
		/// <returns></returns>
        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mPIManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }
    }
}