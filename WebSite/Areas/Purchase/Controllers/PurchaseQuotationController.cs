﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Draft;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PurchaseOrders;
using AutoMapper;
using DataTables.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.Purchase.Controllers
{
    [CustomerAuthorize(Enums.Areas.Purchase)]
    public class PurchaseQuotationController : Controller
    {
        private PurchaseQuotationManagerView mPQManagerView;

        private PurchaseOrderManagerView mPOManagerView;

        private PurchaseRequestManagerView mPRManagerView;

        private ItemMasterManagerView mItemMasterManagerView;

        private BusinessPartnerManagerView mBusinessPartnerManagerView;

        private DraftManagerView mDraftManagerView;

        private PDMManagerView mPDMManagerView;

        private CompaniesManagerView mCompaniesManagerView;

        private UserManagerView mUserManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;

        public PurchaseQuotationController()
        {
            mPQManagerView = new PurchaseQuotationManagerView();
            mPOManagerView = new PurchaseOrderManagerView();

            mItemMasterManagerView = new ItemMasterManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            mDraftManagerView = new DraftManagerView();
            mPDMManagerView = new PDMManagerView();
            mPRManagerView = new PurchaseRequestManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
            mUserManagerView = new UserManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();

            Mapper.CreateMap<DraftView, PurchaseQuotationView>();
            Mapper.CreateMap<DraftLineView, PurchaseQuotationLineView>();
            Mapper.CreateMap<PurchaseRequestView, PurchaseQuotationView>();
            Mapper.CreateMap<PurchaseRequestLineView, PurchaseQuotationLineView>();
            Mapper.CreateMap<UserUDFView, UDF_ARGNS>();
            Mapper.CreateMap<UDF_ARGNS, UserUDFView>();
        }

        [CustomerAuthorize(Enums.Areas.Purchase, Enums.Pages.PurchaseQuotation, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
                List<PurchaseQuotationView> ListPO = new List<PurchaseQuotationView>();
                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                if (System.Web.HttpRuntime.Cache["PQList" + mUser] != null)
                {
                    ListPO = (List<PurchaseQuotationView>)System.Web.HttpRuntime.Cache["PQList" + mUser];
                }

                return View(Tuple.Create(ListPO, ListBp));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult ActionPurchaseOrder(string ActionPurchaseOrder, int IdPO, string fromController)
        {
            PurchaseQuotationView mPQView = null;
            PurchaseQuotationSAP mPQ = null;
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            TempData["ListItems" + mPageKey] = null;
            switch (fromController)
            {
                case "PurchaseQuotation":
                    ViewBag.URLRedirect = "/Purchase/PurchaseQuotation/Index";
                    ViewBag.controllerName = "PurchaseQuotation";
                    break;
                case "PurchaseRequest":
                    ViewBag.URLRedirect = "/Purchase/PurchaseRequest/Index";
                    ViewBag.controllerName = "PurchaseRequest";
                    break;
                case "MyDocuments":
                    ViewBag.URLRedirect = "/Purchase/Draft/Index";
                    ViewBag.controllerName = "Draft";
                    break;
                case "MyApprovals":
                    ViewBag.URLRedirect = "/Purchase/Approval/Index";
                    ViewBag.controllerName = "Approval";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            switch (ActionPurchaseOrder)
            {
                case "Add":
                    ViewBag.Tite = PurchaseOrders.APQ;
                    ViewBag.FormMode = ActionPurchaseOrder;
                    mPQView = mPQManagerView.GetPurchaseQuotation(0, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPQView.MappedUdf = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OPQT");
                    mPQView.DocDate = System.DateTime.Now;
                    mPQView.DocDueDate = System.DateTime.Now;
                    mPQView.TaxDate = System.DateTime.Now;
                    mPQView.ReqDate = System.DateTime.Now;
                    mPQView.CancelDate = null;
                    if (((UserView)Session["UserLogin"]).IsSalesEmployee == true && ((UserView)Session["UserLogin"]).SECode != null)
                    {
                        mPQView.SlpCode = ((UserView)Session["UserLogin"]).SECode.Value;
                    }
                    TempData["ListItemsExit" + mPageKey] = null;
                    break;

                case "View":
                case "Update":
                    if (ActionPurchaseOrder == "View")
                        ViewBag.Tite = PurchaseOrders.VPQ;
                    else
                        ViewBag.Tite = PurchaseOrders.UPQ;
                    ViewBag.FormMode = ActionPurchaseOrder;
                    mPQView = mPQManagerView.GetPurchaseQuotation(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPQView.Lines.Select(c => { c.ListCurrency = mPQView.ListDocumentSAPCombo.ListCurrency; c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); return c; }).ToList();
                    TempData["ListItemsExit" + mPageKey] = mPQView.Lines;
                    break;

                case "ViewDraft":
                    ViewBag.Tite = Drafts.PRD;
                    ViewBag.FormMode = "View";
                    DraftView draft = mDraftManagerView.GetDraftById(IdPO, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPQView = new PurchaseQuotationView();
                    mPQView = Mapper.Map<PurchaseQuotationView>(draft);
                    mPQView.Lines = Mapper.Map<List<PurchaseQuotationLineView>>(draft.Lines);

                    mPQ = mPQManagerView.GetPQInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mPQView);
                    mPQView = Mapper.Map<PurchaseQuotationView>(mPQ);
                    mPQView.Lines = Mapper.Map<List<PurchaseQuotationLineView>>(mPQ.Lines);
                    mPQView.Lines.Select(c => { c.ListCurrency = mPQView.ListDocumentSAPCombo.ListCurrency; c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); return c; }).ToList();


                    TempData["ListItemsExit" + mPageKey] = mPQView.Lines;
                    break;

                case "CopyFromPR":
                    ViewBag.Tite = PurchaseOrders.APQ;
                    ViewBag.FormMode = "Add";
                    PurchaseRequestView mPRView = mPRManagerView.GetPurchaseRequest(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPQView = new PurchaseQuotationView();
                    mPQView = Mapper.Map<PurchaseQuotationView>(mPRView);
                    mPQView.Lines = Mapper.Map<List<PurchaseQuotationLineView>>(mPRView.Lines);

                    mPQ = mPQManagerView.GetPQInternalObjects(((UserView)Session["UserLogin"]).CompanyConnect, mPQView);
                    mPQView = Mapper.Map<PurchaseQuotationView>(mPQ);
                    mPQView.Lines = Mapper.Map<List<PurchaseQuotationLineView>>(mPQ.Lines);
                    mPQView.Lines.Select(c => { c.ListCurrency = mPQView.ListDocumentSAPCombo.ListCurrency; c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); c.BaseType = 1470000113; c.BaseEntry = mPQView.DocEntry; c.BaseLine = c.LineNum; return c; }).ToList();

                    //Actualizo estas 2 fechas a la fecha actual tal como hace SAP y setteo el DocNumber en 0 para evitar equivocaciones por parte del usuario
                    mPQView.DocDate = DateTime.Today;
                    mPQView.TaxDate = DateTime.Today;
                    mPQView.DocNum = 0;
                    TempData["ListItemsExit" + mPageKey] = mPQView.Lines;
                    break;
            }

            Session["LocalCurrency"] = mPQView.LocalCurrency;
            Session["SystemCurrency"] = mPQView.SystemCurrency;
            Session["DistrRuleSAP"] = mPQView.DistrRuleSAP;
            Session["Warehouse"] = mPQView.Warehouse;
            Session["GLAccountSAP"] = mPQView.GLAccountSAP;

            TempData["Attachments" + mPageKey] = mPQView.AttachmentList;
            TempData["ListCurrency" + mPageKey] = mPQView.ListDocumentSAPCombo.ListCurrency;
            List<RatesSystem> ListRates = mPQManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mPQView.ListDocumentSAPCombo.ListCurrency, mPQView.DocDate, mPQView.LocalCurrency, mPQView.SystemCurrency);
            TempData["Rates" + mPageKey] = ListRates;
            mPQView.SystemCurrency = ListRates.Where(c => c.System == true).FirstOrDefault().Currency;
            mPQView.LocalCurrency = ListRates.Where(c => c.Local == true).FirstOrDefault().Currency;
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return View("_PurchaseQuotation", Tuple.Create(mPQView, ((UserView)Session["UserLogin"]).CompanyConnect, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.PurchaseQuotation, ((UserView)Session["UserLogin"]).CompanyConnect), mOITMUDFList));
        }

        [HttpPost]
        public JsonResult Add(PurchaseQuotationView model)
        {
            List<PurchaseQuotationLineView> PQLinesTemp = (List<PurchaseQuotationLineView>)TempData["ListItemsExit" + model.PageKey];
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];
            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];
            model.AttachmentList = mAttachmentList;
            model.Lines.Select(c => { c.ItemCode = ((List<PurchaseQuotationLineView>)PQLinesTemp).Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListItemsExit" + model.PageKey] = PQLinesTemp;
            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            string mResult = mPQManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);
        }

        [HttpPost]
        public JsonResult Update(PurchaseQuotationView model)
        {
            List<GLAccountSAP> GLAccountList = (List<GLAccountSAP>)TempData["ListGLAccount" + model.PageKey];
            List<PurchaseQuotationLineView> mListPurchaseQuotationLineView = (List<PurchaseQuotationLineView>)TempData["ListItemsExit" + model.PageKey];
            List<AttachmentView> mAttachmentList = (List<AttachmentView>)TempData["Attachments" + model.PageKey];
            model.AttachmentList = mAttachmentList;
            model.Lines.Select(c => { c.ItemCode = mListPurchaseQuotationLineView.Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["ListItemsExit" + model.PageKey] = mListPurchaseQuotationLineView;
            TempData["Attachments" + model.PageKey] = mAttachmentList;

            string mResult = mPQManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);
        }

        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = GetItems(searchViewModel.pPageKey, searchViewModel.pMappedUdf, "", searchViewModel.pItemCode, searchViewModel.pItemName, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public PartialViewResult _ItemsGrid(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pItemCode = "", string pItemData = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

            OrderColumn mOrderColumn = new OrderColumn();
            mOrderColumn.Name = "ItemCode";
            mOrderColumn.IsOrdered = true;
            mOrderColumn.SortDirection = OrderColumn.OrderDirection.Ascendant;
            pOrderColumn = mOrderColumn;

            JsonObjectResult mJsonObjectResult = GetItems(pPageKey, pMappedUdf, "", pItemCode, pItemData, pStart, pLength, pOrderColumn);

            return PartialView("_ItemsGrid", Tuple.Create(mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), pLength));
        }

        [HttpPost]
        public JsonResult _UpdateLinesChanged(List<PurchaseQuotationLineView> pLines, string pPageKey)
        {
            List<PurchaseQuotationLineView> ListReturnExit = (List<PurchaseQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            foreach (PurchaseQuotationLineView Line in pLines)
            {
                PurchaseQuotationLineView LineToUpdate = ListReturnExit.Where(c => c.LineNum == Line.LineNum).FirstOrDefault();
                LineToUpdate.Dscription = Line.Dscription;
                LineToUpdate.WhsCode = Line.WhsCode;
                LineToUpdate.OcrCode = Line.OcrCode;
                LineToUpdate.GLAccount = Line.GLAccount;
                LineToUpdate.FreeTxt = Line.FreeTxt;
                LineToUpdate.ShipDate = Line.ShipDate;
                LineToUpdate.PQTReqDate = Line.PQTReqDate;
                LineToUpdate.Quantity = Line.Quantity;
                LineToUpdate.Price = Line.Price;
                LineToUpdate.PriceBefDi = Line.PriceBefDi;
                LineToUpdate.Currency = Line.Currency;
                LineToUpdate.UomCode = Line.UomCode;
                foreach (UDF_ARGNS mUDF in LineToUpdate.MappedUdf)
                {
                    UDF_ARGNS mUDFAux = Line.MappedUdf.Where(c => c.UDFName == mUDF.UDFName).FirstOrDefault();
                    mUDF.Value = mUDFAux.Value;
                }
            }
            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json("Ok");
        }

        [HttpPost]
        public PartialViewResult _ItemsForm(string[] pItems, string pCurrency, DateTime? pReqDate, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.PurchaseTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.PurchaseTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<PurchaseQuotationLineView> ListReturn = null;
            List<PurchaseQuotationLineView> ListExistingItems = new List<PurchaseQuotationLineView>();

            List<PurchaseQuotationLineView> ListReturnExit = (List<PurchaseQuotationLineView>)TempData["ListItemsExit" + pPageKey];

            if (ListReturnExit != null)
            {
                ListReturnExit.ForEach(c => c.CalcTax = "N");
            }

            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
            {
                //Esta lista contiene los items que ya fueron agregados a una linea del documento (solo debo sumarle +1 a la quantity)
                ListExistingItems = ListReturnExit.Where(c => pItems.Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).ToList();
                //Esta lista contiene los items que no fueron agregados a una linea del documento (debo crear una nueva linea)
                pItems = pItems.Where(j => !ListExistingItems.Select(c => c.ItemCode).ToList().Contains(j)).ToArray();
            }

            if (pItems.Length > 0)
            {

                int mLine = 0;

                if (ListReturnExit != null)
                {
                    mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
                }

                List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, "", ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

                //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
                List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "PQT1");

                List<PurchaseQuotationLineView> ListOitm = ListItems.Where(c => pItems.Contains(c.ItemCode)).Select(c => new PurchaseQuotationLineView { Dscription = c.ItemName, ItemCode = c.ItemCode, Currency = c.LastPurCur, LineNum = (mLine = mLine + 1), Price = c.LastPurPrc ?? 0, Quantity = 1, ListCurrency = ListCurrency, WhsCode = c.DfltWH, TaxCode = mDefTAxCode.Code, VatPrcnt = mDefTAxCode.Rate, CalcTax = "Y", PriceBefDi = c.LastPurPrc ?? 0, UnitOfMeasureList = c.UnitOfMeasureList, TaxCodeAP = c.TaxCodeAP }).ToList();
                
                foreach (PurchaseQuotationLineView mLineAux in ListOitm)
                {
                    //Clone the list of udf to a new list in mLineAux.MappedUdf
                    mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                    mListUserUDF.ForEach((item) =>
                    {
                        mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                    });
                }

                if (pItems != null)
                { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
                else
                { ListReturn = new List<PurchaseQuotationLineView>(); }

                if (ListReturnExit != null)
                { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }
            }
            if (ListExistingItems.Count > 0)
            {
                //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                ListExistingItems = ListReturnExit.Where(c => ListExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + 1; return k; }).ToList();
                if (ListReturn != null)
                { ListReturn = ListReturn.Union(ListExistingItems).ToList(); }
                else
                { ListReturn = ListReturnExit.Union(ListExistingItems).ToList(); }
            }
            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();

            TempData["Rates" + pPageKey] = ListRate;

            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

            //TempData["ListItems" + pPageKey] = ListReturn;
            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            ViewBag.CalculateRate = "Y";

            return PartialView("_ItemsForm", Tuple.Create(ListReturn, pCurrency, userSetting, pReqDate));

        }

        [HttpPost]
        public JsonResult GetTaxCodeList()
        {
            try
            {
                List<SalesTaxCodesSAP> mTaxCodeList = mPQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
                return Json(mTaxCodeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public ActionResult _Vendors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect, 
                    Enums.BpType.Vendor, true, 
                    searchViewModel.pCardCode, searchViewModel.pCardName,
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, 
                    ((UserView)Session["UserLogin"]).SECode, 
                    ((UserView)Session["UserLogin"]).BPGroupId, true, 
                    requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(((UserView)Session["UserLogin"]).CompanyConnect,
                    Enums.BpType.Vendor, true, 
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, 
                    false, null, null, true, requestModel.Start, requestModel.Length,
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mPQManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }

        [HttpPost]
        public JsonResult GetBp(string Id, string LocalCurrency, string pPageKey)
        {
            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, LocalCurrency);

            TempData["BusinessPartner" + pPageKey] = mBP;

            if (mBP != null)
            {
                mBP.ErrorResponse = "Ok";
                return Json(mBP, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new BusinessPartnerView { ErrorResponse = @Error.WebServError }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult UpdateRateList(DateTime DocDate, string pPageKey)
        {
            List<CurrencySAP> mListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<RatesSystem> ListRates = mPQManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mListCurrency, DocDate, (string)Session["LocalCurrency"], (string)Session["SystemCurrency"]);
            TempData["Rates" + pPageKey] = ListRates;
            TempData["ListCurrency" + pPageKey] = mListCurrency;
            try
            {
                if (ListRates != null)
                {
                    return Json(ListRates, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult DeleteRow(string ItemCode, string pPageKey)
        {
            try
            {
                List<PurchaseQuotationLineView> ListReturnExit = (List<PurchaseQuotationLineView>)TempData["ListItemsExit" + pPageKey];

                ListReturnExit.RemoveAll(c => c.LineNum == Convert.ToInt32(ItemCode));

                TempData["ListItemsExit" + pPageKey] = ListReturnExit;

                return Json("Ok");
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        private JsonObjectResult GetItems(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pModeCode = "", string pItemCode = "", string pItemName = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                IMapper mapper = new MapperConfiguration(cfg => 
                { cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                    cfg.CreateMap<ItemMasterView, ItemMasterSAP>();
                    cfg.CreateMap<StockModel, StockModelView>();
                    cfg.CreateMap<StockModelView, StockModel>(); }).CreateMapper();

                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(
                    ((UserView)Session["UserLogin"]).CompanyConnect, 
                    "Y", "", "", "N", "",  pMappedUdf, pModeCode, ((UserView)Session["UserLogin"]).IdUser, 
                    pItemCode, pItemName, true, Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                    false, "", "", pStart, pLength, pOrderColumn);

                List<ItemMasterView> ListReturn = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);
                List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                //if pLength is greatter than 0, i need to page the result, then i need to add the actual page to the previous ListItems
                if (pLength > 0)
                {
                    List<ItemMasterView> mListItemsAux = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]);
                    //If its the first time that i enter to GetItems and the TempData of ListItems is null i inizialice the variable.
                    mListItemsAux = (mListItemsAux == null ? new List<ItemMasterView>() : mListItemsAux);
                    //if the item not exist in mListItemsAux List i add it, else i do nothing
                    mListItemsAux.AddRange(ListReturn.Where(c => mListItemsAux.Select(x => x.ItemCode).Contains(c.ItemCode) == false).Select(c => { c.ListCurrency = ListCurrency; return c; }).ToList());
                    TempData["ListItems" + pPageKey] = mListItemsAux;
                }
                else
                {
                    TempData["ListItems" + pPageKey] = ListReturn.Select(c => { c.ListCurrency = ListCurrency; return c; }).ToList();
                }
                mJsonObjectResult.ItemMasterSAPList = mapper.Map<List<ItemMasterSAP>>(ListReturn);

                TempData["ListCurrency" + pPageKey] = ListCurrency;

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult GetListPQ([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            Mapper.CreateMap<OrderColumn, Column>();
            Mapper.CreateMap<Column, OrderColumn>();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            string Vendor = searchViewModel.pCodeVendor;
            DateTime? Date = null;
            int? DocNum = null;
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

            if (searchViewModel.pDate != "")
            {
                Date = Convert.ToDateTime(searchViewModel.pDate);
            }

            if (searchViewModel.pNro != "")
            {
                DocNum = Convert.ToInt32(searchViewModel.pNro);
            }
            JsonObjectResult mJsonObjectResult = mPQManagerView.GetPurchaseQuotationListSearch(((UserView)Session["UserLogin"]).CompanyConnect, Vendor, Date, DocNum, searchViewModel.pDocStatus, searchViewModel.pOwnerCode, searchViewModel.pSECode, requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            return Json(new DataTablesResponse(requestModel.Draw, Mapper.Map<List<PurchaseQuotationView>>(mJsonObjectResult.PurchaseQuotationSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CopyOrder(int IdPO, string pPageKey)
        {
            PurchaseOrderView mPOView = null;
            PurchaseQuotationView mPQView = null;

            Mapper.CreateMap<PurchaseOrderView, PurchaseQuotationView>();
            Mapper.CreateMap<PurchaseOrderLineView, PurchaseQuotationLineView>();

            mPOView = mPOManagerView.GetPurchaseOrder(IdPO, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);

            Session["LocalCurrency" + pPageKey] = mPOView.LocalCurrency;
            Session["SystemCurrency" + pPageKey] = mPOView.SystemCurrency;

            mPQView = Mapper.Map<PurchaseQuotationView>(mPOView);

            mPQView.DocDate = System.DateTime.Now;
            mPQView.DocDueDate = System.DateTime.Now;
            mPQView.TaxDate = System.DateTime.Now;
            mPQView.ReqDate = System.DateTime.Now;
            mPQView.CancelDate = null;
            mPQView.DocEntry = 0;

            mPQView.Comments = "Based On Purchase Order " + IdPO.ToString();

            mPQView.Lines.Select(c => { c.ListCurrency = mPQView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();

            TempData["ListItemsExit" + pPageKey] = mPQView.Lines;

            ViewBag.Tite = "Add";
            ViewBag.FormMode = "Add";

            TempData["ListCurrency" + pPageKey] = mPQView.ListDocumentSAPCombo.ListCurrency;
            TempData["Rates" + pPageKey] = mPQView.ListSystemRates;

            return View("_PurchaseQuotation", Tuple.Create(mPQView, ((UserView)Session["UserLogin"]).CompanyConnect, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.PurchaseQuotation, ((UserView)Session["UserLogin"]).CompanyConnect)));
        }

        [HttpPost]
        public ActionResult ListCopyFrom([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            Mapper.CreateMap<OrderColumn, Column>();
            Mapper.CreateMap<Column, OrderColumn>();
            Mapper.CreateMap<PurchaseRequestView, PurchaseRequestSAP>();
            Mapper.CreateMap<PurchaseRequestSAP, PurchaseRequestView>();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            string Vendor = searchViewModel.pCodeVendor;

            switch (searchViewModel.pDocumentType)
            {
                case "PR":
                    mJsonObjectResult = mPRManagerView.GetPurchaseRequestListSearch(((UserView)Session["UserLogin"]).CompanyConnect, "", null, null, null, "O", "", "", requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
                    return Json(new DataTablesResponse(requestModel.Draw, Mapper.Map<List<PurchaseRequestView>>(mJsonObjectResult.PurchaseRequestSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
                default:
                    return null;
            }
        }

        [HttpPost]
        public PartialViewResult GetCopyFromModel(string pDocumentType)
        {
            switch (pDocumentType)
            {
                case "PR":
                    return PartialView("_ListPR_Model");
                default:
                    return null;
            }
        }

        [HttpPost]
        public PartialViewResult _ListLinesCopyFrom(string DocumentType, string[] pDocuments, string pCurrency, string pPageKey)
        {
            switch (DocumentType)
            {
                case "PR":
                    List<PurchaseRequestLineView> ListPR = mPRManagerView.GetPurchaseRequestLinesSearch(((UserView)Session["UserLogin"]).CompanyConnect, pDocuments);

                    List<PurchaseQuotationLineView> ListReturn = Mapper.Map<List<PurchaseQuotationLineView>>(ListPR);

                    List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

                    List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

                    double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();

                    TempData["Rates" + pPageKey] = ListRate;

                    //Tengo que usar este counter para poner el linenum desde 0 hasta la cantidad de lineas que haya, previo a setear el baseline con el que se van a linkear las lines del documento con el linenum original
                    int counter = 0;
                    ListReturn = ListReturn.Select(c => { c.Currency = pCurrency; c.RateGl = mRateGl; c.ListCurrency = ListCurrency; c.BaseType = 1470000113; c.BaseEntry = c.DocEntry; c.BaseLine = c.LineNum; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); c.LineNum = counter; counter++; return c; }).ToList();
                    List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
                    UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();

                    //TempData["ListItems" + pPageKey] = ListReturn;
                    TempData["ListItemsExit" + pPageKey] = ListReturn;
                    TempData["ListCurrency" + pPageKey] = ListCurrency;
                    ViewBag.CalculateRate = "Y";

                    return PartialView("_ItemsForm", Tuple.Create(ListReturn, pCurrency, userSetting, new DateTime?()));

                default:
                    return null;
            }
        }

        #region Matrix Model
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModelCode"></param>
        /// <param name="pModelName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelListSearch(AdvancedSearchStylesView searchViewModel)
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            bool permissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
                .Where(c => c.PageInternalKey == Enums.Pages.PurchaseQuotation.ToDescriptionString()
                && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
                .FirstOrDefault() != null ? true : false;

            JsonObjectResult mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                permissionToViewStock, searchViewModel.Start, searchViewModel.Length,
                searchViewModel.CodeModel, searchViewModel.NameModel);

            IMapper mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<Column, OrderColumn>(); }
                ).CreateMapper();

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ModelDesc, ModelDescView>();
            }).CreateMapper();

            return PartialView("_ModelList", Tuple.Create(
                mapper.Map<List<ModelDescView>>(mJsonObjectResult.ModelDescViewList),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                searchViewModel.Length));
        }

        [HttpPost]
        public PartialViewResult _ModelDetail(string pPageKey, string pModelCode = "")
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            MatrixModelView mModelObj = mPDMManagerView.GetMatrixModelView(
                ((UserView)Session["UserLogin"]).CompanyConnect, pModelCode, 
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

            GetItems(pPageKey, null, pModelCode, "", "");
            return PartialView("_ModelMatrixDetail", mModelObj);
        }

        [HttpPost]
        public PartialViewResult _FreightTable(int pDocEntry)
        {
            List<FreightView> FreightList = new List<FreightView>();
            FreightList = mPQManagerView.GetFreights(((UserView)Session["UserLogin"]).CompanyConnect, pDocEntry);

            return PartialView("_FreightTable", FreightList);
        }


        //Verificar este metodo para implementarlo Mejor.
        [HttpPost]
        public PartialViewResult _ItemsFormMatrixModel(string pItemsQty, string[] pItems, string pCurrency, DateTime? pReqDate, string pPageKey)
        {

            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.PurchaseTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.PurchaseTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            ICollection<JsonObject> jsonModel = JsonConvert.DeserializeObject<ICollection<JsonObject>>(pItemsQty);
            List<PurchaseQuotationLineView> ListReturn;
            List<PurchaseQuotationLineView> ListReturnExit = (List<PurchaseQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];

            int mLine = 0;

            if (ListReturnExit != null)
            {
                mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
            }

            List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
            ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, "", ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);

            //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
            List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "PQT1");

            List<PurchaseQuotationLineView> ListOitm = ListItems.Where(c => pItems.Contains(c.ItemCode)).Select(c => new PurchaseQuotationLineView { Dscription = c.ItemName, ItemCode = c.ItemCode, Currency = c.LastPurCur, LineNum = (mLine = mLine + 1), Price = c.LastPurPrc ?? 0, Quantity = 1, ListCurrency = ListCurrency, WhsCode = c.DfltWH, TaxCode = mDefTAxCode.Code, VatPrcnt = mDefTAxCode.Rate, CalcTax = "Y", PriceBefDi = c.LastPurPrc ?? 0, UnitOfMeasureList = c.UnitOfMeasureList, TaxCodeAP = c.TaxCodeAP }).ToList();
            foreach (PurchaseQuotationLineView mLineAux in ListOitm)
            {
                //Clone the list of udf to a new list in mLineAux.MappedUdf
                mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                mListUserUDF.ForEach((item) =>
                {
                    mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                });
            }
            foreach (PurchaseQuotationLineView item in ListOitm)
            {
                foreach (var mQty in jsonModel)
                {
                    if (item.ItemCode.Contains(mQty.Code))
                    {
                        item.Quantity = Convert.ToDecimal(mQty.Qty);
                    }
                }
            }

            if (pItems != null)
            { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
            else
            { ListReturn = new List<PurchaseQuotationLineView>(); }

            if (ListReturnExit != null)
            { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }

            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];

            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();
            TempData["Rates" + pPageKey] = ListRate;

            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

            TempData["ListItems" + pPageKey] = ListReturn;
            TempData["ListItemsExit" + pPageKey] = ListReturn;
            TempData["ListCurrency" + pPageKey] = ListCurrency;
            ViewBag.CalculateRate = "Y";

            return PartialView("_ItemsForm", Tuple.Create(ListReturn, pCurrency, userSetting, pReqDate));


        }
        #endregion

        [HttpPost]
        public JsonResult _GetDistributionRuleList()
        {
            try
            {
                //List<ListItem> DistributionRuleList = mUserManagerView.GetDistributionRuleList(((UserView)Session["UserLogin"]).CompanyConnect);

                List<DistrRuleSAP> mDistrRuleSAP = (List<DistrRuleSAP>)Session["DistrRuleSAP"];

                List<ListItem> DistributionRuleList = mDistrRuleSAP.Select(c => new ListItem { Value = c.OcrCode.ToString(), Text = c.OcrName }).ToList();

                return Json(DistributionRuleList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public JsonResult _GetWarehouseList()
        {
            try
            {
                List<Warehouse> mWarehouse = (List<Warehouse>)Session["Warehouse"];

                List<ListItem> WhList = mWarehouse.Select(c => new ListItem { Value = c.WhsCode.ToString(), Text = c.WhsName }).ToList();
                
                return Json(WhList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public JsonResult _GetGLAccountList(string pPageKey)
        {
            try
            {
                List<GLAccountSAP> mGLAccountSAP = (List<GLAccountSAP>)Session["GLAccountSAP"];

                TempData["ListGLAccount" + pPageKey] = mGLAccountSAP;
                List<ListItem> ListReturn = mGLAccountSAP.Select(c => new ListItem { Value = c.FormatCode, Text = c.AcctName }).ToList();

                return Json(ListReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public PartialViewResult _GetHandheldItem(string pBPCurrency, DateTime? pReqDate, string pPageKey, string pItemCode = "")
        {
            //En el caso del 1 es BarCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 1)
            {
                ItemMasterView oItemMaster = mItemMasterManagerView.GeItemByCodeBar(((UserView)Session["UserLogin"]).CompanyConnect, "Y", "N", pItemCode);

                if (oItemMaster != null)
                {
                    GetItems(pPageKey, null, "", oItemMaster.ItemCode);
                    string[] pItems = new string[1];
                    pItems[0] = oItemMaster.ItemCode;
                    return _ItemsForm(pItems, pBPCurrency, pReqDate, pPageKey);
                }
                else
                {
                    return null;
                }
            }
            //En el caso del 2 es QRCode
            if (((UserView)Session["UserLogin"]).CompanyConnect.CodeType == 2)
            {
                QRConfigView mQrConfig = mCompaniesManagerView.GetQRConfig(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
                string[] stringSplit = pItemCode.Split(new Char[] { mQrConfig.Separator.ToCharArray().FirstOrDefault() });
                if (pItemCode != "")
                {
                    try
                    {
                        return _ItemsFormQRCode(stringSplit, pBPCurrency, pReqDate, mQrConfig, pPageKey);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        [HttpPost]
        public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
        {
            List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
            return PartialView("_ListSE", listSE);
        }

        public PartialViewResult _ItemsFormQRCode(string[] pItems, string pBPCurrency, DateTime? pReqDate, QRConfigView mQrConfig, string pPageKey)
        {
            //Get User Setting
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            List<SalesTaxCodesSAP> mTaxCodeList = mPQManagerView.GetTaxCode(((UserView)Session["UserLogin"]).CompanyConnect);
            SalesTaxCodesSAP mDefTAxCode;
            if (!string.IsNullOrEmpty(userSetting.PurchaseTaxCodeDef))
            {
                mDefTAxCode = mTaxCodeList.Where(t => t.Code == userSetting.PurchaseTaxCodeDef).Single();
            }
            else
            {
                mDefTAxCode = new SalesTaxCodesSAP() { Code = "", Name = "", Rate = 0 };
            }

            List<PurchaseQuotationLineView> ListReturn = null;
            PurchaseQuotationLineView ListExistingItem = null;
            List<PurchaseQuotationLineView> ListReturnExit = (List<PurchaseQuotationLineView>)TempData["ListItemsExit" + pPageKey];
            List<CurrencySAP> ListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            try
            {
                string pItemsAux = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];

                if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
                {
                    ListExistingItem = ListReturnExit.Where(c => c.ItemCode == pItemsAux).FirstOrDefault();
                }
                //Si esta lista es distinta de null solo debo sumarle + 1 a la quantity puesto que el item ya existe en las lineas de la orden
                if (ListExistingItem != null && ListReturnExit != null)
                {
                    //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                    ListReturnExit.Where(c => c.ItemCode == ListExistingItem.ItemCode).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]); return k; }).ToList();
                    ListReturn = ListReturnExit;
                }
                //Debo agregar la linea nueva para ese item
                else
                {
                    int mLine = 0;

                    if (ListReturnExit != null)
                    {
                        mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
                    }

                    PurchaseQuotationLineView mPQLine = new PurchaseQuotationLineView();
                    mPQLine.ItemCode = pItems[(mQrConfig.ItemCodePosition ?? 0) - 1];
                    GetItems(pPageKey, null, "", mPQLine.ItemCode);
                    List<ItemMasterView> ListItems = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).ToList();
                    ListItems = mItemMasterManagerView.GetItemsPrice(((UserView)Session["UserLogin"]).CompanyConnect, ListItems, "", ((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection);


                    if (mQrConfig.CurrencyPosition != null)
                        mPQLine.Currency = pItems[(mQrConfig.CurrencyPosition ?? 0) - 1];
                    if (mQrConfig.DscriptionPosition != null)
                        mPQLine.Dscription = pItems[(mQrConfig.DscriptionPosition ?? 0) - 1];
                    if (mQrConfig.FreeTxtPosition != null)
                        mPQLine.FreeTxt = pItems[(mQrConfig.FreeTxtPosition ?? 0) - 1];
                    if (mQrConfig.GLAccountPosition != null)
                    {
                        mPQLine.GLAccount = new GLAccountView();
                        mPQLine.GLAccount.FormatCode = pItems[(mQrConfig.GLAccountPosition ?? 0) - 1];
                    }
                    if (mQrConfig.OcrCodePosition != null)
                        mPQLine.OcrCode = pItems[(mQrConfig.OcrCodePosition ?? 0) - 1];

                    if (mQrConfig.PricePosition != null)
                    {
                        mPQLine.Price = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                        mPQLine.PriceBefDi = Convert.ToDecimal(pItems[(mQrConfig.PricePosition ?? 0) - 1]);
                    }
                    else
                    {
                        UnitOfMeasure mDefaultUOM = ListItems.FirstOrDefault().UnitOfMeasureList.Where(k => k.LineNum == 1).FirstOrDefault();
                        mPQLine.Price = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Price ?? 0);
                        mPQLine.PriceBefDi = mPQLine.Price;
                        mPQLine.Currency = (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault() != null ? (ListItems.FirstOrDefault().ItemPrices.Where(j => j.UOMCode == mDefaultUOM.UomEntry.ToString()).FirstOrDefault().Currency ?? "") : "");
                        mPQLine.UnitOfMeasureList = ListItems.FirstOrDefault().UnitOfMeasureList;
                        mPQLine.ItemPrices = ListItems.FirstOrDefault().ItemPrices;
                    }

                    //Rates
                    mPQLine.TaxCode = mDefTAxCode.Code;
                    mPQLine.VatPrcnt = mDefTAxCode.Rate;

                    if (mQrConfig.QuantityPosition != null)
                        mPQLine.Quantity = Convert.ToDecimal(pItems[(mQrConfig.QuantityPosition ?? 0) - 1]);

                    if (mQrConfig.WhsCodePosition != null)
                    {
                        mPQLine.WhsCode = pItems[(mQrConfig.WhsCodePosition ?? 0) - 1];
                    }
                    else
                    {
                        mPQLine.WhsCode = ListItems.FirstOrDefault().DfltWH;
                    }

                    mPQLine.LineNum = mLine + 1;
                    mPQLine.ListCurrency = ListCurrency;
                    if (ListReturnExit == null)
                        ListReturnExit = new List<PurchaseQuotationLineView>();
                    ListReturnExit.Add(mPQLine);
                    ListReturn = ListReturnExit;
                }
                List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates" + pPageKey];
                double mRateGl = ListRate.Where(c => c.Currency == pBPCurrency).Select(c => c.Rate).FirstOrDefault();
                TempData["Rates" + pPageKey] = ListRate;
                ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; c.Currency = (string.IsNullOrEmpty(c.Currency) ? pBPCurrency : c.Currency); c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

                //TempData["ListItems" + pPageKey] = ListReturn;
                TempData["ListItemsExit" + pPageKey] = ListReturn;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                ViewBag.CalculateRate = "Y";

                return PartialView("_ItemsForm", Tuple.Create(ListReturn, pBPCurrency, userSetting, pReqDate));
            }
            catch (Exception ex)
            {
                TempData["ListItemsExit" + pPageKey] = ListReturnExit;
                TempData["ListCurrency" + pPageKey] = ListCurrency;
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult GetUnitOfMeasure(string ItemCode)
        {
            try
            {
                List<UnitOfMeasureView> mUOM = mItemMasterManagerView.GeUnitOfMeasureByItem(((UserView)Session["UserLogin"]).CompanyConnect, ItemCode);
                return Json(mUOM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        [HttpPost]
        public PartialViewResult UploadAttachment(FormCollection pData)
        {
            string mFileName = string.Empty;
            string mPageKey = pData["pPageKey"].ToString();
            AttachmentView mAttachment = new AttachmentView();
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + mPageKey];
            try
            {
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;
                    AttachmentView mObj = new AttachmentView();
                    foreach (string mKey in mFiles)
                    {
                        HttpPostedFileBase mFilePost = mFiles[mKey];
                        mFileName = mFilePost.FileName;
                        string path = "~/images/" + mFileName;
                        mFileName = Server.MapPath(path);

                        if (System.IO.File.Exists(mFileName))
                        {
                            throw new Exception(PurchaseOrders.TFAE);
                        }

                        mObj.FileName = Path.GetFileNameWithoutExtension(mFilePost.FileName);
                        mObj.FileExt = Path.GetExtension(mFilePost.FileName).Replace(".", "");
                        mObj.srcPath = Server.MapPath("~/images");
                        mObj.Date = DateTime.Today;

                        if (mFileList.Count == 0)
                        {
                            mObj.Line = 0;
                        }
                        else
                        {
                            mObj.Line = mFileList.Max(c => c.Line) + 1;
                        }

                        mFilePost.SaveAs(mFileName);
                        mFileList.Add(mObj);
                    }

                }

                TempData["Attachments" + mPageKey] = mFileList;

                return PartialView("_FilesList", mFileList);
            }
            catch (Exception ex)
            {
                TempData["Attachments" + mPageKey] = mFileList;
                throw ex;
            }

        }

        [HttpPost]
        public PartialViewResult RemovedAtt(int pId, string pPageKey)
        {
            List<AttachmentView> mFileList = (List<AttachmentView>)TempData["Attachments" + pPageKey];

            mFileList.RemoveAll(c => c.Line == pId);

            TempData["Attachments" + pPageKey] = mFileList;

            return PartialView("_FilesList", mFileList);
        }

        [HttpPost]
        public PartialViewResult GetItemsModel()
        {
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return PartialView("_Items", mOITMUDFList);
        }
    }
}