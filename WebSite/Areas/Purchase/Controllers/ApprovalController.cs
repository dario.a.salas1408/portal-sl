﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.Purchase.Controllers
{
	[CustomerAuthorize(Enums.Areas.Purchase)]
	public class ApprovalController : Controller
	{
		private ApprovalManagerView mApprovalManagerView;
		private UserManagerView mUserManagerView;
		private DraftManagerView mDraftManagerView;

		public ApprovalController()
		{
			mApprovalManagerView = new ApprovalManagerView();
			mUserManagerView = new UserManagerView();
			mDraftManagerView = new DraftManagerView();
		}

		[CustomerAuthorize(Enums.Areas.Purchase, Enums.Pages.PurchaseApproval, Enums.Actions.List)]
		public ActionResult Index()
		{
			try
			{
				List<DocumentConfirmationLineView> listDC = new List<DocumentConfirmationLineView>();
				string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

				return View(Tuple.Create(listDC, listDC));
			}
			catch (Exception ex)
			{
				TempData["Error"] = ex.InnerException;
				return RedirectToAction("Index", "Error");
			}
		}

		public PartialViewResult ListApproval(string txtDocDate = "", string txtDocStatus = "", string txtObjectId = "")
		{
			string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();
			DateTime? mDocDate = null;
			if (txtDocDate != "")
			{
				mDocDate = Convert.ToDateTime(txtDocDate);
			}
			List<DocumentConfirmationLineView> documentConfirmationList = mApprovalManagerView.GetApprovalListSearch(
				((UserView)Session["UserLogin"]).CompanyConnect,
				((UserView)Session["UserLogin"]).IdUser, mDocDate, txtDocStatus, txtObjectId);

			List<UserSAP> userList = mUserManagerView.GetUserSapList(((UserView)Session["UserLogin"]).CompanyConnect);
			documentConfirmationList = documentConfirmationList.Select(c =>
			{
				c.UserSignName = (userList.Where(j => j.USERID == c.UserSign).FirstOrDefault() != null ? userList.Where(j => j.USERID == c.UserSign).FirstOrDefault().U_NAME : ""); return c;
			}).ToList();

			TempData["ListDC"] = documentConfirmationList;
			TempData["ListUsers"] = userList;

			return PartialView("_ApprovalList", documentConfirmationList);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="txtDocDate"></param>
		/// <param name="txtDocStatus"></param>
		/// <param name="txtObjectId"></param>
		/// <returns></returns>
		public PartialViewResult ListApprovalByOriginator(string txtDocDate = "", string txtDocStatus = "", string txtObjectId = "")
		{
			string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();
			DateTime? mDocDate = null;
			if (txtDocDate != "")
			{
				mDocDate = Convert.ToDateTime(txtDocDate);
			}
			List<DocumentConfirmationLineView> listConfirmationViews = mApprovalManagerView.GetPurchaseApprovalByOriginator(
				((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser,
				mDocDate,
				txtDocStatus, txtObjectId);

			List<UserSAP> userList = mUserManagerView.GetUserSapList(((UserView)Session["UserLogin"]).CompanyConnect);

			listConfirmationViews = listConfirmationViews.Select(c => { c.UserSignName = 
				(userList.Where(j => j.USERID == c.UserSign).FirstOrDefault() != null ? 
				userList.Where(j => j.USERID == c.UserSign).FirstOrDefault().U_NAME : "");
				c.ByOriginator = true; return c; }).ToList();

			return PartialView("_ApprovalList", listConfirmationViews);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="WddCode"></param>
		/// <returns></returns>
		public PartialViewResult GetApprovalDesicion(string WddCode)
		{
			int mWddCode = Convert.ToInt32(WddCode);
			List<DocumentConfirmationLineView> listConfirmationViews = (List<DocumentConfirmationLineView>)TempData["ListDC"];
			TempData["ListDC"] = listConfirmationViews;
			return PartialView("_ApprovalTemplate", listConfirmationViews.Where(c => c.WddCode == mWddCode).FirstOrDefault());
		}

		public void SaveApprovalResponse(string owwdCode, string remark, string approvalCode)
		{
			UsersSetting userPortal = mApprovalManagerView.GetUserPortal(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);
			mApprovalManagerView.SaveApprovalResponse(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(owwdCode), remark, userPortal, approvalCode);
		}

		public void CreateDocument(string DraftDocEntry)
		{
			mDraftManagerView.CreateDocument(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(DraftDocEntry));
		}

		public PartialViewResult _GetHistory(int WddCode)
		{
			List<UserSAP> ListUserSAP = (List<UserSAP>)TempData["ListUsers"];
			List<StageView> ListStage = mApprovalManagerView.GetStageList(((UserView)Session["UserLogin"]).CompanyConnect);

			List<DocumentConfirmationLineView> listReturn = mApprovalManagerView.GetApprovalListByID(((UserView)Session["UserLogin"]).CompanyConnect, WddCode);
			listReturn = listReturn.Select(c => { c.UserName = ListUserSAP.Where(j => j.USERID == c.UserID).FirstOrDefault().U_NAME; c.StageName = ListStage.Where(k => k.WstCode == c.StepCode).FirstOrDefault().Name; return c; }).ToList();

			TempData["ListUsers"] = ListUserSAP;

			return PartialView("_ApprovalHistory", listReturn);
		}
	}
}