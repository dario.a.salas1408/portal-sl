﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.View.AdvanceSearchViews;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Draft;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PurchaseRequest;
using AutoMapper;
using DataTables.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Areas.Purchase.Controllers
{
    [CustomerAuthorize(Enums.Areas.Purchase)]
    public class PurchaseRequestController : Controller
    {
        private PurchaseRequestManagerView mPRManagerView;
        private ItemMasterManagerView mItemMasterManagerView;
        private DraftManagerView mDraftManagerView;
        private PDMManagerView mPDMManagerView;
        private UserManagerView mUserManagerView;
        private CompaniesManagerView mCompaniesManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;

        public PurchaseRequestController()
        {
            mPRManagerView = new PurchaseRequestManagerView();
            mItemMasterManagerView = new ItemMasterManagerView();
            mDraftManagerView = new DraftManagerView();
            mUserManagerView = new UserManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();

            Mapper.CreateMap<DraftView, PurchaseRequestView>();
            Mapper.CreateMap<DraftLineView, PurchaseRequestLineView>();
            Mapper.CreateMap<UserUDFView, UDF_ARGNS>();
            Mapper.CreateMap<UDF_ARGNS, UserUDFView>();
            mPDMManagerView = new PDMManagerView();

        }

        [CustomerAuthorize(Enums.Areas.Purchase, Enums.Pages.PurchaseRequest, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                List<string> ListString = new List<String>();
                List<PurchaseRequestView> ListPR = new List<PurchaseRequestView>();
                PurchaseRequestCombo ListPRCombo = new PurchaseRequestCombo();
                ListPRCombo = mPRManagerView.GetPurchaseRequestCombo(((UserView)Session["UserLogin"]).CompanyConnect);

                string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

                if (System.Web.HttpRuntime.Cache["PReqList" + mUser] != null)
                {
                    ListPR = (List<PurchaseRequestView>)System.Web.HttpRuntime.Cache["PReqList" + mUser];
                }

                return View(Tuple.Create(ListPR, ListString, ListPRCombo));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }


        [HttpPost]
        public PartialViewResult _Requesters(int pUserType, string pRequesterCode = "", string pRequesterName = "")
        {
            pRequesterCode = pRequesterCode.TrimEnd().TrimStart();
            pRequesterName = pRequesterName.TrimEnd().TrimStart();
            switch (pUserType)
            {
                case 12:
                    return PartialView("_UsersSap", mPRManagerView.GetUserSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pRequesterCode, pRequesterName));
                case 171:
                    return PartialView("_EmployeesSap", mPRManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pRequesterCode, pRequesterName));
            }
            return null;
        }

        [HttpPost]
        public ActionResult GetListPR([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            Mapper.CreateMap<OrderColumn, Column>();
            Mapper.CreateMap<Column, OrderColumn>();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            DateTime? Date = null;
            int? DocNum = null;
            int? mDpto = null;
            string mUser = ((UserView)Session["UserLogin"]).IdUser.ToString();

            if (searchViewModel.pDate != "")
            {
                Date = Convert.ToDateTime(searchViewModel.pDate);
            }

            if (searchViewModel.pNro != "")
            {
                DocNum = Convert.ToInt32(searchViewModel.pNro);
            }
            if (searchViewModel.pDepartment != "")
            {
                mDpto = Convert.ToInt32(searchViewModel.pDepartment);
            }

            JsonObjectResult mJsonObjectResult = mPRManagerView.GetPurchaseRequestListSearch(((UserView)Session["UserLogin"]).CompanyConnect, searchViewModel.pRequester, Date, DocNum, mDpto, searchViewModel.pDocStatus, searchViewModel.pOwnerCode, searchViewModel.pSECode, requestModel.Start, requestModel.Length, Mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            
            return Json(new DataTablesResponse(requestModel.Draw, Mapper.Map<List<PurchaseRequestView>>(mJsonObjectResult.PurchaseRequestSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionPurchaseRequest(string ActionPurchaseRequest, int IdPR, string fromController)
        {
            PurchaseRequestView mPRView = null;
            PurchaseRequestCombo ListPRCombo = new PurchaseRequestCombo();
            ListPRCombo = mPRManagerView.GetPurchaseRequestCombo(((UserView)Session["UserLogin"]).CompanyConnect);
            ListPRCombo.RatesList = mPRManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, ListPRCombo.CurrencyList, DateTime.Today);
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            TempData["Rates" + mPageKey] = ListPRCombo.RatesList;
            TempData["ListCurrency" + mPageKey] = ListPRCombo.CurrencyList;
            TempData["PRCombo" + mPageKey] = ListPRCombo;
            TempData["ListItems" + mPageKey] = null;
            ReqTypeSAP reqUser = new ReqTypeSAP(12, "User");
            ReqTypeSAP reqEmployee = new ReqTypeSAP(171, "Employee");
            switch (fromController)
            {
                case "PurchaseRequest":
                    ViewBag.URLRedirect = "/Purchase/PurchaseRequest/Index";
                    ViewBag.controllerName = "PurchaseRequest";
                    break;
                case "MyDocuments":
                    ViewBag.URLRedirect = "/Purchase/Draft/Index";
                    ViewBag.controllerName = "Draft";
                    break;
                case "MyApprovals":
                    ViewBag.URLRedirect = "/Purchase/Approval/Index";
                    ViewBag.controllerName = "Approval";
                    break;
                default:
                    ViewBag.URLRedirect = "/Home";
                    ViewBag.controllerName = "Home";
                    break;
            }
            switch (ActionPurchaseRequest)
            {
                case "Add":
                    ViewBag.Tite = PurchaseRequest.APR;
                    ViewBag.FormMode = ActionPurchaseRequest;
                    mPRView = mPRManagerView.GetPurchaseRequest(0, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPRView.MappedUdf = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OPRQ");

                    List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
                    UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
                    List<UserSAP> userSAPList = mUserManagerView.GetUserSapList(((UserView)Session["UserLogin"]).CompanyConnect);
                    mPRView.Requester = userSetting.UserCodeSAP;
                    mPRView.ReqName = userSAPList.Where(c => c.USER_CODE == userSetting.UserCodeSAP).FirstOrDefault().U_NAME;

                    mPRView.DocDate = System.DateTime.Now;
                    mPRView.DocDueDate = System.DateTime.Now;
                    mPRView.TaxDate = System.DateTime.Now;
                    mPRView.ReqDate = System.DateTime.Now;
                    mPRView.ListReqType.Add(reqUser);
                    mPRView.ListReqType.Add(reqEmployee);
                    TempData["ListItemsExit" + mPageKey] = null;
                    break;

                case "View":
                case "Update":
                    if (ActionPurchaseRequest == "View")
                        ViewBag.Tite = PurchaseRequest.VPR;
                    else
                        ViewBag.Tite = PurchaseRequest.UPR;
                    ViewBag.FormMode = ActionPurchaseRequest;
                    mPRView = mPRManagerView.GetPurchaseRequest(IdPR, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPRView.Lines.Select(c => { c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); return c; }).ToList();
                    mPRView.ListReqType.Add(reqUser);
                    mPRView.ListReqType.Add(reqEmployee);
                    TempData["ListItemsExit" + mPageKey] = mPRView.Lines;
                    break;

                case "ViewDraft":
                    ViewBag.Tite = Drafts.PRD;
                    ViewBag.FormMode = "View";
                    DraftView draft = mDraftManagerView.GetDraftById(IdPR, ((UserView)Session["UserLogin"]).CompanyConnect);
                    mPRView = new PurchaseRequestView();
                    mPRView = Mapper.Map<PurchaseRequestView>(draft);
                    mPRView.Lines = Mapper.Map<List<PurchaseRequestLineView>>(draft.Lines);
                    mPRView.Lines.Select(c => { c.OcrCode = (c.OcrCode == null ? "" : c.OcrCode); return c; }).ToList();
                    mPRView.ListReqType.Add(reqUser);
                    mPRView.ListReqType.Add(reqEmployee);
                    mPRView.MappedUdf = new List<UDF_ARGNS>();
                    TempData["ListItemsExit" + mPageKey] = mPRView.Lines;
                    break;
            }
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            mPRView.PurchaseRequestCombo = ListPRCombo;
            return View("_PurchaseRequest", Tuple.Create(mPRView, ((UserView)Session["UserLogin"]).CompanyConnect, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.PurchaseRequest, ((UserView)Session["UserLogin"]).CompanyConnect), mOITMUDFList));
        }

        [HttpPost]
        public JsonResult Add(PurchaseRequestView model)
        {
            List<GLAccountView> GLAccountList = (List<GLAccountView>)TempData["ListGLAccount" + model.PageKey];
            List<PurchaseRequestLineView> mListPurchaseRequestLineView = ((List<PurchaseRequestLineView>)TempData["ListItemsExit" + model.PageKey]);

            model.Lines.Select(c => { c.ItemCode = ((List<PurchaseRequestLineView>)TempData["ListItemsExit" + model.PageKey]).Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListItemsExit" + model.PageKey] = mListPurchaseRequestLineView;
            TempData["ListGLAccount" + model.PageKey] = GLAccountList;

            string mResult = mPRManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);
        }

        [HttpPost]
        public JsonResult Update(PurchaseRequestView model)
        {
            List<GLAccountView> GLAccountList = (List<GLAccountView>)TempData["ListGLAccount" + model.PageKey];
            List<PurchaseRequestLineView> ListPurchaseRequestLineView = (List<PurchaseRequestLineView>)TempData["ListItemsExit" + model.PageKey];

            model.Lines.Select(c => { c.ItemCode = ListPurchaseRequestLineView.Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); c.AcctCode = (GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault() != null ? GLAccountList.Where(j => j.FormatCode == c.GLAccount.FormatCode).FirstOrDefault().AcctCode : null); return c; }).ToList();

            TempData["ListGLAccount" + model.PageKey] = GLAccountList;
            TempData["ListItemsExit" + model.PageKey] = ListPurchaseRequestLineView;

            string mResult = mPRManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect);

            return Json(mResult);
        }

        [HttpPost]
        public ActionResult _Items([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

                List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();

                JsonObjectResult mJsonObjectResult = GetItems(searchViewModel.pPageKey, searchViewModel.pMappedUdf, "", searchViewModel.pItemCode, searchViewModel.pItemName, requestModel.Start, requestModel.Length, mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

                return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public PartialViewResult _ItemsGrid(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pItemCode = "", string pItemData = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ItemMasterSAP, ItemMasterView>(); cfg.CreateMap<StockModel, StockModelView>(); }).CreateMapper();

            OrderColumn mOrderColumn = new OrderColumn();
            mOrderColumn.Name = "ItemCode";
            mOrderColumn.IsOrdered = true;
            mOrderColumn.SortDirection = OrderColumn.OrderDirection.Ascendant;
            pOrderColumn = mOrderColumn;

            JsonObjectResult mJsonObjectResult = GetItems(pPageKey, pMappedUdf, "", pItemCode, pItemData, pStart, pLength, pOrderColumn);

            return PartialView("_ItemsGrid", Tuple.Create(mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), pLength));
        }

        [HttpPost]
        public JsonResult _UpdateLinesChanged(List<PurchaseRequestLineView> pLines, string pPageKey)
        {
            List<PurchaseRequestLineView> ListReturnExit = (List<PurchaseRequestLineView>)TempData["ListItemsExit" + pPageKey];
            foreach (PurchaseRequestLineView Line in pLines)
            {
                PurchaseRequestLineView LineToUpdate = ListReturnExit.Where(c => c.LineNum == Line.LineNum).FirstOrDefault();
                LineToUpdate.Dscription = Line.Dscription;
                LineToUpdate.WhsCode = Line.WhsCode;
                LineToUpdate.OcrCode = Line.OcrCode;
                LineToUpdate.GLAccount = Line.GLAccount;
                LineToUpdate.FreeTxt = Line.FreeTxt;
                LineToUpdate.PQTReqDate = Line.PQTReqDate;
                LineToUpdate.Quantity = Line.Quantity;
                LineToUpdate.Price = Line.Price;
                LineToUpdate.PriceBefDi = Line.PriceBefDi;
                LineToUpdate.Currency = Line.Currency;
                LineToUpdate.UomCode = Line.UomCode;
                foreach (UDF_ARGNS mUDF in LineToUpdate.MappedUdf)
                {
                    UDF_ARGNS mUDFAux = Line.MappedUdf.Where(c => c.UDFName == mUDF.UDFName).FirstOrDefault();
                    mUDF.Value = mUDFAux.Value;
                }
            }
            TempData["ListItemsExit" + pPageKey] = ListReturnExit;
            return Json("Ok");
        }

        [HttpPost]
        public PartialViewResult _ItemsForm(string[] pItems, DateTime? pReqDate, string pPageKey)
        {
            List<PurchaseRequestLineView> ListReturn = null;
            List<PurchaseRequestLineView> ListExistingItems = new List<PurchaseRequestLineView>();

            List<PurchaseRequestLineView> ListReturnExit = (List<PurchaseRequestLineView>)TempData["ListItemsExit" + pPageKey];

            if (((UserView)Session["UserLogin"]).CompanyConnect.ItemInMultipleLines == false && ListReturnExit != null)
            {
                //Esta lista contiene los items que ya fueron agregados a una linea del documento (solo debo sumarle +1 a la quantity)
                ListExistingItems = ListReturnExit.Where(c => pItems.Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).ToList();
                //Esta lista contiene los items que no fueron agregados a una linea del documento (debo crear una nueva linea)
                pItems = pItems.Where(j => !ListExistingItems.Select(c => c.ItemCode).ToList().Contains(j)).ToArray();
            }

            if (pItems.Length > 0)
            {
                int mLine = 0;

                if (ListReturnExit != null)
                {
                    mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
                }

                //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
                List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "PRQ1");

                List<PurchaseRequestLineView> ListOitm = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).Select(c => new PurchaseRequestLineView { LineStatus = "O", Dscription = c.ItemName, ItemCode = c.ItemCode, Currency = c.LastPurCur, LineNum = (mLine = mLine + 1), Price = c.LastPurPrc ?? 0, Quantity = 1, WhsCode = c.DfltWH }).ToList();
                foreach (PurchaseRequestLineView mLineAux in ListOitm)
                {
                    //Clone the list of udf to a new list in mLineAux.MappedUdf
                    mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                    mListUserUDF.ForEach((item) =>
                    {
                        mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                    });
                }
                if (pItems != null)
                { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
                else
                { ListReturn = new List<PurchaseRequestLineView>(); }

                if (ListReturnExit != null)
                { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }
            }
            if (ListExistingItems.Count > 0)
            {
                //Hago un groupby en caso de que haya 2 items con el mismo código solo le sume +1 al primero de ellos
                ListExistingItems = ListReturnExit.Where(c => ListExistingItems.Select(j => j.ItemCode).Contains(c.ItemCode)).GroupBy(p => p.ItemCode).Select(g => g.First()).Select(k => { k.Quantity = k.Quantity + 1; return k; }).ToList();
                if (ListReturn != null)
                { ListReturn = ListReturn.Union(ListExistingItems).ToList(); }
                else
                { ListReturn = ListReturnExit.Union(ListExistingItems).ToList(); }
            }
            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();

            TempData["ListItemsExit" + pPageKey] = ListReturn;

            PurchaseRequestCombo combo = (PurchaseRequestCombo)TempData["PRCombo" + pPageKey];
            TempData["PRCombo" + pPageKey] = combo;

            return PartialView("_ItemsForm", Tuple.Create(ListReturn, combo, userSetting, pReqDate));

        }

        public JsonResult UpdateRateList(DateTime DocDate, string pPageKey)
        {
            List<CurrencySAP> mListCurrency = (List<CurrencySAP>)TempData["ListCurrency" + pPageKey];
            List<RatesSystem> ListRates = mPRManagerView.GetListRates(((UserView)Session["UserLogin"]).CompanyConnect, mListCurrency, DocDate);
            TempData["Rates" + pPageKey] = ListRates;
            TempData["ListCurrency" + pPageKey] = mListCurrency;
            try
            {
                if (ListRates != null)
                {
                    return Json(ListRates, JsonRequestBehavior.AllowGet);
                }
                else
                { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public JsonResult DeleteRow(string ItemCode, string pPageKey)
        {
            try
            {
                List<PurchaseRequestLineView> ListReturnExit = (List<PurchaseRequestLineView>)TempData["ListItemsExit" + pPageKey];

                ListReturnExit.RemoveAll(c => c.LineNum == Convert.ToInt32(ItemCode));

                TempData["ListItemsExit" + pPageKey] = ListReturnExit;

                return Json("Ok");
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        private JsonObjectResult GetItems(string pPageKey, List<UDF_ARGNS> pMappedUdf, string pModeCode = "", 
            string pItemCode = "", string pItemName = "", 
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
                pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                        .CompanyStock.FirstOrDefault()
                                        .WarehousePortalList;

                IMapper mapper = new MapperConfiguration(cfg => 
                {
                    cfg.CreateMap<ItemMasterSAP, ItemMasterView>();
                    cfg.CreateMap<ItemMasterView, ItemMasterSAP>();
                    cfg.CreateMap<StockModel, StockModelView>();
                    cfg.CreateMap<StockModelView, StockModel>(); })
                    .CreateMapper();
                
                JsonObjectResult mJsonObjectResult = mItemMasterManagerView.GetOITMListBy(
                    ((UserView)Session["UserLogin"]).CompanyConnect, 
                    "Y", "", "", "N", "", pMappedUdf, pModeCode, 
                    ((UserView)Session["UserLogin"]).IdUser, pItemCode, pItemName, true,
                    Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), false, 
                    "", "", pStart, pLength, pOrderColumn);

                List<ItemMasterView> ListReturn = mapper.Map<List<ItemMasterView>>(mJsonObjectResult.ItemMasterSAPList);

                //if pLength is greatter than 0, i need to page the result, then i need to add the actual page to the previous ListItems
                if (pLength > 0)
                {
                    List<ItemMasterView> mListItemsAux = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]);
                    //If its the first time that i enter to GetItems and the TempData of ListItems is null i inizialice the variable.
                    mListItemsAux = (mListItemsAux == null ? new List<ItemMasterView>() : mListItemsAux);
                    //if the item not exist in mListItemsAux List i add it, else i do nothing
                    mListItemsAux.AddRange(ListReturn.Where(c => mListItemsAux.Select(x => x.ItemCode).Contains(c.ItemCode) == false).ToList());
                    TempData["ListItems" + pPageKey] = mListItemsAux;
                }
                else
                {
                    TempData["ListItems" + pPageKey] = ListReturn;
                }
                mJsonObjectResult.ItemMasterSAPList = mapper.Map<List<ItemMasterSAP>>(ListReturn);

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseRequestController --> GetItems: "  + ex.Message);
                Logger.WriteError("PurchaseRequestController --> GetItems: " + ex.InnerException.ToString());
                return new JsonObjectResult();
            }
        }

        #region Matrix Model

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModelCode"></param>
        /// <param name="pModelName"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelListSearch(AdvancedSearchStylesView searchViewModel)
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            IMapper mapper = new MapperConfiguration(
                cfg => { cfg.CreateMap<Column, OrderColumn>(); }
                ).CreateMapper();

            mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ModelDesc, ModelDescView>();
            }).CreateMapper();

            bool permissionToViewStock = ((UserView)Session["UserLogin"]).ListUserPageAction
                    .Where(c => c.PageInternalKey == Enums.Pages.PurchaseRequest.ToDescriptionString()
                    && c.InternalKey == Enums.Actions.StockMKTDoc.ToDescriptionString())
                    .FirstOrDefault() != null ? true : false;

            JsonObjectResult mJsonObjectResult = mPDMManagerView.GetPDMListDesc(
                ((UserView)Session["UserLogin"]).CompanyConnect, 
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList), 
                permissionToViewStock, searchViewModel.Start, searchViewModel.Length,
                searchViewModel.CodeModel, searchViewModel.NameModel);

            return PartialView("_ModelList", Tuple.Create(
             mapper.Map<List<ModelDescView>>(mJsonObjectResult.ModelDescViewList),
                 Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                 searchViewModel.Length));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pModelCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _ModelDetail(string pPageKey, string pModelCode = "")
        {
            List<WarehousePortalView> pWarehousePortalList = new List<WarehousePortalView>();
            pWarehousePortalList = ((UserView)Session["UserLogin"]).CompanyConnect
                                    .CompanyStock.FirstOrDefault()
                                    .WarehousePortalList;

            MatrixModelView mModelObj = mPDMManagerView.GetMatrixModelView(
                ((UserView)Session["UserLogin"]).CompanyConnect, pModelCode,
                Mapper.Map<List<WarehousePortal>>(pWarehousePortalList));

            GetItems(pPageKey, null, pModelCode, "", "");
            return PartialView("_ModelMatrixDetail", mModelObj); 
        }

        //Verificar este metodo para implementarlo Mejor.
        [HttpPost]
        public PartialViewResult _ItemsFormMatrixModel(string pItemsQty, string[] pItems, string pCurrency, DateTime? pReqDate, string pPageKey)
        {
            ICollection<JsonObject> jsonModel = JsonConvert.DeserializeObject<ICollection<JsonObject>>(pItemsQty);
            List<PurchaseRequestLineView> ListReturn;

            List<PurchaseRequestLineView> ListReturnExit = (List<PurchaseRequestLineView>)TempData["ListItemsExit" + pPageKey];

            int mLine = 0;

            if (ListReturnExit != null)
            {
                mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Select(c => c.LineNum).Max();
            }

            //Busco los UDFs para las lineas de este documento y los alamaceno en un diccionario que luego voy a asignar a las nuevas lineas
            List<UDF_ARGNS> mListUserUDF = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "PRQ1");

            List<PurchaseRequestLineView> ListOitm = ((List<ItemMasterView>)TempData["ListItems" + pPageKey]).Where(c => pItems.Contains(c.ItemCode)).Select(c => new PurchaseRequestLineView { Dscription = c.ItemName, ItemCode = c.ItemCode, Currency = c.LastPurCur, LineNum = (mLine = mLine + 1), Price = c.LastPurPrc ?? 0, Quantity = 1 }).ToList();
            foreach (PurchaseRequestLineView mLineAux in ListOitm)
            {
                //Clone the list of udf to a new list in mLineAux.MappedUdf
                mLineAux.MappedUdf = new List<UDF_ARGNS>(mListUserUDF.Count);
                mListUserUDF.ForEach((item) =>
                {
                    mLineAux.MappedUdf.Add(new UDF_ARGNS(item));
                });
            }

            foreach (PurchaseRequestLineView item in ListOitm)
            {
                foreach (var mQty in jsonModel)
                {
                    if (item.ItemCode.Contains(mQty.Code))
                    {
                        item.Quantity = Convert.ToDecimal(mQty.Qty);
                    }
                }
            }

            if (pItems != null)
            { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
            else
            { ListReturn = new List<PurchaseRequestLineView>(); }

            if (ListReturnExit != null)
            { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }
            ListReturn = ListReturn.Select(c => { c.LineStatus = "O"; return c; }).ToList();

            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            UsersSettingView userSetting = userSettingsList.Where(c => (c.IdUser == ((UserView)Session["UserLogin"]).IdUser) && (c.IdCompany == ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany)).FirstOrDefault();
            
            TempData["ListItemsExit" + pPageKey] = ListReturn;

            PurchaseRequestCombo combo = (PurchaseRequestCombo)TempData["PRCombo" + pPageKey];
            TempData["PRCombo" + pPageKey] = combo;

            return PartialView("_ItemsForm", Tuple.Create(ListReturn, combo, userSetting, pReqDate));

        }
        #endregion

        [HttpPost]
        public JsonResult _GetDistributionRuleList()
        {
            try
            {
                List<ListItem> DistributionRuleList = mUserManagerView.GetDistributionRuleList(((UserView)Session["UserLogin"]).CompanyConnect);

                return Json(DistributionRuleList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public JsonResult _GetWarehouseList()
        {
            try
            {
                List<ListItem> WhList = mUserManagerView.GetWarehouseList(((UserView)Session["UserLogin"]).CompanyConnect);

                return Json(WhList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public JsonResult _GetGLAccountList(string pPageKey)
        {
            try
            {
                List<GLAccountView> GLAccountList = mItemMasterManagerView.GetGLAccountList(((UserView)Session["UserLogin"]).CompanyConnect);
                TempData["ListGLAccount" + pPageKey] = GLAccountList;
                List<ListItem> ListReturn = GLAccountList.Select(c => new ListItem { Value = c.FormatCode, Text = c.AcctName }).ToList();

                return Json(ListReturn, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public PartialViewResult _GetHandheldItem(DateTime? pReqDate, string pPageKey, string pItemCode = "")
        {
            ItemMasterView oItemMaster = mItemMasterManagerView.GeItemByCodeBar(((UserView)Session["UserLogin"]).CompanyConnect, "Y", "N", pItemCode);
            if (oItemMaster != null)
            {
                GetItems(pPageKey, null, "", oItemMaster.ItemCode);
                string[] pItems = new string[1];
                pItems[0] = oItemMaster.ItemCode;
                return _ItemsForm(pItems, pReqDate, pPageKey);
            }
            else
            { return null; }
        }

        [HttpPost]
        public PartialViewResult _GetSalesEmployee(string pEmployeeName = "")
        {
            List<ListItem> listSE = mUserManagerView.GetSalesEmployeeSearchByName(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeName);
            return PartialView("_ListSE", listSE);
        }

        [HttpPost]
        public PartialViewResult _Employee(string pEmployeeCode = "", string pEmployeeName = "")
        {
            return PartialView("_EmployeesSap", mPRManagerView.GetEmployeeSAPSearchByCode(((UserView)Session["UserLogin"]).CompanyConnect, pEmployeeCode, pEmployeeName));
        }

        [HttpPost]
        public JsonResult GetUnitOfMeasure(string ItemCode)
        {
            try
            {
                List<UnitOfMeasureView> mUOM = mItemMasterManagerView.GeUnitOfMeasureByItem(((UserView)Session["UserLogin"]).CompanyConnect, ItemCode);
                return Json(mUOM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        [HttpPost]
        public PartialViewResult GetItemsModel()
        {
            List<UDF_ARGNS> mOITMUDFList = mUserManagerView.GetUserUDFSearch(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, "OITM");
            return PartialView("_Items", mOITMUDFList);
        }
    }
}