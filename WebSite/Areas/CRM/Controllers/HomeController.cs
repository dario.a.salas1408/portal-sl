﻿using ARGNS.ManagerView;
using ARGNS.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.CRM.Controllers
{
    public class HomeController : Controller
    {
        private ChartManagerView mChartManagerView;
        private AreaKeyManagerView mAreasKeyManagerView;

        public HomeController()
        {
            mChartManagerView = new ChartManagerView();
            mAreasKeyManagerView = new AreaKeyManagerView();
        }

        // GET: ServiceCall/Home
        public ActionResult Index()
        {
            AreaKeyView mAreaKey = mAreasKeyManagerView.GetAreasKeyList().Where(c => c.Area.ToUpper() == Util.Enums.Areas.CRM.ToString().ToUpper()).FirstOrDefault();
            List<PortalChartView> mListPortalChartView = mChartManagerView.GetPortalChartListSearch(mAreaKey.IdAreaKeys.ToString(), ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, ((UserView)Session["UserLogin"]).UserGroupObj);
            return View(mListPortalChartView);
        }
    }
}