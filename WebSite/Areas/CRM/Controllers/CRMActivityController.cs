﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PLM.Activity;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.CRM.Controllers
{
    [CustomerAuthorize(Enums.Areas.CRM)]
    public class CRMActivityController : Controller
    {
        private ActivitiesManagerView mManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;

        /// <summary>
        /// 
        /// </summary>
        public CRMActivityController()
        {
            mManagerView = new ActivitiesManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.CRM, Enums.Pages.CRMActivities, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                ActivitiesSAPComboView mActivityCombo = mManagerView.GetActivityCombo(((UserView)Session["UserLogin"]).CompanyConnect);

                List<ActivityAction> mActivityActionList = new List<ActivityAction>();
                mActivityActionList.Add(new ActivityAction("-1", ""));
                mActivityActionList.AddRange(mActivityCombo.ActivityActionList);
                mActivityCombo.ActivityActionList = mActivityActionList;

                List<ActivityPriority> mActivityPriorityList = new List<ActivityPriority>();
                mActivityPriorityList.Add(new ActivityPriority("-1", ""));
                mActivityPriorityList.AddRange(mActivityCombo.ActivityPriorityList);
                mActivityCombo.ActivityPriorityList = mActivityPriorityList;

                List<ActivityTypeSAP> mActivityTypeList = new List<ActivityTypeSAP>();
                mActivityTypeList.Add(new ActivityTypeSAP(-2, ""));
                mActivityTypeList.AddRange(mActivityCombo.ActivityTypeList);
                mActivityCombo.ActivityTypeList = mActivityTypeList;

                mActivityCombo.ActivityStatusList = new List<ActivityStatusSAP>();
                mActivityCombo.ActivityStatusList.Add(new ActivityStatusSAP(-1, ""));
                mActivityCombo.ActivityStatusList.AddRange(mManagerView.GetActivityStatusList(((UserView)Session["UserLogin"]).CompanyConnect));

                mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
                {
                    new ActivityUserType(-1,""),
                    new ActivityUserType(0,"User"),
                    new ActivityUserType(1,"Employee")
                };
                return View(mActivityCombo);
            }

            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserType"></param>
        /// <param name="txtCode"></param>
        /// <param name="txtAction"></param>
        /// <param name="txtType"></param>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="txtPriority"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <param name="txtBPCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult ActivityList(int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode)
        {
            if (txtStatus == "-1")
                txtStatus = null;
            List<ActivitiesView> mActivityList = mManagerView.GetCRMActivitiesSeach(((UserView)Session["UserLogin"]).CompanyConnect, UserType, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks, txtBPCode);
            ActivitiesSAPComboView mActivityCombo = new ActivitiesSAPComboView();
            mActivityCombo.ActivityStatusList = mManagerView.GetActivityStatusList(((UserView)Session["UserLogin"]).CompanyConnect);
            mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
            {
                new ActivityUserType(-1,""),
                new ActivityUserType(0,"User"),
                new ActivityUserType(1,"Employee")
            };
            return PartialView("_ActivityList", Tuple.Create(mActivityList, mActivityCombo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pAction"></param>
        /// <param name="pCode"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        public ActionResult ActivityAction(string pAction, string pCode, string pFrom)
        {
            try
            {
                ActivitiesView mView = null;

                switch (pAction)
                {
                    case "Add":
                        ViewBag.Tite = Activity.Addnew;
                        ViewBag.FormMode = pAction;
                        ViewBag.FromPage = pFrom;
                        mView = mManagerView.GetActivity(0, ((UserView)Session["UserLogin"]).CompanyConnect);
                        mView.Recontact = System.DateTime.Now;
                        mView.endDate = System.DateTime.Now;
                        break;

                    case "Update":
                        ViewBag.Tite = Activity.Editnew;
                        ViewBag.FormMode = pAction;
                        mView = mManagerView.GetActivity(Convert.ToInt32(pCode), ((UserView)Session["UserLogin"]).CompanyConnect);
                        break;

                    case "View":
                        ViewBag.Tite = Activity.ViewActivity;
                        ViewBag.FormMode = pAction;
                        mView = mManagerView.GetActivity(Convert.ToInt32(pCode), ((UserView)Session["UserLogin"]).CompanyConnect);
                        break;

                }

                return View("_Activity", Tuple.Create(mView, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.CRMActivities, ((UserView)Session["UserLogin"]).CompanyConnect)));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(ActivitiesView model)
        {
            if (model.status == -1)
                model.status = null;
            return Json(mManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(ActivitiesView model)
        {
            if (model.status == -1)
                model.status = null;
            return Json(mManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _Vendors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (((UserView)Session["UserLogin"]).BPCode == null)
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser
                    (((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.All, true, 
                    searchViewModel.pCardCode, searchViewModel.pCardName, 
                    ((UserView)Session["UserLogin"]).IsSalesEmployee, ((UserView)Session["UserLogin"]).SECode, 
                    ((UserView)Session["UserLogin"]).BPGroupId, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));
            }
            else
            {
                mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(
                    ((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.All, true, 
                    ((UserView)Session["UserLogin"]).BPCode, searchViewModel.pCardName, 
                    false, null, null, true, requestModel.Start, requestModel.Length, 
                    mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()), true);
            }

            return Json(new DataTablesResponse(requestModel.Draw, 
                mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), 
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), 
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUserType"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetActivityUserList(string pUserType)
        {
            List<ActivityUser> ret = mManagerView.GetActivityUserList(((UserView)Session["UserLogin"]).CompanyConnect, Convert.ToInt32(pUserType));

            if (ret != null)
            {
                return Json(ret, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json("", JsonRequestBehavior.AllowGet); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pAction"></param>
        /// <param name="pCode"></param>
        /// <param name="pFrom"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _Activity(string pAction, string pCode, string pFrom)
        {
            try
            {
                ActivitiesView mView = null;

                switch (pAction)
                {
                    case "Add":
                        ViewBag.Tite = Activity.Addnew;
                        ViewBag.FormMode = pAction;
                        ViewBag.FromPage = pFrom;
                        mView = mManagerView.GetActivity(0, ((UserView)Session["UserLogin"]).CompanyConnect);
                        mView.Recontact = System.DateTime.Now;
                        mView.endDate = System.DateTime.Now;
                        break;

                    case "Update":
                        ViewBag.Tite = Activity.Editnew;
                        ViewBag.FormMode = pAction;
                        ViewBag.FromPage = pFrom;
                        mView = mManagerView.GetActivity(Convert.ToInt32(pCode), ((UserView)Session["UserLogin"]).CompanyConnect);
                        break;

                }

                return PartialView("_Activity", Tuple.Create(mView, mCRPageMapManagerView.HasAPageCRMapping(Enums.Pages.CRMActivities, ((UserView)Session["UserLogin"]).CompanyConnect)));
            }
            catch (Exception ex)
            {
                return PartialView("_Activity");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="IsMyActivity"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActivityListCalendar(DateTime? txtStartDate, DateTime? txtEndDate, bool IsMyActivity)
        {
            List<ActivitiesView> mActivityList = mManagerView.GetCRMActivitiesListFromTo(((UserView)Session["UserLogin"]).CompanyConnect, txtStartDate, txtEndDate, ((UserView)Session["UserLogin"]).IdUser);

            return Json(mActivityList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCode"></param>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="HIni"></param>
        /// <param name="Hend"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateDragDrop(string pCode, DateTime txtStartDate, DateTime txtEndDate, string HIni, string Hend)
        {
            ActivitiesView model = mManagerView.GetActivity(Convert.ToInt32(pCode), ((UserView)Session["UserLogin"]).CompanyConnect);

            model.Recontact = txtStartDate;
            model.endDate = txtEndDate;
            model.BeginTime = Convert.ToInt32(HIni.Split(':')[0].ToString() + HIni.Split(':')[1].ToString());
            model.ENDTime = Convert.ToInt32(Hend.Split(':')[0].ToString() + Hend.Split(':')[1].ToString());

            return Json(mManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString()));
        }
    }
}