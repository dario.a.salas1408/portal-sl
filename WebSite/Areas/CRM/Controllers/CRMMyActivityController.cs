﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.PLM.Activity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.CRM.Controllers
{
    [CustomerAuthorize(Enums.Areas.CRM)]
    public class CRMMyActivityController : Controller
    {
        private ActivitiesManagerView mManagerView;
        private CRPageMapManagerView mCRPageMapManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;

        public CRMMyActivityController()
        {
            mManagerView = new ActivitiesManagerView();
            mCRPageMapManagerView = new CRPageMapManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        }

        [CustomerAuthorize(Enums.Areas.CRM, Enums.Pages.CRMMyActivities, Enums.Actions.List)]
        public ActionResult Index()
        {
            try
            {
                ActivitiesSAPComboView mActivityCombo = mManagerView.GetActivityCombo(((UserView)Session["UserLogin"]).CompanyConnect);
                mActivityCombo.ActivityStatusList.Where(c => c.statusID == -1).FirstOrDefault().name = Resources.Views.PLM.Activity.Activity.Status.ToString();

                List<ActivityAction> mActivityActionList = new List<ActivityAction>();
                mActivityActionList.Add(new ActivityAction("-1", Resources.Views.PLM.Activity.Activity.Action.ToString()));
                mActivityActionList.AddRange(mActivityCombo.ActivityActionList);
                mActivityCombo.ActivityActionList = mActivityActionList;

                List<ActivityPriority> mActivityPriorityList = new List<ActivityPriority>();
                mActivityPriorityList.Add(new ActivityPriority("-1", Resources.Views.PLM.Activity.Activity.Priority.ToString()));
                mActivityPriorityList.AddRange(mActivityCombo.ActivityPriorityList);
                mActivityCombo.ActivityPriorityList = mActivityPriorityList;

                List<ActivityTypeSAP> mActivityTypeList = new List<ActivityTypeSAP>();
                mActivityTypeList.Add(new ActivityTypeSAP(-2, Resources.Views.PLM.Activity.Activity.Type.ToString()));
                mActivityTypeList.AddRange(mActivityCombo.ActivityTypeList);
                mActivityCombo.ActivityTypeList = mActivityTypeList;

                mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
                {
                    new ActivityUserType(-1, Resources.Views.PLM.Activity.Activity.Assigned.ToString()),
                    new ActivityUserType(0,"User"),
                    new ActivityUserType(1,"Employee")
                };

                return View(mActivityCombo);
            }

            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        [HttpPost]
        public PartialViewResult ActivityList(int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            if (txtStatus == "-1")
                txtStatus = null;
            List<ActivitiesView> mActivityList = new List<ActivitiesView>();
            string code = ((UserView)Session["UserLogin"]).BPCode;
            if (((UserView)Session["UserLogin"]).IsCustomer && ((UserView)Session["UserLogin"]).BPCode != null)
            {
                mActivityList = mManagerView.GetBPCRMActivitiesSeach(((UserView)Session["UserLogin"]).CompanyConnect, UserType, ((UserView)Session["UserLogin"]).BPCode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
            }
            if (((UserView)Session["UserLogin"]).IsUser && ((UserView)Session["UserLogin"]).IdUser != null)
            {
                mActivityList = mManagerView.GetUserCRMActivitiesSeach(((UserView)Session["UserLogin"]).CompanyConnect, UserType, ((UserView)Session["UserLogin"]).IdUser, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
            }
            if (((UserView)Session["UserLogin"]).IsSalesEmployee && ((UserView)Session["UserLogin"]).SECode != null)
            {
                mActivityList = mManagerView.GetSalesEmployeeCRMActivitiesSeach(((UserView)Session["UserLogin"]).CompanyConnect, UserType, ((UserView)Session["UserLogin"]).SECode.Value, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
            }
            ActivitiesSAPComboView mActivityCombo = new ActivitiesSAPComboView();
            mActivityCombo.ActivityStatusList = mManagerView.GetActivityStatusList(((UserView)Session["UserLogin"]).CompanyConnect);
            mActivityCombo.ActivityUserTypeList = new List<ActivityUserType>()
            {
                new ActivityUserType(-1,""),
                new ActivityUserType(0,"User"),
                new ActivityUserType(1,"Employee")
            };
            return PartialView("_ActivityList", Tuple.Create(mActivityList, mActivityCombo));
        }
    }
}