﻿$(document).ready(function () {

    $('.input-group.date').datepicker({});

    $("#btnSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        var datetxtStartDate = $("#txtStartDate").datepicker("getDate");
        var datetxtEndDate = $("#txtEndDate").datepicker("getDate");
        $.ajax({
            url: '/CRM/CRMMyActivity/ActivityList',
            contextType: 'application/html;charset=utf-8',
            data: { UserType: $("#UserType").val(), txtCode: $("#txtCode").val(), txtAction: $("#txtAction").val(), txtType: $("#txtType").val(), txtStartDate: (isNaN(datetxtStartDate) == true ? null : ((datetxtStartDate.getMonth() + 1) + "/" + datetxtStartDate.getDate() + "/" + datetxtStartDate.getFullYear())), txtEndDate: (isNaN(datetxtEndDate) == true ? null : ((datetxtEndDate.getMonth() + 1) + "/" + datetxtEndDate.getDate() + "/" + datetxtEndDate.getFullYear())), txtPriority: $("#txtPriority").val(), txtStatus: $("#txtStatus").val(), txtRemarks: $("#txtRemarks").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyActivity").html(data);
            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    }); 

});