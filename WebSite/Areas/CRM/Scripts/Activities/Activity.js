﻿$(document).ready(function () {

    $('.input-group.date').datepicker({});
    $('#BeginTime').timepicker({ showMeridian: false });
    $('#ENDTime').timepicker({ showMeridian: false });

    $("#UpdateCorrectly").hide();
    
    $("#formActivity").validate({
        rules: {
            CardCode: "required",
            endDate: "required",
            Recontact: "required",
        },
        messages: {
            CardCode: $("#CardCode").text() + $("#REQ").text(),
            Recontact: $("#Recontact").text() + $("#REQ").text(),
            endDate: $("#Dateto").text() + $("#REQ").text(),
        },
        submitHandler: function (form) {

            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Error')
                    break;
            }
        }
    });

    $("#SearchVendor").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    function Save() {
        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        })

        var dateendDate = $("#endDate").datepicker("getDate");
        var dateRecontact = $("#Recontact").datepicker("getDate");
        var checkBoxClosed = "N";
        if ($("#Closed").is(':checked')) {
            checkBoxClosed = "Y";
        }
        $.post("/CRM/CRMActivity/Add", {
            ClgCode: $("#ClgCode").val(),
            CardCode: $("#CardCode").val(),
            Recontact: (isNaN(dateRecontact) == true ? null : (dateRecontact.getMonth() + 1) + "/" + dateRecontact.getDate() + "/" + dateRecontact.getFullYear()),
            BeginTime: $("#BeginTime").val().replace(':', ''),
            endDate: (isNaN(dateendDate) == true ? null : (dateendDate.getMonth() + 1) + "/" + dateendDate.getDate() + "/" + dateendDate.getFullYear()),
            ENDTime: $("#ENDTime").val().replace(':', ''),
            Action: $("#Action").val(),
            CntctType: $("#CntctType").val(),
            Priority: $("#Priority").val(),
            Details: $("#Details").val(),
            Notes: $("#Notes").val(),
            UserType: $("#UserType").val(),
            ActUser: $("#ActUser").val(),
            Status: $("#Status").val(),
            Closed: checkBoxClosed
        })
        .success(function (data) {
            var mResult = data.split(';');
            if (mResult[0] == "Ok") {
                $('#Loading').modal('hide');

                switch (fromPage) {
                    case "ServiceCall":
                        location.href = "/ServiceCall/ServiceCall/NewServiceCallActivity?pId=" + mResult[1];
                        break;
                    case "Opportunity":
                        location.href = "/Sales/Opportunities/NewOpportunitiesActivity?pId=" + mResult[1];
                        break;
                    case "MyList":
                        location.href = "/CRM/CRMMyActivity/Index";
                        break;
                    case "Calendar":

                        SaveCalendar();
                        //$("#btnCancelFromCalendar").click();
                        break;
                    default:
                        location.href = "/CRM/CRMActivity/Index";
                        break;
                }
            }
            else {
                $('#Loading').modal('hide');
                ErrorModal(data);
            }
        })
        .error(function (err) { $('#Loading').modal('hide'); ErrorModal(err); });
    }

    function Update() {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        })

        var dateendDate = $("#endDate").datepicker("getDate");
        var dateRecontact = $("#Recontact").datepicker("getDate");
        var checkBoxClosed = "N";
        if ($("#Closed").is(':checked')) {
            checkBoxClosed = "Y";
        }
        $.post("/CRMActivity/Update", {
            ClgCode: $("#ClgCode").val(),
            CardCode: $("#CardCode").val(),
            Recontact: (isNaN(dateRecontact) == true ? null : (dateRecontact.getMonth() + 1) + "/" + dateRecontact.getDate() + "/" + dateRecontact.getFullYear()),
            BeginTime: $("#BeginTime").val().replace(':', ''),
            endDate: (isNaN(dateendDate) == true ? null : (dateendDate.getMonth() + 1) + "/" + dateendDate.getDate() + "/" + dateendDate.getFullYear()),
            ENDTime: $("#ENDTime").val().replace(':', ''),
            Action: $("#Action").val(),
            CntctType: $("#CntctType").val(),
            Priority: $("#Priority").val(),
            Details: $("#Details").val(),
            Notes: $("#Notes").val(),
            UserType: $("#UserType").val(),
            ActUser: $("#ActUser").val(),
            Status: $("#Status").val(),
            Closed: checkBoxClosed
        })
        .success(function (data) {
            if (data == "Ok") {
                $('#Loading').modal('hide');

                if (fromPage != "Calendar") {
                    $("#UpdateCorrectly").show("slow");
                    $("#UpdateCorrectly").delay(1500).slideUp(1000);
                }
                else
                {
                    SaveCalendar();
                }
            }
            else {
                $('#Loading').modal('hide');
                ErrorModal(data);
            }
        })
        .error(function (err) { $('#Loading').modal('hide'); ErrorModal(err); });
    }

    $("#UserType").change(function () {
        var mType = $("#UserType").val();

        $.ajax({
            url: '/CRM/CRMActivity/GetActivityUserList',
            contextType: 'application/html;charset=utf-8',
            data: { pUserType: mType },
            type: 'POST',
            dataType: 'json'
        }).done(function (result) {

            $("#ActUser").empty();
            $.each(result, function (i, result) {
                $("#ActUser").append('<option value="' + result.Code + '">' +
                     result.Name + '</option>');
            });

        }).fail(function () {
            $("#ActUser").empty();
            alert("error");
        });


    });

});

function SetVendor(pCode, pName) {
    $('#CardCode').val(pCode);
    $('#CardName').val(pName);
    $('#VendorModal').modal('hide');
}

function ErrorModal(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}