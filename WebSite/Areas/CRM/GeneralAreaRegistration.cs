﻿using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.CRM
{
    public class GeneralAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CRM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CRM_default",
                "CRM/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new string[] { "ARGNS.WebSite.Areas.CRM.Controllers" }
            );
        }
    }
}