﻿using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.WMS
{
    public class WMSAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "WMS";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "WMS_default",
                "WMS/{controller}/{action}/{id}",
                 new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new string[] { "ARGNS.WebSite.Areas.WMS.Controllers" }
            );
        }

    }
}