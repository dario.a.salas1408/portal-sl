﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Areas.WMS.Controllers
{
    public class HomeController : Controller
    {
        // GET: WMS/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}