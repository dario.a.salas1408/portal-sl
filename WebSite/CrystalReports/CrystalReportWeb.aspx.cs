﻿using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CrystalWeb
{
    public partial class CrystalReportWeb : System.Web.UI.Page
    {
        ConnectionInfo connectionInfo;
        TableLogOnInfo connectionInfoTable;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ConfigurationManager.AppSettings["ReportWay"].ToString() == "0")
                {
                    SetReportWin();
                }
                else
                {
                    SetReportWeb();
                }
            }
        }

        void SetReportWin()
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument oRPDoc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            string reporte = Server.MapPath(ConfigurationManager.AppSettings["ReportPath"]) + @"\" + Request.QueryString["path"];
            oRPDoc.Load(reporte);
            connectionInfo = new ConnectionInfo();
            connectionInfo.ServerName = ConfigurationManager.AppSettings["DBServer"];
            connectionInfo.DatabaseName = ((UserView)Session["UserLogin"]).CompanyConnect.CompanyDB;
            connectionInfo.UserID = ((UserView)Session["UserLogin"]).CompanyConnect.DbUserName;
            connectionInfo.Password = ((UserView)Session["UserLogin"]).CompanyConnect.DbPassword;
            connectionInfoTable = new TableLogOnInfo();

            TableLogOnInfos conectionsInfo = new TableLogOnInfos();
            conectionsInfo.Add(connectionInfoTable);

            connectionInfoTable.ConnectionInfo = connectionInfo;
            oRPDoc.Database.Tables[0].ApplyLogOnInfo(connectionInfoTable);

            SetDBLogonForReport(oRPDoc);
            SetDBLogonForSubreports(oRPDoc);

            this.CrystalReportViewer1.ReportSource = oRPDoc;
            this.CrystalReportViewer1.ReuseParameterValuesOnRefresh = true;
            //this.CrystalReportViewer1.EnableDatabaseLogonPrompt = false;

            this.CrystalReportViewer1.LogOnInfo = conectionsInfo;
            this.CrystalReportViewer1.LogOnInfo.Add(connectionInfoTable);
            this.CrystalReportViewer1.RefreshReport();
            this.CrystalReportViewer1.DataBind();
        }

        void SetReportWeb()
        {
            string reporte = Server.MapPath(ConfigurationManager.AppSettings["ReportPath"]) + @"\" + Request.QueryString["path"];
            this.CrystalReportSource1.Report.FileName = reporte;

            connectionInfo = new ConnectionInfo();
            connectionInfo.ServerName = ConfigurationManager.AppSettings["DBServer"];
            connectionInfo.DatabaseName = ((UserView)Session["UserLogin"]).CompanyConnect.CompanyDB;
            connectionInfo.UserID = ((UserView)Session["UserLogin"]).CompanyConnect.DbUserName;
            connectionInfo.Password = ((UserView)Session["UserLogin"]).CompanyConnect.DbPassword;
            connectionInfoTable = new TableLogOnInfo();

            foreach (String item in Request.QueryString.AllKeys.Where(c => c != "path"))
            {
                CrystalDecisions.Web.Parameter mParameter = new CrystalDecisions.Web.Parameter();

                mParameter.Name = item;
                mParameter.DefaultValue = Request.QueryString[item];

                this.CrystalReportSource1.Report.Parameters.Add(mParameter);
            }

            TableLogOnInfos conectionsInfo = new TableLogOnInfos();
            conectionsInfo.Add(connectionInfoTable);

            connectionInfoTable.ConnectionInfo = connectionInfo;
            this.CrystalReportSource1.ReportDocument.Database.Tables[0].ApplyLogOnInfo(connectionInfoTable);

            SetDBLogonForReport(this.CrystalReportSource1.ReportDocument);
            SetDBLogonForSubreports(this.CrystalReportSource1.ReportDocument);

            //ParameterDiscreteValue objDiscreteValue;
            //ParameterField objParameterField;

            //objDiscreteValue = new ParameterDiscreteValue();
            //objDiscreteValue.Value = 1;
            //objParameterField = CrystalReportViewer1.ParameterFieldInfo["prod_code"];
            //objParameterField.CurrentValues.Add(objDiscreteValue);


            this.CrystalReportViewer1.ReportSource = this.CrystalReportSource1;
            this.CrystalReportViewer1.ReportSourceID = "CrystalReportSource1";
            this.CrystalReportViewer1.ReuseParameterValuesOnRefresh = true;


            this.CrystalReportViewer1.LogOnInfo = conectionsInfo;
            this.CrystalReportViewer1.LogOnInfo.Add(connectionInfoTable);
            this.CrystalReportViewer1.RefreshReport();
            this.CrystalReportViewer1.DataBind();
        }
        private void SetDBLogonForReport(ReportDocument reportDocument)
        {

            foreach (CrystalDecisions.CrystalReports.Engine.Table table in reportDocument.Database.Tables)
            {
                table.ApplyLogOnInfo(connectionInfoTable);
                table.LogOnInfo.ConnectionInfo.DatabaseName = connectionInfo.DatabaseName;
                table.LogOnInfo.ConnectionInfo.ServerName = ConfigurationManager.AppSettings["DBServer"];
                table.LogOnInfo.ConnectionInfo.DatabaseName = ConfigurationManager.AppSettings["Company"];
                table.LogOnInfo.ConnectionInfo.UserID = ConfigurationManager.AppSettings["User"];
                table.LogOnInfo.ConnectionInfo.Password = ConfigurationManager.AppSettings["Password"];
            }
        }
        private void SetDBLogonForSubreports(ReportDocument reportDocument)
        {
            Sections sections = reportDocument.ReportDefinition.Sections;
            foreach (Section section in sections)
            {
                ReportObjects reportObjects = section.ReportObjects;
                foreach (ReportObject reportObject in reportObjects)
                {
                    //if (reportObject.Kind == ReportObjectKind.SubreportObject)
                    //{
                    try
                    {
                        SubreportObject subreportObject = (SubreportObject)reportObject;
                        ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
                        subReportDocument.DataSourceConnections.Clear();
                        SetDBLogonForReport(subReportDocument);
                    }
                    catch
                    {
                    }
                    //}
                }
            }
        }
    }
}