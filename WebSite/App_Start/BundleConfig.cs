﻿using System.Web;
using System.Web.Optimization;

namespace ARGNS.WebSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
               "~/Content/bootstrap.min.css",
               "~/Content/components.css",
               "~/Content/assets/css/icons/icomoon/styles.css",
               "~/Content/dashboard.css",
               "~/Content/fileinput.css",
               "~/Content/DataTable/jquery.dataTables.min.css",
               "~/Content/Dev.css",
               "~/Content/nuevos.css"));


            bundles.Add(new ScriptBundle("~/bundles/ScriptLogin").Include(
                     "~/Scripts/jquery-1.10.2.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/jquery.validate.js",
                     "~/Scripts/bootbox.min.js",
                     "~/Scripts/Login/login.js"));

            bundles.Add(new ScriptBundle("~/bundles/Master").Include(
                    "~/Scripts/jquery-1.10.2.js",
                    "~/Scripts/bootstrap.js",
                     "~/Scripts/bootbox.min.js",
                     "~/Scripts/General/showCompany.js",
                     "~/Scripts/assets/js/core/app.js",
                     "~/Scripts/General/PopupSizeGeneral.js"));


            bundles.Add(new ScriptBundle("~/bundles/Companies").Include(
                    "~/Scripts/jquery.validate.js",
                   "~/Scripts/Companies/companies.js"));

            bundles.Add(new ScriptBundle("~/bundles/CompaniesStock").Include(
                 "~/Scripts/jquery.validate.js",
                  "~/Scripts/Companies/CompaniesStock.js"));

            bundles.Add(new ScriptBundle("~/bundles/CRPageMap").Include(
                 "~/Scripts/jquery.validate.js",
                  "~/Scripts/Companies/CRPageMap.js"));

            bundles.Add(new ScriptBundle("~/bundles/QRConfig").Include(
                 "~/Scripts/jquery.validate.js",
                  "~/Scripts/Companies/QRConfig.js"));


            bundles.Add(new ScriptBundle("~/bundles/News").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Scripts/News/News.js"));

            bundles.Add(new ScriptBundle("~/bundles/Users").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Scripts/Users/Users.js"));

            bundles.Add(new ScriptBundle("~/bundles/MyAccount").Include(
                   "~/Scripts/jquery.validate.js",
                   "~/Scripts/Users/MyAccount.js"));

            bundles.Add(new ScriptBundle("~/bundles/UserGroup").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Scripts/UserGroup/UserGroup.js"));

            bundles.Add(new ScriptBundle("~/bundles/BPGroup").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Scripts/BPGroup/BPGroup.js"));

            bundles.Add(new ScriptBundle("~/bundles/MyDirectAccesses").Include(
                   "~/Scripts/Users/MyDirectAccesses.js"));

            #region Business Partner

            bundles.Add(new ScriptBundle("~/bundles/BusinessPartner").Include(
                   "~/Scripts/jquery.validate.js",
                   "~/Scripts/General/Validate.js",
                  "~/Areas/MasterData/Scripts/BusinessPartner/BusinessPartner.js"));

            bundles.Add(new ScriptBundle("~/bundles/BusinessPartner-Index").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Areas/MasterData/Scripts/BusinessPartner/Index.js"));

            #endregion

            #region Business Partner

            bundles.Add(new ScriptBundle("~/bundles/MyBusinessPartner-Index").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Areas/MasterData/Scripts/MyBusinessPartner/Index.js"));

            #endregion

            #region Customer Aging Report

            bundles.Add(new ScriptBundle("~/bundles/CustomerAging-Index").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Areas/MasterData/Scripts/CustomerAging/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/MyCustomerAging-Index").Include(
                   "~/Scripts/jquery.validate.js",
                  "~/Areas/MasterData/Scripts/CustomerAging/MyCustomerAgingIndex.js"));
            #endregion

            #region CRM

            bundles.Add(new ScriptBundle("~/bundles/CRMActivity").Include(
               "~/Scripts/jquery.validate.js",
                "~/Areas/CRM/Scripts/Activities/Activity.js",
                "~/Scripts/bootstrap-datepicker.js",
                "~/Scripts/bootstrap-timepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/CRMActivityIndex").Include(
               "~/Areas/CRM/Scripts/Activities/ActivityIndex.js",
               "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/CRMMyActivityIndex").Include(
               "~/Areas/CRM/Scripts/MyActivities/MyActivityIndex.js",
               "~/Scripts/bootstrap-datepicker.js"));

            #endregion



            bundles.Add(new ScriptBundle("~/bundles/UserCompanies").Include(
                 "~/Scripts/Users/UserCompanies.js"));

            bundles.Add(new ScriptBundle("~/bundles/UserUDF").Include(
                 "~/Scripts/Users/UserUDF.js"));

            bundles.Add(new ScriptBundle("~/bundles/PurchaseOrder").Include(
                  "~/Scripts/jquery.validate.js",
                  "~/Scripts/bootstrap-datepicker.js",
                  "~/Areas/Purchase/Scripts/PurchaseOrder/PurchaseOrders.js"));

            bundles.Add(new ScriptBundle("~/bundles/Activity").Include(
                     "~/Scripts/jquery.validate.js",
                "~/Scripts/Activity/Activity.js",
                "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/ActivityIndex").Include(
           "~/Scripts/Activity/ActivityIndex.js"));

            bundles.Add(new ScriptBundle("~/bundles/MyActivityIndex").Include(
           "~/Scripts/Activity/MyActivityIndex.js"));

            #region Color Master

            bundles.Add(new StyleBundle("~/bundles/Color_CSS").Include(
                "~/Content/ColorPicker/bootstrap-colorpicker.min.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/Color").Include(
              "~/Scripts/jquery.validate.js",
              "~/Scripts/ColorPicker/bootstrap-colorpicker.min.js",
              "~/Areas/PLM/Scripts/Color/Color.js",
              "~/Scripts/bootstrap-datepicker.js",
              "~/Scripts/fileinput.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Color_Index").Include(
              "~/Areas/PLM/Scripts/Color/ColorIndex.js"));

            #endregion

            bundles.Add(new ScriptBundle("~/bundles/Scale").Include(
              //"~/Scripts/jquery.validate.js",
              "~/Areas/PLM/Scripts/Scale.js",
              "~/Scripts/bootstrap-datepicker.js"));


            bundles.Add(new ScriptBundle("~/bundles/Variable").Include(
              //"~/Scripts/jquery.validate.js",
              "~/Areas/PLM/Scripts/Variable.js",
              "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/CriticalPath").Include(
           "~/Areas/PLM/Scripts/CriticalPath.js"));

            bundles.Add(new ScriptBundle("~/bundles/QueryManager").Include(
           "~/Scripts/jquery.validate.js",
           "~/Scripts/QueryManager/QueryManager.js"));

            bundles.Add(new ScriptBundle("~/bundles/Home").Include(
           "~/Scripts/Home/Home.js"));

            #region PDM

            bundles.Add(new ScriptBundle("~/bundles/PDM-Index").Include(
                  "~/Scripts/BootSideMenu.js",
               "~/Areas/PLM/Scripts/Index.js",
               "~/Scripts/bootstrap-datepicker.js"
               ));

            bundles.Add(new StyleBundle("~/Content/PDM-CSS").Include(
                   "~/Content/BootSideMenu.css",
                   "~/Content/font-awesome.min.css",
                   "~/Content/fileinput.css"
                  ));

            bundles.Add(new ScriptBundle("~/bundles/PDM").Include(
              "~/Scripts/jquery.validate.js",
              "~/Areas/PLM/Scripts/PDM.js",
              "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/FileInput").Include(
              "~/Scripts/fileinput.min.js"));

            bundles.Add(new StyleBundle("~/Content/FileInput-CSS").Include(
                  "~/Content/fileinput.css"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/ChangeLogView").Include(
               "~/Areas/PLM/Scripts/ChangeLog.js"
           ));

            #endregion

            #region Projects

            bundles.Add(new ScriptBundle("~/bundles/Projects").Include(
               "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/PLM/Scripts/Project/Project.js"
           ));

            bundles.Add(new StyleBundle("~/Content/ProjectCSS").Include(
               //"~/Content/components.min.css",
               "~/Content/componentsTimeLine.css"
              ));


            #endregion

            #region RangePlan

            bundles.Add(new ScriptBundle("~/bundles/RangePlan-Index").Include(
                  "~/Scripts/BootSideMenu.js",
                  "~/Scripts/jquery.dataTables.js",
                  "~/Scripts/jquery.dataTables.columnFilter.js",
               "~/Areas/PLM/Scripts/RangePlan/Index.js",
               "~/Scripts/bootstrap-datepicker.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/RangePlan").Include(
              "~/Scripts/jquery.validate.js",
              "~/Areas/PLM/Scripts/RangePlan/RangePlan.js",
              "~/Scripts/bootstrap-datepicker.js",
              "~/Scripts/jquery-ui-1.9.0.custom.min.js",
              "~/Scripts/ui.touch-punch.min.js",
              "~/Scripts/dragable_portal.js"
              ));

            bundles.Add(new StyleBundle("~/Content/Dragable-CSS").Include(
               "~/Content/dragables.css"
              ));

            #endregion

            bundles.Add(new StyleBundle("~/Content/TableFilter-CSS").Include(
               "~/Content/BootSideMenu.css",
               "~/Content/font-awesome.min.css"
              ));

            #region MyPDM

            bundles.Add(new ScriptBundle("~/bundles/MyStyles-Index").Include(
               "~/Areas/PLM/Scripts/MyStyles.js",
               "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/MyCostSheet-Index").Include(
               "~/Areas/PLM/Scripts/MyCostSheet.js"));


            #endregion

            #region Design

            bundles.Add(new ScriptBundle("~/bundles/Design-Index").Include(
           //"~/Scripts/jquery.validate.js",
           //"~/Scripts/jquery-1.10.2.js",
           //"~/Scripts/bootstrap.min.js",
           //"~/Areas/PLM/Scripts/PDM.js",
           "~/Areas/PLM/Scripts/Design/Index.js",
           //"~/Scripts/bootstrap.js",
           "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/Design").Include(
              "~/Scripts/jquery.validate.js",
              "~/Areas/PLM/Scripts/Design/Design.js",
              "~/Scripts/bootstrap-datepicker.js",
              "~/Scripts/fileinput.min.js"));

            #endregion

            #region PDC

            bundles.Add(new ScriptBundle("~/bundles/PDC-Index").Include(
           "~/Areas/PLM/Scripts/PDC/Index.js",
           "~/Scripts/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/PDC").Include(
              "~/Scripts/jquery.validate.js",
              "~/Scripts/bootstrap-datepicker.js",
              "~/Areas/PLM/Scripts/PDC/PDC.js"));

            #endregion

            #region Purchase Request
            bundles.Add(new ScriptBundle("~/bundles/PurchaseRequest-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Purchase/Scripts/PurchaseRequest/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/PurchaseRequest").Include(
                  "~/Scripts/jquery.validate.js",
                  "~/Scripts/bootstrap-datepicker.js",
                  "~/Areas/Purchase/Scripts/PurchaseRequest/PurchaseRequest.js"));
            #endregion

            #region Purchase Invoice

            bundles.Add(new ScriptBundle("~/bundles/PurchaseInvoice-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Purchase/Scripts/PurchaseInvoice/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/PurchaseInvoice").Include(
                  "~/Scripts/jquery.validate.js",
                  "~/Scripts/bootstrap-datepicker.js",
                  "~/Areas/Purchase/Scripts/PurchaseInvoice/PurchaseInvoice.js"));

            #endregion

            #region Draft

            bundles.Add(new ScriptBundle("~/bundles/Draft-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Purchase/Scripts/Draft/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/SalesDraft-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesDraft/Index.js"));
            #endregion

            #region Approval

            bundles.Add(new ScriptBundle("~/bundles/Approval-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Purchase/Scripts/Approval/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/SalesApproval-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesApproval/Index.js"));

            #endregion

            bundles.Add(new ScriptBundle("~/bundles/PurchaseOrder-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Purchase/Scripts/PurchaseOrder/Index.js"));

            bundles.Add(new StyleBundle("~/Content/DataPicker").Include(
               "~/Content/datepicker.css"));

            bundles.Add(new StyleBundle("~/Content/DateTimePicker").Include(
               "~/Content/timepicker.css"));

            #region "PQ"

            bundles.Add(new ScriptBundle("~/bundles/PurchaseQuotation-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Purchase/Scripts/PurchaseQuotation/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/PurchaseQuotation").Include(
               "~/Scripts/jquery.validate.js",
               "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Purchase/Scripts/PurchaseQuotation/PurchaseQuotation.js"));

            #endregion

            #region "SO"

            bundles.Add(new ScriptBundle("~/bundles/SalesOrder-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesOrder/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/SalesOrder").Include(
               "~/Scripts/jquery.validate.js",
               "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesOrder/SalesOrders.js"));

            bundles.Add(new ScriptBundle("~/bundles/SalesOrderSplit").Include(
           "~/Scripts/jquery.validate.js",
           "~/Scripts/bootstrap-datepicker.js",
           "~/Areas/Sales/Scripts/SalesOrder/SalesOrdersSplit.js"));

            #endregion

            #region "SOQ"

            bundles.Add(new ScriptBundle("~/bundles/SalesQuotation-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesQuotation/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/SalesQuotation").Include(
               "~/Scripts/jquery.validate.js",
               "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesQuotation/SalesQuotation.js"));

            #endregion

            #region Purchase Invoice

            bundles.Add(new ScriptBundle("~/bundles/SalesInvoice-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesInvoice/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/SalesInvoice").Include(
                  "~/Scripts/jquery.validate.js",
                  "~/Scripts/bootstrap-datepicker.js",
                  "~/Areas/Sales/Scripts/SalesInvoice/SalesInvoice.js"));

            #endregion

            #region CostSheet

            bundles.Add(new ScriptBundle("~/bundles/CostSheet").Include(
            "~/Areas/PLM/Scripts/CostSheet.js"));

            bundles.Add(new ScriptBundle("~/bundles/CostSheet-Index").Include(
               "~/Areas/PLM/Scripts/CostSheetIndex.js"));

            #endregion

            #region Material Detail

            bundles.Add(new ScriptBundle("~/bundles/MaterialDetail").Include(
                "~/Areas/PLM/Scripts/MaterialDetail.js"));

            #endregion

            #region CheckboxTree
            bundles.Add(new StyleBundle("~/Content/CheckboxTree").Include(
              "~/Content/jquery.bonsai.css"));

            bundles.Add(new ScriptBundle("~/bundles/CheckboxTree").Include(
                "~/Scripts/checkboxtree/jquery.qubit.js",
               "~/Scripts/checkboxtree/jquery.bonsai.js"));

            #endregion

            #region Alertify

            bundles.Add(new StyleBundle("~/Content/Alertify/CSS").Include(
                    "~/Content/Alertify/alertify.core.css",
                    "~/Content/Alertify/alertify.bootstrap.css"));


            bundles.Add(new ScriptBundle("~/bundles/Alertify").Include(
             "~/Scripts/Alertify/alertify.js"));

            #endregion

            #region Jquery UI

            bundles.Add(new StyleBundle("~/Content/JqueryUI/CSS").Include(
                    "~/Content/JqueryUI/jquery-ui.css"));


            bundles.Add(new ScriptBundle("~/bundles/JqueryUI").Include(
             "~/Scripts/JqueryUI/jquery-ui.js"));

            #endregion

            #region "Samples"
            bundles.Add(new ScriptBundle("~/bundles/Samples").Include(
                                          "~/Scripts/jquery.validate.js",
                                          "~/Areas/PLM/Scripts/Samples.js",
                                          "~/Scripts/bootstrap-datepicker.js",
                                          "~/Scripts/fileinput.min.js"));
            #endregion

            #region "Comun"

            bundles.Add(new ScriptBundle("~/bundles/Comun").Include(
                       "~/Scripts/Comun/Error.js",
                       "~/Scripts/Comun/Comun.js"));

            #endregion

            #region "SalesOpportunities"

            bundles.Add(new ScriptBundle("~/bundles/Opportunities-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/SalesOpportunities/Index.js"));


            bundles.Add(new ScriptBundle("~/bundles/MyOpportunities-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/Sales/Scripts/MySalesOpportunities/MyOpportunities.js"));

            bundles.Add(new ScriptBundle("~/bundles/Opportunities-Model").Include(
                "~/Areas/Sales/Scripts/SalesOpportunities/Opportunities.js",
               "~/Scripts/jquery.validate.js",
               "~/Scripts/bootstrap-datepicker.js"

               ));

            #endregion

            #region "Service call"

            bundles.Add(new ScriptBundle("~/bundles/ServiceCall-Index").Include(
                "~/Scripts/bootstrap-datepicker.js",
               "~/Areas/ServiceCall/Scripts/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/ServiceCall-Model").Include(
                "~/Areas/ServiceCall/Scripts/ServiceCall.js",
               "~/Scripts/jquery.validate.js",
               "~/Scripts/bootstrap-datepicker.js",
               "~/Scripts/fileinput.min.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/MyServiceCall-Index").Include(
            "~/Areas/ServiceCall/Scripts/MyServiceCall.js",
           "~/Scripts/jquery.validate.js",
           "~/Scripts/bootstrap-datepicker.js"

           ));

            #endregion

            #region "Item Master"

            bundles.Add(new ScriptBundle("~/bundles/ItemMaster-Index").Include(
                 "~/Scripts/jquery.validate.js",
                "~/Areas/MasterData/Scripts/ItemMaster/Index.js"));


            bundles.Add(new ScriptBundle("~/bundles/ItemMaster").Include(
               "~/Scripts/jquery.validate.js",
               "~/Areas/MasterData/Scripts/ItemMaster/ItemMaster.js"));

            bundles.Add(new ScriptBundle("~/bundles/ItemMasterHandheld").Include(
               "~/Scripts/jquery.validate.js",
               "~/Areas/MasterData/Scripts/ItemMaster/ItemMasterHandheld.js"));

            bundles.Add(new ScriptBundle("~/bundles/InventoryStatus").Include(
               "~/Areas/MasterData/Scripts/InventoryStatus/InventoryStatus.js"));

            #endregion

            #region "Quick Order"

            bundles.Add(new ScriptBundle("~/bundles/QuickOrder-Index").Include(
                "~/Scripts/DataTable/jquery.dataTables.min.js",
                "~/Scripts/DataTable/dataTables.buttons.min.js",
                "~/Scripts/DataTable/buttons.flash.min.js",
                "~/Scripts/DataTable/jszip.min.js",
                "~/Scripts/DataTable/pdfmake.min.js",
                "~/Scripts/DataTable/vfs_fonts.js",
                "~/Scripts/DataTable/buttons.html5.min.js",
                "~/Scripts/DataTable/buttons.print.min.js",
                "~/Areas/Sales/Scripts/QuickOrder/Index.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/QuickOrder-css-Index").Include(
                "~/Content/DataTable/buttons.dataTables.min.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/InventoryStatus").Include(
               "~/Areas/MasterData/Scripts/InventoryStatus/InventoryStatus.js"));

            #endregion


            bundles.Add(new ScriptBundle("~/bundles/DataTables-JS").Include(
              "~/Scripts/DataTable/jquery.dataTables.min.js",
              "~/Scripts/DataTable/dataTables.buttons.min.js",
              "~/Scripts/DataTable/buttons.flash.min.js",
              "~/Scripts/DataTable/jszip.min.js",
              "~/Scripts/DataTable/pdfmake.min.js",
              "~/Scripts/DataTable/vfs_fonts.js",
              "~/Scripts/DataTable/buttons.html5.min.js",
              "~/Scripts/DataTable/buttons.print.min.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/DataTables-CSS").Include(
                "~/Content/DataTable/buttons.dataTables.min.css"
                ));

            #region Scheduler

            bundles.Add(new ScriptBundle("~/bundles/Scheduler").Include(
              "~/Scripts/Scheduler/aui/aui.js"));

            bundles.Add(new ScriptBundle("~/bundles/Scheduler2").Include(
                 "~/Scripts/FullCalendar/moment.min.js",
               "~/Scripts/FullCalendar/fullcalendar.js"));


            bundles.Add(new StyleBundle("~/Content/FullCalendar").Include(
           "~/Content/FullCalendar/fullcalendar.css",
            "~/Content/FullCalendar/fullcalendar.print.css"
          ));


            bundles.Add(new ScriptBundle("~/bundles/Scheduler-Index").Include(
                "~/Scripts/Scheduler/Scheduler.js"));

            #endregion

            #region Shipment

            bundles.Add(new ScriptBundle("~/bundles/Shipment").Include(
                "~/Scripts/bootstrap-datepicker.js",
                "~/Areas/PLM/Scripts/Shipment/Shipment.js"));

            bundles.Add(new ScriptBundle("~/bundles/Shipment-Index").Include(
               "~/Scripts/bootstrap-datepicker.js",
                "~/Areas/PLM/Scripts/Shipment/ShipmentIndex.js"));

            bundles.Add(new ScriptBundle("~/bundles/MyShipment-Index").Include(
               "~/Scripts/bootstrap-datepicker.js",
                "~/Areas/PLM/Scripts/Shipment/MyShipmentIndex.js"));

            #endregion

            #region Tech Pack

            bundles.Add(new ScriptBundle("~/bundles/TechPack").Include(
            "~/Areas/PLM/Scripts/TechPack/TechPack.js"));

            bundles.Add(new StyleBundle("~/Content/TechPack-CSS").Include(
                   "~/Content/fileinput.css"
                  ));

            #endregion

            #region Charts

            bundles.Add(new ScriptBundle("~/bundles/ChartCreator").Include(
                "~/Scripts/Chart/amcharts.js",
                "~/Scripts/Chart/pie.js",
                "~/Scripts/Chart/serial.js",
                "~/Scripts/Chart/serial.js",
                "~/Scripts/Chart/radar.js",
                "~/Scripts/Chart/plugins/export/export.min.js",
                //"~/Scripts/Chart/plugins/export/libs/fabric.js/fabric.js",
                //"~/Scripts/Chart/plugins/export/libs/FileSaver.js/FileSaver.min.js",
                //"~/Scripts/Chart/plugins/export/libs/jszip/jszip.min.js",
                //"~/Scripts/Chart/plugins/export/libs/xlsx/xlsx.min.js",
                //"~/Scripts/Chart/plugins/export/libs/pdfmake/vfs_fonts.js",
                //"~/Scripts/Chart/plugins/export/libs/pdfmake/pdfmake.min.js.map",
                //"~/Scripts/Chart/plugins/export/libs/pdfmake/pdfmake.min.js",
                "~/Scripts/Chart/ChartCreator.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/ChartIndex").Include(
               "~/Scripts/Chart/Index.js"
           ));

            bundles.Add(new StyleBundle("~/Content/ChartsCSS").Include(
                "~/Content/Chart/plugins/export/export.css"
            ));

            #endregion

            #region Tiles

            bundles.Add(new ScriptBundle("~/bundles/TileCreator").Include(
                "~/Scripts/Tile/TileCreator.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/TileIndex").Include(
               "~/Scripts/Tile/Index.js"
           ));

            #endregion

            #region Branches

            bundles.Add(new ScriptBundle("~/bundles/Branches").Include(
                 "~/Scripts/Users/UserBranch.js"));

            #endregion

            #region Select Search
            bundles.Add(new ScriptBundle("~/bundles/SelectSearch-CSS").Include(
               "~/Content/bootstrap-select.min.css"
               ));

            bundles.Add(new ScriptBundle("~/bundles/SelectSearch-JS").Include(
             "~/Scripts/bootstrap-select.min.js"
             ));
            #endregion
        }
    }
}
