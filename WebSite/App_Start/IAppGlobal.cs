﻿using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGNS.WebSite.App_Start
{
    public  interface IAppGlobal
    {
          List<PaymentMean> PaymentMeans { get; set; }
          List<PaymentMeanType> PaymentMeanTypes { get; set; }
          bool MandatoryPaymentMeans { get; set; }
    }
}
