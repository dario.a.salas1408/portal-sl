﻿using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARGNS.WebSite.App_Start
{
    public static class AppGlobal
    {
        public static List<PaymentMean> PaymentMeans { get; set; }
        public static List<PaymentMeanType> PaymentMeanTypes { get; set; }
        public static bool MandatoryPaymentMeans { get; set; }
    }
}