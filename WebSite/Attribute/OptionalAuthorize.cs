﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ARGNS.WebSite.Attribute
{
    public class CustomerAuthorize : AuthorizeAttribute
    {

        private Enums.Areas? _Area = null;
        private Enums.Pages? _Pages = null;
        private Enums.Actions? _Action = null;
        private AreaKeyManagerView _AreaKeyManagerView;
        private bool _Authorize = true;
        private Security _Security;

        public CustomerAuthorize()
        {
            _AreaKeyManagerView = new AreaKeyManagerView();
            _Security = new Security();
        }

        public CustomerAuthorize(Enums.Areas pArea)
        {
            _Area = pArea;
            _AreaKeyManagerView = new AreaKeyManagerView();
            _Security = new Security();
        }

        public CustomerAuthorize(Enums.Areas pArea, Enums.Pages pPage)
        {
            _Area = pArea;
            _Pages = pPage;
            _AreaKeyManagerView = new AreaKeyManagerView();
            _Security = new Security();
        }

        public CustomerAuthorize(Enums.Areas pArea, Enums.Pages pPage, Enums.Actions pAction)
        {
            _Area = pArea;
            _Pages = pPage;
            _Action = pAction;
            _AreaKeyManagerView = new AreaKeyManagerView();
            _Security = new Security();
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {

            base.OnAuthorization(filterContext);

        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            UserView mUser = (UserView)httpContext.Session["UserLogin"];

            _Authorize = AuthorizeSite(mUser);

            return _Authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext ctx)
        {
            if (!ctx.HttpContext.User.Identity.IsAuthenticated)
                base.HandleUnauthorizedRequest(ctx);
            else
            {
                if (!_Authorize)
                {
                    // handle controller access
                    ctx.Result = new ViewResult { ViewName = "_ErrorAccess" };
                    //ctx.HttpContext.Response.StatusCode = 403;
                }

            }
        }

        private bool AuthorizeSite(UserView pUser)
        {
            try
            {
                UserManagerView _UserManagerView = new UserManagerView();

                if (pUser.CompanyConnect.CompanyDB == null)
                { return false; }

                if (!_AreaKeyManagerView.GetAreasAccess(_Area.Value))
                { return false; }

                if (pUser.IsAdmin && _Area.Value != Enums.Areas.WEBPORTAL)
                { return false; }

                AreaKeyView _AreaKey = pUser.ListArea.Where(c => c.Area == _Area.Value.ToDescriptionString()).FirstOrDefault();

                if (_AreaKey == null && _Area.Value != Enums.Areas.WEBPORTAL && !pUser.IsAdmin)
                { return false; }

                if (_AreaKey == null && _Area.Value != Enums.Areas.WEBPORTAL && !pUser.IsAdmin)
                { return false; }


                if (_Action != null)
                {
                    bool _AccessAction = pUser.ListUserPageAction.Where(c => c.PageInternalKey == _Pages.ToDescriptionString() && c.InternalKey == _Action.ToDescriptionString()).FirstOrDefault() == null ? false : true;
                    return _AccessAction;
                }
                if (_Pages != null)
                {
                    bool _AccessPage = pUser.ListUserPageAction.Where(c => c.PageInternalKey == _Pages.ToDescriptionString()).FirstOrDefault() == null ? false : true;
                    return _AccessPage;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

    }
}