﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ARGNS.WebSite.Startup))]
namespace ARGNS.WebSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
