﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;

namespace ARGNS.Util
{
	class PrintCrystal
	{
		public bool isHana { get; set; }
		public bool setDBCredentials { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="prmConnectionInfo"></param>
		/// <param name="prmReportDocument"></param>
		public void SetDBLogonForReport(ConnectionInfo prmConnectionInfo, 
			CrystalDecisions.CrystalReports.Engine.ReportDocument prmReportDocument)
		{
			CrystalDecisions.CrystalReports.Engine.Tables tables = prmReportDocument.Database.Tables;

			foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
			{
				CrystalDecisions.Shared.TableLogOnInfo tableLogonInfo = table.LogOnInfo;
				tableLogonInfo.ConnectionInfo = prmConnectionInfo;
				table.ApplyLogOnInfo(tableLogonInfo);
				if (isHana)
				{
					table.Location = prmConnectionInfo.DatabaseName + "." + table.Location;
				}
				else
				{
					table.Location = prmConnectionInfo.DatabaseName + ".." + table.Location;
				}
			}

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connectionInfo"></param>
		/// <param name="reportDocument"></param>
		public void SetDBLogonForSubreports(ConnectionInfo connectionInfo, ReportDocument reportDocument)
		{
			CrystalDecisions.CrystalReports.Engine.Sections sections = reportDocument.ReportDefinition.Sections;
			foreach (CrystalDecisions.CrystalReports.Engine.Section section in sections)
			{
				CrystalDecisions.CrystalReports.Engine.ReportObjects reportObjects = section.ReportObjects;
				foreach (CrystalDecisions.CrystalReports.Engine.ReportObject reportObject in reportObjects)
				{
					if (reportObject.Kind == CrystalDecisions.Shared.ReportObjectKind.SubreportObject)
					{
						CrystalDecisions.CrystalReports.Engine.SubreportObject subreportObject = (CrystalDecisions.CrystalReports.Engine.SubreportObject)reportObject;
						CrystalDecisions.CrystalReports.Engine.ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
						SetDBLogonForReport(connectionInfo, subReportDocument);

					}
				}

			}

		}
	}
}
