﻿using CrystalDecisions.CrystalReports.Engine;
using System;

namespace ARGNS.Util
{
	public class CrystalReports
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="setDbCredentials"></param>
		/// <returns></returns>
		public static CrystalDecisions.Shared.ConnectionInfo GetConnectionInfo(string pServer, 
			string pUserName, string pPassword, 
			string pDataBase, out bool pSetDbCredentials)
		{
			CrystalDecisions.Shared.ConnectionInfo connectionInfo = new CrystalDecisions.Shared.ConnectionInfo();
			pSetDbCredentials = false;

			try
			{
				connectionInfo.Type = CrystalDecisions.Shared.ConnectionInfoType.SQL;
				connectionInfo.AllowCustomConnection = true;
				connectionInfo.IntegratedSecurity = false;

				connectionInfo.ServerName = pServer;
				connectionInfo.UserID = pUserName;
				connectionInfo.Password = pPassword;
				connectionInfo.DatabaseName = pDataBase;
				pSetDbCredentials = true;
			}
			catch (Exception ex)
			{

			}
			return connectionInfo;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="reportDocument"></param>
		/// <returns></returns>
		public ReportDocument GeneratePDFFile(int pDocEntry, 
			int pDocType, string pDocLocation, 
			string pServer, string pUser, string pPassword, 
			string pDatabase,
			bool  pIsHanaServer)
		{
			return GetReportFile(pDocEntry, pDocType, pDocLocation, 
				pServer, pUser, pPassword, pDatabase, pIsHanaServer);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pDocEntry"></param>
		/// <returns></returns>
		public static ReportDocument GetReportFile(int pDocEntry, 
			int pDocType,
			string pRptFileName, string pServer, 
			string pUser, string pPassword, 
			string pDatabase, bool pIsHanaServer)
		{
			ReportDocument reportDocument = new ReportDocument();

			try
			{
				reportDocument.Load(pRptFileName);
				//Trying to add parameters
				try
				{
					reportDocument.SetParameterValue("ObjectId@", pDocType);
				}
				catch (Exception ex)
				{
				}

				try
				{
					reportDocument.SetParameterValue("DocKey@", pDocEntry);
				}
				catch (Exception ex)
				{
				}

				PrintCrystal crystalPrint = new PrintCrystal();
				crystalPrint.isHana = pIsHanaServer;
				crystalPrint.setDBCredentials = Convert.ToBoolean(true);

				bool setDbCredentials = false;

				CrystalDecisions.Shared.ConnectionInfo connectionInfo = GetConnectionInfo(pServer, pUser, 
					pPassword, pDatabase, 
					out setDbCredentials);

				if (setDbCredentials)
				{
					crystalPrint.SetDBLogonForReport(connectionInfo, reportDocument);
					crystalPrint.SetDBLogonForSubreports(connectionInfo, reportDocument);
				}
			}
			catch (Exception ex)
			{

			}
			return reportDocument;
		}
	}
}
