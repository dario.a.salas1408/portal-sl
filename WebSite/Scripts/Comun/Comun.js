﻿//DROPDOWN SUBMENU OPEN/CLOSE START
$('li.dropdown a').on('click', function (event) {
    if ($(this).parent().attr('class') === "dropdown-submenu") {
        if ($(this).parent().hasClass('open') == true) {
            $(this).parent().removeClass('open');
        }
    }
    else {
        if ($(this).parent().hasClass('open') == false) {
            $('li.dropdown').removeClass('open');
            $(this).parent().toggleClass('open');
        }
        else {
            $(this).parent().removeClass('open');
        }
    }
});
$('body').on('click', function (e) {
    if (!$('li.dropdown').is(e.target)
        && $('li.dropdown').has(e.target).length === 0
        && $('.open').has(e.target).length === 0
    ) {
        $('li.dropdown').removeClass('open');
    }
});
//DROPDOWN SUBMENU OPEN/CLOSE END