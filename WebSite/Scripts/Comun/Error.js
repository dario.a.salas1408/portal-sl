﻿function ErrorCalls(pStatus)
{
    switch (pStatus) {
        case 0:
            window.console.log("Network Error");
            break;
        case 404:
            window.console.log("Url not Found");
            break;
        default:
            window.console.log("Not defined.");
    }
}