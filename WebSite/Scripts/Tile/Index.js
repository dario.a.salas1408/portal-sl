﻿var idTileToDelete = -1;
$(document).ready(function () {

    $("#Ok").click(function () {
        Delete();
    });

});

function DeleteTile(IdTile) {
    $("#myModalLabel").empty().append("Delete Tile");

    $('#myModal').modal({
        backdrop: 'static',
        keyboard: true

    });
    idTileToDelete = IdTile;
}

function Delete() {
    if (idTileToDelete != -1) {
        $.post("/Tile/Delete", {
            mPortalTileId: idTileToDelete
        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/Tile"; }
            else {
                ErrorTile(data);
            }
        })
    }
}