﻿$(document).ready(function () {
    $("#UpdateCorrectly").hide();
    $("#MissingFields").hide();

    if ($("#PortalTileType").val() == "1") {
        $("#HTMLCreatorContainer").css("display", "none");
        $("#LinkCreatorContainer").css("display", "");
    }
    if ($("#PortalTileType").val() == "2") {
        $("#LinkCreatorContainer").css("display", "none");
        $("#HTMLCreatorContainer").css("display", "");
    }

    $('#PortalTileType').change(function () {
        if ($("#PortalTileType").val() == "1") {
            $("#HTMLCreatorContainer").css("display", "none");
            $("#LinkCreatorContainer").css("display", "");
        }
        if ($("#PortalTileType").val() == "2") {
            $("#LinkCreatorContainer").css("display", "none");
            $("#HTMLCreatorContainer").css("display", "");
        }
    });

    $("#LoadSAPQueryTile").hide();

    $("#SearchSAPQueryTile").click(function () {
        $("#LoadSAPQueryTile").show('slow');
        $.ajax({
            url: '/Tile/_GetSAPQueryList',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $("#PageKey").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadSAPQueryTile").hide();
            $("#ModalBodySAPQuery").html(data);
        });
    });

    $("#ExecQueryTile").click(function () {
        var mParamList = [];
        var listParamName = $("input[id^=ParamName]");
        for (i = 0; i < listParamName.length; i++) {
            var mParamId = listParamName[i].id.substring(9);
            mParamList.push({
                ParamName: $("#ParamName" + mParamId).val(),
                ParamValue: $("#ParamValue" + mParamId).val(),
                ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            });
        }
        $.ajax({
            url: '/Tile/_GetSAPQueryResultTable',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $("#PageKey").val(), pQueryInternalKey: $("#QueryInternalKey").val(), pListParams: mParamList },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ItemsQueryTile").html(data);
        });
    });

    $("#btnOkTile").click(function () {
        if (!ValidateMissingFields()) {
            return;
        }
        switch (formMode) {
            case "Update":
                UpdateTile();
                break;
            case "Add":
                AddTile();
                break;
        }
    });

    if ($("#UGAll").is(':checked')) {
        var mListUGCk = $("input[id^=UGCk_]");
        for (i = 0; i < mListUGCk.length; i++) {
            $("#" + mListUGCk[i].id).attr('disabled', 'disabled');
        }
    }

    $("#UGAll").change(function () {
        if ($('#UGAll').is(':checked')) {
            var mListUGCk = $("input[id^=UGCk_]");
            for (i = 0; i < mListUGCk.length; i++) {
                $("#" + mListUGCk[i].id).attr('disabled', 'disabled');
            }
        }
        else {
            var mListUGCk = $("input[id^=UGCk_]");
            for (i = 0; i < mListUGCk.length; i++) {
                $("#" + mListUGCk[i].id).removeAttr('disabled');
            }
        }
    });
});

function SetQueryTile(pQueryName, pQueryInternalKey) {
    $.ajax({
        url: '/Tile/_GetSAPQueryParamList',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $("#PageKey").val(), pQueryInternalKey: pQueryInternalKey },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#QueryName").val(pQueryName);
        $("#QueryInternalKey").val(pQueryInternalKey);
        $("#ParamsDiv").html(data);
        $('#SAPQueryModal').modal('hide');
        $('#LoadSAPQueryTile').modal('hide');
    });
}

function ValidateMissingFields() {
    if ($("#VisualOrder").val() == "" || $("#TileName").val() == "" || $("#PortalTileType").val() == 0) {
        $("#MissingFields").show("slow");
        $("#MissingFields").delay(1500).slideUp(1000);
        return false;
    }

    return true;
}

function ParamValueTileChange(mParaValueId) {
    if ($("#ParamValue" + mParaValueId).val() == "FixedValue") {
        $("#ParamFixedValue" + mParaValueId).css("display", "");
        $("#ParamFixedLabel" + mParaValueId).css("display", "");
    }
    else {
        $("#ParamFixedValue" + mParaValueId).css("display", "none");
        $("#ParamFixedLabel" + mParaValueId).css("display", "none");
    }
}

function UpdateTile() {
    $('#Loading').modal({
        backdrop: 'static'
    });

    var mUGPortalList = [];
    if ($("#UGAll").is(':checked') == false) {
        var mListUGCk = $("input[id^=UGCk_]");
        for (i = 0; i < mListUGCk.length; i++) {
            if ($("#" + mListUGCk[i].id).is(':checked')) {
                mUGPortalList.push({
                    Id: $("#IdTile").val(),
                    UserGroup_Id: mListUGCk[i].id.substring(5),
                });
            }
        }
    }

    var PortalTileView = {
        IdTile: $("#IdTile").val(),
        TileName: $("#TileName").val(),
        IdSAPQuery: $("#QueryInternalKey").val(),
        NameSAPQuery: $("#QueryName").val(),
        IdTileType: $("#PortalTileType").val(),
        UrlPage: $("#UrlPage").val(),
        IdCompany: $("#IdCompany").val(),
        HtmlText: $("#HtmlText").val(),
        VisualOrder: $("#VisualOrder").val(),
        PortalTileParams: [],
        PortalTileUserGroups: mUGPortalList
    };

    var listParamName = $("input[id^=ParamName]");
    for (i = 0; i < listParamName.length; i++) {
        var mParamId = listParamName[i].id.substring(9);
        PortalTileView.PortalTileParams.push({
            Id: mParamId,
            ParamName: $("#ParamName" + mParamId).val(),
            ParamValue: $("#ParamValue" + mParamId).val(),
            ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            IsNew: $("#IsNew" + mParamId).val(),
            PortalTile: { IdTile: $("#IdTile").val() }
        });
    }

    $.ajax({
        url: '/Tile/Update',
        async: false,
        type: "POST",
        data: JSON.stringify(PortalTileView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data.ServiceAnswer == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(2500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorTile(data);
            }
        }
    });
}

function AddTile() {
    $('#Loading').modal({
        backdrop: 'static'
    });

    var mUGPortalList = [];
    if ($("#UGAll").is(':checked') == false) {
        var mListUGCk = $("input[id^=UGCk_]");
        for (i = 0; i < mListUGCk.length; i++) {
            if ($("#" + mListUGCk[i].id).is(':checked')) {
                mUGPortalList.push({
                    Id: $("#IdTile").val(),
                    UserGroup_Id: mListUGCk[i].id.substring(5),
                });
            }
        }
    }

    var PortalTileView = {
        IdTile: $("#IdTile").val(),
        TileName: $("#TileName").val(),
        IdSAPQuery: $("#QueryInternalKey").val(),
        NameSAPQuery: $("#QueryName").val(),
        IdTileType: $("#PortalTileType").val(),
        UrlPage: $("#UrlPage").val(),
        IdCompany: $("#IdCompany").val(),
        HtmlText: $("#HtmlText").val(),
        VisualOrder: $("#VisualOrder").val(),
        PortalTileParams: [],
        PortalTileUserGroups: mUGPortalList
    };

    var listParamName = $("input[id^=ParamName]");
    for (i = 0; i < listParamName.length; i++) {
        var mParamId = listParamName[i].id.substring(9);
        PortalTileView.PortalTileParams.push({
            Id: mParamId,
            ParamName: $("#ParamName" + mParamId).val(),
            ParamValue: $("#ParamValue" + mParamId).val(),
            ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            IsNew: $("#IsNew" + mParamId).val(),
            PortalTile: null
        });
    }

    $.ajax({
        url: '/Tile/Add',
        async: false,
        type: "POST",
        data: JSON.stringify(PortalTileView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data.ServiceAnswer == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(2500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorTile(data);
            }
        }
    });

    function ErrorTile(data) {
        $("#errorMessage").empty().append("<strong>" + data.ServiceAnswer + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }
}

