﻿$(document).ready(function () {
    
    if (countBranchesAvailable == 0) {
        $("#AddtoList").attr("disabled", "disabled");
    }

    $("#AddtoList").click(function () {

        $.post("/Users/_AddBranch", {
            pUserID: $("#IdUser").val(),
            pCompanyId: $("#CompAV").val(),
            pIdBranch: $("#cboBranches").val(),
            pPageKey: $('#Pagekey').val()
        })
            .success(function (data) { 
                $.post("/Users/GetBranchesAvailable", {
                    pUserID: $("#IdUser").val(),
                    pCompanyId: $("#CompAV").val(),
                    pPageKey: $('#Pagekey').val()
                })
                    .success(function (dataJson) {
                        $('#cboBranches').empty();
                        $.each(dataJson, function (key) {
                            $('#cboBranches').append($('<option>', {
                                value: dataJson[key].BPLId,
                                text: dataJson[key].BPLName
                            }));
                        });
                        $("#tableContainer").html(data);
                        if (dataJson.length == 0) {
                            $("#AddtoList").attr("disabled", "disabled");
                        }
                        else {
                            $("#AddtoList").removeAttr("disabled");
                        }
                    })
                    .error(function (err) { Close(); });
            })
            .error(function (err) { Close(); });


    });

    $("#CompAV").change(function () {
        location.href = "/Users/UserBranch?IdUser=" + $("#IdUser").val() + "&IdCompany=" + $("#CompAV").val();
    });
    
});

function DeleteBranch(idUserBranch, idCompany) {
    
    $.post("/Users/_DeleteBranch", {
        pUserID: $("#IdUser").val(),
        pCompanyId: idCompany,
        idUserBranch: idUserBranch,
        pPageKey: $('#Pagekey').val()
    })
        .success(function (data) {
            $.post("/Users/GetBranchesAvailable", {
                pUserID: $("#IdUser").val(),
                pCompanyId: idCompany,
                pPageKey: $('#Pagekey').val()
            })
                .success(function (dataJson) {
                    $('#cboBranches').empty();
                    $.each(dataJson, function (key) {
                        $('#cboBranches').append($('<option>', {
                            value: dataJson[key].BPLId,
                            text: dataJson[key].BPLName
                        }));
                    });
                    $("#tableContainer").html(data);
                    if (dataJson.length == 0) {
                        $("#AddtoList").attr("disabled", "disabled");
                    }
                    else {
                        $("#AddtoList").removeAttr("disabled");
                    }
                })
                .error(function (err) { Close(); });
        })
        .error(function (err) { Close(); });
}