﻿$(document).ready(function () {

    if (CountCompAv == 0) {
        $("#AddtoList").attr("disabled", "disabled");
    }

    $("#AddtoList").click(function () {

        if ($("#UserPasswordSAP").val() != null && $("#UserPasswordSAP").val() != "") {
            $.post("/Users/AddCompany", {
                IdUser: $("#IdUser").val(),
                IdSelector: $("#CompAV").val(),
                UserNameSAP: $("#UserSapId").find(":selected").text(),
                UserCodeSAP: $("#UserSapId").find(":selected").val(),
                UserPasswordSAP: $("#UserPasswordSAP").val(),
                SECode : $("#SalesEmployeeId").val(),
                BPCode: $("#BusinessPartnerId").val(),
                DftDistRule: $("#DftDDR").val(),
                DftWhs: $("#DftWhs").val(),
                SalesTaxCodeDef: $("#SalesTaxCodeDef").val(),
                PurchaseTaxCodeDef: $("#PurchaseTaxCodeDef").val(),
                UserGroupId: $("#UserGroupId").val(),
                CostPriceList: $("#CostPriceList").val(),
                BPGroupId: $("#BPGroupId").val()
            })
           .success(function (data) {
               if (data == "Ok")
               { location.href = "/Users/CompaniesUser?IdUser=" + $("#IdUser").val(); }
               else
               {
                   ErrorUSerCom(data);
               }
           })
           .error(function (err) { Close(); });
        }
        else {
            ErrorUSerCom("Password cannot be empty.");
        }
    });

    $("#SearchBP").click(function () {
        if (typeof dtBP === 'undefined') {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
    });

    $("#CompAV").change(function () {

        $('#DftWhs').hide();
        $('#LoadingDftWhs').show();
        $('#DftDDR').hide();
        $('#LoadingDftDDR').show();
        $('#UserSapId').hide();
        $('#LoadingUserSapId').show();

        $('#PurchaseTaxCodeDef').hide();
        $('#LoadingPurTax').show();

        $('#SalesTaxCodeDef').hide();
        $('#LoadingSalesTax').show();

        $('#CostPriceList').hide();
        $('#LoadingCPL').show();

        $('#UserGroupId').hide();
        $('#LoadingUserGroupId').show();

        if (IsCustomer == "True")
        {
            $('#BusinessPartnerId').val("");
            $('#LoadingCustomer').show();
        }
        if (IsSalesEmployee == "True")
        {
            $('#SalesEmployeeId').hide();
            $('#LoadingSalesEmployee').show();

            $('#BPGroupId').hide();
            $('#LoadingBPGroup').show();
        }
            
        $.ajax({
            url: "/Users/GetUserSap",
            type: "POST",
            data: "CompanyId=" + $("#CompAV").val(),
            traditional: true,
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#UserSapId').empty();

                $.each(data, function (key) {
                   
                    $('#UserSapId').append($('<option>', {
                        value: data[key].Value,
                        text: data[key].Text
                    }));
                });

                $('#UserSapId').show();
                $('#LoadingUserSapId').hide();

            }, error: function (xhr) {

                ErrorCalls(xhr.status);

            },
        });

        //Sales Tax Code
        $.ajax({
            url: "/Users/GetSalesTaxCode",
            type: "POST",
            data: "pCompanyId=" + $("#CompAV").val(),
            traditional: true,
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#SalesTaxCodeDef').empty();

                $.each(data, function (key) {

                    $('#SalesTaxCodeDef').append($('<option>', {
                        value: data[key].Value,
                        text: data[key].Text
                    }));
                });

                $('#SalesTaxCodeDef').show();
                $('#LoadingSalesTax').hide();

            }, error: function (xhr) {

                ErrorCalls(xhr.status);

            },
        });


        //Purchase Tax Code
        $.ajax({
            url: "/Users/GetSalesTaxCode",
            type: "POST",
            data: "pCompanyId=" + $("#CompAV").val(),
            traditional: true,
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#PurchaseTaxCodeDef').empty();

                $.each(data, function (key) {

                    $('#PurchaseTaxCodeDef').append($('<option>', {
                        value: data[key].Value,
                        text: data[key].Text
                    }));
                });

                $('#PurchaseTaxCodeDef').show();
                $('#LoadingPurTax').hide();

            }, error: function (xhr) {

                ErrorCalls(xhr.status);

            },
        });

        $.ajax({
            url: "/Users/GetWHS",
            type: "POST",
            data: "pCompanyId=" + $("#CompAV").val(),
            traditional: true,
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#DftWhs').empty();

                $.each(data, function (key) {
                    
                    $('#DftWhs').append($('<option>', {
                        value: data[key].Value,
                        text: data[key].Text
                    }));
                });

                $('#DftWhs').show();
                $('#LoadingDftWhs').hide();

            }, error: function (xhr) {
                
                ErrorCalls(xhr.status);

            },
        });

       
        $.ajax({
            url: "/Users/GetDtR",
            type: "POST",
            data: "pCompanyId=" + $("#CompAV").val(),
            traditional: true,
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#DftDDR').empty();

                $.each(data, function (key) {

                    $('#DftDDR').append($('<option>', {
                        value: data[key].Value,
                        text: data[key].Text
                    }));
                });

                $('#DftDDR').show();
                $('#LoadingDftDDR').hide();

            }, error: function (xhr) {

                ErrorCalls(xhr.status);

            },
        });

        if (IsCustomer == "True")
        {
            $('#BusinessPartnerId').show();
            $('#LoadingCustomer').hide();
        }

        if (IsSalesEmployee == "True")
        {
            $.ajax({
                url: "/Users/GetSalesEmployee",
                type: "POST",
                data: "pCompanyId=" + $("#CompAV").val(),
                traditional: true,
                cache: false,
                dataType: 'json',
                success: function (data) {

                    $('#SalesEmployeeId').empty();

                    $.each(data, function (key) {

                        $('#SalesEmployeeId').append($('<option>', {
                            value: data[key].Value,
                            text: data[key].Text
                        }));
                    });

                    $('#SalesEmployeeId').show();
                    $('#LoadingSalesEmployee').hide();

                }, error: function (xhr) {

                    ErrorCalls(xhr.status);

                },
            });

            $.ajax({
                url: "/Users/GetBPGroupList",
                type: "POST",
                data: "pCompanyId=" + $("#CompAV").val(),
                traditional: true,
                cache: false,
                dataType: 'json',
                success: function (data) {

                    $('#BPGroupId').empty();

                    $.each(data, function (key) {

                        $('#BPGroupId').append($('<option>', {
                            value: data[key].Value,
                            text: data[key].Text
                        }));
                    });

                    $('#BPGroupId').show();
                    $('#LoadingBPGroup').hide();

                }, error: function (xhr) {

                    ErrorCalls(xhr.status);

                },
            });
        }

        $.ajax({
            url: "/Users/GetAllPriceList",
            type: "POST",
            data: "pCompanyId=" + $("#CompAV").val(),
            traditional: true,
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#CostPriceList').empty();

                $.each(data, function (key) {

                    $('#CostPriceList').append($('<option>', {
                        value: data[key].Value,
                        text: data[key].Text
                    }));
                });

                $('#CostPriceList').show();
                $('#LoadingCPL').hide();

            }, error: function (xhr) {

                ErrorCalls(xhr.status);

            },
        });

        $.ajax({
            url: "/Users/GetUserGroupByCompany",
            type: "POST",
            data: "pCompanyId=" + $("#CompAV").val(),
            traditional: true,
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#UserGroupId').empty();

                $.each(data, function (key) {

                    $('#UserGroupId').append($('<option>', {
                        value: data[key].Value,
                        text: data[key].Text
                    }));
                });

                $('#UserGroupId').show();
                $('#LoadingUserGroupId').hide();

            }, error: function (xhr) {

                ErrorCalls(xhr.status);

            },
        });

    });

   

});

function DeleteCompany(IdCompany) {
    $.post("/Users/DeleteCompany", {
        IdUser: $("#IdUser").val(),
        IdSelector: IdCompany
    })
   .success(function (data) {
       if (data == "Ok")
       { location.href = "/Users/CompaniesUser?IdUser=" + $("#IdUser").val(); }
       else
       {
           ErrorUSerCom(data);
       }
   })
   .error(function (err) { Close(); });
}

function ErrorUSerCom(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}



function ValidateConnection(IdCompany, IdUser) {
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    $.post("/Users/ValidateConnection", {
        pCompanyId: IdCompany,
        idUSer: IdUser
    })
   .success(function (data) {
       if (data == "Ok")
       {
           $('#Loading').modal('hide');
           $("#infoMessage").empty().append("<strong> Connection validation: Correct </strong>");
           $("#infoMessage").show('slow');
           $('#infoBox').modal({
               backdrop: 'static',
               keyboard: true
           });
       }
       else
       {
           $('#Loading').modal('hide');
           $("#errorMessage").empty().append("<strong>  Connection validation: Failed </strong>");
           $("#errorMessage").show('slow');
           $('#errorBox').modal({
               backdrop: 'static',
               keyboard: true
           });
       }
   })
   .error(function (err) { Close(); });
}

function SetBP(pCode) {

    $('#BusinessPartnerId').val(pCode).trigger('change');
    $('#BPModal').modal('hide');
}