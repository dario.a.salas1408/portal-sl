﻿var CheckAllCustomVal = true;
var CheckAllPLMVal = true;
var CheckAllSalesVal = true;
var CheckAllPurchaseVal = true;
var CheckAllMasterDataVal = true;
var CheckAllServiceCallVal = true;
var CheckAllCRMVal = true;
$(document).ready(function () {
    //$("#UpdateCorrectly").hide();

    $("#CheckAllCustom").click(function () {
        var ListCheckCustom = $('input[name="CheckCustom"]');
        if (ListCheckCustom.length > 0) {
            ListCheckCustom.each(function () {
                $(this).prop('checked', CheckAllCustomVal);
            });
            if (CheckAllCustomVal)
                CheckAllCustomVal = false;
            else
                CheckAllCustomVal = true;
        }
    }); 

    $("#CheckAllPLM").click(function () {
        var ListCheckPLM = $('input[name="CheckPLM"]');
        if (ListCheckPLM.length > 0) {
            ListCheckPLM.each(function () {
                $(this).prop('checked', CheckAllPLMVal);
            });
            if (CheckAllPLMVal)
                CheckAllPLMVal = false;
            else
                CheckAllPLMVal = true;
        }
    });

    $("#CheckAllSales").click(function () {
        var ListCheckSales = $('input[name="CheckSales"]');
        if (ListCheckSales.length > 0) {
            ListCheckSales.each(function () {
                $(this).prop('checked', CheckAllSalesVal);
            });
            if (CheckAllSalesVal)
                CheckAllSalesVal = false;
            else
                CheckAllSalesVal = true;
        }
    });

    $("#CheckAllPurchase").click(function () {
        var ListCheckPurchase = $('input[name="CheckPurchase"]');
        if (ListCheckPurchase.length > 0) {
            ListCheckPurchase.each(function () {
                $(this).prop('checked', CheckAllPurchaseVal);
            });
            if (CheckAllPurchaseVal)
                CheckAllPurchaseVal = false;
            else
                CheckAllPurchaseVal = true;
        }
    });

    $("#CheckAllMasterData").click(function () {
        var ListCheckMasterData = $('input[name="CheckMasterData"]');
        if (ListCheckMasterData.length > 0) {
            ListCheckMasterData.each(function () {
                $(this).prop('checked', CheckAllMasterDataVal);
            });
            if (CheckAllMasterDataVal)
                CheckAllMasterDataVal = false;
            else
                CheckAllMasterDataVal = true;
        }
    });

    $("#CheckAllServiceCall").click(function () {
        var ListCheckServiceCall = $('input[name="CheckServiceCall"]');
        if (ListCheckServiceCall.length > 0) {
            ListCheckServiceCall.each(function () {
                $(this).prop('checked', CheckAllServiceCallVal);
            });
            if (CheckAllServiceCallVal)
                CheckAllServiceCallVal = false;
            else
                CheckAllServiceCallVal = true;
        }
    });

    $("#CheckAllCRM").click(function () {
        var ListCheckCRM = $('input[name="CheckCRM"]');
        if (ListCheckCRM.length > 0) {
            ListCheckCRM.each(function () {
                $(this).prop('checked', CheckAllCRMVal);
            });
            if (CheckAllCRMVal)
                CheckAllCRMVal = false;
            else
                CheckAllCRMVal = true;
        }
    });

    $("#btnOk").click(function () {
        Update();
    });

});

function Update() {
    $('#Loading').modal('show');

    var ListCheckBox = $('input[type="checkbox"]');
    var ListCheckedCheckBox = [];
    var ListCheckedCheckBoxCustom = [];
    if (ListCheckBox.length > 0) {
        ListCheckBox.each(function () {
            if($(this).prop('checked'))
            {
                if ($(this)[0].id.includes("Custom_"))
                {
                    ListCheckedCheckBoxCustom.push($(this)[0].id.substring(7))
                }
                else
                {
                    ListCheckedCheckBox.push($(this)[0].id)
                }
            }
        });
    }

    $.post("/Users/UpdateMyDirectAccesses", {
        pListDirectAccesses: ListCheckedCheckBox,
        pListDirectAccessesCustom: ListCheckedCheckBoxCustom,
        pIdUser: $('#IdUser').val()
    })
    .success(function (data) {
        if (data == "Ok") {
            $('#Loading').modal('hide');

            $("#UpdateCorrectly").show("slow");
            $("#UpdateCorrectly").delay(1500).slideUp(1000);
        }
        else {
            $('#Loading').modal('hide');
            Error(data);
        }
    })
    .error(function (err) { });
}

//function Error(data) {
//    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
//    $("#errorMessage").show('slow');
//    $('#errorBox').modal({
//        backdrop: 'static',
//        keyboard: true
//    });
//}