﻿$(document).ready(function () {
    $("#UpdateCorrectly").hide();

    $("#formMyAccount").validate({
        rules: {
            Name: "required",
            OldPassword: "required",
            NewPassword: "required",
            RepNewPassword: "required"

        },
        messages: {
            Name: $("#lblName").text() + $("#REQ").text(),
            Password: $("#lblOP").text() + $("#REQ").text(),
            Password: $("#lblNP").text() + $("#REQ").text(),
            Password: $("#lblRNP").text() + $("#REQ").text()
        },
        submitHandler: function (form){
            Update();
        }
    });

    function Update() {
        $('#Loading').modal('show');

        $.post("/Users/UpdateMyAccount", {
            IdUser: $("#IdUser").val(),
            Name: $("#Name").val(),
            Password: $("#OldPassword").val(),
            NewPassword: $("#NewPassword").val(),
            RepNewPassword: $("#RepNewPassword").val()
        })
        .success(function (data) {
            if (data == "Ok")
            {
                $('#Loading').modal('hide');

                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else
            {
                $('#Loading').modal('hide');
                Error(data);
            }
        })
        .error(function (err) { });
    }

    function Error(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }
});