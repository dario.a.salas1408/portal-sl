﻿

$(document).ready(function () {

    $('#checkboxes').bonsai({
        expandAll: false,
        checkboxes: true
    });

    $("#formUsers").validate({
        rules: {
            Name: "required",
            Password: "required"

        },
        messages: {
            Name: $("#lblName").text() + $("#REQ").text(),
            Password: $("#lblPassword").text() + $("#REQ").text()

        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                case "Delete":
                    $("#myModalLabel").empty().append("Delete Users");

                    $('#myModal').modal({
                        backdrop: 'static',
                        keyboard: true

                    })

                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });


    function Save() {

        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('name'));
        });

        $.post("/Users/Add", $.param({
            IdUser: $("#IdUser").val(),
            Name: $("#Name").val(),
            Password: $("#Password").val(),
            UserIdSAP: $("#UserSapId").val(),
            AccessUser: selected,
            IdRol: $("#Roles").val()

        }, true))
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/Users/Index"; }
            else
            {
                Error(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Update() {
        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).attr('name'));
        });

        $.post("/Users/Update", $.param({
            IdUser: $("#IdUser").val(),
            Name: $("#Name").val(),
            Password: $("#Password").val(),
            UserIdSAP: $("#UserSapId").val(),
            AccessUser: selected,
            IdRol: $("#Roles").val()

        }, true))
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/Users/Index"; }
            else
            {
                Error(data);
            }
        })
        .error(function (err) { });
    }

    function Delete() {

        $.post("/Users/Delete", {
            IdUser: $("#IdUser").val()
        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/Users/Index"; }
            else
            {
                Error(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Error(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }

    $("#Ok").click(function () {
        Delete();
    });

    $("#Roles").change(function () {

        $('#checkboxes input:checked').each(function () {
            var mLenght = this.name.split("-");

            if (mLenght.length == 3)
            { $("#" + this.name).click(); }

        });

        $.post("/Users/GetRolSetting", $.param({
            IdRol: $("#Roles").val()
        }, true))
      .success(function (data) {
          var mName;

          $.each(data.RolPageActions, function (key) {

              mName = "#" + data.RolPageActions[key].Page.IdAreaKeys + "-" + data.RolPageActions[key].IdPage + "-" + data.RolPageActions[key].IdAction

              $(mName).click();
          });

      })
      .error(function (err) { });
    });

});