﻿$(document).ready(function () {

    if (CountCompAv == 0) {
        $("#AddtoList").attr("disabled", "disabled");
    }

    $("#AddtoList").click(function () {
        if ($('#DocumentSelect').val() != -1 && $("#UDFSelect").val() != null) {
            $.post("/Users/_AddUDF", {
                pUserID: $("#IdUser").val(),
                pCompanyId: $("#CompAV").val(),
                pTableID: $("#DocumentSelect").val(),
                pFieldID: $("#UDFSelect").find(":selected").val(),
                pPageKey: $('#Pagekey').val()
            })
           .success(function (data) {
               $("#UDFTableContainer").html(data);

               Refresh();

           })
           .error(function (err) { Close(); });
        }
    });

    $("#CompAV").change(function () {
        $('#DocumentSelect').hide();
        $('#LoadingDocument').show();
        $('#UDFSelect').hide();
        $('#LoadingUDF').show();
        if ($('#DocumentSelect').val() != -1)
        {
            $.ajax({
                url: "/Users/GetUDFByTableID",
                type: "POST",
                data: { pCompanyId: $("#CompAV").val(), pTableID: $('#DocumentSelect').val(), pIdUser: $("#IdUser").val(), pPageKey: $('#Pagekey').val() },
                traditional: true,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    $('#UDFSelect').empty();
                    $.each(data, function (key) {
                        $('#UDFSelect').append($('<option>', {
                            value: data[key].Value,
                            text: data[key].Text
                        }));
                    });
                    $('#UDFSelect').show();
                    $('#LoadingUDF').hide();
                    $('#DocumentSelect').show();
                    $('#LoadingDocument').hide();
                }, error: function (xhr) {
                    ErrorCalls(xhr.status);
                },
            });
        }
    });

    $("#DocumentSelect").change(function () {
        $('#CompAV').hide();
        $('#LoadingCompanies').show();
        $('#UDFSelect').hide();
        $('#LoadingUDF').show();
        if ($('#DocumentSelect').val() != -1) {
            $.ajax({
                url: "/Users/GetUDFByTableID",
                type: "POST",
                data: { pCompanyId: $("#CompAV").val(), pTableID: $('#DocumentSelect').val(), pIdUser: $("#IdUser").val(), pPageKey: $('#Pagekey').val()  },
                traditional: true,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    $('#UDFSelect').empty();
                    $.each(data, function (key) {
                        $('#UDFSelect').append($('<option>', {
                            value: data[key].Value,
                            text: data[key].Text
                        }));
                    });
                    $('#UDFSelect').show();
                    $('#LoadingUDF').hide();
                    $('#CompAV').show();
                    $('#LoadingCompanies').hide();
                }, error: function (xhr) {
                    ErrorCalls(xhr.status);
                },
            });
        }
    });

   

});

function DeleteUDF(IdUDF) {
    $.post("/Users/_DeleteUDF", {
        IdUser: $("#IdUser").val(),
        IdUDF: IdUDF
    })
   .success(function (data) {
       $("#UDFTableContainer").html(data);
       Refresh();
   })
   .error(function (err) { Close(); });
}

function Refresh()
{
    $('#UDFSelect').hide();
    $('#LoadingUDF').show();
    $.ajax({
        url: "/Users/GetUDFByTableID",
        type: "POST",
        data: { pCompanyId: $("#CompAV").val(), pTableID: $('#DocumentSelect').val(), pIdUser: $("#IdUser").val(), pPageKey: $('#Pagekey').val() },
        traditional: true,
        cache: false,
        dataType: 'json',
        success: function (data) {
            $('#UDFSelect').empty();
            $.each(data, function (key) {
                $('#UDFSelect').append($('<option>', {
                    value: data[key].Value,
                    text: data[key].Text
                }));
            });
            $('#UDFSelect').show();
            $('#LoadingUDF').hide();
        }, error: function (xhr) {
            ErrorCalls(xhr.status);
        },
    });
}

//function ErrorUSerCom(data) {
//    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
//    $("#errorMessage").show('slow');
//    $('#errorBox').modal({
//        backdrop: 'static',
//        keyboard: true
//    });
//}



//function ValidateConnection(IdCompany, IdUser) {
//    $('#Loading').modal({
//        backdrop: 'static',
//        keyboard: true
//    });

//    $.post("/Users/ValidateConnection", {
//        pCompanyId: IdCompany,
//        idUSer: IdUser
//    })
//   .success(function (data) {
//       if (data == "Ok")
//       {
//           $('#Loading').modal('hide');
//           $("#infoMessage").empty().append("<strong> Connection validation: Correct </strong>");
//           $("#infoMessage").show('slow');
//           $('#infoBox').modal({
//               backdrop: 'static',
//               keyboard: true
//           });
//       }
//       else
//       {
//           $('#Loading').modal('hide');
//           $("#errorMessage").empty().append("<strong>  Connection validation: Failed </strong>");
//           $("#errorMessage").show('slow');
//           $('#errorBox').modal({
//               backdrop: 'static',
//               keyboard: true
//           });
//       }
//   })
//   .error(function (err) { Close(); });
//}