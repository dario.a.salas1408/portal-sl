﻿function ViewAlert(AlertCode) {

    $.post("/Users/GetAlertById", {
        pAlertCode: AlertCode
    })
    .success(function (data) {
        alertify.alert(data);       
    })
    .error(function (err) { });
}

function ViewMessage(pUserText) {

    alertify.alert(pUserText);
}
