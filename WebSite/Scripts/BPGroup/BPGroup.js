﻿//var used to set the bp that will be added to the actual order
var pSelectedBP = [];

$(document).ready(function () {
    $("#UpdateCorrectly").hide();
    

    $("#formBPGroup").validate({
        rules: {
            Name: "required"

        },
        messages: {
            Name: $("#lblName").text() + $("#REQ").text()
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Edit":
                    Update();
                    break;

                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    $("#SearchBP").click(function () {
        if (typeof assetListBP.dt === 'undefined' || assetListBP.dt == null) {
            assetListBP.init();
        }
        else {
            assetListBP.refresh();
        }
        pSelectedBP = [];
    });

    $("#btnChooseBP").click(function () {
        var mListBP = pSelectedBP;
        $('#Loading').modal({
            backdrop: 'static'
        });
        var data = { pPageKey: $('#Pagekey').val(), pBPList: mListBP };

        $.ajax({
            url: '/BPGroup/AddBPGroupLine',
            data: JSON.stringify(data),
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'html',
            success: function (data, textStatus, jqXHR) {
                $("#TableBPGroupLines").html(data);
                $("#ModalBodyBP").html("");
                $('#Loading').modal("hide");
                $('#BPModal').modal("hide");
                pSelectedBP = [];
            }
        });
    }); 

    function Save() {

        var mBPGroup = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            IdCompany: $("#IdCompany").val(),
            BPGroupLines: []
        };

        $("#TableBPGroupLines :input[type=hidden]").each(function () {
            var input = $(this);
            mBPGroup.BPGroupLines.push({
                Id: 0,
                CardCode: $("#BPGroupLineCC" + input.attr('value')).val(),
                CardName: $("#BPGroupLineCN" + input.attr('value')).val(),
                BPGroup: {}
            });
        });

        $.ajax({
            url: "/BPGroup/Add",
            async: false,
            type: "POST",
            data: JSON.stringify(mBPGroup),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, jqXHR) {
                if (data == "Ok") {
                    location.href = "/BPGroup?IdCompany=" + $("#IdCompany").val();
                }
                else {
                    $('#Loading').modal('hide');
                    ErrorBPGroup(data);
                }
            }
        });
    }

    function Update() {

        var mBPGroup = {
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            IdCompany: $("#IdCompany").val(),
            BPGroupLines: []
        };

        $("#TableBPGroupLines :input[type=hidden]").each(function () {
            var input = $(this);
            mBPGroup.BPGroupLines.push({
                CardCode: $("#BPGroupLineCC" + input.attr('value')).val(),
                CardName: $("#BPGroupLineCN" + input.attr('value')).val(),
                BPGroup: { Id: $("#Id").val() }
            });
        });

        $.ajax({
            url: "/BPGroup/Update",
            async: false,
            type: "POST",
            data: JSON.stringify(mBPGroup),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data, textStatus, jqXHR) {
                if (data == "Ok") {
                    location.href = "/BPGroup?IdCompany=" + $("#IdCompany").val();
                }
                else {
                    $('#Loading').modal('hide');
                    ErrorBPGroup(data);
                }
            }
        });
    }

    function Error(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }

    //Cleanning of the array pSelectedBP when the modal is closed
    $('#BPModal').on('hide.bs.modal', function (e) {
        pSelectedBP = [];
    });

});

function DeleteBP(pCardCode) {
    $('#Loading').modal({
        backdrop: 'static'
    });

    $.ajax({
        url: '/BPGroup/DeleteBP',
        contextType: 'application/html;charset=utf-8',
        data: { pCardCode: pCardCode, pPageKey: $('#Pagekey').val() },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#TableBPGroupLines").html(data);
        $('#Loading').modal("hide");
    });
}

function ErrorBPGroup(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function AddOrRemoveSelectedBP(pCardCode, pCardName, pCheckBoxId) {

    var mChecked = $("input[id='" + pCheckBoxId + "']").is(':checked');
    if (mChecked == true) {
        pSelectedBP.push({ CardCode: pCardCode, CardName: pCardName });
        $("input[id='" + pCheckBoxId + "']").parent().parent().addClass('selected');
    }
    else {
        var index = pSelectedBP.indexOf(pCardCode);
        if (index > -1) {
            pSelectedBP.splice(index, 1);
            $("input[id='" + pCheckBoxId + "']").parent().parent().removeClass('selected');
        }
    }
}

function arrayObjectIndexOf(myArray, searchTerm, property) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) return i;
    }
    return -1;
}

function GetBPModel() {
    $.ajax({
        url: '/BPGroup/GetBPModel',
        contextType: 'application/html;charset=utf-8',
        data: {},
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#ModalBPTable").html(data);
    });
}