﻿$(document).ready(function () {

    $('.input-group.date').datepicker({
        format:"dd/mm/yyyy"
    });

    $('.alert.alert-danger').hide();

    if (formMode == "Update")
    {
        SetVendor($('#txtCodeVendor').val(),$('#txtNameVendor').val());
    }

    $('#Buyer').val("-1");

    $("#formPurchaseOrder").validate({
        rules: {
            txtCodeVendor: "required",
            DocDate: "required",
            DocDueDate: "required",
            TaxDate: "required"
        },
        messages: {
            txtCodeVendor: "Please select the Vendor",
            DocDate: "Please select the Posting Date",
            DocDueDate: "Please select the Delivery Date",
            TaxDate: "Please select the Document Date"
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    $("#btNew").attr("disabled", "disabled");

    $("#txtCodeVendor").attr("disabled", "disabled");

    $("#txtNameVendor").attr("disabled", "disabled");

    $("#TypeCurrency").attr("disabled", "disabled");

    $("#txtCurrencySg").hide();

    $("#SearchItems").click(function () {

        var mItemCode;
        var mItemName;

        if ($('#ckCode').is(':checked')) {
            mItemCode = $('#txtSearch').val();
        }

        if ($('#ckName').is(':checked')) {
            mItemName = $('#txtSearch').val();
        }

        $("#Load").show('slow');

        $.ajax({
            url: '/PurchaseOrder/_Items',
            contextType: 'application/html;charset=utf-8',
            data: { pItemCode: mItemCode, pItemData: mItemName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#Load").hide();
            $("#ModalBody").html(data);
        });

    });

    $("#Load").hide();

    $('#myModal').on('show.bs.modal', function (e) {
        $("#ModalBody").html("");
    });

    $("#LoadVendor").hide();

    $('#VendorModal').on('show.bs.modal', function (e) {
        $("#ModalBodyVendor").html("");
    });

    $("#btnChoose").click(function () {
        var mListItems = [];

        $("#TableItems").find("input:checked").each(function (i, ob) {
            mListItems.push($(ob).val());
        });

        var mTypeCurrency = "";

        switch ($("#TypeCurrency").val()) {
            case "C":
                $("#txtCurrencySg").show();
                mTypeCurrency = $("#txtCurrencySg").val();
                break;
            case "L":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#LocalCurrency").val();
                break;
            case "S":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#SystemCurrency").val();
                break;
        }

        $.ajax({
            url: '/PurchaseOrder/_ItemsForm',
            traditional: true,
            contextType: 'application/html;charset=utf-8',
            data: { pItems: mListItems, pCurrency: mTypeCurrency },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $('#myModal').modal('hide');
            $("#ItemsSelect").html(data);

            var TotalDoc = 0;
            var Disc = 0;
            var mIntRow = 0;

            if ($("#TotalDisc").val() != "") {
                Disc = $("#TotalDisc").val();
            }

            $("#TableItemsForm :input[type=hidden]").each(function () {
                var input = $(this);

                var value = parseFloat($("#td" + input.attr('id')).html());

                TotalDoc = TotalDoc + value;

                mIntRow++;
            });

            $("#TotalDoc").val(TotalDoc);
            $("#TotalDocPD").val(TotalDoc - Disc);

            if (mIntRow > 0) {
                $('.alert.alert-danger').hide();
            }

        });


    });

    $("#SearchVendor").click(function () {
    
        var mVendorCode;
        var mVendorName;

        if ($('#ckCodeVendor').is(':checked')) {
            mVendorCode = $('#txtSearchVendor').val();
        }

        if ($('#ckNameVendor').is(':checked')) {
            mVendorName = $('#txtSearchVendor').val();
        }

        $("#ModalBodyVendor").show('slow');

        $.ajax({
            url: '/PurchaseOrder/_Vendors',
            contextType: 'application/html;charset=utf-8',
            data: { pVendorCode: mVendorCode, pVendorName: mVendorName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadVendor").hide('slow');
            $("#ModalBodyVendor").html(data);
           

        });

    });

    $("#SearchModel").click(function () {

        var mModelCode;
        var mModelName;

        if ($('#ckCodeModel').is(':checked')) {
            mModelCode = $('#txtSearchModel').val();
        }

        if ($('#ckNameModel').is(':checked')) {
            mModelName = $('#txtSearchModel').val();
        }

        $("#modelListSearch").show('slow');

        $.ajax({
            url: '/PurchaseOrder/_ModelListSearch',
            contextType: 'application/html;charset=utf-8',
            data: { pModelCode: mModelCode, pModelName: mModelName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            //$("#LoadVendor").hide('slow');
            $("#modelListSearch").html(data);


        });

    });

    $("#btnSelMatrix").click(function () {

        alert("GILAZO");
        //$('.testing').each(function () {
        //    alert($(this).val());           
        //})

    });

    $("#TypeCurrency").change(function () {
    
        var mTypeCurrency = "";
        var mRow = 0;

        switch ($("#TypeCurrency").val()) {
            case "C":
                $("#txtCurrencySg").show();
                mTypeCurrency = $("#txtCurrencySg").val();
                break;
            case "L":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#LocalCurrency").val();
                break;
            case "S":
                $("#txtCurrencySg").hide();
                mTypeCurrency = $("#SystemCurrency").val();
                break;
        }

        $.ajax({
            url: "/PurchaseOrder/GetRate",
            type: "POST",
            data: { TypeCurrency: mTypeCurrency },
            dataType: 'json',
            success: function (data) {
                $("#RateCurrency").val(data);
            }
        });

        $("#TableItemsForm :input[type=hidden]").each(function () {
            var input = $(this); // This is the jquery object of the input, do what you will

            ChangeValue(input.attr('id'));

            mRow++;
        });

        if (mRow == 0) {
            $("#TotalDoc").val(0);
            $("#TotalDocPD").val(0);
            $("#DiscPrcnt").val(0);
            $("#TotalDisc").val(0);
        }

    });

    $("#DiscPrcnt").change(function () {

        var TotalWDisc = 0;

        if ($("#TotalDoc").val() > 0) {

            if ($("#DiscPrcnt").val() > 100)
            { $("#DiscPrcnt").val(100); }

            TotalWDisc = $("#TotalDoc").val() * ($("#DiscPrcnt").val() / 100);

            $("#TotalDisc").val(parseFloat(TotalWDisc.toFixed(2)));

            $("#TotalDocPD").val($("#TotalDoc").val() - TotalWDisc);

        } else { $("#DiscPrcnt").val(0) }

    });

    $("#TotalDisc").change(function () {

        var TotalWDisc = 0;

        if ($("#TotalDoc").val() > 0) {

            TotalWDisc = ($("#TotalDisc").val() * 100) / $("#TotalDoc").val();

            $("#DiscPrcnt").val(parseFloat(TotalWDisc.toFixed(2)));

            $("#TotalDocPD").val($("#TotalDoc").val() - $("#TotalDisc").val());
        }
        else { $("#TotalDisc").val(0); }

    });


});

function SetVendor(pCode, pName) {
 

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(pName);
    $('#VendorModal').modal('hide');

    $.ajax({
        url: "/PurchaseOrder/GetBp",
        type: "POST",
        data: { Id: pCode, LocalCurrency: $('#LocalCurrency').val() },
        dataType: 'json',
        success: function (data,text) {
            if (data.ErrorResponse == "Ok") {
                $("#ListContact").empty();

                $.each(data.ListContact, function (key) {
                    $('#ListContact').append($('<option>', {
                        value: data.ListContact[key].CrtctCode,
                        text: data.ListContact[key].Name
                    }));
                });

                $("#btNew").removeAttr("disabled");
                $('#RateCurrency').val(data.RateCurrency);
                $('#txtCurrencySg').val(data.Currency);
                $("#txtCurrencySg").attr("disabled", "disabled");
                $("#TypeCurrency").removeAttr("disabled");
                $("#TypeCurrency").val("C");
                $("#TypeCurrency").change();
                $("#txtCurrencySg").show();
            }
            else {
                ErrorPO(data.ErrorResponse);
            }

        }
    });

}

function ChangeValue(Id) {
 
    var count = 0;
    var price = 0;
    var total = 0;

    if ($('#txtcount' + Id).val() != "") {
        count = $('#txtcount' + Id).val();
    }

    if ($('#txt' + Id).val() != "") {
        price = $('#txt' + Id).val();
    }

    var mDpRate;

    $.ajax({
        url: "/PurchaseOrder/GetRate",
        type: "POST",
        data: { TypeCurrency: $('#dp' + Id).val() },
        dataType: 'json',
        success: function (data) {

            total = ((price / $('#RateCurrency').val()) * data) * count;

            $("#td" + Id).html("");
            $("#td" + Id).append(parseFloat(total.toFixed(2)));

            var TotalDoc = 0;
            var Disc = 0;

            if ($("#TotalDisc").val() != "") {
                Disc = $("#TotalDisc").val();
            }

            $("#TableItemsForm :input[type=hidden]").each(function () {
                var input = $(this);

                var value = parseFloat($("#td" + input.attr('id')).html());

                TotalDoc = TotalDoc + value;

            });

            $("#TotalDoc").val(TotalDoc);
            $("#TotalDocPD").val(TotalDoc - Disc);



        }, error: function (xhr) {
            alert('error');
        },
    });

}

function Save() {
    var mTypeCurrency = "";
    var mTotal = parseFloat($("#TotalDocPD").val());
    var mTotalRow = 0;

    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    switch ($("#TypeCurrency").val()) {
        case "C":
            $("#txtCurrencySg").show();
            mTypeCurrency = $("#txtCurrencySg").val();
            break;
        case "L":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#LocalCurrency").val();
            break;
        case "S":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#SystemCurrency").val();
            break;
    }


    var PurchaseOrderView = {
        DocDate: $("#DocDate").val(),
        DocDueDate: $("#DocDueDate").val(),
        TaxDate: $("#TaxDate").val(),
        CardCode: $("#txtCodeVendor").val(),
        DocCur: mTypeCurrency,
        DocTotal: mTotal,
        CardName: $("#txtNameVendor").val(),
        DiscPrcnt: $("#DiscPrcnt").val(),
        CntctCode: $('#ListContact').val(),
        CurSource: $('#CurSource').val(),
        GroupNum: $('#PeyMethod').val(),
        SlpCode: $('#Buyer').val(),
        TrnspCode: $('#ShipType').val(),
        NumAtCard: $('#NumAtCard').val(),
        ListItem: [],
        Lines: []
    };

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        var price = $("#txt" + input.attr('id')).val();

        PurchaseOrderView.Lines.push({
            "ItemCode": input.attr('id'),
            "Quantity": $("#txtcount" + input.attr('id')).val(),
            "Dscription": $("#" + input.attr('id')).val(),
            "Price": parseFloat(price),
            "PriceBefDi": parseFloat(price),
            "Currency": $("#dp" + input.attr('id')).val(),
            "LineNum": input.attr('id')
        });

        mTotalRow++;

    });

    if (mTotalRow == 0) {
        $('.alert.alert-danger').show();
        return;
    }

    $.ajax({
        url: "/PurchaseOrder/Add",
        async: false,
        type: "POST",
        data: JSON.stringify(PurchaseOrderView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                location.href = "/PurchaseOrder/Index";
            }
            else {
                ErrorPO(data);
            }
        }
    });
}

function Update() {
    var mTypeCurrency = "";
    var mTotal = parseFloat($("#TotalDocPD").val());
    var mTotalRow = 0;

    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    })

    switch ($("#TypeCurrency").val()) {
        case "C":
            $("#txtCurrencySg").show();
            mTypeCurrency = $("#txtCurrencySg").val();
            break;
        case "L":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#LocalCurrency").val();
            break;
        case "S":
            $("#txtCurrencySg").hide();
            mTypeCurrency = $("#SystemCurrency").val();
            break;
    }


    var PurchaseOrderView = {
        DocEntry: $("#DocEntry").val(),
        DocDate: $("#DocDate").val(),
        DocDueDate: $("#DocDueDate").val(),
        TaxDate: $("#TaxDate").val(),
        CardCode: $("#txtCodeVendor").val(),
        DocCur: mTypeCurrency,
        DocTotal: mTotal,
        CardName: $("#txtNameVendor").val(),
        DiscPrcnt: $("#DiscPrcnt").val(),
        CntctCode: $('#ListContact').val(),
        CurSource: $('#CurSource').val(),
        GroupNum: $('#PeyMethod').val(),
        SlpCode: $('#Buyer').val(),
        TrnspCode: $('#ShipType').val(),
        NumAtCard: $('#NumAtCard').val(),
        ListItem: [],
        Lines: []
    };

    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this); // This is the jquery object of the input, do what you will
        var price = $("#txt" + input.attr('id')).val();

        PurchaseOrderView.Lines.push({
            "ItemCode": input.attr('id'),
            "Quantity": $("#txtcount" + input.attr('id')).val(),
            "Dscription": $("#" + input.attr('id')).val(),
            "Price": parseFloat(price),
            "PriceBefDi": parseFloat(price),
            "Currency": $("#dp" + input.attr('id')).val(),
            "LineNum": input.attr('id')
        });

        mTotalRow++;

    });

    if (mTotalRow == 0) {
        $('.alert.alert-danger').show();
        return;
    }

    $.ajax({
        url: "/PurchaseOrder/Update",
        async: false,
        type: "POST",
        data: JSON.stringify(PurchaseOrderView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                location.href = "/PurchaseOrder/Index";
            }
            else {
                $('#Loading').modal('hide');
                ErrorPO(data);
            }
        }
    });
}

function DeleteRow(Id) {

    $.post("/PurchaseOrder/DeleteRow", {
        ItemCode: Id
    })
       .success(function (data) {
           if (data == "Ok") {
               $("#tr" + Id).remove();
               $("#TypeCurrency").change();
           }
           else {
               ErrorPO(data);
           }
       });

}

function ErrorPO(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

