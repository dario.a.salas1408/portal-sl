﻿$(document).ready(function () {

    $('#txtDate').datepicker({
        format: 'dd/mm/yyyy'
    });

    $("#LoadVendor").hide();

    $('#VendorModal').on('show.bs.modal', function (e) {
        $("#ModalBodyVendor").html("");
    });

    $("#SearchVendor").click(function () {
       
        var mVendorCode;
        var mVendorName;

        if ($('#ckCodeVendor').is(':checked')) {
            mVendorCode = $('#txtSearchVendor').val();
        }

        if ($('#ckNameVendor').is(':checked')) {
            mVendorName = $('#txtSearchVendor').val();
        }

        $("#LoadVendor").show('slow');

        $.ajax({
            url: '/PurchaseOrder/_Vendors',
            contextType: 'application/html;charset=utf-8',
            data: { pVendorCode: mVendorCode, pVendorName: mVendorName },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#LoadVendor").hide('slow');
            $("#ModalBodyVendor").html(data);
            

        }).fail(function () {
            alert("error");
        }).always(function () {
           
        });

    });

    $("#btSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PurchaseOrder/ListPO',
            contextType: 'application/html;charset=utf-8',
            data: { txtCodeVendor: $("#txtCodeVendor").val(), txtDate: $("#txtDate").val(), txtNro: $("#txtNro").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

           
            $("#BodyPO").html(data);

            $('#Loading').modal('hide');


        }).fail(function () {
            alert("error");
        });

    });

});

function SetVendor(pCode, pName) {

    $('#txtCodeVendor').val(pCode);
    $('#txtNameVendor').val(pName);
    $('#VendorModal').modal('hide');

}