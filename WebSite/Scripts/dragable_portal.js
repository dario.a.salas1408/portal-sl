 
    $(function () {

		// jQuery UI Draggable
        initDragAndDrop();
        // jQuery Ui Droppable
		$(".basket").droppable({
		
			// The class that will be appended to the to-be-dropped-element (basket)
			activeClass:"active",
		
			// The class that will be appended once we are hovering the to-be-dropped-element (basket)
			hoverClass:"hover",
		
			// The acceptance of the item once it touches the to-be-dropped-element basket
			// For different values http://api.jqueryui.com/droppable/#option-tolerance
			tolerance:"touch",
			drop:function (event, ui) {
		
				var basket = $(this),
						move = ui.draggable,
						itemId = basket.find("ul li[data-id='" + move.attr("data-id") + "']");
		
				// To increase the value by +1 if the same item is already in the basket
				if (itemId.html() != null) {
					//itemId.find("input").val(parseInt(itemId.find("input").val()) + 1);
				}
				else {
					// Add the dragged item to the basket
					addBasket(basket, move);
		
					// Updating the quantity by +1" rather than adding it to the basket
					//move.find("input").val(parseInt(move.find("input").val()) + 1);
				}
			}
		});

        // This function runs onc ean item is added to the basket
		function addBasket(basket, move) {
            basket.find("ul").append('<li data-id="' + move.attr("data-id") + '">'
			        + '<input type="hidden" id="Code_' + move.attr("data-id") + '" value="-1">'
                    + '<input type="hidden" id="LineId_' + move.attr("data-id") + '" value="-1">'
                    + '<input type="hidden" id="PicPath_' + move.attr("data-id") + '" value="' + $("#ModelPicDir_" + move.attr("data-id")).val() + '">'
                    + '<img class="img_sm"' + 'src="' + move.find("img").attr("src") + ' ">'
					+ '<span class="name">' + move.find("h3").html() + '</span>'
					+ '<input id="ModDesc_' + move.attr("data-id") + '" class="dragDesc" value="' + move.find("p").html() + '" type="text">'
                    + '<input id="Comment_' + move.attr("data-id") + '" class="dragComment" placeholder="Enter your comments here" type="text">'
					+ '<button class="delete">&#10005;</button>');

            $("#products_drop").find("ul li[data-id='" + move.attr("data-id") + "']").addClass("positionZero");
		}

        // The function that is triggered once delete button is pressed

		$(document).on("click", ".basket ul li button.delete", function () {
			
			//toma el valor del item agregado
			var valor_item = $(this).closest("li").attr("data-id");
			//alert(valor_item);
			
			
			$(this).closest("li").remove();	
			
			//toma el dato del item agregado y saca el class
			$("#products_drop").find("ul li[data-id='" + valor_item + "']").removeClass("border_img");

		});
		
        $(".basket ul li input.count").delegate("click", function () {
		this.value = '';		
		});


    });
	
	
	$(function() {
    $( ".droppable" ).droppable({
    drop: function( event, ui ) {
    $( this )
    .addClass( "ui-state-highlight" )
    }
    });
    });
	
	
	
	function initDragAndDrop()
	{
	    $("#products_drop li").draggable({

	        // brings the item back to its place when dragging is over
	        revert: true,

	        // once the dragging starts, we decrease the opactiy of other items
	        // Appending a class as we do that with CSS
	        drag: function () {
	            $(this).addClass("active");
	            //$(this).closest("#product").addClass("active");


	        },

	        // removing the CSS classes once dragging is over.
	        stop: function () {
	            $(this).removeClass("active").closest("#products_drop").removeClass("active");
	            $(this).addClass("border_img");

	        }
	    });
	}