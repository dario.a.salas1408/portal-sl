﻿

$(document).ready(function () {
    $("#formQuery").validate({
        rules: {
            QueryIdentifier: "required",
            IdCompany: "required",
            SelectField: "required",
            FromField: "required"

        },
        messages: {
            QueryIdentifier: $("#lblQueryIdentifier").text() + $("#REQ").text(),
            IdCompany: $("#lblIdCompany").text() + $("#REQ").text(),
            SelectField: $("#lblSelectField").text() + $("#REQ").text(),
            FromField: $("#lblFromField").text() + $("#REQ").text()

        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                case "Delete":
                    $("#myModalLabel").empty().append("Delete Query");

                    $('#myModal').modal({
                        backdrop: 'static',
                        keyboard: true

                    })

                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });


    function Save() {

        $.post("/QueryManager/Add", {
            QueryIdentifier: $("#QueryIdentifier").val(),
            IdCompany: $("#IdCompany").val(),
            SelectField: $("#SelectField").val(),
            FromField: $("#FromField").val(),
            WhereField: $("#WhereField").val(),
            OrderByField: $("#OrderByField").val(),
            GroupByField: $("#GroupByField").val()
        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/QueryManager/Index"; }
            else
            {
                Error(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Update() {

        $.post("/QueryManager/Update", {
            IdQuery: $("#IdQuery").val(),
            QueryIdentifier: $("#QueryIdentifier").val(),
            IdCompany: $("#IdCompany").val(),
            SelectField: $("#SelectField").val(),
            FromField: $("#FromField").val(),
            WhereField: $("#WhereField").val(),
            OrderByField: $("#OrderByField").val(),
            GroupByField: $("#GroupByField").val()

        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/QueryManager/Index"; }
            else
            {
                Error(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Delete() {

        $.post("/QueryManager/Delete", {
            IdQuery: $("#IdQuery").val()
        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/QueryManager/Index"; }
            else
            {
                Error(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Error(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }

    $("#Ok").click(function () {
        Delete();
    });

});