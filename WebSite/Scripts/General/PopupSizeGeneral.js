﻿var fit_modal_body;

fit_modal_body = function (modal) {
    var body, bodypaddings, header, headerheight, height, modalheight;
    header = $(".modal-header", modal);
    footer = $(".modal-footer", modal);
    body = $(".panel-body", modal);
    modalheight = parseInt(modal.css("height"));
    headerheight = parseInt(header.css("height")) + parseInt(header.css("padding-top")) + parseInt(header.css("padding-bottom"));
    footerheight = parseInt(footer.css("height")) + parseInt(footer.css("padding-top")) + parseInt(footer.css("padding-bottom"));
    bodypaddings = parseInt(body.css("padding-top")) + parseInt(body.css("padding-bottom"));
    height = $(window).height() - headerheight - footerheight - bodypaddings - 50;
    return body.css({ "max-height": "" + height + "px", 'height': 'auto' });
};
fit_modal_body($(".modal"));
$(window).resize(function () {
    return fit_modal_body($(".modal"));
});

var fut_modal_body;

fut_modal_body = function (modal) {
    var body, bodypaddings, header, headerheight, height, modalheight;
    header = $(".modal-header", modal);
    footer = $(".modal-footer", modal);
    body = $(".panel-body", modal);
    modalheight = parseInt(modal.css("height"));
    headerheight = parseInt(header.css("height")) + parseInt(header.css("padding-top")) + parseInt(header.css("padding-bottom"));
    footerheight = parseInt(footer.css("height")) + parseInt(footer.css("padding-top")) + parseInt(footer.css("padding-bottom"));
    bodypaddings = parseInt(body.css("padding-top")) + parseInt(body.css("padding-bottom"));
    height = $(window).height() - headerheight - footerheight - bodypaddings - 100;
    return body.css({ "max-height": "" + height + "px", 'height': 'auto' });
};

fut_modal_body($(".modal"));

$(window).load(function () {
    return fut_modal_body($(".modal"));
});





var fat_modal_body;

fat_modal_body = function (modal) {
    var body, bodypaddings, header, headerheight, height, modalheight;
    header = $(".modal-header", modal);
    footer = $(".modal-footer", modal);
    body = $(".udf_box_limitado", modal);
    modalheight = parseInt(modal.css("height"));
    headerheight = parseInt(header.css("height")) + parseInt(header.css("padding-top")) + parseInt(header.css("padding-bottom"));
    footerheight = parseInt(footer.css("height")) + parseInt(footer.css("padding-top")) + parseInt(footer.css("padding-bottom"));
    bodypaddings = parseInt(body.css("padding-top")) + parseInt(body.css("padding-bottom"));
    height = $(window).height() - headerheight - footerheight - bodypaddings - 200;
    return body.css({ "max-height": "" + height + "px", 'height': 'auto' });
};

fat_modal_body($(".udf_box_limitado"));

$(window).load(function () {
    return fat_modal_body($(".udf_box_limitado"));
});
