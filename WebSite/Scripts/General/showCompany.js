﻿$(document).ready(function () {

    $(document).ready(listenWidth);
    $(window).resize(listenWidth);
    function listenWidth(e) {
        if ($(window).width() < 960) {
            $("li.company").remove().insertBefore($(".navbar-collapse"));
        } else {
            $("li.company").remove().insertAfter($("li.usuario"));
        }
    }
});