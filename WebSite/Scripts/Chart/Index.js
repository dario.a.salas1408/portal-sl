﻿var IdChartToDelete = -1;
$(document).ready(function () {

    $("#Ok").click(function () {
        Delete();
    });

});

function DeleteChart(IdChart) {
    $("#myModalLabel").empty().append("Delete Chart");

    $('#myModal').modal({
        backdrop: 'static',
        keyboard: true

    });
    IdChartToDelete = IdChart;
}

function Delete() {
    if (IdChartToDelete != -1)
    {
        $.post("/Chart/Delete", {
            mPortalChartId: IdChartToDelete
        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/Chart"; }
            else {
                ErrorCo(data);
            }
        })
        .error(function (err) { Close(); });
    }
}