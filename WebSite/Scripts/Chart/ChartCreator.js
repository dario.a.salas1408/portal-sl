﻿$(document).ready(function () {

    $("#LoadSAPQuery").hide();
    $("#UpdateCorrectly").hide();

    $("#SearchSAPQuery").click(function () {
        $("#LoadSAPQuery").show('slow');
        $.ajax({
            url: '/Chart/_GetSAPQueryList',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $("#PageKey").val() },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#LoadSAPQuery").hide();
            $("#ModalBodySAPQuery").html(data);
        });
    });

    $("#ExecQuery").click(function () {
        var mParamList = [];
        var listParamName = $("input[id^=ParamName]");
        for (i = 0; i < listParamName.length; i++) {
            var mParamId = listParamName[i].id.substring(9);
            mParamList.push({
                ParamName: $("#ParamName" + mParamId).val(),
                ParamValue: $("#ParamValue" + mParamId).val(),
                ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            });
        }
        $.ajax({
            url: '/Chart/_GetSAPQueryResultTable',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $("#PageKey").val(), pQueryInternalKey: $("#QueryInternalKey").val(), pListParams: mParamList },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#ItemsQuery").html(data);
        });
    });

    $("#DrawChart").click(function () {
        if ($("#PortalChartType").val() == 0) {
            alert("Please select a chart type");
            return;
        }

        $("#LoadSAPQuery").show('slow');
        var listColumnsSelect = $("select[id^=PortalChartColumn]");
        var mParamList = [];
        var listParamName = $("input[id^=ParamName]");
        for (i = 0; i < listParamName.length; i++) {
            var mParamId = listParamName[i].id.substring(9);
            mParamList.push({
                ParamName: $("#ParamName" + mParamId).val(),
                ParamValue: $("#ParamValue" + mParamId).val(),
                ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            });
        }

        getDataAndDrawChart($("#QueryInternalKey").val(), $("#PortalChartType").val(), (listColumnsSelect.length > 0 ? $("#PortalChartColumn" + listColumnsSelect[0].id.substring(17)).val() : ""), (listColumnsSelect.length > 0 ? $("#PortalChartColumn" + listColumnsSelect[1].id.substring(17)).val() : ""), "ChartDrawPanel", mParamList, 0);
    });

    $('#PortalChartType').change(function () {
        $("#LoadSAPQuery").show('slow');
        var mParamList = [];
        var listParamName = $("input[id^=ParamName]");
        for (i = 0; i < listParamName.length; i++) {
            var mParamId = listParamName[i].id.substring(9);
            mParamList.push({
                ParamName: $("#ParamName" + mParamId).val(),
                ParamValue: $("#ParamValue" + mParamId).val(),
                ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            });
        }
        $.ajax({
            url: '/Chart/_GetPortalChartTypeById',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $("#PageKey").val(), pPortalChartTypeId: $("#PortalChartType").val(), pQueryInternalKey: $("#QueryInternalKey").val(), pListParams: mParamList },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#PortalChartColumns").html(data);
        }); 
    }); 

    $("#btnOkChart").click(function () {
        switch(formMode)
        {
            case "Update":
                UpdateChart();
                break;
            case "Add":
                AddChart();
                break;
        }
    });

    if ($("#UGAll").is(':checked')) {
        var mListUGCk = $("input[id^=UGCk_]");
        for (i = 0; i < mListUGCk.length; i++) {
            $("#" + mListUGCk[i].id).attr('disabled', 'disabled');
        }
    }

    $("#UGAll").change(function () {
        if ($('#UGAll').is(':checked')) {
            var mListUGCk = $("input[id^=UGCk_]");
            for (i = 0; i < mListUGCk.length; i++) {
                $("#" + mListUGCk[i].id).attr('disabled', 'disabled');
            }
        }
        else {
            var mListUGCk = $("input[id^=UGCk_]");
            for (i = 0; i < mListUGCk.length; i++) {
                $("#" + mListUGCk[i].id).removeAttr('disabled');
            }
        }
    });
});

function SetQuery(pQueryName, pQueryInternalKey) {
    $.ajax({
        url: '/Chart/_GetSAPQueryParamList',
        contextType: 'application/html;charset=utf-8',
        data: { pPageKey: $("#PageKey").val(), pQueryInternalKey: pQueryInternalKey },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#QueryName").val(pQueryName);
        $("#QueryInternalKey").val(pQueryInternalKey);
        $("#ParamsDiv").html(data);
        $('#SAPQueryModal').modal('hide');
        $('#LoadSAPQuery').modal('hide');
    });
}

function ChartViewer(pChartId) {
    $('#ChartModal').modal('show');
    $("#LoadChart").hide();
    $.ajax({
        url: '/Chart/_ChartViewer',
        contextType: 'application/html;charset=utf-8',
        data: { IdChart: pChartId },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#LoadChart").hide();
        $("#PanelChartViewer").html(data);
    }).fail(function () {
    });
} 

function getDataAndDrawChart(pQueryInternalKey, pIdChartType, pAxisXName, pAxisYName, pDivToDraw, pListParams, pIdChartPortal) {
    if (pIdChartType == 3)
    {
        $.ajax({
            url: '/Chart/_GetSAPQueryResultTable',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $("#PageKey").val(), pQueryInternalKey: pQueryInternalKey, pListParams: pListParams },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            var jsonObj;
            try {
                jsonObj = $.parseJSON(data);
            }
            catch (Error){
            }
            if (jsonObj !== undefined) {
                if (jsonObj.ServiceAnswer === "Error") {
                    $("#" + pDivToDraw).html(jsonObj.UserMsg);
                }
            }
            else{
                $("#" + pDivToDraw).html(data);
            }
            if (pIdChartPortal != 0)
            {
                $("#ChartDrawPanelOverlay_" + pIdChartPortal).hide();
                $("#ChartDrawPanelLoading_" + pIdChartPortal).hide();
            }
        });

        return true;
    }
    else
    {
        $.ajax({
            url: '/Chart/_DrawChartInPortalPage',
            contextType: 'application/html;charset=utf-8',
            data: { pPageKey: $("#PageKey").val(), pQueryInternalKey: pQueryInternalKey, pListParams: pListParams },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            var jsonObj = $.parseJSON(data);
            //Hago una segunda conversion dado que en la primera solo elimina las comillas del objeto principal
            jsonObj = $.parseJSON(jsonObj);
            if (jsonObj != null) {
                var chartData = [];
                var columnsId = [];
                for (var i = 0; i < jsonObj.Row[0].Property.length; i++) {
                    //In position zero i save the x value
                    if (jsonObj.Row[0].Property[i].Name == pAxisXName)
                    {
                        columnsId[0] = i;
                    }
                    //In position zero i save the y value
                    if (jsonObj.Row[0].Property[i].Name == pAxisYName)
                    {
                        columnsId[1] = i;
                    }
                }
                for (var i = 0; i < jsonObj.Row.length; i++) {
                    chartData.push({
                                [jsonObj.Row[i].Property[columnsId[0]].Name]: jsonObj.Row[i].Property[columnsId[0]].Value,
                                [jsonObj.Row[i].Property[columnsId[1]].Name]: Number(jsonObj.Row[i].Property[columnsId[1]].Value)
                    });
                }
                drawChart(pIdChartType, chartData, pAxisXName, pAxisYName, pDivToDraw);
            }
            if (pIdChartPortal != 0) {
                $("#ChartDrawPanelOverlay_" + pIdChartPortal).hide();
                $("#ChartDrawPanelLoading_" + pIdChartPortal).hide();
            }
        });

        return true;
    }
}

function drawChart(pChartType, pChartData, pAxisName, pAxisValue, pDivToDraw) {

    switch (pChartType) {
        case "1":
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = pChartData;
            chart.categoryField = pAxisName;
            // this single line makes the chart a bar chart,
            // try to set it to false - your bars will turn to columns
            chart.rotate = false;
            chart.startDuration = 1;
            chart["export"] = {
                "enabled": true
            };

            // AXES
            // Category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.gridPosition = "start";
            categoryAxis.axisColor = "#DADADA";
            categoryAxis.fillAlpha = 1;
            categoryAxis.gridAlpha = 0;
            categoryAxis.fillColor = "#FAFAFA";

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.axisColor = "#DADADA";
            chart.addValueAxis(valueAxis);

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.title = pAxisValue;
            graph.valueField = pAxisValue;
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillColors = "#0080FA";
            graph.fillAlphas = 1;
            chart.addGraph(graph);

            // WRITE
            chart.write(pDivToDraw);
            break;

        case "2":
            var chart;
            var legend;
            // PIE CHART
            chart = new AmCharts.AmPieChart();
            chart.dataProvider = pChartData;
            chart.titleField = pAxisName;
            chart.valueField = pAxisValue;
            chart.outlineColor = "#FFFFFF";
            chart.outlineAlpha = 0.8;
            chart.outlineThickness = 2;
            chart["export"] = {
                "enabled": true
            };

            // WRITE
            chart.write(pDivToDraw);
            break;

        case "4":
            chart = new AmCharts.AmRadarChart();
            chart.dataProvider = pChartData;
            chart.categoryField = pAxisName;
            chart.rotate = false;
            chart.startDuration = 2;
            chart["export"] = {
                "enabled": true
            };

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.title = pAxisValue;
            graph.valueField = pAxisValue;
            graph.lineThickness = 2;
            graph.bullet = "round";
            chart.addGraph(graph);

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.axisTitleOffset = 20;
            valueAxis.minimum = 0;
            valueAxis.axisAlpha = 0.15;
            chart.addValueAxis(valueAxis);

            // WRITE
            chart.write(pDivToDraw);

            break;
    }
}

function ParamValueChange(mParaValueId) {
    switch ($("#ParamValue" + mParaValueId).val())
    {
        case "FixedValue":
            $("#ParamValueType" + mParaValueId).css("display", "none");
            $("#ParamValueTypeLabel" + mParaValueId).css("display", "none");
            $("#ParamValueTypeDiv" + mParaValueId).removeClass("col-md-9");
            $("#ParamValueTypeLabel" + mParaValueId).removeClass("col-md-3");

            $("#ParamFixedDiv" + mParaValueId).addClass("col-md-9");
            $("#ParamFixedLabel" + mParaValueId).addClass("col-md-3");
            $("#ParamFixedValue" + mParaValueId).css("display", "");
            $("#ParamFixedLabel" + mParaValueId).css("display", "");
            break;
        case "EnteredByUser":
            $("#ParamFixedValue" + mParaValueId).css("display", "none");
            $("#ParamFixedLabel" + mParaValueId).css("display", "none");
            $("#ParamFixedDiv" + mParaValueId).removeClass("col-md-9");
            $("#ParamFixedLabel" + mParaValueId).removeClass("col-md-3");

            $("#ParamValueTypeDiv" + mParaValueId).addClass("col-md-9");
            $("#ParamValueTypeLabel" + mParaValueId).addClass("col-md-3");
            $("#ParamValueType" + mParaValueId).css("display", "");
            $("#ParamValueTypeLabel" + mParaValueId).css("display", "");
            break;
        default:
            $("#ParamFixedValue" + mParaValueId).css("display", "none");
            $("#ParamFixedLabel" + mParaValueId).css("display", "none");
            $("#ParamValueType" + mParaValueId).css("display", "none");
            $("#ParamValueTypeLabel" + mParaValueId).css("display", "none");
            break;
    }
}

function UpdateChart() {
    $('#Loading').modal({
        backdrop: 'static'
    });

    var mUGPortalList = [];
    if ($("#UGAll").is(':checked') == false) {
        var mListUGCk = $("input[id^=UGCk_]");
        for (i = 0; i < mListUGCk.length; i++) {
            if ($("#" + mListUGCk[i].id).is(':checked')) {
                mUGPortalList.push({
                    PortalChart_IdChart: $("#IdChart").val(),
                    UserGroup_Id: mListUGCk[i].id.substring(5),
                });
            }
        }
    }

    var PortalChartView = {
        IdChart: $("#IdChart").val(),
        ChartName: $("#ChartName").val(),
        IdSAPQuery: $("#QueryInternalKey").val(),
        NameSAPQuery: $("#QueryName").val(),
        IdChartType: $("#PortalChartType").val(),
        PortalPage: $("#PortalChartPageToShow").val(),
        IdCompany: $("#IdCompany").val(),
        MenuChart: $("#ShowMenuCk").is(':checked'),
        PortalChartColumns: [],
        PortalChartParamList: [],
        PortalChartUserGroups: mUGPortalList
    };

    var listColumnsSelect = $("select[id^=PortalChartColumn]");
    for (i = 0; i < listColumnsSelect.length; i++) {
        var mSelectId = listColumnsSelect[i].id.substring(17);
        PortalChartView.PortalChartColumns.push({
            IdPortalColumn: mSelectId,
            IdChart: $("#IdChart").val(),
            MappedFieldName: $("#" + listColumnsSelect[i].id).val()
        });
    }

    var listParamName = $("input[id^=ParamName]");
    for (i = 0; i < listParamName.length; i++) {
        var mParamId = listParamName[i].id.substring(9);
        PortalChartView.PortalChartParamList.push({
            Id: mParamId,
            PortalChart_IdChart: $("#IdChart").val(),
            ParamName: $("#ParamName" + mParamId).val(),
            ParamNamePortal: $("#PortalParamName" + mParamId).val(),
            ParamValue: $("#ParamValue" + mParamId).val(),
            ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            ParamValueType: $("#ParamValueType" + mParamId).val(),
            IsNew: $("#IsNew" + mParamId).val(),
            PortalChart: { IdChart: $("#IdChart").val() }
        });
    }

    $.ajax({
        url: '/Chart/Update',
        async: false,
        type: "POST",
        data: JSON.stringify(PortalChartView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data.ServiceAnswer == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                $('#Loading').modal('hide');
                ErrorMessage(data.ErrorMsg);
            }
        }
    });
}

function AddChart() {

    $('#Loading').modal({
        backdrop: 'static'
    });

    var mUGPortalList = [];
    if ($("#UGAll").is(':checked') == false) {
        var mListUGCk = $("input[id^=UGCk_]");
        for (i = 0; i < mListUGCk.length; i++) {
            if ($("#" + mListUGCk[i].id).is(':checked')) {
                mUGPortalList.push({
                    PortalChart_IdChart: $("#IdChart").val(),
                    UserGroup_Id: mListUGCk[i].id.substring(5),
                });
            }
        }
    }

    var PortalChartView = {
        IdChart: $("#IdChart").val(),
        ChartName: $("#ChartName").val(),
        IdSAPQuery: $("#QueryInternalKey").val(),
        NameSAPQuery: $("#QueryName").val(),
        IdChartType: $("#PortalChartType").val(),
        PortalPage: $("#PortalChartPageToShow").val(),
        IdCompany: $("#IdCompany").val(),
        MenuChart: $("#ShowMenuCk").is(':checked'),
        PortalChartColumns: [],
        PortalChartParamList: [],
        PortalChartUserGroups: mUGPortalList
    };

    var listColumnsSelect = $("select[id^=PortalChartColumn]");
    for (i = 0; i < listColumnsSelect.length; i++) {
        var mSelectId = listColumnsSelect[i].id.substring(17);
        PortalChartView.PortalChartColumns.push({
            IdPortalColumn: mSelectId,
            IdChart: $("#IdChart").val(),
            MappedFieldName: $("#" + listColumnsSelect[i].id).val()
        });
    }

    var listParamName = $("input[id^=ParamName]");
    for (i = 0; i < listParamName.length; i++) {
        var mParamId = listParamName[i].id.substring(9);
        PortalChartView.PortalChartParamList.push({
            Id: mParamId,
            PortalChart_IdChart: $("#IdChart").val(),
            ParamName: $("#ParamName" + mParamId).val(),
            ParamNamePortal: $("#PortalParamName" + mParamId).val(),
            ParamValue: $("#ParamValue" + mParamId).val(),
            ParamFixedValue: $("#ParamFixedValue" + mParamId).val(),
            ParamValueType: $("#ParamValueType" + mParamId).val(),
            IsNew: $("#IsNew" + mParamId).val(),
            PortalChart: null
        });
    }

    $.ajax({
        url: '/Chart/Add',
        async: false,
        type: "POST",
        data: JSON.stringify(PortalChartView),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data.ServiceAnswer == "Ok") {
                $('#Loading').modal('hide');
                DocumentAddUpdateMessage(data.AddUpdateMsg, window.location.href, urlRedirect);
            }
            else {
                $('#Loading').modal('hide');
                ErrorMessage(data.ErrorMsg);
            }
        }
    });
}

function DrawChartWithParamsEnteredByUser(pIdChart)
{
    var mListSearchParamName = $("input[id^=ChartParamName_" + pIdChart + "]");

    for (var i = 0; i < mListSearchParamName.length; i++) {
        mParamsArray[pIdChart][0].push({
            ParamName: mListSearchParamName[i].value,
            ParamValue: "EnteredByUser",
            ParamFixedValue:  $("input[id='ChartParamValue_" + pIdChart + "_" + mListSearchParamName[i].value + "']").val()
        });
    }
    $("#ChartDrawPanelParams_" + pIdChart).hide();
    $("#ChartDrawPanelOverlay_" + pIdChart).show();
    $("#ChartDrawPanelLoading_" + pIdChart).show();  

    getDataAndDrawChart(mParamsArray[pIdChart][1], mParamsArray[pIdChart][2], mParamsArray[pIdChart][3], mParamsArray[pIdChart][4], "ChartDrawPanel_" + pIdChart, mParamsArray[pIdChart][0], pIdChart);
}

function ReloadChartParams(pIdChart)
{
    mParamsArray[pIdChart][0] = jQuery.grep(mParamsArray[pIdChart][0], function (mInternalArray) {
        return mInternalArray.ParamName != $("#ChartParamName_" + pIdChart).val();
    });

    $("#ChartDrawPanelParams_" + pIdChart).show();
    $("#ChartDrawPanelOverlay_" + pIdChart).hide();
    $("#ChartDrawPanelLoading_" + pIdChart).hide();
}

function ErrorMessage(data) {
    $("#errorMessage").empty().append("<strong>" + data + "</strong>");
    $("#errorMessage").show('slow');
    $('#errorBox').modal({
        backdrop: 'static',
        keyboard: true
    });
}

function DocumentAddUpdateMessage(message, urlAddNew, urlGoBack) {
    $("#DocumentAddUpdateMessageContent").empty().append("<strong>" + message + "</strong>");
    $("#DocumentAddUpdateMessageContent").show('slow');
    $('#DocumentAddUpdateMessageBox').modal({
        backdrop: 'static',
        keyboard: true
    });
    $("#AddNewDocumentAddUpdateMessage").click(function () {
        $('#Loading').modal({
            backdrop: 'static'
        })
        location.href = urlAddNew;
    });
    $("#GoBackDocumentAddUpdateMessage").click(function () {
        $('#Loading').modal({
            backdrop: 'static'
        })
        location.href = urlGoBack;
    });
}