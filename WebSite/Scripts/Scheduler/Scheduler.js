﻿var mStart, mEnd;
var mViewDataStart, mViewDataEnd
var mHIni, mHEnd

$(document).ready(function () {

    $("#contenido").hide();

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,listMonth'
        },
        //defaultDate: '2016-09-12',
        navLinks: true, // can click day/week names to navigate views
        selectable: true,
        selectHelper: true,
        eventClick: function(calEvent, jsEvent, view) {

            $.ajax({
                url: '/CRM/CRMActivity/_Activity',
                contextType: 'application/html;charset=utf-8',
                data: { pAction: "Update", pCode: calEvent.id, pFrom: "Calendar" },
                type: 'POST',
                dataType: 'html'
            }).done(function (data) {

                $("#contenido").html(data);

                $("#calendar").hide("clip", 500, callback);

            }).fail(function () {

            });

            // change the border color just for fun
            $(this).css('border-color', 'red');

        },
        viewRender: function (view, element) {
            
            $('#Loading').modal({
                backdrop: 'static',
                keyboard: true
            });

            mViewDataStart = moment(view.start).format('MM/DD/YYYY');
            mViewDataEnd = moment(view.end).format('MM/DD/YYYY');

            $.ajax({
                url: '/CRM/CRMActivity/ActivityListCalendar',
                data: { txtStartDate: moment(view.start).format('MM/DD/YYYY'), txtEndDate: moment(view.end).format('MM/DD/YYYY'), IsMyActivity: false },
                type: 'POST',
                dataType: 'json'
            }).done(function (data) {

                $('#calendar').fullCalendar('removeEvents', function (e) { return !e.isUserCreated });

                var eventData;

                $.each(data, function (key) {

                    if (data[key].BeginTime.toString().length > 3)
                    {
                        mHIni = data[key].BeginTime.toString().substring(0, 2) + ":" + data[key].BeginTime.toString().substring(4, 2)
                    }
                    else
                    {
                        if (data[key].BeginTime.toString().length > 2)
                        {
                            mHIni = data[key].BeginTime.toString().substring(0, 1) + ":" + data[key].BeginTime.toString().substring(4, 1)
                        }
                        else
                        {
                            mHIni = "00:" + data[key].BeginTime.toString();
                        }
                    }


                    if (data[key].ENDTime.toString().length > 3) {
                        mHEnd = data[key].ENDTime.toString().substring(0, 2) + ":" + data[key].ENDTime.toString().substring(4, 2)
                    }
                    else {
                        if (data[key].ENDTime.toString().length > 2) {
                            mHEnd = data[key].ENDTime.toString().substring(0, 1) + ":" + data[key].ENDTime.toString().substring(4, 1)
                        }
                        else {
                            mHEnd = "00:" + data[key].ENDTime.toString();
                        }
                    }

                    eventData = {
                        id: data[key].ClgCode,
                        title: data[key].ActionName,
                        start: (moment(data[key].Recontact).format('MM/DD/YYYY') + " " + mHIni),
                        end: (moment(data[key].endDate).format('MM/DD/YYYY') + " " + mHEnd)
                    };
                    $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

                });
                $('#calendar').fullCalendar('unselect');

                $('#Loading').modal('hide');

            }).fail(function () {

            });

        },
        select: function (start, end) {

            mStart = start;
            mEnd = end;

            $.ajax({
                url: '/CRM/CRMActivity/_Activity',
                contextType: 'application/html;charset=utf-8',
                data: { pAction: "Add", pCode: 0, pFrom: "Calendar" },
                type: 'POST',
                dataType: 'html'
            }).done(function (data) {

                $("#contenido").html(data);

                $("#Recontact").val(moment(start).format('MM/DD/YYYY'));
                $("#BeginTime").val(moment(start).format('HH:mm'));

                $("#endDate").val(moment(end).format('MM/DD/YYYY'));
                $("#ENDTime").val(moment(end).format('HH:mm'));

                $("#calendar").hide("clip", 500, callback);

            }).fail(function () {

            });
        },
        editable: true,
        eventDrop: function (event, delta, revertFunc) {

            if (event.end == null)
            {
                event.end = event.start;
            }

            $.ajax({
                url: '/CRM/CRMActivity/UpdateDragDrop',
                data: { pCode: event.id, txtStartDate: moment(event.start).format('MM/DD/YYYY'), txtEndDate: moment(event.end).format('MM/DD/YYYY'), HIni: moment(event.start).format('HH:mm'), Hend: moment(event.end).format('HH:mm') },
                type: 'POST',
                dataType: 'json'
            }).done(function (data) {

                if (data != "Ok")
                {
                    revertFunc();
                }
                
            }).fail(function () {

            });

        },
        eventResize: function(event, delta, revertFunc) {

            if (event.end == null) {
                event.end = event.start;
            }

            $.ajax({
                url: '/CRM/CRMActivity/UpdateDragDrop',
                data: { pCode: event.id, txtStartDate: moment(event.start).format('MM/DD/YYYY'), txtEndDate: moment(event.end).format('MM/DD/YYYY'), HIni: moment(event.start).format('HH:mm'), Hend: moment(event.end).format('HH:mm') },
                type: 'POST',
                dataType: 'json'
            }).done(function (data) {

                if (data != "Ok") {
                    revertFunc();
                }

            }).fail(function () {

            });

        },
        eventLimit: true, // allow "more" link when too many events
        events: []
    });

    $(document).on('click', '#btnCancelFromCalendar', function () {
        $("#contenido").hide("clip", 500, callbackCancel);
    });


});

function callback() {
    $("#contenido").show("clip", 500);
};

function callbackCancel() {
    $("#calendar").show("clip", 500);
};

function SaveCalendar()
{
    
    $("#contenido").hide("clip", 500, callbackCancel);

    $.ajax({
        url: '/CRM/CRMActivity/ActivityListCalendar',
        data: { txtStartDate: mViewDataStart, txtEndDate: mViewDataEnd, IsMyActivity: false },
        type: 'POST',
        dataType: 'json'
    }).done(function (data) {

        $('#calendar').fullCalendar('removeEvents', function (e) { return !e.isUserCreated });

        var eventData;

        $.each(data, function (key) {

            if (data[key].BeginTime.toString().length > 3) {
                mHIni = data[key].BeginTime.toString().substring(0, 2) + ":" + data[key].BeginTime.toString().substring(4, 2)
            }
            else {
                if (data[key].BeginTime.toString().length > 2) {
                    mHIni = data[key].BeginTime.toString().substring(0, 1) + ":" + data[key].BeginTime.toString().substring(4, 1)
                }
                else {
                    mHIni = "00:" + data[key].BeginTime.toString();
                }
            }


            if (data[key].ENDTime.toString().length > 3) {
                mHEnd = data[key].ENDTime.toString().substring(0, 2) + ":" + data[key].ENDTime.toString().substring(4, 2)
            }
            else {
                if (data[key].ENDTime.toString().length > 2) {
                    mHEnd = data[key].ENDTime.toString().substring(0, 1) + ":" + data[key].ENDTime.toString().substring(4, 1)
                }
                else {
                    mHEnd = "00:" + data[key].ENDTime.toString();
                }
            }


            eventData = {
                id: data[key].ClgCode,
                title: data[key].ActionName,
                start: (moment(data[key].Recontact).format('MM/DD/YYYY') + " " + mHIni),
                end: (moment(data[key].endDate).format('MM/DD/YYYY') + " " + mHEnd)
            };
            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true

        });
        $('#calendar').fullCalendar('unselect');

    }).fail(function () {

    });

}