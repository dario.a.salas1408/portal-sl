﻿

$(document).ready(function () {
    $("#formNews").validate({
        rules: {
            Head: "required",
            Description: "required"
           
        },
        messages: {
            Head: $("#lblHead").text() + $("#REQ").text(),
            Description: $("#lblDescription").text() + $("#REQ").text()
            
        },
        submitHandler: function (form) {
            SubmitForm();
        }
    });

    function Error(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }

    $("#Ok").click(function () {
        Delete();
    });

});


function Save() {

    var data = new FormData();
    var fileInput = $("#file-ImgUpload").fileinput('getFileStack');

    if (fileInput.length > 0) {
        for (var i = 0; i < fileInput.length; i++) {
            data.append(fileInput[i].name, fileInput[i]);
        }
    }

    var New = {
        Id: 0,
        Head: $("#Head").val(),
        Url: $("#Url").val(),
        Description: $("#Description").val(),
        ImgUploadChange: $("#ImgUploadChange").val()
    };

    data.append('pNew', JSON.stringify(New));

    $.ajax({
        url: "/News/Add",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "Ok")
            { location.href = "/News/Index"; }
            else
            {
                Error(data);
            }
        }
    });
}

function Update() {

    var data = new FormData();
    var fileInput = $("#file-ImgUpload").fileinput('getFileStack');

    if (fileInput.length > 0) {
        for (var i = 0; i < fileInput.length; i++) {
            data.append(fileInput[i].name, fileInput[i]);
        }
    }

    var New = {
        Id: $("#Id").val(),
        Head: $("#Head").val(),
        Url: $("#Url").val(),
        Description: $("#Description").val(),
        ImgUploadChange: $("#ImgUploadChange").val()
    };

    data.append('pNew', JSON.stringify(New));

    $.ajax({
        url: "/News/Update",
        type: "POST",
        data: data,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "Ok")
            { location.href = "/News/Index"; }
            else
            {
                Error(data);
            }
        }
    });
}

function Delete() {

    $.post("/News/Delete", {
        Id: $("#Id").val()
    })
    .success(function (data) {
        if (data == "Ok")
        { location.href = "/News/Index"; }
        else
        {
            Error(data);
        }
    })
    .error(function (err) { Close(); });
}

function SubmitForm() {
    switch (formMode) {
        case "Add":
            Save();
            break;
        case "Update":
            Update();
            break;
        case "Delete":
            $("#myModalLabel").empty().append("Delete News");

            $('#myModal').modal({
                backdrop: 'static',
                keyboard: true

            })

            break;
        default:
            alert('Imposible realizar una acción')
            break;
    }
}