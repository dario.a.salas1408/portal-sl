﻿var mForm;
var mRedirectURL;
var Call;


function validateLogin(redirectURL, username, password) {
    $("#error").hide();
    $("#email").val("");
    $("#password").val("");

    $("#loginForm").validate({
        rules: {
            username: "required",
            password: "required"
        },
        messages: {
            username: username,
            password: password
        },
        submitHandler: function (form) {

            mForm = form;

            mRedirectURL = redirectURL;

            $(".panel-body").hide();

            $(".alert.alert-error").hide();

            $(".modal-title").empty().append("Login");

            $('#myModal').modal({
                backdrop: 'static',
                keyboard: true
            })

            $(".close").attr("disabled", "disabled");
            $(".btn.btn-default").attr("disabled", "disabled");
            $("#Login").attr("disabled", "disabled");

            callLogin(form, redirectURL);

        }
    });
}

function callLogin(form, redirectURL) {
    var rememberMe = $('#remember').prop('checked');

 

    Call = $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize() + "&rememberMe=" + rememberMe,
        traditional: true,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data == "ErrorSecurity")
            {
                location.href = "../Error/ErrorAccess?Error=Error to Access, User Corrupt";
            }
            else
            {
                if (data) {

                    $(".form-control.input-lg").empty();
                    $.each(data.Companies, function (key) {

                        if (data.WorkOffline) {
                            data.Companies[key].IdCompany = data.Companies[key].CompanyDB;
                        }

                        $('.form-control.input-lg').append($('<option>', {
                            value: data.Companies[key].IdCompany,
                            text: data.Companies[key].CompanyDB
                        }));
                    });

                    $(".ImageLoading").hide('slow');

                    $(".panel-body").show('slow');

                    $(".modal-title").empty().append(MsgSelComp);

                    $(".close").removeAttr("disabled");
                    $(".btn.btn-default").removeAttr("disabled");
                    $("#Login").removeAttr("disabled");

                }
                else {
                    $(".ImageLoading").hide('slow');

                    $(".alert.alert-error").empty().append("<strong>Usuario y Contraseña incorrecta.</strong>");
                    $(".alert.alert-error").show('slow');

                    $(".close").removeAttr("disabled");
                    $(".btn.btn-default").removeAttr("disabled");

                }
            }

        }, error: function (xhr) {
            alert(xhr.status);

        },
    });

}

function callLoginCompany(form, redirectURL, IdCompany) {

    $(".ImageLoading").show('slow');

    $(".panel-body").hide('slow');

    Call = $.ajax({
        url: "/Login/SigninCompany",
        type: form.method,
        data: "CompanyId=" + IdCompany,
        dataType: 'json',
        success: function (data) {
            if (data.isRedirect == true) {
                location.href = data.redirectUrl;
            }
            else {
                if (data) {
                    location.href = redirectURL;
                }
                else {
                    $(".ImageLoading").hide('slow');

                    $(".alert.alert-error").empty().append("<strong>Error al conectar la Compañia.</strong>");

                    $(".alert.alert-error").show('slow');

                    $(".close").removeAttr("disabled");
                    $(".btn.btn-default").removeAttr("disabled");


                }
            }
        }, error: function (xhr) {
            //alert('error');
        },
    });

}

$(document).ready(function () {

    $("#Login").click(function () {
        callLoginCompany(mForm, mRedirectURL, $(".form-control.input-lg").val());
    });

    $("#CancelLogin").click(function () {
        if (Call && Call.readystate != 4) {
            Call.abort();
        }
    });


});