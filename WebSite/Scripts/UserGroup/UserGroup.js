﻿$(document).ready(function () {
    $("#UpdateCorrectly").hide();

    $("#formUserGroup").validate({
        rules: {
            Name: "required"

        },
        messages: {
            Name: $("#lblName").text() + $("#REQ").text()
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Edit":
                    Update();
                    break;

                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    function Save() {

        $.post("/UserGroup/Add", $.param({
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            CSSName: $("#CSSName").val(),
            JSName: $("#JSName").val(),
            IdCompany: $("#IdCompany").val()
        }))
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/UserGroup/Index?IdCompany=" + $("#IdCompany").val(); }
            else
            {
                Error(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Update() {

        $.post("/UserGroup/Update", $.param({
            Id: $("#Id").val(),
            Name: $("#Name").val(),
            CSSName: $("#CSSName").val(),
            JSName: $("#JSName").val(),
            IdCompany: $("#IdCompany").val()

        }))
        .success(function (data) {
            if (data == "Ok")
            {
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else
            {
                Error(data);
            } 
        })
        .error(function (err) { });
    }

    function Error(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }

});