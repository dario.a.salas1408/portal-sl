﻿$(document).ready(function () {
    if ($("#UseHandheld").is(':checked'))
    {
        $("#rbBarCode").prop('disabled', false);
        $("#rbQRCode").prop('disabled', false);
    }

    if ($("#ServerType").val() != 9)
    {
        $("#UrlHana").prop('disabled', true);
        $("#UrlHanaGetQueryResult").prop('disabled', true);
    }

    $("#UseHandheld").change(function () {
        if($("#UseHandheld").is(':checked'))
        {
            $("#rbBarCode").prop('disabled', false);
            $("#rbQRCode").prop('disabled', false);
        }
        else
        {
            $("#rbBarCode").prop('disabled', true);
            $("#rbQRCode").prop('disabled', true);
            $("#rbBarCode").prop('checked', false);
            $("#rbQRCode").prop('checked', false);
        }
    });

    $("#ServerType").change(function () {
        if ($("#ServerType").val() == 9)
        {
            $("#UrlHana").prop('disabled', false);
            $("#UrlHanaGetQueryResult").prop('disabled', false);
        }
        else
        {
            $("#UrlHana").prop('disabled', true);
            $("#UrlHanaGetQueryResult").prop('disabled', true);
        }

    });

    $("#formcompany").validate({
        rules: {
            Server: "required",
            DbUserName: "required",
            DbPassword: "required",
            CompanyDB: "required",
            UserName: "required",
            Password: "required",
            B1IFID: "required",
            B1IFUser: "required",
            B1IFPassword: "required",
            LicenseServer: "required",
            PortNumber: "required",
            SAPLanguaje: "required"
        },
        messages: {
            Server: $("#SQLS").text() + $("#REQ").text(),
            DbUserName: $("#SQLAUN").text() + $("#REQ").text(),
            DbPassword: $("#SQLAUP").text() + $("#REQ").text(),
            CompanyDB: $("#CDB").text() + $("#REQ").text(),
            UserName: $("#SUN").text() + $("#REQ").text(),
            Password: $("#SUP").text() + $("#REQ").text(),
            B1IFID: $("#B1IFIDLabel").text() + $("#REQ").text(),
            B1IFUser: $("#B1IFUserLabel").text() + $("#REQ").text(),
            B1IFPassword: $("#B1IFPasswordLabel").text() + $("#REQ").text(),
            LicenseServer: $("#LS").text() + $("#REQ").text(),
            PortNumber: $("#LS").text() + $("#REQ").text(),
            SAPLanguaje: $("#SAPLanguajeLabel").text() + $("#REQ").text()
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                case "Delete":

                    $("#myModalLabel").empty().append("Delete Company");

                    $('#myModal').modal({
                        backdrop: 'static',
                        keyboard: true

                    })

                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });


    function Save() {

        $.post("/Companies/Add", {
            IdCompany: $("#IdCompany").val(),
            ServerType: $("#ServerType").val(),
            PortNumber: $("#PortNumber").val(),
            LicenseServer: $("#LicenseServer").val(),
            Server: $("#Server").val(),
            CompanyDB: $("#CompanyDB").val(),
            UserName: $("#UserName").val(),
            Password: $("#Password").val(),
            DbUserName: $("#DbUserName").val(),
            DbPassword: $("#DbPassword").val(),
            UseTrusted: $("#UseTrusted").is(':checked'),
            UseCompanyUser: $("#UseCompanyUser").is(':checked'),
            Active: $("#Active").is(':checked'),
            UseHandheld: $("#UseHandheld").is(':checked'),
            CodeType: ($("#rbBarCode").is(':checked') == true ? "1" : ($("#rbQRCode").is(':checked') == true ? "2" : null)),
            CheckBasketID: $("#CheckBasketID").is(':checked'),
            OnDemand: $("#OnDemand").is(':checked'),
            ItemInMultipleLines: $("#ItemInMultipleLines").is(':checked'),
            B1IFID: $("#B1IFID").val(),
            B1IFUser: $("#B1IFUser").val(),
            B1IFPassword: $("#B1IFPassword").val(),
            SAPLanguaje: $("#SAPLanguaje").val(),
            UrlHana: $("#UrlHana").val(),
            UrlHanaGetQueryResult: $("#UrlHanaGetQueryResult").val(),
            UrlSL: $("#UrlSL").val(),
            VisualOrder: $("#VisualOrder").val()

        })
        .success(function (data) {
            if (data == "Ok") {
                if (formUser == 0)
                { location.href = "/Companies/Index"; }
                else
                { location.href = "/Users/CompaniesUser?IdUser=" + formUser; }
            }
            else {
                ErrorCo(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Update() {

        $.post("/Companies/Update", {
            IdCompany: $("#IdCompany").val(),
            ServerType: $("#ServerType").val(),
            PortNumber: $("#PortNumber").val(),
            LicenseServer: $("#LicenseServer").val(),
            Server: $("#Server").val(),
            CompanyDB: $("#CompanyDB").val(),
            UserName: $("#UserName").val(),
            Password: $("#Password").val(),
            DbUserName: $("#DbUserName").val(),
            DbPassword: $("#DbPassword").val(),
            UseTrusted: $("#UseTrusted").is(':checked'),
            UseCompanyUser: $("#UseCompanyUser").is(':checked'),
            Active: $("#Active").is(':checked'),
            UseHandheld: $("#UseHandheld").is(':checked'),
            CodeType: ($("#rbBarCode").is(':checked') == true ? "1" : ($("#rbQRCode").is(':checked') == true ? "2" : null)),
            CheckBasketID: $("#CheckBasketID").is(':checked'),
            OnDemand: $("#OnDemand").is(':checked'),
            ItemInMultipleLines: $("#ItemInMultipleLines").is(':checked'),
            B1IFID: $("#B1IFID").val(),
            B1IFUser: $("#B1IFUser").val(),
            B1IFPassword: $("#B1IFPassword").val(),
            SAPLanguaje: $("#SAPLanguaje").val(),
            UrlHana: $("#UrlHana").val(),
            UrlHanaGetQueryResult: $("#UrlHanaGetQueryResult").val(),
            UrlSL: $("#UrlSL").val(),
            VisualOrder: $("#VisualOrder").val()

        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/Companies/Index"; }
            else {
                ErrorCo(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function Delete() {

        $.post("/Companies/Delete", {
            IdCompany: $("#IdCompany").val()
        })
        .success(function (data) {
            if (data == "Ok")
            { location.href = "/Companies/Index"; }
            else {
                ErrorCo(data);
            }
        })
        .error(function (err) { Close(); });
    }

    function ErrorCo(data) {
        $("#errorMessage").empty().append("<strong>" + data + "</strong>");
        $("#errorMessage").show('slow');
        $('#errorBox').modal({
            backdrop: 'static',
            keyboard: true
        });
    }

    $("#Ok").click(function () {
        Delete();
    });


});


function ValidateSQLConnection() {
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    $.post("/Companies/ValidateSQLConnection", {
        pServer: $("#Server").val(),
        pUser: $("#DbUserName").val(),
        pPassword: $("#DbPassword").val(),
        pCompanyDB: $("#CompanyDB").val()
        
    })
   .success(function (data) {
       if (data == "Ok") {
           $('#Loading').modal('hide');
           $("#errorMessage").empty().append("<strong>  Connection validation: Correct </strong>");
           $("#errorMessage").show('slow');
           $('#errorBox').modal({
               backdrop: 'static',
               keyboard: true
           });
       }
       else {
           $('#Loading').modal('hide');
           $("#errorMessage").empty().append("<strong>  Connection validation: Failed </strong>");
           $("#errorMessage").show('slow');
           $('#errorBox').modal({
               backdrop: 'static',
               keyboard: true
           });
       }
   })
   .error(function (err) { Close(); });
}


function ValidateSAPConnection(IdCompany, IdUser) {
    $('#Loading').modal({
        backdrop: 'static',
        keyboard: true
    });

    $.post("/Companies/ValidateSAPConnection", {
        IdCompany: $("#IdCompany").val(),
        ServerType: $("#ServerType").val(),
        PortNumber: $("#PortNumber").val(),
        LicenseServer: $("#LicenseServer").val(),
        Server: $("#Server").val(),
        CompanyDB: $("#CompanyDB").val(),
        UserName: $("#UserName").val(),
        Password: $("#Password").val(),
        DbUserName: $("#DbUserName").val(),
        DbPassword: $("#DbPassword").val(),
        UseTrusted: $("#UseTrusted").is(':checked'),
        UseCompanyUser: $("#UseCompanyUser").is(':checked'),
        Active: $("#Active").is(':checked'),
        UseHandheld: $("#UseHandheld").is(':checked'),
        CodeType: ($("#rbBarCode").is(':checked') == true ? "1" : ($("#rbQRCode").is(':checked') == true ? "2" : null)),
        CheckBasketID: $("#CheckBasketID").is(':checked'),
        OnDemand: $("#OnDemand").is(':checked'),
        ItemInMultipleLines: $("#ItemInMultipleLines").is(':checked'),
        UrlHana: $("#UrlHana").val(),
        UrlHanaGetQueryResult: $("#UrlHanaGetQueryResult").val(),
        VisualOrder: $("#VisualOrder").val()
    })
   .success(function (data) {
       if (data == "Ok") {
           $('#Loading').modal('hide');
           $("#infoMessage").empty().append("<strong> Connection validation: Correct </strong>");
           $("#infoMessage").show('slow');
           $('#infoBox').modal({
               backdrop: 'static',
               keyboard: true
           });
       }
       else {
           $('#Loading').modal('hide');
           $("#errorMessage").empty().append("<strong>  Connection validation: Failed </strong>");
           $("#errorMessage").show('slow');
           $('#errorBox').modal({
               backdrop: 'static',
               keyboard: true
           });
       }
   })
   .error(function (err) { Close(); });
}