﻿var idToDelete;
var IdNew = 0;
$(document).ready(function () {
    $("#UpdateCorrectly").hide();

    var validator = $("#formCRPageMap").validate({
        submitHandler: function (form) {
            AddUpdate();
        }
    });

    $("#Ok").click(function () {
        DeleteCRPageMap();
    });

    $("#btNew").click(function () {
        var mListPages = [];
        $("#TableItemsForm :input[type=hidden]").each(function () {
            var input = $(this);
            mListPages.push(input.attr('id'));
        });

        $.ajax({
            url: '/Companies/_PagesToAdd',
            contextType: 'application/html;charset=utf-8',
            traditional: true,
            data: { pPagesAdded: mListPages },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#Load").hide();
            $("#ModalBody").html(data);
        });

    });

    $("#btnChoose").click(function () {
        var mListItems = [];
        var mListId = [];
        $("#TableItems").find("input:checked").each(function (i, ob) {
            mListItems.push($(ob).val());
            IdNew = IdNew + 1;
            mListId.push(IdNew);
        });


        $.ajax({
            url: '/Companies/_AddPageToList',
            contextType: 'application/html;charset=utf-8',
            traditional: true,
            data: { pPagesId: mListItems, IdCompany: $("#IdCompany").val(), pIdNews: mListId },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {
            $("#Load").hide();
            $("#TableItemsForm").html(data);
            $('#myModalAdd').modal('hide');
        });

    });

    $("#btnOk").click(function () {

        var form = $("#formCRPageMap");
        form.validate();
        if (form.valid()) {
            setTimeout(function () {
                form.submit();
            }, 1);
        }
    });

});

function DeleteCRPageMap() {
    $(".modal-backdrop").show();
    $('#Loading').modal('show');

    $.ajax({
        url: '/Companies/_DeleteCRPageMap',
        contextType: 'application/html;charset=utf-8',
        traditional: true,
        data: { pFromDb: idToDelete.split('-')[0], pId: idToDelete.split('-')[1] },
        type: 'POST',
        dataType: 'html'
    }).done(function (data) {
        $("#rowNum-" + idToDelete).remove();
        $('#Loading').modal('hide');
        $('#myModal').modal('hide');
        $(".modal-backdrop").hide();

        //location.href = "/Companies/CRPageMap?IdCompany=" + $("#IdCompany").val();

    });
}

function ShowDeleteMessage(id, message) {
    $(".modal-backdrop").show();
    $('#myModal').show();
    $("#myModalLabel").empty().append(message);
    $('#myModal').modal();
    idToDelete = id;
}



function AddUpdate() {
    $('#Loading').modal('show');
    var mCRPageList = [];
    $("#TableItemsForm :input[type=hidden]").each(function () {
        var input = $(this);

        var mIdPage = input.attr('id').split('-')[0];
        var mFromDb = input.attr('id').split('-')[1];
        var mIdCR = 0;
        var mIdNew = 0;

        if (mFromDb == "S") {
            mIdCR = input.attr('id').split('-')[2];
        }
        else
        {
            mIdNew = input.attr('id').split('-')[2];
        }
        
        mCRPageList.push({
            "CRName": (mFromDb == "S" ? $("#CRName-" + mFromDb + "-" + mIdCR).val() : $("#CRName-" + mFromDb + "-" + mIdNew).val()),
            "IdCompany": $("#IdCompany").val(),
            "IdPage": mIdPage,
            "IdICRPageMap": mIdCR
        });
    });

    $.ajax({
        url: "/Companies/_UpdateCRPageMap",
        async: false,
        type: "POST",
        data: (JSON.stringify(mCRPageList)),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
                setTimeout(function () { location.href = "/Companies/CRPageMap?IdCompany=" + $("#IdCompany").val(); }, 1100);
            }
            else {
                ErrorPO(data);
            }
        }
    });
}