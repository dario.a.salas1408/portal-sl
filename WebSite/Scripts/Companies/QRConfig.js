﻿var idToDelete;
var IdNew = 0;
$(document).ready(function () {
    $("#UpdateCorrectly").hide();

    var validator = $("#formQRConfig").validate({
        submitHandler: function (form) {
            switch (formMode) {
                case "Update":
                    Update();
                    break;
                case "Add":
                    Add();
                    break;
            }
        }
    });

    $("#btnOk").click(function () {
        var form = $("#formQRConfig");
        form.validate();
        if (form.valid()) {
            setTimeout(function () {
                form.submit();
            }, 1);
        }
    });

});

function Add() {
    $('#Loading').modal('show');

    var QRConfigView = {
        IdCompany: $("#IdCompany").val(),
        IdQRConfig: $("#DocDueDate").val(),
        Separator: $("#Separator").val(),
        ItemCodePosition: $("#ItemCodePosition").val(),
        DscriptionPosition: $("#DscriptionPosition").val(),
        WhsCodePosition: $("#WhsCodePosition").val(),
        OcrCodePosition: $("#OcrCodePosition").val(),
        GLAccountPosition: $("#GLAccountPosition").val(),
        FreeTxtPosition: $("#FreeTxtPosition").val(),
        QuantityPosition: $('#QuantityPosition').val(),
        CurrencyPosition: $('#CurrencyPosition').val(),
        PricePosition: $('#PricePosition').val(),
        UomCodePosition: $('#UomCodePosition').val(),
        SerialPosition: $('#SerialPosition').val(),
        BatchPosition: $('#BatchPosition').val()
    };

    $.ajax({
        url: "/Companies/_AddQRConfig",
        async: false,
        type: "POST",
        data: (JSON.stringify(QRConfigView)),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
                setTimeout(function () { location.href = "/Companies/QRConfig?IdCompany=" + $("#IdCompany").val(); }, 1100);
            }
            else {
                ErrorPO(data);
            }
        }
    });
}

function Update() {
    $('#Loading').modal('show');

    var QRConfigView = {
        IdCompany: $("#IdCompany").val(),
        IdQRConfig: $("#DocDueDate").val(),
        Separator: $("#Separator").val(),
        ItemCodePosition: $("#ItemCodePosition").val(),
        DscriptionPosition: $("#DscriptionPosition").val(),
        WhsCodePosition: $("#WhsCodePosition").val(),
        OcrCodePosition: $("#OcrCodePosition").val(),
        GLAccountPosition: $("#GLAccountPosition").val(),
        FreeTxtPosition: $("#FreeTxtPosition").val(),
        QuantityPosition: $('#QuantityPosition').val(),
        CurrencyPosition: $('#CurrencyPosition').val(),
        PricePosition: $('#PricePosition').val(),
        UomCodePosition: $('#UomCodePosition').val(),
        SerialPosition: $('#SerialPosition').val(),
        BatchPosition: $('#BatchPosition').val()
    };

    $.ajax({
        url: "/Companies/_UpdateQRConfig",
        async: false,
        type: "POST",
        data: (JSON.stringify(QRConfigView)),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            if (data == "Ok") {
                $('#Loading').modal('hide');
                $("#UpdateCorrectly").show("slow");
                $("#UpdateCorrectly").delay(1500).slideUp(1000);
            }
            else {
                ErrorPO(data);
            }
        }
    });
}