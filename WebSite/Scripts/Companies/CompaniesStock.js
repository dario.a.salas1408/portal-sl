﻿$(document).ready(function () {
    var result, last;

    $("#AddMas").click(function () {

        if ($("#Formula").val() != "") {
            result = $("#Formula").val();

            last = result.substr(result.length - 1);

            if (last != "+" && last != "-" && last != "/" && last != "*")
            { $("#Formula").val($("#Formula").val() + " " + "+"); }

        }
    });

    $("#AddMenos").click(function () {
        if ($("#Formula").val() != "") {
            result = $("#Formula").val();

            last = result.substr(result.length - 1);

            if (last != "+" && last != "-" && last != "/" && last != "*") {
                $("#Formula").val($("#Formula").val() + " " + "-");
            }
        }
    });

    $("#AddPor").click(function () {
        if ($("#Formula").val() != "") {
            result = $("#Formula").val();

            last = result.substr(result.length - 1);

            if (last != "+" && last != "-" && last != "/" && last != "*") {
                $("#Formula").val($("#Formula").val() + " " + "*");
            }
        }
    });

    $("#AddDividido").click(function () {
        if ($("#Formula").val() != "") {
            result = $("#Formula").val();

            last = result.substr(result.length - 1);
            if (last != "+" && last != "-" && last != "/" && last != "*") {
                $("#Formula").val($("#Formula").val() + " " + "/");
            }
        }
    });

    $("#AddFormula").click(function () {

        result = $("#Formula").val();

        last = result.substr(result.length - 1);

        if ($("#Formula").val() != "") {

            if (last == "+" || last == "-" || last == "/" || last == "*") {
                $("#Formula").val($("#Formula").val() + " " + $("#cbFormula").val());
            }
        } else {
            $("#Formula").val($("#Formula").val() + " " + $("#cbFormula").val());
        }

    });

    $("#Clear").click(function () {
        $("#Formula").val("");
    });

    $("#formcompanystock").validate({
        rules: {
            Formula: "required"

        },
        messages: {
            Formula: "The Formula is required"
        },
        submitHandler: function (form) {
            switch (formMode) {
                case "Add":
                    Save();
                    break;
                case "Update":
                    Update();
                    break;
                default:
                    alert('Imposible realizar una acción')
                    break;
            }
        }
    });

    if ($("#WhAll").is(':checked')) {
        var mListWarehouseCk = $("input[id^=WhCk_]");
        for (i = 0; i < mListWarehouseCk.length; i++) {
            $("#" + mListWarehouseCk[i].id).attr('disabled', 'disabled');
        }
    }

    $("#WhAll").change(function () {
        if ($('#WhAll').is(':checked')) {
            var mListWarehouseCk = $("input[id^=WhCk_]");
            for (i = 0; i < mListWarehouseCk.length; i++) {
                $("#" + mListWarehouseCk[i].id).attr('disabled', 'disabled');
            }
        }
        else {
            var mListWarehouseCk = $("input[id^=WhCk_]");
            for (i = 0; i < mListWarehouseCk.length; i++) {
                $("#" + mListWarehouseCk[i].id).removeAttr('disabled');
            }
        }
    });

});

function Save() {
    var mWarehousePortalList = [];
    if ($("#WhAll").is(':checked') == false) {
        var mListWarehouseCk = $("input[id^=WhCk_]");
        for (i = 0; i < mListWarehouseCk.length; i++) {
            if ($("#" + mListWarehouseCk[i].id).is(':checked')) {
                mWarehousePortalList.push({
                    IdCompany: $("#IdCompany").val(),
                    WhsCode: mListWarehouseCk[i].id.substring(5),
                });
            }
        }
    }

    $.post("/Companies/AddStock", {
        IdCompany: $("#IdCompany").val(),
        Formula: $("#Formula").val(),
        ValidateStock: $("#ValidateStock").is(':checked'),
        IdCompanyStock: $("#IdCompanyStock").val(),
        WarehousePortalList: mWarehousePortalList
    })
        .success(function (data) {
            if (data == "Ok") {
                location.href = "/Companies/Index";
            }
            else {
                ErrorCo(data);
            }
        })
        .error(function (err) { Close(); });
}

function Update() {
    var mWarehousePortalList = [];
    if ($("#WhAll").is(':checked') == false) {
        var mListWarehouseCk = $("input[id^=WhCk_]");
        for (i = 0; i < mListWarehouseCk.length; i++) {
            if ($("#" + mListWarehouseCk[i].id).is(':checked')) {
                mWarehousePortalList.push({
                    IdCompany: $("#IdCompany").val(),
                    WhsCode: mListWarehouseCk[i].id.substring(5),
                });
            }
        }
    }

    $.post("/Companies/Updatetock", {
        IdCompany: $("#IdCompany").val(),
        Formula: $("#Formula").val(),
        ValidateStock: $("#ValidateStock").is(':checked'),
        IdCompanyStock: $("#IdCompanyStock").val(),
        WarehousePortalList: mWarehousePortalList
    })
        .success(function (data) {
            if (data == "Ok") {
                location.href = "/Companies/Index";
            }
            else {
                ErrorCo(data);
            }
        })
        .error(function (err) { Close(); });
}