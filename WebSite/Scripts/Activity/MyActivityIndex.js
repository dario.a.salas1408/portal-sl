﻿$(document).ready(function () {

    $("#btnSearch").click(function () {

        $('#Loading').modal({
            backdrop: 'static',
            keyboard: true
        });

        $.ajax({
            url: '/PLM/Activity/MyActivityList',
            contextType: 'application/html;charset=utf-8',
            data: { UserType: $("#UserType").val(), txtModel: $("#txtModel").val(), txtProject: $("#txtProject").val(), txtStatus: $("#txtStatus").val(), txtRemarks: $("#txtRemarks").val(), },
            type: 'POST',
            dataType: 'html'
        }).done(function (data) {

            $("#BodyActivity").html(data);
            $('#Loading').modal('hide');

        }).fail(function () {
            alert("error");
        });

    });
    if ($("#txtModel").val() != "" || $("#txtProject").val() != "")
    {
        $("#btnSearch").click();
    }

    

});