﻿var mActualPage = 1;
var mSelectedQuantity = 10;

function GetTableDataLength(pArrayQuantities, pSelectedQuantity) {
    var mSelect = "<select name=\"TableData_length\" aria-controls=\"TableData\" class=\"\">";

    for (i = 0; i < pArrayQuantities.length; i++)
    {
        if (pArrayQuantities[i] != pSelectedQuantity)
            mSelect += "<option value=\"" + pArrayQuantities[i] + "\">" + pArrayQuantities[i] + "</option>";
        else
            mSelect += "<option value=\"" + pArrayQuantities[i] + "\" selected>" + pArrayQuantities[i] + "</option>";
    }
    mSelect += "</select>";

    var mReturnDiv = "<div class=\"dataTables_length\" id=\"TableData_length\"><label>Show "+mSelect+" entries</label></div>";
    return mReturnDiv;
}


function GetTableDataInfo(pActualPage, pSelectedQuantity, pTotalEntries) {
    var mStartEntry = ((pActualPage - 1) * pSelectedQuantity) + 1;

    var mEndEntry;
    if ((pActualPage * pSelectedQuantity) <= pTotalEntries)
        mEndEntry = (pActualPage * pSelectedQuantity);
    else
        mEndEntry = mStartEntry + (pTotalEntries - mStartEntry);

    var mReturnDiv = "<div class=\"dataTables_info\" id=\"TableData_info\" role=\"status\" aria-live=\"polite\">Showing " + mStartEntry + " to " + mEndEntry + " of " + pTotalEntries + " entries</div>";
    return mReturnDiv;
}

function GetTableDataPages(pActualPage, pSelectedQuantity, pTotalEntries, btnToClick) {
    var mPreviousPage = pActualPage;
    var mPreviousClass = "disabled";
    if (pActualPage > 1)
    {
        mPreviousPage = pActualPage - 1;
        mPreviousClass = "";
    }
    var mReturnDiv = "<div class=\"dataTables_paginate paging_simple_numbers\" id=\"TableData_paginate\"><a class=\"paginate_button previous " + mPreviousClass + "\" aria-controls=\"TableData\" id=\"TableData_previous\" onclick=\"ChangeDataTablePage(" + mPreviousPage + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">Previous</a><span>";
    var mTotalPages = Math.ceil(pTotalEntries / pSelectedQuantity);
    var mNextPage = mTotalPages;
    var mNextClass = "disabled";

    //<8 mostrar todo del 1 al 7
        //controlar si estoy en la pagina 1
    if(mTotalPages < 8)
    {
        mReturnDiv += "<span>";
        for(var i = 1; i<= mTotalPages; i++)
        {
            mReturnDiv += "<a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(" + i + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">" + i + "</a>";
        }
        
    //    <span>…</span>
    //    <a class="paginate_button " aria-controls="TableData" data-dt-idx="6" tabindex="0">66</a>
    //</span>
    }
    if(mTotalPages > 8)
    {
        mReturnDiv += "<span>";
        if(pActualPage < 5)
        {
            for(var i = 1; i<= 5; i++)
            {
                mReturnDiv += "<a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(" + i + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">" + i + "</a>";
            }
            mReturnDiv += "<span>…</span><a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(" + mTotalPages + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">" + mTotalPages + "</a>";
        }
        else
        {
            if (pActualPage >= 5 && ((mTotalPages - 3) > pActualPage))
            {
                mReturnDiv += "<a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(1, " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">1</a><span>…</span>";
                for(var i = (pActualPage-1); i<= (pActualPage+1); i++)
                {
                    mReturnDiv += "<a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(" + i + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">" + i + "</a>";
                }
                mReturnDiv += "<span>…</span><a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(" + mTotalPages + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">" + mTotalPages + "</a>";
            }
            else
            {
                mReturnDiv += "<a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(1, " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">1</a><span>…</span>";
                for(var i = (pActualPage-1); i<= mTotalPages; i++)
                {
                    mReturnDiv += "<a class=\"paginate_button\" aria-controls=\"TableData\" id=\"TableDataPage" + i + "\" onclick=\"ChangeDataTablePage(" + i + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">" + i + "</a>";
                }
            }
        }
        
    }
    if (mTotalPages > pActualPage)
    {
        mNextClass = "";
        mNextPage = pActualPage + 1;
    }
    mReturnDiv += "</span><a class=\"paginate_button next " + mNextClass + "\" aria-controls=\"TableData\" id=\"TableData_next\" onclick=\"ChangeDataTablePage(" + mNextPage + ", " + pSelectedQuantity + ", " + pTotalEntries + ", " + btnToClick + ")\">Next</a></div>";

    return mReturnDiv;
}

function SetActualPage(pActualPage)
{
    $("#TableDataPage" + pActualPage).addClass("current");
    mActualPage = pActualPage;
}

function ChangeDataTablePage(pActualPage, pSelectedQuantity, pTotalEntries, btnToClick)
{
    $("#TableData_info").replaceWith(GetTableDataInfo(pActualPage, pSelectedQuantity, pTotalEntries));
    $("#TableData_paginate").replaceWith(GetTableDataPages(pActualPage, pSelectedQuantity, pTotalEntries));
    SetActualPage(pActualPage);
    mSelectedQuantity = pSelectedQuantity;
    $("#" + btnToClick).click();
    //LLamar a boton click del search
}