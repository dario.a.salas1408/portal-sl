﻿using ARGNS.ManagerView;
using ARGNS.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARGNS.WebSite.Resources.Views.Companies;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Attribute;
using ARGNS.Util;
using AutoMapper;
using ARGNS.Model.Implementations;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class CompaniesController : Controller
    {

        private CompaniesManagerView mCompanyManagerView = new CompaniesManagerView();
        private CompaniesStockManagerView mCompaniesStockManagerView = new CompaniesStockManagerView();
        private CRPageMapManagerView mCRPageMapManagerView = new CRPageMapManagerView();
        private WarehouseManagerView mWarehouseManagerView = new WarehouseManagerView();

        /// <summary>
        /// 
        /// </summary>
        public CompaniesController()
        {
            Mapper.CreateMap<CompanyConn, CompanyView>();
            Mapper.CreateMap<CompanyView, CompanyConn>();
            Mapper.CreateMap<Page, PageView>();
            Mapper.CreateMap<PageView, Page>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.WEBPORTAL, Enums.Pages.ABMCompanies)]
        public ActionResult Index()
        {
            try
            {
                //return View(mCompanyManagerView.GetCompaniesByUser((UserView)Session["UserLogin"]));
                return View(mCompanyManagerView.GetCompanies());

            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionCompany"></param>
        /// <param name="IdCompany"></param>
        /// <param name="FromUser"></param>
        /// <returns></returns>
        public ActionResult ActionCompany(string ActionCompany, int IdCompany, int FromUser)
        {
            try
            {
                CompanyView mCompanyView = null;

                switch (ActionCompany)
                {
                    case "Add":
                        ViewBag.Tite = Companies.AC;
                        ViewBag.FormMode = ActionCompany;
                        mCompanyView = new CompanyView();
                        mCompanyView.Active = true;
                        break;

                    case "Update":
                        ViewBag.Tite = Companies.UC;
                        ViewBag.FormMode = ActionCompany;
                        mCompanyView = mCompanyManagerView.GetCompany(IdCompany);
                        break;

                    case "Delete":
                        ViewBag.Tite = Companies.DC;
                        ViewBag.FormMode = ActionCompany;
                        mCompanyView = mCompanyManagerView.GetCompany(IdCompany);
                        break;

                }

                ViewBag.ForUser = FromUser;

                return View("_Company", mCompanyView);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(CompanyView model)
        {
            if (mCompanyManagerView.Add(model, (UserView)Session["UserLogin"]))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(CompanyView model)
        {
            if (mCompanyManagerView.Update(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(CompanyView model)
        {
            if (mCompanyManagerView.Delete(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pServer"></param>
        /// <param name="pUser"></param>
        /// <param name="pPassword"></param>
        /// <param name="pCompanyDB"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ValidateSQLConnection(string pServer, string pUser, string pPassword, string pCompanyDB)
        {
            if (mCompanyManagerView.ValidateSQLConnection(pServer, pUser, pPassword, pCompanyDB))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ValidateSAPConnection(CompanyView model)
        {
            if (mCompanyManagerView.ValidateSAPConnection(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public ActionResult CompanyStock(int IdCompany)
        {
            try
            {
                CompanyStockView mCompanyStockView = null;

                mCompanyStockView = mCompaniesStockManagerView.GetCompanyStock(IdCompany);
                mCompanyStockView.WarehousePortalList = mCompaniesStockManagerView.GetWarehousePortalList(IdCompany);
                mCompanyStockView.WarehouseList = mWarehouseManagerView.GetListWarehouse(mCompanyManagerView.GetCompany(IdCompany));
                
                if (mCompanyStockView.ModeAdd)
                {
                    ViewBag.FormMode = "Add";
                }
                else
                {
                    ViewBag.FormMode = "Update";
                }

                ViewBag.Tite = "Stock";

                return View("_CompanyStock", mCompanyStockView);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }

        }

        #region Crystal Report Page Map

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public ActionResult CRPageMap(int IdCompany)
        {
            try
            {
                List<CRPageMapView> mCRPageMapView = mCRPageMapManagerView.GetCRPageMap(IdCompany);
                CompanyView mCompany = mCompanyManagerView.GetCompany(IdCompany);
                ViewBag.Tite = "Crystal Report Page Map";
                TempData["CRPageMapViewTemp"] = mCRPageMapView;
                return View("_CRPageMap", Tuple.Create(mCRPageMapView, mCompany));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPagesAdded"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _PagesToAdd(string[] pPagesAdded)
        {
            try
            {
                List<PageView> mPages = mCRPageMapManagerView.getPagesToAdd(pPagesAdded);
                TempData["PagesListTemp"] = mPages;
                return PartialView("_PagesToAdd", mPages);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPagesId"></param>
        /// <param name="IdCompany"></param>
        /// <param name="pIdNews"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult _AddPageToList(string[] pPagesId, int IdCompany, int[] pIdNews)
        {
            try
            {
                List<PageView> mPagesList = (List<PageView>)TempData["PagesListTemp"];
                List<CRPageMapView> mCRPageMapList = (List<CRPageMapView>)TempData["CRPageMapViewTemp"];
                int i = 0;
                foreach (PageView pageToAdd in mPagesList.Where(c => pPagesId.Contains(c.IdPage.ToString())).ToList())
                {
                    CRPageMapView mCRPMV = new CRPageMapView();
                    mCRPMV.Page = Mapper.Map<Page>(pageToAdd);
                    mCRPMV.IdPage = pageToAdd.IdPage;
                    mCRPMV.IdCompany = IdCompany;
                    mCRPMV.FromDB = "N";
                    mCRPMV.IdICRPageMap = pIdNews[i];
                    mCRPageMapList.Add(mCRPMV);
                    i++;
                }
                TempData["CRPageMapViewTemp"] = mCRPageMapList;
                TempData["PagesListTemp"] = mPagesList;

                return PartialView("_CRPageMapTable", mCRPageMapList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listCRPageMap"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateCRPageMap(List<CRPageMapView> listCRPageMap)
        {
            int IdCompany = listCRPageMap.FirstOrDefault().IdCompany;
            CompanyConn mCompany = Mapper.Map<CompanyConn>(mCompanyManagerView.GetCompany(IdCompany));
            listCRPageMap.Select(c => { c.CompanyConn = mCompany; return c; }).ToList();
            if (mCRPageMapManagerView.Update(listCRPageMap, IdCompany))
            {
                List<PageView> mPages = mCRPageMapManagerView.getPagesToAdd(null);
                listCRPageMap.Select(c => { c.Page = Mapper.Map<Page>(mPages.Where(j => j.IdPage == c.IdPage).FirstOrDefault()); return c; }).ToList();
                TempData["CRPageMapViewTemp"] = listCRPageMap;
                return Json("Ok");
            }
            else
            { return Json(@Error.WebServError); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pFromDb"></param>
        /// <param name="pId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _DeleteCRPageMap(string pFromDb, int pId)
        {
            if (pFromDb == "S")
            {
                if (!mCRPageMapManagerView.Delete(pId))
                {
                    return Json(@Error.WebServError);
                }
            }

            List<CRPageMapView> mCRPageMapList = (List<CRPageMapView>)TempData["CRPageMapViewTemp"];
            mCRPageMapList.Remove(mCRPageMapList.Where(c => c.IdICRPageMap == pId && c.FromDB == pFromDb).FirstOrDefault());
            TempData["CRPageMapViewTemp"] = mCRPageMapList;
            return Json("Ok");

        }

        #endregion

        #region QR Code

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public ActionResult QRConfig(int IdCompany)
        {
            try
            {
                QRConfigView mQRConfigView = mCompanyManagerView.GetQRConfig(IdCompany);
                ViewBag.FormMode = "Update";
                if (mQRConfigView == null) { 
                    mQRConfigView = new QRConfigView();
                    ViewBag.FormMode = "Add";
                }
                CompanyView mCompany = mCompanyManagerView.GetCompany(IdCompany);
                ViewBag.Tite = "QR Code Configuration";
                return View("_QRConfig", Tuple.Create(mQRConfigView, mCompany));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _AddQRConfig(QRConfigView model)
        {
            if (mCompanyManagerView.AddQRConfig(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _UpdateQRConfig(QRConfigView model)
        {
            if (mCompanyManagerView.UpdateQRConfig(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddStock(CompanyStockView model)
        {
            if (mCompaniesStockManagerView.Add(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Updatetock(CompanyStockView model)
        {
            if (mCompaniesStockManagerView.Update(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

    }
}