﻿using ARGNS.ManagerView;
using ARGNS.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARGNS.WebSite.Resources.Views.News;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Attribute;
using ARGNS.Util;
using System.Web.Script.Serialization;
using System.IO;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class NewsController : Controller
    {
        private NewManagerView mNewManagerView = new NewManagerView();
        //
        // GET: /News/

        [CustomerAuthorize(Enums.Areas.WEBPORTAL, Enums.Pages.ABMNews)]
        public ActionResult Index()
        {
            return View(mNewManagerView.GetNews());
        }

        [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
        public ActionResult ActionNews(string ActionNews, int IdNews)
        {
            NewView mNewView = new NewView();

            switch (ActionNews)
            {
                case "Add":
                    ViewBag.Tite = News.AN;
                    ViewBag.FormMode = ActionNews;
                    break;

                case "Update":
                    ViewBag.Tite = News.UN;
                    ViewBag.FormMode = ActionNews;
                    mNewView = mNewManagerView.GetNew(IdNews);
                    break;

                case "Delete":
                    ViewBag.Tite = News.DN;
                    ViewBag.FormMode = ActionNews;
                    mNewView = mNewManagerView.GetNew(IdNews);
                    break;

            }

            return View("_News", mNewView);
        }

        [HttpPost]
        public JsonResult Add(FormCollection pForm)
        {
            try
            { 
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                NewView model = Newtonsoft.Json.JsonConvert.DeserializeObject<NewView>(pForm["pNew"]);

                string mFullPath = string.Empty;
                string mFileName = string.Empty;
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;

                    HttpPostedFileBase mFilePost = mFiles[0];

                    mFileName = mFilePost.FileName;

                    if (!Directory.Exists(Server.MapPath("~/images")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/images"));
                    }

                    mFullPath = Server.MapPath("~/images/" + mFileName);

                    mFilePost.SaveAs(mFullPath);
                }
                model.ImageName = mFileName;

                if (mNewManagerView.Add(model))
                { return Json("Ok"); }
                else
                { return Json(@Error.WebServError); }
            }
            catch(Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        [HttpPost]
        public JsonResult Update(FormCollection pForm)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                NewView model = Newtonsoft.Json.JsonConvert.DeserializeObject<NewView>(pForm["pNew"]);

                string mFullPath = string.Empty;
                string mFileName = string.Empty;
                if (Request.Files.Count > 0)
                {
                    HttpFileCollectionBase mFiles = Request.Files;

                    HttpPostedFileBase mFilePost = mFiles[0];

                    mFileName = mFilePost.FileName;

                    if (!Directory.Exists(Server.MapPath("~/images")))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/images"));
                    }

                    mFullPath = Server.MapPath("~/images/" + mFileName);

                    mFilePost.SaveAs(mFullPath);
                }
                if(model.ImgUploadChange)
                    model.ImageName = mFileName;

                if (mNewManagerView.Update(model))
                { return Json("Ok"); }
                else
                { return Json(@Error.WebServError); }
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }

        }

        [HttpPost]
        public JsonResult Delete(NewView model)
        {
            if (mNewManagerView.Delete(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        public JsonResult DeleteDragAndDropImg()
        {
            return Json("Ok");
        }

    }
}