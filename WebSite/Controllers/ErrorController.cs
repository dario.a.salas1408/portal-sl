﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        public ActionResult Index(string Exception)
        {
            return View();
        }

        public ActionResult ErrorAccess(string Error)
        {
            @TempData["ErrorAccess"] = Error;

            return View("ErrorAccess");
        }
    }
}