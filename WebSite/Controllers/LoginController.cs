﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.App_Start;
using Microsoft.Owin.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;

namespace ARGNS.WebSite.Controllers
{
    public class LoginController : Controller
    {
        private UserManagerView mUserManagerView = new UserManagerView();
        private CompaniesManagerView mCompaniesManager = new CompaniesManagerView();
        private AreaKeyManagerView mAreaKeyManagerView;
        private InfoPortalManagerView mInfoPortalManagerView;
        private UserGroupManagerView mUserGroupManagerView;
        private UserBranchManagerView userBranchManagerView;

        public LoginController()
        {
            mAreaKeyManagerView = new AreaKeyManagerView();
            mInfoPortalManagerView = new InfoPortalManagerView();
            mUserGroupManagerView = new UserGroupManagerView();
            userBranchManagerView = new UserBranchManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //Chequeo si esta creada la base ------------------

            if (!mInfoPortalManagerView.IsDbCreated())
            {
                return RedirectToAction("ErrorAccess", "Error", new { Error = "There is a problem with the Data Base, please contact your partner" });
            }
            //--------------------------------------------------
            //Chequeo si no hay alguna Update -----------------

            string mUrlSql = AppDomain.CurrentDomain.BaseDirectory + @"\UpdatePortal\UpdatePortal.sql";

            DateTime lastModified = System.IO.File.GetLastWriteTime(mUrlSql);

            DateTime? UpdateDb = mInfoPortalManagerView.GetLasUpdate();

            if (UpdateDb == null || lastModified > UpdateDb)
            {
                return RedirectToAction("Index", "Update", new { DateUpdate = UpdateDb });
            }

            //--------------------------------------------------

            if (mAreaKeyManagerView.GetAreasAccess(Enums.Areas.WEBPORTAL))
            {
                return View();
            }

            return RedirectToAction("ErrorAccess", "Error", new { Error = "There is a problem with the system, please contact your partner" });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rememberMe"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Signin(string rememberMe)
        {
            UserView mUser = mUserManagerView.Signin(Request.Form["username"].ToString(), Request.Form["password"].ToString());

            if (mUser != null)
            {
                if (mUser.IsValidUser)
                {
                    mUser.ListPage.ForEach(c => c.translated = GetTras(c.KeyResource));

                    mUser.ListArea.ForEach(c => c.translated = GetTras(c.KeyResource));

                    Session["UserLogin"] = mUser;

                    return Json(mUser, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("ErrorSecurity", JsonRequestBehavior.AllowGet);
                }
            }
            else
            { return Json("", JsonRequestBehavior.AllowGet); }

        }

        [HttpPost]
        public JsonResult SigninCompany(string CompanyId)
        {
            try
            {
                AppGlobal.MandatoryPaymentMeans = Convert.ToBoolean(WebConfigurationManager.AppSettings["MandatoryPaymentMeans"]);

                if (CompanyId != "null")
                {
                    CompanyView mReturn;

                    if (CompanyId != "Disconnected")
                    {
                        mReturn = mCompaniesManager.SignIn(CompanyId, ((UserView)Session["UserLogin"]));
                        if (mReturn.UserGroupId != null)
                            ((UserView)Session["UserLogin"]).UserGroupObj = mUserGroupManagerView.GetUserGroup(mReturn.UserGroupId.Value);

                    }
                    else
                    {
                        mReturn = new CompanyView { IdCompany = 0, CompanyDB = "Disconnected", ResultConnection = ":" };
                    }

                    mReturn.IdUserConected = ((UserView)Session["UserLogin"]).IdUser;

                    ((UserView)Session["UserLogin"]).CompanyConnect = mReturn;

                    if (mReturn.CompanySAPConfig != null && mReturn.CompanySAPConfig.Branches.Count() > 0)
                    {
                        var idBranches = userBranchManagerView.GetUserBranchList(((UserView)Session["UserLogin"]).IdUser, Convert.ToInt32(CompanyId)).Select(c => c.IdBranchSAP).ToArray();

                        mReturn.CompanySAPConfig.Branches = mReturn.CompanySAPConfig.Branches.Where(c => idBranches.Contains(c.BPLId)).ToList();

                        if (mReturn.CompanySAPConfig.Branches.Count() == 0)
                        {
                            TempData["Error"] = "The user has not been associated to any branch.";

                            return Json(new
                            {
                                redirectUrl = Url.Action("Index", "Error"),
                                isRedirect = true
                            });
                        }

                        ((UserView)Session["UserLogin"]).CompanyConnect.IdBranchSelect = mReturn.CompanySAPConfig.Branches.FirstOrDefault().BPLId;
                        ((UserView)Session["UserLogin"]).UseBranche = true;
                    }
                    else
                    {
                        ((UserView)Session["UserLogin"]).UseBranche = false;
                    }

                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                    ((UserView)Session["UserLogin"]).Name,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(2),
                    false,
                    string.Empty,
                    FormsAuthentication.FormsCookiePath);

                    string encTicket = FormsAuthentication.Encrypt(ticket);

                    Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

                    return Json("Ok", JsonRequestBehavior.AllowGet);
                    //}
                }
                else
                {
                    Session["UserLogin"] = null;

                    FormsAuthentication.SignOut();

                    return Json("", JsonRequestBehavior.AllowGet);
                }

               
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Error"),
                    isRedirect = true
                });

            }

        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();

            AuthenticationManager.SignOut();

            Session.Clear();

            return View("Index");
        }

        private string GetTras(string pkey)
        {
            Dictionary<string, string> mValues = new Dictionary<string, string>();

            ResourceManager rm = new ResourceManager("ARGNS.WebSite.Resources.Views.Shared.Shared", Assembly.GetExecutingAssembly());

            mValues = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true).Cast<DictionaryEntry>().ToDictionary(r => r.Key.ToString(), r => r.Value.ToString());

            return mValues.Where(c => c.Key.ToUpper() == pkey.ToUpper()).Select(c => c.Value).FirstOrDefault();

        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


    }
}