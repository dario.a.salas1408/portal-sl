﻿//using ARGNS.ManagerView;
//using ARGNS.View;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using ARGNS.WebSite.Resources.Views.PurchaseOrders;
//using ARGNS.Util;
//using ARGNS.Model.Implementations.SAP;
//using ARGNS.Model.Implementations;
//using ARGNS.WebSite.Resources.Views.Error;

//namespace ARGNS.WebSite.Controllers
//{
//    [Authorize]
//    public class PurchaseOrderController : Controller
//    {
//        private PurchaseOrderManagerView mPOManagerView;

//        private ItemMasterManagerView mItemMasterManagerView;

//        private BusinessPartnerManagerView mBusinessPartnerManagerView;

//        private GlobalDocumentManagerView mGlobalDocumentManagerView;

//        public PurchaseOrderController()
//        {
//            mPOManagerView = new PurchaseOrderManagerView();
//            mItemMasterManagerView = new ItemMasterManagerView();
//            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
//            mGlobalDocumentManagerView = new GlobalDocumentManagerView();
//        }

//        public ActionResult Index()
//        {
//            try
//            {
//                TempData["ListItemsExit"] = null;
//                TempData["ListItems"] = null;
//                List<BusinessPartnerView> ListBp = new List<BusinessPartnerView>();
//                //List<PurchaseOrderView> ListPO = mPOManagerView.GetPurchaseOrders(((UserView)Session["UserLogin"]).CompanyConnect);
//                List<PurchaseOrderView> ListPO = new List<PurchaseOrderView>();
//                return View(Tuple.Create(ListPO, ListBp));
//            }
//            catch (Exception ex)
//            {
//                TempData["Error"] = ex.InnerException;
//                return RedirectToAction("Index", "Error");
//            }
//        }

//        public ActionResult ActionPurchaseOrder(string ActionPurchaseOrder, int IdPO)
//        {
//            PurchaseOrderView mPOView = null;

//            switch (ActionPurchaseOrder)
//            {
//                case "Add":
//                    ViewBag.Tite = PurchaseOrders.APO;
//                    ViewBag.FormMode = ActionPurchaseOrder;
//                    mPOView = mPOManagerView.GetPurchaseOrder(0, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString());

//                    mPOView.DocDate = System.DateTime.Now;
//                    mPOView.DocDueDate = System.DateTime.Now;
//                    mPOView.TaxDate = System.DateTime.Now;
//                    mPOView.ReqDate = null;
//                    mPOView.CancelDate = null;

//                    break;

//                case "Update":
//                    ViewBag.Tite = PurchaseOrders.UPO;
//                    ViewBag.FormMode = ActionPurchaseOrder;
//                    mPOView = mPOManagerView.GetPurchaseOrder(IdPO, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString());

//                    mPOView.Lines.Select(c => { c.ListCurrency = mPOView.ListDocumentSAPCombo.ListCurrency; return c; }).ToList();

//                    TempData["ListItemsExit"] = mPOView.Lines;

//                    break;

//            }

//            TempData["ListCurrency"] = mPOView.ListDocumentSAPCombo.ListCurrency;
//            TempData["Rates"] = mPOView.ListSystemRates;

//            return View("_PurchaseOrder", mPOView);
//        }

//        [HttpPost]
//        public JsonResult Add(PurchaseOrderView model)
//        {
//            model.Lines.Select(c => { c.ItemCode = ((List<PurchaseOrderLineView>)TempData["ListItemsExit"]).Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); return c; }).ToList();

//            if (mPOManagerView.Add(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString()))
//            { return Json("Ok"); }
//            else
//            { return Json(@Error.WebServError); }

//        }

//        [HttpPost]
//        public JsonResult Update(PurchaseOrderView model)
//        {
//            model.Lines.Select(c => { c.ItemCode = ((List<PurchaseOrderLineView>)TempData["ListItemsExit"]).Where(d => d.LineNum == c.LineNum).Select(d => d.ItemCode).FirstOrDefault(); return c; }).ToList();

//            if (mPOManagerView.Update(model, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString()))
//            { return Json("Ok"); }
//            else
//            { return Json(@Error.WebServError); }

//        }

//        [HttpPost]
//        public PartialViewResult _Items(string pItemCode = "", string pItemData = "")
//        {
//            return PartialView("_Items", GetItems(pItemCode, pItemData));
//        }

//        [HttpPost]
//        public PartialViewResult _ItemsForm(string[] pItems, string pCurrency)
//        {
//            List<PurchaseOrderLineView> ListReturn;

//            List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit"];

//            List<Currency> ListCurrency = (List<Currency>)TempData["ListCurrency"];

//            int mLine = 0;

//            if (ListReturnExit != null)
//            {
//                mLine = ListReturnExit.Count == 0 ? 0 : ListReturnExit.Count;
//            }

//            List<PurchaseOrderLineView> ListOitm = ((List<ItemMasterView>)TempData["ListItems"]).Where(c => pItems.Contains(c.ItemCode)).Select(c => new PurchaseOrderLineView { Dscription = c.ItemName, ItemCode = c.ItemCode, Currency = c.LastPurCur, LineNum = mLine++, Price = c.LastPurPrc ?? 0, Quantity = 1, ListCurrency = ListCurrency }).ToList();

//            if (pItems != null)
//            { ListReturn = ListOitm.Where(c => pItems.Contains(c.ItemCode)).ToList(); }
//            else
//            { ListReturn = new List<PurchaseOrderLineView>(); }

//            if (ListReturnExit != null)
//            { ListReturn = ListReturn.Union(ListReturnExit).ToList(); }

//            List<RatesSystem> ListRate = (List<RatesSystem>)TempData["Rates"];

//            double mRateGl = ListRate.Where(c => c.Currency == pCurrency).Select(c => c.Rate).FirstOrDefault();

//            TempData["Rates"] = ListRate;

//            ListReturn = ListReturn.Select(c => { c.Currency = pCurrency; c.RateGl = mRateGl; c.RateLine = ListRate.Where(d => d.Currency == c.Currency).Select(d => d.Rate).FirstOrDefault(); return c; }).ToList();

//            TempData["ListItems"] = ListReturn;

//            TempData["ListItemsExit"] = ListReturn;

//            TempData["ListCurrency"] = ListCurrency;

//            return PartialView("_ItemsForm", ListReturn);

//        }

//        [HttpPost]
//        public PartialViewResult _Vendors(string pVendorCode = "", string pVendorName = "")
//        {
//            List<BusinessPartnerView> ListReturn = mBusinessPartnerManagerView.GetBusinessPartners(((UserView)Session["UserLogin"]).CompanyConnect, Enums.BpType.Vendor).Where(c => (c.CardCode.ToUpper().Contains(pVendorCode.ToUpper())) && (c.CardName.ToUpper().Contains(pVendorName.ToUpper()))).ToList();

//            return PartialView("_Vendors", ListReturn);
//        }

//        [HttpPost]
//        public JsonResult GetBp(string Id, string LocalCurrency)
//        {
//            BusinessPartnerView mBP = mBusinessPartnerManagerView.GetBusinessPartner(Id, ((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser.ToString(), LocalCurrency);

//            if (mBP != null)
//            {
//                mBP.ErrorResponse = "Ok";
//                return Json(mBP, JsonRequestBehavior.AllowGet);
//            }
//            else
//            {
//                return Json(new BusinessPartnerView { ErrorResponse = @Error.WebServError }, JsonRequestBehavior.AllowGet);
//            }

//        }

//        public JsonResult GetRate(string TypeCurrency)
//        {

//            List<RatesSystem> Test = (List<RatesSystem>)TempData["Rates"];
//            try
//            {
//                double mRate = Test.Where(c => c.Currency == TypeCurrency).Select(c => c.Rate).FirstOrDefault();

//                TempData["Rates"] = Test;

//                if (mRate != null)
//                {
//                    return Json(mRate, JsonRequestBehavior.AllowGet);
//                }
//                else
//                { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
//            }
//            catch (Exception ex)
//            {
//                throw;
//            }
//        }

//        [HttpPost]
//        public JsonResult DeleteRow(string ItemCode)
//        {
//            try
//            {
//                List<PurchaseOrderLineView> ListReturnExit = (List<PurchaseOrderLineView>)TempData["ListItemsExit"];

//                ListReturnExit.RemoveAll(c => c.LineNum == Convert.ToInt32(ItemCode));

//                TempData["ListItemsExit"] = ListReturnExit;

//                return Json("Ok");
//            }
//            catch (Exception ex)
//            {
//                return Json(@Error.WebServError);
//            }

//        }

//        private List<ItemMasterView> GetItems(string pItemCode = "", string pItemData = "", string pModCode = "")
//        {
//            try
//            {

//                List<ItemMasterView> ListReturn = mItemMasterManagerView.GetOITMListBy(((UserView)Session["UserLogin"]).CompanyConnect, pItemCode, pItemData, pModCode);

//                List<Currency> ListCurrency = (List<Currency>)TempData["ListCurrency"];

//                TempData["ListItems"] = ListReturn.Select(c => { c.ListCurrency = ListCurrency; return c; }).ToList();

//                TempData["ListCurrency"] = ListCurrency;

//                return ListReturn;
//            }
//            catch (Exception ex)
//            {

//                throw;
//            }

//        }

//        [HttpPost]
//        public PartialViewResult ListPO(string txtCodeVendor = "", string txtDate = "", string txtNro = "")
//        {

//            string Vendor = txtCodeVendor;
//            DateTime? Date = null;
//            int? DocNum = null;

//            if (txtDate != "")
//            {
//                Date = Convert.ToDateTime(txtDate);
//            }

//            if (txtNro != "")
//            {
//                DocNum = Convert.ToInt32(txtNro);
//            }

//            List<PurchaseOrderView> ListPO = mPOManagerView.GetPurchaseOrderListSearch(((UserView)Session["UserLogin"]).CompanyConnect, Vendor, Date, DocNum);

//            return PartialView("_PurchaseOrderList", ListPO);
//        }
//    }
//}