﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class LogController : Controller
    {
        private LogManagerView mLogManagerView;

        public LogController()
        {
            mLogManagerView = new LogManagerView();
        }

        public ActionResult Index()
        {
            return View(mLogManagerView.GetListLogByUser(((UserView)Session["UserLogin"]).IdUser));
        }
    }
}