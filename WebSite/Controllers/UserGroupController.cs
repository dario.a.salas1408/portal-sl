﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Companies;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class UserGroupController : Controller
    {
        private UserGroupManagerView mUserGroupManagerView;
        private CompaniesManagerView mCompanyManagerView;
        public UserGroupController()
        {
            mUserGroupManagerView = new UserGroupManagerView();
            mCompanyManagerView = new CompaniesManagerView();
        }

        public ActionResult Index(int IdCompany)
        {
            List<UserGroupView> mListUserGroup = mUserGroupManagerView.GetUserGroupList(IdCompany);
            CompanyView mCompany = mCompanyManagerView.GetCompany(IdCompany);
            ViewBag.Tite = Companies.UG;
            return View(Tuple.Create(mListUserGroup, mCompany));
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ActionUserGroup"></param>
		/// <param name="IdUserGroup"></param>
		/// <param name="IdCompany"></param>
		/// <returns></returns>
        public ActionResult ActionUserGroup(string ActionUserGroup, int IdUserGroup, int IdCompany)
        {
            UserGroupView mUserGroupView = null;

            switch (ActionUserGroup)
            {
                case "Add":
                    ViewBag.Tite = Users.AUG;
                    ViewBag.FormMode = ActionUserGroup;
                    mUserGroupView = new UserGroupView();
                    mUserGroupView.IdCompany = IdCompany;
                    break;

                case "Edit":
                    ViewBag.Tite = Users.UUG;
                    ViewBag.FormMode = ActionUserGroup;
                    mUserGroupView = mUserGroupManagerView.GetUserGroup(IdUserGroup);
                    break;
            }

            return View("_UserGroup", mUserGroupView);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pUserGroup"></param>
		/// <returns></returns>
        [HttpPost]
        public JsonResult Add(UserGroupView pUserGroup)
        {

            if (mUserGroupManagerView.Add(pUserGroup))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pUserGroup"></param>
		/// <returns></returns>
        [HttpPost]
        public JsonResult Update(UserGroupView pUserGroup)
        {
            if (mUserGroupManagerView.Update(pUserGroup))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }
    }
}