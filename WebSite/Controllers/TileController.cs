﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Charts;
using ARGNS.WebSite.Resources.Views.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class TileController : Controller
    {
        private TileManagerView mTileManagerView;
        private QueryManagerManagerView mQueryManagerManagerView;
        private UserManagerView mUserManagerView;
        private UserGroupManagerView mUserGroupManagerView;

        public TileController()
        {
            mTileManagerView = new TileManagerView();
            mQueryManagerManagerView = new QueryManagerManagerView();
            mUserManagerView = new UserManagerView();
            //mAreaKeyManagerView = new AreaKeyManagerView();
            mUserGroupManagerView = new UserGroupManagerView();
        }

        public ActionResult Index()
        {
            try
            {
                List<PortalTileView> mPortalTileList = mTileManagerView.GetPortalTileList(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
                List<PortalTileTypeView> mPortalTileTypeList = mTileManagerView.GetPortalTileTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
                return View(Tuple.Create(mPortalTileList, mPortalTileTypeList));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        public ActionResult TileCreator(string ActionTileCreator, int pTileId)
        {
            PortalTileView mPortalTileView = null;
            string mPageKey = PageKeyGenerator.GetPageKey();

            switch (ActionTileCreator)
            {
                case "Add":
                    ViewBag.FormMode = ActionTileCreator;
                    ViewBag.PageKey = mPageKey;
                    mPortalTileView = mTileManagerView.GetPortalTileById(((UserView)Session["UserLogin"]).CompanyConnect, pTileId);
                    ViewBag.Tite = Charts.CC;
                    break;
                case "Update":
                    ViewBag.FormMode = ActionTileCreator;
                    ViewBag.PageKey = mPageKey;
                    mPortalTileView = mTileManagerView.GetPortalTileById(((UserView)Session["UserLogin"]).CompanyConnect, pTileId);
                    ViewBag.Tite = Charts.CC;
                    break;
            }
            if(mPortalTileView == null)
            {
                mPortalTileView = new PortalTileView();
            }
            mPortalTileView.ListPortalTileType.Add(new PortalTileTypeView());
            mPortalTileView.ListPortalTileType.AddRange(mTileManagerView.GetPortalTileTypeList(((UserView)Session["UserLogin"]).CompanyConnect));

            List<ListItem> mParaValueList = new List<ListItem>();
            mParaValueList.Add(new ListItem() { Value = "", Text = "" });
            mParaValueList.Add(new ListItem() { Value = "LoggedBP", Text = Charts.LBP });
            mParaValueList.Add(new ListItem() { Value = "LoggedSE", Text = Charts.LSE });
            mParaValueList.Add(new ListItem() { Value = "LoggedUser", Text = Charts.LUser });
            mParaValueList.Add(new ListItem() { Value = "FixedValue", Text = Charts.FixedValue });
            PortalTileParamsView mPortalTileParamsView = new PortalTileParamsView(mParaValueList);
            mPortalTileParamsView.PortalTile = mPortalTileView;
            mPortalTileParamsView.Company = ((UserView)Session["UserLogin"]).CompanyConnect;
            mPortalTileParamsView.ListPortalTileUserGroup = mTileManagerView.GetPortalTileUserGroupList(((UserView)Session["UserLogin"]).CompanyConnect, mPortalTileView.IdTile);
            mPortalTileParamsView.ListUserGroup = mUserGroupManagerView.GetUserGroupList(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);

            return View("_TileCreator", mPortalTileParamsView);
        }

        [HttpPost]
        public ActionResult _GetSAPQueryList(string pPageKey)
        {
            try
            {
                List<QuerySAPView> mListQuerySAP = new List<QuerySAPView>();
                mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                TempData["ListQuery" + pPageKey] = mListQuerySAP;

                return PartialView("_QuerySAPListTile", mListQuerySAP);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public ActionResult _GetSAPQueryResultTable(string pPageKey, int pQueryInternalKey, List<PortalTileParamView> pListParams)
        {
            try
            {
                List<QuerySAPView> mListQuerySAP = (List<QuerySAPView>)TempData["ListQuery" + pPageKey];
                if (mListQuerySAP == null)
                    mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                string mQuery = mListQuerySAP.Where(c => c.IntrnalKey == pQueryInternalKey).FirstOrDefault().QString;
                if (pListParams != null)
                    mQuery = GetQueryWithParams(mQuery, pListParams);
                JsonObjectResult mJsonObjectResult = mUserManagerView.GetSAPQueryResult(((UserView)Session["UserLogin"]).CompanyConnect, mQuery);
                TempData["ListQuery" + pPageKey] = mListQuerySAP;

                return PartialView("_QueryTableResultTile", mJsonObjectResult);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public ActionResult _GetSAPQueryParamList(string pPageKey, int pQueryInternalKey)
        {
            try
            {
                List<QuerySAPView> mListQuerySAP = (List<QuerySAPView>)TempData["ListQuery" + pPageKey];
                TempData["ListQuery" + pPageKey] = mListQuerySAP;

                string mQuery = mListQuerySAP.Where(c => c.IntrnalKey == pQueryInternalKey).FirstOrDefault().QString;
                List<PortalTileParamView> mParamList = new List<PortalTileParamView>();
                int i = 0;
                while (mQuery.IndexOf("[%") != -1)
                {
                    int mStartIndex = mQuery.IndexOf("[%");
                    int mLastIndex = mQuery.IndexOf("]", mStartIndex) + 1;
                    PortalTileParamView mAuxParam = new PortalTileParamView();
                    mAuxParam.ParamName = (mQuery.Substring(mStartIndex, mLastIndex - mStartIndex));
                    mAuxParam.Id = i;
                    mAuxParam.IsNew = true;
                    mParamList.Add(mAuxParam);
                    mQuery = mQuery.Substring(mLastIndex, mQuery.Length - mLastIndex);
                    i++;
                }

                List<ListItem> mParaValueList = new List<ListItem>();
                mParaValueList.Add(new ListItem() { Value = "", Text = "" });
                mParaValueList.Add(new ListItem() { Value = "LoggedBP", Text = Charts.LBP });
                mParaValueList.Add(new ListItem() { Value = "LoggedSE", Text = Charts.LSE });
                mParaValueList.Add(new ListItem() { Value = "LoggedUser", Text = Charts.LUser });
                mParaValueList.Add(new ListItem() { Value = "FixedValue", Text = Charts.FixedValue });

                return PartialView("_ParamListTile", Tuple.Create(mParamList, mParaValueList));
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }


        [HttpPost]
        public JsonResult Add(PortalTileView mPortalTileView)
        {
            return Json(mTileManagerView.Add(mPortalTileView));
        }

        [HttpPost]
        public JsonResult Update(PortalTileView mPortalTileView)
        {
            return Json(mTileManagerView.Update(mPortalTileView));
        }

        [HttpPost]
        public JsonResult Delete(int mPortalTileId)
        {
            if (mTileManagerView.Delete(mPortalTileId))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }
        }

        private string GetQueryWithParams(string pQuery, List<PortalTileParamView> pListParams)
        {
            foreach (PortalTileParamView mParamAux in pListParams)
            {
                string mValueToReplace = "";
                switch (mParamAux.ParamValue)
                {
                    case "LoggedBP":
                        mValueToReplace = ((UserView)Session["UserLogin"]).BPCode;
                        break;
                    case "LoggedSE":
                        mValueToReplace = ((UserView)Session["UserLogin"]).SECode.ToString();
                        break;
                    case "LoggedUser":
                        UserSAP mUserSAP = mUserManagerView.GetUserSAPByPortalUser(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);
                        mValueToReplace = mUserSAP.USERID.ToString();
                        break;
                    case "FixedValue":
                        mValueToReplace = mParamAux.ParamFixedValue;
                        break;
                }
                pQuery = pQuery.Replace(mParamAux.ParamName, mValueToReplace);
            }

            return pQuery;
        }
    }
}