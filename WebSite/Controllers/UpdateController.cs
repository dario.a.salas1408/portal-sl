﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Controllers
{
    public class UpdateController : Controller
    {
        // GET: Update
        public ActionResult Index(string DateUpdate)
        {
            TempData["UpdateDate"] = (DateUpdate == null ? System.DateTime.Now.ToString() : DateUpdate);
            return View();
        }

        public ActionResult Update()
        {
            try
            {
                string sqlConnectionString = ConfigurationManager.ConnectionStrings["DbWebPortal"].ConnectionString;

                string mUrlSql = AppDomain.CurrentDomain.BaseDirectory + @"\UpdatePortal\UpdatePortal.sql";

                string script = System.IO.File.ReadAllText(mUrlSql);

                if (script != "")
                {

                    SqlConnection sqlConnection = new SqlConnection(sqlConnectionString);
                    SqlCommand cmd = new SqlCommand();
                    SqlDataReader reader;

                    cmd.CommandText = script;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = sqlConnection;

                    sqlConnection.Open();
                    reader = cmd.ExecuteReader();
                    sqlConnection.Close();
                }
                return RedirectToAction("Index", "Login");
            }
            catch (Exception ex)
            {
                return RedirectToAction("ErrorAccess", "Error", new { Error = ex.Message });
            }

        }
    }
}