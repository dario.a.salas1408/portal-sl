﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Resources.Views.Charts;
using ARGNS.WebSite.Resources.Views.Error;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ARGNS.WebSite.Controllers
{
    public class ChartController : Controller
    {
        private ChartManagerView mChartManagerView;
        private QueryManagerManagerView mQueryManagerManagerView;
        private UserManagerView mUserManagerView;
        private AreaKeyManagerView mAreaKeyManagerView;
        private UserGroupManagerView mUserGroupManagerView;

        public ChartController()
        {
            mChartManagerView = new ChartManagerView();
            mQueryManagerManagerView = new QueryManagerManagerView();
            mUserManagerView = new UserManagerView();
            mAreaKeyManagerView = new AreaKeyManagerView();
            mUserGroupManagerView = new UserGroupManagerView();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                List<PortalChartView> mPortalChartList = mChartManagerView.GetPortalChartList(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
                List<PortalChartTypeView> mPortalChartTypeList = mChartManagerView.GetPortalChartTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
                List<AreaKeyView> mAreaList = mAreaKeyManagerView.GetAreasKeyList();
                return View(Tuple.Create(mPortalChartList, mPortalChartTypeList, mAreaList));
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Error");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionChartCreator"></param>
        /// <param name="pChartId"></param>
        /// <returns></returns>
        public ActionResult ChartCreator(string ActionChartCreator, int pChartId)
        {
            PortalChartView mPortalChartView = null;
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.URLRedirect = "/Chart/Index";

            switch (ActionChartCreator)
            {
                case "Add":
                    ViewBag.FormMode = ActionChartCreator;
                    ViewBag.PageKey = mPageKey;
                    mPortalChartView = mChartManagerView.GetPortalChartById(((UserView)Session["UserLogin"]).CompanyConnect, pChartId);
                    ViewBag.Tite = Charts.CC;
                    List<QuerySAPView> mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                    TempData["ListQuery" + mPageKey] = mListQuerySAP;
                    break;
                case "Update":
                    ViewBag.FormMode = ActionChartCreator;
                    ViewBag.PageKey = mPageKey;
                    mPortalChartView = mChartManagerView.GetPortalChartById(((UserView)Session["UserLogin"]).CompanyConnect, pChartId);
                    mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                    TempData["ListQuery" + mPageKey] = mListQuerySAP;
                    ViewBag.Tite = Charts.CC;
                    break;
            }

            if(mPortalChartView == null)
            {
                mPortalChartView = new PortalChartView();
            }

            mPortalChartView.ListPortalChartType = mChartManagerView.GetPortalChartTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
            TempData["ListPortalChartType" + mPageKey] = mPortalChartView.ListPortalChartType;
            TempData["PortalChart" + mPageKey] = mPortalChartView;

            List<PortalChartParamsTypeView> mPortalChartParamsType = new List<PortalChartParamsTypeView>();
            mPortalChartParamsType = mChartManagerView.GetPortalChartParamsTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
            List<PortalChartParamsValueTypeView> mPortalChartParamsValueType = new List<PortalChartParamsValueTypeView>();
            mPortalChartParamsValueType = mChartManagerView.GetPortalChartParamsValueTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
            mPortalChartParamsType = mPortalChartParamsType.Select(c => { c.ParamTypeDescTranslated = GetTrans(c.ParamTypeDesc); return c; }).ToList();

            PortalChartParamsView mPortalChartParamsView = new PortalChartParamsView(mPortalChartParamsType, mPortalChartParamsValueType);
            mPortalChartParamsView.PortalChart = mPortalChartView;

            mPortalChartParamsView.Company = ((UserView)Session["UserLogin"]).CompanyConnect;
            mPortalChartParamsView.ListAreas = mAreaKeyManagerView.GetAreasKeyList();
            mPortalChartParamsView.ListPortalChartUserGroup = mChartManagerView.GetPortalChartUserGroupList(((UserView)Session["UserLogin"]).CompanyConnect, mPortalChartView.IdChart);
            mPortalChartParamsView.ListUserGroup = mUserGroupManagerView.GetUserGroupList(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);

            return View("_ChartCreator", mPortalChartParamsView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdChart"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _ChartViewer(int IdChart)
        {
           PortalChartView mPortalChartView = mChartManagerView.GetPortalChartById(((UserView)Session["UserLogin"]).CompanyConnect, IdChart);

           return PartialView("_PortalChart", mPortalChartView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _GetSAPQueryList(string pPageKey)
        {
            try
            {
                List<QuerySAPView> mListQuerySAP = new List<QuerySAPView>();
                mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                TempData["ListQuery" + pPageKey] = mListQuerySAP;

                return PartialView("_QuerySAPList", mListQuerySAP);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pQueryInternalKey"></param>
        /// <param name="pListParams"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _GetSAPQueryResultTable(string pPageKey, int pQueryInternalKey, List<PortalChartParamView> pListParams)
        {
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                List<QuerySAPView> mListQuerySAP = (List<QuerySAPView>)TempData["ListQuery" + pPageKey];
                if(mListQuerySAP == null)
                    mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                string mQuery = mListQuerySAP.Where(c => c.IntrnalKey == pQueryInternalKey).FirstOrDefault().QString;

                if(pListParams != null)
                {
                    bool missingParam = false; 
                    mQuery = GetQueryWithParams(mQuery, pListParams, out missingParam);
                    if (missingParam)
                    {
                        mJsonObjectResult.ErrorMsg = Charts.MissingParameters;
                        mJsonObjectResult.ServiceAnswer = "Error";
                        mJsonObjectResult.UserMsg = Charts.MissingParameters;
                        return Json(mJsonObjectResult);
                    }
                }
                    
                mJsonObjectResult = mUserManagerView.GetSAPQueryResult(((UserView)Session["UserLogin"]).CompanyConnect, mQuery);

                if (!IsEmptyString(mJsonObjectResult.ErrorMsg))
                {
                    mJsonObjectResult.ErrorMsg = Charts.InvalidQuery + " " + mJsonObjectResult.ErrorMsg;
                    mJsonObjectResult.ServiceAnswer = "Error";
                    mJsonObjectResult.UserMsg = Charts.InvalidQuery;
                    return Json(mJsonObjectResult);
                }

                if (mJsonObjectResult.Others.Where(w=> w.Key == "jsonQueryResult").FirstOrDefault().Value != null)
                {
                    RecordsetView mRecordSet = Newtonsoft.Json.JsonConvert.DeserializeObject<RecordsetView>(mJsonObjectResult.Others.Where(w => w.Key == "jsonQueryResult").FirstOrDefault().Value);
                    if (mRecordSet.Row == null)
                    {
                        mJsonObjectResult.ErrorMsg = Charts.NoData + " " + mJsonObjectResult.ErrorMsg;
                        mJsonObjectResult.ServiceAnswer = "Error";
                        mJsonObjectResult.UserMsg = Charts.NoData;
                        return Json(mJsonObjectResult);
                    }
                }
                else
                {
                    mJsonObjectResult.ErrorMsg = Charts.NoData + " " + mJsonObjectResult.ErrorMsg;
                    mJsonObjectResult.ServiceAnswer = "Error";
                    mJsonObjectResult.UserMsg = Charts.NoData;
                    return Json(mJsonObjectResult);
                }

                TempData["ListQuery" + pPageKey] = mListQuerySAP;

                return PartialView("_QueryTableResult", mJsonObjectResult);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool IsEmptyString(string value)
        {
            if (value == null)
            {
                return true; 
            }
            else
            {
                return value.Equals("");
            }
        }  




        [HttpPost]
        public JsonResult _GetSAPQueryResult(string pPageKey, int pQueryInternalKey)
        {
            try
            {
                List<QuerySAPView> mListQuerySAP = (List<QuerySAPView>)TempData["ListQuery" + pPageKey];
                JsonObjectResult mJsonObjectResult = mUserManagerView.GetSAPQueryResult(((UserView)Session["UserLogin"]).CompanyConnect, mListQuerySAP.Where(c => c.IntrnalKey == pQueryInternalKey).FirstOrDefault().QString);
                TempData["ListQuery" + pPageKey] = mListQuerySAP;

                return Json(mJsonObjectResult.Others.Where(c => c.Key == "jsonQueryResult").FirstOrDefault().Value);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public ActionResult _GetPortalChartTypeById(string pPageKey, int pPortalChartTypeId, int? pQueryInternalKey, List<PortalChartParamView> pListParams)
        {
            try
            {
                if(pQueryInternalKey != null)
                { 
                    List<PortalChartTypeView> mListPortalChartType = (List<PortalChartTypeView>)TempData["ListPortalChartType" + pPageKey];
                    List<QuerySAPView> mListQuerySAP = (List<QuerySAPView>)TempData["ListQuery" + pPageKey];
                    PortalChartView mPortalChartView = (PortalChartView)TempData["PortalChart" + pPageKey];
                    TempData["ListPortalChartType" + pPageKey] = mListPortalChartType;
                    TempData["ListQuery" + pPageKey] = mListQuerySAP;
                    TempData["PortalChart" + pPageKey] = mPortalChartView;

                    string mQuery = mListQuerySAP.Where(c => c.IntrnalKey == pQueryInternalKey).FirstOrDefault().QString;
                    if (pListParams != null)
                    {
                        bool missingParam = false;
                        mQuery = GetQueryWithParams(mQuery, pListParams, out missingParam);
                        if (missingParam)
                        {
                            return Json(Charts.MissingParameters);
                        }
                    }

                    JsonObjectResult mJsonObjectResult = mUserManagerView.GetSAPQueryResult(((UserView)Session["UserLogin"]).CompanyConnect, mQuery);
                    RecordsetView mRecordSet = Newtonsoft.Json.JsonConvert.DeserializeObject<RecordsetView>(mJsonObjectResult.Others.Where(c => c.Key == "jsonQueryResult").FirstOrDefault().Value);
                    List<string> mHeadersName = mRecordSet.Row.FirstOrDefault().Property.Select(c => c.Name).ToList();
                    return PartialView("_PortalChartColumns", Tuple.Create(mListPortalChartType.Where(c => c.IdChartType == pPortalChartTypeId).FirstOrDefault(), mHeadersName.Select(c => new ListItem { Value = c.ToString(), Text = c.ToString() }).ToList(), mPortalChartView.PortalChartColumns));
                }
                else
                {
                    return PartialView();
                }
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pPageKey"></param>
        /// <param name="pQueryInternalKey"></param>
        /// <param name="pListParams"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _DrawChartInPortalPage(string pPageKey, int pQueryInternalKey, List<PortalChartParamView> pListParams)
        {
            try
            {
                List<QuerySAPView> mListQuerySAP = new List<QuerySAPView>();
                mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                string mQuery = mListQuerySAP.Where(c => c.IntrnalKey == pQueryInternalKey).FirstOrDefault().QString;
                if (pListParams != null)
                {
                    bool missingParam = false;
                    mQuery = GetQueryWithParams(mQuery, pListParams, out missingParam);
                    if (missingParam)
                    {
                        return Json(Charts.MissingParameters);
                    }
                }

                JsonObjectResult mJsonObjectResult = mUserManagerView.GetSAPQueryResult(((UserView)Session["UserLogin"]).CompanyConnect, mQuery);

                return Json(mJsonObjectResult.Others.Where(c => c.Key == "jsonQueryResult").FirstOrDefault().Value);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public ActionResult _GetChartMenuList(string pPortalPage)
        {
            try
            {
                List<AreaKeyView> mAreaList = mAreaKeyManagerView.GetAreasKeyList();

                List<PortalChartView> mListPortalChartView = mChartManagerView.GetPortalChartListSearch(mAreaList.Where(c => c.KeyResource == pPortalPage).FirstOrDefault().IdAreaKeys.ToString(), ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, ((UserView)Session["UserLogin"]).UserGroupObj);

                return PartialView("_ReportsList", mListPortalChartView.Where(c => c.MenuChart == true).ToList());
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public ActionResult _GetSAPQueryParamList(string pPageKey, int pQueryInternalKey)
        {
            try
            {
                List<QuerySAPView> mListQuerySAP = (List<QuerySAPView>)TempData["ListQuery" + pPageKey];
                TempData["ListQuery" + pPageKey] = mListQuerySAP;

                string mQuery = mListQuerySAP.Where(c => c.IntrnalKey == pQueryInternalKey).FirstOrDefault().QString;
                List<PortalChartParamView> mParamList = new List<PortalChartParamView>();
                int i = 0;
                while (mQuery.IndexOf("[%") != -1)
                {
                    int mStartIndex = mQuery.IndexOf("[%");
                    int mLastIndex = mQuery.IndexOf("]", mStartIndex) + 1;
                    PortalChartParamView mAuxParam = new PortalChartParamView();
                    mAuxParam.ParamName =(mQuery.Substring(mStartIndex, mLastIndex - mStartIndex));
                    mAuxParam.Id = i;
                    mAuxParam.IsNew = true;
                    mParamList.Add(mAuxParam);
                    mQuery = mQuery.Substring(mLastIndex, mQuery.Length - mLastIndex);
                    i++;
                }

                List<PortalChartParamsTypeView> mPortalChartParamsType = new List<PortalChartParamsTypeView>();
                mPortalChartParamsType = mChartManagerView.GetPortalChartParamsTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
                mPortalChartParamsType = mPortalChartParamsType.Select( c => { c.ParamTypeDescTranslated = GetTrans(c.ParamTypeDesc); return c; }).ToList();


                List<PortalChartParamsValueTypeView> mPortalChartParamsValueType = new List<PortalChartParamsValueTypeView>();
                mPortalChartParamsValueType = mChartManagerView.GetPortalChartParamsValueTypeList(((UserView)Session["UserLogin"]).CompanyConnect);

                return PartialView("_ParamList", Tuple.Create(mParamList, mPortalChartParamsType, mPortalChartParamsValueType));
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }
        

        [HttpPost]
        public JsonResult Add(PortalChartView mPortalChartView)
        {
            JsonObjectResult mResult = mChartManagerView.Add(mPortalChartView);
            mResult.AddUpdateMsg = Resources.Views.Charts.Charts.AddResultMsg.Replace("{Code}", mResult.Code);

            return Json(mResult);
        }

        [HttpPost]
        public JsonResult Update(PortalChartView mPortalChartView)
        {
            return Json(mChartManagerView.Update(mPortalChartView));
        }

        [HttpPost]
        public JsonResult Delete(int mPortalChartId)
        {
            if (mChartManagerView.Delete(mPortalChartId))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pQuery"></param>
        /// <param name="pListParams"></param>
        /// <returns></returns>
        private string GetQueryWithParams(string pQuery, List<PortalChartParamView> pListParams, out bool missingParam)
        {
            missingParam = false;

            foreach (PortalChartParamView mParamAux in pListParams)
            {
                string mValueToReplace = "";
                switch (mParamAux.ParamValue)
                {
                    case "LoggedBP":
                        mValueToReplace = ((UserView)Session["UserLogin"]).BPCode;
                        break;
                    case "LoggedSE":
                        mValueToReplace = ((UserView)Session["UserLogin"]).SECode.ToString();
                        break;
                    case "LoggedUser":
                        UserSAP mUserSAP = mUserManagerView.GetUserSAPByPortalUser(((UserView)Session["UserLogin"]).CompanyConnect, 
                            ((UserView)Session["UserLogin"]).IdUser);
                        mValueToReplace = mUserSAP.USERID.ToString();
                        break;
                    case "FixedValue":
                        mValueToReplace = mParamAux.ParamFixedValue;
                        break;
                    case "EnteredByUser":
                        mValueToReplace = mParamAux.ParamFixedValue;
                        break;
                }

                pQuery = pQuery.Replace(mParamAux.ParamName, mValueToReplace);
                if (IsEmptyString(mValueToReplace))
                {
                    missingParam = true; 
                }
            }

            return pQuery;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pkey"></param>
        /// <returns></returns>
        private string GetTrans(string pkey)
        {
            Dictionary<string, string> mValues = new Dictionary<string, string>();

            ResourceManager rm = new ResourceManager("ARGNS.WebSite.Resources.Views.Charts.Charts", Assembly.GetExecutingAssembly());

            mValues = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true).Cast<DictionaryEntry>().ToDictionary(r => r.Key.ToString(), r => r.Value.ToString());

            return mValues.Where(c => c.Key.ToUpper() == pkey.ToUpper()).Select(c => c.Value).FirstOrDefault();

        }
    }
}