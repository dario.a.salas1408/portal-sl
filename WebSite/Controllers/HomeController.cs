﻿using ARGNS.ManagerView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.Util;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System.Resources;
using System.Globalization;
using System.Collections;
using System.Reflection;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class HomeController : Controller
    {
        private UserManagerView mUser;
        private NewManagerView mNew;
        private DirectAccessManagerView mDirectAccessManagerView;
        private PageManagerView mPageManagerView;
        private AreaKeyManagerView mAreaKeyManagerView;
        private ChartManagerView mChartManagerView;
        private TileManagerView mTileManagerView;
        private QueryManagerManagerView mQueryManagerManagerView;
		/// <summary>
		/// 
		/// </summary>
        public HomeController()
        {
            mUser = new UserManagerView();
            mNew = new NewManagerView();
            mDirectAccessManagerView = new DirectAccessManagerView();
            mPageManagerView = new PageManagerView();
            mAreaKeyManagerView = new AreaKeyManagerView();
            mChartManagerView = new ChartManagerView();
            mTileManagerView = new TileManagerView();
            mQueryManagerManagerView = new QueryManagerManagerView();
        }

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
        [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
        public ActionResult Index()
        {
            try
            {
                HomeObjectView mHomeObjectView = new HomeObjectView();
                bool mDiserverConnected = false;
                AreaKeyView mAreaKey = new AreaKeyView();
                List<PortalChartView> mListPortalChartView = new List<PortalChartView>();
                mHomeObjectView.ListNews = mNew.GetNews();

                if (((UserView)Session["UserLogin"]).CompanyConnect.CompanyDB != "Disconnected" )
                {
                    if (((UserView)Session["UserLogin"]).CompanyConnect.ResultConnection.Split(':')[0] != "Error")
                    {
                        mDiserverConnected = true;
                    }

                    mHomeObjectView.ListAlerts = mUser.GetAlertsByUser(((UserView)Session["UserLogin"]).CompanyConnect,((UserView)Session["UserLogin"]).IdUser);
                    mHomeObjectView.ListUserDirectAccessView = mDirectAccessManagerView.GetUserDirectAccessList(((UserView)Session["UserLogin"]).IdUser, ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);
                    mHomeObjectView.ListAreaKeyView = mAreaKeyManagerView.GetAreasKeyList();
                    mHomeObjectView.ListDirectAccessView = mDirectAccessManagerView.GetDirectAccessList();
                    mHomeObjectView.ListTileTypeView = mTileManagerView.GetPortalTileTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
                    mHomeObjectView.ListTileView = mTileManagerView.GetPortalTileList(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);

                    List<QuerySAPView> mListQuerySAP = null;
                    foreach (PortalTileView mTileView in mHomeObjectView.ListTileView)
                    {
                        //Assing the value of Query Result to Query Tiles
                        if(mTileView.IdTileType == mHomeObjectView.ListTileTypeView.Where(c => c.TypeName == "Query").FirstOrDefault().IdTileType)
                        {
                            if(mListQuerySAP == null)
                                mListQuerySAP = mQueryManagerManagerView.GetSAPQueryListSearch(((UserView)Session["UserLogin"]).CompanyConnect);
                            string mQuery = mListQuerySAP.Where(c => c.IntrnalKey == mTileView.IdSAPQuery).FirstOrDefault().QString;
                            if (mTileView.PortalTileParams.Count > 0)
                                mQuery = GetQueryWithParams(mQuery, mTileView.PortalTileParams);
                            JsonObjectResult mJsonObjectResult = mUser.GetSAPQueryResult(((UserView)Session["UserLogin"]).CompanyConnect, mQuery);
                            if(mJsonObjectResult.Others.Count > 0)
                            { 
                                RecordsetView mRecordSet = Newtonsoft.Json.JsonConvert.DeserializeObject<RecordsetView>(mJsonObjectResult.Others.Where(c => c.Key == "jsonQueryResult").FirstOrDefault().Value);
                                if(mRecordSet.Row.FirstOrDefault().Property != null)
                                { 
                                    mTileView.QueryResult = mRecordSet.Row.FirstOrDefault().Property.FirstOrDefault().Value;
                                }
                            }
                        }

                        //Assing the value of Visual Order Tile to UserDirectAccess
                        UserDirectAccessView mDirectAccessAux = mHomeObjectView.ListUserDirectAccessView.Where(c => c.IdTile == mTileView.IdTile).FirstOrDefault();
                        if(mDirectAccessAux != null)
                            mHomeObjectView.ListUserDirectAccessView.Where(c => c.IdTile == mTileView.IdTile).FirstOrDefault().VisualOrder = mTileView.VisualOrder;

                    }

                    Dictionary<string, string> mResourceValues = new Dictionary<string, string>();
                    ResourceManager rm = new ResourceManager("ARGNS.WebSite.Resources.Views.DirectAccess.DirectAccess", Assembly.GetExecutingAssembly());
                    mResourceValues = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true).Cast<DictionaryEntry>().ToDictionary(r => r.Key.ToString(), r => r.Value.ToString());
                    foreach (DirectAccessView mDirectAccess in mHomeObjectView.ListDirectAccessView)
                    {
                        mDirectAccess.TranslatedName = mResourceValues.Where(c => c.Key == mDirectAccess.TranslationKey).Select(c => c.Value).FirstOrDefault();
                    }

                    mHomeObjectView.ListPageView = mPageManagerView.GetPageList();
                    mAreaKey = mAreaKeyManagerView.GetAreasKeyList().Where(c => c.Area.ToUpper() == Util.Enums.Areas.WEBPORTAL.ToString().ToUpper()).FirstOrDefault();
                    mListPortalChartView = mChartManagerView.GetPortalChartListSearch(mAreaKey.IdAreaKeys.ToString(), ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany, ((UserView)Session["UserLogin"]).UserGroupObj);
                }
                return View(Tuple.Create(mHomeObjectView,mListPortalChartView, mDiserverConnected));
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.InnerException;

                return RedirectToAction("Index", "Error");
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pQuery"></param>
		/// <param name="pListParams"></param>
		/// <returns></returns>
        private string GetQueryWithParams(string pQuery, List<PortalTileParamView> pListParams)
        {
            foreach (PortalTileParamView mParamAux in pListParams)
            {
                string mValueToReplace = "";
                switch (mParamAux.ParamValue)
                {
                    case "LoggedBP":
                        mValueToReplace = ((UserView)Session["UserLogin"]).BPCode;
                        break;
                    case "LoggedSE":
                        mValueToReplace = ((UserView)Session["UserLogin"]).SECode.ToString();
                        break;
                    case "LoggedUser":
                        UserSAP mUserSAP = mUser.GetUserSAPByPortalUser(((UserView)Session["UserLogin"]).CompanyConnect, ((UserView)Session["UserLogin"]).IdUser);
                        mValueToReplace = mUserSAP.USERID.ToString();
                        break;
                    case "FixedValue":
                        mValueToReplace = mParamAux.ParamFixedValue;
                        break;
                }
                pQuery = pQuery.Replace(mParamAux.ParamName, mValueToReplace);
            }

            return pQuery;
        }

    }
}