﻿using ARGNS.Model.Implementations.SAP;
using ARGNS.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Controllers
{
    public class BranchController : Controller
    {
        // GET: Branch
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public JsonResult ChangeBranch(int idBranchSelect)
        {
            ((UserView)Session["UserLogin"]).CompanyConnect.IdBranchSelect = idBranchSelect;
            var whsDf = "";

            if (Session["Warehouse"] != null)
            {
                var listWhs = ((IEnumerable<Warehouse>)(Session["Warehouse"])).ToList();

                whsDf = (listWhs).Where(c => c.BPLid == idBranchSelect).FirstOrDefault().WhsCode ?? "";
            }

            return Json(new { Result = "Ok", WhsDf = whsDf });
        }
    }
}