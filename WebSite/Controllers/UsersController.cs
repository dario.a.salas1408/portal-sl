﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ARGNS;
using ARGNS.View;
using ARGNS.ManagerView;
using ARGNS.WebSite.Resources.Views.Users;
using ARGNS.WebSite.Resources.Views.Error;
using System.Web.UI.WebControls;
using ARGNS.Util;
using ARGNS.WebSite.Attribute;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Collections;
using DataTables.Mvc;
using AutoMapper;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class UsersController : Controller
    {
        private UserManagerView mUserManagerView;
        private BPGroupManagerView mBPGroupManagerView;
        private UserGroupManagerView mUserGroupManagerView;
        private CompaniesManagerView mCompaniesManagerView;
        private RolesManagerView mRolesManagerView;
        private PageManagerView mPageManagerView;
        private DirectAccessManagerView mDirectAccessManagerView;
        private TileManagerView mTileManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        private UserBranchManagerView userBranchManagerView;


        public UsersController()
        {
            mUserManagerView = new UserManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
            mRolesManagerView = new RolesManagerView();
            mUserGroupManagerView = new UserGroupManagerView();
            mPageManagerView = new PageManagerView();
            mDirectAccessManagerView = new DirectAccessManagerView();
            mTileManagerView = new TileManagerView();
            mBPGroupManagerView = new BPGroupManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
            userBranchManagerView = new UserBranchManagerView();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.WEBPORTAL, Enums.Pages.ABMUsers)]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ListUsers([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.GetSortedColumns().ToList();

            JsonObjectResult mJsonObjectResult = mUserManagerView.GetUsers(
                requestModel.Start, requestModel.Length, searchViewModel.pCodeVendor);

            mapper = new MapperConfiguration(cfg => { cfg.CreateMap<User, UserView>(); })
                .CreateMapper();

            return Json(new DataTablesResponse(requestModel.Draw,
                    mapper.Map<List<UserView>>(mJsonObjectResult.UserList),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                    Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                    JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionUsers"></param>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult ActionUsers(string ActionUsers, int IdUser)
        {
            UserView mUserView = null;

            switch (ActionUsers)
            {
                case "Add":
                    ViewBag.Tite = Users.AU;
                    ViewBag.FormMode = ActionUsers;
                    mUserView = mUserManagerView.EmptyViewUser(((UserView)Session["UserLogin"]).CompanyConnect);
                    break;

                case "Update":
                    ViewBag.Tite = Users.UU;
                    ViewBag.FormMode = ActionUsers;
                    mUserView = mUserManagerView.GetUser(((UserView)Session["UserLogin"]).CompanyConnect, IdUser);
                    break;

                case "Delete":
                    ViewBag.Tite = Users.DU;
                    ViewBag.FormMode = ActionUsers;
                    mUserView = mUserManagerView.GetUser(((UserView)Session["UserLogin"]).CompanyConnect, IdUser);
                    break;

            }

            return View("_Users", mUserView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(UserView model)
        {
            if (mUserManagerView.ValidateExistingUser(model.Name))
            {
                return Json(Users.ErrorUserExists);
            }
            else
            {
                if (mUserManagerView.Add(model))
                { return Json("Ok"); }
                else
                { return Json(@Error.WebServError); }
            }
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
        public ActionResult DuplicateUser(UserView model)
        {
            if (model.Name == null || model.Password == null)
            {
                return Content(Users.NotEmpty);
            }

            if (model.Name.Equals("") || model.Password.Equals(""))
            {
                return Content(Users.NotEmpty);
            }

            if (mUserManagerView.ValidateExistingUser(model.Name))
            {
                return Content(Users.ErrorUserExists);
            }
            else
            {
                if (mUserManagerView.Duplicate(model))
                {
                    { return Json(Users.UserDuplicatedSuc); }
                }
                else
                {
                    return Content(Error.WebServError);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(UserView model)
        {
            if (mUserManagerView.Update(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(UserView model)
        {
            if (mUserManagerView.Delete(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult CompaniesUser(int IdUser)
        {
            ViewBag.Tite = Users.UCTCU;

            UserView mUserView = mUserManagerView.GetUser(((UserView)Session["UserLogin"]).CompanyConnect, IdUser);

            int[] ids = mUserView.Companies.Select(c => c.IdCompany).ToArray();

            List<CompanyView> mListCom = mCompaniesManagerView.GetAllCompaniesActives().Where(c => !ids.Contains(c.IdCompany)).ToList();

            TempData["Companies"] = mListCom;

            if (mListCom.Count > 0)
            {

                CompanyView mCompanyViewFs = mListCom.FirstOrDefault();

                mUserView.ListUserSap = mUserManagerView.GetUserSap(mCompanyViewFs);

                if (mUserView.ListUserSap.Count == 0)
                {
                    ViewBag.Error = "Error: 401";
                    ViewBag.CompanyError = mListCom.FirstOrDefault().CompanyDB;
                    //return View("Index");
                }

                mUserView.ListWarehouse.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListWarehouse.AddRange(mUserManagerView.GetWarehouseList(mCompanyViewFs));

                mUserView.ListSalesTaxCode.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListSalesTaxCode.AddRange(mUserManagerView.GetSalesTaxCode(mCompanyViewFs));

                mUserView.ListPurchaseTaxCode.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListPurchaseTaxCode.AddRange(mUserManagerView.GetSalesTaxCode(mCompanyViewFs));

                mUserView.ListDistributionRule.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListDistributionRule = mUserManagerView.GetDistributionRuleList(mCompanyViewFs);

                mUserView.ListCostPriceList.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListCostPriceList.AddRange(mUserManagerView.GetAllPriceListSAP(mCompanyViewFs));

                mUserView.ListUserGroup.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListUserGroup.AddRange(mUserGroupManagerView.GetUserGroupList(mCompanyViewFs.IdCompany).Select(c => new ListItem { Value = c.Id.ToString(), Text = c.Name }).ToList());

                mUserView.ListSalesEmployeeSap.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListSalesEmployeeSap.AddRange(mUserManagerView.GetSalesEmployeeList(mCompanyViewFs));

                mUserView.ListBPGroup.Add(new ListItem() { Value = "", Text = "" });
                mUserView.ListBPGroup.AddRange(mBPGroupManagerView.GetBPGroupList(mCompanyViewFs.IdCompany).Select(c => new ListItem { Value = c.Id.ToString(), Text = c.Name }).ToList());
            }

            List<UsersSettingView> userSettingsList = mCompaniesManagerView.GetUserSettings();
            foreach (CompanyView company in mUserView.Companies)
            {
                company.userSettings = userSettingsList.Where(c => (c.IdUser == mUserView.IdUser) && (c.IdCompany == company.IdCompany)).FirstOrDefault();

                company.ListUserGroup.Add(new ListItem() { Value = "", Text = "" });
                company.ListUserGroup.AddRange(mUserGroupManagerView.GetUserGroupList(company.IdCompany).Select(c => new ListItem { Value = c.Id.ToString(), Text = c.Name }).ToList());
                company.ListBusinessPartnerSap = mBusinessPartnerManagerView.GetBusinessPartnerListSearch(company, company.userSettings.IdBp).Select(c => new ListItem { Value = c.CardCode.ToString(), Text = c.CardName }).ToList();
                company.ListSalesEmployeeSap = mUserManagerView.GetSalesEmployeeList(company);
            }

            ViewBag.CompAv = mListCom;

            ViewBag.CountCompAv = ((List<CompanyView>)ViewBag.CompAv).Count();

            return View("_UserCompanies", mUserView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult UDFUser(int IdUser)
        {
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            ViewBag.Tite = Users.UUDF;

            UserView mUserView = mUserManagerView.GetUser(((UserView)Session["UserLogin"]).CompanyConnect, IdUser);
            //int[] ids = mUserView.Companies.Select(c => c.IdCompany).ToArray();
            List<CompanyView> ListCom = mCompaniesManagerView.GetAllCompaniesActives().ToList();
            List<UserUDFView> mListUDF = new List<UserUDFView>();
            List<SAPObjectView> mListSAPObject = new List<SAPObjectView>();
            mListSAPObject.Add(new SAPObjectView(-1, "Select an Object", "", true));
            mListSAPObject.AddRange(mUserManagerView.GetSAPObjects());
            TempData["Companies"] = ListCom;
            if (ListCom.Count > 0)
            {
                CompanyView mCompanyViewFs = ListCom.FirstOrDefault();
                mListUDF = mUserManagerView.GetUserUDF(IdUser);
            }
            ViewBag.CompAv = ListCom;
            ViewBag.CountCompAv = ListCom.Count();

            return View("_UserUDF", Tuple.Create(mUserView, mListUDF, mListSAPObject));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddCompany(UserView model)
        {
            if (mUserManagerView.AddCompanyToUser(model.IdUser, model.IdSelector,
                model.UserNameSAP, model.UserPasswordSAP, model.UserCodeSAP,
                model.BPCode, model.SECode, model.DftDistRule, model.DftWhs,
                model.SalesTaxCodeDef, model.PurchaseTaxCodeDef, model.UserGroupId,
                model.CostPriceList, model.BPGroupId))

            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteCompany(UserView model)
        {
            if (mUserManagerView.DeleteCompanyToUser(model.IdUser, model.IdSelector))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUserSap(int CompanyId)
        {

            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];

            CompanyView mCompanyView = mListCom.Where(c => c.IdCompany == CompanyId).FirstOrDefault();

            List<ListItem> mList = mUserManagerView.GetUserSap(mCompanyView);

            TempData["Companies"] = mListCom;

            return Json(mList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdRol"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetRolSetting(int IdRol)
        {
            RoleView mRol = mRolesManagerView.GetRol(IdRol);

            return Json(mRol, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <param name="idUSer"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ValidateConnection(int pCompanyId, int idUSer)
        {
            if (mCompaniesManagerView.ValidateConnection(pCompanyId, idUSer))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult MyAccount(int IdUser)
        {
            ViewBag.Tite = Users.MyAccount;

            UserView mUserView = mUserManagerView.GetUser(((UserView)Session["UserLogin"]).CompanyConnect, IdUser);

            return View("_MyAccount", mUserView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateMyAccount(UserView model)
        {
            UserView mUserView = mUserManagerView.GetUser(((UserView)Session["UserLogin"]).CompanyConnect, model.IdUser);

            if (((UserView)Session["UserLogin"]).IdUser == model.IdUser &&
                !string.IsNullOrEmpty(model.Name) &&
                model.Password == mUserView.Password &&
                model.NewPassword == model.RepNewPassword)
            {
                model.Password = model.NewPassword;
                if (mUserManagerView.UpdateMyAccount(model))
                { return Json("Ok"); }
                else
                { return Json(@Error.WebServError); }
            }
            else
            {
                return Json(@Error.WebServError);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <returns></returns>
        public ActionResult MyDirectAccesses(int IdUser)
        {
            ViewBag.Tite = Users.MyAccount;

            MyDirectAccessesObjectView mMyDirectAccessesObjectView = new MyDirectAccessesObjectView();
            mMyDirectAccessesObjectView.ListDirectAccessView = mDirectAccessManagerView.GetDirectAccessList();

            List<PortalTileView> mPortalTileList = mTileManagerView.GetPortalTileList((
                (UserView)Session["UserLogin"]).CompanyConnect.IdCompany);

            //List<PortalTileTypeView> mPortalTileTypeList = mTileManagerView.GetPortalTileTypeList(((UserView)Session["UserLogin"]).CompanyConnect);
            foreach (PortalTileView mTile in mPortalTileList)
            {
                DirectAccessView mDirectAccessAux = new DirectAccessView();
                mDirectAccessAux.Area = -1;
                mDirectAccessAux.IdTile = mTile.IdTile;
                mDirectAccessAux.UrlName = mTile.TileName;
                mMyDirectAccessesObjectView.ListDirectAccessView.Add(mDirectAccessAux);
            }

            mMyDirectAccessesObjectView.ListPage = mPageManagerView.GetPageList();
            mMyDirectAccessesObjectView.IdUser = IdUser;
            mMyDirectAccessesObjectView.CompanyConnect = ((UserView)Session["UserLogin"]).CompanyConnect;
            mMyDirectAccessesObjectView.ListUserDirectAccessView = mDirectAccessManagerView.GetUserDirectAccessList(IdUser,
                                                                    ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany);

            Dictionary<string, string> mResourceValues = new Dictionary<string, string>();
            ResourceManager rm = new ResourceManager("ARGNS.WebSite.Resources.Views.DirectAccess.DirectAccess",
                Assembly.GetExecutingAssembly());

            mResourceValues = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true).Cast<DictionaryEntry>()
                                .ToDictionary(r => r.Key.ToString(), r => r.Value.ToString());

            foreach (DirectAccessView mDirectAccess in mMyDirectAccessesObjectView.ListDirectAccessView)
            {
                mDirectAccess.TranslatedName = mResourceValues
                    .Where(c => c.Key == mDirectAccess.TranslationKey)
                    .Select(c => c.Value)
                    .FirstOrDefault();
            }

            return View("_MyDirectAccesses", mMyDirectAccessesObjectView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pListDirectAccesses"></param>
        /// <param name="pListDirectAccessesCustom"></param>
        /// <param name="pIdUser"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult UpdateMyDirectAccesses(List<int> pListDirectAccesses, List<int> pListDirectAccessesCustom, int pIdUser)
        {
            if (mDirectAccessManagerView.UpdateMyDirectAccesses(pListDirectAccesses,
                pListDirectAccessesCustom, pIdUser,
                ((UserView)Session["UserLogin"]).CompanyConnect.IdCompany))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetWHS(int pCompanyId)
        {
            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];

            List<ListItem> mReturnWhs = mUserManagerView.GetWarehouseList(
                mListCom.Where(c => c.IdCompany == pCompanyId)
                .FirstOrDefault());

            TempData["Companies"] = mListCom;

            return Json(mReturnWhs, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetDtR(int pCompanyId)
        {

            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];

            List<ListItem> mReturnWhs = mUserManagerView.GetDistributionRuleList(
                mListCom.Where(c => c.IdCompany == pCompanyId)
                .FirstOrDefault());

            TempData["Companies"] = mListCom;

            return Json(mReturnWhs, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <param name="pTableID"></param>
        /// <param name="pIdUser"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUDFByTableID(int pCompanyId, string pTableID, int pIdUser, string pPageKey)
        {
            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];

            string[] mUDFng = mUserManagerView.GetUserUDF(pIdUser)
                .Where(c => c.IdCompany == pCompanyId && c.Document == pTableID)
                .ToList().Select(c => c.FieldID.ToString())
                .ToArray();

            List<UDF_SAPView> mUDFList = mUserManagerView.GetUDFByTableID(
                mListCom.Where(c => c.IdCompany == pCompanyId).FirstOrDefault(), pTableID);

            List<ListItem> mReturnWhs = mUDFList.Select(c => new ListItem { Value = c.FieldID.ToString(), Text = c.Descr }).ToList();

            TempData["Companies"] = mListCom;
            TempData["UDF_SAPView" + pPageKey] = mUDFList;

            return Json(mReturnWhs.Where(c => !mUDFng.Contains(c.Value)).ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUserID"></param>
        /// <param name="pCompanyId"></param>
        /// <param name="pTableID"></param>
        /// <param name="pFieldID"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _AddUDF(int pUserID, int pCompanyId, string pTableID, int pFieldID, string pPageKey)
        {
            try
            {
                List<CompanyView> ListCom = (List<CompanyView>)TempData["Companies"];
                List<UDF_SAPView> mUDFList = (List<UDF_SAPView>)TempData["UDF_SAPView" + pPageKey];
                List<UserUDFView> mListUserUDF = new List<UserUDFView>();
                UDF_SAPView mUDFAux = mUDFList.Where(c => c.FieldID == pFieldID).FirstOrDefault();
                TempData["Companies"] = ListCom;
                TempData["UDF_SAPView" + pPageKey] = mUDFList;
                mUserManagerView.AddUDF(pUserID, pCompanyId,
                    pTableID, "U_" + mUDFAux.AliasID,
                    pFieldID, mUDFAux.TypeID,
                    mUDFAux.Descr, mUDFAux.RTable);

                mListUserUDF = mUserManagerView.GetUserUDF(pUserID);
                List<SAPObjectView> mListSAPObject = new List<SAPObjectView>();
                mListSAPObject = mUserManagerView.GetSAPObjects();

                return PartialView("_UDFTable", Tuple.Create(ListCom, mListUserUDF, mListSAPObject));
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUser"></param>
        /// <param name="IdUDF"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _DeleteUDF(int IdUser, int IdUDF)
        {
            try
            {
                List<CompanyView> ListCom = (List<CompanyView>)TempData["Companies"];
                List<UserUDFView> mListUserUDF = new List<UserUDFView>();
                TempData["Companies"] = ListCom;
                mUserManagerView.DeleteUDF(IdUser, IdUDF);

                mListUserUDF = mUserManagerView.GetUserUDF(IdUser);
                List<SAPObjectView> mListSAPObject = new List<SAPObjectView>();
                mListSAPObject = mUserManagerView.GetSAPObjects();

                return PartialView("_UDFTable", Tuple.Create(ListCom, mListUserUDF, mListSAPObject));
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBP(int pCompanyId)
        {
            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];
            List<ListItem> mReturnWhs = mUserManagerView.GetBusinessPartnerList(
                mListCom.Where(c => c.IdCompany == pCompanyId)
                .FirstOrDefault());

            TempData["Companies"] = mListCom;

            return Json(mReturnWhs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSalesEmployee(int pCompanyId)
        {
            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];
            List<ListItem> mReturnWhs = mUserManagerView.GetSalesEmployeeList(
                mListCom.Where(c => c.IdCompany == pCompanyId)
                .FirstOrDefault());
            TempData["Companies"] = mListCom;

            return Json(mReturnWhs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBPGroupList(int pCompanyId)
        {
            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];

            List<ListItem> mReturnWhs = new List<System.Web.UI.WebControls.ListItem>();
            mReturnWhs.Add(new ListItem() { Value = "", Text = "" });
            mReturnWhs.AddRange(mBPGroupManagerView.GetBPGroupList(pCompanyId).Select(c => new ListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name
            }).ToList());

            TempData["Companies"] = mListCom;

            return Json(mReturnWhs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetSalesTaxCode(int pCompanyId)
        {
            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];
            List<ListItem> mReturnWhs = new List<ListItem>();

            mReturnWhs.Add(new ListItem() { Value = "", Text = "" });
            mReturnWhs.AddRange(mUserManagerView.GetSalesTaxCode(mListCom.Where(c => c.IdCompany == pCompanyId).FirstOrDefault()));
            TempData["Companies"] = mListCom;

            return Json(mReturnWhs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pAlertCode"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetAlertById(int pAlertCode)
        {
            AlertView mAlert = mUserManagerView.GetAlertById(((UserView)Session["UserLogin"]).CompanyConnect, pAlertCode);
            return PartialView("_AlertTable", mAlert);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAllPriceList(int pCompanyId)
        {
            List<CompanyView> ListCom = (List<CompanyView>)TempData["Companies"];
            List<ListItem> mReturnWhs = new List<ListItem>();

            mReturnWhs.Add(new ListItem() { Value = "", Text = "" });
            mReturnWhs.AddRange(mUserManagerView.GetAllPriceListSAP(ListCom.Where(c => c.IdCompany == pCompanyId).FirstOrDefault()));
            TempData["Companies"] = ListCom;

            return Json(mReturnWhs, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetUserGroupByCompany(int pCompanyId)
        {
            List<CompanyView> mListCom = (List<CompanyView>)TempData["Companies"];
            List<ListItem> mReturnList = new List<ListItem>();

            mReturnList.Add(new ListItem() { Value = "", Text = "" });
            mReturnList.AddRange(mUserGroupManagerView.GetUserGroupList(
                mListCom.Where(c => c.IdCompany == pCompanyId).FirstOrDefault().IdCompany)
                .Select(c => new ListItem { Value = c.Id.ToString(), Text = c.Name })
                .ToList());

            TempData["Companies"] = mListCom;

            return Json(mReturnList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _BPList([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,
            AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Column, OrderColumn>();
                cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>();
            }).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            searchViewModel.pCardCode = searchViewModel.pCardCode.TrimStart().TrimEnd();
            searchViewModel.pCardName = searchViewModel.pCardName.TrimStart().TrimEnd();
            CompanyView mCompany = mCompaniesManagerView.GetCompany(searchViewModel.pIdCompany);

            mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(mCompany,
                Enums.BpType.All, true, searchViewModel.pCardCode, searchViewModel.pCardName,
                false, null, null, true, requestModel.Start, requestModel.Length,
                mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            return Json(new DataTablesResponse(requestModel.Draw,
                mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value),
                Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)),
                JsonRequestBehavior.AllowGet);
        }


        public ActionResult UserBranch(int IdUser, int IdCompany = 0)
        {
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;
            ViewBag.Tite = "User Branches";

            UserView mUserView = mUserManagerView.GetUser(((UserView)Session["UserLogin"]).CompanyConnect, IdUser);

            int[] ids = mUserView.Companies.Select(c => c.IdCompany).ToArray();

            List<CompanyView> ListCom = mCompaniesManagerView.GetAllCompaniesActives().Where(c => ids.Contains(c.IdCompany)).ToList();

            TempData["Companies" + mPageKey] = ListCom;


            List<BranchesSAP> listBranchSAP = new List<BranchesSAP>();
            List<UserBranchView> userBranchViews = new List<UserBranchView>();

            if (ListCom.Count > 0)
            {
                CompanyView mCompanyViewFs = ListCom.Where(c => (IdCompany == 0 ? true : c.IdCompany == IdCompany)).FirstOrDefault();

                if(IdCompany != 0)
                {
                    mUserView.IdSelector = IdCompany;
                }

                mCompanyViewFs.IdUserConected = mUserView.IdUser;

                listBranchSAP = userBranchManagerView.GetBranchesByUser(mCompanyViewFs, mCompanyViewFs.UserName);
                userBranchViews = userBranchManagerView.GetUserBranchList(IdUser, mCompanyViewFs.IdCompany);
                var idbranchesuser = userBranchViews.Select(c => c.IdBranchSAP).ToArray();
                listBranchSAP = listBranchSAP.Where(c => !idbranchesuser.Contains(c.BPLId)).ToList();

            }

            TempData["BranchesSAP" + mPageKey] = listBranchSAP;
            ViewBag.CompAv = ListCom;
            ViewBag.countBranchesAvailable = listBranchSAP.Count();
            ViewBag.Branches = listBranchSAP;

            return View("_UserBranch", Tuple.Create(mUserView, userBranchViews));
        }

        [HttpPost]
        public ActionResult _AddBranch(int pUserID, int pCompanyId, int pIdBranch, string pPageKey)
        {
            try
            {
                List<CompanyView> ListCom = (List<CompanyView>)TempData["Companies" + pPageKey];
                List<UserBranchView> userBranchViews = new List<UserBranchView>();
                List<BranchesSAP> listBranchSAP = (List<BranchesSAP>)TempData["BranchesSAP" + pPageKey];


                var branch = listBranchSAP.Where(c => c.BPLId == pIdBranch).FirstOrDefault();

                TempData["Companies" + pPageKey] = ListCom;
                TempData["BranchesSAP" + pPageKey] = listBranchSAP;

                var newBranch = new UserBranchView() { IdUser = pUserID, BranchName = branch.BPLName, IdBranchSAP = branch.BPLId, IdCompany = pCompanyId };

                userBranchManagerView.Add(newBranch);

                userBranchViews = userBranchManagerView.GetUserBranchList(pUserID, pCompanyId);

                return PartialView("_BranchesTable", userBranchViews);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public ActionResult _DeleteBranch(int pUserID, int pCompanyId, int idUserBranch, string pPageKey)
        {
            try
            {
                List<CompanyView> ListCom = (List<CompanyView>)TempData["Companies" + pPageKey];
                List<UserBranchView> userBranchViews = new List<UserBranchView>();
                List<BranchesSAP> listBranchSAP = (List<BranchesSAP>)TempData["BranchesSAP" + pPageKey];


                TempData["Companies" + pPageKey] = ListCom;
                TempData["BranchesSAP" + pPageKey] = listBranchSAP;

                userBranchManagerView.Delete(idUserBranch);

                userBranchViews = userBranchManagerView.GetUserBranchList(pUserID, pCompanyId);

                return PartialView("_BranchesTable", userBranchViews);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

        [HttpPost]
        public JsonResult GetBranchesAvailable(int pUserID, int pCompanyId, string pPageKey)
        {
            try
            {
                List<CompanyView> ListCom = (List<CompanyView>)TempData["Companies" + pPageKey];
                List<UserBranchView> userBranchViews = new List<UserBranchView>();
                List<BranchesSAP> listBranchSAP = (List<BranchesSAP>)TempData["BranchesSAP" + pPageKey];

                TempData["Companies" + pPageKey] = ListCom;
                TempData["BranchesSAP" + pPageKey] = listBranchSAP;

                userBranchViews = userBranchManagerView.GetUserBranchList(pUserID, pCompanyId);

                var idbranchesuser = userBranchViews.Select(c => c.IdBranchSAP).ToArray();
                listBranchSAP = listBranchSAP.Where(c => !idbranchesuser.Contains(c.BPLId)).ToList();

                return Json(listBranchSAP);
            }
            catch (Exception ex)
            {
                return Json(@Error.WebServError);
            }
        }

    }
}