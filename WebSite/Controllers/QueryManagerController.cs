﻿using ARGNS.ManagerView;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.QueryManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class QueryManagerController : Controller
    {
        private QueryManagerManagerView mQueryManagerManagerView;
        private CompaniesManagerView mCompaniesManagerView;

        public QueryManagerController()
        {
            mQueryManagerManagerView = new QueryManagerManagerView();
            mCompaniesManagerView = new CompaniesManagerView();
        }

        [CustomerAuthorize(Enums.Areas.WEBPORTAL, Enums.Pages.ABMQuery)]
        public ActionResult Index()
        {
            return View(mQueryManagerManagerView.GetListQueryByCompany(((UserView)Session["UserLogin"]).CompanyConnect.IdCompany));
        }

        [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
        public ActionResult ActionQueryManager(string ActionQueryManager, int IdQuery)
        {
            QueryManagerItemView mQueryManagerItemView = null;
            List<CompanyView> mListCompanies = mCompaniesManagerView.GetAllCompaniesActives();

            switch (ActionQueryManager)
            {
                case "Add":
                    ViewBag.Tite = QueryManager.AQ;
                    ViewBag.FormMode = ActionQueryManager;
                    mQueryManagerItemView = new QueryManagerItemView();
                    break;

                case "Update":
                    ViewBag.Tite = QueryManager.UQ;
                    ViewBag.FormMode = ActionQueryManager;
                    mQueryManagerItemView = mQueryManagerManagerView.GetQuery(IdQuery);
                    break;

                case "Delete":
                    ViewBag.Tite = QueryManager.DQ;
                    ViewBag.FormMode = ActionQueryManager;
                    mQueryManagerItemView = mQueryManagerManagerView.GetQuery(IdQuery);
                    break;
                    
            }

            return View("_Query", Tuple.Create(mQueryManagerItemView, mListCompanies));
        }

        [HttpPost]
        public JsonResult Add(QueryManagerItemView model)
        {
            if (mQueryManagerManagerView.Add(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        [HttpPost]
        public JsonResult Update(QueryManagerItemView model)
        {
            if (mQueryManagerManagerView.Update(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        [HttpPost]
        public JsonResult Delete(QueryManagerItemView model)
        {
            if (mQueryManagerManagerView.Delete(model))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        
        [HttpPost]
        public JsonResult GetQueryResult(QueryObjectView pQueryObject)
        {
            bool _ExecQuery = ((UserView)Session["UserLogin"]).ListUserPageAction.Where(c => c.PageInternalKey == Enums.Pages.ABMQuery.ToDescriptionString() && c.InternalKey == Enums.Actions.ExecuteQuery.ToDescriptionString()).FirstOrDefault() != null ? true : false;
            string mQueryResult = string.Empty;
            if(_ExecQuery)
                mQueryResult = mQueryManagerManagerView.GetQueryResult(((UserView)Session["UserLogin"]).CompanyConnect, pQueryObject.pQueryIdentifier, pQueryObject.pQueryParams);

            if (mQueryResult != null)
            {
                return Json(mQueryResult, JsonRequestBehavior.AllowGet);
            }
            else
            { return Json(@Error.WebServError, JsonRequestBehavior.AllowGet); }
        }
    }
}