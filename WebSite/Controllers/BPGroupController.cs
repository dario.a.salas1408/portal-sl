﻿using ARGNS.ManagerView;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using ARGNS.View;
using ARGNS.WebSite.Attribute;
using ARGNS.WebSite.Resources.Views.Companies;
using ARGNS.WebSite.Resources.Views.Error;
using ARGNS.WebSite.Resources.Views.Users;
using AutoMapper;
using DataTables.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ARGNS.WebSite.Controllers
{
    [CustomerAuthorize(Enums.Areas.WEBPORTAL)]
    public class BPGroupController : Controller
    {
        private BPGroupManagerView mBPGroupManagerView;
        private CompaniesManagerView mCompanyManagerView;
        private BusinessPartnerManagerView mBusinessPartnerManagerView;
        public BPGroupController()
        {
            mBPGroupManagerView = new BPGroupManagerView();
            mCompanyManagerView = new CompaniesManagerView();
            mBusinessPartnerManagerView = new BusinessPartnerManagerView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        [CustomerAuthorize(Enums.Areas.WEBPORTAL, Enums.Pages.ABMCompanies, Enums.Actions.Add)]
        public ActionResult Index(int IdCompany)
        {
            List<BPGroupView> mListBPGroup = mBPGroupManagerView.GetBPGroupList(IdCompany);
            CompanyView mCompany = mCompanyManagerView.GetCompany(IdCompany);
            ViewBag.Tite = Companies.BG;
            return View(Tuple.Create(mListBPGroup, mCompany));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ActionBPGroup"></param>
        /// <param name="IdBPGroup"></param>
        /// <param name="IdCompany"></param>
        /// <returns></returns>
        public ActionResult ActionBPGroup(string ActionBPGroup, int IdBPGroup, int IdCompany)
        {
            BPGroupView mBPGroupView = null;
            string mPageKey = PageKeyGenerator.GetPageKey();
            ViewBag.PageKey = mPageKey;

            switch (ActionBPGroup)
            {
                case "Add":
                    ViewBag.Tite = Companies.ABG;
                    ViewBag.FormMode = ActionBPGroup;
                    mBPGroupView = new BPGroupView();
                    mBPGroupView.IdCompany = IdCompany;
                    break;

                case "Edit":
                    ViewBag.Tite = Companies.UBG;
                    ViewBag.FormMode = ActionBPGroup;
                    mBPGroupView = mBPGroupManagerView.GetBPGroup(IdBPGroup);
                    break;
            }
            TempData["BPGroupView" + mPageKey] = mBPGroupView;

            return View("_BPGroup", mBPGroupView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestModel"></param>
        /// <param name="searchViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _BPListModal([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, AdvancedSearchDocumentView searchViewModel)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<Column, OrderColumn>(); cfg.CreateMap<BusinessPartnerSAP, BusinessPartnerView>();}).CreateMapper();

            List<Column> sortedColumns = requestModel.Columns.Where(c => c.IsOrdered == true).ToList();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            BPGroupView mBPGroupAux = (BPGroupView)TempData["BPGroupView" + searchViewModel.pPageKey];
            TempData["BPGroupView" + searchViewModel.pPageKey] = mBPGroupAux;

            searchViewModel.pCardCode = searchViewModel.pCardCode.TrimStart().TrimEnd();
            searchViewModel.pCardName = searchViewModel.pCardName.TrimStart().TrimEnd();
            CompanyView mCompany = mCompanyManagerView.GetCompany(searchViewModel.pIdCompany);

            mJsonObjectResult = mBusinessPartnerManagerView.GetBusinessPartnerListByUser(mCompany,Enums.BpType.All, false, 
                searchViewModel.pCardCode, searchViewModel.pCardName, false, null, null, true,
                requestModel.Start, requestModel.Length, 
                mapper.Map<OrderColumn>(sortedColumns.FirstOrDefault()));

            List<BusinessPartnerSAP> mBPListAux = mJsonObjectResult.BusinessPartnerSAPList
                .Where(c => mBPGroupAux.BPGroupLines.Select(j => j.CardCode)
                .Contains(c.CardCode)).ToList();

            foreach(BusinessPartnerSAP mBPAux in mBPListAux)
            {
                mBPAux.BPGroupSelected = true;
            }

            //TempData["ListBP" + pPageKey] = ListReturn;

            return Json(new DataTablesResponse(requestModel.Draw, mapper.Map<List<BusinessPartnerView>>(mJsonObjectResult.BusinessPartnerSAPList), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value), Convert.ToInt32(mJsonObjectResult.Others.Where(c => c.Key == "TotalRecords").FirstOrDefault().Value)), JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public PartialViewResult _BPListModal(int IdCompany, string pPageKey, string pBPCode = "", string pBPName = "")
        //{
        //    BPGroupView mBPGroupAux = (BPGroupView)TempData["BPGroupView" + pPageKey];
        //    TempData["BPGroupView" + pPageKey] = mBPGroupAux;

        //    List<BusinessPartnerView> ListReturn;
        //    pBPCode = pBPCode.TrimStart().TrimEnd();
        //    pBPName = pBPName.TrimStart().TrimEnd();
        //    CompanyView mCompany = mCompanyManagerView.GetCompany(IdCompany);

        //    ListReturn = mBusinessPartnerManagerView.GetBusinessPartnerListSearch(mCompany, pBPCode, pBPName);
        //    ListReturn = ListReturn.Where(c => !mBPGroupAux.BPGroupLines.Select(j => j.CardCode).Contains(c.CardCode)).ToList();
        //    TempData["ListBP" + pPageKey] = ListReturn;

        //    return PartialView("_BPListModal", ListReturn);
        //}

            /// <summary>
            /// 
            /// </summary>
            /// <param name="pAdvancedSearchDocumentView"></param>
            /// <returns></returns>
        [HttpPost]
        public PartialViewResult AddBPGroupLine(AdvancedSearchDocumentView pAdvancedSearchDocumentView)
        {
            List<BPGroupLineView> mListBPGroupLine = new List<BPGroupLineView>();
            //List<BusinessPartnerView> ListBPAux = (List<BusinessPartnerView>)TempData["ListBP" + pPageKey];
            BPGroupView mBPGroupAux = (BPGroupView)TempData["BPGroupView" + pAdvancedSearchDocumentView.pPageKey];
            //TempData["ListBP" + pPageKey] = ListBPAux;

            foreach(BusinessPartnerView mBP in pAdvancedSearchDocumentView.pBPList)
            {
                BPGroupLineView mBPGroupLineViewAux = new BPGroupLineView();
                //BusinessPartnerView mBusinessPartnerViewAux = ListBPAux.Where(c => c.CardCode == mCardCode).FirstOrDefault();
                mBPGroupLineViewAux.CardCode = mBP.CardCode;
                mBPGroupLineViewAux.CardName = mBP.CardName;
                mListBPGroupLine.Add(mBPGroupLineViewAux);
            }

            mBPGroupAux.BPGroupLines.AddRange(mListBPGroupLine);
            TempData["BPGroupView" + pAdvancedSearchDocumentView.pPageKey] = mBPGroupAux;

            return PartialView("_BPList", mBPGroupAux.BPGroupLines);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCardCode"></param>
        /// <param name="pPageKey"></param>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult DeleteBP(string pCardCode, string pPageKey)
        {
            BPGroupView mBPGroupAux = (BPGroupView)TempData["BPGroupView" + pPageKey];

            mBPGroupAux.BPGroupLines = mBPGroupAux.BPGroupLines.Where(c => c.CardCode != pCardCode).ToList();
            TempData["BPGroupView" + pPageKey] = mBPGroupAux;

            return PartialView("_BPList", mBPGroupAux.BPGroupLines);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPGroup"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Add(BPGroupView pBPGroup)
        {
            if (mBPGroupManagerView.Add(pBPGroup))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPGroup"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Update(BPGroupView pBPGroup)
        {
            if (mBPGroupManagerView.Update(pBPGroup))
            { return Json("Ok"); }
            else
            { return Json(@Error.WebServError); }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public PartialViewResult GetBPModel()
        {
            return PartialView("_BPListModal");
        }
    }
}