﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebServices.Model.Tables;

namespace WebServices.Model
{
    public partial class DataBase : DbContext
    {
        bool mApparelInstall = false;

        public DataBase()
        {
           
        }

        public DataBase(bool pApparelInstall = false)
        {
            
            mApparelInstall = pApparelInstall;
        }

        public virtual DbSet<OHEM> OHEM { get; set; }
        public virtual DbSet<OITW> OITW { get; set; }
        public virtual DbSet<OPOR> OPOR { get; set; }
        public virtual DbSet<POR1> POR1 { get; set; }
        public virtual DbSet<OCRN> OCRN { get; set; }
        public virtual DbSet<OCTG> OCTG { get; set; }
        public virtual DbSet<OPYM> OPYM { get; set; }
        public virtual DbSet<OSHP> OSHP { get; set; }
        public virtual DbSet<OSLP> OSLP { get; set; }
        public virtual DbSet<OVTG> OVTG { get; set; }
        public virtual DbSet<OCPR> OCPR { get; set; }
        public virtual DbSet<OCRD> OCRD { get; set; }
        public virtual DbSet<OCLG> OCLG { get; set; }
        public virtual DbSet<OCLT> OCLT { get; set; }
        public virtual DbSet<OUSR> OUSR { get; set; }
        public virtual DbSet<OPQT> OPQT { get; set; }
        public virtual DbSet<PQT1> PQT1 { get; set; }
        public virtual DbSet<ORDR> ORDR { get; set; }
        public virtual DbSet<RDR1> RDR1 { get; set; }
        public virtual DbSet<C_ARGNS_BRAND> C_ARGNS_BRAND { get; set; }
        public virtual DbSet<OPLN> OPLN { get; set; }
        public virtual DbSet<OITB> OITB { get; set; }
        public virtual DbSet<C_ARGNS_YEAR> C_ARGNS_YEAR { get; set; }
        public virtual DbSet<C_ARGNS_VARIABLE> C_ARGNS_VARIABLE { get; set; }
        public virtual DbSet<C_ARGNS_STYLE_STATUS> C_ARGNS_STYLE_STATUS { get; set; }
        public virtual DbSet<C_ARGNS_SIZE> C_ARGNS_SIZE { get; set; }
        public virtual DbSet<C_ARGNS_SEASON> C_ARGNS_SEASON { get; set; }
        public virtual DbSet<C_ARGNS_SCALE> C_ARGNS_SCALE { get; set; }
        public virtual DbSet<C_ARGNS_PRODLINE> C_ARGNS_PRODLINE { get; set; }
        public virtual DbSet<C_ARGNS_MODELGRP> C_ARGNS_MODELGRP { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_VAR> C_ARGNS_MODEL_VAR { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_SIZE> C_ARGNS_MODEL_SIZE { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_COLOR> C_ARGNS_MODEL_COLOR { get; set; }
        public virtual DbSet<C_ARGNS_MODEL> C_ARGNS_MODEL { get; set; }
        public virtual DbSet<C_ARGNS_DIV> C_ARGNS_DIV { get; set; }
        public virtual DbSet<C_ARGNS_COLOR> C_ARGNS_COLOR { get; set; }
        public virtual DbSet<C_ARGNS_COLLECTION> C_ARGNS_COLLECTION { get; set; }
        public virtual DbSet<C_ARGNS_AMBIENT> C_ARGNS_AMBIENT { get; set; }
        public virtual DbSet<C_ARGNS_COMPOSITION> C_ARGNS_COMPOSITION { get; set; }
        public virtual DbSet<C_ARGNS_SIMBOLS> C_ARGNS_SIMBOLS { get; set; }
        public virtual DbSet<OITM> OITM { get; set; }
        public virtual DbSet<OQUT> OQUT { get; set; }
        public virtual DbSet<QUT1> QUT1 { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_IMG> C_ARGNS_MODEL_IMG { get; set; }
        public virtual DbSet<C_ARGNS_APPTEXGRPS> C_ARGNS_APPTEXGRPS { get; set; }
        public virtual DbSet<OADM> OADM { get; set; }       
        public virtual DbSet<C_ARGNS_MODEL_FILE> C_ARGNS_MODEL_FILE { get; set; }
        public virtual DbSet<C_ARGNS_APRTEX_SETUP> C_ARGNS_APRTEX_SETUP { get; set; }
        public virtual DbSet<C_ARGNS_CRPATH> C_ARGNS_CRPATH { get; set; }
        public virtual DbSet<C_ARGNS_CRPATHACT> C_ARGNS_CRPATHACT { get; set; }
        public virtual DbSet<C_ARGNS_COST_SHEET> C_ARGNS_COST_SHEET { get; set; }
        public virtual DbSet<C_ARGNS_CS_MATERIALS> C_ARGNS_CS_MATERIALS { get; set; }
        public virtual DbSet<C_ARGNS_CS_OPERATIONS> C_ARGNS_CS_OPERATIONS { get; set; }
        public virtual DbSet<C_ARGNS_CS_SCHEMAS> C_ARGNS_CS_SCHEMAS { get; set; }
        public virtual DbSet<C_ARGNS_COMMLOGMODEL> C_ARGNS_COMMLOGMODEL { get; set; }
        public virtual DbSet<C_ARGNS_MODELPOM> C_ARGNS_MODELPOM { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_BOM> C_ARGNS_MODEL_BOM { get; set; }
        public virtual DbSet<C_ARGNS_MODELWFLOW> C_ARGNS_MODELWFLOW { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_INST> C_ARGNS_MODEL_INST { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_FITT> C_ARGNS_MODEL_FITT { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_CS> C_ARGNS_MODEL_CS { get; set; }
        public virtual DbSet<C_ARGNS_MODEL_LOGOS> C_ARGNS_MODEL_LOGOS { get; set; }
        public virtual DbSet<C_ARGNS_LOGO> C_ARGNS_LOGO { get; set; }
        public virtual DbSet<C_ARGNS_DOCNUMBER> C_ARGNS_DOCNUMBER { get; set; }
        public virtual DbSet<C_ARGNS_DOCNUMBER_LIN> C_ARGNS_DOCNUMBER_LIN { get; set; }
        public virtual DbSet<OPRQ> OPRQ { get; set; }
        public virtual DbSet<PRQ1> PRQ1 { get; set; }
        public virtual DbSet<PRQ3> PRQ3 { get; set; }
        public virtual DbSet<OUDP> OUDP { get; set; }
        public virtual DbSet<OUBR> OUBR { get; set; }
        public virtual DbSet<ODRF> ODRF { get; set; }
        public virtual DbSet<DRF1> DRF1 { get; set; }
        public virtual DbSet<OWDD> OWDD { get; set; }
        public virtual DbSet<WDD1> WDD1 { get; set; }
        public virtual DbSet<PQT12> PQT12 { get; set; }
        public virtual DbSet<ADM1> ADM1 { get; set; }
        public virtual DbSet<CRD1> CRD1 { get; set; }
        public virtual DbSet<POR12> POR12 { get; set; }
        public virtual DbSet<QUT12> QUT12 { get; set; }
        public virtual DbSet<RDR12> RDR12 { get; set; }
        public virtual DbSet<OCQG> OCQG { get; set; }
        public virtual DbSet<OCRY> OCRY { get; set; }
        public virtual DbSet<OCST> OCST { get; set; }
        public virtual DbSet<OPCH> OPCH { get; set; }
        public virtual DbSet<PCH1> PCH1 { get; set; }
        public virtual DbSet<PCH12> PCH12 { get; set; }
        public virtual DbSet<OINV> OINV { get; set; }
        public virtual DbSet<INV1> INV1 { get; set; }
        public virtual DbSet<INV12> INV12 { get; set; }
        public virtual DbSet<JDT1> JDT1 { get; set; }
        public virtual DbSet<CINF> CINF { get; set; }
        public virtual DbSet<POR3> POR3 { get; set; }
        public virtual DbSet<OEXD> OEXD { get; set; }
        public virtual DbSet<OOCR> OOCR { get; set; }
        public virtual DbSet<OSTC> OSTC { get; set; }
        public virtual DbSet<OPRJ> OPRJ { get; set; }
        public virtual DbSet<OWHS> OWHS { get; set; }
        public virtual DbSet<PQT3> PQT3 { get; set; }
        public virtual DbSet<QUT3> QUT3 { get; set; }
        public virtual DbSet<RDR3> RDR3 { get; set; }
        public virtual DbSet<OACT> OACT { get; set; }
        public virtual DbSet<C_ARGNS_MODVENDOR> C_ARGNS_MODVENDOR { get; set; }
        public virtual DbSet<C_ARGNS_SAMPLES> C_ARGNS_SAMPLES { get; set; }
        public virtual DbSet<C_ARGNS_SAMPEVAL> C_ARGNS_SAMPEVAL { get; set; }
        public virtual DbSet<C_ARGNS_POMTEMPLATE> C_ARGNS_POMTEMPLATE { get; set; }
        public virtual DbSet<C_ARGNS_POMTEMPLNS> C_ARGNS_POMTEMPLNS { get; set; }
        public virtual DbSet<C_ARGNS_SAMPLESLINES> C_ARGNS_SAMPLESLINES { get; set; }
        public virtual DbSet<C_ARGNS_PROD_DATACOL> C_ARGNS_PROD_DATACOL { get; set; }
        public virtual DbSet<C_ARGNS_SHIFTS> C_ARGNS_SHIFTS { get; set; }
        public virtual DbSet<C_ARGNS_WORKCENTERPRO> C_ARGNS_WORKCENTERPRO { get; set; }
        public virtual DbSet<C_ARGNS_PRODCOLLNS> C_ARGNS_PRODCOLLNS { get; set; }
        public virtual DbSet<C_ARGNS_SENDOUT_PAR> C_ARGNS_SENDOUT_PAR { get; set; }
        public virtual DbSet<C_ARGNS_SENDOUT_STG> C_ARGNS_SENDOUT_STG { get; set; }
        public virtual DbSet<C_ARGNS_OPERTEMPLATE> C_ARGNS_OPERTEMPLATE { get; set; }
        public virtual DbSet<C_ARGNS_OPERTEMPLATEL> C_ARGNS_OPERTEMPLATEL { get; set; }
        public virtual DbSet<C_ARGNS_CUTTIC> C_ARGNS_CUTTIC { get; set; }
        public virtual DbSet<C_ARGNS_RESOURCE> C_ARGNS_RESOURCE { get; set; }
        public virtual DbSet<C_ARGNS_MODLIST> C_ARGNS_MODLIST { get; set; }
        public virtual DbSet<OWST> OWST { get; set; }
        public virtual DbSet<OBCD> OBCD { get; set; }
        public virtual DbSet<RCT2> RCT2 { get; set; }
        public virtual DbSet<ORCT> ORCT { get; set; }
        public virtual DbSet<ITL1> ITL1 { get; set; }
        public virtual DbSet<OBTN> OBTN { get; set; }
        public virtual DbSet<OITL> OITL { get; set; }
        public virtual DbSet<OSRN> OSRN { get; set; }
        public virtual DbSet<OUOM> OUOM { get; set; }
        public virtual DbSet<C_ARGNS_SIMBGRP> C_ARGNS_SIMBGRP { get; set; }
        public virtual DbSet<OOPR> OOPR { get; set; }
        public virtual DbSet<OPR1> OPR1 { get; set; }
        public virtual DbSet<OSCL> OSCL { get; set; }
        public virtual DbSet<OOST> OOST { get; set; }
        public virtual DbSet<CUFD> CUFD { get; set; }
        public virtual DbSet<OSC> OSCS { get; set; }
        public virtual DbSet<OSCT> OSCT { get; set; }
        public virtual DbSet<OSCP> OSCP { get; set; }
        public virtual DbSet<OPST> OPST { get; set; }
        public virtual DbSet<OSCO> OSCO { get; set; }
        public virtual DbSet<HEM6> HEM6 { get; set; }
        public virtual DbSet<OIN> OINS { get; set; }
        public virtual DbSet<SCL5> SCL5 { get; set; }
        public virtual DbSet<ATC1> ATC1 { get; set; }
        public virtual DbSet<UFD1> UFD1 { get; set; }
        public virtual DbSet<OCLA> OCLA { get; set; }
        public virtual DbSet<OALR> OALR { get; set; }
        public virtual DbSet<OAIB> OAIB { get; set; }
        public virtual DbSet<ALR2> ALR2 { get; set; }
        public virtual DbSet<ALR3> ALR3 { get; set; }
        public virtual DbSet<C_ARGNS_RANGEP> C_ARGNS_RANGEP { get; set; }
        public virtual DbSet<C_ARGNS_RANGEPDETAIL> C_ARGNS_RANGEPDETAIL { get; set; }
        public virtual DbSet<ITM1> ITM1 { get; set; }
        public virtual DbSet<ITM9> ITM9 { get; set; }
        public virtual DbSet<CRD2> CRD2 { get; set; }
        public virtual DbSet<OUDG> OUDG { get; set; }
        public virtual DbSet<C_ARGNS_MAT_DET> C_ARGNS_MAT_DET { get; set; }
        public virtual DbSet<C_ARGNS_CNTM> C_ARGNS_CNTM { get; set; }
        public virtual DbSet<C_ARGNS_CNTM_LINE> C_ARGNS_CNTM_LINE { get; set; }
        public virtual DbSet<C_ARGNS_CNTM_STATUS> C_ARGNS_CNTM_STATUS { get; set; }
        public virtual DbSet<C_ARGNS_CNTM_PORT> C_ARGNS_CNTM_PORT { get; set; }
        public virtual DbSet<C_ARGNS_CNTM_OCPK> C_ARGNS_CNTM_OCPK { get; set; }
        public virtual DbSet<C_ARGNS_CNTM_CPK1> C_ARGNS_CNTM_CPK1 { get; set; }
        public virtual DbSet<C_ARGNS_CPKG> C_ARGNS_CPKG { get; set; }
        public virtual DbSet<C_ARGNS_CNTM_OPKG> C_ARGNS_CNTM_OPKG { get; set; }
        public virtual DbSet<C_ARGNS_CNTM_PKG1> C_ARGNS_CNTM_PKG1 { get; set; }
        public virtual DbSet<OPKG> OPKG { get; set; }
        public virtual DbSet<RDR2> RDR2 { get; set; }
        public virtual DbSet<QUT2> QUT2 { get; set; }
        public virtual DbSet<C_ARGNS_INSTRUCTION> C_ARGNS_INSTRUCTION { get; set; }
        public virtual DbSet<UGP1> UGP1 { get; set; }
        public virtual DbSet<OUQR> OUQR { get; set; }
        public virtual DbSet<OQCN> OQCN { get; set; }
        public virtual DbSet<ODPI> ODPI { get; set; }
        public virtual DbSet<ORIN> ORIN { get; set; }
        public virtual DbSet<C_ARGNS_SCHEMA> C_ARGNS_SCHEMA { get; set; }
        public virtual DbSet<C_ARGNS_SCHEMAL> C_ARGNS_SCHEMAL { get; set; }
        public virtual DbSet<C_ARGNS_PATTEMPLNS> C_ARGNS_PATTEMPLNS { get; set; }
        public virtual DbSet<C_ARGNS_PATTEMP> C_ARGNS_PATTEMP { get; set; }
        public virtual DbSet<C_ARGNS_CS_PATTERNS> C_ARGNS_CS_PATTERNS { get; set; }
        public virtual DbSet<C_ARGNS_ROUTING> C_ARGNS_ROUTING { get; set; }
        public virtual DbSet<C_ARGNS_ROUTLINES> C_ARGNS_ROUTLINES { get; set; }
        public virtual DbSet<OHTY> OHTY { get; set; }
        public virtual DbSet<C_ARGNS_FBOOK> C_ARGNS_FBOOK { get; set; }
        public virtual DbSet<C_ARGNS_FBOOKD> C_ARGNS_FBOOKD { get; set; }
        public virtual DbSet<C_ARGNS_PREPACK> C_ARGNS_PREPACK { get; set; }
        public virtual DbSet<C_ARGNS_PREPACKLNS> C_ARGNS_PREPACKLNS { get; set; }
        public virtual DbSet<OITT> OITT { get; set; }
        public virtual DbSet<C_ARGNS_SIZERUNSCALE> C_ARGNS_SIZERUNSCALE { get; set; }
        public virtual DbSet<C_ARGNS_SZRUNSCALELN> C_ARGNS_SZRUNSCALELN { get; set; }
        public virtual DbSet<C_ARGNS_FASEMP> C_ARGNS_FASEMP { get; set; }
        public virtual DbSet<OUGP> OUGP { get; set; }
        public virtual DbSet<OBPL> OBPL { get; set; }
        public virtual DbSet<USR6> USR6 { get; set; }
        public virtual DbSet<OSBQ> OSBQ { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }

    }
}
