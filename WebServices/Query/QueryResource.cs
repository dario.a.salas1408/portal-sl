﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;

namespace WebServices.Query
{
    public static class QueryResource
    {
        public static string QueryGet(string key)
        {
            // Create a resource manager to retrieve resources.
            ResourceManager rm;

            rm = new ResourceManager("WebServices.Query.HANA", Assembly.GetExecutingAssembly());

            return rm.GetString(key);
        }
    }

   
}