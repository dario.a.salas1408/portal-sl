﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations;
using AutoMapper;
using ARGNS.Util;
using WebServices.Model;
using System.Data.Entity;
using WebServices.Model.Tables;
using WebServices.B1IFActivityService;

namespace WebServices.Activities
{
    public class ActivityService : IActivityService
    {
        private IActivityService mServFunction = null;

        public ActivityService()
        {
        }

        private void InitializeService(CompanyConn pCompanyParam)
        {
            if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
            {
                mServFunction = new ActivityServiceSL();
            }
            else
            {
                mServFunction = new ActivityServiceDS();
            }
        }

        public List<ActivitiesSAP> GetActivitiesList(CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetActivitiesList(pCompanyParam);
        }

        public List<ActivitiesSAP> GetActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetActivitiesListSearch(pCompanyParam, UserType, pModel, pProject, txtStatus, txtRemarks);
        }

        public List<ActivityStatusSAP> GetActivityStatusList(CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetActivityStatusList(pCompanyParam);
        }

        public ActivitiesSAP GetActivityById(CompanyConn pCompanyParam, int pCode, bool CheckApparelInstallation)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetActivityById(pCompanyParam, pCode, CheckApparelInstallation);
        }

        public string AddActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mServFunction.AddActivity(pObject, pCompanyParam);
        }

        public string UpdateActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mServFunction.UpdateActivity(pObject, pCompanyParam);
        }

        public string UpdateSalesOpportunitiesActivity(int pActivityId, int? pOpporId, short? pLine, CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mServFunction.UpdateSalesOpportunitiesActivity(pActivityId,pOpporId, pLine, pCompanyParam);
        }

        public List<ActivitiesSAP> GetCRMActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetCRMActivitiesListSearch(pCompanyParam, UserType, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks, txtBPCode);
        }

        public ActivitiesSAPCombo GetActivityCombo(CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetActivityCombo(pCompanyParam);
        }

        public List<ActivitiesSAP> GetBPCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetBPCRMActivitiesSearch(pCompanyParam, UserType, pBPCode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
        }

        public List<ActivitiesSAP> GetUserCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetUserCRMActivitiesSearch(pCompanyParam, UserType, pUserId, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
        }

        public List<ActivitiesSAP> GetSalesEmployeeCRMActivitiesSeach(CompanyConn pCompanyParam, int UserType, int pSECode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetSalesEmployeeCRMActivitiesSeach(pCompanyParam, UserType, pSECode, txtCode, txtAction, txtType, txtStartDate, txtEndDate, txtPriority, txtStatus, txtRemarks);
        }
        public List<ActivitiesSAP> GetCRMActivitiesListFromTo(CompanyConn pCompanyParam, DateTime? txtStartDate, DateTime? txtEndDate, int pUserId)
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetCRMActivitiesListFromTo(pCompanyParam, txtStartDate, txtEndDate, pUserId);
        }

        public List<ActivitiesSAP> GetBPMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetBPMyActivitiesSearch(pCompanyParam, UserType, pBPCode, pModel, pProject, txtStatus, txtRemarks);
        }

        public List<ActivitiesSAP> GetUserMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            InitializeService(pCompanyParam);
            return mServFunction.GetUserMyActivitiesSearch(pCompanyParam, UserType, pUserId, pModel, pProject, txtStatus, txtRemarks);
        }

    }
}
