﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace WebServices.Activities
{
    public class ActivityProcess
    {
        public string AddActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                B1IFActivityService.AddActType request = new B1IFActivityService.AddActType();
                B1IFActivityService.AddActResponseType response = new B1IFActivityService.AddActResponseType();
                B1IFActivityService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFActivityService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                B1IFActivityService.AddActTypeXmlToAdd ActivityObj = new B1IFActivityService.AddActTypeXmlToAdd();
                ActivityObj.Activity = new B1IFActivityService.AddActTypeXmlToAddActivity();

                switch (pObject.Action)
                {
                    case "C":
                        ActivityObj.Activity.Activity = "cn_Conversation";
                        break;
                    case "M":
                        ActivityObj.Activity.Activity = "cn_Meeting";
                        break;
                    case "T":
                        ActivityObj.Activity.Activity = "cn_Task";
                        break;
                    case "E":
                        ActivityObj.Activity.Activity = "cn_Note";
                        break;
                    case "P":
                        ActivityObj.Activity.Activity = "cn_Campaign";
                        break;
                    case "N":
                        ActivityObj.Activity.Activity = "cn_Other";
                        break;
                    default:
                        ActivityObj.Activity.Activity = "cn_Conversation";
                        break;
                }
                ActivityObj.Activity.ActivityType = (pObject.CntctType.HasValue && pObject.CntctType.Value != 0) ? pObject.CntctType.Value.ToString() : "-1";
                ActivityObj.Activity.CardCode = pObject.CardCode;
                ActivityObj.Activity.Closed = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? "tYES" : "tNO";
                ActivityObj.Activity.Details = pObject.Details;
                ActivityObj.Activity.EndDueDate = pObject.Details;
                ActivityObj.Activity.EndDueDate = pObject.endDate.HasValue ? pObject.endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");            
                if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
                {
                    ActivityObj.Activity.HandledBy = pObject.AttendUser.Value.ToString();
                }
                else
                {
                    ActivityObj.Activity.HandledBy = null;
                }
                if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
                {
                    ActivityObj.Activity.HandledByEmployee = pObject.AttendEmpl.Value.ToString();
                }
                else
                {
                    ActivityObj.Activity.HandledByEmployee = null;
                }
                ActivityObj.Activity.InactiveFlag = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? "tYES" : "tNO";
                ActivityObj.Activity.Notes = pObject.Notes;
                switch (pObject.Priority)
                {
                    case "0":
                        ActivityObj.Activity.Priority = "pr_Low";
                        break;
                    case "1":
                        ActivityObj.Activity.Priority = "pr_Normal";
                        break;
                    case "2":
                        ActivityObj.Activity.Priority = "pr_High";
                        break;
                    default:
                        ActivityObj.Activity.Priority = "pr_Normal";
                        break;
                }
                ActivityObj.Activity.StartDate = pObject.Recontact.HasValue ? pObject.Recontact.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
                ActivityObj.Activity.Subject = (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0) ? pObject.CntctSbjct.Value.ToString() : "-1";
                
                request.XmlToAdd = ActivityObj;
                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSAddAct(request);

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdateActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                B1IFActivityService.UpdateActType request = new B1IFActivityService.UpdateActType();
                B1IFActivityService.UpdateActResponseType response = new B1IFActivityService.UpdateActResponseType();
                B1IFActivityService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFActivityService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                B1IFActivityService.UpdateActTypeXmlToUpdate ActivityObj = new B1IFActivityService.UpdateActTypeXmlToUpdate();
                ActivityObj.Activity = new B1IFActivityService.UpdateActTypeXmlToUpdateActivity();

                ActivityObj.Activity.ActivityCode = pObject.ClgCode.ToString();
                switch (pObject.Action)
                {
                    case "C":
                        ActivityObj.Activity.Activity = "cn_Conversation";
                        break;
                    case "M":
                        ActivityObj.Activity.Activity = "cn_Meeting";
                        break;
                    case "T":
                        ActivityObj.Activity.Activity = "cn_Task";
                        break;
                    case "E":
                        ActivityObj.Activity.Activity = "cn_Note";
                        break;
                    case "P":
                        ActivityObj.Activity.Activity = "cn_Campaign";
                        break;
                    case "N":
                        ActivityObj.Activity.Activity = "cn_Other";
                        break;
                    default:
                        ActivityObj.Activity.Activity = "cn_Conversation";
                        break;
                }
                ActivityObj.Activity.ActivityType = (pObject.CntctType.HasValue && pObject.CntctType.Value != 0) ? pObject.CntctType.Value.ToString() : "-1";
                ActivityObj.Activity.CardCode = pObject.CardCode;
                ActivityObj.Activity.Closed = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? "tYES" : "tNO";
                ActivityObj.Activity.Details = pObject.Details;
                ActivityObj.Activity.EndDueDate = pObject.Details;
                ActivityObj.Activity.EndDueDate = pObject.endDate.HasValue ? pObject.endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");              
                if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
                {
                    ActivityObj.Activity.HandledBy = pObject.AttendUser.Value.ToString();
                }
                else
                {
                    ActivityObj.Activity.HandledBy = null;
                }
                if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
                {
                    ActivityObj.Activity.HandledByEmployee = pObject.AttendEmpl.Value.ToString();
                }
                else
                {
                    ActivityObj.Activity.HandledByEmployee = null;
                }
                ActivityObj.Activity.InactiveFlag = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? "tYES" : "tNO";
                ActivityObj.Activity.Notes = pObject.Notes;
                switch (pObject.Priority)
                {
                    case "0":
                        ActivityObj.Activity.Priority = "pr_Low";
                        break;
                    case "1":
                        ActivityObj.Activity.Priority = "pr_Normal";
                        break;
                    case "2":
                        ActivityObj.Activity.Priority = "pr_High";
                        break;
                    default:
                        ActivityObj.Activity.Priority = "pr_Normal";
                        break;
                }
                ActivityObj.Activity.StartDate = pObject.Recontact.HasValue ? pObject.Recontact.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
                ActivityObj.Activity.Subject = (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0) ? pObject.CntctSbjct.Value.ToString() : "-1";

                request.XmlToUpdate = ActivityObj;
                request.ActivityCode = pObject.ClgCode.ToString();
                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSUpdateAct(request);

                return "Ok";

            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

    }
}