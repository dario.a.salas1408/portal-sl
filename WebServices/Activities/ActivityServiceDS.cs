﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations;
using WebServices.Model;
using AutoMapper;
using ARGNS.Util;
using WebServices.Model.Tables;
using System.Data.Entity;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace WebServices.Activities
{
    public class ActivityServiceDS : IActivityService
    {
        private DataBase mDbContext;
        private B1WSHandler mB1WSHandler;

        public ActivityServiceDS()
        {
            Database.SetInitializer<DataBase>(null);
            mDbContext = new DataBase();
            mB1WSHandler = new B1WSHandler();
        }

        public List<ActivitiesSAP> GetActivitiesList(CompanyConn pCompanyParam)
        {
            List<ActivitiesSAP> ret = null;
            Mapper.CreateMap<OCLG, ActivitiesSAP>();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = Mapper.Map<List<ActivitiesSAP>>(mDbContext.OCLG.ToList().OrderByDescending(c => c.ClgCode).ToList());
                ret.ForEach(c => c.CardName = mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).Single().CardName);
                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetActivitiesList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ActivitiesSAP> GetActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesSAP> ret = null;
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                int mStatus = 0;
                if (!string.IsNullOrEmpty(txtStatus))
                    mStatus = Convert.ToInt32(txtStatus);

                ret = (from mAct in mDbContext.OCLG
                       join mBP in mDbContext.OCRD on mAct.CardCode equals mBP.CardCode
                       join mModel in mDbContext.C_ARGNS_MODEL on mAct.U_ARGNS_PRDCODE equals mModel.U_ModCode
                       where
                       (
                         (string.IsNullOrEmpty(pProject) ? true : mAct.U_ARGNS_PROYECT != null && mAct.U_ARGNS_PROYECT.Contains(pProject))
                          && (string.IsNullOrEmpty(pModel) ? true : mAct.U_ARGNS_PRDCODE != null && mAct.U_ARGNS_PRDCODE.Contains(pModel))
                          && (string.IsNullOrEmpty(txtStatus) ? true : mAct.status != null && mAct.status == mStatus)
                          && (string.IsNullOrEmpty(txtRemarks) ? true : mAct.Details != null && mAct.Details.Contains(txtRemarks))
                          && (UserType == -1 ? true : (UserType == 0 ? mAct.AttendUser != null : (UserType == 1 ? mAct.AttendEmpl != null : true)))
                       )
                       select new ActivitiesSAP()
                       {
                           ClgCode = mAct.ClgCode,
                           Action = mAct.Action,
                           AttendUser = mAct.AttendUser,
                           AttendEmpl = mAct.AttendEmpl,
                           CardCode = mAct.CardCode,
                           CardName = mBP.CardName,
                           Recontact = mAct.Recontact,
                           endDate = mAct.endDate,
                           U_ARGNS_PRDCODE = mAct.U_ARGNS_PRDCODE,
                           ModelName = mModel.U_ModDesc,
                           U_ARGNS_PROYECT = mAct.U_ARGNS_PROYECT,
                           ModelImage = mModel.U_Pic,
                           Notes = mAct.Notes,
                           status = mAct.status,
                           Details = mAct.Details
                       }).ToList();

                ret.ForEach(c =>
                {
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetActivitiesListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ActivityStatusSAP> GetActivityStatusList(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<ActivityStatusSAP, OCLA>();
                Mapper.CreateMap<OCLA, ActivityStatusSAP>();

                return Mapper.Map<List<ActivityStatusSAP>>(mDbContext.OCLA.ToList());
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetActivityStatusList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        private string GetAttendUserName(short? pAttenUser, int? pAttenEmp)
        {
            string ret = string.Empty;
            try
            {
                if (pAttenUser.HasValue)
                {
                    string mActUser = mDbContext.OUSR.Where(c => c.USERID == pAttenUser.Value).SingleOrDefault().U_NAME;
                    ret = mActUser;
                }
                else
                {
                    OHEM mActEmp = mDbContext.OHEM.Where(c => c.empID == pAttenEmp.Value).SingleOrDefault();
                    ret = mActEmp.lastName + " " + mActEmp.firstName;
                }

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetAttendUserName :" + ex.Message);
                return string.Empty;
            }

        }

        private string GetActionName(string pAction)
        {
            string ret = string.Empty;
            try
            {
                switch (pAction)
                {
                    case "C":
                        ret = "Phone Call";
                        break;
                    case "M":
                        ret = "Meeting";
                        break;
                    case "T":
                        ret = "Task";
                        break;
                    case "E":
                        ret = "Notes";
                        break;
                    case "P":
                        ret = "Campaign";
                        break;
                    case "N":
                        ret = "Other";
                        break;
                }

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetActionName :" + ex.Message);
                return string.Empty;
            }


        }

        public ActivitiesSAPCombo GetActivityCombo(CompanyConn pCompanyParam)
        {
            if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Closed)
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
            }

            ActivitiesSAPCombo ret = null;
            Mapper.CreateMap<OCLT, ActivityTypeSAP>();
            Mapper.CreateMap<ActivityStatusSAP, OCLA>();
            Mapper.CreateMap<OCLA, ActivityStatusSAP>();
            try
            {
                ret = new ActivitiesSAPCombo();

                //Activity Type
                ret.ActivityTypeList = Mapper.Map<List<ActivityTypeSAP>>(mDbContext.OCLT.ToList());
                ret.ActivityStatusList = new List<ActivityStatusSAP>();
                ret.ActivityStatusList.Add(new ActivityStatusSAP(-1, ""));
                ret.ActivityStatusList.AddRange(Mapper.Map<List<ActivityStatusSAP>>(mDbContext.OCLA.ToList()));

                //Activity Action
                ret.ActivityActionList = new List<ActivityAction>()
                {
                    new ActivityAction(){Code="C",Name="Phone Call"},
                    new ActivityAction(){Code="M",Name="Meeting"},
                    new ActivityAction(){Code="T",Name="Task"},
                    new ActivityAction(){Code="E",Name="Notes"},
                    new ActivityAction(){Code="P",Name="Campaign"},
                    new ActivityAction(){Code="N",Name="Other"},
                };

                //Activity Priority
                ret.ActivityPriorityList = new List<ActivityPriority>()
                {
                    new ActivityPriority(){Code="0",Name="Low"},
                    new ActivityPriority(){Code="1",Name="Normal"},
                    new ActivityPriority(){Code="2",Name="High"},                   
                };

                //Activity User Type
                ret.ActivityUserTypeList = new List<ActivityUserType>()
                {
                    new ActivityUserType(){Code=0,Name="User"},
                    new ActivityUserType(){Code=1,Name="Employee"},                                 
                };

                //Activity User Type
                ret.ActivityUserList = new List<ActivityUser>();

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetActivityCombo :" + ex.Message);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public ActivitiesSAP GetActivityById(CompanyConn pCompanyParam, int pCode, bool CheckApparelInstallation)
        {
            ActivitiesSAP ret = null;
            Mapper.CreateMap<OCLG, ActivitiesSAP>();
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                if (CheckApparelInstallation)
                {
                    ret = Mapper.Map<ActivitiesSAP>(mDbContext.OCLG.Where(c => c.ClgCode == pCode).SingleOrDefault());
                }
                else
                {
                    ret = (from mOCLG in mDbContext.OCLG
                           where (
                                mOCLG.ClgCode == pCode
                           )
                           orderby (mOCLG.ClgCode)
                           select new ActivitiesSAP()
                           {
                               ClgCode = mOCLG.ClgCode,
                               CardCode = mOCLG.CardCode,
                               Notes = mOCLG.Notes,
                               Recontact = mOCLG.Recontact,
                               Closed = mOCLG.Closed,
                               CntctSbjct = mOCLG.CntctSbjct,
                               AttendUser = mOCLG.AttendUser,
                               AttendEmpl = mOCLG.AttendEmpl,
                               Action = mOCLG.Action,
                               Details = mOCLG.Details,
                               CntctType = mOCLG.CntctType,
                               Priority = mOCLG.Priority,
                               endDate = mOCLG.endDate,
                               inactive = mOCLG.inactive,
                               OprId = mOCLG.OprId,
                               OprLine = mOCLG.OprLine,
                               status = mOCLG.status,
                               BeginTime = mOCLG.BeginTime,
                               ENDTime = mOCLG.ENDTime
                           }).SingleOrDefault();
                }
                if (ret == null)
                {
                    ret = new ActivitiesSAP();
                }
                ret.CardName = !string.IsNullOrEmpty(ret.CardCode) ? mDbContext.OCRD.Where(c => c.CardCode == ret.CardCode).Single().CardName : string.Empty;
                ret.ActivityCombo = GetActivityCombo(pCompanyParam);
                ret.UserType = (ret.AttendEmpl != null ? 1 : 0);

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetActivityById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string AddActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {
            ActivitiesServiceWeb.Activity oActivity = new ActivitiesServiceWeb.Activity();

            ActivitiesServiceWeb.MsgHeader oActivitiesMsgHeader = new ActivitiesServiceWeb.MsgHeader();

            oActivitiesMsgHeader.ServiceName = ActivitiesServiceWeb.MsgHeaderServiceName.ActivitiesService;
            oActivitiesMsgHeader.ServiceNameSpecified = true;
            oActivitiesMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ActivitiesServiceWeb.ActivitiesService oActivityServiceWeb = new ActivitiesServiceWeb.ActivitiesService();
            oActivityServiceWeb.MsgHeaderValue = oActivitiesMsgHeader;
            try
            {
                oActivity.CardCode = pObject.CardCode;
                if (pObject.Recontact.HasValue)
                {
                    oActivity.StartDate = pObject.Recontact.Value;
                    oActivity.StartDateSpecified = true;
                }
                if (pObject.BeginTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? 0 : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? pObject.BeginTime.Value : Convert.ToInt32(pObject.BeginTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.StartTime = mDate;
                    oActivity.StartTimeSpecified = true;
                }
                if (pObject.endDate.HasValue)
                {
                    oActivity.EndDueDate = pObject.endDate.Value;
                    oActivity.EndDueDateSpecified = true;
                }
                if (pObject.ENDTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? 0 : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? pObject.ENDTime.Value : Convert.ToInt32(pObject.ENDTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.EndTime = mDate;
                    oActivity.EndTimeSpecified = true;
                }
                if (pObject.Duration.HasValue)
                {
                    oActivity.Duration = (double)pObject.Duration.Value;
                    oActivity.DurationSpecified = true;
                    oActivity.DurationType = ActivitiesServiceWeb.ActivityDurationType.du_Days;
                    oActivity.DurationTypeSpecified = true;
                }
                if (pObject.status.HasValue)
                {
                    oActivity.Status = pObject.status.Value;
                    oActivity.StatusSpecified = true;
                }
                switch (pObject.Action)
                {
                    case "C":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                    case "M":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Meeting;
                        break;
                    case "T":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Task;
                        break;
                    case "E":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Note;
                        break;
                    case "P":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Campaign;
                        break;
                    case "N":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Other;
                        break;
                    default:
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                }
                oActivity.Activity1Specified = true;

                if (pObject.CntctType.HasValue && pObject.CntctType.Value != 0)
                {
                    oActivity.ActivityType = (long)pObject.CntctType;
                    oActivity.ActivityTypeSpecified = true;
                }
                if (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0)
                    oActivity.Subject = (long)pObject.CntctSbjct;
                oActivity.Notes = pObject.Notes;
                oActivity.Details = pObject.Details;

                switch (pObject.Priority)
                {
                    case "0":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Low;
                        break;
                    case "1":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                    case "2":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_High;
                        break;
                    default:
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                }

                oActivity.PrioritySpecified = true;

                oActivity.Closed = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? ActivitiesServiceWeb.ActivityClosed.tYES : ActivitiesServiceWeb.ActivityClosed.tNO;
                oActivity.InactiveFlag = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? ActivitiesServiceWeb.ActivityInactiveFlag.tYES : ActivitiesServiceWeb.ActivityInactiveFlag.tNO;
                if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
                {
                    oActivity.HandledBy = pObject.AttendUser.Value;
                    oActivity.HandledBySpecified = true;
                }
                if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
                {
                    oActivity.HandledByEmployee = pObject.AttendEmpl.Value;
                    oActivity.HandledByEmployeeSpecified = true;
                }

                
                XmlDocument mXmlDoc = GetSoapStructure(false, oActivitiesMsgHeader, oActivity);

                //I Add some UDF only in case the activity come from Apparel Critical Path, and has setted the Apparel UDFs
                #region Apparel UDFs

                if (!string.IsNullOrEmpty(pObject.U_ARGNS_PRDCODE))
                {
                    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PRDCODE);
                }
                if (!string.IsNullOrEmpty(pObject.U_ARGNS_PROYECT))
                {
                    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PROYECT);
                }
                if (!string.IsNullOrEmpty(pObject.U_ARGNS_WORKF2))
                {
                    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_WORKF2);
                }
                if (pObject.U_ARGNS_JOBID != null)
                {
                    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_JOBID.ToString());
                }

                #endregion

                string mResponse = mB1WSHandler.ProcessActivityWithError(mXmlDoc);

                return "Ok" + ";" + mResponse;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> AddActivity :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {
            ActivitiesServiceWeb.Activity oActivity = new ActivitiesServiceWeb.Activity();
            ActivitiesServiceWeb.ActivityParams oActivityParams = new ActivitiesServiceWeb.ActivityParams();
            ActivitiesServiceWeb.MsgHeader oActivitiesMsgHeader = new ActivitiesServiceWeb.MsgHeader();

            oActivitiesMsgHeader.ServiceName = ActivitiesServiceWeb.MsgHeaderServiceName.ActivitiesService;
            oActivitiesMsgHeader.ServiceNameSpecified = true;
            oActivitiesMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ActivitiesServiceWeb.ActivitiesService oActivityServiceWeb = new ActivitiesServiceWeb.ActivitiesService();
            oActivityServiceWeb.MsgHeaderValue = oActivitiesMsgHeader;
            try
            {
                oActivityParams.ActivityCodeSpecified = true;
                oActivityParams.ActivityCode = pObject.ClgCode;
                oActivity = oActivityServiceWeb.GetActivity(oActivityParams);

                if (pObject.Recontact.HasValue)
                {
                    oActivity.StartDate = pObject.Recontact.Value;
                    oActivity.StartDateSpecified = true;
                }
                if (pObject.BeginTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? 0 : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? pObject.BeginTime.Value : Convert.ToInt32(pObject.BeginTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.StartTime = mDate;
                    oActivity.StartTimeSpecified = true;
                }
                if (pObject.endDate.HasValue)
                {
                    oActivity.EndDueDate = pObject.endDate.Value;
                    oActivity.EndDueDateSpecified = true;
                }
                if (pObject.ENDTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? 0 : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? pObject.ENDTime.Value : Convert.ToInt32(pObject.ENDTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.EndTime = mDate;
                    oActivity.EndTimeSpecified = true;
                }
                if (pObject.status.HasValue)
                {
                    oActivity.Status = pObject.status.Value;
                    oActivity.StatusSpecified = true;
                }

                switch (pObject.Action)
                {
                    case "C":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                    case "M":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Meeting;
                        break;
                    case "T":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Task;
                        break;
                    case "E":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Note;
                        break;
                    case "P":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Campaign;
                        break;
                    case "N":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Other;
                        break;
                    default:
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                }
                if (pObject.CntctType.HasValue && pObject.CntctType.Value != 0)
                    oActivity.ActivityType = (long)pObject.CntctType;

                if (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0)
                    oActivity.Subject = (long)pObject.CntctSbjct;
                oActivity.Notes = pObject.Notes;
                oActivity.Details = pObject.Details;
                switch (pObject.Priority)
                {
                    case "0":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Low;
                        break;
                    case "1":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                    case "2":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_High;
                        break;
                    default:
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                }
                oActivity.PrioritySpecified = true;

                oActivity.Closed = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? ActivitiesServiceWeb.ActivityClosed.tYES : ActivitiesServiceWeb.ActivityClosed.tNO;
                oActivity.InactiveFlag = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? ActivitiesServiceWeb.ActivityInactiveFlag.tYES : ActivitiesServiceWeb.ActivityInactiveFlag.tNO;
                if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
                {
                    oActivity.HandledBy = pObject.AttendUser.Value;
                    oActivity.HandledBySpecified = true;
                }
                if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
                {
                    oActivity.HandledByEmployee = pObject.AttendEmpl.Value;
                    oActivity.HandledByEmployeeSpecified = true;
                }

                oActivityServiceWeb.UpdateActivity(oActivity);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> UpdateActivity :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pActivityId"></param>
        /// <param name="pOpporId"></param>
        /// <param name="pLine"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateSalesOpportunitiesActivity(int pActivityId, int? pOpporId, short? pLine, CompanyConn pCompanyParam)
        {
            ActivitiesServiceWeb.Activity oActivity = new ActivitiesServiceWeb.Activity();
            ActivitiesServiceWeb.ActivityParams oActivityParams = new ActivitiesServiceWeb.ActivityParams();
            ActivitiesServiceWeb.MsgHeader oActivitiesMsgHeader = new ActivitiesServiceWeb.MsgHeader();

            oActivitiesMsgHeader.ServiceName = ActivitiesServiceWeb.MsgHeaderServiceName.ActivitiesService;
            oActivitiesMsgHeader.ServiceNameSpecified = true;
            oActivitiesMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ActivitiesServiceWeb.ActivitiesService oActivityServiceWeb = new ActivitiesServiceWeb.ActivitiesService();
            oActivityServiceWeb.MsgHeaderValue = oActivitiesMsgHeader;
            try
            {
                oActivityParams.ActivityCodeSpecified = true;
                oActivityParams.ActivityCode = pActivityId;
                oActivity = oActivityServiceWeb.GetActivity(oActivityParams);

                oActivity.SalesOpportunityIdSpecified = true;
                oActivity.SalesOpportunityLineSpecified = true;
                oActivity.ParentObjectIdSpecified = true;

                //Opportunity ID
                oActivity.SalesOpportunityId = (pOpporId.HasValue) ? (long)pOpporId.Value : 0;
                //Opportunity Line                
                oActivity.SalesOpportunityLine = (pLine.HasValue) ? (long)pLine.Value : 0;
                if (!pOpporId.HasValue && !pLine.HasValue)
                {
                    oActivity.ParentObjectType = "0";
                    oActivity.ParentObjectId = 0;
                }
                else
                {
                    oActivity.ParentObjectType = "97";
                    oActivity.ParentObjectId = pOpporId.Value;
                }

                oActivityServiceWeb.UpdateActivity(oActivity);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> UpdateSalesOpportunitiesActivity :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="UserType"></param>
        /// <param name="txtCode"></param>
        /// <param name="txtAction"></param>
        /// <param name="txtType"></param>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="txtPriority"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <param name="txtBPCode"></param>
        /// <returns></returns>
        public List<ActivitiesSAP> GetCRMActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode)
        {
            List<ActivitiesSAP> ret = null;
            Mapper.CreateMap<OCLG, ActivitiesSAP>();
            Mapper.CreateMap<ActivitiesSAP, OCLG>();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                int mStatus = 0;
                if (!string.IsNullOrEmpty(txtStatus))
                    mStatus = Convert.ToInt32(txtStatus);

                int mCodeInt = 0;
                if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
                    mCodeInt = Convert.ToInt32(txtCode);

                int mTypeInt = 0;
                if (!string.IsNullOrEmpty(txtType))
                    mTypeInt = Convert.ToInt32(txtType);

                ret = (from mOCLG in mDbContext.OCLG
                       where (
                           ((!string.IsNullOrEmpty(txtCode) && txtCode != "0") ? mOCLG.ClgCode == mCodeInt : true) &&
                           ((!string.IsNullOrEmpty(txtAction) && txtAction != "-1") ? mOCLG.Action == txtAction : true) &&
                           ((!string.IsNullOrEmpty(txtType) && txtType != "-2") ? mOCLG.CntctType == mTypeInt : true) &&
                           ((txtStartDate != null) ? mOCLG.Recontact == txtStartDate : true) &&
                           ((txtEndDate != null) ? mOCLG.endDate == txtEndDate : true) &&
                           ((!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1") ? mOCLG.Priority == txtPriority : true) &&
                           (string.IsNullOrEmpty(txtRemarks) ? true : mOCLG.Details != null && mOCLG.Details.ToUpper().Contains(txtRemarks.ToUpper())) &&
                           ((!string.IsNullOrEmpty(txtStatus)) ? mOCLG.status == mStatus : true) &&
                           ((!string.IsNullOrEmpty(txtBPCode)) ? mOCLG.CardCode == txtBPCode : true) && 
                           (UserType == -1 ? true : (UserType == 0 ? mOCLG.AttendUser != null : (UserType == 1 ? mOCLG.AttendEmpl != null : true)))
                       )
                       orderby (mOCLG.ClgCode)
                       select new ActivitiesSAP()
                       {
                           ClgCode = mOCLG.ClgCode,
                           CardCode = mOCLG.CardCode,
                           Notes = mOCLG.Notes,
                           Recontact = mOCLG.Recontact,
                           Closed = mOCLG.Closed,
                           CntctSbjct = mOCLG.CntctSbjct,
                           AttendUser = mOCLG.AttendUser,
                           AttendEmpl = mOCLG.AttendEmpl,
                           Action = mOCLG.Action,
                           Details = mOCLG.Details,
                           CntctType = mOCLG.CntctType,
                           Priority = mOCLG.Priority,
                           endDate = mOCLG.endDate,
                           inactive = mOCLG.inactive,
                           OprId = mOCLG.OprId,
                           OprLine = mOCLG.OprLine,
                           status = mOCLG.status
                       }).ToList();

                ret.ForEach(c =>
                {
                    c.CardName = (mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault() != null ? mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault().CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetCRMActivitiesListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="UserType"></param>
        /// <param name="pBPCode"></param>
        /// <param name="txtCode"></param>
        /// <param name="txtAction"></param>
        /// <param name="txtType"></param>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="txtPriority"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <returns></returns>
        public List<ActivitiesSAP> GetBPCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesSAP> ret = null;
            Mapper.CreateMap<OCLG, ActivitiesSAP>();
            Mapper.CreateMap<ActivitiesSAP, OCLG>();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                int mStatus = 0;
                if (!string.IsNullOrEmpty(txtStatus))
                    mStatus = Convert.ToInt32(txtStatus);

                int mCodeInt = 0;
                if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
                    mCodeInt = Convert.ToInt32(txtCode);

                int mTypeInt = 0;
                if (!string.IsNullOrEmpty(txtType))
                    mTypeInt = Convert.ToInt32(txtType);

                ret = (from mOCLG in mDbContext.OCLG
                       where (
                           (mOCLG.CardCode == pBPCode) &&
                           ((!string.IsNullOrEmpty(txtCode) && txtCode != "0") ? mOCLG.ClgCode == mCodeInt : true) &&
                           ((!string.IsNullOrEmpty(txtAction) && txtAction != "-1") ? mOCLG.Action == txtAction : true) &&
                           ((!string.IsNullOrEmpty(txtType) && txtType != "-2") ? mOCLG.CntctType == mTypeInt : true) &&
                           ((txtStartDate != null) ? mOCLG.Recontact == txtStartDate : true) &&
                           ((txtEndDate != null) ? mOCLG.endDate == txtEndDate : true) &&
                           ((!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1") ? mOCLG.Priority == txtPriority : true) &&
                           (string.IsNullOrEmpty(txtRemarks) ? true : mOCLG.Details != null && mOCLG.Details.ToUpper().Contains(txtRemarks.ToUpper())) &&
                           ((!string.IsNullOrEmpty(txtStatus)) ? mOCLG.status == mStatus : true) &&
                           (UserType == -1 ? true : (UserType == 0 ? mOCLG.AttendUser != null : (UserType == 1 ? mOCLG.AttendEmpl != null : true)))
                       )
                       orderby (mOCLG.ClgCode)
                       select new ActivitiesSAP()
                       {
                           ClgCode = mOCLG.ClgCode,
                           CardCode = mOCLG.CardCode,
                           Notes = mOCLG.Notes,
                           Recontact = mOCLG.Recontact,
                           Closed = mOCLG.Closed,
                           CntctSbjct = mOCLG.CntctSbjct,
                           AttendUser = mOCLG.AttendUser,
                           AttendEmpl = mOCLG.AttendEmpl,
                           Action = mOCLG.Action,
                           Details = mOCLG.Details,
                           CntctType = mOCLG.CntctType,
                           Priority = mOCLG.Priority,
                           endDate = mOCLG.endDate,
                           inactive = mOCLG.inactive,
                           OprId = mOCLG.OprId,
                           OprLine = mOCLG.OprLine,
                           status = mOCLG.status
                       }).ToList();


                ret.ForEach(c =>
                {
                    c.CardName = (mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault() != null ? mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault().CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetBPCRMActivitiesSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="UserType"></param>
        /// <param name="pUserId"></param>
        /// <param name="txtCode"></param>
        /// <param name="txtAction"></param>
        /// <param name="txtType"></param>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="txtPriority"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <returns></returns>
        public List<ActivitiesSAP> GetUserCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesSAP> ret = null;
            Mapper.CreateMap<OCLG, ActivitiesSAP>();
            Mapper.CreateMap<ActivitiesSAP, OCLG>();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                int mStatus = 0;
                if (!string.IsNullOrEmpty(txtStatus))
                    mStatus = Convert.ToInt32(txtStatus);

                int mCodeInt = 0;
                if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
                    mCodeInt = Convert.ToInt32(txtCode);

                int mTypeInt = 0;
                if (!string.IsNullOrEmpty(txtType))
                    mTypeInt = Convert.ToInt32(txtType);

                List<int> mListEmployeeId = mDbContext.OHEM.Where(c => c.userId == pUserId).Select(c => c.empID).ToList();
                
                ret = (from mOCLG in mDbContext.OCLG
                       where (
                           (mOCLG.AttendUser == pUserId || mListEmployeeId.Contains(mOCLG.AttendEmpl != null ? mOCLG.AttendEmpl.Value : -1)) &&
                           ((!string.IsNullOrEmpty(txtCode) && txtCode != "0") ? mOCLG.ClgCode == mCodeInt : true) &&
                           ((!string.IsNullOrEmpty(txtAction) && txtAction != "-1") ? mOCLG.Action == txtAction : true) &&
                           ((!string.IsNullOrEmpty(txtType) && txtType != "-2") ? mOCLG.CntctType == mTypeInt : true) &&
                           ((txtStartDate != null) ? mOCLG.Recontact == txtStartDate : true) &&
                           ((txtEndDate != null) ? mOCLG.endDate == txtEndDate : true) &&
                           ((!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1") ? mOCLG.Priority == txtPriority : true) &&
                           (string.IsNullOrEmpty(txtRemarks) ? true : mOCLG.Details != null && mOCLG.Details.ToUpper().Contains(txtRemarks.ToUpper())) &&
                           ((!string.IsNullOrEmpty(txtStatus)) ? mOCLG.status == mStatus : true) &&
                           (UserType == -1 ? true : (UserType == 0 ? mOCLG.AttendUser != null : (UserType == 1 ? mOCLG.AttendEmpl != null : true)))
                       )
                       orderby (mOCLG.ClgCode)
                       select new ActivitiesSAP()
                       {
                           ClgCode = mOCLG.ClgCode,
                           CardCode = mOCLG.CardCode,
                           Notes = mOCLG.Notes,
                           Recontact = mOCLG.Recontact,
                           Closed = mOCLG.Closed,
                           CntctSbjct = mOCLG.CntctSbjct,
                           AttendUser = mOCLG.AttendUser,
                           AttendEmpl = mOCLG.AttendEmpl,
                           Action = mOCLG.Action,
                           Details = mOCLG.Details,
                           CntctType = mOCLG.CntctType,
                           Priority = mOCLG.Priority,
                           endDate = mOCLG.endDate,
                           inactive = mOCLG.inactive,
                           OprId = mOCLG.OprId,
                           OprLine = mOCLG.OprLine,
                           status = mOCLG.status
                       }).ToList();

                ret.ForEach(c =>
                {
                    c.CardName = (mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault() != null ? mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault().CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetUserCRMActivitiesSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="UserType"></param>
        /// <param name="pSECode"></param>
        /// <param name="txtCode"></param>
        /// <param name="txtAction"></param>
        /// <param name="txtType"></param>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="txtPriority"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <returns></returns>
        public List<ActivitiesSAP> GetSalesEmployeeCRMActivitiesSeach(CompanyConn pCompanyParam, int UserType, int pSECode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesSAP> ret = null;
            Mapper.CreateMap<OCLG, ActivitiesSAP>();
            Mapper.CreateMap<ActivitiesSAP, OCLG>();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                int mStatus = 0;
                if (!string.IsNullOrEmpty(txtStatus))
                    mStatus = Convert.ToInt32(txtStatus);

                int mCodeInt = 0;
                if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
                    mCodeInt = Convert.ToInt32(txtCode);

                int mTypeInt = 0;
                if (!string.IsNullOrEmpty(txtType))

                    mTypeInt = Convert.ToInt32(txtType);

                List<int> mListEmployeeId = mDbContext.OHEM.Where(c => c.salesPrson == pSECode).Select(c => c.empID).ToList();

                ret = (from mOCLG in mDbContext.OCLG
                       where (
                           (mListEmployeeId.Contains(mOCLG.AttendEmpl != null ? mOCLG.AttendEmpl.Value : -1)) &&
                           ((!string.IsNullOrEmpty(txtCode) && txtCode != "0") ? mOCLG.ClgCode == mCodeInt : true) &&
                           ((!string.IsNullOrEmpty(txtAction) && txtAction != "-1") ? mOCLG.Action == txtAction : true) &&
                           ((!string.IsNullOrEmpty(txtType) && txtType != "-2") ? mOCLG.CntctType == mTypeInt : true) &&
                           ((txtStartDate != null) ? mOCLG.Recontact == txtStartDate : true) &&
                           ((txtEndDate != null) ? mOCLG.endDate == txtEndDate : true) &&
                           ((!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1") ? mOCLG.Priority == txtPriority : true) &&
                           (string.IsNullOrEmpty(txtRemarks) ? true : mOCLG.Details != null && mOCLG.Details.ToUpper().Contains(txtRemarks.ToUpper())) &&
                           ((!string.IsNullOrEmpty(txtStatus)) ? mOCLG.status == mStatus : true) &&
                           (UserType == -1 ? true : (UserType == 0 ? mOCLG.AttendUser != null : (UserType == 1 ? mOCLG.AttendEmpl != null : true)))
                       )
                       orderby (mOCLG.ClgCode)
                       select new ActivitiesSAP()
                       {
                           ClgCode = mOCLG.ClgCode,
                           CardCode = mOCLG.CardCode,
                           Notes = mOCLG.Notes,
                           Recontact = mOCLG.Recontact,
                           Closed = mOCLG.Closed,
                           CntctSbjct = mOCLG.CntctSbjct,
                           AttendUser = mOCLG.AttendUser,
                           AttendEmpl = mOCLG.AttendEmpl,
                           Action = mOCLG.Action,
                           Details = mOCLG.Details,
                           CntctType = mOCLG.CntctType,
                           Priority = mOCLG.Priority,
                           endDate = mOCLG.endDate,
                           inactive = mOCLG.inactive,
                           OprId = mOCLG.OprId,
                           OprLine = mOCLG.OprLine,
                           status = mOCLG.status
                       }).ToList();

                ret.ForEach(c =>
                {
                    c.CardName = (mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault() != null ? mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault().CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetSalesEmployeeCRMActivitiesSeach :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="txtStartDate"></param>
        /// <param name="txtEndDate"></param>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public List<ActivitiesSAP> GetCRMActivitiesListFromTo(CompanyConn pCompanyParam, DateTime? txtStartDate, DateTime? txtEndDate, int pUserId)
        {
            List<ActivitiesSAP> ret = null;
            Mapper.CreateMap<OCLG, ActivitiesSAP>();
            Mapper.CreateMap<ActivitiesSAP, OCLG>();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = (from mOCLG in mDbContext.OCLG
                       where (
                           mOCLG.AttendUser == pUserId &&
                           (mOCLG.Recontact >= txtStartDate && mOCLG.endDate <= txtEndDate)
                       )
                       orderby (mOCLG.ClgCode)
                       select new ActivitiesSAP()
                       {
                           ClgCode = mOCLG.ClgCode,
                           CardCode = mOCLG.CardCode,
                           Notes = mOCLG.Notes,
                           Recontact = mOCLG.Recontact,
                           Closed = mOCLG.Closed,
                           CntctSbjct = mOCLG.CntctSbjct,
                           AttendUser = mOCLG.AttendUser,
                           AttendEmpl = mOCLG.AttendEmpl,
                           Action = mOCLG.Action,
                           Details = mOCLG.Details,
                           CntctType = mOCLG.CntctType,
                           Priority = mOCLG.Priority,
                           endDate = mOCLG.endDate,
                           inactive = mOCLG.inactive,
                           OprId = mOCLG.OprId,
                           OprLine = mOCLG.OprLine,
                           status = mOCLG.status,
                           BeginTime = mOCLG.BeginTime,
                           ENDTime = mOCLG.ENDTime
                       }).ToList();

                ret.ForEach(c =>
                {
                    c.CardName = (mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault() != null ? mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault().CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetCRMActivitiesListFromTo :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="UserType"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pModel"></param>
        /// <param name="pProject"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <returns></returns>
        public List<ActivitiesSAP> GetBPMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesSAP> ret = null;
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                int mStatus = 0;
                if (!string.IsNullOrEmpty(txtStatus))
                    mStatus = Convert.ToInt32(txtStatus);

                ret = (from mAct in mDbContext.OCLG
                       join mBP in mDbContext.OCRD on mAct.CardCode equals mBP.CardCode
                       join mModel in mDbContext.C_ARGNS_MODEL on mAct.U_ARGNS_PRDCODE equals mModel.U_ModCode
                       where
                       (
                         (mAct.CardCode == pBPCode) &&
                         (string.IsNullOrEmpty(pProject) ? true : mAct.U_ARGNS_PROYECT != null && mAct.U_ARGNS_PROYECT.Contains(pProject))
                          && (string.IsNullOrEmpty(pModel) ? true : mAct.U_ARGNS_PRDCODE != null && mAct.U_ARGNS_PRDCODE.Contains(pModel))
                          && (string.IsNullOrEmpty(txtStatus) ? true : mAct.status != null && mAct.status == mStatus)
                          && (string.IsNullOrEmpty(txtRemarks) ? true : mAct.Details != null && mAct.Details.Contains(txtRemarks))
                          && (UserType == -1 ? true : (UserType == 0 ? mAct.AttendUser != null : (UserType == 1 ? mAct.AttendEmpl != null : true)))
                       )
                       select new ActivitiesSAP()
                       {
                           ClgCode = mAct.ClgCode,
                           Action = mAct.Action,
                           AttendUser = mAct.AttendUser,
                           AttendEmpl = mAct.AttendEmpl,
                           CardCode = mAct.CardCode,
                           CardName = mBP.CardName,
                           Recontact = mAct.Recontact,
                           endDate = mAct.endDate,
                           U_ARGNS_PRDCODE = mAct.U_ARGNS_PRDCODE,
                           ModelName = mModel.U_ModDesc,
                           U_ARGNS_PROYECT = mAct.U_ARGNS_PROYECT,
                           ModelImage = mModel.U_Pic,
                           Notes = mAct.Notes,
                           status = mAct.status,
                           Details = mAct.Details
                       }).ToList();

                ret.ForEach(c =>
                {
                    c.CardName = (mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault() != null ? mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault().CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetBPMyActivitiesSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="UserType"></param>
        /// <param name="pUserId"></param>
        /// <param name="pModel"></param>
        /// <param name="pProject"></param>
        /// <param name="txtStatus"></param>
        /// <param name="txtRemarks"></param>
        /// <returns></returns>
        public List<ActivitiesSAP> GetUserMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesSAP> ret = null;
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                int mStatus = 0;
                if (!string.IsNullOrEmpty(txtStatus))
                    mStatus = Convert.ToInt32(txtStatus);

                ret = (from mAct in mDbContext.OCLG
                       join mBP in mDbContext.OCRD on mAct.CardCode equals mBP.CardCode
                       join mModel in mDbContext.C_ARGNS_MODEL on mAct.U_ARGNS_PRDCODE equals mModel.U_ModCode
                       where
                       (
                         (mAct.AttendUser == pUserId) &&
                         (string.IsNullOrEmpty(pProject) ? true : mAct.U_ARGNS_PROYECT != null && mAct.U_ARGNS_PROYECT.Contains(pProject))
                          && (string.IsNullOrEmpty(pModel) ? true : mAct.U_ARGNS_PRDCODE != null && mAct.U_ARGNS_PRDCODE.Contains(pModel))
                          && (string.IsNullOrEmpty(txtStatus) ? true : mAct.status != null && mAct.status == mStatus)
                          && (string.IsNullOrEmpty(txtRemarks) ? true : mAct.Details != null && mAct.Details.Contains(txtRemarks))
                          && (UserType == -1 ? true : (UserType == 0 ? mAct.AttendUser != null : (UserType == 1 ? mAct.AttendEmpl != null : true)))
                       )
                       select new ActivitiesSAP()
                       {
                           ClgCode = mAct.ClgCode,
                           Action = mAct.Action,
                           AttendUser = mAct.AttendUser,
                           AttendEmpl = mAct.AttendEmpl,
                           CardCode = mAct.CardCode,
                           CardName = mBP.CardName,
                           Recontact = mAct.Recontact,
                           endDate = mAct.endDate,
                           U_ARGNS_PRDCODE = mAct.U_ARGNS_PRDCODE,
                           ModelName = mModel.U_ModDesc,
                           U_ARGNS_PROYECT = mAct.U_ARGNS_PROYECT,
                           ModelImage = mModel.U_Pic,
                           Notes = mAct.Notes,
                           status = mAct.status,
                           Details = mAct.Details
                       }).ToList();

                ret.ForEach(c =>
                {
                    c.CardName = (mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault() != null ? mDbContext.OCRD.Where(z => z.CardCode == c.CardCode).FirstOrDefault().CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceDS -> GetUserMyActivitiesSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIsUpdate"></param>
        /// <param name="pHeader"></param>
        /// <param name="pBody"></param>
        /// <returns></returns>
        private XmlDocument GetSoapStructure(bool pIsUpdate, object pHeader, object pBody)
        {

            XmlSerializer mySerializer = new XmlSerializer(pBody.GetType(), @"http://www.sap.com/SBO/DIS");
            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);
            xns.Add(string.Empty, @"http://www.sap.com/SBO/DIS");
            MemoryStream streamDocs = new MemoryStream();
            mySerializer.Serialize(streamDocs, pBody, xns);
            XmlDocument mXmlDocument = new XmlDocument();
            mXmlDocument.LoadXml(@"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Header></soap:Header><soap:Body><" + (pIsUpdate ? "UpdateActivity" : "AddActivity") + @" xmlns=""" + pHeader.GetType().GetProperty("ServiceName").GetValue(pHeader, null) + @"""></" + (pIsUpdate ? "UpdateActivity" : "AddActivity") + "></soap:Body></soap:Envelope>");
            XmlElement mBodyElement = mXmlDocument.DocumentElement;

            mBodyElement.SelectSingleNode("//*[local-name()='" + (pIsUpdate ? "UpdateActivity" : "AddActivity") + "']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            streamDocs = new MemoryStream();

            mySerializer = new XmlSerializer(pHeader.GetType(), @"http://www.sap.com/SBO/DIS");
            mySerializer.Serialize(streamDocs, pHeader, xns);

            mBodyElement.SelectSingleNode("//*[local-name()='Header']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            return mXmlDocument;

        }
    }
}