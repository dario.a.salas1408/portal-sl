﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using WebServices.SLService.SAPB1;
using WebServices.Connection;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Dynamic;
using Newtonsoft.Json;

namespace WebServices.Activities
{
    public class ActivityServiceSL : IActivityService
    {
        private B1WSHandler mB1WSHandler;

        public ActivityServiceSL()
        {
            mB1WSHandler = new B1WSHandler();
        }

        public List<ActivitiesSAP> GetActivitiesList(CompanyConn pCompanyParam)
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetActivitiesList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivitiesSAP> GetActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {

                if (!string.IsNullOrEmpty(pModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pModel) + "'), tolower(U_ARGNS_PRDCODE))";
                }

                if (!string.IsNullOrEmpty(pProject))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pProject) + "'), tolower(U_ARGNS_PROYECT))";
                }

                if (!string.IsNullOrEmpty(txtRemarks))
                {
                    mFilters = mFilters + "and substringof(tolower('" + txtRemarks + "'), tolower(Details))";
                }

                if (!string.IsNullOrEmpty(txtStatus))
                {
                    mFilters = mFilters + "and status eq " + txtStatus + " ";
                }
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ActivitiesListSearch", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Where(c => (UserType == -1 ? true : (UserType == 0 ? c.AttendUser != null : (UserType == 1 ? c.AttendEmpl != null : true)))).ToList();

                mRet.ForEach(c =>
                {
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetActivitiesListSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivityStatusSAP> GetActivityStatusList(CompanyConn pCompanyParam)
        {
            List<ActivityStatusSAP> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OCLA");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivityStatusSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                return mRet;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetActivityStatusList :" + ex.Message);
                return null;
            }
        }

        private string GetAttendUserName(short? pAttenUser, int? pAttenEmp, CompanyConn pCompanyParam)
        {
            string ret = string.Empty;
            string mUrl = string.Empty;
            string mFilters = string.Empty;
            try
            {
                if (pAttenUser.HasValue)
                {
                    mFilters = "USERID eq " + pAttenUser.Value + "";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User", mFilters);
                    List<UserSAP> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    ret = mRetList.Single().U_NAME;
                }
                else
                {
                    mFilters = "empID eq " + pAttenEmp.Value + "";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", mFilters);
                    List<EmployeeSAP> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    ret = mRetList.SingleOrDefault().lastName + " " + mRetList.SingleOrDefault().firstName;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetAttendUserName :" + ex.Message);
                return string.Empty;
            }
            return ret;
        }


        private string GetActionName(string pAction)
        {
            string ret = string.Empty;
            try
            {
                switch (pAction)
                {
                    case "C":
                        ret = "Phone Call";
                        break;
                    case "M":
                        ret = "Meeting";
                        break;

                    case "T":
                        ret = "Task";
                        break;
                    case "E":
                        ret = "Notes";
                        break;
                    case "P":
                        ret = "Campaign";
                        break;
                    case "N":
                        ret = "Other";
                        break;
                }

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetActionName :" + ex.Message);
                return string.Empty;
            }


        }

        public ActivitiesSAPCombo GetActivityCombo(CompanyConn pCompanyParam)
        {
            ActivitiesSAPCombo ret = null;
            string mUrl;
            try
            {
                ret = new ActivitiesSAPCombo();

                //Activity Type              
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ActivityType");
                ret.ActivityTypeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivityTypeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                ret.ActivityStatusList = new List<ActivityStatusSAP>();
                ret.ActivityStatusList.Add(new ActivityStatusSAP(-1, ""));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OCLA");
                ret.ActivityStatusList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivityStatusSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Activity Action
                ret.ActivityActionList = new List<ActivityAction>()
                {
                    new ActivityAction(){Code="C",Name="Phone Call"},
                    new ActivityAction(){Code="M",Name="Meeting"},
                    new ActivityAction(){Code="T",Name="Task"},
                    new ActivityAction(){Code="E",Name="Notes"},
                    new ActivityAction(){Code="P",Name="Campaign"},
                    new ActivityAction(){Code="N",Name="Other"},

                };

                //Activity Priority
                ret.ActivityPriorityList = new List<ActivityPriority>()
                {
                    new ActivityPriority(){Code="0",Name="Low"},
                    new ActivityPriority(){Code="1",Name="Normal"},
                    new ActivityPriority(){Code="2",Name="High"},
                };

                //Activity User Type
                ret.ActivityUserTypeList = new List<ActivityUserType>()
                {
                    new ActivityUserType(){Code=0,Name="User"},
                    new ActivityUserType(){Code=1,Name="Employee"},
                };

                //Activity User Type
                ret.ActivityUserList = new List<ActivityUser>();


                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetActivityCombo :" + ex.Message);
                return null;
            }
        }

        public ActivitiesSAP GetActivityById(CompanyConn pCompanyParam, int pCode, bool CheckApparelInstallation)
        {
            ActivitiesSAP ret = null;
            string mUrl;
            string mFilters;
            try
            {
                mFilters = "ClgCode eq " + pCode + "";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                List<ActivitiesSAP> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                ret = mRetList.SingleOrDefault();

                if (ret == null)
                {
                    ret = new ActivitiesSAP();
                }
                ret.ActivityCombo = GetActivityCombo(pCompanyParam);
                ret.UserType = (ret.AttendEmpl != null ? 1 : 0);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetActivityById :" + ex.Message);
                return null;
            }
            return ret;
        }

        public string AddActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {
            ActivitiesServiceWeb.Activity oActivity = new ActivitiesServiceWeb.Activity();

            //ActivitiesServiceWeb.MsgHeader oActivitiesMsgHeader = new ActivitiesServiceWeb.MsgHeader();

            //oActivitiesMsgHeader.ServiceName = ActivitiesServiceWeb.MsgHeaderServiceName.ActivitiesService;
            //oActivitiesMsgHeader.ServiceNameSpecified = true;
            //oActivitiesMsgHeader.SessionID = pCompanyParam.DSSessionId;

            //ActivitiesServiceWeb.ActivitiesService oActivityServiceWeb = new ActivitiesServiceWeb.ActivitiesService();
            //oActivityServiceWeb.MsgHeaderValue = oActivitiesMsgHeader;
            try
            {
                oActivity.ActivityCode = pObject.ClgCode;
                oActivity.CardCode = pObject.CardCode;
                if (pObject.Recontact.HasValue)
                {
                    oActivity.StartDate = pObject.Recontact.Value;
                    oActivity.StartDateSpecified = true;
                }
                if (pObject.BeginTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? 0 : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? pObject.BeginTime.Value : Convert.ToInt32(pObject.BeginTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.StartTime = mDate;
                    oActivity.StartTimeSpecified = true;
                }
                if (pObject.endDate.HasValue)
                {
                    oActivity.EndDueDate = pObject.endDate.Value;
                    oActivity.EndDueDateSpecified = true;
                }
                if (pObject.ENDTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? 0 : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? pObject.ENDTime.Value : Convert.ToInt32(pObject.ENDTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.EndTime = mDate;
                    oActivity.EndTimeSpecified = true;
                }
                if (pObject.status.HasValue)
                {
                    oActivity.Status = pObject.status.Value;
                    oActivity.StatusSpecified = true;
                }

                switch (pObject.Action)
                {
                    case "C":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                    case "M":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Meeting;
                        break;
                    case "T":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Task;
                        break;
                    case "E":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Note;
                        break;
                    case "P":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Campaign;
                        break;
                    case "N":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Other;
                        break;
                    default:
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                }

                oActivity.Activity1Specified = true;

                if (pObject.CntctType.HasValue && pObject.CntctType.Value != 0)
                {
                    oActivity.ActivityType = (long)pObject.CntctType;
                    oActivity.ActivityTypeSpecified = true;
                }

                if (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0)
                    oActivity.Subject = (long)pObject.CntctSbjct;
                oActivity.Notes = pObject.Notes;
                oActivity.Details = pObject.Details;

                switch (pObject.Priority)
                {
                    case "0":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Low;
                        break;
                    case "1":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                    case "2":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_High;
                        break;
                    default:
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                }

                oActivity.PrioritySpecified = true;

                oActivity.Closed = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? ActivitiesServiceWeb.ActivityClosed.tYES : ActivitiesServiceWeb.ActivityClosed.tNO;
                oActivity.InactiveFlag = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? ActivitiesServiceWeb.ActivityInactiveFlag.tYES : ActivitiesServiceWeb.ActivityInactiveFlag.tNO;
                if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
                {
                    oActivity.HandledBy = pObject.AttendUser.Value;
                    oActivity.HandledBySpecified = true;
                }
                if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
                {
                    oActivity.HandledByEmployee = pObject.AttendEmpl.Value;
                    oActivity.HandledByEmployeeSpecified = true;
                }

                //oActivityServiceWeb.AddActivity(oActivity);
                //return "Ok";

                // XmlDocument mXmlDoc = GetSoapStructure(false, oActivitiesMsgHeader, oActivity);

                //I Add some UDF only in case the activity come from Apparel Critical Path, and has setted the Apparel UDFs
                #region Apparel UDFs

                //if (!string.IsNullOrEmpty(pObject.U_ARGNS_PRDCODE))
                //{
                //    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PRDCODE);
                //}
                //if (!string.IsNullOrEmpty(pObject.U_ARGNS_PROYECT))
                //{
                //    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PROYECT);
                //}
                //if (!string.IsNullOrEmpty(pObject.U_ARGNS_WORKF2))
                //{
                //    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_WORKF2);
                //}
                //if (pObject.U_ARGNS_JOBID != null)
                //{
                //    mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_JOBID.ToString());
                //}

                #endregion


                dynamic documentToAdd = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)documentToAdd;

                foreach (var property in oActivity.GetType().GetProperties())
                {
                    if (!property.Name.EndsWith("Specified"))
                    {
                        string value = (property.GetValue(oActivity) ?? "").ToString();

                        if (value != "0" && value != "0.0" && value != "")
                        {
                            if (value == "tYES")
                            {
                                dictionary.Add(property.Name, "Y");
                            }
                            else
                            {
                                if (property.GetValue(oActivity).GetType().Name == "String"
                                    || property.GetValue(oActivity).GetType().Name == "Boolean"
                                    || property.GetValue(oActivity).GetType().Name == "Int32"
                                    || property.GetValue(oActivity).GetType().Name == "Decimal"
                                    || property.GetValue(oActivity).GetType().Name == "Double"
                                    || property.GetValue(oActivity).GetType().Name == "Int64")
                                {
                                    dictionary.Add(property.Name, property.GetValue(oActivity));
                                }
                                else
                                {
                                    switch (property.Name)
                                    {
                                        case "Priority":
                                            dictionary.Add(property.Name, oActivity.Priority);
                                            break;
                                        case "Activity1":
                                            dictionary.Add("Activity", pObject.Action);
                                            break;
                                        case "StartTime":
                                            dictionary.Add(property.Name, oActivity.StartTime);
                                            break;
                                        case "EndTime":
                                            dictionary.Add(property.Name, oActivity.EndTime);
                                            break;
                                        case "StartDate":
                                            dictionary.Add(property.Name, oActivity.StartDate);
                                            break;
                                        case "EndDueDate":
                                            dictionary.Add(property.Name, oActivity.EndDueDate);
                                            break;

                                    }
                                }
                            }
                        }
                    }
                }

                List<dynamic> linesToAdd = new List<dynamic>();

                if (!string.IsNullOrEmpty(pObject.U_ARGNS_PRDCODE))
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_PRDCODE);
                    // mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PRDCODE);
                }
                if (!string.IsNullOrEmpty(pObject.U_ARGNS_PROYECT))
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_PROYECT);
                    //mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PROYECT);
                }
                if (!string.IsNullOrEmpty(pObject.U_ARGNS_WORKF2))
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_WORKF2);
                    // mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_WORKF2);
                }
                if (pObject.U_ARGNS_JOBID != null)
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_JOBID.ToString());
                    //mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_JOBID.ToString());
                }

                var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

                var result = SLManager.callAPI(pCompanyParam, "Activities", RestSharp.Method.POST, jsonToAdd);

                var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                if (errorAPI.error != null)
                {
                    return "Error:" + errorAPI.error.message.value;
                }

                //string mResponse = mB1WSHandler.ProcessActivityWithError(mXmlDoc);

                //return "Ok" + ";" + mResponse;
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> AddActivity :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string UpdateActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam)
        {

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            var result2 = SLManager.callAPI(pCompanyParam, $"Activities({pObject.ClgCode})", RestSharp.Method.GET, "");
            var docSAP = JsonConvert.DeserializeObject<ActivitiesServiceWeb.Activity>(result2.Content, settings);
            ActivitiesServiceWeb.Activity oActivity = new ActivitiesServiceWeb.Activity();
            

            //ActivitiesServiceWeb.Activity oActivity = new ActivitiesServiceWeb.Activity();
            //ActivitiesServiceWeb.ActivityParams oActivityParams = new ActivitiesServiceWeb.ActivityParams();
            //ActivitiesServiceWeb.MsgHeader oActivitiesMsgHeader = new ActivitiesServiceWeb.MsgHeader();

            //oActivitiesMsgHeader.ServiceName = ActivitiesServiceWeb.MsgHeaderServiceName.ActivitiesService;
            //oActivitiesMsgHeader.ServiceNameSpecified = true;
            //oActivitiesMsgHeader.SessionID = pCompanyParam.DSSessionId;

            //ActivitiesServiceWeb.ActivitiesService oActivityServiceWeb = new ActivitiesServiceWeb.ActivitiesService();
            //oActivityServiceWeb.MsgHeaderValue = oActivitiesMsgHeader;

            try
            {
                //oActivityParams.ActivityCodeSpecified = true;
                //oActivityParams.ActivityCode = pObject.ClgCode;
                //oActivity = oActivityServiceWeb.GetActivity(oActivityParams);

                if (pObject.Recontact.HasValue)
                {
                    oActivity.StartDate = pObject.Recontact.Value;
                    oActivity.StartDateSpecified = true;
                }
                if (pObject.BeginTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? 0 : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.BeginTime < 1000) ? ((pObject.BeginTime < 100) ? pObject.BeginTime.Value : Convert.ToInt32(pObject.BeginTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.BeginTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.StartTime = mDate;
                    oActivity.StartTimeSpecified = true;
                }
                if (pObject.endDate.HasValue)
                {
                    oActivity.EndDueDate = pObject.endDate.Value;
                    oActivity.EndDueDateSpecified = true;
                }
                if (pObject.ENDTime.HasValue)
                {
                    DateTime mDate = new DateTime();
                    int mHour = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? 0 : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 1))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(0, 2)));
                    int mMinutes = ((pObject.ENDTime < 1000) ? ((pObject.ENDTime < 100) ? pObject.ENDTime.Value : Convert.ToInt32(pObject.ENDTime.ToString().Substring(1, 2))) : Convert.ToInt32(pObject.ENDTime.ToString().Substring(2, 2)));
                    mDate = mDate.AddHours(mHour);
                    mDate = mDate.AddMinutes(mMinutes);
                    oActivity.EndTime = mDate;
                    oActivity.EndTimeSpecified = true;
                }
                if (pObject.status.HasValue)
                {
                    oActivity.Status = pObject.status.Value;
                    oActivity.StatusSpecified = true;
                }

                switch (pObject.Action)
                {
                    case "C":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                    case "M":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Meeting;
                        break;
                    case "T":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Task;
                        break;
                    case "E":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Note;
                        break;
                    case "P":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Campaign;
                        break;
                    case "N":
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Other;
                        break;
                    default:
                        oActivity.Activity1 = ActivitiesServiceWeb.ActivityActivity.cn_Conversation;
                        break;
                }

                if (pObject.CntctType.HasValue && pObject.CntctType.Value != 0)
                    oActivity.ActivityType = (long)pObject.CntctType;

                if (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0)
                    oActivity.Subject = (long)pObject.CntctSbjct;
                oActivity.Notes = pObject.Notes;
                oActivity.Details = pObject.Details;

                switch (pObject.Priority)
                {
                    case "0":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Low;
                        break;
                    case "1":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                    case "2":
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_High;
                        break;
                    default:
                        oActivity.Priority = ActivitiesServiceWeb.ActivityPriority.pr_Normal;
                        break;
                }

                oActivity.PrioritySpecified = true;

                oActivity.Closed = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? ActivitiesServiceWeb.ActivityClosed.tYES : ActivitiesServiceWeb.ActivityClosed.tNO;
                oActivity.InactiveFlag = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? ActivitiesServiceWeb.ActivityInactiveFlag.tYES : ActivitiesServiceWeb.ActivityInactiveFlag.tNO;
                if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
                {
                    oActivity.HandledBy = pObject.AttendUser.Value;
                    oActivity.HandledBySpecified = true;
                }
                if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
                {
                    oActivity.HandledByEmployee = pObject.AttendEmpl.Value;
                    oActivity.HandledByEmployeeSpecified = true;
                }

                //oActivityServiceWeb.UpdateActivity(oActivity);

                dynamic documentToAdd = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)documentToAdd;

                foreach (var property in oActivity.GetType().GetProperties())
                {
                    //dictionary.Add(property.Name, property.GetValue(oActivity));

                    if (!property.Name.EndsWith("Specified"))
                    {
                        string value = (property.GetValue(oActivity) ?? "").ToString();

                        if (value != "0" && value != "0.0" && value != "")
                        {
                            if (value == "tYES")
                            {
                                dictionary.Add(property.Name, "Y");
                            }
                            else
                            {
                                if (property.GetValue(oActivity).GetType().Name == "String"
                                    || property.GetValue(oActivity).GetType().Name == "Boolean"
                                    || property.GetValue(oActivity).GetType().Name == "Int32"
                                    || property.GetValue(oActivity).GetType().Name == "Decimal"
                                    || property.GetValue(oActivity).GetType().Name == "Double"
                                    || property.GetValue(oActivity).GetType().Name == "Int64")
                                {
                                    dictionary.Add(property.Name, property.GetValue(oActivity));
                                }
                                else
                                {
                                    switch (property.Name)
                                    {
                                        case "Priority":
                                            dictionary.Add(property.Name, oActivity.Priority);
                                            break;
                                        case "Activity1":
                                            dictionary.Add("Activity", pObject.Action);
                                            break;
                                        case "StartTime":
                                            dictionary.Add(property.Name, oActivity.StartTime);
                                            break;
                                        case "EndTime":
                                            dictionary.Add(property.Name, oActivity.EndTime);
                                            break;
                                        case "StartDate":
                                            dictionary.Add(property.Name, oActivity.StartDate);
                                            break;
                                        case "EndDueDate":
                                            dictionary.Add(property.Name, oActivity.EndDueDate);
                                            break;

                                    }
                                }
                            }
                        }
                    }
                }

                List<dynamic> linesToAdd = new List<dynamic>();

                if (!string.IsNullOrEmpty(pObject.U_ARGNS_PRDCODE))
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_PRDCODE);
                    // mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PRDCODE);
                }
                if (!string.IsNullOrEmpty(pObject.U_ARGNS_PROYECT))
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_PROYECT);
                    //mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_PROYECT);
                }
                if (!string.IsNullOrEmpty(pObject.U_ARGNS_WORKF2))
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_WORKF2);
                    // mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_WORKF2);
                }
                if (pObject.U_ARGNS_JOBID != null)
                {
                    dictionary.Add("U_ARGNS_PRDCODE", pObject.U_ARGNS_JOBID.ToString());
                    //mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Activity']").InnerXml, "U_ARGNS_PRDCODE", pObject.U_ARGNS_JOBID.ToString());
                }

                var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

                var result = SLManager.callAPI(pCompanyParam, $"Activities({pObject.ClgCode})", RestSharp.Method.PATCH, jsonToAdd);

                var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                if (errorAPI != null)
                {
                    return "Error:" + errorAPI.error.message.value;
                }
                
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> UpdateActivity :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string UpdateSalesOpportunitiesActivity(int pActivityId, int? pOpporId, short? pLine, CompanyConn pCompanyParam)
        {
            return null;
        }

        public List<ActivitiesSAP> GetCRMActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode)
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            string mFilters = "1 eq 1 ";

            if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
            {
                mFilters = mFilters + "and ClgCode eq " + txtCode + " ";
            }
            if (!string.IsNullOrEmpty(txtAction) && txtAction != "-1")
            {
                mFilters = mFilters + "and substringof(tolower('" + txtAction + "'), tolower(Action)) ";
            }
            if (!string.IsNullOrEmpty(txtType) && txtType != "-2")
            {
                mFilters = mFilters + "and CntctType eq " + txtType + " ";
            }
            if (txtStartDate != null)
            {
                mFilters = mFilters + "and  Recontact eq datetime'" + txtStartDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (txtEndDate != null)
            {
                mFilters = mFilters + "and  endDate eq datetime'" + txtEndDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1")
            {
                mFilters = mFilters + "and Priority eq '" + txtPriority + "' ";
            }
            if (!string.IsNullOrEmpty(txtRemarks))
            {
                mFilters = mFilters + "and substringof(tolower('" + txtRemarks + "'), tolower(Details)) ";
            }
            if (!string.IsNullOrEmpty(txtStatus))
            {
                mFilters = mFilters + "and status eq " + txtStatus + " ";
            }
            if (!string.IsNullOrEmpty(txtBPCode))
            {
                mFilters = mFilters + "and CardCode eq '" + txtBPCode + "' ";
            }
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Where(c => (UserType == -1 ? true : (UserType == 0 ? c.AttendUser != null : (UserType == 1 ? c.AttendEmpl != null : true)))).ToList();

                mRet.ForEach(c =>
                {
                    mFilters = "1 eq 1 and substringof(tolower('" + c.CardCode + "'), tolower(CardCode))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                    BusinessPartnerSAP mBPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    c.CardName = (mBPAux != null ? mBPAux.CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetCRMActivitiesListSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivitiesSAP> GetBPCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            string mFilters = "substringof(tolower('" + pBPCode + "'), tolower(CardCode)) ";

            if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
            {
                mFilters = mFilters + "and ClgCode eq " + txtCode + " ";
            }
            if (!string.IsNullOrEmpty(txtAction) && txtAction != "-1")
            {
                mFilters = mFilters + "and substringof(tolower('" + txtAction + "'), tolower(Action)) ";
            }
            if (!string.IsNullOrEmpty(txtType) && txtType != "-2")
            {
                mFilters = mFilters + "and CntctType eq " + txtType + " ";
            }
            if (txtStartDate != null)
            {
                mFilters = mFilters + "and  Recontact eq datetime'" + txtStartDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (txtEndDate != null)
            {
                mFilters = mFilters + "and  endDate eq datetime'" + txtEndDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1")
            {
                mFilters = mFilters + "and Priority eq " + txtPriority + " ";
            }
            if (!string.IsNullOrEmpty(txtRemarks))
            {
                mFilters = mFilters + "and substringof(tolower('" + txtRemarks + "'), tolower(Details)) ";
            }
            if (!string.IsNullOrEmpty(txtStatus))
            {
                mFilters = mFilters + "and status eq " + txtStatus + " ";
            }
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Where(c => (UserType == -1 ? true : (UserType == 0 ? c.AttendUser != null : (UserType == 1 ? c.AttendEmpl != null : true)))).ToList();

                mRet.ForEach(c =>
                {
                    mFilters = "1 eq 1 and substringof(tolower('" + c.CardCode + "'), tolower(CardCode))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                    BusinessPartnerSAP mBPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    c.CardName = (mBPAux != null ? mBPAux.CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetBPCRMActivitiesSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivitiesSAP> GetUserCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "userId eq " + pUserId);
            List<EmployeeSAP> mListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            string mFilters = "AttendUser eq " + pUserId + " ";

            string mSecFilter = string.Empty;

            foreach (EmployeeSAP mEmployee in mListEmployee)
            {
                if (string.IsNullOrEmpty(mSecFilter))
                {
                    mSecFilter = " AttendEmpl eq " + mEmployee.empID;
                }
                else
                {
                    mSecFilter = mSecFilter + " or AttendEmpl eq " + mEmployee.empID;
                }

            }
            if (!string.IsNullOrEmpty(mSecFilter))
                mFilters = mFilters + " or (" + mSecFilter + ")";

            if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
            {
                mFilters = mFilters + " and ClgCode eq " + txtCode + " ";
            }
            if (!string.IsNullOrEmpty(txtAction) && txtAction != "-1")
            {
                mFilters = mFilters + " and substringof(tolower('" + txtAction + "'), tolower(Action)) ";
            }
            if (!string.IsNullOrEmpty(txtType) && txtType != "-2")
            {
                mFilters = mFilters + " and CntctType eq " + txtType + " ";
            }
            if (txtStartDate != null)
            {
                mFilters = mFilters + " and  Recontact eq datetime'" + txtStartDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (txtEndDate != null)
            {
                mFilters = mFilters + " and  endDate eq datetime'" + txtEndDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1")
            {
                mFilters = mFilters + " and Priority eq " + txtPriority + " ";
            }
            if (!string.IsNullOrEmpty(txtRemarks))
            {
                mFilters = mFilters + " and substringof(tolower('" + txtRemarks + "'), tolower(Details)) ";
            }
            if (!string.IsNullOrEmpty(txtStatus))
            {
                mFilters = mFilters + " and status eq " + txtStatus + " ";
            }
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Where(c => (UserType == -1 ? true : (UserType == 0 ? c.AttendUser != null : (UserType == 1 ? c.AttendEmpl != null : true)))).ToList();

                mRet.ForEach(c =>
                {
                    mFilters = "1 eq 1 and substringof(tolower('" + c.CardCode + "'), tolower(CardCode))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                    BusinessPartnerSAP mBPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    c.CardName = (mBPAux != null ? mBPAux.CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetUserCRMActivitiesSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivitiesSAP> GetSalesEmployeeCRMActivitiesSeach(CompanyConn pCompanyParam, int UserType, int pSECode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks)
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "salesPrson eq " + pSECode);
            List<EmployeeSAP> mListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            string mFilters = "1 eq 1 ";

            string mSecFilter = string.Empty;

            foreach (EmployeeSAP mEmployee in mListEmployee)
            {
                if (string.IsNullOrEmpty(mSecFilter))
                {
                    mSecFilter = " AttendEmpl eq " + mEmployee.empID;
                }
                else
                {
                    mSecFilter = mSecFilter + " or AttendEmpl eq " + mEmployee.empID;
                }

            }
            if (!string.IsNullOrEmpty(mSecFilter))
                mFilters = mFilters + " and (" + mSecFilter + ")";
            else
            {
                mFilters = mFilters + " and (1 eq 2)";
            }

            if (!string.IsNullOrEmpty(txtCode) && txtCode != "0")
            {
                mFilters = mFilters + " and ClgCode eq " + txtCode + " ";
            }
            if (!string.IsNullOrEmpty(txtAction) && txtAction != "-1")
            {
                mFilters = mFilters + " and substringof(tolower('" + txtAction + "'), tolower(Action)) ";
            }
            if (!string.IsNullOrEmpty(txtType) && txtType != "-2")
            {
                mFilters = mFilters + " and CntctType eq " + txtType + " ";
            }
            if (txtStartDate != null)
            {
                mFilters = mFilters + " and  Recontact eq datetime'" + txtStartDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (txtEndDate != null)
            {
                mFilters = mFilters + " and  endDate eq datetime'" + txtEndDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (!string.IsNullOrEmpty(txtPriority) && txtPriority != "-1")
            {
                mFilters = mFilters + " and Priority eq " + txtPriority + " ";
            }
            if (!string.IsNullOrEmpty(txtRemarks))
            {
                mFilters = mFilters + " and substringof(tolower('" + txtRemarks + "'), tolower(Details)) ";
            }
            if (!string.IsNullOrEmpty(txtStatus))
            {
                mFilters = mFilters + " and status eq " + txtStatus + " ";
            }
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Where(c => (UserType == -1 ? true : (UserType == 0 ? c.AttendUser != null : (UserType == 1 ? c.AttendEmpl != null : true)))).ToList();

                mRet.ForEach(c =>
                {
                    mFilters = "1 eq 1 and substringof(tolower('" + c.CardCode + "'), tolower(CardCode))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                    BusinessPartnerSAP mBPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    c.CardName = (mBPAux != null ? mBPAux.CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetSalesEmployeeCRMActivitiesSeach :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivitiesSAP> GetCRMActivitiesListFromTo(CompanyConn pCompanyParam, DateTime? txtStartDate, DateTime? txtEndDate, int pUserId)
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            string mFilters = "AttendUser eq " + pUserId + " ";

            if (txtStartDate != null)
            {
                mFilters = mFilters + "and  Recontact gt datetime'" + txtStartDate.Value.ToString("yyyy-MM-dd") + "' ";
            }
            if (txtEndDate != null)
            {
                mFilters = mFilters + "and  Recontact lt datetime'" + txtEndDate.Value.ToString("yyyy-MM-dd") + "' ";
            }

            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mRet.ForEach(c =>
                {
                    mFilters = "1 eq 1 and substringof(tolower('" + c.CardCode + "'), tolower(CardCode))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                    BusinessPartnerSAP mBPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    c.CardName = (mBPAux != null ? mBPAux.CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetCRMActivitiesListFromTo :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivitiesSAP> GetBPMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            string mFilters = "substringof(tolower('" + pBPCode + "'), tolower(CardCode)) ";
            try
            {

                if (!string.IsNullOrEmpty(pModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pModel) + "'), tolower(U_ARGNS_PRDCODE))";
                }

                if (!string.IsNullOrEmpty(pProject))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pProject) + "'), tolower(U_ARGNS_PROYECT))";
                }

                if (!string.IsNullOrEmpty(txtRemarks))
                {
                    mFilters = mFilters + "and substringof(tolower('" + txtRemarks + "'), tolower(Details))";
                }

                if (!string.IsNullOrEmpty(txtStatus))
                {
                    mFilters = mFilters + "and status eq " + txtStatus + " ";
                }
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ActivitiesListSearch", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Where(c => (UserType == -1 ? true : (UserType == 0 ? c.AttendUser != null : (UserType == 1 ? c.AttendEmpl != null : true)))).ToList();

                mRet.ForEach(c =>
                {
                    mFilters = "1 eq 1 and substringof(tolower('" + c.CardCode + "'), tolower(CardCode))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                    BusinessPartnerSAP mBPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    c.CardName = (mBPAux != null ? mBPAux.CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetActivitiesListSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ActivitiesSAP> GetUserMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "")
        {
            List<ActivitiesSAP> mRet = null;
            string mUrl;
            string mFilters = "AttendUser eq " + pUserId + " ";
            try
            {

                if (!string.IsNullOrEmpty(pModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pModel) + "'), tolower(U_ARGNS_PRDCODE))";
                }

                if (!string.IsNullOrEmpty(pProject))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pProject) + "'), tolower(U_ARGNS_PROYECT))";
                }

                if (!string.IsNullOrEmpty(txtRemarks))
                {
                    mFilters = mFilters + "and substringof(tolower('" + txtRemarks + "'), tolower(Details))";
                }

                if (!string.IsNullOrEmpty(txtStatus))
                {
                    mFilters = mFilters + "and status eq " + txtStatus + " ";
                }
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ActivitiesListSearch", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Where(c => (UserType == -1 ? true : (UserType == 0 ? c.AttendUser != null : (UserType == 1 ? c.AttendEmpl != null : true)))).ToList();

                mRet.ForEach(c =>
                {
                    mFilters = "1 eq 1 and substringof(tolower('" + c.CardCode + "'), tolower(CardCode))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                    BusinessPartnerSAP mBPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    c.CardName = (mBPAux != null ? mBPAux.CardName : "");
                    c.ActionName = GetActionName(c.Action);
                    //para este foreach traer toda la ousr o ohem inicializandola primero como null y buscar en base a eso (hay que crear un web service que traiga toda la ousr y toda la ohem.)
                    c.AssignedUserName = GetAttendUserName(c.AttendUser, c.AttendEmpl, pCompanyParam);
                    c.UserType = (c.AttendEmpl != null ? 1 : 0);
                });

            }
            catch (Exception ex)
            {
                Logger.WriteError("ActivityServiceSL -> GetUserMyActivitiesSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        private XmlDocument GetSoapStructure(bool pIsUpdate, object pHeader, object pBody)
        {

            XmlSerializer mySerializer = new XmlSerializer(pBody.GetType(), @"http://www.sap.com/SBO/DIS");
            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);
            xns.Add(string.Empty, @"http://www.sap.com/SBO/DIS");
            MemoryStream streamDocs = new MemoryStream();
            mySerializer.Serialize(streamDocs, pBody, xns);
            XmlDocument mXmlDocument = new XmlDocument();
            mXmlDocument.LoadXml(@"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Header></soap:Header><soap:Body><" + (pIsUpdate ? "UpdateActivity" : "AddActivity") + @" xmlns=""" + pHeader.GetType().GetProperty("ServiceName").GetValue(pHeader, null) + @"""></" + (pIsUpdate ? "UpdateActivity" : "AddActivity") + "></soap:Body></soap:Envelope>");
            XmlElement mBodyElement = mXmlDocument.DocumentElement;

            mBodyElement.SelectSingleNode("//*[local-name()='" + (pIsUpdate ? "UpdateActivity" : "AddActivity") + "']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            streamDocs = new MemoryStream();

            mySerializer = new XmlSerializer(pHeader.GetType(), @"http://www.sap.com/SBO/DIS");
            mySerializer.Serialize(streamDocs, pHeader, xns);

            mBodyElement.SelectSingleNode("//*[local-name()='Header']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            return mXmlDocument;

        }
    }
}