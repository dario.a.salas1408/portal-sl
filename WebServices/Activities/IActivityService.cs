﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebServices.Activities
{
    [ServiceContract]
    public interface IActivityService
    {
        [OperationContract]
        List<ActivitiesSAP> GetActivitiesList(CompanyConn pCompanyParam);

        [OperationContract]
        List<ActivitiesSAP> GetActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "");

        [OperationContract]
        List<ActivityStatusSAP> GetActivityStatusList(CompanyConn pCompanyParam);

        [OperationContract]
        ActivitiesSAP GetActivityById(CompanyConn pCompanyParam, int pCode, bool CheckApparelInstallation);

        [OperationContract]
        string AddActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateActivity(ActivitiesSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateSalesOpportunitiesActivity(int pActivityId, int? pOpporId, short? pLine, CompanyConn pCompanyParam);

        [OperationContract]
        List<ActivitiesSAP> GetCRMActivitiesListSearch(CompanyConn pCompanyParam, int UserType, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks, string txtBPCode);

        [OperationContract]
        List<ActivitiesSAP> GetUserCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks);

        [OperationContract]
        List<ActivitiesSAP> GetBPCRMActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks);

        [OperationContract]
        List<ActivitiesSAP> GetSalesEmployeeCRMActivitiesSeach(CompanyConn pCompanyParam, int UserType, int pSECode, string txtCode, string txtAction, string txtType, DateTime? txtStartDate, DateTime? txtEndDate, string txtPriority, string txtStatus, string txtRemarks);

        [OperationContract]
        ActivitiesSAPCombo GetActivityCombo(CompanyConn pCompanyParam);

        [OperationContract]
        List<ActivitiesSAP> GetCRMActivitiesListFromTo(CompanyConn pCompanyParam, DateTime? txtStartDate, DateTime? txtEndDate, int pUserId);

        [OperationContract]
        List<ActivitiesSAP> GetBPMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, string pBPCode, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "");

        [OperationContract]
        List<ActivitiesSAP> GetUserMyActivitiesSearch(CompanyConn pCompanyParam, int UserType, int pUserId, string pModel = "", string pProject = "", string txtStatus = "", string txtRemarks = "");

    }
}
