﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using WebServices.Model;
using WebServices.Model.Tables;

namespace WebServices.ServiceCalls
{
    public class ServiceCallServiceDS : IServiceCallService
    {
        private DataBase mDbContext;

        public ServiceCallServiceDS()
        {
            mDbContext = new DataBase();
        }

        public ServiceCallComboList GetServiceCallComboList(CompanyConn pCompanyParam)
        {
            ServiceCallComboList ret = null;
            try
            {
                Mapper.CreateMap<OSCT, CallType>();
                Mapper.CreateMap<OSC, ServiceStatus>();
                Mapper.CreateMap<OSCP, ServiceProblemType>();
                Mapper.CreateMap<OPST, ServiceProblemSubType>();
                Mapper.CreateMap<OSCO, ServiceOrigin>();
                Mapper.CreateMap<OUSR, UserSAP>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                Mapper.CreateMap<OITB, ItemGroupSAP>();


                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = new ServiceCallComboList();

                //CallType
                ret.CallTypeList.AddRange(Mapper.Map<List<CallType>>(mDbContext.OSCT.ToList()));

                //Service Status
                ret.ServiceStatusList.AddRange(Mapper.Map<List<ServiceStatus>>(mDbContext.OSCS.ToList()));

                //Service Problem Type
                ret.ServiceProblemTypeList.AddRange(Mapper.Map<List<ServiceProblemType>>(mDbContext.OSCP.ToList()));

                //Service Problem SubType
                ret.ServiceProblemSubTypeList.AddRange(Mapper.Map<List<ServiceProblemSubType>>(mDbContext.OPST.ToList()));

                //Service Origin
                ret.ServiceOriginList.AddRange(Mapper.Map<List<ServiceOrigin>>(mDbContext.OSCO.ToList()));

                //Service Handled By
                ret.HandledByList = Mapper.Map<List<UserSAP>>(mDbContext.OUSR.ToList());

                //Service Technician
                var mTechnician = (from mEmplo in mDbContext.OHEM
                                   join mRol in mDbContext.HEM6 on mEmplo.empID equals mRol.empID
                                   where mRol.roleID == -2
                                   select new EmployeeSAP()
                                   {
                                       empID = mEmplo.empID,
                                       firstName = mEmplo.firstName,
                                       lastName = mEmplo.lastName,
                                       completeName = mEmplo.lastName + " " + mEmplo.firstName

                                   }).ToList();

                ret.TechnicianList.AddRange(mTechnician);

                //Service Priority               
                ret.ServicePriorityList.Add(new ServicePriority() { Code = "L", Name = "Low" });
                ret.ServicePriorityList.Add(new ServicePriority() { Code = "M", Name = "Medium" });
                ret.ServicePriorityList.Add(new ServicePriority() { Code = "H", Name = "High" });

                ret.ItemGroupList.AddRange(Mapper.Map<List<ItemGroupSAP>>(mDbContext.OITB.ToList()));

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
            return ret;

        }

        public ServiceCallSAP GetServiceCallById(CompanyConn pCompanyParam, int pId)
        {
            ServiceCallSAP ret = null;
            try
            {
                Mapper.CreateMap<OSCL, ServiceCallSAP>();
                Mapper.CreateMap<OCPR, ContacPerson>();
                Mapper.CreateMap<SCL5, ServiceCallActivities>();
                Mapper.CreateMap<OCLG, ActivitiesSAP>();
                Mapper.CreateMap<ATC1, AttachmentSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = Mapper.Map<ServiceCallSAP>(mDbContext.OSCL.Where(c => c.callID == pId).SingleOrDefault());
                if (ret != null)
                {
                    //Contact Person
                    List<OCPR> mListOCPR = mDbContext.OCPR.Where(c => c.CardCode == ret.customer).ToList();
                    ret.ContactPersonList = Mapper.Map<List<ContacPerson>>(mListOCPR);
                    ret.CustomerPhone = mDbContext.OCRD.Where(c => c.CardCode == ret.customer).SingleOrDefault().Phone1;

                    //Activities List
                    ret.ActivitiesList = Mapper.Map<List<ServiceCallActivities>>(mDbContext.SCL5.Where(c => c.SrvcCallId == ret.callID).ToList());

                    //Attachment List
                    ret.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mDbContext.ATC1.Where(c => c.AbsEntry == ret.AtcEntry).ToList());
                }
                else
                {
                    ret = new ServiceCallSAP();
                }
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
            return ret;
        }

        private int GetNextServiceCallId(CompanyConn pCompanyParam)
        {
            int ret = 0;
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = mDbContext.OSCL.Max(c => c.callID) + 1;
            }
            catch (Exception)
            {
                ret = 0;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
            return ret;
        }

        public JsonObjectResult AddServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            ServiceCallServiceWeb.MsgHeader header = new ServiceCallServiceWeb.MsgHeader();
            header.ServiceName = ServiceCallServiceWeb.MsgHeaderServiceName.ServiceCallsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            ServiceCallServiceWeb.ServiceCallsService oServiceCall = new ServiceCallServiceWeb.ServiceCallsService();
            oServiceCall.MsgHeaderValue = header;

            ServiceCallServiceWeb.ServiceCall mServiceObject = new ServiceCallServiceWeb.ServiceCall();

            mServiceObject.CustomerCode = pObject.customer;
            mServiceObject.CustomerName = pObject.custmrName;
            mServiceObject.CustomerRefNo = pObject.NumAtCard;
            mServiceObject.Subject = pObject.subject;
            mServiceObject.ItemCode = pObject.itemCode;
            mServiceObject.ItemDescription = pObject.itemName;
            mServiceObject.Resolution = pObject.resolution;
            mServiceObject.Description = pObject.descrption;


            if (pObject.createDate.HasValue)
            {
                mServiceObject.CreationDate = pObject.createDate.Value;
                mServiceObject.CreationDateSpecified = true;
            }

            if (pObject.itemGroup.HasValue)
            {
                if (pObject.itemGroup.Value != 0)
                {
                    mServiceObject.ItemGroupCodeSpecified = true;
                    mServiceObject.ItemGroupCode = pObject.itemGroup.Value;

                }

            }

            if (pObject.contractID.HasValue)
            {
                mServiceObject.ContractID = pObject.contractID.Value;
                mServiceObject.ContractIDSpecified = true;
            }

            if (pObject.status.HasValue)
            {
                mServiceObject.Status = pObject.status.Value;
                mServiceObject.StatusSpecified = true;
            }

            mServiceObject.PrioritySpecified = true;
            switch (pObject.priority)
            {
                case "L":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Low;
                    break;
                case "M":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Medium;
                    break;
                case "H":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_High;
                    break;
            }

            if (pObject.origin.HasValue)
            {
                if (pObject.origin.Value != 0)
                {
                    mServiceObject.Origin = pObject.origin.Value;
                    mServiceObject.OriginSpecified = true;
                }
            }

            if (pObject.problemTyp.HasValue)
            {
                if (pObject.problemTyp.Value != 0)
                {
                    mServiceObject.ProblemType = pObject.problemTyp.Value;
                    mServiceObject.ProblemTypeSpecified = true;
                }

            }

            if (pObject.ProSubType.HasValue)
            {
                if (pObject.ProSubType.Value != 0)
                {
                    mServiceObject.ProblemSubType = pObject.ProSubType.Value;
                    mServiceObject.ProblemSubTypeSpecified = true;
                }
            }

            if (pObject.callType.HasValue)
            {
                if (pObject.callType.Value != 0)
                {
                    mServiceObject.CallType = pObject.callType.Value;
                    mServiceObject.CallTypeSpecified = true;
                }
            }

            if (pObject.technician.HasValue)
            {
                if (pObject.technician.Value != 0)
                {
                    mServiceObject.TechnicianCode = pObject.technician.Value;
                    mServiceObject.TechnicianCodeSpecified = true;
                }

            }

            if (pObject.assignee.HasValue)
            {
                mServiceObject.AssigneeCode = pObject.assignee.Value;
                mServiceObject.AssigneeCodeSpecified = true;
            }

            if (pObject.contctCode.HasValue)
            {
                if (pObject.contctCode.Value != 0)
                {
                    mServiceObject.ContactCode = pObject.contctCode.Value;
                    mServiceObject.ContactCodeSpecified = true;
                }
            }

            //Creo las lineas de Actividades
            ServiceCallServiceWeb.ServiceCallServiceCallActivity[] newLines = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[pObject.ActivitiesList.Count];
            int positionNewLine = 0;
            foreach (ServiceCallActivities portalLine in pObject.ActivitiesList)
            {
                ServiceCallServiceWeb.ServiceCallServiceCallActivity line = new ServiceCallServiceWeb.ServiceCallServiceCallActivity();

                line.ActivityCode = portalLine.ClgID.Value;
                line.ActivityCodeSpecified = true;

                line.LineNum = portalLine.Line;
                line.LineNumSpecified = true;

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mServiceObject.ServiceCallActivities = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[newLines.Length];
            mServiceObject.ServiceCallActivities = newLines;            

            try
            {
                ServiceCallServiceWeb.ServiceCallParams parametros = new ServiceCallServiceWeb.ServiceCallParams();
                parametros = oServiceCall.Add(mServiceObject);

                mJsonObjectResult.Code = parametros.ServiceCallID.ToString();
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                //Attachment
                if (pObject.AttachmentList.Count > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>Y</Override></row>";
                    }
                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    mServiceObject = oServiceCall.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        mServiceObject.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        mServiceObject.AttachmentEntrySpecified = true;
                    }

                    oServiceCall.Update(mServiceObject);
                }
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public string UpdateServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {
            ServiceCallServiceWeb.MsgHeader header = new ServiceCallServiceWeb.MsgHeader();
            header.ServiceName = ServiceCallServiceWeb.MsgHeaderServiceName.ServiceCallsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            ServiceCallServiceWeb.ServiceCallsService oServiceCall = new ServiceCallServiceWeb.ServiceCallsService();
            oServiceCall.MsgHeaderValue = header;

            ServiceCallServiceWeb.ServiceCallParams parametros = new ServiceCallServiceWeb.ServiceCallParams();
            parametros.ServiceCallID = pObject.callID;
            parametros.ServiceCallIDSpecified = true;

            ServiceCallServiceWeb.ServiceCall mServiceObject = oServiceCall.GetByParams(parametros);

            mServiceObject.CustomerCode = pObject.customer;
            mServiceObject.CustomerName = pObject.custmrName;
            mServiceObject.CustomerRefNo = pObject.NumAtCard;
            mServiceObject.Subject = pObject.subject;
            mServiceObject.ItemCode = pObject.itemCode;
            mServiceObject.ItemDescription = pObject.itemName;
            mServiceObject.Resolution = pObject.resolution;
            mServiceObject.Description = pObject.descrption;

            if (pObject.createDate.HasValue)
            {
                mServiceObject.CreationDate = pObject.createDate.Value;
                mServiceObject.CreationDateSpecified = true;
            }

            if (pObject.contractID.HasValue)
            {
                mServiceObject.ContractID = pObject.contractID.Value;
                mServiceObject.ContractIDSpecified = true;
            }

            if (pObject.status.HasValue)
            {
                mServiceObject.Status = pObject.status.Value;
                mServiceObject.StatusSpecified = true;
            }

            mServiceObject.PrioritySpecified = true;
            switch (pObject.priority)
            {
                case "L":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Low;
                    break;
                case "M":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Medium;
                    break;
                case "H":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_High;
                    break;
            }

            if (pObject.origin.HasValue)
            {
                if (pObject.origin.Value != 0)
                {
                    mServiceObject.Origin = pObject.origin.Value;
                    mServiceObject.OriginSpecified = true;
                }
            }

            if (pObject.problemTyp.HasValue)
            {
                if (pObject.problemTyp.Value != 0)
                {
                    mServiceObject.ProblemType = pObject.problemTyp.Value;
                    mServiceObject.ProblemTypeSpecified = true;
                }

            }

            if (pObject.ProSubType.HasValue)
            {
                if (pObject.ProSubType.Value != 0)
                {
                    mServiceObject.ProblemSubType = pObject.ProSubType.Value;
                    mServiceObject.ProblemSubTypeSpecified = true;
                }
            }

            if (pObject.callType.HasValue)
            {
                if (pObject.callType.Value != 0)
                {
                    mServiceObject.CallType = pObject.callType.Value;
                    mServiceObject.CallTypeSpecified = true;
                }
            }

            if (pObject.technician.HasValue)
            {
                if (pObject.technician.Value != 0)
                {
                    mServiceObject.TechnicianCode = pObject.technician.Value;
                    mServiceObject.TechnicianCodeSpecified = true;
                }

            }

            if (pObject.assignee.HasValue)
            {
                mServiceObject.AssigneeCode = pObject.assignee.Value;
                mServiceObject.AssigneeCodeSpecified = true;
            }

            if (pObject.contctCode.HasValue)
            {
                if (pObject.contctCode.Value != 0)
                {
                    mServiceObject.ContactCode = pObject.contctCode.Value;
                    mServiceObject.ContactCodeSpecified = true;
                }
            }

            if (pObject.itemGroup.HasValue)
            {
                if (pObject.itemGroup.Value != 0)
                {
                    mServiceObject.ItemGroupCodeSpecified = true;
                    mServiceObject.ItemGroupCode = pObject.itemGroup.Value;
                }
            }

            //Creo las lineas de Actividades
            ServiceCallServiceWeb.ServiceCallServiceCallActivity[] newLines = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[pObject.ActivitiesList.Count];
            int positionNewLine = 0;
            foreach (ServiceCallActivities portalLine in pObject.ActivitiesList)
            {
                ServiceCallServiceWeb.ServiceCallServiceCallActivity line = new ServiceCallServiceWeb.ServiceCallServiceCallActivity();

                line.ActivityCode = portalLine.ClgID.Value;
                line.ActivityCodeSpecified = true;

                line.LineNum = portalLine.Line;
                line.LineNumSpecified = true;

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mServiceObject.ServiceCallActivities = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[newLines.Length];
            mServiceObject.ServiceCallActivities = newLines;

            try
            {
                oServiceCall.Update(mServiceObject);

                //Attachment
                if (pObject.AttachmentList.Count > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>Y</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    mServiceObject = oServiceCall.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        mServiceObject.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        mServiceObject.AttachmentEntrySpecified = true;
                    }

                    oServiceCall.Update(mServiceObject);
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public List<ServiceCallSAP> GetServiceCallListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null)
        {
            try
            {
                Mapper.CreateMap<OSCL, ServiceCallSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OSCL> ListRestu = Mapper.Map<List<OSCL>>(mDbContext.OSCL.Where(c => (!string.IsNullOrEmpty(pCodeBP) ? c.customer.Contains(pCodeBP) : true) && (pPriority != "" ? (c.priority == pPriority) : true) && (pDate == null ? c.createDate != null : c.createDate >= pDate) && ((pStatus != null && pStatus != -99) ? c.status == pStatus : true) && (pSubject != "" ? c.subject.ToUpper().Contains(pSubject.ToUpper()) : true) && (!string.IsNullOrEmpty(pRefNumber) ? c.NumAtCard.Contains(pRefNumber) : true) && (pUser != null ? c.assignee == pUser : true)).ToList());

                return Mapper.Map<List<ServiceCallSAP>>(ListRestu);
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }

        }

        public List<EquipmentCardSAP> GetEquipmentCardListSearch(CompanyConn pCompanyParam, string pItemCode = "", string pItemName = "")
        {
            try
            {
                Mapper.CreateMap<OIN, EquipmentCardSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OIN> ListRestu = Mapper.Map<List<OIN>>(mDbContext.OINS.Where(c => (!string.IsNullOrEmpty(pItemCode) ? c.itemCode.ToUpper().Contains(pItemCode.ToUpper()) : true) && (!string.IsNullOrEmpty(pItemName) ? c.itemName.ToUpper().Contains(pItemName.ToUpper()) : true)).ToList());

                return Mapper.Map<List<EquipmentCardSAP>>(ListRestu);
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ServiceStatus> GetServiceCallStatusList(CompanyConn pCompanyParam)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<OSC, ServiceStatus>(); }).CreateMapper();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                return mapper.Map<List<ServiceStatus>>(mDbContext.OSCS.ToList());
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

    }
}