﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebServices.ServiceCalls
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceCallService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceCallService.svc or ServiceCallService.svc.cs at the Solution Explorer and start debugging.
    public class ServiceCallService : IServiceCallService
    {
        private IServiceCallService mServiceCall = null;

        private void InitializeService(CompanyConn pCompanyParam)
        {
            if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
            {
                mServiceCall = new ServiceCallServiceSL();
            }
            else
            {
                mServiceCall = new ServiceCallServiceDS();
            }
        }

        public ServiceCallComboList GetServiceCallComboList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceCall.GetServiceCallComboList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallService -> GetServiceCallComboList :" + ex.Message);
                return null;
            }

        }

        public ServiceCallSAP GetServiceCallById(CompanyConn pCompanyParam, int pId)
        {

            try
            {
                InitializeService(pCompanyParam);
                return mServiceCall.GetServiceCallById(pCompanyParam, pId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallService -> GetServiceCallById :" + ex.Message);
                return null;
            }
        }


        public JsonObjectResult AddServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {

            try
            {
                InitializeService(pCompanyParam);
                return mServiceCall.AddServiceCall(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallService -> AddServiceCall :" + ex.Message);
                return null;
            }
        }


        public string UpdateServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {

            try
            {
                InitializeService(pCompanyParam);
                return mServiceCall.UpdateServiceCall(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallService -> UpdateServiceCall :" + ex.Message);
                return null;
            }
        }


        public List<ServiceCallSAP> GetServiceCallListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceCall.GetServiceCallListSearch(pCompanyParam, pCodeBP, pPriority, pSubject, pRefNumber, pDate, pStatus, pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallService -> GetServiceCallListSearch :" + ex.Message);
                return null;
            }

        }


       public  List<EquipmentCardSAP> GetEquipmentCardListSearch(CompanyConn pCompanyParam, string pItemCode = "", string pItemName = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceCall.GetEquipmentCardListSearch(pCompanyParam, pItemCode, pItemName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallService -> GetEquipmentCardListSearch :" + ex.Message);
                return null;
            }
        }

        public List<ServiceStatus> GetServiceCallStatusList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceCall.GetServiceCallStatusList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallService -> GetServiceCallStatusList:" + ex.Message);
                return null;
            }
        }
    }
}
