﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using WebServices.Model.Tables;

namespace WebServices.ServiceCalls
{
    public class ServiceCallServiceSL : IServiceCallService
    {


        public ServiceCallComboList GetServiceCallComboList(CompanyConn pCompanyParam)
        {
            ServiceCallComboList ret = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1";
            try
            {
                ret = new ServiceCallComboList();

                //CallType
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSCT");
                ret.CallTypeList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<CallType>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Service Status
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSC");
                ret.ServiceStatusList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceStatus>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Service Problem Type
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSCP");
                ret.ServiceProblemTypeList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceProblemType>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Service Problem SubType
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OPST");
                ret.ServiceProblemSubTypeList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceProblemSubType>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Service Origin
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSCO");
                ret.ServiceOriginList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceOrigin>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Service Handled By
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User");
                ret.HandledByList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Service Priority               
                ret.ServicePriorityList.Add(new ServicePriority() { Code = "L", Name = "Low" });
                ret.ServicePriorityList.Add(new ServicePriority() { Code = "M", Name = "Medium" });
                ret.ServicePriorityList.Add(new ServicePriority() { Code = "H", Name = "High" });

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OITB");
                ret.ItemGroupList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemGroupSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Service Technician        
                mFilters = "roleID eq -2";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Technician", mFilters);
                ret.TechnicianList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

            }
            catch (Exception)
            {
                return null;
            }
            return ret;
        }

        public ServiceCallSAP GetServiceCallById(CompanyConn pCompanyParam, int pId)
        {
            ServiceCallSAP ret = null;
            string mUrl = string.Empty;
            string mFilters = string.Empty;
            try
            {
                mFilters = "callID eq " + pId + "";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSCL", mFilters);
                List<ServiceCallSAP> retList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceCallSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                ret = retList.SingleOrDefault();
                if (ret != null)
                {
                    //Contact Person
                    mFilters = "CardCode eq '" + HttpUtility.UrlEncode(ret.customer) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ContacPerson", mFilters);
                    ret.ContactPersonList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContacPerson>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                    mFilters = "CardCode eq '" + HttpUtility.UrlEncode(ret.customer) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);

                    List<BusinessPartnerSAP> mBPList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    ret.CustomerPhone = mBPList.SingleOrDefault().Phone1;

                    //Activities List
                    mFilters = "SrvcCallId eq " + ret.callID + "";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SCL5", mFilters);
                    ret.ActivitiesList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceCallActivities>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    //Attachment List
                    if (ret.AtcEntry != null)
                    {
                        mFilters = "AbsEntry eq " + ret.AtcEntry + "";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ATC1", mFilters);
                        ret.AttachmentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AttachmentSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    }                    
                }
                else
                {
                    ret = new ServiceCallSAP();
                }
            }
            catch (Exception)
            {
                return null;
            }
            return ret;
        }

        private int GetNextServiceCallId(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int ret = 0;
            try
            {
                mOthers = "&$orderby=callID desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSCL", "", mOthers);
                List<ServiceCallSAP> listResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceCallSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                ret = listResult.SingleOrDefault().callID + 1;
            }
            catch (Exception)
            {
                ret = 1;
            }
            return ret;
        }

        public JsonObjectResult AddServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            ServiceCallServiceWeb.MsgHeader header = new ServiceCallServiceWeb.MsgHeader();
            header.ServiceName = ServiceCallServiceWeb.MsgHeaderServiceName.ServiceCallsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            ServiceCallServiceWeb.ServiceCallsService oServiceCall = new ServiceCallServiceWeb.ServiceCallsService();
            oServiceCall.MsgHeaderValue = header;

            ServiceCallServiceWeb.ServiceCall mServiceObject = new ServiceCallServiceWeb.ServiceCall();

            mServiceObject.CustomerCode = pObject.customer;
            mServiceObject.CustomerName = pObject.custmrName;
            mServiceObject.CustomerRefNo = pObject.NumAtCard;
            mServiceObject.Subject = pObject.subject;
            mServiceObject.ItemCode = pObject.itemCode;
            mServiceObject.ItemDescription = pObject.itemName;
            mServiceObject.Resolution = pObject.resolution;
            mServiceObject.Description = pObject.descrption;


            if (pObject.createDate.HasValue)
            {
                mServiceObject.CreationDate = pObject.createDate.Value;
                mServiceObject.CreationDateSpecified = true;
            }

            if (pObject.itemGroup.HasValue)
            {
                if (pObject.itemGroup.Value != 0)
                {
                    mServiceObject.ItemGroupCode = pObject.itemGroup.Value;
                    mServiceObject.ItemGroupCodeSpecified = true;
                }

            }

            if (pObject.contractID.HasValue)
            {
                mServiceObject.ContractID = pObject.contractID.Value;
                mServiceObject.ContractIDSpecified = true;
            }

            if (pObject.status.HasValue)
            {
                mServiceObject.Status = pObject.status.Value;
                mServiceObject.StatusSpecified = true;
            }

            mServiceObject.PrioritySpecified = true;
            switch (pObject.priority)
            {
                case "L":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Low;
                    break;
                case "M":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Medium;
                    break;
                case "H":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_High;
                    break;
            }

            if (pObject.origin.HasValue)
            {
                if (pObject.origin.Value != 0)
                {
                    mServiceObject.Origin = pObject.origin.Value;
                    mServiceObject.OriginSpecified = true;
                }
            }

            if (pObject.problemTyp.HasValue)
            {
                if (pObject.problemTyp.Value != 0)
                {
                    mServiceObject.ProblemType = pObject.problemTyp.Value;
                    mServiceObject.ProblemTypeSpecified = true;
                }

            }

            if (pObject.ProSubType.HasValue)
            {
                if (pObject.ProSubType.Value != 0)
                {
                    mServiceObject.ProblemSubType = pObject.ProSubType.Value;
                    mServiceObject.ProblemSubTypeSpecified = true;
                }
            }

            if (pObject.callType.HasValue)
            {
                if (pObject.callType.Value != 0)
                {
                    mServiceObject.CallType = pObject.callType.Value;
                    mServiceObject.CallTypeSpecified = true;
                }
            }

            if (pObject.technician.HasValue)
            {
                if (pObject.technician.Value != 0)
                {
                    mServiceObject.TechnicianCode = pObject.technician.Value;
                    mServiceObject.TechnicianCodeSpecified = true;
                }

            }

            if (pObject.assignee.HasValue)
            {
                mServiceObject.AssigneeCode = pObject.assignee.Value;
                mServiceObject.AssigneeCodeSpecified = true;
            }

            if (pObject.contctCode.HasValue)
            {
                if (pObject.contctCode.Value != 0)
                {
                    mServiceObject.ContactCode = pObject.contctCode.Value;
                    mServiceObject.ContactCodeSpecified = true;
                }
            }

            //Creo las lineas de Actividades
            ServiceCallServiceWeb.ServiceCallServiceCallActivity[] newLines = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[pObject.ActivitiesList.Count];
            int positionNewLine = 0;
            foreach (ServiceCallActivities portalLine in pObject.ActivitiesList)
            {
                ServiceCallServiceWeb.ServiceCallServiceCallActivity line = new ServiceCallServiceWeb.ServiceCallServiceCallActivity();

                line.ActivityCode = portalLine.ClgID.Value;
                line.ActivityCodeSpecified = true;

                line.LineNum = portalLine.Line;
                line.LineNumSpecified = true;

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mServiceObject.ServiceCallActivities = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[newLines.Length];
            mServiceObject.ServiceCallActivities = newLines;  

            try
            {
                ServiceCallServiceWeb.ServiceCallParams parametros = new ServiceCallServiceWeb.ServiceCallParams();
                parametros = oServiceCall.Add(mServiceObject);

                mJsonObjectResult.Code = parametros.ServiceCallID.ToString();
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                //Attachment
                if (pObject.AttachmentList.Count > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>Y</Override></row>";
                    }
                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    mServiceObject = oServiceCall.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        mServiceObject.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        mServiceObject.AttachmentEntrySpecified = true;
                    }

                    oServiceCall.Update(mServiceObject);
                }
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public string UpdateServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam)
        {

            ServiceCallServiceWeb.MsgHeader header = new ServiceCallServiceWeb.MsgHeader();
            header.ServiceName = ServiceCallServiceWeb.MsgHeaderServiceName.ServiceCallsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            ServiceCallServiceWeb.ServiceCallsService oServiceCall = new ServiceCallServiceWeb.ServiceCallsService();
            oServiceCall.MsgHeaderValue = header;

            ServiceCallServiceWeb.ServiceCallParams parametros = new ServiceCallServiceWeb.ServiceCallParams();
            parametros.ServiceCallID = pObject.callID;
            parametros.ServiceCallIDSpecified = true;

            ServiceCallServiceWeb.ServiceCall mServiceObject = oServiceCall.GetByParams(parametros);

            mServiceObject.CustomerCode = pObject.customer;
            mServiceObject.CustomerName = pObject.custmrName;
            mServiceObject.CustomerRefNo = pObject.NumAtCard;
            mServiceObject.Subject = pObject.subject;
            mServiceObject.ItemCode = pObject.itemCode;
            mServiceObject.ItemDescription = pObject.itemName;
            mServiceObject.Resolution = pObject.resolution;
            mServiceObject.Description = pObject.descrption;


            if (pObject.createDate.HasValue)
            {
                mServiceObject.CreationDate = pObject.createDate.Value;
                mServiceObject.CreationDateSpecified = true;
            }

            if (pObject.itemGroup.HasValue)
            {
                if (pObject.itemGroup.Value != 0)
                {
                    mServiceObject.ItemGroupCode = pObject.itemGroup.Value;
                    mServiceObject.ItemGroupCodeSpecified = true;
                }

            }

            if (pObject.contractID.HasValue)
            {
                mServiceObject.ContractID = pObject.contractID.Value;
                mServiceObject.ContractIDSpecified = true;
            }

            if (pObject.status.HasValue)
            {
                mServiceObject.Status = pObject.status.Value;
                mServiceObject.StatusSpecified = true;
            }

            mServiceObject.PrioritySpecified = true;
            switch (pObject.priority)
            {
                case "L":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Low;
                    break;
                case "M":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_Medium;
                    break;
                case "H":
                    mServiceObject.Priority = ServiceCallServiceWeb.ServiceCallPriority.scp_High;
                    break;
            }

            if (pObject.origin.HasValue)
            {
                if (pObject.origin.Value != 0)
                {
                    mServiceObject.Origin = pObject.origin.Value;
                    mServiceObject.OriginSpecified = true;
                }
            }

            if (pObject.problemTyp.HasValue)
            {
                if (pObject.problemTyp.Value != 0)
                {
                    mServiceObject.ProblemType = pObject.problemTyp.Value;
                    mServiceObject.ProblemTypeSpecified = true;
                }

            }

            if (pObject.ProSubType.HasValue)
            {
                if (pObject.ProSubType.Value != 0)
                {
                    mServiceObject.ProblemSubType = pObject.ProSubType.Value;
                    mServiceObject.ProblemSubTypeSpecified = true;
                }
            }

            if (pObject.callType.HasValue)
            {
                if (pObject.callType.Value != 0)
                {
                    mServiceObject.CallType = pObject.callType.Value;
                    mServiceObject.CallTypeSpecified = true;
                }
            }

            if (pObject.technician.HasValue)
            {
                if (pObject.technician.Value != 0)
                {
                    mServiceObject.TechnicianCode = pObject.technician.Value;
                    mServiceObject.TechnicianCodeSpecified = true;
                }

            }

            if (pObject.assignee.HasValue)
            {
                mServiceObject.AssigneeCode = pObject.assignee.Value;
                mServiceObject.AssigneeCodeSpecified = true;
            }

            if (pObject.contctCode.HasValue)
            {
                if (pObject.contctCode.Value != 0)
                {
                    mServiceObject.ContactCode = pObject.contctCode.Value;
                    mServiceObject.ContactCodeSpecified = true;
                }
            }

            //Creo las lineas de Actividades
            ServiceCallServiceWeb.ServiceCallServiceCallActivity[] newLines = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[pObject.ActivitiesList.Count];
            int positionNewLine = 0;
            foreach (ServiceCallActivities portalLine in pObject.ActivitiesList)
            {
                ServiceCallServiceWeb.ServiceCallServiceCallActivity line = new ServiceCallServiceWeb.ServiceCallServiceCallActivity();

                line.ActivityCode = portalLine.ClgID.Value;
                line.ActivityCodeSpecified = true;

                line.LineNum = portalLine.Line;
                line.LineNumSpecified = true;

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mServiceObject.ServiceCallActivities = new ServiceCallServiceWeb.ServiceCallServiceCallActivity[newLines.Length];
            mServiceObject.ServiceCallActivities = newLines;  

            try
            {
                oServiceCall.Update(mServiceObject);

                //Attachment
                if (pObject.AttachmentList.Count > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>Y</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    mServiceObject = oServiceCall.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        mServiceObject.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        mServiceObject.AttachmentEntrySpecified = true;
                    }

                    oServiceCall.Update(mServiceObject);
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public List<ServiceCallSAP> GetServiceCallListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null)
        {
            List<ServiceCallSAP> mRet = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {

                if (!string.IsNullOrEmpty(pCodeBP))
                {
                    mFilters = mFilters + " and customer eq '" + pCodeBP + "'";
                }

                if (!string.IsNullOrEmpty(pPriority))
                {
                    mFilters = mFilters + " and priority eq '" + pPriority + "'";
                }

                if (!string.IsNullOrEmpty(pSubject))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pSubject + "'), tolower(subject))";
                }

                if (!string.IsNullOrEmpty(pRefNumber))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pRefNumber + "'), tolower(NumAtCard))";
                }

                if (pDate != null)
                {
                    DateTime mDate = pDate ?? DateTime.Now;
                    mFilters = mFilters + " and  createDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (pUser != null)
                {
                    mFilters = mFilters + " and assignee eq " + pUser + "";
                }

                if (pStatus != null && pStatus != -99)
                {
                    mFilters = mFilters + " and status eq " + pStatus;
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSCL", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceCallSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceCallServiceSL -> GetServiceCallListSearch :" + ex.Message);
                return null;
            }
            return mRet;

        }

        public List<EquipmentCardSAP> GetEquipmentCardListSearch(CompanyConn pCompanyParam, string pItemCode = "", string pItemName = "")
        {
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            List<EquipmentCardSAP> ret = null;

            try
            {
                if (!string.IsNullOrEmpty(pItemCode))
                {
                    mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pItemCode) + "'), tolower(itemCode))";
                }

                if (!string.IsNullOrEmpty(pItemName))
                {
                    mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pItemName) + "'), tolower(itemName))";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OINS", mFilters);
                ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EquipmentCardSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception)
            {
                return null;
            }

            return ret;
        }

        public List<ServiceStatus> GetServiceCallStatusList(CompanyConn pCompanyParam)
        {
            List<ServiceStatus> ret = null;
            string mUrl = string.Empty;
            try
            {
                //Service Status
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSC");
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServiceStatus>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}