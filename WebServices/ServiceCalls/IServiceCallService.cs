﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebServices.ServiceCalls
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServiceCallService" in both code and config file together.
    [ServiceContract]
    public interface IServiceCallService
    {
        [OperationContract]
        ServiceCallComboList GetServiceCallComboList(CompanyConn pCompanyParam);

        [OperationContract]
        ServiceCallSAP GetServiceCallById(CompanyConn pCompanyParam, int pId);

        [OperationContract]
        JsonObjectResult AddServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateServiceCall(ServiceCallSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        List<ServiceCallSAP> GetServiceCallListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pPriority = "", string pSubject = "", string pRefNumber = "", DateTime? pDate = null, short? pStatus = null, short? pUser = null);

        [OperationContract]
        List<EquipmentCardSAP> GetEquipmentCardListSearch(CompanyConn pCompanyParam, string pItemCode = "", string pItemName = "");

        [OperationContract]
        List<ServiceStatus> GetServiceCallStatusList(CompanyConn pCompanyParam);
    }
}
