﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServices
{
    public class ODataReturnObj
    {
        public string JSONResult { get; set; }
        public string Count { get; set; }
    }
}