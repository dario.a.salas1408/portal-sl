﻿using ARGNS.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace WebServices
{
    public static class RESTService
    {
        public static string GetURL(string pHanaUrl, string pView, string pFilter = "", string pOthers = "", bool pJsonFormat = true,bool pCount = false, bool pPaging = false, int pStart = 0, int pLength = 0, string pOrderColumn = "")
        {
            string mUrlReturn = pHanaUrl.Trim() + pView + (pJsonFormat ? "?$format=json" : "");

            if (!string.IsNullOrEmpty(pFilter))
            {
                mUrlReturn = mUrlReturn + (pJsonFormat ? "&$filter=" : "?$filter=") + pFilter;
            }
            if (pPaging)
            {
                mUrlReturn = mUrlReturn + ((pJsonFormat || !string.IsNullOrEmpty(pFilter)) ? "&$skip=" + pStart + "&$top=" + pLength + "&$orderby=" + pOrderColumn : "?$skip=" + pStart + "&$top=" + pLength + "&$orderby=" + pOrderColumn);
            }
            if (pCount)
            {
                mUrlReturn = mUrlReturn + ((pJsonFormat || !string.IsNullOrEmpty(pFilter) || pPaging) ? "&$inlinecount=allpages" : "?$inlinecount=allpages");
            }
            if (!string.IsNullOrEmpty(pOthers))
            {
                mUrlReturn = mUrlReturn + pOthers;
            }

            return mUrlReturn;
        }

        public static string GetRequestJson(string pUrl, WebServices.UtilWeb.MethodType pMethodType, string pUser, string pPass, bool pJsonFormat = true)
        {
            HttpWebRequest requestJSON = null;
            string ret = string.Empty;
            try
            {
                requestJSON = WebRequest.Create(pUrl) as HttpWebRequest;
                requestJSON.Method = pMethodType.ToString();
                requestJSON.KeepAlive = true;                               //keep alive
                requestJSON.ServicePoint.Expect100Continue = false;        //content
                requestJSON.AllowAutoRedirect = true;
                if (pJsonFormat)
                {
                    requestJSON.Accept = "application/json;odata=minimalmetadata";
                    requestJSON.ContentType = "application/json;odata=minimalmetadata;charset=utf8";
                }
                requestJSON.Timeout = 10000000;
                requestJSON.Credentials = new NetworkCredential(pUser, pPass);


                HttpWebResponse responseJSON = requestJSON.GetResponse() as HttpWebResponse;
                if (responseJSON.StatusDescription == "OK")
                {
                    //.- Get the response body, read it and create a BigCommerceProductModel -.\\
                    Stream responseStreamJSON = responseJSON.GetResponseStream();
                    StreamReader wmsReaderStream = new StreamReader(responseStreamJSON);
                    string responseFromServer = wmsReaderStream.ReadToEnd();
                    if (pJsonFormat)
                    {
                        var jObj = JObject.Parse(responseFromServer);
                        ret = jObj["d"]["results"].ToString();
                    }
                    else
                    {
                        ret = responseFromServer;
                    }

                    wmsReaderStream.Close();
                    responseStreamJSON.Close();
                }

                requestJSON.Abort();
                responseJSON.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethodType"></param>
        /// <param name="pUser"></param>
        /// <param name="pPass"></param>
        /// <param name="pJsonFormat"></param>
        /// <returns></returns>
        public static int GetRequestJsonCount(string pUrl, WebServices.UtilWeb.MethodType pMethodType, 
            string pUser, string pPass, bool pJsonFormat = true)
        {
            HttpWebRequest requestJSON = null;
            string returnValue = string.Empty;
            try
            {
                requestJSON = WebRequest.Create(pUrl) as HttpWebRequest;
                requestJSON.Method = pMethodType.ToString();
                requestJSON.KeepAlive = true;                               //keep alive
                requestJSON.ServicePoint.Expect100Continue = false;        //content
                requestJSON.AllowAutoRedirect = true;
                requestJSON.Timeout = 10000000;
                requestJSON.Credentials = new NetworkCredential(pUser, pPass);


                HttpWebResponse responseJSON = requestJSON.GetResponse() as HttpWebResponse;
                if (responseJSON.StatusDescription == "OK")
                {
                    //.- Get the response body, read it and create a BigCommerceProductModel -.\\
                    Stream responseStreamJSON = responseJSON.GetResponseStream();
                    StreamReader wmsReaderStream = new StreamReader(responseStreamJSON);
                    string responseFromServer = wmsReaderStream.ReadToEnd();
                    returnValue = responseFromServer;

                    wmsReaderStream.Close();
                    responseStreamJSON.Close();
                }

                requestJSON.Abort();
                responseJSON.Close();
            }
            catch (Exception ex)
            {
                Logger.WriteError("RESTService --> GetRequestJsonCount" + ex.Message);
                Logger.WriteError("RESTService --> GetRequestJsonCount" + ex.InnerException);
            }

            return int.Parse(returnValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pUrl"></param>
        /// <param name="pMethodType"></param>
        /// <param name="pUser"></param>
        /// <param name="pPass"></param>
        /// <param name="pJsonFormat"></param>
        /// <returns></returns>
        public static ODataReturnObj GetRequestJsonWithCount(string pUrl, WebServices.UtilWeb.MethodType pMethodType, string pUser, string pPass, bool pJsonFormat = true)
        {
            ODataReturnObj mODataReturnObj = new ODataReturnObj();
            HttpWebRequest requestJSON = null;
            string ret = string.Empty;
            try
            {
                requestJSON = WebRequest.Create(pUrl) as HttpWebRequest;
                requestJSON.Method = pMethodType.ToString();
                requestJSON.KeepAlive = true;                               //keep alive
                requestJSON.ServicePoint.Expect100Continue = false;        //content
                requestJSON.AllowAutoRedirect = true;
                if (pJsonFormat)
                {
                    requestJSON.Accept = "application/json;odata=minimalmetadata";
                    requestJSON.ContentType = "application/json;odata=minimalmetadata;charset=utf8";
                }
                requestJSON.Timeout = 10000000;
                requestJSON.Credentials = new NetworkCredential(pUser, pPass);


                HttpWebResponse responseJSON = requestJSON.GetResponse() as HttpWebResponse;
                if (responseJSON.StatusDescription == "OK")
                {
                    //.- Get the response body, read it and create a BigCommerceProductModel -.\\
                    Stream responseStreamJSON = responseJSON.GetResponseStream();
                    StreamReader wmsReaderStream = new StreamReader(responseStreamJSON);
                    string responseFromServer = wmsReaderStream.ReadToEnd();
                    if (pJsonFormat)
                    {
                        var jObj = JObject.Parse(responseFromServer);
                        mODataReturnObj.JSONResult = jObj["d"]["results"].ToString();
                        mODataReturnObj.Count = jObj["d"]["__count"].ToString();
                    }
                    else
                    {
                        mODataReturnObj.JSONResult = responseFromServer;
                    }

                    wmsReaderStream.Close();
                    responseStreamJSON.Close();
                }

                requestJSON.Abort();
                responseJSON.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            return mODataReturnObj;
        }
    }
}