﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebServices.ItemMaster
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceItemMaster" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceItemMaster.svc or ServiceItemMaster.svc.cs at the Solution Explorer and start debugging.
    public class ServiceItemMaster : IServiceItemMaster
    {
        private IServiceItemMaster mServiceItemMaster = null;

        public ServiceItemMaster()
        {
        }

        private void InitializeService(CompanyConn pCompanyParam)
        {
            if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
            {
                mServiceItemMaster = new SericeItemMasterSL();
            }
            else
            {
                mServiceItemMaster = new SericeItemMasterDS();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pModelCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public StockModel GetStockByModel(CompanyConn pCompanyParam, string pModelCode, List<WarehousePortal> pWarehousePortalList)
        {
            InitializeService(pCompanyParam);
            return mServiceItemMaster.GetStockByModel(pCompanyParam, pModelCode, pWarehousePortalList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="CheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pListUDFOITM"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetOITMListBy(CompanyConn pCompanyParam,
            bool CheckApparelInstallation,
            string pPrchseItem, string pSellItem,
            string pInventoryItem, string pItemWithStock,
            string pItemGroup,
            string pModCode,
            List<UDF_ARGNS> pMappedUdf, string pItemCode,
            string pItemName, bool pOnlyActiveItems,
            List<WarehousePortal> pWarehousePortalList,
            List<UDF_ARGNS> pListUDFOITM = null,
            bool pCkCatalogueNum = false, string pBPCode = "",
            string pBPCatalogCode = "", int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetOITMListBy(pCompanyParam, CheckApparelInstallation,
                    pPrchseItem, pSellItem,
                    pInventoryItem, pItemWithStock,
                    pItemGroup,
                    pModCode,
                    pMappedUdf, pItemCode,
                    pItemName, pOnlyActiveItems,
                    pWarehousePortalList,
                    pListUDFOITM,
                    pCkCatalogueNum, pBPCode,
                    pBPCatalogCode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetItemMasterList :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pListItems"></param>
        /// <param name="pItemType"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetOITMListByList(CompanyConn pCompanyParam, List<string> pListItems, string pItemType)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetOITMListByList(pCompanyParam, pListItems, pItemType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetOITMListByList :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="CheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pCostPriceList"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="uCardCode"></param>
        /// <param name="pItemGroupCode"></param>
        /// <param name="pListUDFOITM"></param>
        /// <param name="pNoItemsWithZeroStockCk"></param>
        /// <param name="pSupplierCardCode"></param>
        /// <param name="pItemList"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetQuickOrderListBy(CompanyConn pCompanyParam,
            bool CheckApparelInstallation, string pPrchseItem,
            string pSellItem, string pModCode, int? pCostPriceList,
            List<UDF_ARGNS> pMappedUdf, string pItemCode,
            string pItemName, string uCardCode, short pItemGroupCode,
            bool pOnlyActiveItems, bool isSO,
            List<UDF_ARGNS> pListUDFOITM = null, bool pNoItemsWithZeroStockCk = false,
            string pSupplierCardCode = "", List<string> pItemList = null,
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetQuickOrderListBy(pCompanyParam,
                    CheckApparelInstallation, pPrchseItem, pSellItem, pModCode,
                    pCostPriceList, pMappedUdf, pItemCode, pItemName, uCardCode,
                    pItemGroupCode, pOnlyActiveItems, isSO, pListUDFOITM, pNoItemsWithZeroStockCk,
                    pSupplierCardCode, pItemList, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetQuickOrderListBy :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="ListItems"></param>
        /// <param name="CardCode"></param>
        /// <param name="pSession"></param>
        /// <param name="pOrderItems"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsPrice(CompanyConn pCompanyParam, List<ItemMasterSAP> ListItems, string CardCode, string pSession, bool isSO, bool pOrderItems = true, string ano = "")
        {
            try
            {

                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetItemsPrice(pCompanyParam, ListItems, CardCode, pSession, isSO,pOrderItems, ano);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetItemsPrice:" + ex.Message);
                return ListItems;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pCodeBar"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCodeBar(CompanyConn pCompanyParam, string pPrchseItem, string pSellItem, string pCodeBar)
        {
            try
            {

                InitializeService(pCompanyParam);
                return mServiceItemMaster.GeItemByCodeBar(pCompanyParam, pPrchseItem, pSellItem, pCodeBar);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GeItemByCodeBar:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<StockModel> GetItemStock(CompanyConn pCompanyParam, string[] pItemCode, List<WarehousePortal> pWarehousePortalList)
        {
            try
            {

                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetItemStock(pCompanyParam, pItemCode, pWarehousePortalList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetItemStock:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCode(CompanyConn pCompanyParam, string pItemCode, string pBarCode = null, List<UDF_ARGNS> pListUDF = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GeItemByCode(pCompanyParam, pItemCode, pBarCode, pListUDF);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GeItemByCode:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pPriceList"></param>
        /// <returns></returns>
        public decimal GetPriceItemByPriceList(CompanyConn pCompanyParam, string pItemCode, short pPriceList)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetPriceItemByPriceList(pCompanyParam, pItemCode, pPriceList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetPriceItemByPriceList:" + ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<ItemGroupSAP> GetItemGroupList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetItemGroupList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetItemGroupList:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCodeFrom"></param>
        /// <param name="pItemCodeTo"></param>
        /// <param name="pVendorFrom"></param>
        /// <param name="pVendorTo"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pSelectedWH"></param>
        /// <returns></returns>
        public JsonObjectResult GetInventoryStatus(CompanyConn pCompanyParam,
            string pItemCodeFrom, string pItemCodeTo,
            string pVendorFrom, string pVendorTo,
            string pItemGroup, string[] pSelectedWH, int pStart, int pLenght,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetInventoryStatus(pCompanyParam,
                    pItemCodeFrom, pItemCodeTo, pVendorFrom,
                    pVendorTo, pItemGroup, pSelectedWH,
                    pStart, pLenght, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetInventoryStatus:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        public List<AvailableToPromise> GetATPByWarehouse(CompanyConn pCompanyParam, string pItemCode, string pWarehouse)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetATPByWarehouse(pCompanyParam, pItemCode, pWarehouse);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetATPByWarehouse:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pListItems"></param>
        /// <param name="pFormula"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsWithPositiveStock(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItems, string[] pFormula)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetItemsWithPositiveStock(pCompanyParam, pListItems, pFormula);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetItemsWithPositiveStock:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<Warehouse> GetAvailableWarehousesByCompany(CompanyConn pCompanyParam, List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GetAvailableWarehousesByCompany(pCompanyParam, pWarehousePortalList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GetAvailableWarehousesByCompany:" + ex.Message);
                return null;
            }
        }

        public List<ItemMasterSAP> GeUOMByItemList(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItem, string pSellItem, string pPrchseItem)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mServiceItemMaster.GeUOMByItemList(pCompanyParam, pListItem, pSellItem, pPrchseItem);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMaster -> GeUOMByItemList:" + ex.Message);
                return null;
            }
        }
    }
}
