﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System.Collections.Generic;
using System.ServiceModel;

namespace WebServices.ItemMaster
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServiceItemMaster" in both code and config file together.
    [ServiceContract]
    public interface IServiceItemMaster
    {

        [OperationContract]
        JsonObjectResult GetOITMListBy(CompanyConn pCompanyParam, bool CheckApparelInstallation,
            string pPrchseItem, string pSellItem,
            string pInventoryItem, string pItemWithStock,
            string pItemGroup,
            string pModCode, List<UDF_ARGNS> pMappedUdf, string pItemCode,
            string pItemName, bool pOnlyActiveItems, List<WarehousePortal> pWarehousePortalList,
            List<UDF_ARGNS> pListUDFOITM = null,
            bool pCkCatalogueNum = false, string pBPCode = "",
            string pBPCatalogCode = "", int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        List<ItemMasterSAP> GetOITMListByList(CompanyConn pCompanyParam,
            List<string> pListItems, string pItemType);

        [OperationContract]
        JsonObjectResult GetQuickOrderListBy(CompanyConn pCompanyParam, bool CheckApparelInstallation,
            string pPrchseItem, string pSellItem, string pModCode, int? pCostPriceList,
            List<UDF_ARGNS> pMappedUdf, string pItemCode, string pItemName,
            string uCardCode, short pItemGroupCode, bool pOnlyActiveItems, bool isSO,
            List<UDF_ARGNS> pListUDFOITM = null,
            bool pNoItemsWithZeroStockCk = false,
            string pSupplierCardCode = "", List<string> pItemList = null,
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        List<ItemMasterSAP> GetItemsPrice(CompanyConn pCompanyParam, List<ItemMasterSAP> ListItems, string CardCode, string pSession, bool isSO, bool pOrderItems = true, string ano = "");

        [OperationContract]
        ItemMasterSAP GeItemByCodeBar(CompanyConn pCompanyParam, string pPrchseItem, string pSellItem, string pCodeBar);

        [OperationContract]
        List<StockModel> GetItemStock(CompanyConn pCompanyParam, string[] pItemCode, List<WarehousePortal> pWarehousePortalList);

        [OperationContract]
        StockModel GetStockByModel(CompanyConn pCompanyParam, string pModelCode, List<WarehousePortal> pWarehousePortalList);

        [OperationContract]
        ItemMasterSAP GeItemByCode(CompanyConn pCompanyParam, string pItemCode, string pBarCode = null, List<UDF_ARGNS> pListUDF = null);

        [OperationContract]
        decimal GetPriceItemByPriceList(CompanyConn pCompanyParam, string pItemCode, short pPriceList);

        [OperationContract]
        List<ItemGroupSAP> GetItemGroupList(CompanyConn pCompanyParam);

        [OperationContract]
        JsonObjectResult GetInventoryStatus(CompanyConn pCompanyParam,
            string pItemCodeFrom, string pItemCodeTo,
            string pVendorFrom, string pVendorTo,
            string pItemGroup, string[] pSelectedWH,
            int pStart, int pLength,
            OrderColumn pOrderColumn = null);

        [OperationContract]
        List<AvailableToPromise> GetATPByWarehouse(CompanyConn pCompanyParam, string pItemCode, string pWarehouse);

        [OperationContract]
        List<ItemMasterSAP> GetItemsWithPositiveStock(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItems, string[] pFormula);

        [OperationContract]
        List<Warehouse> GetAvailableWarehousesByCompany(CompanyConn pCompanyParam, List<WarehousePortal> pWarehousePortalList);

        [OperationContract]
        List<ItemMasterSAP> GeUOMByItemList(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItem, string pSellItem, string pPrchseItem);
    }
}
