﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebServices.DocumentsSAP;
using WebServices.Model.Tables;
using WebServices.User;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using WebServices.Query;
using System.Xml;
using WebServices.Model;

namespace WebServices.ItemMaster
{
    public class SericeItemMasterSL : IServiceItemMaster
    {
        private B1WSHandler mB1WSHandler;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="CheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pListUDFOITM"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetOITMListBy(CompanyConn pCompanyParam, bool CheckApparelInstallation,
            string pPrchseItem, string pSellItem,
            string pInventoryItem, string pItemWithStock,
            string pItemGroup,
            string pModCode, List<UDF_ARGNS> pMappedUdf,
            string pItemCode, string pItemName,
            bool pOnlyActiveItems,
            List<WarehousePortal> pWarehousePortalList,
            List<UDF_ARGNS> pListUDFOITM = null,
            bool pCkCatalogueNum = false, string pBPCode = "",
            string pBPCatalogCode = "", int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null)
        {
            List<ItemMasterSAP> listItemMasterSAP = new List<ItemMasterSAP>();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            try
            {
                string mUrl = string.Empty;
                string mFilters = "1 eq 1";
                string mFrom = "";
                string mPagination = "";

                if (pLength != 0 && !pItemWithStock.Equals("Y"))
                {
                    mPagination = string.Format("&$top={0}&$skip={1}", pLength, pStart);
                }

                if (!string.IsNullOrEmpty(pItemCode) && !string.IsNullOrEmpty(pItemName))
                {
                    mFilters = mFilters + " and (substringof(tolower('" + HttpUtility.UrlEncode(pItemCode.Trim()) + "'), tolower(ItemCode)) and substringof(tolower('" + HttpUtility.UrlEncode(pItemName.Trim()) + "'), tolower(ItemName)))";
                }
                else if (!string.IsNullOrEmpty(pItemCode))
                {
                    mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pItemCode.Trim()) + "'), tolower(ItemCode))";
                }
                else if (!string.IsNullOrEmpty(pItemName))
                {
                    mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pItemName.Trim()) + "'), tolower(ItemName))";
                }

                if (pOnlyActiveItems)
                {
                    mFilters = mFilters + " and validFor eq 'Y' ";
                }

                if (!pItemGroup.Equals("0") && !pItemGroup.Trim().Equals(""))
                {
                    mFilters = mFilters + string.Format(" and ItmsGrpCod eq {0} ", pItemGroup);
                }

                foreach (UDF_ARGNS udf in pMappedUdf)
                {
                    string mTypeID = string.Empty;

                    if (!string.IsNullOrEmpty(udf.Value))
                    {
                        mTypeID = pListUDFOITM.Where(c => c.UDFName == udf.UDFName).FirstOrDefault().TypeID;
                        if (mTypeID == "N")
                        {
                            mFilters += " and " + udf.UDFName + " eq " + udf.Value;
                        }
                        else
                        {
                            mFilters += " and " + udf.UDFName + " eq '" + udf.Value + "'";
                        }
                    }
                }
                if (CheckApparelInstallation)
                {
                    List<OITM> ListOITM;
                    if (pCkCatalogueNum)
                    {
                        mFrom = "CustomOITMBPCatalogModel";
                        mFilters += " and CardCodeBPCatalog eq '" + pBPCode + "' ";
                        if (!string.IsNullOrEmpty(pBPCatalogCode))
                        {
                            mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pBPCatalogCode.Trim()) + "'), tolower(Substitute))";
                        }
                    }
                    else
                    {
                        mFrom = "CustomOITMModel";
                    }
                    if (!string.IsNullOrEmpty(pModCode))
                    {
                        mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pModCode.Trim()) + "'), tolower(U_ARGNS_MOD))";
                    }

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, mFrom, mFilters) + mPagination;

                    string mResultJsonOITM = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                    Newtonsoft.Json.Linq.JArray mJArrayOITM = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonOITM);
                    ListOITM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITM>>(mResultJsonOITM);
                    foreach (OITM item in ListOITM)
                    {
                        item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOITM, new List<UFD1_SAP>(), (Newtonsoft.Json.Linq.JObject)mJArrayOITM.Where(c => (string)c["ItemCode"] == item.ItemCode).FirstOrDefault());
                    }
                    listItemMasterSAP = (from mOITM in ListOITM
                                         where (
                                             (!string.IsNullOrEmpty(pPrchseItem) ? mOITM.PrchseItem == pPrchseItem : true) &&
                                             (!string.IsNullOrEmpty(pSellItem) ? mOITM.SellItem == pSellItem : true) &&
                                             (!string.IsNullOrEmpty(pInventoryItem) ? mOITM.InvntItem == pInventoryItem : true) /*&&
                                         (pModCode != "" ? (mOITM.U_ARGNS_MOD != null ? mOITM.U_ARGNS_MOD.ToUpper().Contains(pModCode.ToUpper()) : false) : true)*/
                                         )
                                         select new ItemMasterSAP()
                                         {
                                             ItemCode = mOITM.ItemCode,
                                             ItemName = mOITM.ItemName,
                                             InvntItem = mOITM.InvntItem,
                                             PrchseItem = mOITM.PrchseItem,
                                             SellItem = mOITM.SellItem,
                                             ItmsGrpCod = mOITM.ItmsGrpCod,
                                             MappedUdf = mOITM.MappedUdf,
                                             PicturName = mOITM.PicturName,
                                             UgpEntry = mOITM.UgpEntry,
                                             Substitute = mOITM.Substitute,
                                             TaxCodeAP = mOITM.TaxCodeAP,
                                             TaxCodeAR = mOITM.TaxCodeAR,
                                             SUoMEntry = mOITM.SUoMEntry,
                                             PUoMEntry = mOITM.PUoMEntry,
                                             ManBtchNum = mOITM.ManBtchNum,
                                             ManSerNum = mOITM.ManSerNum,
                                             PriceUnit = mOITM.PriceUnit
                                         }).ToList();

                }
                else
                {
                    List<OITM> ListOITM;
                    if (pCkCatalogueNum)
                    {
                        mFrom = "ItemsBPCatalog";
                        mFilters += " and CardCodeBPCatalog eq '" + pBPCode + "' ";
                        if (!string.IsNullOrEmpty(pBPCatalogCode))
                        {
                            mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pBPCatalogCode.Trim()) + "'), tolower(Substitute))";
                        }
                    }
                    else
                    {
                        mFrom = "Items";
                    }

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, mFrom, mFilters) + mPagination;
                    string mResultJsonOITM = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    Newtonsoft.Json.Linq.JArray mJArrayOITM = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonOITM);
                    ListOITM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITM>>(mResultJsonOITM);
                    foreach (OITM item in ListOITM)
                    {
                        item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOITM, new List<UFD1_SAP>(), (Newtonsoft.Json.Linq.JObject)mJArrayOITM.Where(c => (string)c["ItemCode"] == item.ItemCode).FirstOrDefault());
                    }
                    listItemMasterSAP = (from mOITM in ListOITM
                                         where (
                                             (!string.IsNullOrEmpty(pPrchseItem) ? mOITM.PrchseItem == pPrchseItem : true) &&
                                             (!string.IsNullOrEmpty(pSellItem) ? mOITM.SellItem == pSellItem : true) &&
                                             (!string.IsNullOrEmpty(pInventoryItem) ? mOITM.InvntItem == pInventoryItem : true) &&
                                             ((!string.IsNullOrEmpty(pItemName) && !string.IsNullOrEmpty(pItemCode)) ? mOITM.ItemName.ToUpper().Contains(pItemName.ToUpper()) || mOITM.ItemCode.ToUpper().Contains(pItemCode.ToUpper()) : (!string.IsNullOrEmpty(pItemName) ? mOITM.ItemName.ToUpper().Contains(pItemName.ToUpper()) : (!string.IsNullOrEmpty(pItemCode) ? mOITM.ItemCode.ToUpper().Contains(pItemCode.ToUpper()) : true)))
                                         )
                                         select new ItemMasterSAP()
                                         {
                                             ItemCode = mOITM.ItemCode,
                                             ItemName = mOITM.ItemName,
                                             InvntItem = mOITM.InvntItem,
                                             PrchseItem = mOITM.PrchseItem,
                                             SellItem = mOITM.SellItem,
                                             ItmsGrpCod = mOITM.ItmsGrpCod,
                                             MappedUdf = mOITM.MappedUdf,
                                             PicturName = mOITM.PicturName,
                                             UgpEntry = mOITM.UgpEntry,
                                             Substitute = mOITM.Substitute,
                                             TaxCodeAP = mOITM.TaxCodeAP,
                                             TaxCodeAR = mOITM.TaxCodeAR,
                                             SUoMEntry = mOITM.SUoMEntry,
                                             PUoMEntry = mOITM.PUoMEntry,
                                             ManSerNum = mOITM.ManSerNum,
                                             ManBtchNum = mOITM.ManBtchNum,
                                             PriceUnit = mOITM.PriceUnit
                                         }).ToList();

                }

                if (!pItemWithStock.Equals("Y"))
                {
                    mJsonObjectResult.ItemMasterSAPList = listItemMasterSAP;

                    mUrl = pCompanyParam.UrlHana + mFrom + "/$count/?$filter=" + mFilters;

                    int mCounter = RESTService.GetRequestJsonCount(mUrl,
                                    WebServices.UtilWeb.MethodType.GET,
                                    pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                    mJsonObjectResult.Others.Add("TotalRecords", mCounter.ToString());
                }
                else
                {
                    List<StockModel> stockModel = GetItemStock(pCompanyParam,
                        listItemMasterSAP.Select(s => s.ItemCode).ToArray(),
                        pWarehousePortalList);


                    mJsonObjectResult.ItemMasterSAPList = (from items in listItemMasterSAP
                                                           join stock in stockModel
                                                           on items.ItemCode equals
                                                           stock.ItemCode
                                                           select items).Distinct().ToList();

                    foreach (var item in mJsonObjectResult.ItemMasterSAPList)
                    {
                        item.Stock = stockModel.Where(w => w.ItemCode == item.ItemCode).FirstOrDefault();
                    }

                    mJsonObjectResult.Others.Add("TotalRecords", mJsonObjectResult.ItemMasterSAPList.Count().ToString());
                }


                ///--------------------------
                foreach (ItemMasterSAP mItemMasterSAP in mJsonObjectResult.ItemMasterSAPList)
                {
                    mFilters = "UgpEntry eq " + mItemMasterSAP.UgpEntry;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUGP", mFilters);
                    OUGP mOUGPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUGP>>(
                        RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET,
                        pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                    if (mOUGPAux != null)
                    {
                        mItemMasterSAP.UomGroupName = mOUGPAux.UgpName;
                    }

                    mItemMasterSAP.UnitOfMeasureList = GetUOMLists(pCompanyParam, mItemMasterSAP.UgpEntry, pSellItem, pPrchseItem, mItemMasterSAP.SUoMEntry, mItemMasterSAP.PUoMEntry);
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL --> GetOITMListBy" + ex.Message);
                return mJsonObjectResult;
            }
            finally
            {

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pListItems"></param>
        /// <param name="pItemType"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetOITMListByList(CompanyConn pCompanyParam, List<string> pListItems, string pItemType)
        {
            List<ItemMasterSAP> mListItems = new List<ItemMasterSAP>();

            string mSQLQuery = "", mFrom = " FROM \"{0}\".\"OITM\" as T0";
            string mSelect = "SELECT T0.\"ItemCode\", T0.\"ItemName\", T0.\"InvntItem\", T0.\"PrchseItem\", T0.\"SellItem\", T0.\"ItmsGrpCod\", T0.\"PicturName\", T0.\"UgpEntry\", T0.\"TaxCodeAP\", T0.\"TaxCodeAR\", T0.\"SUoMEntry\", T0.\"PUoMEntry\" ";
            string mWhere = " WHERE 1=1 ";
            if (pListItems != null)
            {
                mWhere += " and (T0.\"ItemCode\" in (";
                for (int i = 0; i < pListItems.Count; i++)
                {
                    if (i == 0)
                        mWhere += "'" + pListItems[i] + "'";
                    else
                        mWhere += ",'" + pListItems[i] + "'";
                }
                mWhere += "))";
            }

            mSQLQuery = mSelect + mFrom + mWhere;
            mSQLQuery = String.Format(mSQLQuery, pCompanyParam.CompanyDB);
            UserSL pUserServ = new UserSL();

            mListItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(pUserServ.GetQueryResult(pCompanyParam, mSQLQuery));

            return mListItems;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="CheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pCostPriceList"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="uCardCode"></param>
        /// <param name="pItemGroupCode"></param>
        /// <param name="pListUDFOITM"></param>
        /// <param name="pNoItemsWithZeroStockCk"></param>
        /// <param name="pSupplierCardCode"></param>
        /// <param name="pItemList"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetQuickOrderListBy(CompanyConn pCompanyParam,
            bool CheckApparelInstallation, string pPrchseItem, string pSellItem,
            string pModCode, int? pCostPriceList, List<UDF_ARGNS> pMappedUdf,
            string pItemCode, string pItemName, string uCardCode, short pItemGroupCode,
            bool pOnlyActiveItems, bool isSO, List<UDF_ARGNS> pListUDFOITM = null,
            bool pNoItemsWithZeroStockCk = false, string pSupplierCardCode = "",
            List<string> pItemList = null, int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            List<ItemMasterSAP> ListItemMasterSAP = new List<ItemMasterSAP>();
            CounterHANA mCounterHANA = new CounterHANA();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            List<OITM> ListOITM;
            UserSL pUserServ = new UserSL();

            #region Typing Query to get Items

            string mQuery = string.Empty;
            string mSelectQuery = "SELECT \"OITM\".\"ItemCode\", \"OITM\".\"ItemName\", \"OITM\".\"InvntItem\", \"OITM\".\"PrchseItem\", \"OITM\".\"SellItem\", \"OITM\".\"ItmsGrpCod\", \"OITM\".\"PicturName\", \"OITM\".\"UgpEntry\", \"OITM\".\"CardCode\", \"T1\".\"OnHand\", \"T1\".\"OnOrder\", \"T1\".\"IsCommited\", \"OITM\".\"TaxCodeAP\", \"OITM\".\"TaxCodeAR\", \"OITM\".\"SUoMEntry\", \"OITM\".\"PUoMEntry\" ";
            string mSelectSubQuery = " SELECT \"OITM\".\"ItemCode\", SUM(\"OITW\".\"OnHand\") AS \"OnHand\", SUM(\"OITW\".\"OnOrder\") AS \"OnOrder\", SUM(\"OITW\".\"IsCommited\") AS \"IsCommited\" ";
            string mFromQuery = " FROM ({SubQuery}) AS \"T1\" INNER JOIN \"{0}\".\"OITM\" ON \"OITM\".\"ItemCode\" = \"T1\".\"ItemCode\"";
            string mFromSubQuery = " FROM \"{0}\".\"OITM\" INNER JOIN \"{0}\".\"OITW\" ON \"OITW\".\"ItemCode\" = \"OITM\".\"ItemCode\" INNER JOIN \"{0}\".\"OWHS\" ON \"OITW\".\"WhsCode\" = \"OWHS\".\"WhsCode\"";
            string mWhereQuery = "";
            string mWhereSubQuery = " WHERE " + (pPrchseItem == "Y" ? "\"OITM\".\"PrchseItem\" = '" + pPrchseItem + "'" : "1 = 1");
            string mOrderBySubQuery = (pLength > 0 ? " ORDER BY \"OITM\"." + Utils.OrderString(pOrderColumn, "HANA") + " LIMIT " + pLength + " OFFSET " + pStart : "");
            string mGroupBySubQuery = " GROUP BY \"OITM\".\"ItemCode\"";

            if (pNoItemsWithZeroStockCk)
            {
                mGroupBySubQuery += " HAVING(SUM(\"OITW\".\"OnHand\") + SUM(\"OITW\".\"OnOrder\") -SUM(\"OITW\".\"IsCommited\")) > 0 ";
            }
            mWhereSubQuery += (pSellItem == "Y" ? " and \"OITM\".\"SellItem\" = '" + pSellItem + "'" : "");
            mWhereSubQuery += (pItemGroupCode != -1 ? " and \"OITM\".\"ItmsGrpCod\" = " + pItemGroupCode + "" : "");
            mWhereSubQuery += (pSupplierCardCode != "" ? " and \"OITM\".\"CardCode\" = '" + pSupplierCardCode + "'" : "");
            if (pItemList != null)
            {
                mWhereSubQuery += " and (\"OITM\".\"ItemCode\" in (";
                for (int i = 0; i < pItemList.Count; i++)
                {
                    if (i == 0)
                        mWhereSubQuery += "'" + pItemList[i] + "'";
                    else
                        mWhereSubQuery += ",'" + pItemList[i] + "'";
                }
                mWhereSubQuery += "))";
            }

            //Adding mapped udf filters to where
            foreach (UDF_ARGNS udf in pMappedUdf)
            {
                string mTypeID = string.Empty;

                if (!string.IsNullOrEmpty(udf.Value))
                {
                    mTypeID = pListUDFOITM.Where(c => c.UDFName == udf.UDFName).FirstOrDefault().TypeID;
                    if (mTypeID == "N")
                    {
                        mWhereSubQuery += " and \"OITM\".\"" + udf.UDFName + "\" = " + udf.Value;
                    }
                    else
                    {
                        mWhereSubQuery += " and \"OITM\".\"" + udf.UDFName + "\" = '" + udf.Value + "'";
                    }
                }
            }

            if (!string.IsNullOrEmpty(pItemCode) && !string.IsNullOrEmpty(pItemName))
            {
                mWhereSubQuery += " and (\"OITM\".\"ItemCode\" like '%" + pItemCode + "%' or \"OITM\".\"ItemName\" like '%" + pItemName + "%')";
            }
            else if (!string.IsNullOrEmpty(pItemCode))
            {
                mWhereSubQuery += " and (\"OITM\".\"ItemCode\" like '%" + pItemCode + "%')";
            }
            else if (!string.IsNullOrEmpty(pItemName))
            {
                mWhereSubQuery += " and (\"OITM\".\"ItemName\" like '%" + pItemName + "%')";
            }

            if (pOnlyActiveItems)
            {
                mWhereSubQuery += " AND \"OITM\".\"validFor\" = 'Y'";
            }

            if (pCostPriceList != null)
            {
                mSelectQuery += ", \"ITM1\".\"Price\" as 'CostPrice', \"ITM1\".\"Currency\" as 'CostPriceCurrency'";
                mFromQuery += " left outer join \"{0}\".\"ITM1\" on \"OITM\".\"ItemCode\" = \"ITM1\".\"ItemCode\"";
                mWhereQuery += "WHERE \"ITM1\".\"PriceList\" = " + pCostPriceList;
            }

            #endregion

            try
            {
                #region Getting Items

                if (CheckApparelInstallation)
                {
                    mSelectQuery += ", \"OITM\".\"U_ARGNS_MOD\"";

                    //Getting the total of records
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, null, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2("Select count(*) as Counter ", "from(" + mQuery + ") as TT", null);
                    mQuery = String.Format(mQuery, pCompanyParam.CompanyDB);
                    mCounterHANA = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CounterHANA>>(pUserServ.GetQueryResult(pCompanyParam, mQuery)).FirstOrDefault();

                    //Getting the items
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, mOrderBySubQuery, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectQuery, "from(" + mQuery + ") AS T1 INNER JOIN \"{0}\".\"OITM\" ON \"OITM\".\"ItemCode\" = \"T1\".\"ItemCode\"", pListUDFOITM, mWhereQuery, null, null, "HANA");
                    mQuery = String.Format(mQuery, pCompanyParam.CompanyDB);
                    string mJsonResult = pUserServ.GetQueryResult(pCompanyParam, mQuery);
                    Newtonsoft.Json.Linq.JArray mJArrayOITM = Newtonsoft.Json.Linq.JArray.Parse(mJsonResult);
                    ListOITM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITM>>(mJsonResult);

                    //Adding mapped udf values from query to items
                    for (int i = 0; i < mJArrayOITM.Count; i++)
                    {
                        ListOITM[i].MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOITM, null, (Newtonsoft.Json.Linq.JObject)mJArrayOITM[i]);
                    }

                    ListItemMasterSAP = (from mOITM in ListOITM
                                         where (
                                             (pModCode != "" ? (mOITM.U_ARGNS_MOD != null ? mOITM.U_ARGNS_MOD.ToUpper().Contains(pModCode.ToUpper()) : false) : true)
                                         )
                                         select new ItemMasterSAP()
                                         {
                                             ItemCode = mOITM.ItemCode,
                                             ItemName = mOITM.ItemName,
                                             InvntItem = mOITM.InvntItem,
                                             PrchseItem = mOITM.PrchseItem,
                                             SellItem = mOITM.SellItem,
                                             ItmsGrpCod = mOITM.ItmsGrpCod,
                                             MappedUdf = mOITM.MappedUdf,
                                             PicturName = mOITM.PicturName,
                                             UgpEntry = mOITM.UgpEntry,
                                             CardCode = mOITM.CardCode,
                                             CostPriceList = pCostPriceList,
                                             CostPrice = mOITM.CostPrice,
                                             CostPriceCurrency = mOITM.CostPriceCurrency,
                                             Stock = new StockModel(mOITM.ItemCode, mOITM.OnHand, mOITM.OnOrder, mOITM.IsCommited),
                                             TaxCodeAP = mOITM.TaxCodeAP,
                                             TaxCodeAR = mOITM.TaxCodeAR,
                                             SUoMEntry = mOITM.SUoMEntry,
                                             PUoMEntry = mOITM.PUoMEntry
                                         }).ToList();
                }
                else
                {
                    //Getting the total of records
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, null, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2("Select count(*) as Counter ", "from(" + mQuery + ") as TT", null);
                    mQuery = String.Format(mQuery, pCompanyParam.CompanyDB);
                    mCounterHANA = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CounterHANA>>(pUserServ.GetQueryResult(pCompanyParam, mQuery)).FirstOrDefault();

                    //Getting the items
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, mOrderBySubQuery, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectQuery, "from(" + mQuery + ") AS T1 INNER JOIN \"{0}\".\"OITM\" ON \"OITM\".\"ItemCode\" = \"T1\".\"ItemCode\"", pListUDFOITM, mWhereQuery, null, null, "HANA");
                    mQuery = String.Format(mQuery, pCompanyParam.CompanyDB);
                    string mJsonResult = pUserServ.GetQueryResult(pCompanyParam, mQuery);
                    Newtonsoft.Json.Linq.JArray mJArrayOITM = Newtonsoft.Json.Linq.JArray.Parse(mJsonResult);
                    ListOITM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITM>>(mJsonResult);

                    //Adding mapped udf values from query to items
                    for (int i = 0; i < mJArrayOITM.Count; i++)
                    {
                        ListOITM[i].MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOITM, null, (Newtonsoft.Json.Linq.JObject)mJArrayOITM[i]);
                    }

                    ListItemMasterSAP = (from mOITM in ListOITM
                                         select new ItemMasterSAP()
                                         {
                                             ItemCode = mOITM.ItemCode,
                                             ItemName = mOITM.ItemName,
                                             InvntItem = mOITM.InvntItem,
                                             PrchseItem = mOITM.PrchseItem,
                                             SellItem = mOITM.SellItem,
                                             ItmsGrpCod = mOITM.ItmsGrpCod,
                                             MappedUdf = mOITM.MappedUdf,
                                             PicturName = mOITM.PicturName,
                                             UgpEntry = mOITM.UgpEntry,
                                             CardCode = mOITM.CardCode,
                                             CostPriceList = pCostPriceList,
                                             CostPrice = mOITM.CostPrice,
                                             CostPriceCurrency = mOITM.CostPriceCurrency,
                                             Stock = new StockModel(mOITM.ItemCode, mOITM.OnHand, mOITM.OnOrder, mOITM.IsCommited),
                                             TaxCodeAP = mOITM.TaxCodeAP,
                                             TaxCodeAR = mOITM.TaxCodeAR,
                                             SUoMEntry = mOITM.SUoMEntry,
                                             PUoMEntry = mOITM.PUoMEntry
                                         }).ToList();
                }
                //Search BP Name
                //ListItemMasterSAP = ListItemMasterSAP.Select(c => { c.CardName = (!string.IsNullOrEmpty(c.CardCode) ? mDbContext.OCRD.Where(j => j.CardCode == c.CardCode).FirstOrDefault().CardName : ""); return c; }).ToList();

                #endregion

                #region Getting Units of Meassure

                foreach (ItemMasterSAP mItemMasterSAP in ListItemMasterSAP)
                {
                    mItemMasterSAP.UnitOfMeasureList = GetUOMLists(pCompanyParam, mItemMasterSAP.UgpEntry, pSellItem, pPrchseItem, mItemMasterSAP.SUoMEntry, mItemMasterSAP.PUoMEntry);
                }

                #endregion

                #region Getting Items Prices

                ServicesProcess mServiceProcess = new ServicesProcess();
                ListItemMasterSAP = mServiceProcess.GetItemPriceInBatch(ListItemMasterSAP, uCardCode, pCompanyParam.DSSessionId, true, isSO);

                #endregion

                mJsonObjectResult.ItemMasterSAPList = ListItemMasterSAP;
                mJsonObjectResult.Others.Add("TotalRecords", mCounterHANA.Counter);
                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMasterDS -> GetQuickOrderListBy:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="ListItems"></param>
        /// <param name="CardCode"></param>
        /// <param name="pSession"></param>
        /// <param name="pOrderItems"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsPrice(CompanyConn pCompanyParam, List<ItemMasterSAP> ListItems, string CardCode, string pSession, bool IsSO, bool pOrderItems = true, string ano = "")
        {
            string mUrl = string.Empty;
            string mFilters = string.Empty;

            ServicesProcess mServiceProcess = new ServicesProcess();
            GetDefaultWarehouse(pCompanyParam, ListItems);

            mFilters = "CardCode eq '" + CardCode + "'";
            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
            OCRD mBP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRD>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).SingleOrDefault();

            foreach (ItemMasterSAP item in ListItems)
            {
                mFilters = "ItemCode eq '" + item.ItemCode + "' and  PriceList eq " + mBP.ListNum;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ITM9", mFilters);
                List<ITM9> prices = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ITM9>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                foreach (ITM9 itemUOMPrice in prices)
                {
                    item.ItemPrices.Add(new ItemPrices { Currency = itemUOMPrice.Currency, ItemCode = itemUOMPrice.ItemCode, Price = itemUOMPrice.Price, UOMCode = itemUOMPrice.UomEntry.ToString(), PriceList = mBP.ListNum, HasCurrencyPrice = true, HasUOMPrice = true });
                }


            }


            //foreach (var item in ListItems)
            //{
            //    //mFilters = "Valid eq 'Y' and CardCode eq '" + CardCode + "' and  ItemCode eq '" + item.ItemCode + "'";
            //    //mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSPP", mFilters);
            //    //OSPP SpecialPrices = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSPP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).SingleOrDefault();

            //    //mFilters = "UgpEntry eq " + item.UgpEntry;
            //    //mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUGP", mFilters);
            //    //OUGP mOUGPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUGP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

            //    //mServiceProcess.GetItemPriceServiceLayer(item, pCompanyParam, mBP.CardCode, date, SpecialPrices == null ? (int)mBP.ListNum : (int)listPrice.BASE_NUM, sL, SpecialPrices == null ? false : true, mOUGPAux.BaseUom.ToString());

            //}


            return mServiceProcess.GetItemPriceInBatchSL(ListItems, CardCode, pSession, pOrderItems, IsSO, ano, pCompanyParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="ListItems"></param>
        public void GetDefaultWarehouse(CompanyConn pCompanyParam, List<ItemMasterSAP> ListItems)
        {
            string mWhs = string.Empty;
            string mUrl = string.Empty;
            string mFilters = "";
            try
            {
                foreach (ItemMasterSAP item in ListItems)
                {

                    //1. Check si El Item tiene un Def Whs 
                    mFilters = "ItemCode eq '" + item.ItemCode + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Items", mFilters);
                    mWhs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault().DfltWH;

                    if (string.IsNullOrEmpty(mWhs))
                    {
                        //2. Check si El usuario Conectado tiene Whs por Def 
                        mFilters = "USER_CODE eq '" + pCompanyParam.UserName + "'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUDG", mFilters);
                        List<UserDefault> mList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserDefault>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                        mWhs = (mList.Count > 0) ? mList.Single().Warehouse : string.Empty;

                        if (string.IsNullOrEmpty(mWhs))
                        {
                            //3. Por Ultimo Busca el Default Whs de Gereral Setting   
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OADM");
                            item.DfltWH = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OADM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).SingleOrDefault().DfltWhs;

                        }
                        else
                        {
                            item.DfltWH = mWhs;
                        }

                    }
                    else
                    {
                        item.DfltWH = mWhs;
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetDefaultWarehouse:" + ex.Message);
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pCodeBar"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCodeBar(CompanyConn pCompanyParam, string pPrchseItem, string pSellItem, string pCodeBar)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "1 eq 1 and BcdCode eq '" + pCodeBar + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OBCD", mFilters);

                ItemMasterSAP ItemReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                return ItemReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GeItemByCodeBar:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<StockModel> GetItemStock(CompanyConn pCompanyParam,
            string[] pItemCode,
            List<WarehousePortal> pWarehousePortalList)
        {
            string mSelectAux = string.Empty;
            try
            {
                UserSL pUserServ = new UserSL();
                mSelectAux = "SELECT \"OITM\".\"ItemCode\", \"OITM\".\"ItemName\", " +
                    "\"OWHS\".\"WhsCode\", \"OWHS\".\"WhsName\", " +
                    "\"OITW\".\"OnHand\", \"OITW\".\"IsCommited\", " +
                    "\"OITW\".\"OnOrder\", " +
                    "(\"OITW\".\"OnHand\" + \"OITW\".\"IsCommited\" + \"OITW\".\"OnOrder\") as Total " +
                    "FROM \"{0}\".\"OITW\" AS OITW INNER JOIN \"{0}\".\"OITM\" AS OITM " +
                    "ON \"OITW\".\"ItemCode\" = \"OITM\".\"ItemCode\" " +
                    "INNER JOIN \"{0}\".\"OWHS\" AS OWHS ON \"OITW\".\"WhsCode\" = \"OWHS\".\"WhsCode\" " +
                    "WHERE (\"OITW\".\"OnHand\" + \"OITW\".\"IsCommited\" + \"OITW\".\"OnOrder\") > 0";
                if (pWarehousePortalList.Count > 0)
                {
                    mSelectAux = mSelectAux + " and \"OWHS\".\"WhsCode\" IN (";
                    string mSecFilter = string.Empty;
                    foreach (WarehousePortal mWhsPortalAux in pWarehousePortalList)
                    {
                        if (string.IsNullOrEmpty(mSecFilter))
                        {
                            mSecFilter = "'" + mWhsPortalAux.WhsCode + "'";
                        }
                        else
                        {
                            mSecFilter = mSecFilter + ",'" + mWhsPortalAux.WhsCode + "'";
                        }
                    }
                    mSelectAux = mSelectAux + mSecFilter + ")";
                }

                if (pItemCode.Count() > 0)
                {
                    mSelectAux = mSelectAux + " and \"OITM\".\"ItemCode\" IN (";
                    string mSecFilter = string.Empty;
                    foreach (string item in pItemCode)
                    {
                        if (string.IsNullOrEmpty(mSecFilter))
                        {
                            mSecFilter = "'" + ChangeUTFCharacter(item) + "'";
                        }
                        else
                        {
                            mSecFilter = mSecFilter + ",'" + ChangeUTFCharacter(item) + "'";
                        }
                    }

                    mSelectAux = mSelectAux + mSecFilter + ")";
                }

                mSelectAux = String.Format(mSelectAux, pCompanyParam.CompanyDB);
                List<StockModel> result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StockModel>>
                    (pUserServ.GetQueryResult(pCompanyParam, mSelectAux))
                    .ToList();

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetItemStock :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bp"></param>
        /// <returns></returns>
        public string ChangeUTFCharacter(string bp)
        {
            bp = bp.Replace("Ñ", "%")
                .Replace("ñ", "%")
                .Replace("ã", "%")
                .Replace("ç", "%")
                .Trim();

            Regex regex = new Regex(@"[^a-zA-Z0-9\/s-]", (RegexOptions)0);
            return regex.Replace(bp, "%");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public StockModel GetStockByModel(CompanyConn pCompanyParam, string pModelCode,
            List<WarehousePortal> pWarehousePortalList)
        {
            string mSelectAux = string.Empty;
            try
            {
                UserSL pUserServ = new UserSL();
                mSelectAux = "SELECT \"OITM\".\"ItemCode\", \"OITM\".\"ItemName\", \"OWHS\".\"WhsCode\", \"OWHS\".\"WhsName\", \"OITW\".\"OnHand\", \"OITW\".\"IsCommited\", \"OITW\".\"OnOrder\", (\"OITW\".\"OnHand\" + \"OITW\".\"IsCommited\" + \"OITW\".\"OnOrder\") as Total, \"OITM\".\"U_ARGNS_ITYPE\" FROM \"{0}\".\"OITW\" AS OITW INNER JOIN \"{0}\".\"OITM\" AS OITM ON \"OITW\".\"ItemCode\" = \"OITM\".\"ItemCode\" INNER JOIN \"{0}\".\"OWHS\" AS OWHS ON \"OITW\".\"WhsCode\" = \"OWHS\".\"WhsCode\" WHERE (\"OITW\".\"OnHand\" + \"OITW\".\"IsCommited\" + \"OITW\".\"OnOrder\") > 0";
                if (pWarehousePortalList.Count > 0)
                {
                    mSelectAux = mSelectAux + " and \"OWHS\".\"WhsCode\" IN (";
                    string mSecFilter = string.Empty;
                    foreach (WarehousePortal mWhsPortalAux in pWarehousePortalList)
                    {
                        if (string.IsNullOrEmpty(mSecFilter))
                        {
                            mSecFilter = "'" + mWhsPortalAux.WhsCode + "'";
                        }
                        else
                        {
                            mSecFilter = mSecFilter + ",'" + mWhsPortalAux.WhsCode + "'";
                        }
                    }
                    mSelectAux = mSelectAux + mSecFilter + ")";
                }

                mSelectAux = mSelectAux + string.Format(" and \"OITM\".\"U_ARGNS_MOD\" = '{0}'", pModelCode);

                mSelectAux = String.Format(mSelectAux, pCompanyParam.CompanyDB);

                List<StockModel> result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StockModel>>
                    (pUserServ.GetQueryResult(pCompanyParam, mSelectAux))
                    .ToList();

                StockModel returnValue = new StockModel();
                returnValue.OnHand = result.Where(c => c.U_ARGNS_ITYPE != "P").Sum(s => s.OnHand);
                returnValue.OnHandPrePack = result.Where(c => c.U_ARGNS_ITYPE == "P").Sum(s => s.OnHand);
                returnValue.OnOrder = result.Sum(s => s.OnOrder);
                returnValue.Available = result.Sum(s => s.Available);
                returnValue.IsCommited = result.Sum(s => s.IsCommited);
                returnValue.Total = result.Where(c => c.U_ARGNS_ITYPE != "P").Sum(s => s.Total);
                returnValue.TotalPrepack = result.Where(c => c.U_ARGNS_ITYPE == "P").Sum(s => s.Total);
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetItemStock :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCode(CompanyConn pCompanyParam, string pItemCode, string pBarCode = null, List<UDF_ARGNS> pListUDF = null)
        {
            try
            {

                string mUrl = string.Empty;
                string mFilters = "";
                if (!string.IsNullOrEmpty(pBarCode))
                {
                    mFilters = "1 eq 1 and BcdCode eq '" + HttpUtility.UrlEncode(pBarCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OBCD", mFilters);

                    ItemMasterSAP ItemAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                    pItemCode = (ItemAux != null ? ItemAux.ItemCode : pItemCode);
                }

                mFilters = "1 eq 1 and ItemCode eq '" + HttpUtility.UrlEncode(pItemCode.Trim()) + "'";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Items", mFilters);

                ItemMasterSAP ItemReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                if (ItemReturn != null)
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OITB", "1 eq 1");

                    ItemReturn.ListItemGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList(); ;

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PriceList", "1 eq 1");

                    ItemReturn.ListPriceListSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PriceListSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList(); ;

                    if (ItemReturn.AtcEntry != null)
                    {
                        mFilters = "AbsEntry eq " + ItemReturn.AtcEntry + "";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ATC1", mFilters);
                        ItemReturn.ListAttachmentSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AttachmentSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    }

                    mFilters = "UgpEntry eq " + ItemReturn.UgpEntry;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUGP", mFilters);
                    OUGP mOUGPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUGP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    if (mOUGPAux != null)
                    {
                        ItemReturn.UomGroupName = mOUGPAux.UgpName;
                    }
                    ItemReturn.UnitOfMeasureList = GetUOMLists(pCompanyParam, ItemReturn.UgpEntry, "", "", ItemReturn.SUoMEntry, ItemReturn.PUoMEntry);
                }

                Logger.WriteInfo("SericeItemMasterSL ->  ItemReturn.SerialsSAP");

                ItemReturn.SerialsSAP = GetSerialsByItemCode(pCompanyParam, pItemCode, pListUDF.Where(c => c.Document == "OSRN").ToList());

                //ItemReturn.SerialsSAP = new List<Serial>();

                Logger.WriteInfo("SericeItemMasterSL ->  ItemReturn.SerialsSAP -> Salio");

                var oitmFilter = "TableID eq 'OITM'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", oitmFilter);
                List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Items", (pItemCode == "" ? "" : $"ItemCode eq '{pItemCode}'"));

                string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                Newtonsoft.Json.Linq.JArray mJArrayOINV = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);

                //var uDFs = UDFUtil.GetObjectListWithUDFHana(pListUDF.Where(c => c.Document == "OITM").ToList(), mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOINV[0]);

                var uDFs = UDFUtil.GetObjectListWithUDFHana(pListUDF.Where(c => c.Document == "OITM").ToList(), mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOINV[0]);

                //foreach (var itemUDF in uDFs.Where(c => c.RTable != null))
                //{
                //    var masterData = GlobalUtil.getMasterData(pCompanyParam.DSSessionId, itemUDF.RTable, $" \"Code\" = '{itemUDF.Value}' ", true);

                //    itemUDF.Value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");
                //}

               

                ItemReturn.MappedUdf = uDFs;

                return ItemReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GeItemByCodeBar:" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pPriceList"></param>
        /// <returns></returns>
        public decimal GetPriceItemByPriceList(CompanyConn pCompanyParam, string pItemCode, short pPriceList)
        {
            try
            {

                string mUrl = string.Empty;
                string mFilters = "1 eq 1 and ItemCode eq '" + HttpUtility.UrlEncode(pItemCode.Trim()) + "' and PriceList eq " + pPriceList;

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ITM1", mFilters);

                ITM1 mITM1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ITM1>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                if (mITM1 != null)
                {
                    return mITM1.Price ?? 0;
                }

                return 0;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetPriceItemByPriceList:" + ex.Message);
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<ItemGroupSAP> GetItemGroupList(CompanyConn pCompanyParam)
        {
            try
            {
                string mUrl = string.Empty;

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OITB");

                List<ItemGroupSAP> mItemGroupReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemGroupSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                return mItemGroupReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetItemGroupList:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCodeFrom"></param>
        /// <param name="pItemCodeTo"></param>
        /// <param name="pVendorFrom"></param>
        /// <param name="pVendorTo"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pSelectedWH"></param>
        /// <returns></returns>
        public JsonObjectResult GetInventoryStatus(CompanyConn pCompanyParam,
            string pItemCodeFrom, string pItemCodeTo,
            string pVendorFrom, string pVendorTo,
            string pItemGroup, string[] pSelectedWH,
            int pStart, int pLenght,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                string mCount = "SELECT COUNT(*) AS Counter ";
                string mWhere = " where T0.\"InvntItem\" = 'Y' ", mSQLQuery = "", mFrom = " from \"{0}\".\"OITM\" as T0";
                string mSelect = "SELECT T0.\"ItmsGrpCod\", T0.\"ItemCode\", T0.\"ItemName\", " +
                                    "T0.\"OnHand\", T0.\"IsCommited\", T0.\"OnOrder\", T0.\"CardCode\" ";
                if (pSelectedWH != null)
                {
                    mSelect = "SELECT T0.\"ItmsGrpCod\", T0.\"ItemCode\", T0.\"ItemName\", " +
                                 "AuxTable.\"ONHAND\", AuxTable.\"ISCOMMITED\", AuxTable.\"ONORDER\", T0.\"CardCode\" ";
                    mFrom += " INNER JOIN (select T3.\"ItemCode\", sum(T3.\"OnHand\") as OnHand, sum(T3.\"IsCommited\") as IsCommited, sum(T3.\"OnOrder\") as OnOrder from \"{0}\".\"OITM\" AS T4 inner join \"{0}\".\"OITW\" AS T3 on T3.\"ItemCode\" = T4.\"ItemCode\" where T3.\"WhsCode\" in (";
                    for (int i = 0; i < pSelectedWH.Count(); i++)
                    {
                        if (i == 0)
                            mFrom += "'" + pSelectedWH[i] + "'";
                        else
                            mFrom += ",'" + pSelectedWH[i] + "'";
                    }
                    mFrom += ") group by T3.\"ItemCode\") AS AuxTable on T0.\"ItemCode\" = AuxTable.\"ItemCode\" ";
                }

                if (!string.IsNullOrEmpty(pVendorFrom) || !string.IsNullOrEmpty(pVendorTo))
                {
                    mFrom += " INNER JOIN \"{0}\".\"OCRD\" AS T1 ON T0.\"CardCode\" = T1.\"CardCode\" ";
                    mSelect += " ,T1.\"CardName\", T1.\"Phone1\" ";
                    mWhere += (!string.IsNullOrEmpty(pVendorFrom) ? " and T1.\"CardCode\" >= '" + pVendorFrom + "'" : "");
                    mWhere += (!string.IsNullOrEmpty(pVendorTo) ? " and T1.\"CardCode\" <= '" + pVendorTo + "'" : "");
                }

                if (!string.IsNullOrEmpty(pItemCodeFrom) || !string.IsNullOrEmpty(pItemCodeTo))
                {
                    mFrom += " INNER JOIN \"{0}\".\"OITB\" AS T2 ON T0.\"ItmsGrpCod\" = T2.\"ItmsGrpCod\" ";
                    mSelect += " ,T2.\"ItmsGrpNam\" ";
                    mWhere += (!string.IsNullOrEmpty(pItemCodeFrom) ? " and T0.\"ItemCode\" >= '" + pItemCodeFrom + "'" : "");
                    mWhere += (!string.IsNullOrEmpty(pItemCodeTo) ? " and T0.\"ItemCode\" <= '" + pItemCodeTo + "'" : "");
                }

                mWhere += (!string.IsNullOrEmpty(pItemGroup) ? " and T0.\"ItmsGrpCod\" = " + pItemGroup : "");

                mSQLQuery = mSelect + mFrom + mWhere +
                    string.Format(" ORDER BY {0} LIMIT {1} OFFSET {2}",
                    Utils.OrderString(pOrderColumn, "HANA"),
                    pLenght, pStart);

                mSQLQuery = String.Format(mSQLQuery, pCompanyParam.CompanyDB);
                UserSL pUserServ = new UserSL();

                mJsonObjectResult.InventoryStatusList = Newtonsoft.Json.JsonConvert.DeserializeObject
                    <List<InventoryStatus>>(pUserServ.GetQueryResult(pCompanyParam, mSQLQuery));


                mSQLQuery = String.Format(mCount + mFrom + mWhere, pCompanyParam.CompanyDB);

                List<CounterHANA> count = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CounterHANA>>
                    (pUserServ.GetQueryResult(pCompanyParam, mSQLQuery).ToString());

                mJsonObjectResult.Others.Add("TotalRecords", count.FirstOrDefault().Counter);

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetInventoryStatus:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        public List<AvailableToPromise> GetATPByWarehouse(CompanyConn pCompanyParam, string pItemCode, string pWarehouse)
        {
            try
            {
                List<AvailableToPromise> mListATP = new List<AvailableToPromise>();

                string mSQLQuery;
                string mSelectOITW = "SELECT DISTINCT 'OITW' as Document, 'IS' as CardCode, T0.\"OnHand\" FROM \"{0}\".\"OITW\" as T0 WHERE T0.\"ItemCode\" = '" + pItemCode + "' and T0.\"WhsCode\" = '" + pWarehouse + "'";
                string mSelectOPOR = "SELECT DISTINCT 'OPOR' as Document, 'PO' as DocumentName, T0.\"DocEntry\", T1.\"LineNum\", T0.\"CardCode\", T2.\"CardName\", T0.\"DocDate\" as OrderDate, T1.\"ShipDate\" as DeliveryDate, T1.\"OpenQty\" as Ordered, null as Committed, UOM.\"UomCode\", UOM.\"UomName\", UOM.\"BaseQty\" FROM \"{0}\".\"OPOR\" as T0 inner join \"{0}\".\"POR1\" as T1 on T0.\"DocEntry\" = T1.\"DocEntry\" " +
                                     " inner join \"{0}\".\"OCRD\" as T2 on T0.\"CardCode\" = T2.\"CardCode\" LEFT JOIN (SELECT T3.\"UomEntry\", T3.\"UomCode\", T3.\"UomName\", T4.\"BaseQty\" from \"{0}\".\"OUOM\" AS T3  inner join \"{0}\".\"UGP1\" AS T4 on T3.\"UomEntry\" = T4.\"UomEntry\") AS UOM on T1.\"UomCode\" = UOM.\"UomCode\" WHERE T0.\"DocStatus\" = 'O' and T1.\"LineStatus\" = 'O' and T1.\"ItemCode\" = '" + pItemCode + "' and T1.\"WhsCode\" = '" + pWarehouse + "'";
                string mSelectORDR = "SELECT DISTINCT 'ORDR' as Document, 'SO' as DocumentName, T0.\"DocEntry\", T1.\"LineNum\", T0.\"CardCode\", T2.\"CardName\", T0.\"DocDate\" as OrderDate, T1.\"ShipDate\" as DeliveryDate, null as Ordered, T1.\"OpenQty\" as Committed, UOM.\"UomCode\", UOM.\"UomName\", UOM.\"BaseQty\" FROM \"{0}\".\"ORDR\" as T0 inner join \"{0}\".\"RDR1\" as T1 on T0.\"DocEntry\" = T1.\"DocEntry\" " +
                                     " inner join \"{0}\".\"OCRD\" as T2 on T0.\"CardCode\" = T2.\"CardCode\" LEFT JOIN (SELECT T3.\"UomEntry\", T3.\"UomCode\", T3.\"UomName\", T4.\"BaseQty\" from \"{0}\".\"OUOM\" AS T3  inner join \"{0}\".\"UGP1\" AS T4 on T3.\"UomEntry\" = T4.\"UomEntry\") AS UOM on T1.\"UomCode\" = UOM.\"UomCode\" WHERE T0.\"DocStatus\" = 'O' and T1.\"LineStatus\" = 'O' and T1.\"ItemCode\" = '" + pItemCode + "' and T1.\"WhsCode\" = '" + pWarehouse + "'";
                UserSL pUserServ = new UserSL();

                mSQLQuery = String.Format(mSelectOITW, pCompanyParam.CompanyDB);
                mListATP.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<AvailableToPromise>>(pUserServ.GetQueryResult(pCompanyParam, mSQLQuery)));
                mSQLQuery = String.Format(mSelectOPOR, pCompanyParam.CompanyDB);
                mListATP.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<AvailableToPromise>>(pUserServ.GetQueryResult(pCompanyParam, mSQLQuery)));
                mSQLQuery = String.Format(mSelectORDR, pCompanyParam.CompanyDB);
                mListATP.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<AvailableToPromise>>(pUserServ.GetQueryResult(pCompanyParam, mSQLQuery)));

                return mListATP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetATPByWarehouse:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUgpEntry"></param>
        /// <returns></returns>
        private List<UnitOfMeasure> GetUOMLists(CompanyConn pCompanyParam, int pUgpEntry, string pSellItem, string pPrchseItem, int? SUoMEntry = null, int? PUoMEntry = null)
        {
            List<UnitOfMeasure> oUnitOfMeasure = new List<UnitOfMeasure>();
            if (pUgpEntry != -1)
            {

                string mFilters = "UgpEntry eq " + pUgpEntry;
                string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UOMGroup", mFilters);
                oUnitOfMeasure = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UnitOfMeasure>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                if (SUoMEntry != null && pSellItem == "Y")
                {
                    oUnitOfMeasure = oUnitOfMeasure.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                    oUnitOfMeasure.Where(c => c.UomEntry == SUoMEntry).FirstOrDefault().LineNum = 1;
                }
                if (PUoMEntry != null && pPrchseItem == "Y")
                {
                    oUnitOfMeasure = oUnitOfMeasure.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                    oUnitOfMeasure.Where(c => c.UomEntry == PUoMEntry).FirstOrDefault().LineNum = 1;
                }
            }
            else
            {
                oUnitOfMeasure.Add(new UnitOfMeasure() { UomEntry = -1, UomCode = "Manual", UomName = "Manual", LineNum = 1, BaseQty = 1 });
            }
            return oUnitOfMeasure;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pListItems"></param>
        /// <param name="pFormula"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsWithPositiveStock(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItems, string[] pFormula)
        {
            throw new NotImplementedException();
        }

        public List<ItemMasterSAP> GeUOMByItemList(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItem, string pSellItem, string pPrchseItem)
        {
            try
            {
                List<ItemMasterSAP> pListItemAux = GetOITMListByList(pCompanyParam, pListItem.Select(c => c.ItemCode).ToList(), null);

                foreach (ItemMasterSAP mItem in pListItem)
                {
                    ItemMasterSAP mItemAux = pListItemAux.Where(j => j.ItemCode == mItem.ItemCode).FirstOrDefault();
                    if (mItemAux != null)
                    {
                        mItem.UgpEntry = mItemAux.UgpEntry;
                        mItem.SUoMEntry = mItemAux.SUoMEntry;
                        mItem.PUoMEntry = mItemAux.PUoMEntry;
                        mItem.TaxCodeAP = mItemAux.TaxCodeAP;
                        mItem.TaxCodeAR = mItemAux.TaxCodeAR;
                        mItem.UnitOfMeasureList = GetUOMLists(pCompanyParam, mItem.UgpEntry, pSellItem, pPrchseItem, mItem.SUoMEntry, mItem.PUoMEntry);

                        string mFilters = "UgpEntry eq " + mItem.UgpEntry;
                        string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUGP", mFilters);
                        OUGP mOUGPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUGP>>(
                            RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET,
                            pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                        if (mOUGPAux != null)
                        {
                            mItem.UomGroupName = mOUGPAux.UgpName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GeUOMByItemList :" + ex.Message);
                throw ex;
            }

            return pListItem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<Warehouse> GetAvailableWarehousesByCompany(CompanyConn pCompanyParam, List<WarehousePortal> pWarehousePortalList)
        {
            try
            {

                string mUrl = string.Empty;
                string mFilters = "1 eq 1";
                string mSecFilter = string.Empty;

                if (pWarehousePortalList.Count > 0)
                {
                    mFilters = mFilters + mSecFilter + " and ( ";
                    foreach (WarehousePortal mWH in pWarehousePortalList)
                    {
                        if (string.IsNullOrEmpty(mSecFilter))
                        {
                            mSecFilter = " WhsCode eq '" + mWH.WhsCode + "'";
                        }
                        else
                        {
                            mSecFilter = mSecFilter + " or WhsCode eq '" + mWH.WhsCode + "'";
                        }

                    }
                    mFilters = mFilters + mSecFilter + ")";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", mFilters);

                List<Warehouse> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Warehouse>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return ListRestu;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterSL -> GetAvailableWarehousesByCompany :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private class CounterHANA
        {
            public string Counter { get; set; }
        }


        private List<Serial> GetSerialsByItemCode(CompanyConn companyParam, string itemCode, List<UDF_ARGNS> pListUDF = null)
        {
            string mUrl = string.Empty;
            var whsUseBin = false;
            var where = string.Empty;
            var wherebySAP = string.Empty;

            List<OSBQ> OSBQ;

            mUrl = RESTService.GetURL(companyParam.UrlHana, "OITL", (itemCode == "" ? "" : $"ItemCode eq '{itemCode}'"));
            var OITL = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITL>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, companyParam.DbUserName, companyParam.DbPassword)).ToList();

            mUrl = RESTService.GetURL(companyParam.UrlHana, "ITL1", (itemCode == "" ? "" : $"ItemCode eq '{itemCode}'"));
            var ITL1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ITL1>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, companyParam.DbUserName, companyParam.DbPassword)).ToList();

            mUrl = RESTService.GetURL(companyParam.UrlHana, "OSRN", (itemCode == "" ? "" : $"ItemCode eq '{itemCode}'"));
            var OSRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSRN>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, companyParam.DbUserName, companyParam.DbPassword)).ToList();

            mUrl = RESTService.GetURL(companyParam.UrlHana, "OSBQ", $"ItemCode eq '{itemCode}'");
            OSBQ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSBQ>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, companyParam.DbUserName, companyParam.DbPassword)).ToList();


            var mFilters = "TableID eq 'OSRN'";
            mUrl = RESTService.GetURL(companyParam.UrlHana, "UFD1", mFilters);
            List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, companyParam.DbUserName, companyParam.DbPassword));

            List<Serial> sqlSAPUseBin = new List<Serial>();
            List<Serial> sqlSAP = new List<Serial>();

            sqlSAPUseBin = (from t0 in OITL
                            join t1 in ITL1 on t0.LogEntry equals t1.LogEntry
                            join t2 in OSRN on new { t1.SysNumber, t0.ItemCode } equals new { t2.SysNumber, t2.ItemCode }
                            join t3 in OSBQ on t1.MdAbsEntry equals t3.SnBMDAbs
                            where t3.ItemCode == itemCode && t3.OnHandQty > 0
                            select new Serial { DistNumber = t2.DistNumber, AbsEntry = t2.AbsEntry, SysNumber = t1.SysNumber, whsCode = t3.WhsCode, AllocQty = t1.AllocQty })
                           .GroupBy(c => new { c.DistNumber, c.SysNumber, c.whsCode, c.AbsEntry })
                           .Where(c => c.Sum(d => d.AllocQty) == 0).Select(c => new Serial { DistNumber = c.Key.DistNumber, AbsEntry = c.Key.AbsEntry, SysNumber = c.Key.SysNumber, whsCode = c.Key.whsCode }).ToList();


            sqlSAP = (from t0 in OITL
                      join t1 in ITL1 on t0.LogEntry equals t1.LogEntry
                      join t2 in OSRN on new { t1.SysNumber, t0.ItemCode } equals new { t2.SysNumber, t2.ItemCode }
                      where t0.ItemCode == itemCode
                      select new Serial { DistNumber = t2.DistNumber, AbsEntry = t2.AbsEntry, SysNumber = t1.SysNumber, AllocQty = t1.AllocQty })
                               .GroupBy(c => new { c.DistNumber, c.SysNumber, c.AbsEntry })
                               .Where(c => c.Sum(d => d.AllocQty) == 0).Select(c => new Serial { DistNumber = c.Key.DistNumber, AbsEntry = c.Key.AbsEntry, SysNumber = c.Key.SysNumber, whsCode = "-" }).ToList();

            sqlSAP = sqlSAP.Union(sqlSAPUseBin).ToList();

            //foreach (var item in sqlSAP)
            //{
            //    string mWhere = $"DistNumber eq '{item.DistNumber}' and ItemCode eq '{itemCode}' and SysNumber eq {item.SysNumber}";


            //    mUrl = RESTService.GetURL(companyParam.UrlHana, "OSRN", mWhere);

            //    string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, companyParam.DbUserName, companyParam.DbPassword);

            //    Newtonsoft.Json.Linq.JArray mJArrayOINV = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);

            //    item.UDFs = UDFUtil.GetObjectListWithUDFHana(pListUDF, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOINV[0]);

            //    foreach (var itemUDF in item.UDFs.Where(c => c.RTable != null))
            //    {

            //        //var masterData = GlobalUtil.getMasterData(companyParam.DSSessionId, itemUDF.RTable, $" \"Code\" = '{itemUDF.Value}' ", true);

            //        //itemUDF.Value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");
            //    }

            //}

            return sqlSAP;
        }
        //private List<Serial> GetSerialsByItemCode(CompanyConn companyParam, string itemCode, List<UDF_ARGNS> pListUDF = null)
        //{
        //    mB1WSHandler = new B1WSHandler();
        //    List<Serial> listSerialsWithBin = new List<Serial>();
        //    List<Serial> listSerialsOutBin = new List<Serial>();
        //    var listMemory = new List<ObjectMemory>();

        //    var wherebySAPWhitBin = string.Format(QueryResource.QueryGet("WherebySAPWhitBin01"), itemCode);

        //    var wherebySAPWhitOutBin = string.Format(QueryResource.QueryGet("WherebySAPWhitOutBin01"), itemCode);

        //    var serialsWhitBin = string.Format(QueryResource.QueryGet("SerialsWhitBin"), wherebySAPWhitBin);


        //    XmlDocument xml = new XmlDocument();
        //    xml.LoadXml(mB1WSHandler.ExecuetQuery(companyParam.DSSessionId, serialsWhitBin));

        //    var nodes = xml.SelectNodes("//*[local-name()='row']");
        //    foreach (XmlNode row in nodes)
        //    {
        //        if (Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='SysNumber']").InnerText) != 0)
        //        {
        //            listSerialsWithBin.Add(new Serial
        //            {
        //                DistNumber = row.SelectSingleNode(".//*[local-name()='DistNumber']").InnerText,
        //                SysNumber = Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='SysNumber']").InnerText),
        //                AbsEntry = Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='AbsEntry']").InnerText),
        //                AllocQty = Convert.ToDecimal(row.SelectSingleNode(".//*[local-name()='AllocQty']").InnerText ?? "0")
        //            });
        //        }
        //    }


        //    var serialsWhitOutBin = string.Format(QueryResource.QueryGet("SerialsWhitOutBin"), wherebySAPWhitOutBin);


        //    xml = new XmlDocument();
        //    xml.LoadXml(mB1WSHandler.ExecuetQuery(companyParam.DSSessionId, serialsWhitOutBin));

        //    nodes = xml.SelectNodes("//*[local-name()='row']");
        //    foreach (XmlNode row in nodes)
        //    {
        //        if (Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='SysNumber']").InnerText) != 0)
        //        {
        //            listSerialsOutBin.Add(new Serial
        //            {
        //                DistNumber = row.SelectSingleNode(".//*[local-name()='DistNumber']").InnerText,
        //                SysNumber = Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='SysNumber']").InnerText),
        //                AbsEntry = Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='AbsEntry']").InnerText),
        //                AllocQty = Convert.ToDecimal(row.SelectSingleNode(".//*[local-name()='AllocQty']").InnerText ?? "0")
        //            });
        //        }

        //    }

        //    var listUnion = listSerialsWithBin.Union(listSerialsOutBin).ToList();

        //    string distNumber = string.Empty;
        //    string sysNumber = string.Empty;

        //    foreach (var item in listUnion)
        //    {
        //        if (distNumber == "")
        //        {
        //            distNumber = "'" + item.DistNumber + "'";
        //        }
        //        else { distNumber = distNumber + ",'" + item.DistNumber + "'"; }

        //        if (sysNumber == "")
        //        {
        //            sysNumber = item.SysNumber.ToString();
        //        }
        //        else { sysNumber = sysNumber + "," + item.SysNumber.ToString() + ""; }


        //    }

        //    var resutl = mB1WSHandler.ExecuetQuery(companyParam.DSSessionId, string.Format(QueryResource.QueryGet("GetSerialsByDocument09"), distNumber, itemCode, sysNumber));

        //    xml.LoadXml(resutl);

        //    nodes = xml.SelectNodes("//*[local-name()='row']");

        //    foreach (XmlNode row in nodes)
        //    {
        //        var inteDistNumber = row.SelectSingleNode(".//*[local-name()='DistNumber']").InnerText;
        //        var intesysNumber = Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='SysNumber']").InnerText);

        //        var item = listUnion.Where(c => c.SysNumber == intesysNumber && c.DistNumber == inteDistNumber).FirstOrDefault();

        //        item.UDFs = new List<UDF_ARGNS>();

        //        foreach (var itemUdf in pListUDF)
        //        {
        //            var value = row.SelectSingleNode(".//*[local-name()='" + itemUdf.UDFName + "']").InnerText;

        //            var memory = listMemory.Where(c => c.Objet1 == itemUdf.RTable && c.Value1 == value).FirstOrDefault();

        //            if (value != string.Empty && itemUdf.RTable != null)
        //            {
        //                if (memory == null)
        //                {
        //                    var masterData = GlobalUtil.getMasterData(companyParam.DSSessionId, itemUdf.RTable, $" \"Code\" = '{value}' ", true);

        //                    var newMemory = new ObjectMemory { Objet1 = itemUdf.RTable, Value1 = value };

        //                    value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");

        //                    newMemory.Value2 = value;

        //                    listMemory.Add(newMemory);
        //                }
        //                else
        //                {
        //                    value = memory.Value2;
        //                }
        //            }
        //            else
        //            {
        //                if (value == string.Empty)
        //                    value = "??";
        //            }

        //            item.UDFs.Add(new UDF_ARGNS
        //            {
        //                Value = value,
        //                UDFName = itemUdf.UDFName,
        //                RTable = itemUdf.RTable,
        //                Document = itemUdf.Document,
        //                FieldID = itemUdf.FieldID,
        //                TypeID = itemUdf.TypeID,
        //                UDFDesc = itemUdf.UDFDesc
        //            });
        //        }
        //    }

        //    return listUnion;
        //}
    }

}