﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.Portal;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WebServices.DocumentsSAP;
using WebServices.Model;
using WebServices.Model.Tables;


namespace WebServices.ItemMaster
{
    public class SericeItemMasterDS : IServiceItemMaster
    {
        private DataBase mDbContext;
        private B1WSHandler mB1WSHandler;
        private string connectionString;

        public SericeItemMasterDS()
        {
            mDbContext = new DataBase();
            mB1WSHandler = new B1WSHandler();
            connectionString = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="CheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pInventoryItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="pListUDFOITM"></param>
        /// <param name="pCkCatalogueNum"></param>
        /// <param name="pBPCode"></param>
        /// <param name="pBPCatalogCode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetOITMListBy(CompanyConn pCompanyParam,
            bool CheckApparelInstallation, string pPrchseItem,
            string pSellItem, string pInventoryItem,
            string pItemWithStock, string pItemGroup,
            string pModCode,
            List<UDF_ARGNS> pMappedUdf, string pItemCode, string pItemName,
            bool pOnlyActiveItems,
            List<WarehousePortal> pWarehousePortalList,
            List<UDF_ARGNS> pListUDFOITM = null, bool pCkCatalogueNum = false,
            string pBPCode = "", string pBPCatalogCode = "",
            int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            Mapper.CreateMap<ItemMasterSAP, OITM>();
            Mapper.CreateMap<OITM, ItemMasterSAP>();

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            List<ItemMasterSAP> listItemMasterSAP = new List<ItemMasterSAP>();
            List<OITM> ListOITM;
            int mTotalRecords;
            try
            {
                string mSelect = @"Select [OITM].ItemCode, [OITM].ItemName, 
                            [OITM].InvntItem, [OITM].PrchseItem, [OITM].SellItem, 
                            [OITM].ItmsGrpCod, [OITM].PicturName, [OITM].UgpEntry, 
                            [OITM].TaxCodeAP, [OITM].TaxCodeAR, [OITM].SUoMEntry, [OITM].PUoMEntry, [OITM].ManBtchNum, [OITM].ManSerNum , [OITM].PriceUnit ";

                string mQuery = string.Empty;
                string mWhere = " WHERE " + (!string.IsNullOrEmpty(pPrchseItem) ? "PrchseItem = '" + pPrchseItem + "'" : "1 = 1");

                if (pOnlyActiveItems)
                {
                    mWhere = mWhere + " AND [OITM].validFor = 'Y' ";
                }

                string mFrom = " FROM [OITM] ";
                string mOrderBy = "";
                if (pLength > 0 && !pItemWithStock.Equals("Y"))
                    mOrderBy = " ORDER BY " + Utils.OrderString(pOrderColumn) +
                        " OFFSET " + pStart + " ROWS FETCH NEXT " + pLength + " ROWS ONLY";
                if (pCkCatalogueNum)
                {
                    mSelect += " ,[OSCN].Substitute ";
                    mFrom += " INNER JOIN [OSCN] ON [OSCN].ItemCode = [OITM].ItemCode ";
                    mWhere += " and [OSCN].CardCode = '" + pBPCode + "' ";
                    if (!string.IsNullOrEmpty(pBPCatalogCode))
                    {
                        mWhere += "and ([OSCN].Substitute like '%" + pBPCatalogCode + "%')";
                    }
                }
                mWhere += (!string.IsNullOrEmpty(pSellItem) ? " and SellItem = '" + pSellItem + "'" : "");
                mWhere += (!string.IsNullOrEmpty(pInventoryItem) ? " and InvntItem = '" + pInventoryItem + "'" : "");
                foreach (UDF_ARGNS udf in pMappedUdf)
                {
                    string mTypeID = string.Empty;

                    if (!string.IsNullOrEmpty(udf.Value))
                    {
                        mTypeID = pListUDFOITM.Where(c => c.UDFName == udf.UDFName).FirstOrDefault().TypeID;
                        if (mTypeID == "N")
                        {
                            mWhere += " and " + udf.UDFName + " = " + udf.Value;
                        }
                        else
                        {
                            mWhere += " and " + udf.UDFName + " = '" + udf.Value + "'";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(pItemCode) && !string.IsNullOrEmpty(pItemName))
                {
                    mWhere += "and ([OITM].ItemCode like '%" + pItemCode + "%' or [OITM].ItemName like '%" + pItemName + "%')";
                }
                else if (!string.IsNullOrEmpty(pItemCode))
                {
                    mWhere += "and ([OITM].ItemCode like '%" + pItemCode + "%')";
                }
                else if (!string.IsNullOrEmpty(pItemName))
                {
                    mWhere += "and ([OITM].ItemName like '%" + pItemName + "%')";
                }
                if (CheckApparelInstallation)
                {
                    mQuery = SQLQueryUtil.getSelectQuery2("Select count(*) as Counter", mFrom, null, mWhere);
                    DataTable dt = SQLQueryUtil.GetQueryResult(mQuery, mConnectionString);
                    mTotalRecords = Convert.ToInt32(dt.Rows[0]["Counter"]);
                    mSelect += ", U_ARGNS_MOD, U_ARGNS_ITYPE";
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelect, mFrom, pListUDFOITM, mWhere, mOrderBy);
                    ListOITM = UDFUtil.GetDBObjectList(mQuery, mConnectionString, pListUDFOITM, typeof(OITM), "OITM").Cast<OITM>().ToList();
                    listItemMasterSAP = (from mOITM in ListOITM
                                         where (
                                             (pModCode != "" ? (mOITM.U_ARGNS_MOD != null ? mOITM.U_ARGNS_MOD.ToUpper().Contains(pModCode.ToUpper()) : false) : true)
                                         )
                                         select new ItemMasterSAP()
                                         {
                                             ItemCode = mOITM.ItemCode,
                                             ItemName = mOITM.ItemName,
                                             InvntItem = mOITM.InvntItem,
                                             PrchseItem = mOITM.PrchseItem,
                                             SellItem = mOITM.SellItem,
                                             ItmsGrpCod = mOITM.ItmsGrpCod,
                                             MappedUdf = mOITM.MappedUdf,
                                             PicturName = mOITM.PicturName,
                                             UgpEntry = mOITM.UgpEntry,
                                             Substitute = mOITM.Substitute,
                                             TaxCodeAP = mOITM.TaxCodeAP,
                                             TaxCodeAR = mOITM.TaxCodeAR,
                                             SUoMEntry = mOITM.SUoMEntry,
                                             PUoMEntry = mOITM.PUoMEntry,
                                             PriceUnit = mOITM.PriceUnit,
                                             ManSerNum = mOITM.ManSerNum,
                                             ManBtchNum = mOITM.ManBtchNum
                                         }).ToList();
                }
                else
                {
                    mWhere += ((!string.IsNullOrEmpty(pItemName) && !string.IsNullOrEmpty(pItemCode)) ? "and ([OITM].ItemCode like '%" + pItemCode + "%' or [OITM].ItemName like '%" + pItemName + "%')" : (!string.IsNullOrEmpty(pItemName) ? "and ([OITM].ItemName like '%" + pItemName + "%')" : (!string.IsNullOrEmpty(pItemCode) ? "and ([OITM].ItemCode like '%" + pItemCode + "%')" : "")));
                    mQuery = SQLQueryUtil.getSelectQuery2("Select count(*) as Counter", mFrom, null, mWhere);
                    DataTable dt = SQLQueryUtil.GetQueryResult(mQuery, mConnectionString);
                    mTotalRecords = Convert.ToInt32(dt.Rows[0]["Counter"]);
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelect, mFrom, pListUDFOITM, mWhere, mOrderBy);
                    listItemMasterSAP = Mapper.Map<List<ItemMasterSAP>>(UDFUtil.GetDBObjectList(mQuery, mConnectionString, pListUDFOITM, typeof(OITM), "OITM").Cast<OITM>().ToList());
                }

                foreach (ItemMasterSAP mItemMasterSAP in listItemMasterSAP)
                {
                    if (mItemMasterSAP.UgpEntry != -1)
                    {
                        OUGP mOUGPAux = mDbContext.OUGP.Where(c => c.UgpEntry == mItemMasterSAP.UgpEntry).FirstOrDefault();
                        if (mOUGPAux != null)
                        {
                            mItemMasterSAP.UomGroupName = mOUGPAux.UgpName;
                        }
                        mItemMasterSAP.UnitOfMeasureList.AddRange((from mUGP1 in mDbContext.UGP1
                                                                   join mUOM in mDbContext.OUOM on mUGP1.UomEntry equals mUOM.UomEntry
                                                                   where mUGP1.UgpEntry == mItemMasterSAP.UgpEntry
                                                                   select new UnitOfMeasure()
                                                                   {
                                                                       UomEntry = mUOM.UomEntry,
                                                                       UomCode = mUOM.UomCode,
                                                                       UomName = mUOM.UomName,
                                                                       LineNum = mUGP1.LineNum,
                                                                       BaseQty = mUGP1.BaseQty
                                                                   }).ToList());
                        if (mItemMasterSAP.SUoMEntry != null && pSellItem == "Y")
                        {
                            mItemMasterSAP.UnitOfMeasureList = mItemMasterSAP.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                            mItemMasterSAP.UnitOfMeasureList.Where(c => c.UomEntry == mItemMasterSAP.SUoMEntry).FirstOrDefault().LineNum = 1;
                        }
                        if (mItemMasterSAP.PUoMEntry != null && pPrchseItem == "Y")
                        {
                            mItemMasterSAP.UnitOfMeasureList = mItemMasterSAP.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                            mItemMasterSAP.UnitOfMeasureList.Where(c => c.UomEntry == mItemMasterSAP.PUoMEntry).FirstOrDefault().LineNum = 1;
                        }
                    }
                    else
                    {
                        mItemMasterSAP.UomGroupName = "Manual";
                        mItemMasterSAP.UnitOfMeasureList.Add(new UnitOfMeasure() { UomEntry = -1, UomCode = "Manual", UomName = "Manual", LineNum = 1, BaseQty = 1 });
                    }
                }

                if (!pItemWithStock.Equals("Y"))
                {
                    mJsonObjectResult.ItemMasterSAPList = listItemMasterSAP;
                    mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());
                }
                else
                {
                    List<StockModel> stockModel = GetItemStock(pCompanyParam,
                        listItemMasterSAP.Select(s => s.ItemCode).ToArray(),
                        pWarehousePortalList);


                    mJsonObjectResult.ItemMasterSAPList = (from items in listItemMasterSAP
                                                           join stock in stockModel
                                                           on items.ItemCode equals
                                                           stock.ItemCode
                                                           select items)
                                                           .Distinct()
                                                           .ToList();

                    mJsonObjectResult.Others.Add("TotalRecords", mJsonObjectResult.ItemMasterSAPList.Count.ToString());

                    mJsonObjectResult.ItemMasterSAPList = mJsonObjectResult.ItemMasterSAPList;
                }


                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMasterDS -> GetOITMListBy :" + ex.Message);
                Logger.WriteError("ServiceItemMasterDS -> GetOITMListBy :" + ex.InnerException);
                return mJsonObjectResult;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pModelCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public StockModel GetStockByModel(CompanyConn pCompanyParam, string pModelCode, List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<StockModel> result = new List<StockModel>();
                List<string> mWhsCodeListAux = pWarehousePortalList.Select(c => c.WhsCode).ToList();
                result = (from mOITW in mDbContext.OITW
                          join mOITM in mDbContext.OITM on mOITW.ItemCode equals mOITM.ItemCode
                          join mOWHS in mDbContext.OWHS on mOITW.WhsCode equals mOWHS.WhsCode
                          where
                          (
                             mOITM.U_ARGNS_MOD.Equals(pModelCode) &&
                             (pWarehousePortalList.Count > 0 ? mWhsCodeListAux.Contains(mOITW.WhsCode) : true)
                             && ((mOITW.OnHand + mOITW.OnOrder + mOITW.IsCommited)) > 0
                          )
                          select new StockModel()
                          {
                              ItemCode = mOITW.ItemCode,
                              WhsCode = mOITW.WhsCode,
                              WhsName = mOWHS.WhsName,
                              OnHand = mOITW.OnHand,
                              IsCommited = mOITW.IsCommited,
                              OnOrder = mOITW.OnOrder,
                              U_ARGNS_ITYPE = mOITM.U_ARGNS_ITYPE
                          }).ToList();

                StockModel returnValue = new StockModel();
                //returnValue.OnHand = result.Sum(s => s.OnHand);
                returnValue.OnHand = result.Where(c => c.U_ARGNS_ITYPE != "P").Sum(s => s.OnHand);
                returnValue.OnHandPrePack = result.Where(c => c.U_ARGNS_ITYPE == "P").Sum(s => s.OnHand);
                returnValue.OnOrder = result.Sum(s => s.OnOrder);
                returnValue.Available = result.Sum(s => s.Available);
                returnValue.IsCommited = result.Sum(s => s.IsCommited);
                //returnValue.Total = result.Sum(s => s.Total);
                returnValue.Total = result.Where(c => c.U_ARGNS_ITYPE != "P").Sum(s => s.Total);
                returnValue.TotalPrepack = result.Where(c => c.U_ARGNS_ITYPE == "P").Sum(s => s.Total);
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetItemStock :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pListItems"></param>
        /// <param name="pItemType"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetOITMListByList(CompanyConn pCompanyParam, List<string> pListItems, string pItemType)
        {
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            Mapper.CreateMap<ItemMasterSAP, OITM>();
            Mapper.CreateMap<OITM, ItemMasterSAP>();

            List<ItemMasterSAP> ListItemMasterSAP = new List<ItemMasterSAP>();
            try
            {
                string mQuery = string.Empty;
                string mSelect = "Select [OITM].ItemCode, [OITM].ItemName, [OITM].InvntItem, [OITM].PrchseItem, [OITM].SellItem, [OITM].ItmsGrpCod, [OITM].PicturName, [OITM].UgpEntry, [OITM].TaxCodeAP, [OITM].TaxCodeAR, [OITM].SUoMEntry, [OITM].PUoMEntry ";
                string mFrom = " FROM [OITM] ";
                string mWhere = " WHERE 1=1 ";
                if (pListItems != null)
                {
                    mWhere += " and ([OITM].ItemCode in (";
                    for (int i = 0; i < pListItems.Count; i++)
                    {
                        if (i == 0)
                            mWhere += "'" + pListItems[i] + "'";
                        else
                            mWhere += ",'" + pListItems[i] + "'";
                    }
                    mWhere += "))";
                }
                mWhere += (!string.IsNullOrEmpty(pItemType) ? " and U_ARGNS_ITYPE = '" + pItemType + "'" : "");
                mQuery = SQLQueryUtil.getSelectQuery2(mSelect, mFrom, null, mWhere, null);
                ListItemMasterSAP = Mapper.Map<List<ItemMasterSAP>>(UDFUtil.GetDBObjectList(mQuery, mConnectionString, new List<UDF_ARGNS>(), typeof(OITM), "OITM").Cast<OITM>().ToList());

                return ListItemMasterSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMasterDS -> GetOITMListByList :" + ex.Message);
                Logger.WriteError("ServiceItemMasterDS -> GetOITMListByList :" + ex.InnerException);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="CheckApparelInstallation"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pModCode"></param>
        /// <param name="pCostPriceList"></param>
        /// <param name="pMappedUdf"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pItemName"></param>
        /// <param name="uCardCode"></param>
        /// <param name="pItemGroupCode"></param>
        /// <param name="pOnlyActiveItems"></param>
        /// <param name="pListUDFOITM"></param>
        /// <param name="pNoItemsWithZeroStockCk"></param>
        /// <param name="pSupplierCardCode"></param>
        /// <param name="pItemList"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetQuickOrderListBy(CompanyConn pCompanyParam, bool CheckApparelInstallation,
            string pPrchseItem, string pSellItem, string pModCode, int? pCostPriceList,
            List<UDF_ARGNS> pMappedUdf, string pItemCode, string pItemName,
            string uCardCode, short pItemGroupCode, bool pOnlyActiveItems, bool isSO,
            List<UDF_ARGNS> pListUDFOITM = null,
            bool pNoItemsWithZeroStockCk = false, string pSupplierCardCode = "",
            List<string> pItemList = null, int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null)
        {
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            List<ItemMasterSAP> ListItemMasterSAP = new List<ItemMasterSAP>();
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            List<OITM> ListOITM;

            #region Typing Query to get Items

            int mTotalRecords;
            string mQuery = string.Empty;
            string mSelectQuery = "SELECT [OITM].[ItemCode], [OITM].[ItemName], [OITM].[InvntItem], [OITM].[PrchseItem], [OITM].[SellItem], [OITM].[ItmsGrpCod], [OITM].[PicturName], [OITM].[UgpEntry], [OITM].[CardCode], [OCRD].[CardName], [T1].[OnHand], [T1].[OnOrder], [T1].[IsCommited], [OITM].[TaxCodeAP], [OITM].[TaxCodeAR], [OITM].[SUoMEntry], [OITM].[PUoMEntry] ";
            string mSelectSubQuery = " SELECT [OITM].[ItemCode], SUM([OITW].[OnHand]) AS OnHand, SUM([OITW].[OnOrder]) AS OnOrder, SUM([OITW].[IsCommited]) AS IsCommited";
            string mFromQuery = " FROM ({SubQuery}) AS T1 INNER JOIN [OITM] ON [OITM].[ItemCode] = [T1].[ItemCode] LEFT OUTER JOIN [OCRD] ON [OCRD].CardCode = [OITM].CardCode";
            string mFromSubQuery = " FROM [OITM] INNER JOIN [OITW] ON [OITW].[ItemCode] = [OITM].[ItemCode] INNER JOIN [OWHS] ON [OITW].[WhsCode] = [OWHS].[WhsCode]";
            string mWhereQuery = "WHERE 1 = 1 ";
            string mWhereSubQuery = " WHERE " + (pPrchseItem == "Y" ? "[OITM].[PrchseItem] = '" + pPrchseItem + "'" : "1 = 1");
            string mOrderBySubQuery = (pLength > 0 ? " ORDER BY [OITM]." + Utils.OrderString(pOrderColumn) + " OFFSET " + pStart + " ROWS FETCH NEXT " + pLength + " ROWS ONLY" : "");
            string mGroupBySubQuery = " GROUP BY [OITM].[ItemCode]";

            if (pNoItemsWithZeroStockCk)
            {
                mGroupBySubQuery += " HAVING(SUM([OITW].[OnHand]) + SUM([OITW].[OnOrder]) -SUM([OITW].[IsCommited])) > 0 ";
            }
            mWhereSubQuery += (pSellItem == "Y" ? " and [OITM].[SellItem] = '" + pSellItem + "'" : "");
            mWhereSubQuery += (pItemGroupCode != -1 ? " and [OITM].[ItmsGrpCod] = " + pItemGroupCode + "" : "");
            mWhereSubQuery += (pSupplierCardCode != "" ? " and [OITM].[CardCode] = '" + pSupplierCardCode + "'" : "");
            if (pItemList != null)
            {
                mWhereSubQuery += " and ([OITM].ItemCode in (";
                for (int i = 0; i < pItemList.Count; i++)
                {
                    if (i == 0)
                        mWhereSubQuery += "'" + pItemList[i] + "'";
                    else
                        mWhereSubQuery += ",'" + pItemList[i] + "'";
                }
                mWhereSubQuery += "))";
            }

            //Adding mapped udf filters to where
            foreach (UDF_ARGNS udf in pMappedUdf)
            {
                string mTypeID = string.Empty;

                if (!string.IsNullOrEmpty(udf.Value))
                {
                    mTypeID = pListUDFOITM.Where(c => c.UDFName == udf.UDFName).FirstOrDefault().TypeID;
                    if (mTypeID == "N")
                    {
                        mWhereSubQuery += " and [OITM].[" + udf.UDFName + "] = " + udf.Value;
                    }
                    else
                    {
                        mWhereSubQuery += " and [OITM].[" + udf.UDFName + "] = '" + udf.Value + "'";
                    }
                }
            }

            if (!string.IsNullOrEmpty(pItemCode) && !string.IsNullOrEmpty(pItemName))
            {
                mWhereSubQuery += " and ([OITM].[ItemCode] like '%" + pItemCode + "%' or [OITM].[ItemName] like '%" + pItemName + "%')";
            }
            else if (!string.IsNullOrEmpty(pItemCode))
            {
                mWhereSubQuery += " and ([OITM].[ItemCode] like '%" + pItemCode + "%')";
            }
            else if (!string.IsNullOrEmpty(pItemName))
            {
                mWhereSubQuery += " and ([OITM].[ItemName] like '%" + pItemName + "%')";
            }


            if (pCostPriceList != null)
            {
                mSelectQuery += ", [ITM1].[Price] as 'CostPrice', [ITM1].[Currency] as 'CostPriceCurrency'";
                mFromQuery += " left outer join [ITM1] on [OITM].[ItemCode] = [ITM1].[ItemCode]";
                mWhereQuery += " AND [ITM1].[PriceList] = " + pCostPriceList;
            }

            if (pOnlyActiveItems)
            {
                mWhereSubQuery += " AND [OITM].[validFor] = 'Y'";
            }

            #endregion

            try
            {
                #region Getting Items

                if (CheckApparelInstallation)
                {
                    mSelectQuery += ", [OITM].[U_ARGNS_MOD]";

                    //Getting the total of records
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, null, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2("Select count(*) as Counter ", "from(" + mQuery + ") as TT", null);
                    DataTable dt = SQLQueryUtil.GetQueryResult(mQuery, mConnectionString);
                    mTotalRecords = Convert.ToInt32(dt.Rows[0]["Counter"]);

                    //Getting the items
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, mOrderBySubQuery, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectQuery, mFromQuery.Replace("{SubQuery}", mQuery), pListUDFOITM, mWhereQuery);
                    ListOITM = UDFUtil.GetDBObjectList(mQuery, mConnectionString, pListUDFOITM, typeof(OITM), "OITM").Cast<OITM>().ToList();

                    ListItemMasterSAP = (from mOITM in ListOITM
                                         where (
                                             (pModCode != "" ? (mOITM.U_ARGNS_MOD != null ? mOITM.U_ARGNS_MOD.ToUpper().Contains(pModCode.ToUpper()) : false) : true)
                                         )
                                         select new ItemMasterSAP()
                                         {
                                             ItemCode = mOITM.ItemCode,
                                             ItemName = mOITM.ItemName,
                                             InvntItem = mOITM.InvntItem,
                                             PrchseItem = mOITM.PrchseItem,
                                             SellItem = mOITM.SellItem,
                                             ItmsGrpCod = mOITM.ItmsGrpCod,
                                             MappedUdf = mOITM.MappedUdf,
                                             PicturName = mOITM.PicturName,
                                             UgpEntry = mOITM.UgpEntry,
                                             CardCode = mOITM.CardCode,
                                             CardName = mOITM.CardName,
                                             CostPriceList = pCostPriceList,
                                             CostPrice = mOITM.CostPrice,
                                             CostPriceCurrency = mOITM.CostPriceCurrency,
                                             Stock = new StockModel(mOITM.ItemCode, mOITM.OnHand, mOITM.OnOrder, mOITM.IsCommited),
                                             TaxCodeAP = mOITM.TaxCodeAP,
                                             TaxCodeAR = mOITM.TaxCodeAR,
                                             SUoMEntry = mOITM.SUoMEntry,
                                             PUoMEntry = mOITM.PUoMEntry
                                         }).ToList();
                }
                else
                {
                    //Getting the total of records
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, null, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2("Select count(*) as Counter ", "from(" + mQuery + ") as TT", null);
                    DataTable dt = SQLQueryUtil.GetQueryResult(mQuery, mConnectionString);
                    mTotalRecords = Convert.ToInt32(dt.Rows[0]["Counter"]);

                    //Getting the items
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectSubQuery, mFromSubQuery, null, mWhereSubQuery, mOrderBySubQuery, mGroupBySubQuery);
                    mQuery = SQLQueryUtil.getSelectQuery2(mSelectQuery, mFromQuery.Replace("{SubQuery}", mQuery), pListUDFOITM, mWhereQuery);
                    ListOITM = UDFUtil.GetDBObjectList(mQuery, mConnectionString, pListUDFOITM, typeof(OITM), "OITM").Cast<OITM>().ToList();

                    ListItemMasterSAP = (from mOITM in ListOITM
                                         select new ItemMasterSAP()
                                         {
                                             ItemCode = mOITM.ItemCode,
                                             ItemName = mOITM.ItemName,
                                             InvntItem = mOITM.InvntItem,
                                             PrchseItem = mOITM.PrchseItem,
                                             SellItem = mOITM.SellItem,
                                             ItmsGrpCod = mOITM.ItmsGrpCod,
                                             MappedUdf = mOITM.MappedUdf,
                                             PicturName = mOITM.PicturName,
                                             UgpEntry = mOITM.UgpEntry,
                                             CardCode = mOITM.CardCode,
                                             CardName = mOITM.CardName,
                                             CostPriceList = pCostPriceList,
                                             CostPrice = mOITM.CostPrice,
                                             CostPriceCurrency = mOITM.CostPriceCurrency,
                                             Stock = new StockModel(mOITM.ItemCode, mOITM.OnHand, mOITM.OnOrder, mOITM.IsCommited),
                                             TaxCodeAP = mOITM.TaxCodeAP,
                                             TaxCodeAR = mOITM.TaxCodeAR,
                                             SUoMEntry = mOITM.SUoMEntry,
                                             PUoMEntry = mOITM.PUoMEntry
                                         }).ToList();
                }

                #endregion

                #region Getting Units of Meassure

                foreach (ItemMasterSAP mItemMasterSAP in ListItemMasterSAP)
                {
                    if (mItemMasterSAP.UgpEntry != -1)
                    {
                        mItemMasterSAP.UnitOfMeasureList.AddRange((from mUGP1 in mDbContext.UGP1
                                                                   join mUOM in mDbContext.OUOM on mUGP1.UomEntry equals mUOM.UomEntry
                                                                   where mUGP1.UgpEntry == mItemMasterSAP.UgpEntry
                                                                   select new UnitOfMeasure()
                                                                   {
                                                                       UomEntry = mUOM.UomEntry,
                                                                       UomCode = mUOM.UomCode,
                                                                       UomName = mUOM.UomName,
                                                                       LineNum = mUGP1.LineNum,
                                                                       BaseQty = mUGP1.BaseQty,
                                                                       AltQty = mUGP1.AltQty
                                                                   }).ToList());
                        if (mItemMasterSAP.SUoMEntry != null && pSellItem == "Y")
                        {
                            mItemMasterSAP.UnitOfMeasureList = mItemMasterSAP.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                            mItemMasterSAP.UnitOfMeasureList.Where(c => c.UomEntry == mItemMasterSAP.SUoMEntry).FirstOrDefault().LineNum = 1;
                        }
                        if (mItemMasterSAP.PUoMEntry != null && pPrchseItem == "Y")
                        {
                            mItemMasterSAP.UnitOfMeasureList = mItemMasterSAP.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                            mItemMasterSAP.UnitOfMeasureList.Where(c => c.UomEntry == mItemMasterSAP.PUoMEntry).FirstOrDefault().LineNum = 1;
                        }
                    }
                    else
                    {
                        mItemMasterSAP.UnitOfMeasureList.Add(new UnitOfMeasure() { UomEntry = -1, UomCode = "Manual", UomName = "Manual", LineNum = 1, BaseQty = 1 });
                    }
                }

                #endregion

                mDbContext.Dispose();

                #region Getting Items Prices

                ServicesProcess mServiceProcess = new ServicesProcess();
                ListItemMasterSAP = mServiceProcess.GetItemPriceInBatch(ListItemMasterSAP, uCardCode, pCompanyParam.DSSessionId, true, isSO);

                #endregion

                mJsonObjectResult.ItemMasterSAPList = ListItemMasterSAP;
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());
                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServiceItemMasterDS -> GetQuickOrderListBy:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="ListItems"></param>
        /// <param name="CardCode"></param>
        /// <param name="pSession"></param>
        /// <param name="pOrderItems"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsPrice(CompanyConn pCompanyParam, List<ItemMasterSAP> ListItems, string CardCode, string pSession, bool IsSo, bool pOrderItems = true, string ano = "")
        {
            //Obtengo el ConnString con los Parametros de la Company y abro conexion
            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            OCRD mBP = mDbContext.OCRD.Where(c => c.CardCode == CardCode).FirstOrDefault();

            foreach (ItemMasterSAP item in ListItems)
            {
                List<ITM9> prices = mDbContext.ITM9.Where(c => c.ItemCode == item.ItemCode && c.PriceList == mBP.ListNum).ToList();
                foreach (ITM9 itemUOMPrice in prices)
                {
                    item.ItemPrices.Add(new ItemPrices { Currency = itemUOMPrice.Currency, ItemCode = itemUOMPrice.ItemCode, Price = itemUOMPrice.Price, UOMCode = itemUOMPrice.UomEntry.ToString(), PriceList = mBP.ListNum, HasCurrencyPrice = true, HasUOMPrice = true });
                }
            }

            mDbContext.Dispose();

            ServicesProcess mServiceProcess = new ServicesProcess();

            return mServiceProcess.GetItemPriceInBatch(ListItems, CardCode, pSession, pOrderItems, IsSo, ano);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="ListItems"></param>
        public void GetDefaultWarehouse(CompanyConn pCompanyParam, List<ItemMasterSAP> ListItems)
        {
            string mWhs = string.Empty;
            try
            {
                foreach (ItemMasterSAP item in ListItems)
                {

                    //1. Check si El Item tiene un Def Whs
                    mWhs = mDbContext.OITM.Where(c => c.ItemCode == item.ItemCode).Select(x => x.DfltWH).Single();
                    if (string.IsNullOrEmpty(mWhs))
                    {
                        //2. Check si El usuario Conectado tiene Whs por Def 
                        mWhs = (from mUser in mDbContext.OUSR
                                join mDefUser in mDbContext.OUDG on mUser.DfltsGroup equals mDefUser.Code
                                where mUser.USER_CODE == pCompanyParam.UserName
                                select mDefUser
                                  ).Single().Warehouse;

                        if (string.IsNullOrEmpty(mWhs))
                        {
                            //3. Por Ultimo Busca el Default Whs de Gereral Setting
                            item.DfltWH = mDbContext.OADM.Select(c => c.DfltWhs).Single();

                        }
                        else
                        {
                            item.DfltWH = mWhs;
                        }

                    }
                    else
                    {
                        item.DfltWH = mWhs;
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetDefaultWarehouse:" + ex.Message);
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pPrchseItem"></param>
        /// <param name="pSellItem"></param>
        /// <param name="pCodeBar"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCodeBar(CompanyConn pCompanyParam, string pPrchseItem, string pSellItem, string pCodeBar)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ItemMasterSAP ItemReturn = mDbContext.OBCD.Where(c => c.BcdCode == pCodeBar).Select(j => new ItemMasterSAP { ItemCode = j.ItemCode, UomEntry = j.UomEntry }).FirstOrDefault();

                return ItemReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GeItemByCodeBar:" + ex.Message);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open) { mDbContext.Database.Connection.Close(); }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<StockModel> GetItemStock(CompanyConn pCompanyParam,
            string[] pItemCode,
            List<WarehousePortal> pWarehousePortalList)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                if (mDbContext.Database.Connection.State != ConnectionState.Open)
                {
                    mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                    mDbContext.Database.Connection.Open();
                }

                List<StockModel> result = new List<StockModel>();
                List<string> mWhsCodeListAux = pWarehousePortalList.Select(c => c.WhsCode).ToList();
                result = (from mOITW in mDbContext.OITW
                          join mOITM in mDbContext.OITM on mOITW.ItemCode equals mOITM.ItemCode
                          join mOWHS in mDbContext.OWHS on mOITW.WhsCode equals mOWHS.WhsCode
                          where
                          (
                             (pItemCode.Count() == 0 ? (true) : (pItemCode.Contains(mOITM.ItemCode)))
                             && ((mOITW.OnHand + mOITW.OnOrder - mOITW.IsCommited)) > 0
                             && (pWarehousePortalList.Count > 0 ? mWhsCodeListAux.Contains(mOITW.WhsCode) : true)
                          )
                          select new StockModel()
                          {
                              ItemCode = mOITW.ItemCode,
                              WhsCode = mOITW.WhsCode,
                              WhsName = mOWHS.WhsName,
                              OnHand = mOITW.OnHand,
                              IsCommited = mOITW.IsCommited,
                              OnOrder = mOITW.OnOrder
                          }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetItemStock :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        public ItemMasterSAP GeItemByCode(CompanyConn pCompanyParam, string pItemCode, string pBarCode = null, List<UDF_ARGNS> pListUDF = null)
        {
            try
            {
                Mapper.CreateMap<ItemMasterSAP, OITM>();
                Mapper.CreateMap<OITM, ItemMasterSAP>();

                Mapper.CreateMap<ItemGroup, OITB>();
                Mapper.CreateMap<OITB, ItemGroup>();

                Mapper.CreateMap<PriceListSAP, OPLN>();
                Mapper.CreateMap<OPLN, PriceListSAP>();

                Mapper.CreateMap<AttachmentSAP, ATC1>();
                Mapper.CreateMap<ATC1, AttachmentSAP>();

                connectionString = Utils.ConnectionString(pCompanyParam);

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                if (!string.IsNullOrEmpty(pBarCode))
                {
                    OBCD AuxItem = mDbContext.OBCD.Where(c => c.BcdCode == pBarCode).FirstOrDefault();
                    pItemCode = (AuxItem != null ? AuxItem.ItemCode : pItemCode);
                }

                ItemMasterSAP ItemReturn = (from mOITM in mDbContext.OITM
                                            where (
                                                (mOITM.ItemCode == pItemCode)
                                            )
                                            select new ItemMasterSAP()
                                            {
                                                ItemCode = mOITM.ItemCode,
                                                ItemName = mOITM.ItemName,
                                                InvntItem = mOITM.InvntItem,
                                                PrchseItem = mOITM.PrchseItem,
                                                SellItem = mOITM.SellItem,
                                                ItmsGrpCod = mOITM.ItmsGrpCod,
                                                CardCode = mOITM.CardCode ?? "",
                                                PicturName = mOITM.PicturName,
                                                UserText = mOITM.UserText,
                                                ItemType = mOITM.ItemType,
                                                AtcEntry = mOITM.AtcEntry,
                                                UgpEntry = mOITM.UgpEntry,
                                                TaxCodeAP = mOITM.TaxCodeAP,
                                                TaxCodeAR = mOITM.TaxCodeAR,
                                                validFor = mOITM.validFor,
                                                validFrom = mOITM.validFrom,
                                                validTo = mOITM.validTo,
                                                frozenFor = mOITM.frozenFor,
                                                frozenFrom = mOITM.frozenFrom,
                                                frozenTo = mOITM.frozenTo,
                                                SUoMEntry = mOITM.SUoMEntry,
                                                PUoMEntry = mOITM.PUoMEntry,
                                                CodeBars = mOITM.CodeBars ?? ""
                                            }).FirstOrDefault();

                if (ItemReturn != null)
                {
                    ItemReturn.ListItemGroup = Mapper.Map<List<ItemGroup>>(mDbContext.OITB.ToList());
                    ItemReturn.ListPriceListSAP = Mapper.Map<List<PriceListSAP>>(mDbContext.OPLN.ToList());

                    if (ItemReturn.AtcEntry != null)
                    {
                        ItemReturn.ListAttachmentSAP = Mapper.Map<List<AttachmentSAP>>(mDbContext.ATC1.Where(c => c.AbsEntry == ItemReturn.AtcEntry).ToList());
                    }

                    //Unit of Measure
                    if (ItemReturn.UgpEntry != -1)
                    {
                        OUGP mOUGPAux = mDbContext.OUGP.Where(c => c.UgpEntry == ItemReturn.UgpEntry).FirstOrDefault();
                        if (mOUGPAux != null)
                        {
                            ItemReturn.UomGroupName = mOUGPAux.UgpName;
                        }
                        ItemReturn.UnitOfMeasureList.AddRange((from mUGP1 in mDbContext.UGP1
                                                               join mUOM in mDbContext.OUOM on mUGP1.UomEntry equals mUOM.UomEntry
                                                               where mUGP1.UgpEntry == ItemReturn.UgpEntry
                                                               select new UnitOfMeasure()
                                                               {
                                                                   UomEntry = mUOM.UomEntry,
                                                                   UomCode = mUOM.UomCode,
                                                                   UomName = mUOM.UomName,
                                                                   LineNum = mUGP1.LineNum,
                                                                   BaseQty = mUGP1.BaseQty,
                                                                   AltQty = mUGP1.AltQty
                                                               }).ToList());
                        //if (ItemReturn.SUoMEntry != null && pSellItem == "Y")
                        //{
                        //    ItemReturn.UnitOfMeasureList = ItemReturn.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                        //    ItemReturn.UnitOfMeasureList.Where(c => c.UomEntry == ItemReturn.SUoMEntry).FirstOrDefault().LineNum = 1;
                        //}
                        //if (ItemReturn.PUoMEntry != null && pSellItem == "Y")
                        //{
                        //    ItemReturn.UnitOfMeasureList = ItemReturn.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                        //    ItemReturn.UnitOfMeasureList.Where(c => c.UomEntry == ItemReturn.PUoMEntry).FirstOrDefault().LineNum = 1;
                        //}
                    }
                    else
                    {
                        ItemReturn.UomGroupName = "Manual";
                        ItemReturn.UnitOfMeasureList.Add(new UnitOfMeasure() { UomEntry = -1, UomCode = "Manual", UomName = "Manual", LineNum = 1, BaseQty = 1 });
                    }
                }

                //Get Serials

                ItemReturn.SerialsSAP = GetSerialsByItemCode(mDbContext, pItemCode, pListUDF.Where(c => c.Document == "OSRN").ToList(), pCompanyParam.DSSessionId);

                string sqlSelect = string.Empty;
                string mWhere = "[OITM].[ItemCode] = '" + ItemReturn.ItemCode + "'";

                foreach (var item in pListUDF.Where(c => c.Document == "OITM").ToList())
                {
                    if (sqlSelect == string.Empty)
                    {
                        sqlSelect = item.UDFName;
                    }
                    else
                    { sqlSelect = sqlSelect + "," + item.UDFName; }

                }

                if (sqlSelect != string.Empty)
                {

                    sqlSelect = "Select " + sqlSelect + " From OITM where " + mWhere;

                    OITM OITM = (OITM)UDFUtil.GetDBObjectList(sqlSelect, connectionString, pListUDF.Where(c => c.Document == "OITM").ToList(), typeof(OITM), "OITM").FirstOrDefault();

                    var udfsItems = OITM.MappedUdf;

                    foreach (var itemUDF in udfsItems.Where(c => c.RTable != null))
                    {
                        var masterData = GlobalUtil.getMasterData(pCompanyParam.DSSessionId, itemUDF.RTable, $" Code = '{itemUDF.Value}' ", false);

                        itemUDF.Value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");
                    }

                    ItemReturn.MappedUdf = udfsItems;
                }

                return ItemReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GeItemByCode:" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pPriceList"></param>
        /// <returns></returns>
        public decimal GetPriceItemByPriceList(CompanyConn pCompanyParam, string pItemCode, short pPriceList)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ITM1 mITM1 = mDbContext.ITM1.Where(c => c.ItemCode == pItemCode && c.PriceList == pPriceList).FirstOrDefault();

                if (mITM1 != null)
                {
                    return mITM1.Price ?? 0;
                }

                return 0;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetPriceItemByPriceList:" + ex.Message);
                return 0;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<ItemGroupSAP> GetItemGroupList(CompanyConn pCompanyParam)
        {
            try
            {
                List<ItemGroupSAP> mItemGroupReturn = new List<ItemGroupSAP>();
                Mapper.CreateMap<ItemGroupSAP, OITB>();
                Mapper.CreateMap<OITB, ItemGroupSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                mItemGroupReturn = Mapper.Map<List<ItemGroupSAP>>(mDbContext.OITB.ToList());

                return mItemGroupReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetItemGroupList:" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCodeFrom"></param>
        /// <param name="pItemCodeTo"></param>
        /// <param name="pVendorFrom"></param>
        /// <param name="pVendorTo"></param>
        /// <param name="pItemGroup"></param>
        /// <param name="pSelectedWH"></param>
        /// <returns></returns>
        public JsonObjectResult GetInventoryStatus(CompanyConn pCompanyParam,
            string pItemCodeFrom, string pItemCodeTo, string pVendorFrom,
            string pVendorTo, string pItemGroup,
            string[] pSelectedWH, int pStart, int pLenght,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                string mWhere = " where InvntItem = 'Y' ", mSQLQuery = "", mFrom = " from OITM";
                string mCount = "SELECT COUNT(*) ";
                string mSelect = "SELECT OITM.[ItmsGrpCod], " +
                                    "OITM.[ItemCode], OITM.[ItemName], " +
                                 "OITM.[OnHand], OITM.[IsCommited], " +
                                 "OITM.[OnOrder], OITM.[CardCode] ";

                if (pSelectedWH != null)
                {
                    mSelect = "SELECT OITM.[ItmsGrpCod], OITM.[ItemCode], OITM.[ItemName], " +
                                 "AuxTable.[OnHand], AuxTable.[IsCommited], " +
                                 "AuxTable.[OnOrder], OITM.[CardCode] ";

                    mFrom += " INNER JOIN (select oitw.ItemCode, sum(oitw.OnHand) as OnHand, " +
                        "sum(oitw.IsCommited) as IsCommited, sum(oitw.OnOrder) as OnOrder from " +
                        "oitm inner join oitw on oitw.ItemCode = oitm.ItemCode where oitw.WhsCode in (";

                    for (int i = 0; i < pSelectedWH.Count(); i++)
                    {
                        if (i == 0)
                            mFrom += "'" + pSelectedWH[i] + "'";
                        else
                            mFrom += ",'" + pSelectedWH[i] + "'";
                    }

                    mFrom += ") group by oitw.ItemCode) AS AuxTable on OITM.ItemCode = AuxTable.ItemCode ";
                }

                if (!string.IsNullOrEmpty(pVendorFrom) || !string.IsNullOrEmpty(pVendorTo))
                {
                    mFrom += " INNER JOIN OCRD ON OITM.[CardCode] = OCRD.[CardCode] ";
                    mSelect += " ,OCRD.[CardName], OCRD.[Phone1] ";
                    mWhere += (!string.IsNullOrEmpty(pVendorFrom) ? " and OCRD.[CardCode] >= '" + pVendorFrom + "'" : "");
                    mWhere += (!string.IsNullOrEmpty(pVendorTo) ? " and OCRD.[CardCode] <= '" + pVendorTo + "'" : "");
                }

                if (!string.IsNullOrEmpty(pItemCodeFrom) || !string.IsNullOrEmpty(pItemCodeTo))
                {
                    mFrom += " INNER JOIN OITB ON OITM.[ItmsGrpCod] = OITB.[ItmsGrpCod] ";
                    mSelect += " ,OITB.[ItmsGrpNam] ";
                    mWhere += (!string.IsNullOrEmpty(pItemCodeFrom) ? " and OITM.[ItemCode] >= '" + pItemCodeFrom + "'" : "");
                    mWhere += (!string.IsNullOrEmpty(pItemCodeTo) ? " and OITM.[ItemCode] <= '" + pItemCodeTo + "'" : "");
                }

                mWhere += (!string.IsNullOrEmpty(pItemGroup) ? " and OITM.[ItmsGrpCod] = " + pItemGroup : "");

                mSQLQuery = mSelect + mFrom + mWhere +
                    string.Format(" ORDER BY {0} OFFSET {1} ROWS FETCH NEXT {2} ROWS ONLY",
                    Utils.OrderString(pOrderColumn),
                    pStart, pLenght);

                mJsonObjectResult.InventoryStatusList = UDFUtil.GetDBObjectList(mSQLQuery,
                    Utils.ConnectionString(pCompanyParam),
                    null, typeof(InventoryStatus),
                    "ItemMasterSAP")
                    .Cast<InventoryStatus>()
                    .ToList();

                int mTotalRecords = UDFUtil.GetCountValue(mCount + mFrom + mWhere,
                    Utils.ConnectionString(pCompanyParam));

                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetInventoryStatus:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pItemCode"></param>
        /// <param name="pWarehouse"></param>
        /// <returns></returns>
        public List<AvailableToPromise> GetATPByWarehouse(CompanyConn pCompanyParam, string pItemCode, string pWarehouse)
        {
            try
            {
                List<AvailableToPromise> mListATP = new List<AvailableToPromise>();

                string mSelectOITW = "SELECT DISTINCT 'OITW' as Document, [OITW].[OnHand] FROM [OITW] WHERE [OITW].[ItemCode] = '" + pItemCode + "' and [OITW].[WhsCode] = '" + pWarehouse + "'";
                string mSelectOPOR = "SELECT DISTINCT 'OPOR' as Document, 'PO' as DocumentName, [OPOR].[DocEntry], [POR1].[LineNum], [OPOR].[CardCode], [OCRD].[CardName], [OPOR].[DocDate] as OrderDate, [POR1].[ShipDate] as DeliveryDate, [POR1].[OpenQty] as Ordered, null as Committed, [UOM].[UomCode], [UOM].[UomName], [UOM].[BaseQty] FROM [OPOR] inner join [POR1] on [OPOR].[DocEntry] = [POR1].[DocEntry] " +
                                     " inner join [OCRD] on [OPOR].[CardCode] = [OCRD].[CardCode] LEFT JOIN (SELECT [OUOM].[UomEntry], [OUOM].[UomCode], [OUOM].[UomName], [UGP1].[BaseQty] from [OUOM] inner join [UGP1] on [OUOM].[UomEntry] = [UGP1].[UomEntry]) AS UOM on [POR1].[UomCode] = [UOM].[UomCode] WHERE [OPOR].[DocStatus] = 'O' and [POR1].[LineStatus] = 'O' and [POR1].[ItemCode] = '" + pItemCode + "' and [POR1].[WhsCode] = '" + pWarehouse + "'";
                string mSelectORDR = "SELECT DISTINCT 'ORDR' as Document, 'SO' as DocumentName, [ORDR].[DocEntry], [RDR1].[LineNum], [ORDR].[CardCode], [OCRD].[CardName], [ORDR].[DocDate] as OrderDate, [RDR1].[ShipDate] as DeliveryDate, null as Ordered, [RDR1].[OpenQty] as Committed, [UOM].[UomCode], [UOM].[UomName], [UOM].[BaseQty] FROM [ORDR] inner join [RDR1] on [ORDR].[DocEntry] = [RDR1].[DocEntry] " +
                                     " inner join [OCRD] on [ORDR].[CardCode] = [OCRD].[CardCode] LEFT JOIN (SELECT [OUOM].[UomEntry], [OUOM].[UomCode], [OUOM].[UomName], [UGP1].[BaseQty] from [OUOM] inner join [UGP1] on [OUOM].[UomEntry] = [UGP1].[UomEntry]) AS UOM on [RDR1].[UomCode] = [UOM].[UomCode] WHERE [ORDR].[DocStatus] = 'O' and [RDR1].[LineStatus] = 'O' and [RDR1].[ItemCode] = '" + pItemCode + "' and [RDR1].[WhsCode] = '" + pWarehouse + "'";
                mListATP.AddRange(UDFUtil.GetDBObjectList(mSelectOITW, Utils.ConnectionString(pCompanyParam), null, typeof(AvailableToPromise), "AvailableToPromise").Cast<AvailableToPromise>().ToList());
                mListATP.AddRange(UDFUtil.GetDBObjectList(mSelectOPOR, Utils.ConnectionString(pCompanyParam), null, typeof(AvailableToPromise), "AvailableToPromise").Cast<AvailableToPromise>().ToList());
                mListATP.AddRange(UDFUtil.GetDBObjectList(mSelectORDR, Utils.ConnectionString(pCompanyParam), null, typeof(AvailableToPromise), "AvailableToPromise").Cast<AvailableToPromise>().ToList());

                return mListATP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetATPByWarehouse:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pListItems"></param>
        /// <param name="pFormula"></param>
        /// <returns></returns>
        public List<ItemMasterSAP> GetItemsWithPositiveStock(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItems, string[] pFormula)
        {
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            string mQuery = string.Empty;
            List<StockModel> mAllStockList = new List<StockModel>();

            try
            {
                string mIn = "(";
                for (int i = 0; i < pListItems.Count; i++)
                {
                    if (i == 0)
                        mIn += "'" + pListItems[i].ItemCode + "'";
                    else
                        mIn += ",'" + pListItems[i].ItemCode + "'";
                }
                mIn += ")";

                mQuery = "SELECT [OITW].[ItemCode], [OITW].[OnHand] AS OnHand, [OITW].[OnOrder] AS OnOrder, [OITW].[IsCommited] AS IsCommited, [OITW].[WhsCode] AS Warehouse FROM [OITW] INNER JOIN [OITM] ON [OITW].[ItemCode] = [OITM].[ItemCode] INNER JOIN [OWHS] ON [OITW].[WhsCode] = [OWHS].[WhsCode] ";
                mQuery += (pListItems.Count > 0 ? "WHERE [OITW].[ItemCode] IN " + mIn : "");

                mAllStockList = Mapper.Map<List<StockModel>>(UDFUtil.GetDBObjectList(mQuery, mConnectionString, null, typeof(StockModel), "").Cast<StockModel>().ToList());
                foreach (StockModel mStockAux in mAllStockList)
                {
                    mStockAux.Available = ARGNS.Util.Miscellaneous.GetAvailable(pFormula, mStockAux);
                }
                mAllStockList = mAllStockList.Where(c => c.Available > 0).ToList();

                return pListItems.Where(c => mAllStockList.Select(j => j.ItemCode).Distinct().Contains(c.ItemCode)).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetItemsWithPositiveStock:" + ex.Message);
                return null;
            }
        }


        public List<ItemMasterSAP> GeUOMByItemList(CompanyConn pCompanyParam, List<ItemMasterSAP> pListItem, string pSellItem, string pPrchseItem)
        {
            try
            {
                List<ItemMasterSAP> pListItemAux = GetOITMListByList(pCompanyParam, pListItem.Select(c => c.ItemCode).ToList(), null);

                foreach (ItemMasterSAP mItem in pListItem)
                {
                    ItemMasterSAP mItemAux = pListItemAux.Where(j => j.ItemCode == mItem.ItemCode).FirstOrDefault();
                    if (mItemAux != null)
                    {
                        mItem.UgpEntry = mItemAux.UgpEntry;
                        mItem.SUoMEntry = mItemAux.SUoMEntry;
                        mItem.PUoMEntry = mItemAux.PUoMEntry;
                        mItem.TaxCodeAP = mItemAux.TaxCodeAP;
                        mItem.TaxCodeAR = mItemAux.TaxCodeAR;
                        if (mItem.UgpEntry != -1)
                        {
                            OUGP mOUGPAux = mDbContext.OUGP.Where(c => c.UgpEntry == mItem.UgpEntry).FirstOrDefault();
                            if (mOUGPAux != null)
                            {
                                mItem.UomGroupName = mOUGPAux.UgpName;
                            }
                            mItem.UnitOfMeasureList.AddRange((from mUGP1 in mDbContext.UGP1
                                                              join mUOM in mDbContext.OUOM on mUGP1.UomEntry equals mUOM.UomEntry
                                                              where mUGP1.UgpEntry == mItem.UgpEntry
                                                              select new UnitOfMeasure()
                                                              {
                                                                  UomEntry = mUOM.UomEntry,
                                                                  UomCode = mUOM.UomCode,
                                                                  UomName = mUOM.UomName,
                                                                  LineNum = mUGP1.LineNum,
                                                                  BaseQty = mUGP1.BaseQty
                                                              }).ToList());
                            if (mItem.SUoMEntry != null && pSellItem == "Y")
                            {
                                mItem.UnitOfMeasureList = mItem.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                                mItem.UnitOfMeasureList.Where(c => c.UomEntry == mItem.SUoMEntry).FirstOrDefault().LineNum = 1;
                            }
                            if (mItem.PUoMEntry != null && pPrchseItem == "Y")
                            {
                                mItem.UnitOfMeasureList = mItem.UnitOfMeasureList.Select(c => { c.LineNum = (c.LineNum + 1); return c; }).ToList();
                                mItem.UnitOfMeasureList.Where(c => c.UomEntry == mItem.PUoMEntry).FirstOrDefault().LineNum = 1;
                            }
                        }
                        else
                        {
                            mItem.UomGroupName = "Manual";
                            mItem.UnitOfMeasureList.Add(new UnitOfMeasure() { UomEntry = -1, UomCode = "Manual", UomName = "Manual", LineNum = 1, BaseQty = 1 });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GeUOMByItemList:" + ex.Message);
                throw ex;
            }
            return pListItem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pWarehousePortalList"></param>
        /// <returns></returns>
        public List<Warehouse> GetAvailableWarehousesByCompany(CompanyConn pCompanyParam, List<WarehousePortal> pWarehousePortalList)
        {
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            string mQuery = string.Empty;
            List<Warehouse> mAllStockList = new List<Warehouse>();

            try
            {
                string mIn = "(";
                for (int i = 0; i < pWarehousePortalList.Count; i++)
                {
                    if (i == 0)
                        mIn += "'" + pWarehousePortalList[i].WhsCode + "'";
                    else
                        mIn += ",'" + pWarehousePortalList[i].WhsCode + "'";
                }
                mIn += ")";

                mQuery = "SELECT [OWHS].[WhsCode], [OWHS].[WhsName] FROM [OWHS]";
                mQuery += (pWarehousePortalList.Count > 0 ? " WHERE [OWHS].[WhsCode] IN " + mIn : "");

                mAllStockList = Mapper.Map<List<Warehouse>>(UDFUtil.GetDBObjectList(mQuery, mConnectionString, null, typeof(Warehouse), "").Cast<Warehouse>().ToList());

                return mAllStockList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("SericeItemMasterDS -> GetAvailableWarehousesByCompany:" + ex.Message);
                return null;
            }
        }

        private List<Serial> GetSerialsByItemCode(DataBase dbContext, string itemCode, List<UDF_ARGNS> pListUDF = null, string sessionId = null)
        {

            var wherebySAPWhitBin = $"join OSBQ T3 ON T1.MdAbsEntry = T3.SnBMDAbs  and T3.ItemCode = '{itemCode}' and  T3.OnHandQty > 0";

            var wherebySAPWhitOutBin = $"WHERE T0.ItemCode = '{itemCode}' and T1.MdAbsEntry NOT IN (SELECT SnBMDAbs FROM OSBQ)";


            var serialsWhitBin = dbContext.Database.SqlQuery<Serial>("SELECT T2.DistNumber,T1.SysNumber,T2.AbsEntry,T3.WhsCode, SUM(T1.AllocQty) AllocQty " +
               "FROM OITL T0 " +
               "JOIN ITL1 T1 ON T0.LogEntry = T1.LogEntry " +
               "JOIN OSRN T2 ON T1.SysNumber = T2.SysNumber AND T0.ItemCode = T2.ItemCode " +
               wherebySAPWhitBin +
               "GROUP BY  T2.DistNumber,T1.SysNumber,T3.WhsCode,T2.AbsEntry HAVING SUM(T1.AllocQty)=0").ToList<Serial>();


            foreach (var item in serialsWhitBin)
            {
                string mWhere = $"[OSRN].[DistNumber] = '{item.DistNumber}' and [OSRN].[ItemCode] = '{itemCode}' and [OSRN].[SysNumber] = {item.SysNumber}";
                item.UDFs = UDFUtil.GetObjectListWithUDF(pListUDF, mWhere, typeof(OSRN), connectionString, "OSRN").Cast<OSRN>().FirstOrDefault().MappedUdf;

                foreach (var itemUDF in item.UDFs.Where(c => c.RTable != null))
                {
                    var masterData = GlobalUtil.getMasterData(sessionId, itemUDF.RTable, $" Code = '{itemUDF.Value}' ", false);

                    itemUDF.Value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");
                }
            }

            var serialsWhitOutBin = dbContext.Database.SqlQuery<Serial>("SELECT T2.DistNumber,T1.SysNumber,T2.AbsEntry, SUM(T1.AllocQty) AllocQty " +
              "FROM OITL T0 " +
              "JOIN ITL1 T1 ON T0.LogEntry = T1.LogEntry " +
              "JOIN OSRN T2 ON T1.SysNumber = T2.SysNumber AND T0.ItemCode = T2.ItemCode " +
              wherebySAPWhitOutBin +
              "GROUP BY  T2.DistNumber,T1.SysNumber,T2.AbsEntry HAVING SUM(T1.AllocQty)=0").ToList<Serial>();


            foreach (var item in serialsWhitOutBin)
            {
                string mWhere = $"[OSRN].[DistNumber] = '{item.DistNumber}' and [OSRN].[ItemCode] = '{itemCode}' and [OSRN].[SysNumber] = {item.SysNumber}";
                item.UDFs = UDFUtil.GetObjectListWithUDF(pListUDF, mWhere, typeof(OSRN), connectionString, "OSRN").Cast<OSRN>().FirstOrDefault().MappedUdf;

                foreach (var itemUDF in item.UDFs.Where(c => c.RTable != null))
                {
                    var masterData = GlobalUtil.getMasterData(sessionId, itemUDF.RTable, $" Code = '{itemUDF.Value}' ", false);

                    itemUDF.Value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");
                }
            }

            return serialsWhitBin.Union(serialsWhitOutBin).ToList();
        }
    }
}