﻿using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebServices
{
    public static class GlobalUtil
    {
        private static B1WSHandler mB1WSHandler;

        public static ARGNS.Model.Implementations.SAP.MasterData getMasterData(string sessionId, string table, string condition, bool HANA)
        {
            ARGNS.Model.Implementations.SAP.MasterData listMasterData = new ARGNS.Model.Implementations.SAP.MasterData();
            try
            {
                mB1WSHandler = new B1WSHandler();

                string from = HANA == true ? $"\"@{table}\"" : $"[@{table}]";
                string where = condition == string.Empty ? string.Empty : $"where {condition}";

                string query = $"select * from {from} {where}";

                string head = "<?xml version=\"1.0\"?><env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"><env:Body><ExecuteSQLResponse xmlns=\"http://www.sap.com/SBO/DIS\"><BOM><BO><AdmInfo><Object>oRecordset</Object></AdmInfo>";
                string ending = "</BO></BOM></ExecuteSQLResponse></env:Body></env:Envelope>";
                string tablenodeIni = $"<{table}>";
                string tablenodeEnd = $"</{table}>";

                var documentresult = mB1WSHandler.ExecuetQuery(sessionId, query);

                documentresult = documentresult.Replace(head, "");
                documentresult = documentresult.Replace(ending, "");
                documentresult = documentresult.Replace(tablenodeIni, "<MasterData><Rows>");
                documentresult = documentresult.Replace(tablenodeEnd, "</Rows></MasterData>");
                documentresult = documentresult.Replace("<row>", "<MasterDataRows>");
                documentresult = documentresult.Replace("</row>", "</MasterDataRows>");

                listMasterData = Deserialize<ARGNS.Model.Implementations.SAP.MasterData>(documentresult);

            }
            catch (Exception ex)
            {
                Logger.WriteError("GlobalUtil -> getMasterData:" + ex.Message);
            }
            return listMasterData;
        }
        
        public static T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        public static string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }

        public static string getData(string sessionId, string table, string condition, string select, bool HANA)
        {
            string documentresult = string.Empty;
            try
            {
                mB1WSHandler = new B1WSHandler();

                string from = HANA == true ? $"\"{table}\"" : $"[@{table}]";
                string where = condition == string.Empty ? string.Empty : $"where {condition}";

                string query = $"select {select} from {from} {where}";

                string head = "<?xml version=\"1.0\"?><env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"><env:Body><ExecuteSQLResponse xmlns=\"http://www.sap.com/SBO/DIS\"><BOM><BO><AdmInfo><Object>oRecordset</Object></AdmInfo>";
                string ending = "</BO></BOM></ExecuteSQLResponse></env:Body></env:Envelope>";
                string tablenodeIni = $"<{table}>";
                string tablenodeEnd = $"</{table}>";

                documentresult = mB1WSHandler.ExecuetQuery(sessionId, query);
                

            }
            catch (Exception ex)
            {
                Logger.WriteError("GlobalUtil -> getData:" + ex.Message);
            }
            return documentresult;
        }
    }
    
}