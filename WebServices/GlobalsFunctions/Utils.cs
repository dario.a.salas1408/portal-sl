﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.View;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WebServices
{
	public static class Utils
    {
        public static void ReleaseObject(object pObj)
        {
            try
            {
                if (pObj != null)
                {
                    if (System.Runtime.InteropServices.Marshal.IsComObject(pObj))
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(pObj);
                    }

                    pObj = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }

            }
            catch (Exception)
            {
            }
        }

        public static String ConnectionString(CompanyConn pCompanyParam)
        {
            string ret = string.Empty;
            try
            {
                ret = @"data source=" + pCompanyParam.Server + ";initial catalog=" + pCompanyParam.CompanyDB + ";user id=" + pCompanyParam.DbUserName + ";password=" + pCompanyParam.DbPassword + ";Pooling=false;MultipleActiveResultSets=True;App=EntityFramework;";

                return ret;
            }
            catch (Exception)
            {
                return ret;
            }
        }

        public static XmlDocument GetSoapStructure(bool pIsUpdate, object pHeader, object pBody)
        {
            string body = string.Empty;
            XmlSerializer mySerializer = new XmlSerializer(pBody.GetType(), @"http://www.sap.com/SBO/DIS");
            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);
            xns.Add(string.Empty, @"http://www.sap.com/SBO/DIS");


            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = Encoding.GetEncoding("UTF-8")
            };

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, xmlWriterSettings))
                {
                    mySerializer.Serialize(xmlWriter, pBody, xns);
                }

                body = stringWriter.ToString();
            }
            
            MemoryStream streamDocs = new MemoryStream();
            //mySerializer.Serialize(streamDocs, pBody, xns);
            XmlDocument mXmlDocument = new XmlDocument();
            mXmlDocument.LoadXml(@"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Header></soap:Header><soap:Body><" + (pIsUpdate ? "Update" : "Add") + @" xmlns=""" + pHeader.GetType().GetProperty("ServiceName").GetValue(pHeader, null) + @"""></" + (pIsUpdate ? "Update" : "Add") + "></soap:Body></soap:Envelope>");
            XmlElement mBodyElement = mXmlDocument.DocumentElement;

            mBodyElement.SelectSingleNode("//*[local-name()='" + (pIsUpdate ? "Update" : "Add") + "']").InnerXml = (body).Remove(0, 22);

            streamDocs = new MemoryStream();

            mySerializer = new XmlSerializer(pHeader.GetType(), @"http://www.sap.com/SBO/DIS");
            mySerializer.Serialize(streamDocs, pHeader, xns);

            mBodyElement.SelectSingleNode("//*[local-name()='Header']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            return mXmlDocument;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pColumnToOrder"></param>
        /// <param name="pDBType"></param>
        /// <returns></returns>
        public static string OrderString(OrderColumn pColumnToOrder, string pDBType = "SQL")
        {
            string mReturnString = "";
            if(pColumnToOrder != null)
            { 
                if (pColumnToOrder.SortDirection == OrderColumn.OrderDirection.Ascendant)
                    mReturnString = (pDBType == "HANA" ? "\"" : "") + pColumnToOrder.Name + (pDBType == "HANA" ? "\"" : "") + " asc";
                else
                    mReturnString = (pDBType == "HANA" ? "\"" : "") + pColumnToOrder.Name + (pDBType == "HANA" ? "\"" : "") + " desc";
            }
            return mReturnString;
        }

    }
}
