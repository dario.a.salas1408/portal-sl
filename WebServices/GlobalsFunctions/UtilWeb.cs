﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebServices
{
    public class UtilWeb
    {

        public enum TransactionType
        {
            Add = 1,
            Update = 2,
            Delete = 3
        }

        public enum CurrentType
        {
            SystemCurrency,
            LocalCurrency,
            CurrencyRate,
        };

        public enum MethodType
        {
            GET,
            POST,
        };

        //public enum ServiceHANAName
        //{
        //    ActivitiesService,
        //    ApparelService,
        //    BPService,
        //    DocumentService,
        //    UserService,
        //};

        //Armo el Header del DI Server
        public static string ExecuteQuery(string pSession,string pQuery)
        {
            string ret = string.Empty;
            XmlDocument pXML = null;
            try
            {                
                string mXmlString = "<DoQuery>" + pQuery + "</DoQuery>";
                pXML = new System.Xml.XmlDocument();
                pXML.LoadXml(mXmlString);

                ret = @"<?xml version=""1.0"" ?>" +
                  @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                  "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                  @"<env:Body><dis:ExecuteSQL  xmlns:dis=""http://www.sap.com/SBO/DIS"">" + pXML.InnerXml + "</dis:ExecuteSQL></env:Body></env:Envelope>";

            }
            catch (Exception)
            {
                ret = string.Empty;
            }

            return ret;
        }

        public static string GetHeader(string pSession, XmlDocument pXML, int pType, string pObjectType)
        {
            string ret = string.Empty;

            switch (pType)
            {
                case 1:
                    ret = @"<?xml version=""1.0"" encoding=""UTF-16"" ?>" +
                      @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                      "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                      @"<env:Body><dis:Add  xmlns:dis=""http://www.sap.com/SBO/DIS""><Service>" + pObjectType + "</Service>" + pXML.InnerXml + "</dis:Add></env:Body></env:Envelope>";
                    break;
                case 2:

                    ret = @"<?xml version=""1.0"" encoding=""UTF-16"" ?>" +
                      @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                      "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                      @"<env:Body><dis:Update  xmlns:dis=""http://www.sap.com/SBO/DIS""><Service>" + pObjectType + "</Service>" + pXML.InnerXml + "</dis:Update></env:Body></env:Envelope>";
                    break;

                case 3:

                    ret = @"<?xml version=""1.0"" encoding=""UTF-16"" ?>" +
                      @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                      "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                      @"<env:Body><dis:Remove  xmlns:dis=""http://www.sap.com/SBO/DIS""><Service>" + pObjectType + "</Service>" + pXML.InnerXml + "</dis:Remove></env:Body></env:Envelope>";
                    break;


            }

            return ret;
        }

        //Recorro el XML para Obtener el Resultado recibido
        public static string GetResult(XmlDocument xmlD)
        {
            string ret = String.Empty;
            //Si La transaccion fue realizado con Exito
            XmlNodeList mElement = xmlD.GetElementsByTagName("RetKey");
            if (mElement.Count > 0)
            {
                foreach (XmlNode item in mElement)
                {
                    ret = item.InnerText;
                }

            }
            else
            {
                //Si La transaccion fallo, devuelvo el Error
                mElement = xmlD.GetElementsByTagName("env:Reason");
                if (mElement.Count > 0)
                {
                    foreach (XmlNode item in mElement)
                    {
                        ret = "Error:" + item.FirstChild.InnerText;
                    }
                }
            }

            return ret;
        }

        public static string GetResult(XmlDocument xmlD, string pTagName)
        {
            string ret = String.Empty;
            //Si La transaccion fue realizado con Exito
            XmlNodeList mElement = xmlD.GetElementsByTagName(pTagName);
            if (mElement.Count > 0)
            {
                foreach (XmlNode item in mElement)
                {
                    ret = item.InnerText;
                }

            }
            else
            {
                //Si La transaccion fallo, devuelvo el Error
                mElement = xmlD.GetElementsByTagName("env:Reason");
                if (mElement.Count > 0)
                {
                    foreach (XmlNode item in mElement)
                    {
                        ret = "Error:" + item.FirstChild.InnerText;
                    }
                }
            }
            return ret;
        }

    }
}