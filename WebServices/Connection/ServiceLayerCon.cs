﻿using Microsoft.Data.OData;
using System;
using System.Configuration;
using System.Data.Services.Client;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WebServices.SLService.SAPB1;

namespace WebServices.Connection
{
	public class ServiceLayerCon
    {
        public string strCurrentSessionGUID = string.Empty;
        public string strCurrentRouteIDString = string.Empty;
        private int currentDefaultPagingSizing = 10;
        private StringBuilder sbHttpRequestHeaders = new StringBuilder();           //Used to build HttpHeaders
        private StringBuilder sbHttpResponseHeaders = new StringBuilder();
        private ServiceLayer SLayer;


        //public CompanyConn Login(CompanyConn mCompanyConn)
        //{

        //    Uri service = new Uri(ConfigurationManager.AppSettings["WebB1s"]);
        //    SLayer = new ServiceLayer(service);

        //    SLayer.Format.UseJson();
        //    SLayer.Format.UseJson();

        //    SLayer.IgnoreMissingProperties = true;

        //    SLayer.SendingRequest += SLayer_SendingRequest;
        //    SLayer.ReceivingResponse += SLayer_ReceivingResponse;

        //    SLayer.MergeOption = MergeOption.OverwriteChanges;
        //    SLayer.Timeout = 7200;

        //    ServicePointManager.ServerCertificateValidationCallback += RemoteSSLTLSCertificateValidate;

        //    B1Session session = null;

        //    strCurrentSessionGUID = string.Empty;

        //    Uri login = new Uri(ConfigurationManager.AppSettings["WebB1s"] + "Login");
        //    //Use : UriOperationParameter for querying options.
        //    //Use : BodyOperationParameter for sending JSON body.
        //    BodyOperationParameter[] body = new BodyOperationParameter[3];
        //    body[0] = new BodyOperationParameter("UserName", mCompanyConn.UserName);
        //    body[1] = new BodyOperationParameter("Password", mCompanyConn.Password);
        //    body[2] = new BodyOperationParameter("CompanyDB", mCompanyConn.CompanyDB);

        //    //Both HTTP & HTTPs protocols are supported.
        //    session = (B1Session)SLayer.Execute<B1Session>(login, "POST", true, body).SingleOrDefault();
        //    if (null != session)
        //    {
        //        strCurrentSessionGUID = session.SessionId;
        //    }

        //    mCompanyConn.SLSessionId = session.SessionId;
        //    mCompanyConn.SLRouteId = strCurrentRouteIDString;

        //    return mCompanyConn;

        //}

        public ServiceLayer GetServiceLayer()
        {
            Uri service = new Uri(ConfigurationManager.AppSettings["WebB1s"]);
            SLayer = new ServiceLayer(service);

            SLayer.Format.UseJson();
            SLayer.Format.UseJson();

            SLayer.IgnoreMissingProperties = true;

            return SLayer;

        }


        public void SLayer_SendingRequest(object sender, System.Data.Services.Client.SendingRequestEventArgs e)
        {
            //throw new NotImplementedException();
            HttpWebRequest request = (HttpWebRequest)e.Request;
            if (null != request)
            {
                request.Accept = "application/json;odata=minimalmetadata";
                request.KeepAlive = true;                               //keep alive
                request.ServicePoint.Expect100Continue = false;        //content
                request.AllowAutoRedirect = true;
                request.ContentType = "application/json;odata=minimalmetadata;charset=utf8";
                request.Timeout = 10000000;    //number of seconds before considering a request as timeout (consider to change it for batch operations)

                //This way works to bring additional information with request headers
                if (false == string.IsNullOrEmpty(strCurrentSessionGUID))
                {
                    string strB1Session = "B1SESSION=" + strCurrentSessionGUID;
                    if (!string.IsNullOrEmpty(strCurrentRouteIDString))
                        strB1Session += "; " + strCurrentRouteIDString;

                    e.RequestHeaders.Add("Cookie", strB1Session);
                }

                //Only works for get requests, but we can always use this, even it will be ignored by other request types.
                e.RequestHeaders.Add("Prefer", "odata.maxpagesize=" + currentDefaultPagingSizing.ToString());


                //For GUI, non-functional
                BuildRequestStringContent(request);
            }
            else
                throw new Exception("Failed to intercept the sending request");
        }

        private void BuildRequestStringContent(HttpWebRequest request)
        {
            sbHttpRequestHeaders.Clear();

            if (null != request)
            {
                sbHttpRequestHeaders.Append(CreateHeaderItem("Method", request.Method.ToString()));
                //sbHttpRequestHeaders.Append(CreateHeaderItem("ServerURI", request.ServicePoint.Address.AbsoluteUri.ToString()));
                //sbHttpRequestHeaders.Append(CreateHeaderItem("Address", request.Address.ToString()));
                sbHttpRequestHeaders.Append(CreateHeaderItem("RequestUri", request.RequestUri.ToString()));
                sbHttpRequestHeaders.Append(CreateHeaderItem("Accept", request.Accept));
                sbHttpRequestHeaders.Append(CreateHeaderItem("Keep-Alive", request.KeepAlive.ToString()));
                sbHttpRequestHeaders.Append(CreateHeaderItem("ContentType", request.ContentType));
                sbHttpRequestHeaders.Append(CreateHeaderItem("ContentLength", request.ContentLength.ToString()));
                sbHttpRequestHeaders.Append(CreateHeaderItem("Connection", request.Connection));
                sbHttpRequestHeaders.Append(CreateHeaderItem("UserAgent", request.UserAgent));
                sbHttpRequestHeaders.Append(CreateHeaderItem("Timeout", request.Timeout.ToString()));
                sbHttpRequestHeaders.Append(CreateHeaderItem("ProtocolVersion", request.ProtocolVersion.ToString()));

                sbHttpRequestHeaders.Append(CreateHeaderItem("Cookie", request.Headers["Cookie"]));
                sbHttpRequestHeaders.Append(CreateHeaderItem("Prefer", request.Headers["Prefer"]));

                sbHttpRequestHeaders.Append("\n\n");
            }
        }

        private string CreateHeaderItem(string strName, string strValue)
        {
            string strFormat = "{0} : {1}\n";
            return string.Format(strFormat, strName, strValue);
        }

        public void SLayer_ReceivingResponse(object sender, ReceivingResponseEventArgs e)
        {
            if (null == e.ResponseMessage)
                return;

            string strMessage = e.ResponseMessage.GetHeader("Set-Cookie");

            if (false == string.IsNullOrEmpty(strMessage))
            {
                int idx = strMessage.IndexOf("ROUTEID=");
                if (idx > 0)
                {
                    string strSubString = strMessage.Substring(idx);
                    int idxSplitter = strSubString.IndexOf(";");
                    if (idxSplitter > 0)
                    {
                        strCurrentRouteIDString = strSubString.Substring(0, idxSplitter);
                    }
                    else
                    {
                        strCurrentRouteIDString = string.Empty;
                    }
                }
            }

            // Just for login
            BuildResponseStringContent(e.ResponseMessage);
        }

        private void BuildResponseStringContent(IODataResponseMessage response)
        {
            sbHttpResponseHeaders.Clear();

            if (null != response)
            {
                sbHttpResponseHeaders.Append(CreateHeaderItem("StatusCode", response.StatusCode.ToString()));
                if (response.GetHeader("DataServiceVersion") != "")
                    sbHttpResponseHeaders.Append(CreateHeaderItem("DataServiceVersion", response.GetHeader("DataServiceVersion")));
                if (response.GetHeader("Date") != "")
                    sbHttpResponseHeaders.Append(CreateHeaderItem("Date", response.GetHeader("Date")));
                sbHttpResponseHeaders.Append("\n\n");
            }
        }

        private static bool RemoteSSLTLSCertificateValidate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors ssl)
        {
            //accept
            return true;
        }
    }
}