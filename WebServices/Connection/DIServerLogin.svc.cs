﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using System.Xml;
using WebServices.Model;
using WebServices.SLService.SAPB1;
using WebServices.Model.Tables;
using AutoMapper;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Interfaces.SAP;

namespace WebServices.Connection
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DIServerLogin" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DIServerLogin.svc or DIServerLogin.svc.cs at the Solution Explorer and start debugging.
    public class DIServerLogin : IDIServerLogin
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pParam"></param>
        /// <returns></returns>
        public CompanyConn Login(CompanyConn pParam)
        {
            string ret = string.Empty;

            if (pParam.ServerType == (int)Enums.eServerType.dst_HANA)
            {
                pParam.DSSessionId = SLManager.login(pParam);
            }
            else
            {
                pParam.DSSessionId = GetDIServerConnection(pParam);
            }
            //if (pParam.ServerType == (int)Enums.eServerType.dst_HANA)
            //{
            //    mServiceLayerCon = new ServiceLayerCon();
            //    pParam = mServiceLayerCon.Login(pParam);
            //}

            return pParam;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pType"></param>
        /// <returns></returns>
        public string GetServerType(int pType)
        {
            string ret = string.Empty;
            switch (pType)
            {
                case 1:
                    //.- SQL 2005 -.\\
                    ret = "dst_MSSQL";
                    break;
                case 4:
                    //.- SQL 2005 -.\\
                    ret = "dst_MSSQL2005";
                    break;
                case 6:
                    //.- SQL 2008 -.\\
                    ret = "dst_MSSQL2008";
                    break;
                case 7:
                    //.- SQL 2012 -.\\
                    ret = "dst_MSSQL2012";
                    break;
                case 8:
                    //.- SQL 2014 -.\\
                    ret = "dst_MSSQL2014";
                    break;
                case 9:
                    //.- HANA -.\\
                    ret = "dst_HANADB";
                    break;
                case 10:
                    //.- SQL 2016 -.\\
                    ret = "dst_MSSQL2016";
                    break;
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pParam"></param>
        /// <returns></returns>
        private string GetDIServerConnection(CompanyConn pParam)
        {
            SBODI_Server.Node DISnode = null;
            string sSOAPans = null, sCmd = null;

            try
            {
                DISnode = new SBODI_Server.Node();

                sCmd = @"<?xml version=""1.0"" encoding=""UTF-16""?>";
                sCmd += @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">";
                sCmd += @"<env:Body><dis:Login xmlns:dis=""http://www.sap.com/SBO/DIS"">";
                sCmd += "<DatabaseServer>" + pParam.Server + "</DatabaseServer>";
                sCmd += "<DatabaseName>" + pParam.CompanyDB + "</DatabaseName>";
                sCmd += "<DatabaseType>" + GetServerType(pParam.ServerType) + "</DatabaseType>";
                sCmd += "<DatabaseUsername>" + pParam.DbUserName + "</DatabaseUsername>";
                sCmd += "<DatabasePassword>" + pParam.DbPassword + "</DatabasePassword>";
                sCmd += "<CompanyUsername>" + pParam.UserName + "</CompanyUsername>";
                sCmd += "<CompanyPassword>" + pParam.Password + "</CompanyPassword>";
                sCmd += "<Language>" + "ln_English" + "</Language>";
                sCmd += "<LicenseServer>" + pParam.LicenseServer + ":" + pParam.PortNumber + "</LicenseServer>";
                sCmd += "</dis:Login></env:Body></env:Envelope>";

                sSOAPans = DISnode.Interact(sCmd);

                //  Parse the SOAP answer
                System.Xml.XmlValidatingReader xmlValid = null;
                string sRet = null;
                xmlValid = new System.Xml.XmlValidatingReader(sSOAPans, System.Xml.XmlNodeType.Document, null);
                while (xmlValid.Read())
                {
                    if (xmlValid.NodeType == System.Xml.XmlNodeType.Text)
                    {
                        if (sRet == null)
                        {
                            sRet += xmlValid.Value;
                        }
                        else
                        {
                            if (sRet.StartsWith("Error"))
                            {
                                sRet += " " + xmlValid.Value;
                            }
                            else
                            {
                                sRet = "Error " + sRet + " " + xmlValid.Value;
                            }
                        }
                    }
                }
                if (Strings.InStr(sSOAPans, "<env:Fault>", Microsoft.VisualBasic.CompareMethod.Text) != 0)
                {
                    sRet = "Error: " + sRet;
                }

                return sRet;
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
            finally
            {
                Utils.ReleaseObject((object)DISnode);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSession"></param>
        /// <returns></returns>
        public string Logout(string pSession)
        {
            string mXmlResult = string.Empty;
            string ret = string.Empty;
            string AddCmd = null;
            SBODI_Server.Node mServerNode = null;
            XmlDocument mResultXML = null;

            try
            {
                mResultXML = new XmlDocument();
                mServerNode = new SBODI_Server.Node();

                //Armo la cabecera del XML
                AddCmd = @"<?xml version=""1.0"" ?>" +
                     @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                     "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                     @"<env:Body><dis:Logout xmlns:dis=""http://www.sap.com/SBO/DIS""></dis:Logout></env:Body></env:Envelope>";

                //Envio Solicitud al DI Server y Obtengo la Respuesta
                mXmlResult = mServerNode.Interact(AddCmd);
                mResultXML.LoadXml(mXmlResult);

                //Recupero los Valores de la Respuesta            
                return UtilWeb.GetResult(mResultXML);

            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
            finally
            {
                Utils.ReleaseObject((object)mServerNode);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public bool CheckApparelInstallation(CompanyConn pCompanyParam)
        {
            try
            {
                if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
                {
                    return CheckApparelInstallationSL(pCompanyParam);
                }
                else
                {
                    return CheckApparelInstallationEF(pCompanyParam);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("DIServerLogin -> CheckApparelInstallation :" + ex.Message);
                return false;
            }
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="pCompany"></param>
        /// <returns></returns>
        public CompanySAPConfig GetCompanySAPConfig(CompanyConn pCompany)
        {
            try
            {
                if (pCompany.ServerType == (int)Enums.eServerType.dst_HANA)
                {
                    return GetCompanySAPConfigSL(pCompany);
                }
                else
                {
                    return GetCompanySAPConfigEF(pCompany);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("DIServerLogin -> GetCompanySAPConfig :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        private bool CheckApparelInstallationSL(CompanyConn pCompanyParam)
        {
            try
            {
                string mUrl = string.Empty;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Color");
                ARGNS_COLOR mBP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNS_COLOR>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DIServerLogin -> CheckApparelInstallationSL :" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pParam"></param>
        /// <returns></returns>
        private bool CheckApparelInstallationEF(CompanyConn pParam)
        {
            DataBase mDbContext = new DataBase();
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pParam);
                mDbContext.Database.Connection.Open();

                mDbContext.C_ARGNS_COLOR.Take(1).ToList();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DIServerLogin -> CheckApparelInstallationEF :" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        private CompanySAPConfig GetCompanySAPConfigSL(CompanyConn pCompanyParam)
        {
            CompanySAPConfig mCompanySAPConfig = new CompanySAPConfig();

            try
            {
                string mUrl = string.Empty;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Color");
                ARGNS_COLOR mBP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNS_COLOR>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                mCompanySAPConfig.CheckApparelInstallation = true;
            }
            catch (Exception ex)
            {
                mCompanySAPConfig.CheckApparelInstallation = false;
            }

            try
            {
                string mUrl = string.Empty;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentSettings");
                DocumentSettingsSAP mDocumentSettingsSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentSettingsSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OADM");
                CompanySettingsSAP mCompanySettingsSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CompanySettingsSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                mCompanySAPConfig.DocumentSettingsSAP = mDocumentSettingsSAP;
                mCompanySAPConfig.CompanySettingsSAP = mCompanySettingsSAP;

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OBPL", "Disabled eq 'N'");
                var OBPL = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OBPL>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "USR6", "UserCode eq '" + pCompanyParam.UserName + "'");
                var USR6 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<USR6>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                mCompanySAPConfig.Branches = (from c in USR6 join j in OBPL on c.BPLId equals j.BPLId where c.UserCode == pCompanyParam.UserName && j.Disabled == "N" select new BranchesSAP { BPLId = j.BPLId, BPLName = j.BPLName }).ToList();

                return mCompanySAPConfig;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CompanySAPConfig GetCompanySAPConfigEF(CompanyConn pParam)
        {
            DataBase mDbContext = new DataBase();
            CompanySAPConfig mCompanySAPConfig = new CompanySAPConfig();

            try
            {

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pParam);
                mDbContext.Database.Connection.Open();

                mDbContext.C_ARGNS_COLOR.Take(1).ToList();
                mCompanySAPConfig.CheckApparelInstallation = true;
            }
            catch (Exception ex)
            {
                mCompanySAPConfig.CheckApparelInstallation = false;
            }

            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<CINF, DocumentSettingsSAP>(); cfg.CreateMap<OADM, CompanySettingsSAP>(); }).CreateMapper();

                mCompanySAPConfig.DocumentSettingsSAP = mapper.Map<DocumentSettingsSAP>(mDbContext.CINF.FirstOrDefault());
                mCompanySAPConfig.CompanySettingsSAP = mapper.Map<CompanySettingsSAP>(mDbContext.OADM.FirstOrDefault());
                mCompanySAPConfig.Branches = (from c in mDbContext.USR6 join j in mDbContext.OBPL on c.BPLId equals j.BPLId where c.UserCode == pParam.UserName && j.Disabled == "N" select new BranchesSAP { BPLId = j.BPLId, BPLName = j.BPLName }).ToList();


                return mCompanySAPConfig;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
