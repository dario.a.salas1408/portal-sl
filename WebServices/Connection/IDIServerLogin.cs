﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ARGNS.Model.Implementations;

namespace WebServices.Connection
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDIServerLogin" in both code and config file together.
    [ServiceContract]
    public interface IDIServerLogin
    {
        [OperationContract]
        CompanyConn Login(CompanyConn pParam);

        [OperationContract]
        string Logout(string pSession);

        [OperationContract]
        bool CheckApparelInstallation(CompanyConn pParam);

        [OperationContract]
        CompanySAPConfig GetCompanySAPConfig(CompanyConn pCompany);
    }
}
