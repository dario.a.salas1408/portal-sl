﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations;
using WebServices.Model.Tables;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Util;
using System.IO;
using System.Web.Script.Serialization;
using AutoMapper;
using System.Xml;
using WebServices.User;
using System.Collections;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.PDM.MaterialDetails;
using System.Xml.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace WebServices.MasterData.General
{
    public class MasterDataServiceSL : IMasterDataService
    {
        private B1WSHandler mB1WSHandler;

        public MasterDataServiceSL()
        {
            mB1WSHandler = new B1WSHandler();
        }

        #region  Common Method
        public List<ARGNSSegmentation> GetSegmentationList(CompanyConn pCompanyParam)
        {
            List<ARGNSSegmentation> mRet = null;
            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Segmentation");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSegmentation>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSegmentationList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public ARGNSSegmentation GetSegmentationById(CompanyConn pCompanyParam, string pCode)
        {
            ARGNSSegmentation mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "Code eq '" + pCode + "' and U_Active eq 'Y'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Segmentation", mFilters);
                List<ARGNSSegmentation> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSegmentation>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRetList.SingleOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSegmentationById :" + ex.Message);
                mRet = null;
            }
            return mRet;
        }

        public List<ARGNSModelFile> GetModelFileList(CompanyConn pCompanyParam, string pCode, string pU_ModCode)
        {
            List<ARGNSModelFile> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "' ";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelFile", mFilters);

                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelFile>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRet.Select(c => { c.U_ModCode = pU_ModCode; return c; }).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelFileList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSCrPath> GetModelProjectList(CompanyConn pCompanyParam, string pModCode)
        {
            List<ARGNSCrPath> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "U_Model eq '" + HttpUtility.UrlEncode(pModCode) + "' ";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CRPath", mFilters);

                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCrPath>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelProjectList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        #endregion

        #region Product Data

        public List<ARGNSModel> GetPDMListSearch(CompanyConn pCompanyParam, ref PDMSAPCombo combo,
            string pCodeModel = "", string pNameModel = "",
            string pSeasonModel = "", string pGroupModel = "",
            string pBrand = "", string pCollection = "",
            string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            List<ARGNSModel> mListModelReturn = null;
            string mUrl = string.Empty;

            try
            {
                string mFilters = null;

                string[] mSessonCodeArr = null, mModelGroupCodeArr = null, mBrandCodeArr = null, mCollCodeArr = null, mSubCollCodeArr = null;

                if (pSeasonModel != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSeasonModel.Trim()) + "'), tolower(Name)) ";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season", mFilters);
                    List<ARGNSSeason> mSeasonList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mSessonCodeArr = mSeasonList.Select(c => c.Code).ToArray();


                }
                if (pGroupModel != "")
                {
                    mFilters = "substringof(tolower('" + pGroupModel.Trim() + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp", mFilters);
                    List<ARGNSModelGroup> mGroupModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mModelGroupCodeArr = mGroupModelList.Select(c => c.Code).ToArray();
                }
                if (pBrand != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pBrand.Trim()) + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand", mFilters);
                    List<ARGNSBrand> mBrandList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mBrandCodeArr = mBrandList.Select(c => c.Code).ToArray();
                }
                if (pCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCollection.Trim()) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection", mFilters);
                    List<ARGNSCollection> mCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mCollCodeArr = mCollList.Select(c => c.U_CollCode).ToArray();
                }
                if (pSubCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSubCollection.Trim()) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient", mFilters);
                    List<ARGNSSubCollection> mSubCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mSubCollCodeArr = mSubCollList.Select(c => c.U_AmbCode).ToArray();
                }

                mFilters = "1 eq 1 ";

                if (!string.IsNullOrEmpty(pCodeModel))
                {
                    mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pCodeModel.Trim()) + "'), tolower(U_ModCode)) ";
                }

                if (!string.IsNullOrEmpty(pNameModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pNameModel.Trim()) + "'), tolower(U_ModDesc))";
                }
                if (!string.IsNullOrEmpty(pSeasonModel))
                {

                    string mInternaFilter = string.Empty;

                    foreach (string item in mSessonCodeArr)
                    {
                        if (string.IsNullOrEmpty(mInternaFilter))
                        {
                            mInternaFilter = "U_Season eq '" + item + "'";
                        }
                        else
                        {
                            mInternaFilter = mInternaFilter + " or U_Season eq '" + item + "'";
                        }

                    }

                    if (!string.IsNullOrEmpty(mInternaFilter))
                    {
                        mFilters = mFilters + " and (" + mInternaFilter + ")";
                    }
                    else
                    {
                        mFilters = mFilters + " and 1 eq 0 ";
                    }

                }
                if (!string.IsNullOrEmpty(pGroupModel))
                {

                    string mInternaFilter = string.Empty;

                    foreach (string item in mModelGroupCodeArr)
                    {
                        if (string.IsNullOrEmpty(mInternaFilter))
                        {
                            mInternaFilter = "U_ModGrp eq '" + item + "'";
                        }
                        else
                        {
                            mInternaFilter = mInternaFilter + " or U_ModGrp eq '" + item + "'";
                        }

                    }

                    if (!string.IsNullOrEmpty(mInternaFilter))
                    {
                        mFilters = mFilters + " and (" + mInternaFilter + ")";
                    }
                    else
                    {
                        mFilters = mFilters + " and 1 eq 0 ";
                    }

                }

                if (!string.IsNullOrEmpty(pBrand))
                {
                    string mInternaFilter = string.Empty;

                    foreach (string item in mBrandCodeArr)
                    {
                        if (string.IsNullOrEmpty(mInternaFilter))
                        {
                            mInternaFilter = "U_Brand eq '" + item + "'";
                        }
                        else
                        {
                            mInternaFilter = mInternaFilter + " or U_Brand eq '" + item + "'";
                        }

                    }

                    if (!string.IsNullOrEmpty(mInternaFilter))
                    {
                        mFilters = mFilters + " and (" + mInternaFilter + ")";
                    }
                    else
                    {
                        mFilters = mFilters + " and 1 eq 0 ";
                    }

                }
                if (!string.IsNullOrEmpty(pCollection))
                {
                    string mInternaFilter = string.Empty;

                    foreach (string item in mCollCodeArr)
                    {
                        if (string.IsNullOrEmpty(mInternaFilter))
                        {
                            mInternaFilter = "U_CollCode eq '" + item + "'";
                        }
                        else
                        {
                            mInternaFilter = mInternaFilter + " or U_CollCode eq '" + item + "'";
                        }

                    }

                    if (!string.IsNullOrEmpty(mInternaFilter))
                    {
                        mFilters = mFilters + " and (" + mInternaFilter + ")";
                    }
                    else
                    {
                        mFilters = mFilters + " and 1 eq 0 ";
                    }
                }

                if (!string.IsNullOrEmpty(pSubCollection))
                {
                    string mInternaFilter = string.Empty;

                    foreach (string item in mSubCollCodeArr)
                    {
                        if (string.IsNullOrEmpty(mInternaFilter))
                        {
                            mInternaFilter = "U_AmbCode eq '" + item + "'";
                        }
                        else
                        {
                            mInternaFilter = mInternaFilter + " or U_AmbCode eq '" + item + "'";
                        }

                    }

                    if (!string.IsNullOrEmpty(mInternaFilter))
                    {
                        mFilters = mFilters + " and (" + mInternaFilter + ")";
                    }
                    else
                    {
                        mFilters = mFilters + " and 1 eq 0 ";
                    }

                }
                mFilters = mFilters + "and U_Status ne '-1'";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Lleno el combo con los datos
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                combo.ListBrand = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                combo.ListModelGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                combo.ListSeason = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                combo.ListCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                combo.ListSubCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return Mapper.Map<List<ARGNSModel>>(mListModelReturn).OrderBy(o => o.U_ModCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPDMListSearch :" + ex.Message);
                Logger.WriteError("MasterDataServiceSL -> GetPDMListSearch :" + ex.InnerException);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bp"></param>
        /// <returns></returns>
        public string ChangeUTFCharacter(string bp)
        {
            bp = bp.Replace("Ñ", "%")
                .Replace("ñ", "%")
                .Replace("ã", "%")
                .Replace("ç", "%")
                .Trim();

            Regex regex = new Regex(@"[^a-zA-Z0-9\s]", (RegexOptions)0);
            return regex.Replace(bp, "%");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <param name="pCatalogCode"></param>
        /// <returns></returns>
        public JsonObjectResult GetPDMListDesc(CompanyConn pCompanyParam,
            int pStart, int pLength,
            string pCodeModel = "", string pNameModel = "",
            string pSeasonModel = "", string pGroupModel = "",
            string pBrand = "", string pCollection = "",
            string pSubCollection = "", string pCatalogCode = "")
        {
            string mUrl = string.Empty;
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                string mSeasonCode = null, mModelGroupCode = null, mBrandCode = null, mCollCode = null, mSubCollCode = null, mFilters = null;
                List<ARGNSSeason> mSeasonList = null;
                List<ARGNSModelGroup> mGroupModelList = null;
                List<ARGNSBrand> mBrandList = null;
                List<ARGNSCollection> mCollList = null;
                List<ARGNSSubCollection> mSubCollList = null;
                List<C_ARGNS_FBOOKD> mSubCatalogList = null;
                string mPagination = string.Format("&$top={0}&$skip={1}", pLength, pStart);
                int mTotalModelsPerCatalog = 0;

                if (pSeasonModel != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSeasonModel) + "'), tolower(Name)) ";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season", mFilters);
                    mSeasonList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSeasonCode = ((mSeasonList == null || mSeasonList.Count == 0) ? null : mSeasonList.FirstOrDefault().Code);
                }

                if (pGroupModel != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pGroupModel) + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp", mFilters);
                    mGroupModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mModelGroupCode = ((mGroupModelList == null || mGroupModelList.Count == 0) ? null : mGroupModelList.FirstOrDefault().Code);
                }

                if (pBrand != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pBrand) + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand", mFilters);
                    mBrandList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mBrandCode = ((mBrandList == null || mBrandList.Count == 0) ? null : mBrandList.FirstOrDefault().Code);
                }

                if (pCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCollection) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection", mFilters);
                    mCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mCollCode = ((mCollList == null || mCollList.Count == 0) ? null : mCollList.FirstOrDefault().U_CollCode);
                }

                if (pSubCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSubCollection.ToUpper()) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient", mFilters);
                    mSubCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSubCollCode = ((mSubCollList == null || mSubCollList.Count == 0) ? null : mSubCollList.FirstOrDefault().U_AmbCode);
                }

                if (pCatalogCode != "")
                {
                    mFilters = " Code eq '" + pCatalogCode + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_FBOOKD", mFilters + mPagination);
                    mSubCatalogList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<C_ARGNS_FBOOKD>>(
                        RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET,
                        pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mUrl = pCompanyParam.UrlHana + "ARGNS_FBOOKD" + "/$count/?$filter=" + mFilters;

                    mTotalModelsPerCatalog = RESTService.GetRequestJsonCount(mUrl,
                                    WebServices.UtilWeb.MethodType.GET,
                                    pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                }

                mFilters = "1 eq 1 ";

                if (!string.IsNullOrEmpty(pCodeModel))
                {
                    mFilters = mFilters + " and substringof(tolower('" + HttpUtility.UrlEncode(pCodeModel.Trim()) + "'), tolower(U_ModCode))";
                }

                if (!string.IsNullOrEmpty(pNameModel))
                {
                    if (!string.IsNullOrEmpty(pCodeModel))
                    {
                        mFilters = mFilters + "or substringof(tolower('" + HttpUtility.UrlEncode(pNameModel.Trim()) + "'), tolower(U_ModDesc))";
                    }
                    else
                    {
                        mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pNameModel.Trim()) + "'), tolower(U_ModDesc))";
                    }
                }

                if (!string.IsNullOrEmpty(pSeasonModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + ((mSeasonCode == null) ? HttpUtility.UrlEncode(pSeasonModel) : HttpUtility.UrlEncode(mSeasonCode)) + "'), tolower(U_Season))";
                }

                if (!string.IsNullOrEmpty(pGroupModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(((pGroupModel == null) ? mModelGroupCode : pGroupModel)) + "'), tolower(U_ModGrp))";
                }

                if (!string.IsNullOrEmpty(pBrand))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(((pBrand == null) ? mBrandCode : pBrand)) + "'), tolower(U_Brand))";
                }

                if (!string.IsNullOrEmpty(pCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(((pCollection == null) ? mCollCode : pCollection)) + "'), tolower(U_CollCode))";
                }

                if (!string.IsNullOrEmpty(pSubCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(((pSubCollection == null) ? mSubCollCode : pSubCollection)) + "'), tolower(U_AmbCode))";
                }



                //Lista de Todos los Combos del Modelo
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                mBrandList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                mGroupModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mSeasonList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                mSubCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                if (pCatalogCode != "")
                {
                    mFilters = mFilters + "and U_Status ne '-1' and (";

                    foreach (var item in mSubCatalogList)
                    {
                        if (item != mSubCatalogList.First())
                        {
                            mFilters = mFilters + " or "; 
                        }
                        mFilters = mFilters + string.Format(" U_ModCode eq '{0}' ",  item.U_ModCode);
                    }
                    mFilters = mFilters +  ")"; 


                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);

                    List<ARGNSModel> returnList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>
                        (RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET,
                        pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mJsonObjectResult.ModelDescViewList = (from mModel in returnList
                                                           join mCatalog in mSubCatalogList on mModel.U_ModCode equals mCatalog.U_ModCode
                                                           select new ModelDesc()
                                                           {
                                                               Code = mModel.Code,
                                                               ModelCode = mModel.U_ModCode,
                                                               ModelImage = mModel.U_Pic,
                                                               Modelprice = mModel.U_Price ?? 0,
                                                               ModDescription = mModel.U_ModDesc,
                                                               SeasonDesc = !string.IsNullOrEmpty(mModel.U_Season) ? (mSeasonList.Where(c => c.Code == mModel.U_Season).FirstOrDefault() != null ? mSeasonList.Where(c => c.Code == mModel.U_Season).FirstOrDefault().Name : null) : null,
                                                               CollDesc = !string.IsNullOrEmpty(mModel.U_CollCode) ? (mCollList.Where(c => c.U_CollCode == mModel.U_CollCode).FirstOrDefault() != null ? mCollList.Where(c => c.U_CollCode == mModel.U_CollCode).FirstOrDefault().U_Desc : null) : null,
                                                               SubCollDesc = !string.IsNullOrEmpty(mModel.U_AmbCode) ? (mSubCollList.Where(c => c.U_AmbCode == mModel.U_AmbCode).FirstOrDefault() != null ? mSubCollList.Where(c => c.U_AmbCode == mModel.U_AmbCode).FirstOrDefault().U_Desc : null) : null,
                                                               BrandDesc = !string.IsNullOrEmpty(mModel.U_Brand) ? (mBrandList.Where(c => c.Code == mModel.U_Brand).FirstOrDefault() != null ? mBrandList.Where(c => c.Code == mModel.U_Brand).FirstOrDefault().Name : null) : null,
                                                               ProdGroupDesc = !string.IsNullOrEmpty(mModel.U_ModGrp) ? (mGroupModelList.Where(c => c.Code == mModel.U_ModGrp).FirstOrDefault() != null ? mGroupModelList.Where(c => c.Code == mModel.U_ModGrp).FirstOrDefault().Name : null) : null,
                                                               CatalogCode = pCatalogCode
                                                           })
                                                           .ToList();

                    mJsonObjectResult.Others.Add("TotalRecords", mTotalModelsPerCatalog.ToString());

                }
                else
                {
                    mFilters = mFilters + "and U_Status ne '-1'";


                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters + mPagination);

                    List<ARGNSModel> returnList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>
                        (RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET,
                        pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mUrl = pCompanyParam.UrlHana + "Model" + "/$count/?$filter=" + mFilters;

                    int mCounter = RESTService.GetRequestJsonCount(mUrl,
                                    WebServices.UtilWeb.MethodType.GET,
                                    pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                    mJsonObjectResult.Others.Add("TotalRecords", mCounter.ToString());

                    mJsonObjectResult.ModelDescViewList = (from mModel in returnList
                                                           select new ModelDesc()
                                                           {
                                                               Code = mModel.Code,
                                                               ModelCode = mModel.U_ModCode,
                                                               ModelImage = mModel.U_Pic,
                                                               Modelprice = mModel.U_Price ?? 0,
                                                               ModDescription = mModel.U_ModDesc,
                                                               SeasonDesc = !string.IsNullOrEmpty(mModel.U_Season) ? (mSeasonList.Where(c => c.Code == mModel.U_Season).FirstOrDefault() != null ? mSeasonList.Where(c => c.Code == mModel.U_Season).FirstOrDefault().Name : null) : null,
                                                               CollDesc = !string.IsNullOrEmpty(mModel.U_CollCode) ? (mCollList.Where(c => c.U_CollCode == mModel.U_CollCode).FirstOrDefault() != null ? mCollList.Where(c => c.U_CollCode == mModel.U_CollCode).FirstOrDefault().U_Desc : null) : null,
                                                               SubCollDesc = !string.IsNullOrEmpty(mModel.U_AmbCode) ? (mSubCollList.Where(c => c.U_AmbCode == mModel.U_AmbCode).FirstOrDefault() != null ? mSubCollList.Where(c => c.U_AmbCode == mModel.U_AmbCode).FirstOrDefault().U_Desc : null) : null,
                                                               BrandDesc = !string.IsNullOrEmpty(mModel.U_Brand) ? (mBrandList.Where(c => c.Code == mModel.U_Brand).FirstOrDefault() != null ? mBrandList.Where(c => c.Code == mModel.U_Brand).FirstOrDefault().Name : null) : null,
                                                               ProdGroupDesc = !string.IsNullOrEmpty(mModel.U_ModGrp) ? (mGroupModelList.Where(c => c.Code == mModel.U_ModGrp).FirstOrDefault() != null ? mGroupModelList.Where(c => c.Code == mModel.U_ModGrp).FirstOrDefault().Name : null) : null,
                                                               CatalogCode = pCatalogCode
                                                           })
                                                           .ToList();
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceSL -> GetPDMListDesc :" + ex.Message);
                return new JsonObjectResult();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pModCode"></param>
        /// <returns></returns>
        public ModelDesc GetModelDesc(CompanyConn pCompanyParam, string pModCode)
        {
            List<ModelDesc> ret = null;
            List<ARGNSModel> mListModelReturn = null;
            string mUrl = string.Empty;
            try
            {

                string mFilters = null;
                List<ARGNSSeason> mSeasonList = null;
                List<ARGNSModelGroup> mGroupModelList = null;
                List<ARGNSBrand> mBrandList = null;
                List<ARGNSCollection> mCollList = null;
                List<ARGNSSubCollection> mSubCollList = null;

                mFilters = "1 eq 1 ";

                if (!string.IsNullOrEmpty(pModCode))
                {
                    mFilters = mFilters + " and U_ModCode eq '" + pModCode + "' ";
                }
                mFilters = mFilters + "and U_Status ne '-1'";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Lista de Todos los Combos del Modelo
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                mBrandList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                mGroupModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mSeasonList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                mSubCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                ret = (from mModel in mListModelReturn
                       select new ModelDesc()
                       {
                           Code = mModel.Code,
                           ModelCode = mModel.U_ModCode,
                           ModelImage = mModel.U_Pic,
                           Modelprice = mModel.U_Price ?? 0,
                           ModDescription = mModel.U_ModDesc,
                           SeasonDesc = !string.IsNullOrEmpty(mModel.U_Season) ? (mSeasonList.Where(c => c.Code == mModel.U_Season).FirstOrDefault() != null ? mSeasonList.Where(c => c.Code == mModel.U_Season).FirstOrDefault().Name : null) : null,
                           CollDesc = !string.IsNullOrEmpty(mModel.U_CollCode) ? (mCollList.Where(c => c.U_CollCode == mModel.U_CollCode).FirstOrDefault() != null ? mCollList.Where(c => c.U_CollCode == mModel.U_CollCode).FirstOrDefault().U_Desc : null) : null,
                           SubCollDesc = !string.IsNullOrEmpty(mModel.U_AmbCode) ? (mSubCollList.Where(c => c.U_AmbCode == mModel.U_AmbCode).FirstOrDefault() != null ? mSubCollList.Where(c => c.U_AmbCode == mModel.U_AmbCode).FirstOrDefault().U_Desc : null) : null,
                           BrandDesc = !string.IsNullOrEmpty(mModel.U_Brand) ? (mBrandList.Where(c => c.Code == mModel.U_Brand).FirstOrDefault() != null ? mBrandList.Where(c => c.Code == mModel.U_Brand).FirstOrDefault().Name : null) : null,
                           ProdGroupDesc = !string.IsNullOrEmpty(mModel.U_ModGrp) ? (mGroupModelList.Where(c => c.Code == mModel.U_ModGrp).FirstOrDefault() != null ? mGroupModelList.Where(c => c.Code == mModel.U_ModGrp).FirstOrDefault().Name : null) : null,
                       }).ToList();

                return ret.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelDesc :" + ex.Message);
                return null;
            }
        }

        public ARGNSModel GetModelById(CompanyConn pCompanyParam, string pCode, string pModCode, List<UDF_ARGNS> pListUDFModel = null)
        {
            ARGNSModel mModelReturn = null;
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
            string mUrl = string.Empty;
            string mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                List<ARGNSModel> modelListAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(mResultJson);
                mModelReturn = modelListAux.FirstOrDefault();

                if (mModelReturn != null)
                {
                    mFilters = "TableID eq '@ARGNS_MODEL'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
                    Newtonsoft.Json.Linq.JArray mJArrayOPOR = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mModelReturn.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFModel, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOPOR[0]);

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelSize", mFilters);
                    mModelReturn.ModelSizeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelSize>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelVar", mFilters);
                    mModelReturn.ModelVariableList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelVar>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelImg", mFilters);
                    mModelReturn.ModelImageList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelImg>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelFile", mFilters);
                    mModelReturn.ModelFileList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelFile>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ApparelSetup");
                    mModelReturn.administrationApparel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AdministrationApparel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OADM");
                    mModelReturn.administrationSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AdministrationSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelPom", mFilters);
                    mModelReturn.ModelPomList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelPom>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelBom", mFilters);
                    mModelReturn.ModelBomList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelBom>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelFitt", mFilters);
                    mModelReturn.ModelFittList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelFitt>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelInst", mFilters);
                    mModelReturn.ModelInstList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelInst>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelWFlow", mFilters);
                    mModelReturn.ModelWFLOWList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelWFLOW>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelLogos", mFilters);
                    mModelReturn.ModelLogosList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelLogos>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelCS", mFilters);
                    mModelReturn.ModelCSList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelCostSheet>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomizedModelColor", mFilters);
                    mModelReturn.ModelColorList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelColor>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mFilters = "U_ARGNS_MOD eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomizedStockModel", mFilters);
                    mModelReturn.ModelStockList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StockModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    if (mModelReturn.ModelSizeList.Count > 0)
                    {
                        mModelReturn.ModelStockList = mModelReturn.ModelStockList.Where(c => c.U_ARGNS_SIZE == c.U_SizeCode).ToList();
                    }
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Segmentation", "Code eq '" + mModelReturn.U_ATGrp + "' and U_Active eq 'Y'");
                    List<ARGNSSegmentation> mSegmentationList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSegmentation>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mModelReturn.ModelSegmentation = mSegmentationList.SingleOrDefault();
                }
                else
                {
                    mModelReturn = new ARGNSModel();
                }

                //Lista de Todos los Combos del Modelo
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                mPDMSAPCombo.ListBrand.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Div");
                mPDMSAPCombo.ListDivision.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSDivision>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OITB");
                mPDMSAPCombo.ListItemGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                mPDMSAPCombo.ListModelGroup.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ProdLine");
                mPDMSAPCombo.ListProductLine.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSProductLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mPDMSAPCombo.ListSeason.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "StyleStatus");
                mPDMSAPCombo.ListStatus.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSStyleStatus>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Year");
                mPDMSAPCombo.ListYear.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSYear>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PriceList");
                mPDMSAPCombo.PriceList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PriceListSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Segmentation");
                mPDMSAPCombo.ListSegmentation.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSegmentation>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mPDMSAPCombo.ListCollection.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                mPDMSAPCombo.ListSubCollection.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Composition");
                mPDMSAPCombo.CompositionList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSComposition>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Simbols");
                mPDMSAPCombo.SimbolList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSimbol>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP");
                mPDMSAPCombo.BusinessPartnerList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Currency");
                mPDMSAPCombo.CurrencyList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrencySAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                mPDMSAPCombo.DesignerList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mFilters = "CodeAux eq '1'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomizedDocNumber", mFilters);
                mPDMSAPCombo.DocNumbering = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSDocNumber>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mModelReturn.ListPDMSAPCombo = mPDMSAPCombo;

                return mModelReturn;

            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceSL -> GetModelById :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBPCode"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <returns></returns>
        public List<ARGNSModel> GetBPPDMListSearch(CompanyConn pCompanyParam, string pBPCode,
            ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "",
            string pNameModel = "", string pSeasonModel = "",
            string pGroupModel = "", string pBrand = "",
            string pCollection = "", string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            List<ARGNSModel> mListModelReturn = null;
            string mUrl = string.Empty;
            try
            {
                string mSeasonCode = null, mModelGroupCode = null, mBrandCode = null, mCollCode = null, mSubCollCode = null, mFilters = null;
                mFilters = "(U_CardType eq 'C' or U_CardType eq 'S') and U_CardCode eq '" + HttpUtility.UrlEncode(pBPCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelVendor", mFilters);
                List<C_ARGNS_MODVENDOR> mModVendorListAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<C_ARGNS_MODVENDOR>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                var mSubQuery = mModVendorListAux.Select(c => c.U_ModCode);
                if (pSeasonModel != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSeasonModel.ToUpper()) + "'), tolower(Name)) ";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season", mFilters);
                    List<ARGNSSeason> mSeasonList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSeasonCode = (mSeasonList == null ? null : mSeasonList.FirstOrDefault().Code);
                }
                if (pGroupModel != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pGroupModel.ToUpper()) + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp", mFilters);
                    List<ARGNSModelGroup> mGroupModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mModelGroupCode = (mGroupModelList == null ? null : mGroupModelList.FirstOrDefault().Code);
                }
                if (pBrand != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pBrand.ToUpper()) + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand", mFilters);
                    List<ARGNSBrand> mBrandList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mBrandCode = (mBrandList == null ? null : mBrandList.FirstOrDefault().Code);
                }
                if (pCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCollection.ToUpper()) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection", mFilters);
                    List<ARGNSCollection> mCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mCollCode = (mCollList == null ? null : mCollList.FirstOrDefault().U_CollCode);
                }
                if (pSubCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSubCollection.ToUpper()) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient", mFilters);
                    List<ARGNSSubCollection> mSubCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSubCollCode = (mSubCollList == null ? null : mSubCollList.FirstOrDefault().U_AmbCode);
                }

                mFilters = "1 eq 1 and substringof(tolower('" + HttpUtility.UrlEncode(pCodeModel) + "'), tolower(U_ModCode)) ";
                if (!string.IsNullOrEmpty(pNameModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pNameModel) + "'), tolower(U_ModDesc))";
                }
                if (!string.IsNullOrEmpty(pSeasonModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mSeasonCode) + "'), tolower(U_Season))";
                }
                if (!string.IsNullOrEmpty(pGroupModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mModelGroupCode) + "'), tolower(U_ModGrp))";
                }
                if (!string.IsNullOrEmpty(pBrand))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mBrandCode) + "'), tolower(U_Brand))";
                }
                if (!string.IsNullOrEmpty(pCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mCollCode) + "'), tolower(U_CollCode))";
                }
                if (!string.IsNullOrEmpty(pSubCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mSubCollCode) + "'), tolower(U_AmbCode))";
                }
                mFilters = mFilters + "and U_Status ne '-1'";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mListModelReturn = mListModelReturn.Where(c => c.U_Customer == pBPCode || mSubQuery.Contains(c.U_ModCode)).ToList();

                //Lleno el combo con los datos
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                mPDMSAPCombo.ListBrand = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                mPDMSAPCombo.ListModelGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mPDMSAPCombo.ListSeason = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mPDMSAPCombo.ListCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                mPDMSAPCombo.ListSubCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return Mapper.Map<List<ARGNSModel>>(mListModelReturn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetBPPDMListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserCode"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <returns></returns>
        public JsonObjectResult GetUserPDMListSearch(CompanyConn pCompanyParam,
            string pUserCode, ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "",
            string pNameModel = "", string pSeasonModel = "",
            string pGroupModel = "", string pBrand = "",
            string pCollection = "", string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            List<ARGNSModel> mListModelReturn = null;
            string mUrl = string.Empty;
            try
            {

                string mSeasonCode = null, mModelGroupCode = null, mBrandCode = null, mCollCode = null, mSubCollCode = null, mFilters = null;
                if (pSeasonModel != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSeasonModel.ToUpper()) + "'), tolower(Name)) ";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season", mFilters);
                    List<ARGNSSeason> mSeasonList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSeasonCode = (mSeasonList == null ? null : mSeasonList.FirstOrDefault().Code);
                }
                if (pGroupModel != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pGroupModel.ToUpper()) + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp", mFilters);
                    List<ARGNSModelGroup> mGroupModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mModelGroupCode = (mGroupModelList == null ? null : mGroupModelList.FirstOrDefault().Code);
                }
                if (pBrand != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pBrand.ToUpper()) + "'), tolower(Name))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand", mFilters);
                    List<ARGNSBrand> mBrandList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mBrandCode = (mBrandList == null ? null : mBrandList.FirstOrDefault().Code);
                }
                if (pCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCollection.ToUpper()) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection", mFilters);
                    List<ARGNSCollection> mCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mCollCode = (mCollList == null ? null : mCollList.FirstOrDefault().U_CollCode);
                }
                if (pSubCollection != "")
                {
                    mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSubCollection.ToUpper()) + "'), tolower(U_Desc))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient", mFilters);
                    List<ARGNSSubCollection> mSubCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSubCollCode = (mSubCollList == null ? null : mSubCollList.FirstOrDefault().U_AmbCode);
                }

                mFilters = "1 eq 1 and substringof(tolower('" + HttpUtility.UrlEncode(pCodeModel) + "'), tolower(U_ModCode)) ";
                if (!string.IsNullOrEmpty(pNameModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pNameModel) + "'), tolower(U_ModDesc))";
                }
                if (!string.IsNullOrEmpty(pSeasonModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mSeasonCode) + "'), tolower(U_Season))";
                }
                if (!string.IsNullOrEmpty(pGroupModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mModelGroupCode) + "'), tolower(U_ModGrp))";
                }
                if (!string.IsNullOrEmpty(pBrand))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mBrandCode) + "'), tolower(U_Brand))";
                }
                if (!string.IsNullOrEmpty(pCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mCollCode) + "'), tolower(U_CollCode))";
                }
                if (!string.IsNullOrEmpty(pSubCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mSubCollCode) + "'), tolower(U_AmbCode))";
                }
                mFilters = mFilters + "and U_Owner eq '" + HttpUtility.UrlEncode(pUserCode) + "' and U_Status ne '-1'";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Lleno el combo con los datos
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                mPDMSAPCombo.ListBrand = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                mPDMSAPCombo.ListModelGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mPDMSAPCombo.ListSeason = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mPDMSAPCombo.ListCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                mPDMSAPCombo.ListSubCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mJsonObjectResult.ARGNSModelList = Mapper.Map<List<ARGNSModel>>(mListModelReturn)
                    .OrderBy(o => o.U_ModCode)
                    .Skip(pStart)
                    .Take(pLength)
                    .ToList();

                int mTotalRecords = mListModelReturn.Count();
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetUserPDMListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateModel(ARGNSModel pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = (long)(pObject.U_PList.HasValue ? pObject.U_PList : 1);
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;

                #region Child

                //Sizes
                if (pObject.ModelSizeList.Count > 0)
                {
                    foreach (ARGNSModelSize item in pObject.ModelSizeList)
                    {

                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE mSizeModel = oArgnsModel.ARGNS_MODEL_SIZECollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mSizeModel.U_SizeCod = item.U_SizeCod;
                        mSizeModel.U_Selected = item.U_Selected;
                        mSizeModel.U_SclCode = item.U_SclCode;
                        if (item.U_VOrder != null)
                            mSizeModel.U_VOrder = (long)item.U_VOrder;
                    }

                }


                //Color
                if (pObject.ModelColorList.Count > 0)
                {

                    foreach (ARGNSModelColor item in pObject.ModelColorList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR mColorModel = oArgnsModel.ARGNS_MODEL_COLORCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mColorModel.U_ColCode = item.U_ColCode;
                        mColorModel.U_Active = item.U_Active;
                    }

                }


                //Variable
                if (pObject.ModelVariableList.Count > 0)
                {
                    foreach (ARGNSModelVar item in pObject.ModelVariableList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR mVarModel = oArgnsModel.ARGNS_MODEL_VARCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mVarModel.U_VarCode = item.U_VarCode;
                        mVarModel.U_Selected = item.U_Selected;
                        mVarModel.U_VarDesc = item.U_VarDesc;
                    }

                }

                //Image
                if (pObject.ModelImageList.Count > 0)
                {

                    oArgnsModel.ARGNS_MODEL_IMGCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG[pObject.ModelImageList.Count];
                    int counter = 0;
                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG mImageModelAdd = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG() { Code = item.Code, LineId = item.LineId, LineIdSpecified = true, U_File = item.U_File, U_FType = item.U_FType, U_Path = item.U_Path };
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter] = mImageModelAdd;
                        counter++;
                    }

                }

                //Attachment
                if (pObject.ModelFileList.Count > 0)
                {

                    oArgnsModel.ARGNS_MODEL_FILECollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FILE[pObject.ModelFileList.Count];
                    int counter = 0;
                    foreach (ARGNSModelFile item in pObject.ModelFileList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FILE mFileModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FILE();
                        mFileModel.U_File = item.U_File;
                        mFileModel.U_Path = item.U_Path;
                        mFileModel.U_FType = "FIL";
                        mFileModel.U_DesignTool = "";
                        oArgnsModel.ARGNS_MODEL_FILECollection[counter] = mFileModel;

                    }

                }

                //ModelInstruction
                if (pObject.ModelInstList.Count > 0)
                {
                    foreach (ARGNSModelInst item in pObject.ModelInstList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST mModelInst = oArgnsModel.ARGNS_MODEL_INSTCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mModelInst.U_Sector = item.U_Sector;
                        mModelInst.U_ModCode = item.U_ModCode;
                        mModelInst.U_InstCod = item.U_InstCod;
                        mModelInst.U_Instuct = item.U_Instuct;
                        mModelInst.U_Descrip = item.U_Descrip;
                        mModelInst.U_Imag = item.U_Imag;
                        mModelInst.U_Version = item.U_Version;
                        mModelInst.U_Active = item.U_Active;
                    }

                }

                //ModelFitt
                if (pObject.ModelFittList.Count > 0)
                {
                    foreach (ARGNSModelFitt item in pObject.ModelFittList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FITT mModelFitt = oArgnsModel.ARGNS_MODEL_FITTCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mModelFitt.U_ModCode = item.U_ModCode;
                        mModelFitt.U_FittCod = item.U_FittCod;
                        mModelFitt.U_Fitting = item.U_Fitting;
                    }
                }


                //ModelBom                
                if (pObject.ModelBomList.Count > 0)
                {
                    foreach (ARGNSModelBom item in pObject.ModelBomList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_BOM mModelBom = oArgnsModel.ARGNS_MODEL_BOMCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();

                        mModelBom.U_ItemCode = item.U_ItemCode;
                        mModelBom.U_Quantity = item.U_Quantity.HasValue ? Convert.ToDouble(item.U_Quantity.Value, System.Globalization.CultureInfo.InvariantCulture) : 0;
                        mModelBom.U_UOM = item.U_UOM;
                        mModelBom.U_Whse = item.U_Whse;
                        mModelBom.U_IssueMth = item.U_IssueMth;
                        mModelBom.U_PList = item.U_PList.HasValue ? (long)item.U_PList.Value : 0;
                        mModelBom.U_UPrice = item.U_UPrice.HasValue ? Convert.ToDouble(item.U_UPrice.Value, System.Globalization.CultureInfo.InvariantCulture) : 0;
                        mModelBom.U_Total = item.U_Total.HasValue ? Convert.ToDouble(item.U_Total.Value, System.Globalization.CultureInfo.InvariantCulture) : 0;
                        mModelBom.U_Comments = item.U_Comments;
                        mModelBom.U_TreeType = item.U_TreeType;
                        mModelBom.U_PVendor = item.U_PVendor;
                    }
                }


                //POM
                if (pObject.ModelPomList.Count > 0)
                {
                    foreach (ARGNSModelPom item in pObject.ModelPomList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODELPOM mPomModel = oArgnsModel.ARGNS_MODELPOMCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        if (item.U_Value != null)
                            mPomModel.U_Value = (double)item.U_Value;
                        if (item.U_TolPosit != null)
                            mPomModel.U_TolPosit = (double)item.U_TolPosit;
                        if (item.U_TolNeg != null)
                            mPomModel.U_TolNeg = (double)item.U_TolNeg;
                        mPomModel.U_QAPoint = !string.IsNullOrEmpty(item.U_QAPoint) ? item.U_QAPoint : "N";
                    }

                }
                #endregion

                XmlDocument mXmlDoc = Utils.GetSoapStructure(true, oArgnsModelMsgHeader, oArgnsModel);
                if (pObject.MappedUdf.Count > 0)
                {
                    mXmlDoc.SelectSingleNode("//*[local-name()='ArgnsModel']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='ArgnsModel']").InnerXml, pObject.MappedUdf);
                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult AddModel(ARGNSModel pObject, CompanyConn pCompanyParam)
        {
            int counter;
            string mUrl;
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;


            //Obtengo la Lista de Precio Por Defecto
            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ApparelSetup");
            AdministrationApparel mAdmApparel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AdministrationApparel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
            short mDefaultList = (mAdmApparel != null && mAdmApparel.U_BOMDSPL.HasValue) ? (short)mAdmApparel.U_BOMDSPL.Value : (short)1;

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                string mLastCode = GetLastModelCode(pCompanyParam);
                oArgnsModel.Code = mLastCode;
                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = (long)mDefaultList;
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;

                #region Child

                //Sizes
                if (pObject.ModelSizeList.Count > 0)
                {
                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE> mSizeList = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE mSizeModel;

                    foreach (ARGNSModelSize item in pObject.ModelSizeList)
                    {

                        mSizeModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE();
                        mSizeModel.U_SizeCod = item.U_SizeCod;
                        mSizeModel.U_Selected = item.U_Selected;
                        mSizeModel.U_SclCode = item.U_SclCode;
                        if (item.U_VOrder != null)
                            mSizeModel.U_VOrder = (long)item.U_VOrder;

                        mSizeList.Add(mSizeModel);
                    }

                    oArgnsModel.ARGNS_MODEL_SIZECollection = mSizeList.ToArray();
                }

                //Color               
                if (pObject.ModelColorList.Count > 0)
                {
                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR> mColorList = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR mColorModel;

                    foreach (ARGNSModelColor item in pObject.ModelColorList)
                    {
                        mColorModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR();

                        mColorModel.LineIdSpecified = true;
                        mColorModel.LineId = item.LineId;
                        mColorModel.U_ColCode = item.U_ColCode;
                        mColorModel.U_Active = item.U_Active;
                        mColorList.Add(mColorModel);
                    }
                    oArgnsModel.ARGNS_MODEL_COLORCollection = mColorList.ToArray();
                }


                //Variable
                if (pObject.ModelVariableList.Count > 0)
                {

                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR> mVarList = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR mVarModel;

                    foreach (ARGNSModelVar item in pObject.ModelVariableList)
                    {
                        mVarModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR();
                        mVarModel.U_VarCode = item.U_VarCode;
                        mVarModel.U_Selected = item.U_Selected;
                        mVarModel.U_VarDesc = item.U_VarDesc;

                        mVarList.Add(mVarModel);
                    }

                    oArgnsModel.ARGNS_MODEL_VARCollection = mVarList.ToArray();
                }

                //Image
                if (pObject.ModelImageList.Count > 0)
                {
                    oArgnsModel.ARGNS_MODEL_IMGCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG[pObject.ModelImageList.Count];
                    counter = 0;
                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG mImageModelAdd = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG() { Code = item.Code, LineId = item.LineId, LineIdSpecified = true, U_File = item.U_File, U_FType = item.U_FType, U_Path = item.U_Path };
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter] = mImageModelAdd;
                        counter++;
                    }
                }

                #endregion

                //Inserto las Listas de Precios
                CreatePriceList(pObject.U_ModCode, mDefaultList, Convert.ToDouble(pObject.U_Price), pCompanyParam.DSSessionId, pCompanyParam);

                WebServices.ARGNSModelServiceWeb.ArgnsModelParams mModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
                mModelParams = oArgnsModelService.Add(oArgnsModel);
                mJsonObjectResult.Code = mModelParams.Code;
                //mJsonObjectResult.Others.Add("U_ModCode", mDbContext.C_ARGNS_MODEL.Where(c => c.Code == mModelParams.Code).FirstOrDefault().U_ModCode);
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }
            return mJsonObjectResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pModCode"></param>
        /// <param name="pPriceList"></param>
        /// <param name="pPrice"></param>
        /// <param name="pSession"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string CreatePriceList(string pModCode, short pPriceList, double pPrice, string pSession, CompanyConn pCompanyParam)
        {
            SBODI_Server.Node mServerNode = null;
            XmlDocument mResultXML = null;
            XmlNodeList mElement;
            string ret = string.Empty;
            string mQuery;
            try
            {
                mResultXML = new XmlDocument();
                mServerNode = new SBODI_Server.Node();

                //Obtengo Todas as Listas de Precio de SAP
                mQuery = "SELECT A.\"ListNum\" as \"LISTNUM\" , A.\"ListName\" as \"LISTNAME\", (SELECT B.\"ListName\" FROM \"OPLN\" B WHERE B.\"ListNum\" = A.\"BASE_NUM\") As \"BASE_Name\", A.\"Factor\" as \"Factor\", (SELECT B.\"ListNum\" FROM \"{0}\".\"OPLN\" B WHERE B.\"ListNum\" = A.\"BASE_NUM\") As \"BASE_NUM\" FROM  \"{0}\".\"OPLN\" A ORDER BY A.\"ListNum\"";
                mResultXML.LoadXml(mServerNode.Interact(UtilWeb.ExecuteQuery(pSession, string.Format(mQuery, pCompanyParam.CompanyDB))));
                mElement = mResultXML.GetElementsByTagName("row");

                if (mElement.Count > 0)
                {
                    string mCode;
                    string mInsertQuery = "INSERT INTO \"{11}\".\"@ARGNS_MODLIST\" (\"Code\",\"Name\" ,\"U_ModCode\",\"U_ListNum\",\"U_ListName\",\"U_Price\",\"U_DefList\",\"U_BasList\" ,\"U_BASE_NUM\" ,\"U_Factor\",\"U_Manual\") VALUES ('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}','{8}','{9}','{10}')";
                    foreach (XmlNode item in mElement)
                    {
                        mCode = GetLastModelPriceList(pCompanyParam);

                        if (pPriceList == Convert.ToInt32(item["LISTNUM"].InnerText))
                        {
                            mQuery = string.Format(mInsertQuery, mCode, mCode, pModCode, item["LISTNUM"].InnerText, item["LISTNAME"].InnerText, pPrice, "Y", item["BASE_Name"].InnerText, item["BASE_NUM"].InnerText, item["Factor"].InnerText, "Y", pCompanyParam.CompanyDB);
                            mResultXML.LoadXml(mServerNode.Interact(UtilWeb.ExecuteQuery(pSession, mQuery)));
                        }
                        else
                        {
                            mQuery = string.Format(mInsertQuery, mCode, mCode, pModCode, item["LISTNUM"].InnerText, item["LISTNAME"].InnerText, 0, "N", item["BASE_Name"].InnerText, item["BASE_NUM"].InnerText, item["Factor"].InnerText, "N", pCompanyParam.CompanyDB);
                            mResultXML.LoadXml(mServerNode.Interact(UtilWeb.ExecuteQuery(pSession, mQuery)));
                        }
                    }
                }

                ret = "Ok";
            }
            catch (Exception ex)
            {
                ret = "Error:" + ex.Message;
            }
            finally
            {
                Utils.ReleaseObject((object)mServerNode);
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string GetLastModelPriceList(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelPriceList", "", mOthers);
                List<ARGNSModelPriceList> listResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelPriceList>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mMaxVal = (listResult.SingleOrDefault() != null) ? (listResult.SingleOrDefault().DocEntry + 1) : 1;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastModelPriceList :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        public string GetLastModelCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", "", mOthers);
                List<ARGNSModel> mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mMaxVal = mListModelReturn.SingleOrDefault().DocEntry + 1;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastModelCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        public bool GetModelExist(CompanyConn pCompanyParam, string pModCode)
        {
            bool ret = true;
            string mUrl = string.Empty;
            string mFilter;
            try
            {
                mFilter = "U_ModCode eq '" + pModCode + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilter);
                List<ARGNSModel> mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                ret = (mListModelReturn.Count > 0) ? true : false;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelExist :" + ex.Message);
                return ret = true; ;
            }

            return ret;
        }

        #endregion

        #region Design

        public ARGNSModel GetDesignById(CompanyConn pCompanyParam, string pCode)
        {
            ARGNSModel mModelReturn = null;
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
            string mUrl = string.Empty;
            string mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);

                List<ARGNSModel> modelListAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mModelReturn = modelListAux.FirstOrDefault();
                if (mModelReturn != null)
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelImg", mFilters);
                    mModelReturn.ModelImageList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelImg>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
                else
                {
                    mModelReturn = new ARGNSModel();
                }
                //Lista de Todos los Combos del Modelo
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                mPDMSAPCombo.ListBrand.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Div");
                mPDMSAPCombo.ListDivision.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSDivision>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OITB");
                mPDMSAPCombo.ListItemGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                mPDMSAPCombo.ListModelGroup.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ProdLine");
                mPDMSAPCombo.ListProductLine.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSProductLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mPDMSAPCombo.ListSeason.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "StyleStatus");
                mPDMSAPCombo.ListStatus.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSStyleStatus>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Year");
                mPDMSAPCombo.ListYear.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSYear>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PriceList");
                mPDMSAPCombo.PriceList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PriceListSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Segmentation");
                mPDMSAPCombo.ListSegmentation.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSegmentation>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mPDMSAPCombo.ListCollection.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                mPDMSAPCombo.ListSubCollection.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Composition");
                mPDMSAPCombo.CompositionList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSComposition>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Simbols");
                mPDMSAPCombo.SimbolList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSimbol>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP");
                mPDMSAPCombo.BusinessPartnerList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Currency");
                mPDMSAPCombo.CurrencyList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrencySAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                mPDMSAPCombo.DesignerList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                mModelReturn.ListPDMSAPCombo = mPDMSAPCombo;
            }
            catch (Exception)
            {
                throw;
            }

            return mModelReturn;
        }

        public List<ARGNSModel> GetDesignListSearch(CompanyConn pCompanyParam, ref PDMSAPCombo combo,
            string pCodeModel = "", string pNameModel = "",
            string pSeasonModel = "", string pGroupModel = "",
            string pBrand = "", string pCollection = "",
            string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            List<ARGNSModel> mListModelReturn = null;
            string mUrl = string.Empty;
            try
            {
                string mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSeasonModel.ToUpper()) + "'), tolower(Name)) ";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season", mFilters);

                List<ARGNSSeason> mSeasonList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                string mSeasonCode = (mSeasonList.FirstOrDefault() == null ? null : mSeasonList.FirstOrDefault().Code);

                mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pGroupModel.ToUpper()) + "'), tolower(Name))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp", mFilters);
                List<ARGNSModelGroup> mGroupModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                string mModelGroupCode = (mGroupModelList.FirstOrDefault() == null ? null : mGroupModelList.FirstOrDefault().Code);

                mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pBrand.ToUpper()) + "'), tolower(Name))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand", mFilters);
                List<ARGNSBrand> mBrandList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                string mBrandCode = (mBrandList.FirstOrDefault() == null ? null : mBrandList.FirstOrDefault().Code);

                mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCollection.ToUpper()) + "'), tolower(U_Desc))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection", mFilters);
                List<ARGNSCollection> mCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                string mCollCode = (mCollList.FirstOrDefault() == null ? null : mCollList.FirstOrDefault().U_CollCode);

                mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pSubCollection.ToUpper()) + "'), tolower(U_Desc))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient", mFilters);
                List<ARGNSSubCollection> mSubCollList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                string mSubCollCode = (mSubCollList.FirstOrDefault() == null ? null : mSubCollList.FirstOrDefault().U_AmbCode);


                mFilters = "1 eq 1 and substringof(tolower('" + HttpUtility.UrlEncode(pCodeModel) + "'), tolower(U_ModCode)) ";
                if (!string.IsNullOrEmpty(pNameModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pNameModel) + "'), tolower(U_ModDesc))";
                }
                if (!string.IsNullOrEmpty(pSeasonModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mSeasonCode) + "'), tolower(U_Season))";
                }
                if (!string.IsNullOrEmpty(pGroupModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mModelGroupCode) + "'), tolower(U_ModGrp))";
                }
                if (!string.IsNullOrEmpty(pBrand))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mBrandCode) + "'), tolower(U_Brand))";
                }
                if (!string.IsNullOrEmpty(pCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mCollCode) + "'), tolower(U_CollCode))";
                }
                if (!string.IsNullOrEmpty(pSubCollection))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mSubCollCode) + "'), tolower(U_AmbCode))";
                }
                mFilters = mFilters + "and substringof(tolower('-1'), tolower(U_Status))";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Lleno el combo con los datos
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                combo.ListBrand = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                combo.ListModelGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                combo.ListSeason = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                combo.ListCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                combo.ListSubCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return Mapper.Map<List<ARGNSModel>>(mListModelReturn).OrderBy(o => o.U_ModCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetDesignListSearch :" + ex.Message);
                return null;
            }
        }

        public string UpdateDesign(ARGNSModel pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = (long)(pObject.U_PList.HasValue ? pObject.U_PList : 1);
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;

                //Image
                int mCount = 0;
                if (pObject.ModelImageList.Count > 0)
                {
                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG> mListImg = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG mImageModel;

                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        mImageModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG();
                        mImageModel.LineIdSpecified = true;
                        mImageModel.LineId = mCount + 1;
                        mImageModel.U_File = item.U_File;
                        mImageModel.U_FType = item.U_FType;
                        mImageModel.U_Path = item.U_Path;
                        mListImg.Add(mImageModel);
                        mCount++;
                    }
                    oArgnsModel.ARGNS_MODEL_IMGCollection = mListImg.ToArray();
                }

                oArgnsModelService.Update(oArgnsModel);
                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string AddDesign(ARGNSModel pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                string mLastCode = GetLastModelCode(pCompanyParam);
                oArgnsModel.Code = mLastCode;
                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = (long)(pObject.U_PList.HasValue ? pObject.U_PList : 1);
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;

                int counter = 0;
                //Image
                if (pObject.ModelImageList.Count > 0)
                {
                    oArgnsModel.ARGNS_MODEL_IMGCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG[pObject.ModelImageList.Count];
                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter] = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG();
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].LineIdSpecified = true;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_ModCode = item.U_ModCode;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_File = item.U_File;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_FType = item.U_FType;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_Path = item.U_Path;
                        counter++;
                    }
                }

                oArgnsModelService.Add(oArgnsModel);
                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region Logo

        public List<ARGNSModelLogo> GetLogoList(CompanyConn pCompanyParam, string pCode)
        {
            string mUrl = string.Empty;
            string mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelLogos", mFilters);

                List<ARGNSModelLogo> listReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelLogo>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return listReturn.Where(w => w.U_CodeLogo != null).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("ScaleMasterService -> GetLogoListSL :" + ex.Message);
                return null;
            }
        }

        public ARGNSLogo GetLogoById(CompanyConn pCompanyParam, string pCode)
        {
            string mUrl = string.Empty;
            string mFilters = "U_CodeLogo eq '" + HttpUtility.UrlEncode(pCode) + "'";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Logo", mFilters);

                List<ARGNSLogo> listReturnAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSLogo>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return listReturnAux.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteError("ScaleMasterService -> GetLogoByIdSL :" + ex.Message);
                return null;
            }
        }

        #endregion

        #region Color

        public ARGNSColor GetColorByCode(CompanyConn pCompanyParam, string pColorCode)
        {
            ARGNSColor mRet = null;
            string mUrl = string.Empty;
            string mFilters = "U_ColCode eq " + HttpUtility.UrlEncode(pColorCode);
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Color", mFilters);

                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSColor>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                if (mRet == null)
                {
                    mRet = new ARGNSColor();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return mRet;
        }

        public List<ARGNSColor> GetColorMasterListSearch(CompanyConn pCompanyParam, string pColorCode = "", string pColorName = "")
        {
            List<ARGNSColor> mRet = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(pColorCode))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pColorCode) + "'), tolower(U_ColCode))";
                }

                if (!string.IsNullOrEmpty(pColorName))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pColorName) + "'), tolower(U_ColDesc))";
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Color", mFilters);

                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSColor>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception)
            {
                throw;
            }

            return mRet;
        }

        public List<ARGNSModelColor> GetColorByModel(CompanyConn pCompanyParam, string pModel)
        {
            List<ARGNSModelColor> mRet = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(pModel))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pModel) + "'), tolower(U_ModCode))";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomModelColor", mFilters);

                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelColor>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetColorByModel :" + ex.Message);
                return null;
            }

            return mRet;
        }

        public string AddColor(ARGNSColor pObject, CompanyConn pCompanyParam)
        {
            ARGNSColorServiceWeb.ArgnsColor oArgnsColor = new ARGNSColorServiceWeb.ArgnsColor();
            ARGNSColorServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSColorServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSColorServiceWeb.MsgHeaderServiceName.ARGNS_COLOR;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSColorServiceWeb.ARGNS_COLOR oArgnsColorService = new ARGNSColorServiceWeb.ARGNS_COLOR();
            oArgnsColorService.MsgHeaderValue = oArgnsModelMsgHeader;
            try
            {
                oArgnsColor.Code = GetLastColorCode(pCompanyParam);
                oArgnsColor.U_Active = pObject.U_Active;
                oArgnsColor.U_Argb = pObject.U_Argb;
                oArgnsColor.U_Attrib1 = pObject.U_Attrib1;
                oArgnsColor.U_Attrib2 = pObject.U_Attrib2;
                oArgnsColor.U_ColCode = pObject.U_ColCode;
                oArgnsColor.U_ColDesc = pObject.U_ColDesc;
                oArgnsColor.U_NRFCode = pObject.U_NRFCode;
                oArgnsColor.U_Pic = pObject.U_Pic;
                oArgnsColor.U_Shade = pObject.U_Shade;

                oArgnsColorService.Add(oArgnsColor);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdateColor(ARGNSColor pObject, CompanyConn pCompanyParam)
        {
            ARGNSColorServiceWeb.ArgnsColor oArgnsColor = new ARGNSColorServiceWeb.ArgnsColor();
            ARGNSColorServiceWeb.ArgnsColorParams oArgnsColorParams = new ARGNSColorServiceWeb.ArgnsColorParams();
            ARGNSColorServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSColorServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSColorServiceWeb.MsgHeaderServiceName.ARGNS_COLOR;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSColorServiceWeb.ARGNS_COLOR oArgnsColorService = new ARGNSColorServiceWeb.ARGNS_COLOR();
            oArgnsColorService.MsgHeaderValue = oArgnsModelMsgHeader;
            try
            {
                oArgnsColorParams.Code = pObject.Code;
                oArgnsColor = oArgnsColorService.GetByParams(oArgnsColorParams);

                oArgnsColor.U_Active = pObject.U_Active;
                oArgnsColor.U_Argb = pObject.U_Argb;
                oArgnsColor.U_Attrib1 = pObject.U_Attrib1;
                oArgnsColor.U_Attrib2 = pObject.U_Attrib2;
                oArgnsColor.U_ColCode = pObject.U_ColCode;
                oArgnsColor.U_ColDesc = pObject.U_ColDesc;
                oArgnsColor.U_NRFCode = pObject.U_NRFCode;
                oArgnsColor.U_Pic = pObject.U_Pic;
                oArgnsColor.U_Shade = pObject.U_Shade;

                oArgnsColorService.Update(oArgnsColor);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string GetLastColorCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                mOthers = "&$orderby=Code desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Color", "", mOthers);
                List<ARGNSColor> mListColorReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSColor>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                if (mListColorReturn.Count > 0)
                {
                    mMaxVal = Convert.ToInt32(mListColorReturn.SingleOrDefault().DocEntry) + 1;
                }
                else
                {
                    mMaxVal = 1;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastColorCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        #endregion

        #region Scale Master

        public List<ARGNSScale> GetScaleMasterListSearch(CompanyConn pCompanyParam, string pScaleCode = "", string pScaleName = "")
        {
            List<ARGNSScale> mRet = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {

                if (!string.IsNullOrEmpty(pScaleCode))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pScaleCode) + "'), tolower(U_SclCode))";
                }

                if (!string.IsNullOrEmpty(pScaleName))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pScaleName) + "'), tolower(U_SclDesc))";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Scale", mFilters);

                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSScale>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetScaleMasterListSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSSize> GetScaleDescriptionListSearch(CompanyConn pCompanyParam, string pScaleCode = "")
        {
            List<ARGNSSize> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {

                mFilters = "Code eq '" + HttpUtility.UrlEncode(pScaleCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Size", mFilters);

                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSize>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetScaleDescriptionListSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSSize> GetSizeByScale(CompanyConn pCompanyParam, string pScaleCode = "", string pModeCode = "")
        {
            List<ARGNSSize> mRet = null;
            List<ARGNSModel> mModel = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {

                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModeCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);

                mModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


                mFilters = "Code eq '" + HttpUtility.UrlEncode(mModel.Single().Code) + "' and U_SclCode eq '" + HttpUtility.UrlEncode(pScaleCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelSize", mFilters);
                JavaScriptSerializer serializeraux = new JavaScriptSerializer();
                mRet = serializeraux.Deserialize<List<ARGNSSize>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSizeByScale :" + ex.Message);
                return null;
            }
            return mRet;
        }

        #endregion

        #region Variable

        public List<ARGNSVariable> GetVariableListSearch(CompanyConn pCompanyParam, ref List<ARGNSSegmentation> pSegmentationList, string pVarCode = "", string pVarName = "", string pVarSegmentation = "")
        {
            string mFilters = string.Empty, mUrl = string.Empty, mSegmentationCode = string.Empty;

            try
            {
                if (pVarSegmentation != "")
                {
                    mFilters = "substringof(tolower('" + pVarSegmentation + "'), tolower(Name)) ";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Segmentation", mFilters);
                    List<ARGNSSegmentation> mSegmentationListAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSegmentation>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSegmentationCode = (mSegmentationListAux.Count == 0 ? "null" : mSegmentationListAux.FirstOrDefault().Code);
                }

                mFilters = "1 eq 1 and substringof(tolower('" + HttpUtility.UrlEncode(pVarCode) + "'), tolower(U_VarCode)) ";
                if (!string.IsNullOrEmpty(pVarName))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pVarName) + "'), tolower(U_VarDesc))";
                }
                if (!string.IsNullOrEmpty(pVarSegmentation))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(mSegmentationCode) + "'), tolower(U_ATGrp))";
                }
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Variable", mFilters);
                List<ARGNSVariable> mSegmentationReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSVariable>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mSegmentationReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetVariablerListSearch :" + ex.Message);
                return null;
            }
        }

        #endregion

        #region SKU & Prepacks

        public List<SkuModel> GetSkuModelList(CompanyConn pCompanyParam, string pModCode, ref List<ARGNSModelImg> pImgList)
        {
            List<SkuModel> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                mFilters = "U_ARGNS_MOD eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomOITMModel", mFilters);
                mRet = serializer.Deserialize<List<SkuModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                //mRet = mRet.Where(c => c.U_ARGNS_ITYPE != "P").ToList();

                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                List<ARGNSModel> mModelList = serializer.Deserialize<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mFilters = "Code eq '" + HttpUtility.UrlEncode(mModelList.SingleOrDefault().Code) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelImg", mFilters);
                pImgList = serializer.Deserialize<List<ARGNSModelImg>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                pImgList = pImgList.Select(c => { c.U_ModCode = pModCode; return c; }).ToList();

                //Buscar codigo size run del prepack
                foreach (SkuModel mSku in mRet.Where(c => c.U_ARGNS_ITYPE == "P"))
                {
                    mFilters = "Code eq '" + mSku.ItemCode + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_Apparel_OITT", mFilters);
                    OITT mBillOfMaterialItem = serializer.Deserialize<List<OITT>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    if (mBillOfMaterialItem != null)
                    {
                        mFilters = "U_PpCode eq '" + mBillOfMaterialItem.U_ARGNS_PPCode + "'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_PREPACK", mFilters);
                        string ppCode = serializer.Deserialize<List<C_ARGNS_PREPACK>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault().Code;

                        mFilters = "Code eq '" + ppCode + "'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_PREPACKLNS", mFilters);
                        List<C_ARGNS_PREPACKLNS> mLines = serializer.Deserialize<List<C_ARGNS_PREPACKLNS>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();
                        string mSizeRunCode = "";
                        foreach (C_ARGNS_PREPACKLNS mLine in mLines.OrderBy(c => c.LineId))
                        {
                            mSizeRunCode += Convert.ToDouble(mLine.U_Value).ToString();
                        }
                        mSku.U_PpSizeRun = mSizeRunCode;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSkuModelList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSPrepack> GetModelPrepacks(CompanyConn pCompanyParam, string pModCode)
        {
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                mFilters = "U_ModCode eq '" + pModCode + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_PREPACK", mFilters);
                List<ARGNSPrepack> mPrepackList = serializer.Deserialize<List<ARGNSPrepack>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                foreach (ARGNSPrepack mPrepack in mPrepackList)
                {
                    mFilters = "Code eq '" + mPrepack.Code + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_PREPACKLNS", mFilters);
                    mPrepack.PrepackLines = serializer.Deserialize<List<ARGNSPrepackLines>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

                return mPrepackList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceSL -> GetModelPrepacks :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSSizeRun> GetSizeRunsByScale(CompanyConn pCompanyParam, string pScaleCode)
        {
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                mFilters = "U_SclCode eq '" + pScaleCode + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_SIZERUNSCALE", mFilters);
                List<ARGNSSizeRun> mSizeRunList = serializer.Deserialize<List<ARGNSSizeRun>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                foreach (ARGNSSizeRun mSizeRun in mSizeRunList)
                {
                    mFilters = "Code eq '" + mSizeRun.Code + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_SZRUNSCALELN", mFilters);
                    mSizeRun.Lines = serializer.Deserialize<List<ARGNSSizeRunLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

                return mSizeRunList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceSL -> GetSizeRunsByScale :" + ex.Message);
                return null;
            }
        }

        #endregion

        #region Comments
        public List<ARGNSModelComment> GetModelComment(CompanyConn pCompanyParam, string pModCode)
        {
            List<ARGNSModelComment> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                mFilters = "U_MODCODE eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CommLogModel", mFilters);
                mRet = serializer.Deserialize<List<ARGNSModelComment>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSkuModelList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public string AddLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam)
        {
            string mLastCode;
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel oArgnsModelComment = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel();
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams oArgnsModelParams = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams();
            ARGNSModelCommentServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelCommentServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelCommentServiceWeb.MsgHeaderServiceName.ARGNS_UDOCLMODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL oArgnsModelCommentService = new ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL();
            oArgnsModelCommentService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {

                mLastCode = GetLastCommentCode(pCompanyParam);
                oArgnsModelComment.Code = mLastCode;
                oArgnsModelComment.Name = mLastCode;
                oArgnsModelComment.U_MODCODE = pObject.U_MODCODE;
                oArgnsModelComment.U_DATE = System.DateTime.Now;
                oArgnsModelComment.U_DATESpecified = true;
                oArgnsModelComment.U_USER = pObject.U_USER;
                oArgnsModelComment.U_COMMENT = pObject.U_COMMENT;
                oArgnsModelComment.U_FILE = pObject.U_FILE;
                oArgnsModelComment.U_FILEPATH = pObject.U_FILEPATH;

                oArgnsModelParams = oArgnsModelCommentService.Add(oArgnsModelComment);
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
            return "OK";
        }

        public string UpdateLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel oArgnsModelComment = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel();
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams oArgnsModelParams = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams();
            ARGNSModelCommentServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelCommentServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelCommentServiceWeb.MsgHeaderServiceName.ARGNS_UDOCLMODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL oArgnsModelCommentService = new ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL();
            oArgnsModelCommentService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModelComment = oArgnsModelCommentService.GetByParams(oArgnsModelParams);

                oArgnsModelComment.Code = pObject.Code;
                oArgnsModelComment.Name = pObject.Name;
                oArgnsModelComment.U_COMMENT = pObject.U_COMMENT;
                oArgnsModelComment.U_DATE = System.DateTime.Now;
                oArgnsModelComment.U_FILE = pObject.U_FILE;
                oArgnsModelComment.U_FILEPATH = pObject.U_FILEPATH;
                oArgnsModelComment.U_MODCODE = pObject.U_MODCODE;
                oArgnsModelComment.U_USER = pObject.U_USER;

                oArgnsModelCommentService.Update(oArgnsModelComment);
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }

            return "OK";
        }

        public string GetLastCommentCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CommLogModel", "", mOthers);
                List<ARGNSModelSample> listResult = serializer.Deserialize<List<ARGNSModelSample>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mMaxVal = (listResult.Count != 0) ? listResult.SingleOrDefault().DocEntry + 1 : 1;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastCommentCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }
        #endregion

        #region Cost Sheet

        public List<ARGNSCostSheet> GetModelCostSheet(CompanyConn pCompanyParam, string pModCode)
        {
            List<ARGNSCostSheet> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CostSheet", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCostSheet>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelCostSheet :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public ARGNSCostSheet GetModelCostSheetByCode(CompanyConn pCompanyParam, string pCode, string pModelCode)
        {
            ARGNSCostSheet mResult = null;
            string mUrl = string.Empty;
            string mFilters;

            try
            {

                mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CostSheet", mFilters);
                List<ARGNSCostSheet> mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCostSheet>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mResult = mRet.SingleOrDefault();

                if (mResult != null)
                {
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CSMaterials", mFilters);
                    mResult.ListMaterial = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCSMaterial>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CSOperations", mFilters);
                    mResult.ListOperation = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCSOperation>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CSSchemas", mFilters);
                    mResult.ListSchema = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCSSchema>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CSPatterns", mFilters);
                    mResult.ListPattern = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCSPatterns>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
                else
                {
                    mResult = new ARGNSCostSheet();
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(pModelCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                    List<ARGNSModel> mAuxModList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mResult.U_ModCode = mAuxModList.SingleOrDefault().U_ModCode;
                }

                mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PriceList");
                mResult.CostSheetCombo.PriceList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PriceListSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Currency");
                mResult.CostSheetCombo.CurrencyList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrencySAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mFilters = "CardType eq 'S'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP");
                mResult.CostSheetCombo.VendorList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mResult.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelCostSheetByCode :" + ex.Message);
                return null;
            }
            return mResult;
        }

        public List<ARGNSCostSheet> GetCostSheetListSearch(CompanyConn pCompanyParam, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            List<ARGNSCostSheet> ListRestu = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(txtStyle))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtStyle) + "'), tolower(U_ModCode))";
                }

                if (!string.IsNullOrEmpty(txtCodeCostSheet))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtCodeCostSheet) + "'), tolower(Name))";
                }

                if (!string.IsNullOrEmpty(txtDescription))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtDescription) + "'), tolower(U_Description))";
                }
                // ModeDBCode = mARGNS_MODEL.Code
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomCostSheetMod", mFilters);

                ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCostSheet>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCostSheetListSearch :" + ex.Message);
                return null;
            }
            return ListRestu;
        }

        public List<ARGNSCostSheet> GetUserCostSheetListSearch(CompanyConn pCompanyParam, string pUserCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            List<ARGNSCostSheet> ListRestu = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "U_Owner eq '" + HttpUtility.UrlEncode(pUserCode) + "' ";

                if (!string.IsNullOrEmpty(txtStyle))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtStyle) + "'), tolower(U_ModCode))";
                }

                if (!string.IsNullOrEmpty(txtCodeCostSheet))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtCodeCostSheet) + "'), tolower(Name))";
                }

                if (!string.IsNullOrEmpty(txtDescription))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtDescription) + "'), tolower(U_Description))";
                }
                // ModeDBCode = mARGNS_MODEL.Code
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomCostSheetMod", mFilters);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                ListRestu = serializer.Deserialize<List<ARGNSCostSheet>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetUserCostSheetListSearch :" + ex.Message);
                return null;
            }
            return ListRestu;
        }

        public List<ARGNSCostSheet> GetBPCostSheetListSearch(CompanyConn pCompanyParam, string pBPCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            List<ARGNSCostSheet> mRet = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                if (!string.IsNullOrEmpty(txtStyle))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtStyle) + "'), tolower(U_ModCode))";
                }

                if (!string.IsNullOrEmpty(txtCodeCostSheet))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtCodeCostSheet) + "'), tolower(Name))";
                }

                if (!string.IsNullOrEmpty(txtDescription))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtDescription) + "'), tolower(U_Description))";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomCostSheetMod", mFilters);
                List<ARGNSCostSheet> ListRestu = serializer.Deserialize<List<ARGNSCostSheet>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


                mFilters = "U_CardType eq 'S' and U_CardCode eq '" + HttpUtility.UrlEncode(pBPCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelVendor", mFilters);
                List<ARGNSMODVENDOR> mModList = serializer.Deserialize<List<ARGNSMODVENDOR>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                var mSubQuery = mModList.Select(c => c.U_ModCode);

                mRet = (from mCostSheet in ListRestu
                        where mCostSheet.U_Customer == pBPCode || mSubQuery.Contains(mCostSheet.U_ModCode)
                        select new ARGNSCostSheet()
                        {
                            Code = mCostSheet.Code,
                            DocEntry = mCostSheet.DocEntry,
                            Name = mCostSheet.Name,
                            U_ModCode = mCostSheet.U_ModCode,
                            U_Description = mCostSheet.U_Description,
                            ModeDBCode = mCostSheet.ModeDBCode,
                            U_Owner = mCostSheet.U_Owner
                        }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetBPCostSheetListSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSSchema> GetCSSchemaList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            List<ARGNSSchema> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCode) + "'), tolower(U_SchCode)) and substringof(tolower('" + HttpUtility.UrlEncode(pName) + "'), tolower(Name))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CostSheetSchema", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSchema>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCSSchemaList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSSchemaLine> GetCSSchemaLineList(CompanyConn pCompanyParam, string pCode)
        {
            List<ARGNSSchemaLine> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CostSheetSchemaLine", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSchemaLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCSSchemaLineList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSOperTemplate> GetCSOperationTemplateList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            List<ARGNSOperTemplate> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCode) + "'), tolower(U_OTCode)) and substringof(tolower('" + HttpUtility.UrlEncode(pName) + "'), tolower(Name))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OperTemplate", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSOperTemplate>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCSSchemaList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSOperTemplateLine> GetCSOperationTemplateLineList(CompanyConn pCompanyParam, string pCode)
        {
            List<ARGNSOperTemplateLine> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OperTemplatel", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSOperTemplateLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCSSchemaLineList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSPatternTemplate> GetCSPatternList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            List<ARGNSPatternTemplate> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "substringof(tolower('" + HttpUtility.UrlEncode(pCode) + "'), tolower(U_CodeTmpl)) and substringof(tolower('" + HttpUtility.UrlEncode(pName) + "'), tolower(Name))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CostSheetPattern", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPatternTemplate>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCSPatternList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSPatternTemplateLine> GetCSPatternLineList(CompanyConn pCompanyParam, string pCode)
        {
            List<ARGNSPatternTemplateLine> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CostSheetPatternLine", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPatternTemplateLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCSPatternLineList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public string UpdateCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam)
        {
            try
            {
                ARGNSCostSheetServiceWeb.ArgnsCostSheet oCostSheet = new ARGNSCostSheetServiceWeb.ArgnsCostSheet();
                ARGNSCostSheetServiceWeb.ArgnsCostSheetParams oCostSheetParams = new ARGNSCostSheetServiceWeb.ArgnsCostSheetParams();
                ARGNSCostSheetServiceWeb.MsgHeader oCostSheetMsgHeader = new ARGNSCostSheetServiceWeb.MsgHeader();

                oCostSheetMsgHeader.ServiceName = ARGNSCostSheetServiceWeb.MsgHeaderServiceName.ARGNS_COST_SHEET;
                oCostSheetMsgHeader.ServiceNameSpecified = true;
                oCostSheetMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET oCostSheetService = new ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET();
                oCostSheetService.MsgHeaderValue = oCostSheetMsgHeader;


                oCostSheetParams.Code = pObject.Code;
                oCostSheet = oCostSheetService.GetByParams(oCostSheetParams);

                oCostSheet.U_OTCode = pObject.U_OTCode;
                oCostSheet.U_SchCode = pObject.U_SchCode;
                oCostSheet.U_CodeTmpl = pObject.U_CodeTmpl;
                oCostSheet.U_UserText1 = pObject.U_UserText1;
                oCostSheet.U_UserText2 = pObject.U_UserText2;
                oCostSheet.U_UserText3 = pObject.U_UserText3;
                if (pObject.U_PurchPrice != null)
                {
                    oCostSheet.U_PurchPrice = (double)pObject.U_PurchPrice;
                    oCostSheet.U_PurchPriceSpecified = true;
                }
                if (pObject.U_TMaterials != null)
                {
                    oCostSheet.U_TMaterials = (double)pObject.U_TMaterials;
                    oCostSheet.U_TMaterialsSpecified = true;
                }
                if (pObject.U_TOperations != null)
                {
                    oCostSheet.U_TOperations = (double)pObject.U_TOperations;
                    oCostSheet.U_TOperationsSpecified = true;
                }
                if (pObject.U_IndrCost != null)
                {
                    oCostSheet.U_IndrCost = (double)pObject.U_IndrCost;
                    oCostSheet.U_IndrCostSpecified = true;
                }
                if (pObject.U_TCost != null)
                {
                    oCostSheet.U_TCost = (double)pObject.U_TCost;
                    oCostSheet.U_TCostSpecified = true;
                }
                if (pObject.U_Duty != null)
                {
                    oCostSheet.U_Duty = (double)pObject.U_Duty;
                    oCostSheet.U_DutySpecified = true;
                }
                if (pObject.U_IntTransp != null)
                {
                    oCostSheet.U_IntTransp = (double)pObject.U_IntTransp;
                    oCostSheet.U_IntTranspSpecified = true;
                }
                if (pObject.U_Freight != null)
                {
                    oCostSheet.U_Freight = (double)pObject.U_Freight;
                    oCostSheet.U_FreightSpecified = true;
                }
                if (pObject.U_Margin != null)
                {
                    oCostSheet.U_Margin = (double)pObject.U_Margin;
                    oCostSheet.U_MarginSpecified = true;
                }
                if (pObject.U_DDP != null)
                {
                    oCostSheet.U_DDP = (double)pObject.U_DDP;
                    oCostSheet.U_DDPSpecified = true;
                }
                if (pObject.U_CostPercent != null)
                {
                    oCostSheet.U_CostPercent = (double)pObject.U_CostPercent;
                    oCostSheet.U_CostPercentSpecified = true;
                }
                if (pObject.U_SlsPercent != null)
                {
                    oCostSheet.U_SlsPercent = (double)pObject.U_SlsPercent;
                    oCostSheet.U_SlsPercentSpecified = true;
                }
                if (pObject.U_StrFactor != null)
                {
                    oCostSheet.U_StrFactor = (double)pObject.U_StrFactor;
                    oCostSheet.U_StrFactorSpecified = true;
                }
                if (pObject.U_TMargin != null)
                {
                    oCostSheet.U_TMargin = (double)pObject.U_TMargin;
                    oCostSheet.U_TMarginSpecified = true;
                }
                if (pObject.U_CIF != null)
                {
                    oCostSheet.U_CIF = (double)pObject.U_CIF;
                    oCostSheet.U_CIFSpecified = true;
                }
                if (pObject.U_FPrice != null)
                {
                    oCostSheet.U_FPrice = (double)pObject.U_FPrice;
                    oCostSheet.U_FPriceSpecified = true;
                }

                #region Materials

                if (pObject.ListMaterial.Count > 0 || oCostSheet.ARGNS_CS_MATERIALSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mMaterialSAP in oCostSheet.ARGNS_CS_MATERIALSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSMaterial mMaterial = pObject.ListMaterial.Where(c => c.LineId == mMaterialSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mMaterial != null)
                        {
                            mMaterialSAP.U_ItemCode = mMaterial.U_ItemCode;
                            mMaterialSAP.U_ItemName = mMaterial.U_ItemName;
                            if (mMaterial.U_Quantity.HasValue)
                            {
                                mMaterialSAP.U_Quantity = (double)mMaterial.U_Quantity;
                                mMaterialSAP.U_QuantitySpecified = true;
                            }
                            mMaterialSAP.U_CxT = mMaterial.U_CxT;
                            mMaterialSAP.U_CxS = mMaterial.U_CxS;
                            mMaterialSAP.U_CxC = mMaterial.U_CxC;
                            mMaterialSAP.U_CxGT = mMaterial.U_CxGT;
                            mMaterialSAP.U_UoM = mMaterial.U_UoM;
                            mMaterialSAP.U_Whse = mMaterial.U_Whse;
                            mMaterialSAP.U_OcrCode = mMaterial.U_OcrCode;
                            if (mMaterial.U_PList.HasValue)
                            {
                                mMaterialSAP.U_PList = (long)mMaterial.U_PList;
                                mMaterialSAP.U_PListSpecified = true;
                            }
                            mMaterialSAP.U_Currency = mMaterial.U_Currency;
                            if (mMaterial.U_UPrice.HasValue)
                            {
                                mMaterialSAP.U_UPrice = (double)mMaterial.U_UPrice;
                                mMaterialSAP.U_UPriceSpecified = true;
                            }
                            mMaterialSAP.U_PVendor = mMaterial.U_PVendor;
                            mMaterialSAP.U_Comments = mMaterial.U_Comments;
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_MATERIALSCollection = oCostSheet.ARGNS_CS_MATERIALSCollection.Where(c => c.LineId != mMaterialSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[oCostSheet.ARGNS_CS_MATERIALSCollection.Length + pObject.ListMaterial.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_MATERIALSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_MATERIALSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSMaterial mMaterial in pObject.ListMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS lineMaterial = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_ItemCode = mMaterial.U_ItemCode;
                        lineMaterial.U_ItemName = mMaterial.U_ItemName;
                        if (mMaterial.U_Quantity.HasValue)
                        {
                            lineMaterial.U_Quantity = (double)mMaterial.U_Quantity;
                            lineMaterial.U_QuantitySpecified = true;
                        }
                        lineMaterial.U_CxT = mMaterial.U_CxT;
                        lineMaterial.U_CxS = mMaterial.U_CxS;
                        lineMaterial.U_CxC = mMaterial.U_CxC;
                        lineMaterial.U_CxGT = mMaterial.U_CxGT;
                        lineMaterial.U_UoM = mMaterial.U_UoM;
                        lineMaterial.U_Whse = mMaterial.U_Whse;
                        lineMaterial.U_OcrCode = mMaterial.U_OcrCode;
                        if (mMaterial.U_PList.HasValue)
                        {
                            lineMaterial.U_PList = (long)mMaterial.U_PList;
                            lineMaterial.U_PListSpecified = true;
                        }
                        lineMaterial.U_Currency = mMaterial.U_Currency;
                        if (mMaterial.U_UPrice.HasValue)
                        {
                            lineMaterial.U_UPrice = (double)mMaterial.U_UPrice;
                            lineMaterial.U_UPriceSpecified = true;
                        }
                        lineMaterial.U_PVendor = mMaterial.U_PVendor;
                        lineMaterial.U_Comments = mMaterial.U_Comments;

                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_MATERIALSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[newLines.Length];
                    oCostSheet.ARGNS_CS_MATERIALSCollection = newLines;
                }

                #endregion

                #region Operations

                if (pObject.ListOperation.Count > 0 || oCostSheet.ARGNS_CS_OPERATIONSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mOperationSAP in oCostSheet.ARGNS_CS_OPERATIONSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSOperation mOperation = pObject.ListOperation.Where(c => c.LineId == mOperationSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mOperation != null)
                        {
                            mOperationSAP.U_ItemCode = mOperation.U_ItemCode;
                            mOperationSAP.U_ItemName = mOperation.U_ItemName;
                            if (mOperation.U_Quantity.HasValue)
                            {
                                mOperationSAP.U_Quantity = (double)mOperation.U_Quantity;
                                mOperationSAP.U_QuantitySpecified = true;
                            }
                            mOperationSAP.U_UoM = mOperation.U_UoM;
                            if (mOperation.U_PList.HasValue)
                            {
                                mOperationSAP.U_PList = (long)mOperation.U_PList;
                                mOperationSAP.U_PListSpecified = true;
                            }
                            mOperationSAP.U_Currency = mOperation.U_Currency;
                            if (mOperation.U_Price.HasValue)
                            {
                                mOperationSAP.U_Price = (double)mOperation.U_Price;
                                mOperationSAP.U_PriceSpecified = true;
                            }
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_OPERATIONSCollection = oCostSheet.ARGNS_CS_OPERATIONSCollection.Where(c => c.LineId != mOperationSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[oCostSheet.ARGNS_CS_OPERATIONSCollection.Length + pObject.ListOperation.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_OPERATIONSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_OPERATIONSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSOperation mOperation in pObject.ListOperation.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS lineOperation = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS();
                        lineOperation.LineId = mOperation.LineId;
                        lineOperation.LineIdSpecified = true;
                        lineOperation.U_ItemCode = mOperation.U_ItemCode;
                        lineOperation.U_ItemName = mOperation.U_ItemName;
                        if (mOperation.U_Quantity.HasValue)
                        {
                            lineOperation.U_Quantity = (double)mOperation.U_Quantity;
                            lineOperation.U_QuantitySpecified = true;
                        }
                        lineOperation.U_UoM = mOperation.U_UoM;
                        if (mOperation.U_PList.HasValue)
                        {
                            lineOperation.U_PList = (long)mOperation.U_PList;
                            lineOperation.U_PListSpecified = true;
                        }
                        lineOperation.U_Currency = mOperation.U_Currency;
                        if (mOperation.U_Price.HasValue)
                        {
                            lineOperation.U_Price = (double)mOperation.U_Price;
                            lineOperation.U_PriceSpecified = true;
                        }

                        newLines[positionNewLine] = lineOperation;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[newLines.Length];
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = newLines;
                }

                #endregion

                #region Schemas

                if (pObject.ListSchema.Count > 0 || oCostSheet.ARGNS_CS_SCHEMASCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mSchemaSAP in oCostSheet.ARGNS_CS_SCHEMASCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSSchema mSchema = pObject.ListSchema.Where(c => c.LineId == mSchemaSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mSchema != null)
                        {
                            if (mSchema.U_Percent.HasValue)
                            {
                                mSchemaSAP.U_Percent = (double)mSchema.U_Percent;
                                mSchemaSAP.U_PercentSpecified = true;
                            }
                            if (mSchema.U_PList.HasValue)
                            {
                                mSchemaSAP.U_PList = (long)mSchema.U_PList;
                                mSchemaSAP.U_PListSpecified = true;
                            }
                            mSchemaSAP.U_Currency = mSchema.U_Currency;
                            if (mSchema.U_Amount.HasValue)
                            {
                                mSchemaSAP.U_Amount = (double)mSchema.U_Amount;
                                mSchemaSAP.U_AmountSpecified = true;
                            }
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_SCHEMASCollection = oCostSheet.ARGNS_CS_SCHEMASCollection.Where(c => c.LineId != mSchemaSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[oCostSheet.ARGNS_CS_SCHEMASCollection.Length + pObject.ListSchema.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_SCHEMASCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_SCHEMASCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSSchema mSchema in pObject.ListSchema.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS lineSchema = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS();
                        lineSchema.LineId = mSchema.LineId;
                        lineSchema.LineIdSpecified = true;
                        if (mSchema.U_Percent.HasValue)
                        {
                            lineSchema.U_Percent = (double)mSchema.U_Percent;
                            lineSchema.U_PercentSpecified = true;
                        }
                        if (mSchema.U_PList.HasValue)
                        {
                            lineSchema.U_PList = (long)mSchema.U_PList;
                            lineSchema.U_PListSpecified = true;
                        }
                        lineSchema.U_Currency = mSchema.U_Currency;
                        if (mSchema.U_Amount.HasValue)
                        {
                            lineSchema.U_Amount = (double)mSchema.U_Amount;
                            lineSchema.U_AmountSpecified = true;
                        }

                        newLines[positionNewLine] = lineSchema;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_SCHEMASCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[newLines.Length];
                    oCostSheet.ARGNS_CS_SCHEMASCollection = newLines;
                }

                #endregion

                #region Patterns

                if (pObject.ListPattern.Count > 0 || oCostSheet.ARGNS_CS_PATTERNSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mPatternSAP in oCostSheet.ARGNS_CS_PATTERNSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSPatterns mPattern = pObject.ListPattern.Where(c => c.LineId == mPatternSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPattern != null)
                        {
                            mPatternSAP.U_PattCode = mPattern.U_PattCode;
                            mPatternSAP.U_Desc = mPattern.U_Desc;
                            mPatternSAP.U_ItemCode = mPattern.U_ItemCode;
                            mPatternSAP.U_ItemName = mPattern.U_ItemName;
                            if (mPattern.U_Quantity.HasValue)
                            {
                                mPatternSAP.U_Quantity = (double)mPattern.U_Quantity;
                                mPatternSAP.U_QuantitySpecified = true;
                            }
                            mPatternSAP.U_UoM = mPattern.U_UoM;
                            if (mPattern.U_PattQty.HasValue)
                            {
                                mPatternSAP.U_PattQty = (long)mPattern.U_PattQty;
                                mPatternSAP.U_PattQtySpecified = true;
                            }
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_PATTERNSCollection = oCostSheet.ARGNS_CS_PATTERNSCollection.Where(c => c.LineId != mPatternSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[oCostSheet.ARGNS_CS_PATTERNSCollection.Length + pObject.ListPattern.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_PATTERNSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_PATTERNSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSPatterns mPattern in pObject.ListPattern.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS linePattern = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS();
                        linePattern.LineId = mPattern.LineId;
                        linePattern.LineIdSpecified = true;
                        linePattern.U_PattCode = mPattern.U_PattCode;
                        linePattern.U_Desc = mPattern.U_Desc;
                        linePattern.U_ItemCode = mPattern.U_ItemCode;
                        linePattern.U_ItemName = mPattern.U_ItemName;
                        if (mPattern.U_Quantity.HasValue)
                        {
                            linePattern.U_Quantity = (double)mPattern.U_Quantity;
                            linePattern.U_QuantitySpecified = true;
                        }
                        linePattern.U_UoM = mPattern.U_UoM;
                        if (mPattern.U_PattQty.HasValue)
                        {
                            linePattern.U_PattQty = (long)mPattern.U_PattQty;
                            linePattern.U_PattQtySpecified = true;
                        }

                        newLines[positionNewLine] = linePattern;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_PATTERNSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[newLines.Length];
                    oCostSheet.ARGNS_CS_PATTERNSCollection = newLines;
                }

                #endregion

                oCostSheetService.Update(oCostSheet);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> UpdateCostSheet :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public JsonObjectResult AddCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            try
            {
                ARGNSCostSheetServiceWeb.ArgnsCostSheet oCostSheet = new ARGNSCostSheetServiceWeb.ArgnsCostSheet();
                ARGNSCostSheetServiceWeb.ArgnsCostSheetParams oCostSheetParams = new ARGNSCostSheetServiceWeb.ArgnsCostSheetParams();
                ARGNSCostSheetServiceWeb.MsgHeader oCostSheetMsgHeader = new ARGNSCostSheetServiceWeb.MsgHeader();

                oCostSheetMsgHeader.ServiceName = ARGNSCostSheetServiceWeb.MsgHeaderServiceName.ARGNS_COST_SHEET;
                oCostSheetMsgHeader.ServiceNameSpecified = true;
                oCostSheetMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET oCostSheetService = new ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET();
                oCostSheetService.MsgHeaderValue = oCostSheetMsgHeader;

                oCostSheet.Code = GetLastCostSheetCode(pCompanyParam);
                oCostSheet.Name = pObject.Name;
                oCostSheet.U_ModCode = pObject.U_ModCode;
                oCostSheet.U_Currency = pObject.U_Currency;
                oCostSheet.U_Description = pObject.U_Description;
                oCostSheet.U_OTCode = pObject.U_OTCode;
                oCostSheet.U_SchCode = pObject.U_SchCode;
                oCostSheet.U_CodeTmpl = pObject.U_CodeTmpl;
                oCostSheet.U_UserText1 = pObject.U_UserText1;
                oCostSheet.U_UserText2 = pObject.U_UserText2;
                oCostSheet.U_UserText3 = pObject.U_UserText3;
                if (pObject.U_PurchPrice != null)
                {
                    oCostSheet.U_PurchPrice = (double)pObject.U_PurchPrice;
                    oCostSheet.U_PurchPriceSpecified = true;
                }
                if (pObject.U_TMaterials != null)
                {
                    oCostSheet.U_TMaterials = (double)pObject.U_TMaterials;
                    oCostSheet.U_TMaterialsSpecified = true;
                }
                if (pObject.U_TOperations != null)
                {
                    oCostSheet.U_TOperations = (double)pObject.U_TOperations;
                    oCostSheet.U_TOperationsSpecified = true;
                }
                if (pObject.U_IndrCost != null)
                {
                    oCostSheet.U_IndrCost = (double)pObject.U_IndrCost;
                    oCostSheet.U_IndrCostSpecified = true;
                }
                if (pObject.U_TCost != null)
                {
                    oCostSheet.U_TCost = (double)pObject.U_TCost;
                    oCostSheet.U_TCostSpecified = true;
                }
                if (pObject.U_Duty != null)
                {
                    oCostSheet.U_Duty = (double)pObject.U_Duty;
                    oCostSheet.U_DutySpecified = true;
                }
                if (pObject.U_IntTransp != null)
                {
                    oCostSheet.U_IntTransp = (double)pObject.U_IntTransp;
                    oCostSheet.U_IntTranspSpecified = true;
                }
                if (pObject.U_Freight != null)
                {
                    oCostSheet.U_Freight = (double)pObject.U_Freight;
                    oCostSheet.U_FreightSpecified = true;
                }
                if (pObject.U_Margin != null)
                {
                    oCostSheet.U_Margin = (double)pObject.U_Margin;
                    oCostSheet.U_MarginSpecified = true;
                }
                if (pObject.U_DDP != null)
                {
                    oCostSheet.U_DDP = (double)pObject.U_DDP;
                    oCostSheet.U_DDPSpecified = true;
                }
                if (pObject.U_CostPercent != null)
                {
                    oCostSheet.U_CostPercent = (double)pObject.U_CostPercent;
                    oCostSheet.U_CostPercentSpecified = true;
                }
                if (pObject.U_SlsPercent != null)
                {
                    oCostSheet.U_SlsPercent = (double)pObject.U_SlsPercent;
                    oCostSheet.U_SlsPercentSpecified = true;
                }
                if (pObject.U_StrFactor != null)
                {
                    oCostSheet.U_StrFactor = (double)pObject.U_StrFactor;
                    oCostSheet.U_StrFactorSpecified = true;
                }
                if (pObject.U_TMargin != null)
                {
                    oCostSheet.U_TMargin = (double)pObject.U_TMargin;
                    oCostSheet.U_TMarginSpecified = true;
                }
                if (pObject.U_CIF != null)
                {
                    oCostSheet.U_CIF = (double)pObject.U_CIF;
                    oCostSheet.U_CIFSpecified = true;
                }
                if (pObject.U_FPrice != null)
                {
                    oCostSheet.U_FPrice = (double)pObject.U_FPrice;
                    oCostSheet.U_FPriceSpecified = true;
                }

                #region Materials

                if (pObject.ListMaterial.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[pObject.ListMaterial.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSMaterial mMaterial in pObject.ListMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS lineMaterial = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_ItemCode = mMaterial.U_ItemCode;
                        lineMaterial.U_ItemName = mMaterial.U_ItemName;
                        if (mMaterial.U_Quantity.HasValue)
                        {
                            lineMaterial.U_Quantity = (double)mMaterial.U_Quantity;
                            lineMaterial.U_QuantitySpecified = true;
                        }
                        lineMaterial.U_CxT = mMaterial.U_CxT;
                        lineMaterial.U_CxS = mMaterial.U_CxS;
                        lineMaterial.U_CxC = mMaterial.U_CxC;
                        lineMaterial.U_CxGT = mMaterial.U_CxGT;
                        lineMaterial.U_UoM = mMaterial.U_UoM;
                        lineMaterial.U_Whse = mMaterial.U_Whse;
                        lineMaterial.U_OcrCode = mMaterial.U_OcrCode;
                        if (mMaterial.U_PList.HasValue)
                        {
                            lineMaterial.U_PList = (long)mMaterial.U_PList;
                            lineMaterial.U_PListSpecified = true;
                        }
                        lineMaterial.U_Currency = mMaterial.U_Currency;
                        if (mMaterial.U_UPrice.HasValue)
                        {
                            lineMaterial.U_UPrice = (double)mMaterial.U_UPrice;
                            lineMaterial.U_UPriceSpecified = true;
                        }
                        lineMaterial.U_PVendor = mMaterial.U_PVendor;
                        lineMaterial.U_Comments = mMaterial.U_Comments;

                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_MATERIALSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[newLines.Length];
                    oCostSheet.ARGNS_CS_MATERIALSCollection = newLines;
                }

                #endregion

                #region Operations

                if (pObject.ListOperation.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[pObject.ListOperation.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSOperation mOperation in pObject.ListOperation.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS lineOperation = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS();
                        lineOperation.LineId = mOperation.LineId;
                        lineOperation.LineIdSpecified = true;
                        lineOperation.U_ItemCode = mOperation.U_ItemCode;
                        lineOperation.U_ItemName = mOperation.U_ItemName;
                        if (mOperation.U_Quantity.HasValue)
                        {
                            lineOperation.U_Quantity = (double)mOperation.U_Quantity;
                            lineOperation.U_QuantitySpecified = true;
                        }
                        lineOperation.U_UoM = mOperation.U_UoM;
                        if (mOperation.U_PList.HasValue)
                        {
                            lineOperation.U_PList = (long)mOperation.U_PList;
                            lineOperation.U_PListSpecified = true;
                        }
                        lineOperation.U_Currency = mOperation.U_Currency;
                        if (mOperation.U_Price.HasValue)
                        {
                            lineOperation.U_Price = (double)mOperation.U_Price;
                            lineOperation.U_PriceSpecified = true;
                        }

                        newLines[positionNewLine] = lineOperation;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[newLines.Length];
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = newLines;
                }

                #endregion

                #region Schemas

                if (pObject.ListSchema.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[pObject.ListSchema.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSSchema mSchema in pObject.ListSchema.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS lineSchema = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS();
                        lineSchema.LineId = mSchema.LineId;
                        lineSchema.LineIdSpecified = true;
                        if (mSchema.U_Percent.HasValue)
                        {
                            lineSchema.U_Percent = (double)mSchema.U_Percent;
                            lineSchema.U_PercentSpecified = true;
                        }
                        if (mSchema.U_PList.HasValue)
                        {
                            lineSchema.U_PList = (long)mSchema.U_PList;
                            lineSchema.U_PListSpecified = true;
                        }
                        lineSchema.U_Currency = mSchema.U_Currency;
                        if (mSchema.U_Amount.HasValue)
                        {
                            lineSchema.U_Amount = (double)mSchema.U_Amount;
                            lineSchema.U_AmountSpecified = true;
                        }

                        newLines[positionNewLine] = lineSchema;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_SCHEMASCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[newLines.Length];
                    oCostSheet.ARGNS_CS_SCHEMASCollection = newLines;
                }

                #endregion

                #region Patterns

                if (pObject.ListPattern.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[pObject.ListPattern.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSPatterns mPattern in pObject.ListPattern.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS linePattern = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS();
                        linePattern.LineId = mPattern.LineId;
                        linePattern.LineIdSpecified = true;
                        linePattern.U_PattCode = mPattern.U_PattCode;
                        linePattern.U_Desc = mPattern.U_Desc;
                        linePattern.U_ItemCode = mPattern.U_ItemCode;
                        linePattern.U_ItemName = mPattern.U_ItemName;
                        if (mPattern.U_Quantity.HasValue)
                        {
                            linePattern.U_Quantity = (double)mPattern.U_Quantity;
                            linePattern.U_QuantitySpecified = true;
                        }
                        linePattern.U_UoM = mPattern.U_UoM;
                        if (mPattern.U_PattQty.HasValue)
                        {
                            linePattern.U_PattQty = (long)mPattern.U_PattQty;
                            linePattern.U_PattQtySpecified = true;
                        }

                        newLines[positionNewLine] = linePattern;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_PATTERNSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[newLines.Length];
                    oCostSheet.ARGNS_CS_PATTERNSCollection = newLines;
                }

                #endregion

                oCostSheetParams = oCostSheetService.Add(oCostSheet);
                mJsonObjectResult.Code = oCostSheetParams.Code;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                string mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pObject.U_ModCode) + "'";
                string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                List<ARGNSModel> mAuxModList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                string mModelCodeDB = mAuxModList.SingleOrDefault().Code;

                mJsonObjectResult.Others.Add("ModelCodeDB", mModelCodeDB);

                this.UpdateCostSheetModelLine(pCompanyParam, pObject);
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public string GetLastCostSheetCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CostSheet", "", mOthers);
                List<ARGNSCostSheet> mListCostSheetReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCostSheet>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                if (mListCostSheetReturn.Count > 0)
                {
                    mMaxVal = Convert.ToInt32(mListCostSheetReturn.SingleOrDefault().DocEntry) + 1;
                }
                else
                {
                    mMaxVal = 1;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastCostSheetCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        private string UpdateCostSheetModelLine(CompanyConn pCompanyParam, ARGNSCostSheet pObject)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;
            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                string mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pObject.U_ModCode) + "'";
                string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                List<ARGNSModel> mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                string mModelCodeDB = mListModelReturn.FirstOrDefault().Code;

                oArgnsModelParams.Code = mModelCodeDB;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS mNewLine = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS();
                mNewLine.Code = mModelCodeDB;
                if (oArgnsModel.ARGNS_MODEL_CSCollection.Count() > 0)
                    mNewLine.LineId = oArgnsModel.ARGNS_MODEL_CSCollection.Max(c => c.LineId) + 1;
                else
                    mNewLine.LineId = 1;
                mNewLine.U_CSCode = pObject.Name;
                mNewLine.U_Desc = pObject.U_Description;
                mNewLine.U_Selected = "N";

                ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS[] newLines = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS[oArgnsModel.ARGNS_MODEL_CSCollection.Length + 1];
                oArgnsModel.ARGNS_MODEL_CSCollection.CopyTo(newLines, 0);
                int positionNewLine = oArgnsModel.ARGNS_MODEL_CSCollection.Length;
                newLines[positionNewLine] = mNewLine;
                oArgnsModel.ARGNS_MODEL_CSCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS[newLines.Length];
                oArgnsModel.ARGNS_MODEL_CSCollection = newLines;

                oArgnsModelService.Update(oArgnsModel);

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }

        #endregion

        #region POM Master
        public List<ARGNSModelPom> GetModelPomList(CompanyConn pCompanyParam, string pModCode)
        {
            List<ARGNSModelPom> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {


                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                List<ARGNSModel> mModelList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mFilters = "Code eq '" + HttpUtility.UrlEncode(mModelList.SingleOrDefault().Code) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelPom", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelPom>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelPomList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<ARGNSModelPomTemplate> GetModelPomTemplateList(CompanyConn pCompanyParam, List<string> pSclcodeList, List<string> pPomCodeList)
        {
            try
            {
                List<ARGNSModelPomTemplate> result = null;
                string mUrl = string.Empty;
                string mFilters = "(";
                for (int i = 0; i < pSclcodeList.Count; i++)
                {
                    if (i == 0)
                        mFilters += "U_Scale eq " + pSclcodeList[i];
                    else
                        mFilters += " or U_Scale eq " + pSclcodeList[i];
                }
                mFilters += ") ";

                mFilters = " and (";
                for (int i = 0; i < pPomCodeList.Count; i++)
                {
                    if (i == 0)
                        mFilters += "U_CodeTmpl ne " + pPomCodeList[i];
                    else
                        mFilters += " or U_CodeTmpl ne " + pPomCodeList[i];
                }
                mFilters += ") ";


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PomTemplate", mFilters);
                result = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelPomTemplate>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelPomTemplateList :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSPomTemplateLines> GetModelPomTemplateLines(CompanyConn pCompanyParam, string pPOMCode)
        {
            try
            {
                List<ARGNSPomTemplateLines> result = null;
                List<C_ARGNS_POMTEMPLATE> mPomTemplateListAux = null;
                string mUrl = string.Empty;
                string mFilters = "Code eq '" + HttpUtility.UrlEncode(pPOMCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_POMTEMPLNS", mFilters);
                result = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPomTemplateLines>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PomTemplate", mFilters);
                mPomTemplateListAux = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<C_ARGNS_POMTEMPLATE>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());
                foreach (ARGNSPomTemplateLines mARGNSPomTemplateLinesAux in result)
                {
                    mARGNSPomTemplateLinesAux.U_PomCode = mPomTemplateListAux.Where(c => c.Code == mARGNSPomTemplateLinesAux.Code).FirstOrDefault().U_CodeTmpl;
                    mARGNSPomTemplateLinesAux.IsNew = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelPomTemplateLines :" + ex.Message);
                return null;
            }
        }

        #endregion

        #region Critical Path

        public List<ARGNSCrPath> GetCriticalPathListSearch(CompanyConn pCompanyParam, string pCollCode = "", string pSubCollCode = "", string pSeasonCode = "", string pModelCode = "", string pProjectCode = "", string pVendorCode = "", string pCustomerCode = "")
        {
            int SalesCode, ActivityCode;
            List<ARGNSCrPath> ret = null;
            string mUrl = string.Empty;
            string mFilters = "CardType eq 'S' and substringof(tolower('" + HttpUtility.UrlEncode(pVendorCode.ToUpper()) + "'), tolower(CardName)) ";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                List<string> mBPVen = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRD>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword))).Select(c => c.CardCode).ToList();
                mFilters = "CardType eq 'C' and substringof(tolower('" + HttpUtility.UrlEncode(pCustomerCode.ToUpper()) + "'), tolower(CardName)) ";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                List<string> mBPCust = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRD>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword))).Select(c => c.CardCode).ToList();
                mFilters = "1 eq 1 ";
                if (!string.IsNullOrEmpty(pProjectCode))
                    mFilters += "and substringof(tolower('" + HttpUtility.UrlEncode(pProjectCode.ToUpper()) + "'), tolower(U_Desc)) ";
                if (!string.IsNullOrEmpty(pModelCode))
                    mFilters += "and substringof(tolower('" + HttpUtility.UrlEncode(pModelCode.ToUpper()) + "'), tolower(ModelDesc)) ";
                if (!string.IsNullOrEmpty(pSeasonCode))
                    mFilters += "and substringof(tolower('" + HttpUtility.UrlEncode(pSeasonCode.ToUpper()) + "'), tolower(SeasonDesc)) ";
                if (!string.IsNullOrEmpty(pCollCode))
                    mFilters += "and substringof(tolower('" + HttpUtility.UrlEncode(pCollCode.ToUpper()) + "'), tolower(CollectionDesc)) ";
                if (!string.IsNullOrEmpty(pSubCollCode))
                    mFilters += "and substringof(tolower('" + HttpUtility.UrlEncode(pSubCollCode.ToUpper()) + "'), tolower(SubCollDesc)) ";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomizedCRPath", mFilters);
                ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCrPath>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                if (!string.IsNullOrEmpty(pVendorCode))
                    ret = ret.Where(c => mBPVen.Contains(c.U_Vendor)).ToList();
                if (!string.IsNullOrEmpty(pCustomerCode))
                    ret = ret.Where(c => mBPCust.Contains(c.U_Customer)).ToList();

                foreach (ARGNSCrPath item in ret)
                {
                    //Obtengo la Sales Order Asociada al Critical Path
                    if (!string.IsNullOrEmpty(item.SalesOrderNum))
                    {
                        SalesCode = Convert.ToInt32(item.SalesOrderNum);
                        mFilters = "DocNum eq " + SalesCode;
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrder", mFilters);
                        ORDR mSales = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ORDR>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword))).SingleOrDefault();
                        if (mSales != null)
                        {
                            item.SalesCardCode = mSales.CardCode;
                            item.SalesCardName = mSales.CardName;
                            item.SalesDelivDate = mSales.DocDueDate;
                            item.SalesPostingDate = mSales.DocDate;
                            item.SalesQty = mSales.DocTotal;
                        }
                    }

                    //Listo Todas las Acividades Del Critical Path Para Obtener Datos

                    mFilters = "Code eq '" + HttpUtility.UrlEncode(item.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CRPathAct", mFilters);
                    item.ActivitiesList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCrPathActivities>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    foreach (var Act in item.ActivitiesList)
                    {
                        if (!string.IsNullOrEmpty(Act.U_ActSAP))
                        {
                            ActivityCode = Convert.ToInt32(Act.U_ActSAP);
                            mFilters = "ClgCode eq " + ActivityCode;
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                            var mAct = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCLG>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword))).SingleOrDefault();
                            if (mAct != null)
                            {

                                string mFiltersOCLA = "statusID eq " + mAct.status;
                                string mUrlOCLA = RESTService.GetURL(pCompanyParam.UrlHana, "OCLA", mFiltersOCLA);
                                var mOCLA = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCLA>>(RESTService.GetRequestJson(mUrlOCLA, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword))).FirstOrDefault();

                                Act.Closed = mAct.Closed == "Y" ? true : false;

                                if (mOCLA != null)
                                {
                                    Act.U_Status = mOCLA.name;
                                }
                                else
                                {
                                    Act.U_Status = "";
                                }

                            }
                        }
                    }


                }


                return ret;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCriticalPathList :" + ex.Message);
                return null;
            }
        }

        #endregion

        #region Projects

        public ARGNSProject GetProjectByCode(CompanyConn pCompanyParam, string pCode, string pModelId)
        {
            try
            {
                ARGNSProject mProject = null;
                string mUrl = string.Empty;
                string mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CRPath", mFilters);
                mProject = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSProject>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault());
                if (mProject != null)
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CRPathAct", mFilters);
                    mProject.ActivitiesList = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCrPathActivities>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());
                    foreach (ARGNSCrPathActivities mCRActivity in mProject.ActivitiesList)
                    {
                        if (!string.IsNullOrEmpty(mCRActivity.U_ActSAP))
                        {
                            mFilters = "ClgCode eq " + HttpUtility.UrlEncode(mCRActivity.U_ActSAP);
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                            mCRActivity.ActivitySAP = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault());
                        }
                    }
                }
                else
                {
                    mProject = new ARGNSProject();
                }

                mFilters = "U_Active eq 'Y'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNSRouting", mFilters);
                mProject.RoutingList = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSRouting>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Department");
                mProject.DepartmentList = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<Department>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OHTY");
                mProject.RolSAPList = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<RolSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());
                mFilters = "Code eq '" + HttpUtility.UrlEncode(pModelId) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                C_ARGNS_MODEL mModel = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<C_ARGNS_MODEL>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault());
                if (mModel != null)
                {
                    mProject.U_Pic = mModel.U_Pic;
                    mProject.u_ModelDesc = mModel.U_ModDesc;
                }
                return mProject;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetProjectByCode :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSRoutingLine> GetWorkflowLines(CompanyConn pCompanyParam, string pWorkflow)
        {
            try
            {
                List<ARGNSRoutingLine> mWorkflowLines = null;
                string mUrl = string.Empty;
                string mFilters = "Code eq '" + HttpUtility.UrlEncode(pWorkflow) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNSRoutingLine", mFilters);
                mWorkflowLines = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSRoutingLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList());

                return mWorkflowLines;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetWorkflowLines :" + ex.Message);
                return null;
            }
        }

        public string GetCriticalPathDefaultBP(CompanyConn pCompanyParam)
        {
            try
            {
                ARGNSAprtxSetup mSetup = null;
                string mUrl = string.Empty;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ApparelSetup");
                mSetup = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSAprtxSetup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault());

                return mSetup.U_BPDef;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetCriticalPathDefaultBP :" + ex.Message);
                return null;
            }
        }

        public string UpdateActivityCodeInProjectLines(CompanyConn pCompanyParam, Dictionary<string, string> pActivityCodeList, string pProjectCode)
        {
            try
            {
                ARGNSProjectServiceWeb.ArgnsCriticalPath oProject = new ARGNSProjectServiceWeb.ArgnsCriticalPath();
                ARGNSProjectServiceWeb.ArgnsCriticalPathParams oProjectParams = new ARGNSProjectServiceWeb.ArgnsCriticalPathParams();
                ARGNSProjectServiceWeb.MsgHeader oProjectMsgHeader = new ARGNSProjectServiceWeb.MsgHeader();

                oProjectMsgHeader.ServiceName = ARGNSProjectServiceWeb.MsgHeaderServiceName.ARGNS_CRPATH;
                oProjectMsgHeader.ServiceNameSpecified = true;
                oProjectMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSProjectServiceWeb.ARGNS_CRPATH oProjectService = new ARGNSProjectServiceWeb.ARGNS_CRPATH();
                oProjectService.MsgHeaderValue = oProjectMsgHeader;


                oProjectParams.Code = pProjectCode;
                oProject = oProjectService.GetByParams(oProjectParams);

                #region Activity List

                if (oProject.ARGNS_CRPATHACTCollection.Count() > 0)
                {
                    foreach (var mActivitySAP in oProject.ARGNS_CRPATHACTCollection)
                    {
                        mActivitySAP.U_ActSAP = pActivityCodeList["LineId_" + mActivitySAP.LineId];
                    }
                }

                #endregion

                oProjectService.Update(oProject);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateActivityCodeInProjectLines :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string UpdateProject(CompanyConn pCompanyParam, ARGNSProject pObject)
        {
            try
            {
                ARGNSProjectServiceWeb.ArgnsCriticalPath oProject = new ARGNSProjectServiceWeb.ArgnsCriticalPath();
                ARGNSProjectServiceWeb.ArgnsCriticalPathParams oProjectParams = new ARGNSProjectServiceWeb.ArgnsCriticalPathParams();
                ARGNSProjectServiceWeb.MsgHeader oProjectMsgHeader = new ARGNSProjectServiceWeb.MsgHeader();

                oProjectMsgHeader.ServiceName = ARGNSProjectServiceWeb.MsgHeaderServiceName.ARGNS_CRPATH;
                oProjectMsgHeader.ServiceNameSpecified = true;
                oProjectMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSProjectServiceWeb.ARGNS_CRPATH oProjectService = new ARGNSProjectServiceWeb.ARGNS_CRPATH();
                oProjectService.MsgHeaderValue = oProjectMsgHeader;


                oProjectParams.Code = pObject.Code;
                oProject = oProjectService.GetByParams(oProjectParams);

                oProject.U_Desc = pObject.U_Desc;
                oProject.U_Status = pObject.U_Status;
                oProject.U_Calendar = pObject.U_Calendar;
                oProject.U_SDate = pObject.U_SDate.Value;
                oProject.U_SDateSpecified = true;

                #region Activity List

                if (pObject.ActivitiesList.Count > 0 || oProject.ARGNS_CRPATHACTCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mActivitySAP in oProject.ARGNS_CRPATHACTCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCrPathActivities mActivity = pObject.ActivitiesList.Where(c => c.LineId == mActivitySAP.LineId).FirstOrDefault();
                        if (mActivity != null)
                        {
                            mActivitySAP.U_Depart = mActivity.U_Depart;
                            mActivitySAP.U_Role = mActivity.U_Role;
                            mActivitySAP.U_Manager = mActivity.U_Manager;
                            mActivitySAP.U_User = mActivity.U_User;
                            mActivitySAP.U_Bp = mActivity.U_Bp;
                            if (mActivity.U_PlSDate.HasValue)
                            {
                                mActivitySAP.U_PlSDate = mActivity.U_PlSDate.Value;
                                mActivitySAP.U_PlSDateSpecified = true;
                            }
                            if (mActivity.U_PlCDate.HasValue)
                            {
                                mActivitySAP.U_PlCDate = mActivity.U_PlCDate.Value;
                                mActivitySAP.U_PlCDateSpecified = true;
                            }
                        }
                    }
                }

                #endregion

                oProjectService.Update(oProject);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateProject :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public JsonObjectResult AddProject(CompanyConn pCompanyParam, ARGNSProject pObject)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            try
            {
                ARGNSProjectServiceWeb.ArgnsCriticalPath oProject = new ARGNSProjectServiceWeb.ArgnsCriticalPath();
                ARGNSProjectServiceWeb.ArgnsCriticalPathParams oProjectParams = new ARGNSProjectServiceWeb.ArgnsCriticalPathParams();
                ARGNSProjectServiceWeb.MsgHeader oProjectMsgHeader = new ARGNSProjectServiceWeb.MsgHeader();

                oProjectMsgHeader.ServiceName = ARGNSProjectServiceWeb.MsgHeaderServiceName.ARGNS_CRPATH;
                oProjectMsgHeader.ServiceNameSpecified = true;
                oProjectMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSProjectServiceWeb.ARGNS_CRPATH oProjectService = new ARGNSProjectServiceWeb.ARGNS_CRPATH();
                oProjectService.MsgHeaderValue = oProjectMsgHeader;

                oProject.Code = GetLastProjectCode(pCompanyParam);
                oProject.U_Desc = pObject.U_Desc;
                oProject.U_Model = pObject.U_Model;
                oProject.U_Workflow = pObject.U_Workflow;
                oProject.U_Status = pObject.U_Status;
                oProject.U_Calendar = pObject.U_Calendar;
                oProject.U_Planning = pObject.U_Planning;
                oProject.U_SDate = pObject.U_SDate.Value;
                oProject.U_SDateSpecified = true;
                oProject.U_SalesO = pObject.U_SalesO;
                oProject.U_ProyCode = pObject.ModelID + "-" + pObject.U_Model + "-" + oProject.Code;

                #region Materials

                if (pObject.ActivitiesList.Count > 0)
                {
                    ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT[] newLines = new ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT[pObject.ActivitiesList.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCrPathActivities mActivity in pObject.ActivitiesList.ToList())
                    {
                        ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT lineActivity = new ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT();
                        lineActivity.LineId = mActivity.LineId;
                        lineActivity.LineIdSpecified = true;

                        lineActivity.U_Updated = mActivity.U_Updated;
                        lineActivity.U_PhaseId = mActivity.U_PhaseId;
                        lineActivity.U_Desc = mActivity.U_Desc;
                        lineActivity.U_Depart = mActivity.U_Depart;
                        lineActivity.U_Role = mActivity.U_Role;
                        lineActivity.U_Manager = mActivity.U_Manager;
                        lineActivity.U_User = mActivity.U_User;
                        lineActivity.U_Bp = mActivity.U_Bp;
                        lineActivity.U_ActSAP = mActivity.U_ActSAP;
                        if (mActivity.U_PlanLead.HasValue)
                        {
                            lineActivity.U_PlanLead = (double)mActivity.U_PlanLead;
                            lineActivity.U_PlanLeadSpecified = true;
                            lineActivity.U_DelivDay = (double)mActivity.U_PlanLead;
                            lineActivity.U_DelivDaySpecified = true;
                        }
                        if (mActivity.U_PlSDate.HasValue)
                        {
                            lineActivity.U_PlSDate = mActivity.U_PlSDate.Value;
                            lineActivity.U_PlSDateSpecified = true;
                            lineActivity.U_SDate = mActivity.U_PlSDate.Value;
                            lineActivity.U_SDateSpecified = true;
                        }
                        if (mActivity.U_PlCDate.HasValue)
                        {
                            lineActivity.U_PlCDate = mActivity.U_PlCDate.Value;
                            lineActivity.U_PlCDateSpecified = true;
                            lineActivity.U_CDate = mActivity.U_PlCDate.Value;
                            lineActivity.U_CDateSpecified = true;
                        }

                        newLines[positionNewLine] = lineActivity;
                        positionNewLine += 1;
                    }
                    oProject.ARGNS_CRPATHACTCollection = new ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT[newLines.Length];
                    oProject.ARGNS_CRPATHACTCollection = newLines;
                }

                #endregion

                oProjectParams = oProjectService.Add(oProject);
                mJsonObjectResult.Code = oProjectParams.Code;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public string GetLastProjectCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {

                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CRPath", "", mOthers);
                List<ARGNSCrPath> listResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCrPath>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mMaxVal = Convert.ToInt32(listResult.SingleOrDefault().Code) + 1;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastProjectCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        #endregion

        #region Samples

        public List<ARGNSModelSample> GetModelSapmples(CompanyConn pCompanyParam, string pModCode)
        {
            List<ARGNSModelSample> mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Samples", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelSample>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User");
                List<OUSR> listUser = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUSR>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                foreach (ARGNSModelSample sampleAux in mRet)
                {
                    OUSR userAux = listUser.Where(j => j.USERID.ToString() == sampleAux.U_User).FirstOrDefault();
                    sampleAux.USER_CODE = userAux.USER_CODE;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetModelSapmples :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public ARGNSModelSample GetSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode)
        {
            ARGNSModelSample mRet = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {


                mFilters = "Code eq '" + HttpUtility.UrlEncode(pSampleCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Samples", mFilters);
                List<ARGNSModelSample> mSampList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelSample>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mSampList.SingleOrDefault();
                if (mRet == null)
                {
                    mRet = new ARGNSModelSample();
                }
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SampEval");
                mRet.ListSampleVal = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNS_SampleVal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User");
                mRet.ListUser = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PomTemplate");
                mRet.ListPOM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelPomTemplate>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                List<ARGNSModel> mModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mFilters = "Code eq '" + HttpUtility.UrlEncode(mModel.Single().Code) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelSize", mFilters);
                mRet.ListScale = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSScale>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet.ListScale = mRet.ListScale.GroupBy(c => c.U_SclCode).Select(mgroup => mgroup.First()).ToList();

                mFilters = "Code eq '" + pSampleCode + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SamplesLines", mFilters);
                mRet.ListLine = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelSampleLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSample :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public ARGNSModelSample GetSampleByCodSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode)
        {
            List<ARGNSModelSample> listResult = null;
            string mUrl = string.Empty;
            string mFilters;
            try
            {

                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "' and U_SampCode eq '" + HttpUtility.UrlEncode(pSampleCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Samples", mFilters);
                listResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelSample>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSampleByCodSample :" + ex.Message);
                return null;
            }
            return listResult.SingleOrDefault();
        }

        public string GetLastSampleCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {

                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Samples", "", mOthers);
                List<ARGNSModelSample> listResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelSample>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mMaxVal = listResult.SingleOrDefault().DocEntry + 1;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastSampleCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        public string AddSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            ARGNSSampleServiceWeb.ArgnsStyleSample oSample = new ARGNSSampleServiceWeb.ArgnsStyleSample();
            ARGNSSampleServiceWeb.MsgHeader oSampleMsgHeader = new ARGNSSampleServiceWeb.MsgHeader();

            oSampleMsgHeader.ServiceName = ARGNSSampleServiceWeb.MsgHeaderServiceName.ARGNS_SAMPLES;
            oSampleMsgHeader.ServiceNameSpecified = true;
            oSampleMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSSampleServiceWeb.ARGNS_SAMPLES oSampleServiceWeb = new ARGNSSampleServiceWeb.ARGNS_SAMPLES();
            oSampleServiceWeb.MsgHeaderValue = oSampleMsgHeader;
            try
            {
                oSample.U_DateSpecified = true;
                oSample.U_ApproDateSpecified = true;

                oSample.Code = GetLastSampleCode(pCompanyParam);
                oSample.U_ModCode = pObject.U_ModCode;
                oSample.U_Type = pObject.U_Type;
                oSample.U_SampCode = pObject.U_SampCode;
                oSample.U_Status = pObject.U_Status;
                oSample.U_SclCode = pObject.U_SclCode;
                oSample.U_POM = pObject.U_POM;
                oSample.U_Date = pObject.U_Date;
                oSample.U_User = pObject.U_User;
                oSample.U_ApproDate = pObject.U_ApproDate;
                oSample.U_Active = pObject.U_Active;
                oSample.U_Comments = pObject.U_Comments;
                oSample.U_BPCustomer = pObject.U_BPCustomer;
                oSample.U_BPSupp = pObject.U_BPSupp;
                oSample.U_Picture = pObject.U_Picture;

                int counter = 0;
                if (pObject.ListLine.Count > 0)
                {
                    oSample.ARGNS_SAMPLESLINESCollection = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES[pObject.ListLine.Count];
                    foreach (ARGNSModelSampleLine item in pObject.ListLine)
                    {
                        oSample.ARGNS_SAMPLESLINESCollection[counter] = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES();

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineIdSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_TargetSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_ActualSpecified = true;

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineId = item.LineId;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeCode = item.U_SizeCode;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Desc = item.U_Desc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_POM = item.U_POM;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeDesc = item.U_SizeDesc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Actual = (double)item.U_Actual;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Target = (double)item.U_Target;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Revision = item.U_Revision;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SclCode = item.U_SclCode;
                        counter++;
                    }
                }


                oSampleServiceWeb.Add(oSample);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataSL -> AddSample :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string UpdateSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            ARGNSSampleServiceWeb.ArgnsStyleSample oSample = new ARGNSSampleServiceWeb.ArgnsStyleSample();
            ARGNSSampleServiceWeb.ArgnsStyleSampleParams oSampleParams = new ARGNSSampleServiceWeb.ArgnsStyleSampleParams();
            ARGNSSampleServiceWeb.MsgHeader oSampleMsgHeader = new ARGNSSampleServiceWeb.MsgHeader();

            oSampleMsgHeader.ServiceName = ARGNSSampleServiceWeb.MsgHeaderServiceName.ARGNS_SAMPLES;
            oSampleMsgHeader.ServiceNameSpecified = true;
            oSampleMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSSampleServiceWeb.ARGNS_SAMPLES oSampleServiceWeb = new ARGNSSampleServiceWeb.ARGNS_SAMPLES();
            oSampleServiceWeb.MsgHeaderValue = oSampleMsgHeader;

            try
            {
                oSampleParams.Code = pObject.Code;
                oSample = oSampleServiceWeb.GetByParams(oSampleParams);

                oSample.U_DateSpecified = true;
                oSample.U_ApproDateSpecified = true;

                oSample.U_ModCode = pObject.U_ModCode;
                oSample.U_Type = pObject.U_Type;
                oSample.U_SampCode = pObject.U_SampCode;
                oSample.U_Status = pObject.U_Status;
                oSample.U_SclCode = pObject.U_SclCode;
                oSample.U_POM = pObject.U_POM;
                oSample.U_Date = pObject.U_Date;
                oSample.U_User = pObject.U_User;
                oSample.U_ApproDate = pObject.U_ApproDate;
                oSample.U_Active = pObject.U_Active;
                oSample.U_Comments = pObject.U_Comments;
                oSample.U_BPCustomer = pObject.U_BPCustomer;
                oSample.U_BPSupp = pObject.U_BPSupp;
                oSample.U_Picture = pObject.U_Picture;

                //oSample.ARGNS_SAMPLESLINESCollection = null;
                //oSampleServiceWeb.Update(oSample);

                int counter = 0;
                if (pObject.ListLine.Count > 0)
                {

                    oSample.ARGNS_SAMPLESLINESCollection = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES[pObject.ListLine.Count];
                    foreach (ARGNSModelSampleLine item in pObject.ListLine)
                    {
                        oSample.ARGNS_SAMPLESLINESCollection[counter] = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES();

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineIdSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_TargetSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_ActualSpecified = true;

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineId = counter + 1;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeCode = item.U_SizeCode;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Desc = item.U_Desc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_POM = item.U_POM;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeDesc = item.U_SizeDesc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Target = (double)item.U_Target;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Actual = (double)item.U_Actual;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Revision = item.U_Revision;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SclCode = item.U_SclCode;
                        counter++;
                    }
                }

                oSampleServiceWeb.Update(oSample);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataSL -> UpdateSample :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region PDC

        public List<ARGNSPDC> GetPDCList(CompanyConn pCompanyParam, ref PDCSAPCombo combo, DateTime? pPD, string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "")
        {
            List<ARGNSPDC> mRet = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {



                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shifts");
                combo.ListShifts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSShifts>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP");
                combo.ListBusinessPartner = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                combo.ListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


                if (pPD != null)
                {
                    mFilters = mFilters + " and U_Date eq '" + pPD + "'";
                }

                if (!string.IsNullOrEmpty(pCode))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pCode + "'), tolower(Code))";
                }

                if (!string.IsNullOrEmpty(pShift) && pShift != "-1")
                {
                    mFilters = mFilters + " and U_ShiftCode eq '" + HttpUtility.UrlEncode(pShift) + "'";
                }

                if (!string.IsNullOrEmpty(pEmployee) && pEmployee != "-1")
                {
                    mFilters = mFilters + " and U_empID eq " + HttpUtility.UrlEncode(pEmployee) + "";
                }

                if (!string.IsNullOrEmpty(pVendor))
                {
                    mFilters = mFilters + " and U_Vendor eq '" + HttpUtility.UrlEncode(pVendor) + "'";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PridDataCol", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPDC>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPDCList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public ARGNSPDC GetPDCById(CompanyConn pCompanyParam, string pPDCCode, ref PDCSAPCombo combo)
        {
            ARGNSPDC mRet = null;
            string mUrl = string.Empty;
            string mFilters = "";
            try
            {


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shifts");
                combo.ListShifts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSShifts>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP");
                combo.ListBusinessPartner = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                combo.ListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "WorkCenterPro");
                combo.ListWorkCenter = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSWorkCenter>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mRet = new ARGNSPDC();
                if (Convert.ToUInt32(pPDCCode) != 0)
                {
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(pPDCCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PridDataCol", mFilters);
                    List<ARGNSPDC> mPdcList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPDC>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mRet = mPdcList.SingleOrDefault();

                    mFilters = "Code eq '" + HttpUtility.UrlEncode(pPDCCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ProdColLns", mFilters);
                    mRet.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPDCLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPDCById :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public PDCSAPCombo GetPDCCombo(CompanyConn pCompanyParam)
        {
            PDCSAPCombo mRet = null;
            string mUrl = string.Empty;
            try
            {

                mRet = new PDCSAPCombo();
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shifts");
                mRet.ListShifts = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSShifts>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP");
                mRet.ListBusinessPartner = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                mRet.ListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPDCCombo :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public ARGNSPDCLine GetPDCLineBarCode(CompanyConn pCompanyParam, string CodeBar)
        {
            ARGNSPDCLine mPDCLineReturn = null;
            string mUrl = string.Empty;
            string mFilters = "";
            try
            {


                mFilters = "U_CodeBars eq '" + HttpUtility.UrlEncode(CodeBar) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ProdColLns", mFilters);
                List<C_ARGNS_PRODCOLLNS> listAuxARGNS_PRODCOLLNS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<C_ARGNS_PRODCOLLNS>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                decimal? sumBadQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_BadQty);
                decimal? sumQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_Qty);

                mFilters = "U_BarCode eq '" + HttpUtility.UrlEncode(CodeBar) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomPDC", mFilters);
                List<ARGNSPDCLine> mPDCLineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPDCLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mPDCLineReturn = mPDCLineList.SingleOrDefault();

                if (mPDCLineReturn != null)
                {

                    mPDCLineReturn.U_Qty = (((sumQty + sumBadQty) > 0) ? Convert.ToDecimal(mPDCLineReturn.U_IssQty) - (sumQty + sumBadQty) : Convert.ToDecimal(mPDCLineReturn.U_IssQty)).ToString();

                    mFilters = "DocEntry eq " + HttpUtility.UrlEncode(mPDCLineReturn.U_ProdOrd) + " and U_ItemCode eq '" + HttpUtility.UrlEncode(mPDCLineReturn.U_OperCode) + "' ";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomPDCResource", mFilters);
                    List<ARGNSPDCResource> mPDCResourceList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPDCResource>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    ARGNSPDCResource mPDCResource = mPDCResourceList.SingleOrDefault();

                    mPDCLineReturn.U_WorkCtr = mPDCResource.U_WorkCePrCode;
                    mPDCLineReturn.U_ResourID = mPDCResource.U_ResCode;
                    mPDCLineReturn.U_ResName = mPDCResource.U_ResName;
                    mPDCLineReturn.U_CodeBars = CodeBar;
                    mPDCLineReturn.U_BadQty = "0";
                }


            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPDCLineBarCode :" + ex.Message);
                return null;
            }
            return mPDCLineReturn;
        }

        public List<ARGNSPDCLine> GetPDCLineCutTick(CompanyConn pCompanyParam, string CutTick)
        {
            List<ARGNSPDCLine> mPDCListReturn = null;
            string mUrl = string.Empty;
            string mFilters = "";
            try
            {


                mFilters = "U_ProdOrd eq " + CutTick + "";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomPDC", mFilters);
                mPDCListReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPDCLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                foreach (ARGNSPDCLine line in mPDCListReturn)
                {

                    mFilters = "U_CodeBars eq '" + HttpUtility.UrlEncode(line.U_CodeBars) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ProdColLns", mFilters);
                    List<C_ARGNS_PRODCOLLNS> listAuxARGNS_PRODCOLLNS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<C_ARGNS_PRODCOLLNS>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    decimal? sumBadQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_BadQty);
                    decimal? sumQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_Qty);

                    mFilters = "DocEntry eq " + line.U_ProdOrd + " and U_ItemCode eq '" + HttpUtility.UrlEncode(line.U_OperCode) + "' ";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CustomPDCResource", mFilters);
                    List<ARGNSPDCResource> mPDCResourceList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPDCResource>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    ARGNSPDCResource mPDCResource = mPDCResourceList.SingleOrDefault();

                    line.U_WorkCtr = mPDCResource.U_WorkCePrCode;
                    line.U_ResourID = mPDCResource.U_ResCode;
                    line.U_ResName = mPDCResource.U_ResName;
                    line.U_BadQty = "0";
                    line.U_Qty = (((sumQty + sumBadQty) > 0) ? Convert.ToDecimal(line.U_IssQty) - (sumQty + sumBadQty) : Convert.ToDecimal(line.U_IssQty)).ToString();
                }
                mPDCListReturn = mPDCListReturn.Where(c => Convert.ToDecimal(c.U_Qty) != 0).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPDCLineBarCode :" + ex.Message);
                return null;
            }
            return mPDCListReturn;
        }

        public string AddPDC(CompanyConn pCompanyParam, ARGNSPDC pPDC)
        {
            ARGNSPDCServiceWeb.ArgnsProdDataCollection oPDC = new ARGNSPDCServiceWeb.ArgnsProdDataCollection();
            ARGNSPDCServiceWeb.MsgHeader oPDCMsgHeader = new ARGNSPDCServiceWeb.MsgHeader();

            oPDCMsgHeader.ServiceName = ARGNSPDCServiceWeb.MsgHeaderServiceName.ARGNS_PROD_DATACOL;
            oPDCMsgHeader.ServiceNameSpecified = true;
            oPDCMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSPDCServiceWeb.ARGNS_PROD_DATACOL oPDCServiceWeb = new ARGNSPDCServiceWeb.ARGNS_PROD_DATACOL();
            oPDCServiceWeb.MsgHeaderValue = oPDCMsgHeader;
            try
            {

                oPDC.Code = GetLastPDCCode(pCompanyParam);
                oPDC.Canceled = "N";
                oPDC.Object = "ARGNS_PROD_DATACOL";
                oPDC.CreateDate = System.DateTime.Now;
                oPDC.DataSource = "I";
                oPDC.Transfered = "N";
                oPDC.U_empID = (long)pPDC.U_empID;
                oPDC.U_empIDSpecified = true;
                oPDC.U_ShiftCode = pPDC.U_ShiftCode;
                oPDC.U_Vendor = pPDC.U_Vendor;
                if (pPDC.U_Date != null)
                {
                    oPDC.U_Date = pPDC.U_Date.Value;

                }
                oPDC.U_DateSpecified = true;

                if (pPDC.U_UnprodMins != null && !string.IsNullOrEmpty(pPDC.U_UnprodMins))
                {
                    oPDC.U_UnprodMins = Convert.ToInt32(pPDC.U_UnprodMins);
                    oPDC.U_UnprodMinsSpecified = true;
                }
                if (pPDC.U_MinPres != null && !string.IsNullOrEmpty(pPDC.U_MinPres))
                {
                    oPDC.U_MinPres = Convert.ToInt32(pPDC.U_MinPres);
                    oPDC.U_MinPresSpecified = true;
                }

                int counter = 0;
                if (pPDC.Lines.Count > 0)
                {
                    oPDC.ARGNS_PRODCOLLNSCollection = new ARGNSPDCServiceWeb.ArgnsProdDataCollectionARGNS_PRODCOLLNS[pPDC.Lines.Count];
                    foreach (ARGNSPDCLine item in pPDC.Lines)
                    {
                        oPDC.ARGNS_PRODCOLLNSCollection[counter] = new ARGNSPDCServiceWeb.ArgnsProdDataCollectionARGNS_PRODCOLLNS();
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].LineId = Convert.ToInt32(item.LineId);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].LineIdSpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].Object = "ARGNS_PROD_DATACOL";
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_BadQty = Convert.ToDouble(item.U_BadQty);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_BadQtySpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_CodeBars = item.U_CodeBars;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Color = item.U_Color;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Model = item.U_Model;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_OperCode = item.U_OperCode;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_ProdOrd = Convert.ToUInt32(item.U_ProdOrd);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_ProdOrdSpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Qty = Convert.ToDouble(item.U_Qty);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_QtySpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Scale = item.U_Scale;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Size = item.U_Size;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Stage = Convert.ToUInt32(item.U_Stage);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_StageSpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Variable = item.U_Variable;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_WorkCtr = item.U_WorkCtr;
                        counter++;
                    }
                }
                oPDCServiceWeb.Add(oPDC);

                ARGNSSendOutServiceWeb.ArgnsSendOut oSendOut = null;
                ARGNSSendOutServiceWeb.ArgnsSendOutParams oSendOutParams = new ARGNSSendOutServiceWeb.ArgnsSendOutParams();
                ARGNSSendOutServiceWeb.MsgHeader oSendOutMsgHeader = new ARGNSSendOutServiceWeb.MsgHeader();

                oSendOutMsgHeader.ServiceName = ARGNSSendOutServiceWeb.MsgHeaderServiceName.ARGNS_SENDOUT_STG;
                oSendOutMsgHeader.ServiceNameSpecified = true;
                oSendOutMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSSendOutServiceWeb.ARGNS_SENDOUT_STG oSendOutServiceWeb = new ARGNSSendOutServiceWeb.ARGNS_SENDOUT_STG();
                oSendOutServiceWeb.MsgHeaderValue = oSendOutMsgHeader;

                foreach (ARGNSPDCLine item in pPDC.Lines)
                {
                    oSendOut = new ARGNSSendOutServiceWeb.ArgnsSendOut();
                    int auxDocEntry = GetSendOutCode(pCompanyParam, item.U_CodeBars);
                    oSendOutParams.DocEntry = auxDocEntry;
                    oSendOutParams.DocEntrySpecified = true;
                    oSendOut = oSendOutServiceWeb.GetByParams(oSendOutParams);

                    ARGNSSendOutServiceWeb.ArgnsSendOutARGNS_SENDOUT_PAR auxPAR = oSendOut.ARGNS_SENDOUT_PARCollection.Where(c => c.U_BarCode == item.U_CodeBars).FirstOrDefault();
                    long auxProdOrderNum = auxPAR.U_ProdOrder;
                    auxPAR.U_RecQty = auxPAR.U_RecQty + Convert.ToDouble(item.U_Qty);
                    auxPAR.U_BadQty = auxPAR.U_BadQty + Convert.ToDouble(item.U_BadQty);

                    oSendOutServiceWeb.Update(oSendOut);

                    //Si tengo Bad Qty debo restarle dicha cantidad a las Issue Qty a la operacion siguiente (DocEntry +1 y ProdOrder igual a la linea donde estoy parado)
                    if (Convert.ToDouble(item.U_BadQty) > 0)
                    {
                        oSendOut = new ARGNSSendOutServiceWeb.ArgnsSendOut();
                        oSendOutParams.DocEntry = auxDocEntry + 1;
                        oSendOutParams.DocEntrySpecified = true;
                        try
                        {
                            oSendOut = oSendOutServiceWeb.GetByParams(oSendOutParams);
                        }
                        catch (Exception)
                        {
                            oSendOut = null;
                        }

                        if (oSendOut != null)
                        {
                            auxPAR = null;
                            auxPAR = oSendOut.ARGNS_SENDOUT_PARCollection.Where(c => c.U_ProdOrder == auxProdOrderNum).FirstOrDefault();
                            if (auxPAR != null)
                            {
                                auxPAR.U_IssQty = auxPAR.U_IssQty - Convert.ToDouble(item.U_BadQty);
                            }
                            oSendOutServiceWeb.Update(oSendOut);
                        }
                    }
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataService -> PDC :" + ex.Message);
                return "Error:" + ex.Message; ;
            }
        }

        public string GetLastPDCCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {

                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PridDataCol", "", mOthers);
                List<ARGNSModelSample> listResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelSample>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mMaxVal = listResult.SingleOrDefault().DocEntry + 1;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastSampleCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        public int GetSendOutCode(CompanyConn pCompanyParam, string pBarCode)
        {
            string mUrl = string.Empty;
            string mFilters;
            int mMaxVal;
            try
            {

                mFilters = "U_BarCode eq '" + HttpUtility.UrlEncode(pBarCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SendoutPar", mFilters);
                List<ARGNSSendOutPar> listResult = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSendOutPar>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mMaxVal = listResult.SingleOrDefault().DocEntry;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetSendOutCode :" + ex.Message);
                return 0;
            }
            return mMaxVal;
        }

        #endregion

        #region Model LOGs

        public List<ARGNSModelHistory> GetModelHistoryList(CompanyConn pCompanyParam, string pModelCode = "")
        {
            string mSqlQuery;
            List<ARGNSModelHistory> ret = null;
            try
            {
                mSqlQuery = String.Format("SELECT T0.\"Code\",T0.\"LogInst\",T0.\"UpdateDate\", IFNULL((Select T1.\"U_NAME\" From \"{0}\".\"OUSR\" T1 Where T1.\"USERID\"=T0.\"UserSign\"),'manager') as \"UserSignName\"  FROM  \"{0}\".\"@AARGNS_MODEL\" T0 WHERE T0.\"Code\" = '" + pModelCode + "' AND  T0.\"Object\" = 'ARGNS_MODEL'", pCompanyParam.CompanyDB);
                UserSL pUserServ = new UserSL();
                ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelHistory>>(pUserServ.GetQueryResult(pCompanyParam, mSqlQuery));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelHistoryList :" + ex.Message);
                ret = new List<ARGNSModelHistory>();
            }

            return ret;
        }

        public List<SAPLogs> GetSAPModelLogs(CompanyConn pCompanyParam, string pModelCode = "", string pLogInst = "")
        {
            string mSqlQuery;
            List<ARGNSModel> mRet = null;
            List<ARGNSModel> mretAux = null;
            List<SAPLogs> mResult = null;
            try
            {
                UserSL pUserServ = new UserSL();
                ArrayList mInstList = new ArrayList(pLogInst.Split(',').ToArray());
                mResult = new List<SAPLogs>();

                if (mInstList.Count > 1)
                {

                    mSqlQuery = "select  T0.\"Code\", T0.\"Name\", T0.\"DocEntry\", T0.\"LogInst\", T0.\"UserSign\", T0.\"UpdateDate\", T0.\"UpdateTime\", " +
                                          " T0.\"U_ATGrp\", T0.\"U_ModCode\", T0.\"U_ModDesc\", T0.\"U_InvItem\", T0.\"U_SalItem\", T0.\"U_PurItem\", T0.\"U_SSDate\", T0.\"U_SCDate\", T0.\"U_Year\", T0.\"U_COO\",T0.\"U_Active\", " +
                                           "T0.\"U_FixAsset\", T0.\"U_SapGrp\", T0.\"U_PList\", T0.\"U_Division\", T0.\"U_Season\", T0.\"U_FrgnDesc\", T0.\"U_ModGrp\", T0.\"U_StyleTyp\", T0.\"U_SclCode\", T0.\"U_LineCode\", T0.\"U_Vendor\", T0.\"U_MainWhs\", " +
                                           "T0.\"U_Comments\", T0.\"U_BOM\", T0.\"U_Owner\", T0.\"U_Approved\", T0.\"U_IsEE\", T0.\"U_Price\", T0.\"U_WhsNewI\", T0.\"U_CollCode\", T0.\"U_AmbCode\", T0.\"U_CompCode\", T0.\"U_Brand\", T0.\"U_RoutCode\", " +
                                           "T0.\"U_SclPOM\", T0.\"U_CodePOM\", T0.\"U_ChartCod\", T0.\"U_Designer\", T0.\"U_Customer\", T0.\"U_GrpSCod\", T0.\"U_Currency\", T0.\"U_Status\", T0.\"U_RMaterial\", T0.\"U_InsChart\", T0.\"U_DocNumb\", T0.\"U_BinCode\",  " +
                                           "T0.\"U_SkuGen\", T0.\"U_DropCode\", T0.\"U_MenType\"  " +
                                           "from \"{0}\".\"@AARGNS_MODEL\"T0 WHERE T0.\"LogInst\" = " + mInstList[0] + "  AND  T0.\"Code\" = '" + pModelCode + "'  AND  T0.\"Object\" = 'ARGNS_MODEL'";
                    mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(pUserServ.GetQueryResult(pCompanyParam, String.Format(mSqlQuery, pCompanyParam.CompanyDB)));


                    mSqlQuery = "select  T0.\"Code\", T0.\"Name\", T0.\"DocEntry\", T0.\"LogInst\", T0.\"UserSign\", T0.\"UpdateDate\", T0.\"UpdateTime\", " +
                                   " T0.\"U_ATGrp\", T0.\"U_ModCode\", T0.\"U_ModDesc\", T0.\"U_InvItem\", T0.\"U_SalItem\", T0.\"U_PurItem\", T0.\"U_SSDate\", T0.\"U_SCDate\", T0.\"U_Year\", T0.\"U_COO\",T0.\"U_Active\", " +
                                    "T0.\"U_FixAsset\", T0.\"U_SapGrp\", T0.\"U_PList\", T0.\"U_Division\", T0.\"U_Season\", T0.\"U_FrgnDesc\", T0.\"U_ModGrp\", T0.\"U_StyleTyp\", T0.\"U_SclCode\", T0.\"U_LineCode\", T0.\"U_Vendor\", T0.\"U_MainWhs\", " +
                                    "T0.\"U_Comments\", T0.\"U_BOM\", T0.\"U_Owner\", T0.\"U_Approved\", T0.\"U_IsEE\", T0.\"U_Price\", T0.\"U_WhsNewI\", T0.\"U_CollCode\", T0.\"U_AmbCode\", T0.\"U_CompCode\", T0.\"U_Brand\", T0.\"U_RoutCode\", " +
                                    "T0.\"U_SclPOM\", T0.\"U_CodePOM\", T0.\"U_ChartCod\", T0.\"U_Designer\", T0.\"U_Customer\", T0.\"U_GrpSCod\", T0.\"U_Currency\", T0.\"U_Status\", T0.\"U_RMaterial\", T0.\"U_InsChart\", T0.\"U_DocNumb\", T0.\"U_BinCode\",  " +
                                    "T0.\"U_SkuGen\", T0.\"U_DropCode\", T0.\"U_MenType\"  " +
                                    "from \"{0}\".\"@AARGNS_MODEL\"T0 WHERE T0.\"LogInst\" = " + mInstList[1] + "  AND  T0.\"Code\" = '" + pModelCode + "'  AND  T0.\"Object\" = 'ARGNS_MODEL'";
                    mretAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(pUserServ.GetQueryResult(pCompanyParam, String.Format(mSqlQuery, pCompanyParam.CompanyDB)));

                    Compare(mRet.SingleOrDefault(), mretAux.SingleOrDefault(), Convert.ToInt32(mInstList[1]), ref mResult);
                }
                else
                {

                    mSqlQuery = "select  T0.\"Code\", T0.\"Name\", T0.\"DocEntry\", T0.\"LogInst\", T0.\"UserSign\", T0.\"UpdateDate\", T0.\"UpdateTime\", " +
                                  " T0.\"U_ATGrp\", T0.\"U_ModCode\", T0.\"U_ModDesc\", T0.\"U_InvItem\", T0.\"U_SalItem\", T0.\"U_PurItem\", T0.\"U_SSDate\", T0.\"U_SCDate\", T0.\"U_Year\", T0.\"U_COO\",T0.\"U_Active\", " +
                                   "T0.\"U_FixAsset\", T0.\"U_SapGrp\", T0.\"U_PList\", T0.\"U_Division\", T0.\"U_Season\", T0.\"U_FrgnDesc\", T0.\"U_ModGrp\", T0.\"U_StyleTyp\", T0.\"U_SclCode\", T0.\"U_LineCode\", T0.\"U_Vendor\", T0.\"U_MainWhs\", " +
                                   "T0.\"U_Comments\", T0.\"U_BOM\", T0.\"U_Owner\", T0.\"U_Approved\", T0.\"U_IsEE\", T0.\"U_Price\", T0.\"U_WhsNewI\", T0.\"U_CollCode\", T0.\"U_AmbCode\", T0.\"U_CompCode\", T0.\"U_Brand\", T0.\"U_RoutCode\", " +
                                   "T0.\"U_SclPOM\", T0.\"U_CodePOM\", T0.\"U_ChartCod\", T0.\"U_Designer\", T0.\"U_Customer\", T0.\"U_GrpSCod\", T0.\"U_Currency\", T0.\"U_Status\", T0.\"U_RMaterial\", T0.\"U_InsChart\", T0.\"U_DocNumb\", T0.\"U_BinCode\",  " +
                                   "T0.\"U_SkuGen\", T0.\"U_DropCode\", T0.\"U_MenType\"  " +
                                   "from \"{0}\".\"@AARGNS_MODEL\"T0 WHERE T0.\"LogInst\" >= " + mInstList[0] + "  AND  T0.\"Code\" = '" + pModelCode + "'  AND  T0.\"Object\" = 'ARGNS_MODEL' ORDER BY T0.\"LogInst\" ASC";
                    mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(pUserServ.GetQueryResult(pCompanyParam, String.Format(mSqlQuery, pCompanyParam.CompanyDB)));

                    int mCount = Convert.ToInt32(mInstList[0]);


                    for (int i = 0; i < mRet.Count - 1; i++)
                    {
                        mCount++;

                        if (i < mRet.Count - 1)
                        {
                            Compare(mRet[i], mRet[i + 1], mCount, ref mResult);
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetSAPModelLogs :" + ex.Message);
                mResult = null;
            }

            return mResult;
        }

        static void Compare<T>(T Object1, T object2, int pNum, ref List<SAPLogs> mResult)
        {
            //Get the type of the object
            Type type = typeof(T);
            SAPLogs mLog;

            //Loop through each properties inside class and get values for the property from both the objects and compare
            foreach (System.Reflection.PropertyInfo property in type.GetProperties())
            {
                if (property.Name != "ExtensionData")
                {
                    string Object1Value = string.Empty;
                    string Object2Value = string.Empty;

                    if (type.GetProperty(property.Name).GetValue(Object1, null) != null)
                        Object1Value = type.GetProperty(property.Name).GetValue(Object1, null).ToString();

                    if (type.GetProperty(property.Name).GetValue(object2, null) != null)
                        Object2Value = type.GetProperty(property.Name).GetValue(object2, null).ToString();

                    if (Object1Value.Trim() != Object2Value.Trim())
                    {
                        mLog = new SAPLogs();
                        mLog.Id = pNum;
                        mLog.ChangedField = property.Name;
                        mLog.OldValue = Object1Value.Trim();
                        mLog.NewValue = Object2Value.Trim();

                        mResult.Add(mLog);
                    }
                }
            }

        }


        #endregion

        #region Range Plan

        public List<ARGNSRangePlan> GetRangePlanListSearch(CompanyConn pCompanyParam, ref RangePlanCombo mRangePlanCombo, string pCode = "", string pSeason = "", string pCollection = "", string pEmployee = "")
        {
            List<ARGNSRangePlan> mRet = null;
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(pCode))
                {
                    mFilters = mFilters + "and Code eq '" + HttpUtility.UrlEncode(pCode) + "' ";
                }
                if (!string.IsNullOrEmpty(pSeason) && pSeason != "-1")
                {
                    mFilters = mFilters + "and U_Season eq '" + HttpUtility.UrlEncode(pSeason) + "' ";
                }
                if (!string.IsNullOrEmpty(pCollection) && pCollection != "-1")
                {
                    mFilters = mFilters + "and U_Coll eq '" + HttpUtility.UrlEncode(pCollection) + "' ";
                }
                if (!string.IsNullOrEmpty(pEmployee) && pEmployee != "-1")
                {
                    mFilters = mFilters + "and U_RPEmpl eq '" + HttpUtility.UrlEncode(pEmployee) + "' ";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "RangePlan", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSRangePlan>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mRangePlanCombo.ListSeason = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mRangePlanCombo.ListCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                mRangePlanCombo.ListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetRangePlanListSearch :" + ex.Message);
                return null;
            }

            return mRet;
        }

        public ARGNSRangePlan GetRangePlanById(CompanyConn pCompanyParam, string pCode)
        {
            ARGNSRangePlan mRet = null;
            string mUrl = string.Empty;
            string mFilters = "Code eq '" + HttpUtility.UrlEncode(pCode) + "'";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "RangePlan", mFilters);
                List<ARGNSRangePlan> mListRangeP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSRangePlan>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mListRangeP.FirstOrDefault();
                if (mRet != null)
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "RangePlanDetail", mFilters);
                    mRet.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSRangePlanDetail>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                    mRet.RangePlanCombo.ListBrand = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                    mRet.RangePlanCombo.ListCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Div");
                    mRet.RangePlanCombo.ListDivision = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSDivision>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                    mRet.RangePlanCombo.ListModelGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ProdLine");
                    mRet.RangePlanCombo.ListProductLine = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSProductLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                    mRet.RangePlanCombo.ListSeason = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                    mRet.RangePlanCombo.ListSubCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                    mRet.RangePlanCombo.ListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Project");
                    mRet.RangePlanCombo.ListProject = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetRangePlanById :" + ex.Message);
                return null;
            }

            return mRet;
        }

        public RangePlanCombo GetRangePlanCombo(CompanyConn pCompanyParam)
        {
            RangePlanCombo mCombo = new RangePlanCombo();
            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Brand");
                mCombo.ListBrand = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSBrand>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Collection");
                mCombo.ListCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Div");
                mCombo.ListDivision = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSDivision>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelGrp");
                mCombo.ListModelGroup = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelGroup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ProdLine");
                mCombo.ListProductLine = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSProductLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Season");
                mCombo.ListSeason = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSeason>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Ambient");
                mCombo.ListSubCollection = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSSubCollection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                mCombo.ListEmployee = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Project");
                mCombo.ListProject = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetRangePlanCombo :" + ex.Message);
                return null;
            }

            return mCombo;
        }

        public string UpdateRangePlan(ARGNSRangePlan pObject, CompanyConn pCompanyParam)
        {
            ARGNSRangePlanServiceWeb.ArgnsRangePlan oRangePlan = new ARGNSRangePlanServiceWeb.ArgnsRangePlan();
            ARGNSRangePlanServiceWeb.ArgnsRangePlanParams oRangePlanParams = new ARGNSRangePlanServiceWeb.ArgnsRangePlanParams();
            ARGNSRangePlanServiceWeb.MsgHeader oRangePlanMsgHeader = new ARGNSRangePlanServiceWeb.MsgHeader();

            oRangePlanMsgHeader.ServiceName = ARGNSRangePlanServiceWeb.MsgHeaderServiceName.ARGNS_RANGEP;
            oRangePlanMsgHeader.ServiceNameSpecified = true;
            oRangePlanMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSRangePlanServiceWeb.ARGNS_RANGEP oRangePlanServiceWeb = new ARGNSRangePlanServiceWeb.ARGNS_RANGEP();
            oRangePlanServiceWeb.MsgHeaderValue = oRangePlanMsgHeader;

            try
            {
                oRangePlanParams.Code = pObject.Code;
                oRangePlan = oRangePlanServiceWeb.GetByParams(oRangePlanParams);

                oRangePlan.U_Season = pObject.U_Season;
                oRangePlan.U_RangeDesc = pObject.U_RangeDesc;
                oRangePlan.U_Coll = pObject.U_Coll;
                oRangePlan.U_PrjCode = pObject.U_PrjCode;
                oRangePlan.U_RPEmpl = pObject.U_RPEmpl;
                if (pObject.U_SDate.HasValue && pObject.U_SDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    oRangePlan.U_SDate = pObject.U_SDate.Value;
                    oRangePlan.U_SDateSpecified = true;
                }
                if (pObject.U_DTo.HasValue && pObject.U_DTo.Value.ToString("yyyyMMdd") != "00010101")
                {
                    oRangePlan.U_DTo = pObject.U_DTo.Value;
                    oRangePlan.U_DToSpecified = true;
                }

                //Range Plan Details
                int mCount = 0;
                if (pObject.Lines.Count > 0)
                {
                    List<ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL> mRangeLinesList = new List<ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL>();
                    ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL mRangeLines;

                    foreach (ARGNSRangePlanDetail portalLine in pObject.Lines)
                    {
                        mRangeLines = new ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL();
                        mRangeLines.Code = pObject.Code;
                        mRangeLines.LineId = mCount + 1;
                        mRangeLines.LineIdSpecified = true;
                        mRangeLines.U_ModCode = portalLine.U_ModCode;
                        mRangeLines.U_ModDesc = portalLine.U_ModDesc;
                        mRangeLines.U_ModPic = portalLine.U_ModPic;
                        mRangeLines.U_Comments = portalLine.U_Comments;

                        mRangeLinesList.Add(mRangeLines);
                        mCount++;
                    }
                    oRangePlan.ARGNS_RANGEPDETAILCollection = mRangeLinesList.ToArray();
                }

                oRangePlanServiceWeb.Update(oRangePlan);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataDS -> UpdateRangePlan :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }


        #endregion

        #region Material Detail

        public ARGNSMaterialDetail GetMaterialDetails(CompanyConn pCompanyParam, MaterialDetailsUDF mMaterialDetailsUDF, string pModCode = "")
        {
            ARGNSMaterialDetail mMaterialDetail = new ARGNSMaterialDetail();
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MaterialDetails", mFilters);
                mMaterialDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMaterialDetail>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                if (mMaterialDetail != null)
                {
                    //Setting Material Details Lines
                    #region Material Details Lines
                    //Material Detail Fabric
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_FABRIC", mFilters);
                    string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    Newtonsoft.Json.Linq.JArray mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListFabric = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDFabric>>(mResultJson);
                    foreach (ARGNSMDFabric mFabric in mMaterialDetail.ListFabric)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_FABRIC'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFabric.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFFabric, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mFabric.LineId).FirstOrDefault());
                    }

                    //Material Detail Accessories
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_ACCESS", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListAccessories = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDAccess>>(mResultJson);
                    foreach (ARGNSMDAccess mAccessories in mMaterialDetail.ListAccessories)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_ACCESS'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mAccessories.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFAccessories, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mAccessories.LineId).FirstOrDefault());
                    }

                    //Material Detail Care Instructions
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_CARE_INST", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListCareInstructions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDCareInst>>(mResultJson);
                    foreach (ARGNSMDCareInst mCareInstructions in mMaterialDetail.ListCareInstructions)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_CARE_INST'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mCareInstructions.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFCareInstructions, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mCareInstructions.LineId).FirstOrDefault());
                    }

                    //Material Detail Labelling
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_LABELLING", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListLabelling = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDLabelling>>(mResultJson);
                    foreach (ARGNSMDLabelling mLabelling in mMaterialDetail.ListLabelling)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_LABELLING'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mLabelling.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFLabelling, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mLabelling.LineId).FirstOrDefault());
                    }

                    //Material Detail Packaging
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_PACKAGING", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListPackaging = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDPackaging>>(mResultJson);
                    foreach (ARGNSMDPackaging mPackaging in mMaterialDetail.ListPackaging)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_PACKAGING'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mPackaging.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFPackaging, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mPackaging.LineId).FirstOrDefault());
                    }

                    //Material Detail Footwear Material
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_FOOTWEAR", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListFootwearMaterial = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDFootwearMaterial>>(mResultJson);
                    foreach (ARGNSMDFootwearMaterial mFootwearMaterial in mMaterialDetail.ListFootwearMaterial)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_FOOTWEAR'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFootwearMaterial.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFFootwearMaterial, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mFootwearMaterial.LineId).FirstOrDefault());
                    }

                    //Material Detail Footwear Details
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_FDETAILS", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListFootwearDetail = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDFootwearDetails>>(mResultJson);
                    foreach (ARGNSMDFootwearDetails mFootwearDetail in mMaterialDetail.ListFootwearDetail)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_FDETAILS'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFootwearDetail.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFFootwearDetail, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mFootwearDetail.LineId).FirstOrDefault());
                    }

                    //Material Detail Footwear Packaging
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_FPACKAGING", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListFootwearPackaging = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDFootwearPackaging>>(mResultJson);
                    foreach (ARGNSMDFootwearPackaging mFootwearPackaging in mMaterialDetail.ListFootwearPackaging)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_FPACKAGING'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFootwearPackaging.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFFootwearPackaging, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mFootwearPackaging.LineId).FirstOrDefault());
                    }

                    //Material Detail Footwear Pictogram
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_FPICTOGRAM", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListFootwearPictogram = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDFootwearPictogram>>(mResultJson);
                    foreach (ARGNSMDFootwearPictogram mFootwearPictogram in mMaterialDetail.ListFootwearPictogram)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_FPICTOGRAM'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFootwearPictogram.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFFootwearPictogram, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mFootwearPictogram.LineId).FirstOrDefault());
                    }

                    //Material Detail Footwear Size Range
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_FSRANGE", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListFootwearSizeRange = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDFootwearSRange>>(mResultJson);
                    foreach (ARGNSMDFootwearSRange mFootwearSizeRange in mMaterialDetail.ListFootwearSizeRange)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_FSRANGE'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFootwearSizeRange.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFFootwearSizeRange, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mFootwearSizeRange.LineId).FirstOrDefault());
                    }

                    //Material Detail Footwear Accessories
                    mFilters = "Code eq '" + HttpUtility.UrlEncode(mMaterialDetail.Code) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MD_FACCESS", mFilters);
                    mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mJArray = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mMaterialDetail.ListFootwearAccessories = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMDFootwearAccess>>(mResultJson);
                    foreach (ARGNSMDFootwearAccess mFootwearAccessories in mMaterialDetail.ListFootwearAccessories)
                    {
                        mFilters = "TableID eq '@ARGNS_MD_FACCESS'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFootwearAccessories.MappedUdf = UDFUtil.GetObjectListWithUDFHana(mMaterialDetailsUDF.ListUDFFootwearAccessories, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArray.Where(c => (int)c["LineId"] == mFootwearAccessories.LineId).FirstOrDefault());
                    }

                    #endregion

                    //Setteo la imagen del modelo 
                    mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                    ARGNSModel mModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    if (mModel != null)
                        mMaterialDetail.U_Pic = mModel.U_Pic;
                }
                else
                {
                    mMaterialDetail = new ARGNSMaterialDetail();
                    //Setteo la imagen del modelo 
                    mFilters = "U_ModCode eq '" + HttpUtility.UrlEncode(pModCode) + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                    ARGNSModel mModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    if (mModel != null)
                        mMaterialDetail.U_Pic = mModel.U_Pic;
                }

                return mMaterialDetail;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetMaterialDetails :" + ex.Message);
                return null;
            }
        }

        public string UpdateMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail)
        {
            ARGNSMaterialDetailServiceWeb.ArgnsMatDet oMatDet = new ARGNSMaterialDetailServiceWeb.ArgnsMatDet();
            ARGNSMaterialDetailServiceWeb.ArgnsMatDetParams oMatDetParams = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetParams();
            ARGNSMaterialDetailServiceWeb.MsgHeader oMatDetParamsMsgHeader = new ARGNSMaterialDetailServiceWeb.MsgHeader();

            oMatDetParamsMsgHeader.ServiceName = ARGNSMaterialDetailServiceWeb.MsgHeaderServiceName.ARGNS_MAT_DET;
            oMatDetParamsMsgHeader.ServiceNameSpecified = true;
            oMatDetParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET oMatDetService = new ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET();
            oMatDetService.MsgHeaderValue = oMatDetParamsMsgHeader;

            try
            {
                oMatDetParams.Code = pMaterialDetail.Code;
                oMatDet = oMatDetService.GetByParams(oMatDetParams);

                oMatDet.U_Description = pMaterialDetail.U_Description;

                #region Fabric

                if (pMaterialDetail.ListFabric.Count > 0 || oMatDet.ARGNS_MD_FABRICCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mFabricSAP in oMatDet.ARGNS_MD_FABRICCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFabric mFabric = pMaterialDetail.ListFabric.Where(c => c.LineId == mFabricSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mFabric != null)
                        {
                            mFabricSAP.U_ColCode = mFabric.U_ColCode;
                            mFabricSAP.U_CompCode = mFabric.U_CompCode;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FABRICCollection = oMatDet.ARGNS_MD_FABRICCollection.Where(c => c.LineId != mFabricSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[oMatDet.ARGNS_MD_FABRICCollection.Length + pMaterialDetail.ListFabric.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FABRICCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FABRICCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFabric mFabric in pMaterialDetail.ListFabric.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC lineFabric = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC();
                        lineFabric.LineId = mFabric.LineId;
                        lineFabric.LineIdSpecified = true;
                        lineFabric.U_ColCode = mFabric.U_ColCode;
                        lineFabric.U_CompCode = mFabric.U_CompCode;
                        newLines[positionNewLine] = lineFabric;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FABRICCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[newLines.Length];
                    oMatDet.ARGNS_MD_FABRICCollection = newLines;
                }

                #endregion

                #region Accessories

                if (pMaterialDetail.ListAccessories.Count > 0 || oMatDet.ARGNS_MD_ACCESSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mAccessoriesSAP in oMatDet.ARGNS_MD_ACCESSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDAccess mAccessories = pMaterialDetail.ListAccessories.Where(c => c.LineId == mAccessoriesSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mAccessories != null)
                        {
                            mAccessoriesSAP.U_Buttons = mAccessories.U_Buttons;
                            mAccessoriesSAP.U_Trims = mAccessories.U_Trims;
                            mAccessoriesSAP.U_Zippers = mAccessories.U_Zippers;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_ACCESSCollection = oMatDet.ARGNS_MD_ACCESSCollection.Where(c => c.LineId != mAccessoriesSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[oMatDet.ARGNS_MD_ACCESSCollection.Length + pMaterialDetail.ListAccessories.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_ACCESSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_ACCESSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDAccess mAccessory in pMaterialDetail.ListAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS lineAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS();
                        lineAccessories.LineId = mAccessory.LineId;
                        lineAccessories.LineIdSpecified = true;
                        lineAccessories.U_Buttons = mAccessory.U_Buttons;
                        lineAccessories.U_Trims = mAccessory.U_Trims;
                        newLines[positionNewLine] = lineAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_ACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_ACCESSCollection = newLines;
                }

                #endregion

                #region Care Instruction

                if (pMaterialDetail.ListCareInstructions.Count > 0 || oMatDet.ARGNS_MD_CARE_INSTCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mCISAP in oMatDet.ARGNS_MD_CARE_INSTCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDCareInst mCI = pMaterialDetail.ListCareInstructions.Where(c => c.LineId == mCISAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mCI != null)
                        {
                            mCISAP.U_ColCode = mCI.U_ColCode;
                            mCISAP.U_CareText = mCI.U_CareText;
                            mCISAP.U_CareCode = mCI.U_CareCode;
                            mCISAP.U_CareLbl = mCI.U_CareLbl;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_CARE_INSTCollection = oMatDet.ARGNS_MD_CARE_INSTCollection.Where(c => c.LineId != mCISAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[oMatDet.ARGNS_MD_CARE_INSTCollection.Length + pMaterialDetail.ListCareInstructions.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_CARE_INSTCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_CARE_INSTCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDCareInst mCI in pMaterialDetail.ListCareInstructions.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST lineCI = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST();
                        lineCI.LineId = mCI.LineId;
                        lineCI.LineIdSpecified = true;
                        lineCI.U_ColCode = mCI.U_ColCode;
                        lineCI.U_CareText = mCI.U_CareText;
                        lineCI.U_CareCode = mCI.U_CareCode;
                        lineCI.U_CareLbl = mCI.U_CareLbl;
                        newLines[positionNewLine] = lineCI;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_CARE_INSTCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[newLines.Length];
                    oMatDet.ARGNS_MD_CARE_INSTCollection = newLines;
                }

                #endregion

                #region Labelling

                if (pMaterialDetail.ListLabelling.Count > 0 || oMatDet.ARGNS_MD_LABELLINGCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mLabellingSAP in oMatDet.ARGNS_MD_LABELLINGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDLabelling mCI = pMaterialDetail.ListLabelling.Where(c => c.LineId == mLabellingSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mCI != null)
                        {
                            mLabellingSAP.U_LImage = mCI.U_LImage;
                            mLabellingSAP.U_LSize = mCI.U_LSize;
                            mLabellingSAP.U_MLabel = mCI.U_MLabel;
                            mLabellingSAP.U_Position = mCI.U_Position;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_LABELLINGCollection = oMatDet.ARGNS_MD_LABELLINGCollection.Where(c => c.LineId != mLabellingSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[oMatDet.ARGNS_MD_LABELLINGCollection.Length + pMaterialDetail.ListLabelling.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_LABELLINGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_LABELLINGCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDLabelling mLabelling in pMaterialDetail.ListLabelling.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING lineLabelling = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING();
                        lineLabelling.LineId = mLabelling.LineId;
                        lineLabelling.LineIdSpecified = true;
                        lineLabelling.U_LImage = mLabelling.U_LImage;
                        lineLabelling.U_LSize = mLabelling.U_LSize;
                        lineLabelling.U_MLabel = mLabelling.U_MLabel;
                        lineLabelling.U_Position = mLabelling.U_Position;
                        newLines[positionNewLine] = lineLabelling;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_LABELLINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[newLines.Length];
                    oMatDet.ARGNS_MD_LABELLINGCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListPackaging.Count > 0 || oMatDet.ARGNS_MD_PACKAGINGCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mPackagingSAP in oMatDet.ARGNS_MD_PACKAGINGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDPackaging mPackaging = pMaterialDetail.ListPackaging.Where(c => c.LineId == mPackagingSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPackaging != null)
                        {
                            mPackagingSAP.U_Hangtag = mPackaging.U_Hangtag;
                            mPackagingSAP.U_Others = mPackaging.U_Others;
                            mPackagingSAP.U_OutPack = mPackaging.U_OutPack;
                            mPackagingSAP.U_Polybag = mPackaging.U_Polybag;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_PACKAGINGCollection = oMatDet.ARGNS_MD_PACKAGINGCollection.Where(c => c.LineId != mPackagingSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[oMatDet.ARGNS_MD_PACKAGINGCollection.Length + pMaterialDetail.ListPackaging.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_PACKAGINGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_PACKAGINGCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDPackaging mPackaging in pMaterialDetail.ListPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_Polybag = mPackaging.U_Polybag;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_PACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_PACKAGINGCollection = newLines;
                }

                #endregion

                #region Material

                if (pMaterialDetail.ListFootwearMaterial.Count > 0 || oMatDet.ARGNS_MD_FOOTWEARCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mMaterialSAP in oMatDet.ARGNS_MD_FOOTWEARCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearMaterial mMaterial = pMaterialDetail.ListFootwearMaterial.Where(c => c.LineId == mMaterialSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mMaterial != null)
                        {
                            mMaterialSAP.U_Heel = mMaterial.U_Heel;
                            mMaterialSAP.U_Insole = mMaterial.U_Insole;
                            mMaterialSAP.U_Lining = mMaterial.U_Lining;
                            mMaterialSAP.U_Outsole = mMaterial.U_Outsole;
                            mMaterialSAP.U_Sfabric = mMaterial.U_Sfabric;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FOOTWEARCollection = oMatDet.ARGNS_MD_FOOTWEARCollection.Where(c => c.LineId != mMaterialSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[oMatDet.ARGNS_MD_FOOTWEARCollection.Length + pMaterialDetail.ListFootwearMaterial.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FOOTWEARCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FOOTWEARCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearMaterial mMaterial in pMaterialDetail.ListFootwearMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR lineMaterial = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_Heel = mMaterial.U_Heel;
                        lineMaterial.U_Insole = mMaterial.U_Insole;
                        lineMaterial.U_Lining = mMaterial.U_Lining;
                        lineMaterial.U_Outsole = mMaterial.U_Outsole;
                        lineMaterial.U_Sfabric = mMaterial.U_Sfabric;
                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FOOTWEARCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[newLines.Length];
                    oMatDet.ARGNS_MD_FOOTWEARCollection = newLines;
                }

                #endregion

                #region Detail

                if (pMaterialDetail.ListFootwearDetail.Count > 0 || oMatDet.ARGNS_MD_FDETAILSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mDetailSAP in oMatDet.ARGNS_MD_FDETAILSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearDetails mDetail = pMaterialDetail.ListFootwearDetail.Where(c => c.LineId == mDetailSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mDetail != null)
                        {
                            mDetailSAP.U_BHeight = mDetail.U_BHeight;
                            mDetailSAP.U_BWidth = mDetail.U_BWidth;
                            mDetailSAP.U_HHeight = mDetail.U_HHeight;
                            mDetailSAP.U_Others = mDetail.U_Others;
                            mDetailSAP.U_Seams = mDetail.U_Seams;
                            mDetailSAP.U_Strap = mDetail.U_Strap;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FDETAILSCollection = oMatDet.ARGNS_MD_FDETAILSCollection.Where(c => c.LineId != mDetailSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[oMatDet.ARGNS_MD_FDETAILSCollection.Length + pMaterialDetail.ListFootwearDetail.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FDETAILSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FDETAILSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearDetails mDetail in pMaterialDetail.ListFootwearDetail.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS lineDetail = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS();
                        lineDetail.LineId = mDetail.LineId;
                        lineDetail.LineIdSpecified = true;
                        lineDetail.U_BHeight = mDetail.U_BHeight;
                        lineDetail.U_BWidth = mDetail.U_BWidth;
                        lineDetail.U_HHeight = mDetail.U_HHeight;
                        lineDetail.U_Others = mDetail.U_Others;
                        lineDetail.U_Seams = mDetail.U_Seams;
                        lineDetail.U_Strap = mDetail.U_Strap;
                        newLines[positionNewLine] = lineDetail;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FDETAILSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[newLines.Length];
                    oMatDet.ARGNS_MD_FDETAILSCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListFootwearPackaging.Count > 0 || oMatDet.ARGNS_MD_FPACKAGINGCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mPackagingSAP in oMatDet.ARGNS_MD_FPACKAGINGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearPackaging mPackaging = pMaterialDetail.ListFootwearPackaging.Where(c => c.LineId == mPackagingSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPackaging != null)
                        {
                            mPackagingSAP.U_Hangtag = mPackaging.U_Hangtag;
                            mPackagingSAP.U_Others = mPackaging.U_Others;
                            mPackagingSAP.U_OutPack = mPackaging.U_OutPack;
                            mPackagingSAP.U_PolybagSB = mPackaging.U_PolybagSB;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FPACKAGINGCollection = oMatDet.ARGNS_MD_FPACKAGINGCollection.Where(c => c.LineId != mPackagingSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[oMatDet.ARGNS_MD_FPACKAGINGCollection.Length + pMaterialDetail.ListFootwearPackaging.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FPACKAGINGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FPACKAGINGCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPackaging mPackaging in pMaterialDetail.ListFootwearPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_PolybagSB = mPackaging.U_PolybagSB;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = newLines;
                }

                #endregion

                #region Pictogram

                if (pMaterialDetail.ListFootwearPictogram.Count > 0 || oMatDet.ARGNS_MD_FPICTOGRAMCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mPictogramSAP in oMatDet.ARGNS_MD_FPICTOGRAMCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearPictogram mPictogram = pMaterialDetail.ListFootwearPictogram.Where(c => c.LineId == mPictogramSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPictogram != null)
                        {
                            mPictogramSAP.U_Pictogram = mPictogram.U_Pictogram;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FPICTOGRAMCollection = oMatDet.ARGNS_MD_FPICTOGRAMCollection.Where(c => c.LineId != mPictogramSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[oMatDet.ARGNS_MD_FPICTOGRAMCollection.Length + pMaterialDetail.ListFootwearPictogram.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FPICTOGRAMCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPictogram mPictogram in pMaterialDetail.ListFootwearPictogram.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM linePictogram = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM();
                        linePictogram.LineId = mPictogram.LineId;
                        linePictogram.LineIdSpecified = true;
                        linePictogram.U_Pictogram = mPictogram.U_Pictogram;
                        newLines[positionNewLine] = linePictogram;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[newLines.Length];
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = newLines;
                }

                #endregion

                #region Size Range

                if (pMaterialDetail.ListFootwearSizeRange.Count > 0 || oMatDet.ARGNS_MD_FSRANGECollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mSRangeSAP in oMatDet.ARGNS_MD_FSRANGECollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearSRange mSRange = pMaterialDetail.ListFootwearSizeRange.Where(c => c.LineId == mSRangeSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mSRange != null)
                        {
                            mSRangeSAP.U_SRange = mSRange.U_SRange;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FSRANGECollection = oMatDet.ARGNS_MD_FSRANGECollection.Where(c => c.LineId != mSRangeSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[oMatDet.ARGNS_MD_FSRANGECollection.Length + pMaterialDetail.ListFootwearSizeRange.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FSRANGECollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FSRANGECollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearSRange mSRange in pMaterialDetail.ListFootwearSizeRange.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE lineSRange = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE();
                        lineSRange.LineId = mSRange.LineId;
                        lineSRange.LineIdSpecified = true;
                        lineSRange.U_SRange = mSRange.U_SRange;
                        newLines[positionNewLine] = lineSRange;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FSRANGECollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[newLines.Length];
                    oMatDet.ARGNS_MD_FSRANGECollection = newLines;
                }

                #endregion

                #region Footware Accessories

                if (pMaterialDetail.ListFootwearAccessories.Count > 0 || oMatDet.ARGNS_MD_FACCESSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mFAccessoriesSAP in oMatDet.ARGNS_MD_FACCESSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearAccess mFAccessories = pMaterialDetail.ListFootwearAccessories.Where(c => c.LineId == mFAccessoriesSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mFAccessories != null)
                        {
                            mFAccessoriesSAP.U_Buckles = mFAccessories.U_Buckles;
                            mFAccessoriesSAP.U_Enclosures = mFAccessories.U_Enclosures;
                            mFAccessoriesSAP.U_Eyelets = mFAccessories.U_Eyelets;
                            mFAccessoriesSAP.U_Labelling = mFAccessories.U_Labelling;
                            mFAccessoriesSAP.U_Others = mFAccessories.U_Others;
                            mFAccessoriesSAP.U_SLaces = mFAccessories.U_SLaces;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FACCESSCollection = oMatDet.ARGNS_MD_FACCESSCollection.Where(c => c.LineId != mFAccessoriesSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[oMatDet.ARGNS_MD_FACCESSCollection.Length + pMaterialDetail.ListFootwearAccessories.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FACCESSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FACCESSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearAccess mFAccessories in pMaterialDetail.ListFootwearAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS lineFAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS();
                        lineFAccessories.LineId = mFAccessories.LineId;
                        lineFAccessories.LineIdSpecified = true;
                        lineFAccessories.U_Buckles = mFAccessories.U_Buckles;
                        lineFAccessories.U_Enclosures = mFAccessories.U_Enclosures;
                        lineFAccessories.U_Eyelets = mFAccessories.U_Eyelets;
                        lineFAccessories.U_Labelling = mFAccessories.U_Labelling;
                        lineFAccessories.U_Others = mFAccessories.U_Others;
                        lineFAccessories.U_SLaces = mFAccessories.U_SLaces;
                        newLines[positionNewLine] = lineFAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_FACCESSCollection = newLines;
                }

                #endregion


                XmlDocument mXmlDoc = GetSoapStructure(true, oMatDetParamsMsgHeader, oMatDet);

                #region AddUDFToXML

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FABRIC']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFabric mAuxLine = pMaterialDetail.ListFabric.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_ACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDAccess mAuxLine = pMaterialDetail.ListAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_CARE_INST']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDCareInst mAuxLine = pMaterialDetail.ListCareInstructions.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_LABELLING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDLabelling mAuxLine = pMaterialDetail.ListLabelling.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_PACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDPackaging mAuxLine = pMaterialDetail.ListPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearAccess mAuxLine = pMaterialDetail.ListFootwearAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FDETAILS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearDetails mAuxLine = pMaterialDetail.ListFootwearDetail.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FOOTWEAR']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearMaterial mAuxLine = pMaterialDetail.ListFootwearMaterial.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPackaging mAuxLine = pMaterialDetail.ListFootwearPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPICTOGRAM']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPictogram mAuxLine = pMaterialDetail.ListFootwearPictogram.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FSRANGE']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearSRange mAuxLine = pMaterialDetail.ListFootwearSizeRange.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }

                #endregion

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateMaterialDetail :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string AddMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail)
        {
            ARGNSMaterialDetailServiceWeb.ArgnsMatDet oMatDet = new ARGNSMaterialDetailServiceWeb.ArgnsMatDet();
            ARGNSMaterialDetailServiceWeb.MsgHeader oMatDetParamsMsgHeader = new ARGNSMaterialDetailServiceWeb.MsgHeader();

            oMatDetParamsMsgHeader.ServiceName = ARGNSMaterialDetailServiceWeb.MsgHeaderServiceName.ARGNS_MAT_DET;
            oMatDetParamsMsgHeader.ServiceNameSpecified = true;
            oMatDetParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET oMatDetService = new ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET();
            oMatDetService.MsgHeaderValue = oMatDetParamsMsgHeader;
            try
            {
                oMatDet = new ARGNSMaterialDetailServiceWeb.ArgnsMatDet();

                oMatDet.Code = GetLastMaterialDetailCode(pCompanyParam);
                oMatDet.U_ModCode = pMaterialDetail.U_ModCode;
                oMatDet.U_Description = pMaterialDetail.U_Description;

                #region Fabric

                if (pMaterialDetail.ListFabric.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[pMaterialDetail.ListFabric.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFabric mFabric in pMaterialDetail.ListFabric.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC lineFabric = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC();
                        lineFabric.LineId = mFabric.LineId;
                        lineFabric.LineIdSpecified = true;
                        lineFabric.U_ColCode = mFabric.U_ColCode;
                        lineFabric.U_CompCode = mFabric.U_CompCode;
                        newLines[positionNewLine] = lineFabric;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FABRICCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[newLines.Length];
                    oMatDet.ARGNS_MD_FABRICCollection = newLines;
                }

                #endregion

                #region Accessories

                if (pMaterialDetail.ListAccessories.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[pMaterialDetail.ListAccessories.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDAccess mAccesory in pMaterialDetail.ListAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS lineAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS();
                        lineAccessories.LineId = mAccesory.LineId;
                        lineAccessories.LineIdSpecified = true;
                        lineAccessories.U_Buttons = mAccesory.U_Buttons;
                        lineAccessories.U_Trims = mAccesory.U_Trims;
                        newLines[positionNewLine] = lineAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_ACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_ACCESSCollection = newLines;
                }

                #endregion

                #region Care Instruction

                if (pMaterialDetail.ListCareInstructions.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[pMaterialDetail.ListCareInstructions.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDCareInst mCI in pMaterialDetail.ListCareInstructions.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST lineCI = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST();
                        lineCI.LineId = mCI.LineId;
                        lineCI.LineIdSpecified = true;
                        lineCI.U_ColCode = mCI.U_ColCode;
                        lineCI.U_CareText = mCI.U_CareText;
                        lineCI.U_CareCode = mCI.U_CareCode;
                        lineCI.U_CareLbl = mCI.U_CareLbl;
                        newLines[positionNewLine] = lineCI;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_CARE_INSTCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[newLines.Length];
                    oMatDet.ARGNS_MD_CARE_INSTCollection = newLines;
                }

                #endregion

                #region Labelling

                if (pMaterialDetail.ListLabelling.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[pMaterialDetail.ListLabelling.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDLabelling mLabelling in pMaterialDetail.ListLabelling.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING lineLabelling = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING();
                        lineLabelling.LineId = mLabelling.LineId;
                        lineLabelling.LineIdSpecified = true;
                        lineLabelling.U_LImage = mLabelling.U_LImage;
                        lineLabelling.U_LSize = mLabelling.U_LSize;
                        lineLabelling.U_MLabel = mLabelling.U_MLabel;
                        lineLabelling.U_Position = mLabelling.U_Position;
                        newLines[positionNewLine] = lineLabelling;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_LABELLINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[newLines.Length];
                    oMatDet.ARGNS_MD_LABELLINGCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListPackaging.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[pMaterialDetail.ListPackaging.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDPackaging mPackaging in pMaterialDetail.ListPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_Polybag = mPackaging.U_Polybag;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_PACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_PACKAGINGCollection = newLines;
                }

                #endregion

                #region Material

                if (pMaterialDetail.ListFootwearMaterial.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[pMaterialDetail.ListFootwearMaterial.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearMaterial mMaterial in pMaterialDetail.ListFootwearMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR lineMaterial = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_Heel = mMaterial.U_Heel;
                        lineMaterial.U_Insole = mMaterial.U_Insole;
                        lineMaterial.U_Lining = mMaterial.U_Lining;
                        lineMaterial.U_Outsole = mMaterial.U_Outsole;
                        lineMaterial.U_Sfabric = mMaterial.U_Sfabric;
                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FOOTWEARCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[newLines.Length];
                    oMatDet.ARGNS_MD_FOOTWEARCollection = newLines;
                }

                #endregion

                #region Detail

                if (pMaterialDetail.ListFootwearDetail.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[pMaterialDetail.ListFootwearDetail.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearDetails mDetail in pMaterialDetail.ListFootwearDetail.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS lineDetail = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS();
                        lineDetail.LineId = mDetail.LineId;
                        lineDetail.LineIdSpecified = true;
                        lineDetail.U_BHeight = mDetail.U_BHeight;
                        lineDetail.U_BWidth = mDetail.U_BWidth;
                        lineDetail.U_HHeight = mDetail.U_HHeight;
                        lineDetail.U_Others = mDetail.U_Others;
                        lineDetail.U_Seams = mDetail.U_Seams;
                        lineDetail.U_Strap = mDetail.U_Strap;
                        newLines[positionNewLine] = lineDetail;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FDETAILSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[newLines.Length];
                    oMatDet.ARGNS_MD_FDETAILSCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListFootwearPackaging.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[pMaterialDetail.ListFootwearPackaging.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPackaging mPackaging in pMaterialDetail.ListFootwearPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_PolybagSB = mPackaging.U_PolybagSB;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = newLines;
                }

                #endregion

                #region Pictogram

                if (pMaterialDetail.ListFootwearPictogram.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[pMaterialDetail.ListFootwearPictogram.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPictogram mPictogram in pMaterialDetail.ListFootwearPictogram.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM linePictogram = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM();
                        linePictogram.LineId = mPictogram.LineId;
                        linePictogram.LineIdSpecified = true;
                        linePictogram.U_Pictogram = mPictogram.U_Pictogram;
                        newLines[positionNewLine] = linePictogram;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[newLines.Length];
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = newLines;
                }

                #endregion

                #region Size Range

                if (pMaterialDetail.ListFootwearSizeRange.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[pMaterialDetail.ListFootwearSizeRange.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearSRange mSRange in pMaterialDetail.ListFootwearSizeRange.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE lineSRange = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE();
                        lineSRange.LineId = mSRange.LineId;
                        lineSRange.LineIdSpecified = true;
                        lineSRange.U_SRange = mSRange.U_SRange;
                        newLines[positionNewLine] = lineSRange;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FSRANGECollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[newLines.Length];
                    oMatDet.ARGNS_MD_FSRANGECollection = newLines;
                }

                #endregion

                #region Footware Accessories

                if (pMaterialDetail.ListFootwearAccessories.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[pMaterialDetail.ListFootwearAccessories.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearAccess mFAccessories in pMaterialDetail.ListFootwearAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS lineFAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS();
                        lineFAccessories.LineId = mFAccessories.LineId;
                        lineFAccessories.LineIdSpecified = true;
                        lineFAccessories.U_Buckles = mFAccessories.U_Buckles;
                        lineFAccessories.U_Enclosures = mFAccessories.U_Enclosures;
                        lineFAccessories.U_Eyelets = mFAccessories.U_Eyelets;
                        lineFAccessories.U_Labelling = mFAccessories.U_Labelling;
                        lineFAccessories.U_Others = mFAccessories.U_Others;
                        lineFAccessories.U_SLaces = mFAccessories.U_SLaces;
                        newLines[positionNewLine] = lineFAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_FACCESSCollection = newLines;
                }

                #endregion

                XmlDocument mXmlDoc = GetSoapStructure(false, oMatDetParamsMsgHeader, oMatDet);

                #region AddUDFToXML

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FABRIC']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFabric mAuxLine = pMaterialDetail.ListFabric.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_ACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDAccess mAuxLine = pMaterialDetail.ListAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_CARE_INST']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDCareInst mAuxLine = pMaterialDetail.ListCareInstructions.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_LABELLING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDLabelling mAuxLine = pMaterialDetail.ListLabelling.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_PACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDPackaging mAuxLine = pMaterialDetail.ListPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearAccess mAuxLine = pMaterialDetail.ListFootwearAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FDETAILS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearDetails mAuxLine = pMaterialDetail.ListFootwearDetail.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FOOTWEAR']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearMaterial mAuxLine = pMaterialDetail.ListFootwearMaterial.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPackaging mAuxLine = pMaterialDetail.ListFootwearPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPICTOGRAM']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPictogram mAuxLine = pMaterialDetail.ListFootwearPictogram.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FSRANGE']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearSRange mAuxLine = pMaterialDetail.ListFootwearSizeRange.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }

                #endregion

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> AddMaterialDetail :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string GetLastMaterialDetailCode(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "MaterialDetails", "", mOthers);
                List<ARGNSMaterialDetail> mListModelReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSMaterialDetail>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                if (mListModelReturn.Count > 0)
                {
                    mMaxVal = Convert.ToInt32(mListModelReturn.SingleOrDefault().DocEntry) + 1;
                }
                else
                {
                    mMaxVal = 1;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastMaterialDetailCode :" + ex.Message);
                return null;
            }
            return mMaxVal.ToString();
        }

        #endregion

        #region Shipment

        public ARGNSContainer GetContainerById(CompanyConn pCompanyParam, int pDocEntry)
        {
            ARGNSContainer mARGNSContainer = new ARGNSContainer();
            string mUrl = string.Empty;
            string mFilters;
            try
            {
                mFilters = "DocEntry eq " + pDocEntry;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shipment", mFilters);
                mARGNSContainer = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                if (mARGNSContainer != null)
                {
                    //Setting Shipment Lines
                    mFilters = "DocEntry eq " + mARGNSContainer.DocEntry;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentLines", mFilters);
                    mARGNSContainer.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    foreach (ARGNSContainerLine mContainerLine in mARGNSContainer.Lines)
                    {
                        if (mContainerLine.U_ObjType == "17")
                        {
                            mFilters = "DocEntry eq " + mContainerLine.U_BaseEntry + " and LineNum eq " + mContainerLine.U_BaseLine;
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrderLine", mFilters);
                            List<DraftLine> mListDraftLineAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                            mContainerLine.DraftLine = mListDraftLineAux.FirstOrDefault();
                        }
                        if (mContainerLine.U_ObjType == "22")
                        {
                            mFilters = "DocEntry eq " + mContainerLine.U_BaseEntry + " and LineNum eq " + mContainerLine.U_BaseLine;
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrderLine", mFilters);
                            List<DraftLine> mListDraftLineAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                            mContainerLine.DraftLine = mListDraftLineAux.FirstOrDefault();
                        }
                    }
                    mFilters = "DocEntry eq " + mARGNSContainer.DocEntry;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentContainer", mFilters);
                    mARGNSContainer.ListPackage = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerPackage>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    foreach (ARGNSContainerPackage mPackage in mARGNSContainer.ListPackage)
                    {
                        mFilters = "U_CntnerNum eq " + mPackage.U_CntnerNum + " and DocEntry eq " + pDocEntry;
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentContainerLines", mFilters);
                        mPackage.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerPackageLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines)
                        {
                            ARGNSContainerLine mContainerLineAux = mARGNSContainer.Lines.Where(c => c.U_BaseEntry == mPackageLine.U_BaseEntry && c.U_BaseLine.ToString() == mPackageLine.U_BaseLine).FirstOrDefault();
                            mPackageLine.U_ARGNS_MOD = mContainerLineAux.U_ARGNS_MOD;
                            mPackageLine.U_ARGNS_COL = mContainerLineAux.U_ARGNS_COL;
                            mPackageLine.U_ARGNS_SIZE = mContainerLineAux.U_ARGNS_SIZE;
                            mPackageLine.U_ItemName = mContainerLineAux.U_ItemName;
                        }
                    }
                }
                else
                {
                    mARGNSContainer = new ARGNSContainer();
                    mARGNSContainer.Lines = new List<ARGNSContainerLine>();
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentStatus");
                mARGNSContainer.ListStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerStatus>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentPort");
                mARGNSContainer.ListPort = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerPort>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Freight");
                mARGNSContainer.ListFreight = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Freight>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mARGNSContainer;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetContainerById :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSContainer> GetContainerList(CompanyConn pCompanyParam, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            List<ARGNSContainer> mARGNSContainerList = new List<ARGNSContainer>();
            string mUrl = string.Empty;
            string mFilters;

            try
            {
                mFilters = "1 eq 1 ";
                if (!string.IsNullOrEmpty(txtShipmentNum))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtShipmentNum.Trim()) + "'), tolower(U_Shipment)) ";
                }
                if (!string.IsNullOrEmpty(txtStatus))
                {
                    mFilters = mFilters + "and U_Status eq '" + HttpUtility.UrlEncode(txtStatus) + "'";
                }
                if (!string.IsNullOrEmpty(txtPOE))
                {
                    mFilters = mFilters + "and U_POE eq '" + HttpUtility.UrlEncode(txtPOE) + "'";
                }
                if (dateASD != null)
                {
                    mFilters = mFilters + "and U_ASD eq '" + dateASD + "'";
                }
                if (dateADA != null)
                {
                    mFilters = mFilters + "and U_ADA eq '" + dateADA + "'";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shipment", mFilters);
                mARGNSContainerList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetContainerList :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSContainer> GetMyShipmentListSearchBP(CompanyConn pCompanyParam, string pBP, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            List<ARGNSContainer> mARGNSContainerList = new List<ARGNSContainer>();
            List<ARGNSContainerLine> mARGNSContainerLineList = new List<ARGNSContainerLine>();
            string mUrl = string.Empty;
            string mFilters;

            try
            {
                mFilters = "U_CardCode eq '" + pBP + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentLines", mFilters);
                mARGNSContainerLineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                List<int> mListShipmentDocEntryList = mARGNSContainerLineList.Select(c => c.DocEntry).Distinct().ToList();

                mFilters = "(";
                for (int i = 0; i < mListShipmentDocEntryList.Count; i++)
                {
                    if (i == 0)
                        mFilters += "DocEntry eq " + mListShipmentDocEntryList[i];
                    else
                        mFilters += " or DocEntry eq " + mListShipmentDocEntryList[i];
                }
                mFilters += ") ";

                if (!string.IsNullOrEmpty(txtShipmentNum))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(txtShipmentNum.Trim()) + "'), tolower(U_Shipment)) ";
                }
                if (!string.IsNullOrEmpty(txtStatus))
                {
                    mFilters = mFilters + "and U_Status eq '" + HttpUtility.UrlEncode(txtStatus) + "'";
                }
                if (!string.IsNullOrEmpty(txtPOE))
                {
                    mFilters = mFilters + "and U_POE eq '" + HttpUtility.UrlEncode(txtPOE) + "'";
                }
                if (dateASD != null)
                {
                    mFilters = mFilters + "and U_ASD eq '" + dateASD + "'";
                }
                if (dateADA != null)
                {
                    mFilters = mFilters + "and U_ADA eq '" + dateADA + "'";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shipment", mFilters);
                mARGNSContainerList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetMyShipmentListSearchBP :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSContainerPackage> GetContainerPackageList(CompanyConn pCompanyParam, int pDocEntry)
        {
            List<ARGNSContainerPackage> mARGNSContainerPackageList = new List<ARGNSContainerPackage>();
            string mUrl = string.Empty;
            string mFilters;

            try
            {
                mFilters = "DocEntry eq " + pDocEntry;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentContainer", mFilters);
                mARGNSContainerPackageList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerPackage>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                foreach (ARGNSContainerPackage mPackage in mARGNSContainerPackageList)
                {
                    mFilters = "DocEntry eq " + pDocEntry + " and U_CntnerNum eq " + mPackage.U_CntnerNum;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentContainerLines", mFilters);
                    mPackage.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerPackageLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines)
                    {
                        mFilters = "ItemCode eq '" + mPackageLine.U_ItemCode + "'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Items", mFilters);
                        List<OITM> mListOITMAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                        mPackageLine.U_ItemName = mListOITMAux.FirstOrDefault().ItemName;
                    }
                }

                return mARGNSContainerPackageList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetContainerPackageList :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSPSlipPackage> GetPSlipPackageList(CompanyConn pCompanyParam, int pContainerID, int pShipmentID)
        {
            List<ARGNSPSlipPackage> mARGNSPSlipPackageList = new List<ARGNSPSlipPackage>();

            string mUrl = string.Empty;
            string mFilters;

            try
            {
                mFilters = "U_CntnerNum eq " + pContainerID + " and DocEntry eq " + pShipmentID;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentPSlip", mFilters);
                mARGNSPSlipPackageList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPSlipPackage>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                foreach (ARGNSPSlipPackage mPSlip in mARGNSPSlipPackageList)
                {
                    mFilters = "U_CntnerNum eq " + pContainerID + " and DocEntry eq " + mPSlip.DocEntry + " and U_PackageNum eq " + mPSlip.U_PackageNum;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentPSlipLines", mFilters);
                    mPSlip.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPSlipPackageLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

                return mARGNSPSlipPackageList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPSlipPackageList :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSContainerSetup> GetContainerSetupList(CompanyConn pCompanyParam)
        {
            List<ARGNSContainerSetup> mARGNSContainerList = new List<ARGNSContainerSetup>();

            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ContainerSetup");
                mARGNSContainerList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerSetup>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetContainerSetupList :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSPSlipPackageType> GetPSlipPackageTypeList(CompanyConn pCompanyParam)
        {
            List<ARGNSPSlipPackageType> mARGNSContainerList = new List<ARGNSPSlipPackageType>();

            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OPKG");
                mARGNSContainerList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSPSlipPackageType>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetPSlipPackageTypeList :" + ex.Message);
                return null;
            }
        }

        public string UpdateShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment)
        {
            ARGNSContainerServiceWeb.ArgnsContainer oArgnsContainer = new ARGNSContainerServiceWeb.ArgnsContainer();
            ARGNSContainerServiceWeb.ArgnsContainerParams oArgnsContainerParams = new ARGNSContainerServiceWeb.ArgnsContainerParams();
            ARGNSContainerServiceWeb.MsgHeader oArgnsContainerParamsMsgHeader = new ARGNSContainerServiceWeb.MsgHeader();

            oArgnsContainerParamsMsgHeader.ServiceName = ARGNSContainerServiceWeb.MsgHeaderServiceName.ARGNS_CNTM;
            oArgnsContainerParamsMsgHeader.ServiceNameSpecified = true;
            oArgnsContainerParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSContainerServiceWeb.ARGNS_CNTM oArgnsContainerService = new ARGNSContainerServiceWeb.ARGNS_CNTM();
            oArgnsContainerService.MsgHeaderValue = oArgnsContainerParamsMsgHeader;

            try
            {
                oArgnsContainerParams.DocEntry = pShipment.DocEntry;
                oArgnsContainerParams.DocEntrySpecified = true;
                oArgnsContainer = oArgnsContainerService.GetByParams(oArgnsContainerParams);

                oArgnsContainer.U_Shipment = pShipment.U_Shipment;
                oArgnsContainer.U_Broker = pShipment.U_Broker;
                oArgnsContainer.U_Status = pShipment.U_Status;
                oArgnsContainer.U_ShipVia = pShipment.U_ShipVia;
                oArgnsContainer.U_POE = pShipment.U_POE;
                oArgnsContainer.U_UserTxt1 = pShipment.U_UserTxt1;
                oArgnsContainer.U_UserTxt2 = pShipment.U_UserTxt2;
                oArgnsContainer.U_UserTxt3 = pShipment.U_UserTxt3;
                oArgnsContainer.U_UserTxt4 = pShipment.U_UserTxt4;
                decimal? mQuantity = pShipment.Lines.Sum(c => c.DraftLine.Quantity);
                if (mQuantity != null)
                {
                    oArgnsContainer.U_Quantity = (double)mQuantity;
                    oArgnsContainer.U_QuantitySpecified = true;
                }
                if (pShipment.U_ESD != null)
                    oArgnsContainer.U_ESD = pShipment.U_ESD.Value;
                if (pShipment.U_EDA != null)
                    oArgnsContainer.U_EDA = pShipment.U_EDA.Value;
                if (pShipment.U_EDW != null)
                    oArgnsContainer.U_EDW = pShipment.U_EDW.Value;
                if (pShipment.U_ASD != null)
                    oArgnsContainer.U_ASD = pShipment.U_ASD.Value;
                if (pShipment.U_ADA != null)
                    oArgnsContainer.U_ADA = pShipment.U_ADA.Value;
                if (pShipment.U_ADW != null)
                    oArgnsContainer.U_ADW = pShipment.U_ADW.Value;

                #region Shipment Plan Details

                if (pShipment.Lines.Count > 0 || oArgnsContainer.ARGNS_CNTM_LINECollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el Shipment
                    foreach (var mContainerLineSAP in oArgnsContainer.ARGNS_CNTM_LINECollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSContainerLine mContainerLine = pShipment.Lines.Where(c => c.LineId == mContainerLineSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mContainerLine != null)
                        {
                            //En este caso no modificamos nada porque en el portal los campos de las lineas son read only
                        }
                        //Sino la borro
                        else
                        {
                            oArgnsContainer.ARGNS_CNTM_LINECollection = oArgnsContainer.ARGNS_CNTM_LINECollection.Where(c => c.LineId != mContainerLineSAP.LineId).ToArray();
                        }
                    }

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[oArgnsContainer.ARGNS_CNTM_LINECollection.Length + pShipment.Lines.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsContainer.ARGNS_CNTM_LINECollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsContainer.ARGNS_CNTM_LINECollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSContainerLine mContainerLine in pShipment.Lines.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE lineContainer = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE();
                        lineContainer.LineId = mContainerLine.LineId;
                        lineContainer.LineIdSpecified = true;
                        lineContainer.U_BaseDocNo = mContainerLine.U_BaseDocNo;
                        lineContainer.U_BaseEntry = mContainerLine.U_BaseEntry;
                        lineContainer.U_BaseLine = mContainerLine.U_BaseLine.Value;
                        lineContainer.U_BaseLineSpecified = true;
                        lineContainer.U_ARGNS_COL = mContainerLine.U_ARGNS_COL;
                        lineContainer.U_ARGNS_MOD = mContainerLine.U_ARGNS_MOD;
                        lineContainer.U_ARGNS_SEASON = mContainerLine.U_ARGNS_SEASON;
                        lineContainer.U_ARGNS_SIZE = mContainerLine.U_ARGNS_SIZE;
                        lineContainer.U_ItemName = mContainerLine.U_ItemName;
                        lineContainer.U_ItemCode = mContainerLine.U_ItemCode;
                        lineContainer.U_CardCode = mContainerLine.U_CardCode;
                        lineContainer.U_CardName = mContainerLine.U_CardName;
                        lineContainer.U_LineStatus = mContainerLine.U_LineStatus;
                        lineContainer.U_ObjType = mContainerLine.U_ObjType;
                        lineContainer.U_WhsCode = mContainerLine.U_WhsCode;

                        newLines[positionNewLine] = lineContainer;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_LINECollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_LINECollection = newLines;
                }

                #endregion


                #region Shipment Containers

                if (pShipment.ListPackage.Count > 0 || oArgnsContainer.ARGNS_CNTM_OCPKCollection.Count() > 0)
                {

                    #region Containers ya existentes

                    //Actualizo y elimino los container que ya estaban creados
                    foreach (var mPackageSAP in oArgnsContainer.ARGNS_CNTM_OCPKCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSContainerPackage mPackage = pShipment.ListPackage.Where(c => c.LineId == mPackageSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPackage != null)
                        {
                            if (mPackage.Lines.Count > 0)
                            {
                                //Actualizo y elimino las lineas que ya estaban creadas en el Container
                                foreach (var mPackageLineSAP in oArgnsContainer.ARGNS_CNTM_CPK1Collection.Where(c => c.DocEntry == mPackage.DocEntry && c.U_CntnerNum == mPackage.U_CntnerNum))
                                {
                                    //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                                    ARGNSContainerPackageLine mPackageLine = mPackage.Lines.Where(c => c.LineId == mPackageLineSAP.LineId && c.IsNew == false).FirstOrDefault();
                                    if (mPackageLine != null)
                                    {
                                        //Aca va lo que se pueda modificar en la linea desde el portal
                                        mPackageLineSAP.U_Quantity = (double)mPackageLine.U_Quantity;
                                        mPackageLineSAP.U_QuantitySpecified = true;
                                    }
                                    //Sino la borro
                                    else
                                    {
                                        oArgnsContainer.ARGNS_CNTM_CPK1Collection = oArgnsContainer.ARGNS_CNTM_CPK1Collection.Where(c => c.LineId != mPackageLineSAP.LineId && c.U_CntnerNum == mPackageLineSAP.U_CntnerNum).ToArray();
                                    }
                                }

                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[] newPackageLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length + mPackage.Lines.Where(c => c.IsNew == true).ToList().Count];
                                oArgnsContainer.ARGNS_CNTM_CPK1Collection.CopyTo(newPackageLines, 0);
                                int positionNewPackageLine = oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length;

                                //Agrego las lineas nuevas del container
                                foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines.Where(c => c.IsNew == true).ToList())
                                {
                                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1 linePackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1();
                                    linePackage.LineId = mPackageLine.LineId;
                                    linePackage.LineIdSpecified = true;
                                    linePackage.U_BaseEntry = mPackageLine.U_BaseEntry;
                                    linePackage.U_BaseLine = mPackageLine.U_BaseLine;
                                    linePackage.U_BaseRef = mPackageLine.U_BaseRef;
                                    linePackage.U_CntnerNum = mPackageLine.U_CntnerNum.Value;
                                    linePackage.U_CntnerNumSpecified = true;
                                    linePackage.U_ItemCode = mPackageLine.U_ItemCode;
                                    linePackage.U_Quantity = (double)mPackageLine.U_Quantity.Value;
                                    linePackage.U_QuantitySpecified = true;

                                    newPackageLines[positionNewPackageLine] = linePackage;
                                    positionNewPackageLine += 1;
                                }
                                oArgnsContainer.ARGNS_CNTM_CPK1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[newPackageLines.Length];
                                oArgnsContainer.ARGNS_CNTM_CPK1Collection = newPackageLines;
                            }
                        }
                        //Sino borro el container
                        else
                        {
                            oArgnsContainer.ARGNS_CNTM_OCPKCollection = oArgnsContainer.ARGNS_CNTM_OCPKCollection.Where(c => c.LineId != mPackageSAP.LineId).ToArray();
                        }
                    }

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[oArgnsContainer.ARGNS_CNTM_OCPKCollection.Length + pShipment.ListPackage.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsContainer.ARGNS_CNTM_OCPKCollection.Length;

                    #endregion

                    #region Containers nuevos

                    //Agrego los container nuevos
                    foreach (ARGNSContainerPackage mPackage in pShipment.ListPackage.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK auxPackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK();
                        auxPackage.LineId = mPackage.LineId;
                        auxPackage.LineIdSpecified = true;
                        auxPackage.U_CntnerNum = mPackage.U_CntnerNum.Value;
                        auxPackage.U_CntnerNumSpecified = true;
                        auxPackage.U_CntnerTyp = mPackage.U_CntnerTyp;
                        auxPackage.U_Weight = 0;
                        auxPackage.U_WeightSpecified = true;


                        if (mPackage.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[] newPackageLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length + mPackage.Lines.Where(c => c.IsNew == true).ToList().Count];
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection.CopyTo(newPackageLines, 0);
                            int positionNewPackageLine = oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length;

                            //Agrego las lineas nuevas del container
                            foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1 linePackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1();
                                linePackage.LineId = mPackageLine.LineId;
                                linePackage.LineIdSpecified = true;
                                linePackage.U_BaseEntry = mPackageLine.U_BaseEntry;
                                linePackage.U_BaseLine = mPackageLine.U_BaseLine;
                                linePackage.U_BaseRef = mPackageLine.U_BaseRef;
                                linePackage.U_CntnerNum = mPackageLine.U_CntnerNum.Value;
                                linePackage.U_CntnerNumSpecified = true;
                                linePackage.U_ItemCode = mPackageLine.U_ItemCode;
                                linePackage.U_Quantity = (double)mPackageLine.U_Quantity.Value;
                                linePackage.U_QuantitySpecified = true;
                                linePackage.U_UomEntry = -1;
                                linePackage.U_UomEntrySpecified = true;
                                linePackage.U_NumPerMsr = 1;
                                linePackage.U_NumPerMsrSpecified = true;

                                newPackageLines[positionNewPackageLine] = linePackage;
                                positionNewPackageLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[newPackageLines.Length];
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = newPackageLines;
                        }

                        newLines[positionNewLine] = auxPackage;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = newLines;

                    #endregion

                }

                #endregion


                #region Shipment Containers Packagin Slip

                if (pShipment.ListPackaginSlip.Count > 0 || oArgnsContainer.ARGNS_CNTM_OPKGCollection.Count() > 0)
                {

                    #region Packagin Slip ya existentes

                    //Actualizo y elimino los packagin slip que ya estaban creados
                    foreach (var mPackaginSlipSAP in oArgnsContainer.ARGNS_CNTM_OPKGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSPSlipPackage mPackaginSlip = pShipment.ListPackaginSlip.Where(c => c.LineId == mPackaginSlipSAP.LineId && c.DocEntry == mPackaginSlipSAP.DocEntry && c.IsNew == false).FirstOrDefault();
                        if (mPackaginSlip != null)
                        {
                            if (mPackaginSlip.Lines.Count > 0)
                            {
                                //Actualizo y elimino las lineas que ya estaban creadas en el Packagin Slip
                                foreach (var mPackagingSlipLineSAP in oArgnsContainer.ARGNS_CNTM_PKG1Collection.Where(c => c.DocEntry == mPackaginSlip.DocEntry && c.U_CntnerNum == mPackaginSlip.U_CntnerNum && c.U_PackageNum == mPackaginSlip.U_PackageNum))
                                {
                                    //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                                    ARGNSPSlipPackageLine mPackagingSlipLine = mPackaginSlip.Lines.Where(c => c.LineId == mPackagingSlipLineSAP.LineId && c.DocEntry == mPackagingSlipLineSAP.DocEntry && c.IsNew == false).FirstOrDefault();
                                    if (mPackagingSlipLine != null)
                                    {
                                        //Aca va lo que se pueda modificar en la linea desde el portal
                                        mPackagingSlipLineSAP.U_Quantity = (double)mPackagingSlipLine.U_Quantity;
                                        mPackagingSlipLineSAP.U_QuantitySpecified = true;
                                    }
                                    //Sino la borro
                                    else
                                    {
                                        oArgnsContainer.ARGNS_CNTM_PKG1Collection = oArgnsContainer.ARGNS_CNTM_PKG1Collection.Where(c => c.LineId != mPackagingSlipLineSAP.LineId && c.U_CntnerNum == mPackagingSlipLineSAP.U_CntnerNum && c.U_PackageNum == mPackagingSlipLineSAP.U_PackageNum).ToArray();
                                    }
                                }

                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[] newPackagingSlipLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length + mPackaginSlip.Lines.Where(c => c.IsNew == true).ToList().Count];
                                oArgnsContainer.ARGNS_CNTM_PKG1Collection.CopyTo(newPackagingSlipLines, 0);
                                int positionNewPackagingSlipLine = oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length;

                                //Agrego las lineas nuevas del packaging slip
                                foreach (ARGNSPSlipPackageLine mPackagingSlipLine in mPackaginSlip.Lines.Where(c => c.IsNew == true).ToList())
                                {
                                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1 linePackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1();
                                    linePackagingSlip.LineId = mPackagingSlipLine.LineId;
                                    linePackagingSlip.LineIdSpecified = true;
                                    linePackagingSlip.U_BaseEntry = mPackagingSlipLine.U_BaseEntry;
                                    linePackagingSlip.U_BaseLine = mPackagingSlipLine.U_BaseLine;
                                    linePackagingSlip.U_BaseRef = mPackagingSlipLine.U_BaseRef;
                                    linePackagingSlip.U_CntnerNum = mPackagingSlipLine.U_CntnerNum.Value;
                                    linePackagingSlip.U_CntnerNumSpecified = true;
                                    linePackagingSlip.U_PackageNum = mPackagingSlipLine.U_PackageNum.Value;
                                    linePackagingSlip.U_PackageNumSpecified = true;
                                    linePackagingSlip.U_ItemCode = mPackagingSlipLine.U_ItemCode;
                                    linePackagingSlip.U_Quantity = (double)mPackagingSlipLine.U_Quantity.Value;
                                    linePackagingSlip.U_QuantitySpecified = true;

                                    newPackagingSlipLines[positionNewPackagingSlipLine] = linePackagingSlip;
                                    positionNewPackagingSlipLine += 1;
                                }
                                oArgnsContainer.ARGNS_CNTM_PKG1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[newPackagingSlipLines.Length];
                                oArgnsContainer.ARGNS_CNTM_PKG1Collection = newPackagingSlipLines;
                            }
                        }
                        //Sino borro el packaging slip
                        else
                        {
                            oArgnsContainer.ARGNS_CNTM_OPKGCollection = oArgnsContainer.ARGNS_CNTM_OPKGCollection.Where(c => c.LineId != mPackaginSlipSAP.LineId && c.DocEntry == mPackaginSlipSAP.DocEntry).ToArray();
                        }
                    }

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[oArgnsContainer.ARGNS_CNTM_OPKGCollection.Length + pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsContainer.ARGNS_CNTM_OPKGCollection.Length;

                    #endregion

                    #region Packaging Slip nuevos

                    //Agrego los packaging slip nuevos
                    foreach (ARGNSPSlipPackage mPackagingSlip in pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG auxPackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG();
                        auxPackagingSlip.LineId = mPackagingSlip.LineId;
                        auxPackagingSlip.LineIdSpecified = true;
                        auxPackagingSlip.U_CntnerNum = mPackagingSlip.U_CntnerNum.Value;
                        auxPackagingSlip.U_CntnerNumSpecified = true;
                        auxPackagingSlip.U_PackageNum = mPackagingSlip.U_PackageNum.Value;
                        auxPackagingSlip.U_PackageNumSpecified = true;
                        auxPackagingSlip.U_PackageTyp = mPackagingSlip.U_PackageTyp;
                        auxPackagingSlip.U_Weight = 0;
                        auxPackagingSlip.U_WeightSpecified = true;
                        auxPackagingSlip.U_WeightUnit = "";

                        if (mPackagingSlip.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[] newPackagingSlipLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length + mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList().Count];
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection.CopyTo(newPackagingSlipLines, 0);
                            int positionNewPackagingSlipLine = oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length;

                            //Agrego las lineas nuevas del packagin slip
                            foreach (ARGNSPSlipPackageLine mPackagingSlipLine in mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1 linePackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1();
                                linePackagingSlip.LineId = mPackagingSlipLine.LineId;
                                linePackagingSlip.LineIdSpecified = true;
                                linePackagingSlip.U_BaseEntry = mPackagingSlipLine.U_BaseEntry;
                                linePackagingSlip.U_BaseLine = mPackagingSlipLine.U_BaseLine;
                                linePackagingSlip.U_BaseRef = mPackagingSlipLine.U_BaseRef;
                                linePackagingSlip.U_CntnerNum = mPackagingSlipLine.U_CntnerNum.Value;
                                linePackagingSlip.U_CntnerNumSpecified = true;
                                linePackagingSlip.U_PackageNum = mPackagingSlipLine.U_PackageNum.Value;
                                linePackagingSlip.U_PackageNumSpecified = true;
                                linePackagingSlip.U_ItemCode = mPackagingSlipLine.U_ItemCode;
                                linePackagingSlip.U_Quantity = (double)mPackagingSlipLine.U_Quantity.Value;
                                linePackagingSlip.U_QuantitySpecified = true;
                                linePackagingSlip.U_UomEntry = -1;
                                linePackagingSlip.U_UomEntrySpecified = true;
                                linePackagingSlip.U_NumPerMsr = 1;
                                linePackagingSlip.U_NumPerMsrSpecified = true;

                                newPackagingSlipLines[positionNewPackagingSlipLine] = linePackagingSlip;
                                positionNewPackagingSlipLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[newPackagingSlipLines.Length];
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = newPackagingSlipLines;
                        }

                        newLines[positionNewLine] = auxPackagingSlip;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = newLines;

                    #endregion

                }

                #endregion


                oArgnsContainerService.Update(oArgnsContainer);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> UpdateShipment :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string AddShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment)
        {
            ARGNSContainerServiceWeb.ArgnsContainer oArgnsContainer = new ARGNSContainerServiceWeb.ArgnsContainer();
            ARGNSContainerServiceWeb.MsgHeader oArgnsContainerParamsMsgHeader = new ARGNSContainerServiceWeb.MsgHeader();

            oArgnsContainerParamsMsgHeader.ServiceName = ARGNSContainerServiceWeb.MsgHeaderServiceName.ARGNS_CNTM;
            oArgnsContainerParamsMsgHeader.ServiceNameSpecified = true;
            oArgnsContainerParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSContainerServiceWeb.ARGNS_CNTM oArgnsContainerService = new ARGNSContainerServiceWeb.ARGNS_CNTM();
            oArgnsContainerService.MsgHeaderValue = oArgnsContainerParamsMsgHeader;

            try
            {
                oArgnsContainer.DocEntry = GetLastShipmentDocEntry(pCompanyParam);
                oArgnsContainer.DocEntrySpecified = true;
                oArgnsContainer.DocNum = GetLastShipmentDocNum(pCompanyParam);
                oArgnsContainer.DocNumSpecified = true;

                oArgnsContainer.U_Shipment = pShipment.U_Shipment;
                oArgnsContainer.U_Broker = pShipment.U_Broker;
                oArgnsContainer.U_Status = pShipment.U_Status;
                oArgnsContainer.U_ShipVia = pShipment.U_ShipVia;
                oArgnsContainer.U_POE = pShipment.U_POE;
                oArgnsContainer.U_UserTxt1 = pShipment.U_UserTxt1;
                oArgnsContainer.U_UserTxt2 = pShipment.U_UserTxt2;
                oArgnsContainer.U_UserTxt3 = pShipment.U_UserTxt3;
                oArgnsContainer.U_UserTxt4 = pShipment.U_UserTxt4;
                decimal? mQuantity = pShipment.Lines.Sum(c => c.DraftLine.Quantity);
                if (mQuantity != null)
                {
                    oArgnsContainer.U_Quantity = (double)mQuantity;
                    oArgnsContainer.U_QuantitySpecified = true;
                }
                if (pShipment.U_ESD != null)
                    oArgnsContainer.U_ESD = pShipment.U_ESD.Value;
                if (pShipment.U_EDA != null)
                    oArgnsContainer.U_EDA = pShipment.U_EDA.Value;
                if (pShipment.U_EDW != null)
                    oArgnsContainer.U_EDW = pShipment.U_EDW.Value;
                if (pShipment.U_ASD != null)
                    oArgnsContainer.U_ASD = pShipment.U_ASD.Value;
                if (pShipment.U_ADA != null)
                    oArgnsContainer.U_ADA = pShipment.U_ADA.Value;
                if (pShipment.U_ADW != null)
                    oArgnsContainer.U_ADW = pShipment.U_ADW.Value;

                #region Shipment Plan Details

                if (pShipment.Lines.Count > 0)
                {
                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[pShipment.Lines.Where(c => c.IsNew == true).ToList().Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSContainerLine mContainerLine in pShipment.Lines.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE lineContainer = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE();
                        lineContainer.LineId = mContainerLine.LineId;
                        lineContainer.LineIdSpecified = true;
                        lineContainer.U_BaseDocNo = mContainerLine.U_BaseDocNo;
                        lineContainer.U_BaseEntry = mContainerLine.U_BaseEntry;
                        lineContainer.U_BaseLine = mContainerLine.U_BaseLine.Value;
                        lineContainer.U_BaseLineSpecified = true;
                        lineContainer.U_ARGNS_COL = mContainerLine.U_ARGNS_COL;
                        lineContainer.U_ARGNS_MOD = mContainerLine.U_ARGNS_MOD;
                        lineContainer.U_ARGNS_SEASON = mContainerLine.U_ARGNS_SEASON;
                        lineContainer.U_ARGNS_SIZE = mContainerLine.U_ARGNS_SIZE;
                        lineContainer.U_ItemName = mContainerLine.U_ItemName;
                        lineContainer.U_ItemCode = mContainerLine.U_ItemCode;
                        lineContainer.U_CardCode = mContainerLine.U_CardCode;
                        lineContainer.U_CardName = mContainerLine.U_CardName;
                        lineContainer.U_LineStatus = mContainerLine.U_LineStatus;
                        lineContainer.U_ObjType = mContainerLine.U_ObjType;
                        lineContainer.U_WhsCode = mContainerLine.U_WhsCode;

                        newLines[positionNewLine] = lineContainer;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_LINECollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_LINECollection = newLines;
                }

                #endregion

                #region Shipment Containers

                if (pShipment.ListPackage.Count > 0)
                {

                    #region Containers nuevos

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[pShipment.ListPackage.Where(c => c.IsNew == true).ToList().Count];
                    int positionNewLine = 0;

                    //Agrego los container nuevos
                    foreach (ARGNSContainerPackage mPackage in pShipment.ListPackage.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK auxPackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK();
                        auxPackage.LineId = mPackage.LineId;
                        auxPackage.LineIdSpecified = true;
                        auxPackage.U_CntnerNum = mPackage.U_CntnerNum.Value;
                        auxPackage.U_CntnerNumSpecified = true;
                        auxPackage.U_CntnerTyp = mPackage.U_CntnerTyp;
                        auxPackage.U_Weight = 0;
                        auxPackage.U_WeightSpecified = true;


                        if (mPackage.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[] newPackageLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[mPackage.Lines.Where(c => c.IsNew == true).ToList().Count];
                            int positionNewPackageLine = 0;

                            //Agrego las lineas nuevas del container
                            foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1 linePackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1();
                                linePackage.LineId = mPackageLine.LineId;
                                linePackage.LineIdSpecified = true;
                                linePackage.U_BaseEntry = mPackageLine.U_BaseEntry;
                                linePackage.U_BaseLine = mPackageLine.U_BaseLine;
                                linePackage.U_BaseRef = mPackageLine.U_BaseRef;
                                linePackage.U_CntnerNum = mPackageLine.U_CntnerNum.Value;
                                linePackage.U_CntnerNumSpecified = true;
                                linePackage.U_ItemCode = mPackageLine.U_ItemCode;
                                linePackage.U_Quantity = (double)mPackageLine.U_Quantity.Value;
                                linePackage.U_QuantitySpecified = true;
                                linePackage.U_UomEntry = -1;
                                linePackage.U_UomEntrySpecified = true;
                                linePackage.U_NumPerMsr = 1;
                                linePackage.U_NumPerMsrSpecified = true;

                                newPackageLines[positionNewPackageLine] = linePackage;
                                positionNewPackageLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[newPackageLines.Length];
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = newPackageLines;
                        }

                        newLines[positionNewLine] = auxPackage;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = newLines;

                    #endregion

                }

                #endregion

                #region Shipment Containers Packagin Slip

                if (pShipment.ListPackaginSlip.Count > 0)
                {

                    #region Packaging Slip nuevos

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList().Count];
                    int positionNewLine = 0;

                    //Agrego los packaging slip nuevos
                    foreach (ARGNSPSlipPackage mPackagingSlip in pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG auxPackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG();
                        auxPackagingSlip.LineId = mPackagingSlip.LineId;
                        auxPackagingSlip.LineIdSpecified = true;
                        auxPackagingSlip.U_CntnerNum = mPackagingSlip.U_CntnerNum.Value;
                        auxPackagingSlip.U_CntnerNumSpecified = true;
                        auxPackagingSlip.U_PackageNum = mPackagingSlip.U_PackageNum.Value;
                        auxPackagingSlip.U_PackageNumSpecified = true;
                        auxPackagingSlip.U_PackageTyp = mPackagingSlip.U_PackageTyp;
                        auxPackagingSlip.U_Weight = 0;
                        auxPackagingSlip.U_WeightSpecified = true;
                        auxPackagingSlip.U_WeightUnit = "";

                        if (mPackagingSlip.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[] newPackagingSlipLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList().Count];
                            int positionNewPackagingSlipLine = 0;

                            //Agrego las lineas nuevas del packaging slip
                            foreach (ARGNSPSlipPackageLine mPackagingSlipLine in mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1 linePackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1();
                                linePackagingSlip.LineId = mPackagingSlipLine.LineId;
                                linePackagingSlip.LineIdSpecified = true;
                                linePackagingSlip.U_BaseEntry = mPackagingSlipLine.U_BaseEntry;
                                linePackagingSlip.U_BaseLine = mPackagingSlipLine.U_BaseLine;
                                linePackagingSlip.U_BaseRef = mPackagingSlipLine.U_BaseRef;
                                linePackagingSlip.U_CntnerNum = mPackagingSlipLine.U_CntnerNum.Value;
                                linePackagingSlip.U_CntnerNumSpecified = true;
                                linePackagingSlip.U_PackageNum = mPackagingSlipLine.U_PackageNum.Value;
                                linePackagingSlip.U_PackageNumSpecified = true;
                                linePackagingSlip.U_ItemCode = mPackagingSlipLine.U_ItemCode;
                                linePackagingSlip.U_Quantity = (double)mPackagingSlipLine.U_Quantity.Value;
                                linePackagingSlip.U_QuantitySpecified = true;
                                linePackagingSlip.U_UomEntry = -1;
                                linePackagingSlip.U_UomEntrySpecified = true;
                                linePackagingSlip.U_NumPerMsr = 1;
                                linePackagingSlip.U_NumPerMsrSpecified = true;

                                newPackagingSlipLines[positionNewPackagingSlipLine] = linePackagingSlip;
                                positionNewPackagingSlipLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[newPackagingSlipLines.Length];
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = newPackagingSlipLines;
                        }

                        newLines[positionNewLine] = auxPackagingSlip;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = newLines;

                    #endregion

                }

                #endregion


                oArgnsContainerService.Add(oArgnsContainer);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> AddShipment :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public List<Draft> GetDocumentWhitOpenLines(CompanyConn pCompanyParam, string pObjType)
        {
            string mUrl = string.Empty;
            string mFilters;

            try
            {
                List<Draft> mDrafList = new List<Draft>();
                List<int> mDocEntryList = new List<int>();
                if (pObjType == "22")
                {
                    mFilters = "LineStatus eq 'O'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrderLine", mFilters);
                    List<POR1> mListPOLinesAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<POR1>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mDocEntryList = mListPOLinesAux.Select(p => p.DocEntry).Distinct().ToList();

                    mFilters = "(";
                    for (int i = 0; i < mDocEntryList.Count; i++)
                    {
                        if (i == 0)
                            mFilters += "DocEntry eq " + mDocEntryList[i];
                        else
                            mFilters += " or DocEntry eq " + mDocEntryList[i];
                    }
                    mFilters += ")";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrder", mFilters);
                    mDrafList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Draft>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
                if (pObjType == "17")
                {
                    mFilters = "LineStatus eq 'O'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrderLine", mFilters);
                    List<RDR1> mListSOLinesAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR1>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mDocEntryList = mListSOLinesAux.Select(p => p.DocEntry).Distinct().ToList();

                    mFilters = "(";
                    for (int i = 0; i < mDocEntryList.Count; i++)
                    {
                        if (i == 0)
                            mFilters += "DocEntry eq " + mDocEntryList[i];
                        else
                            mFilters += " or DocEntry eq " + mDocEntryList[i];
                    }
                    mFilters += ")";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrder", mFilters);
                    mDrafList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Draft>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

                return mDrafList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetDocumentWhitOpenLines :" + ex.Message);
                return null;
            }
        }

        public List<DraftLine> GetDocumentOpenLines(CompanyConn pCompanyParam, string pObjType, List<int> pDocEntryList)
        {
            string mUrl = string.Empty;
            string mFilters;

            try
            {
                List<DraftLine> mDrafLineList = new List<DraftLine>();
                if (pObjType == "22")
                {
                    mFilters = "LineStatus eq 'O' and (";
                    for (int i = 0; i < pDocEntryList.Count; i++)
                    {
                        if (i == 0)
                            mFilters += "DocEntry eq " + pDocEntryList[i];
                        else
                            mFilters += " or DocEntry eq " + pDocEntryList[i];
                    }
                    mFilters += ")";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentDocumentOpenLinesPOView", mFilters);
                    mDrafLineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
                if (pObjType == "17")
                {
                    mFilters = "LineStatus eq 'O' and (";
                    for (int i = 0; i < pDocEntryList.Count; i++)
                    {
                        if (i == 0)
                            mFilters += "DocEntry eq " + pDocEntryList[i];
                        else
                            mFilters += " or DocEntry eq " + pDocEntryList[i];
                    }
                    mFilters += ")";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentDocumentOpenLinesSOView", mFilters);
                    mDrafLineList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

                return mDrafLineList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetDocumentOpenLines :" + ex.Message);
                return null;
            }
        }

        public List<Draft> GetShipmentDocument(CompanyConn pCompanyParam, string pShipmentID, string pObjType)
        {
            string mUrl = string.Empty;
            string mFilters;

            try
            {
                List<ODRF> mDocumentList = new List<ODRF>();

                if (pObjType == "22")
                {
                    mFilters = "U_ARGNS_SHPCNO eq '" + pShipmentID + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OPDN", mFilters);
                    mDocumentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ODRF>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
                if (pObjType == "17")
                {
                    mFilters = "U_ARGNS_SHPCNO eq '" + pShipmentID + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ODLN", mFilters);
                    mDocumentList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ODRF>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

                return Mapper.Map<List<Draft>>(mDocumentList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetShipmentDocument :" + ex.Message);
                return null;
            }
        }

        public ShipmentMembers GetShipmentMembersObject(CompanyConn pCompanyParam)
        {
            ShipmentMembers mShipmentMembers = new ShipmentMembers();

            string mUrl = string.Empty;

            try
            {
                mShipmentMembers.ListStatus.Add(new ARGNSContainerStatus("", ""));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentStatus");
                mShipmentMembers.ListStatus.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerStatus>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mShipmentMembers.ListPort.Add(new ARGNSContainerPort("", ""));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShipmentPort");
                mShipmentMembers.ListPort.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainerPort>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                mShipmentMembers.ListFreight.Add(new Freight(null, ""));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Freight");
                mShipmentMembers.ListFreight.AddRange(Mapper.Map<List<Freight>>(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Freight>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword))));

                return mShipmentMembers;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetShipmentMembersObject :" + ex.Message);
                return null;
            }
        }

        public int GetLastShipmentDocEntry(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                mOthers = "&$orderby=DocEntry desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shipment", "", mOthers);
                List<ARGNSContainer> mListShipmentReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                if (mListShipmentReturn.Count > 0)
                {
                    mMaxVal = Convert.ToInt32(mListShipmentReturn.SingleOrDefault().DocEntry) + 1;
                }
                else
                {
                    mMaxVal = 1;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastShipmentDocEntry :" + ex.Message);
                return -1;
            }
            return mMaxVal;
        }

        public int GetLastShipmentDocNum(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mOthers;
            int mMaxVal;
            try
            {
                mOthers = "&$orderby=DocNum desc&$top=1";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Shipment", "", mOthers);
                List<ARGNSContainer> mListShipmentReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSContainer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                if (mListShipmentReturn.Count > 0)
                {
                    mMaxVal = Convert.ToInt32(mListShipmentReturn.SingleOrDefault().DocNum) + 1;
                }
                else
                {
                    mMaxVal = 1;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetLastShipmentDocNum :" + ex.Message);
                return -1;
            }
            return mMaxVal;
        }

        #endregion

        #region Tech Pack

        public ARGNSTechPack GetTechPack(CompanyConn pCompanyParam, int pId)
        {
            ARGNSModel mModelReturn = null;
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
            string mUrl = string.Empty;
            string mFilters = "Code eq '" + pId + "'";
            ARGNSTechPack mARGNSTechPack = new ARGNSTechPack();
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ModelInst", mFilters);
                mARGNSTechPack.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSTechPackLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "TechPackSection");
                mARGNSTechPack.Sections = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSTechPackSection>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Model", mFilters);
                ARGNSModel mModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                mARGNSTechPack.Code = pId.ToString();
                mARGNSTechPack.ModelCode = mModel.U_ModCode;
                mARGNSTechPack.ModelDesc = mModel.U_ModDesc;
                mARGNSTechPack.Lines = mARGNSTechPack.Lines.Select(c => { c.U_ModCode = mModel.U_ModCode; return c; }).ToList();

                return mARGNSTechPack;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> GetTechPack :" + ex.Message);
                return null;
            }
        }

        public string UpdateTechPack(ARGNSTechPack pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                if (pObject.Lines.Count > 0 || oArgnsModel.ARGNS_MODEL_INSTCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el TechPack
                    foreach (var mTechPackLineSAP in oArgnsModel.ARGNS_MODEL_INSTCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSTechPackLine mTechPackLine = pObject.Lines.Where(c => c.LineId == mTechPackLineSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mTechPackLine != null)
                        {
                            mTechPackLineSAP.U_Sector = mTechPackLine.U_Sector;
                            mTechPackLineSAP.U_InstCod = mTechPackLine.U_InstCod;
                            mTechPackLineSAP.U_Instuct = mTechPackLine.U_Instuct;
                            mTechPackLineSAP.U_Descrip = mTechPackLine.U_Descrip;
                            if (mTechPackLine.ImgChanged == true)
                                mTechPackLineSAP.U_Imag = mTechPackLine.U_Imag;
                            mTechPackLineSAP.U_Active = mTechPackLine.U_Active;
                        }
                        //Sino la borro
                        else
                        {
                            oArgnsModel.ARGNS_MODEL_INSTCollection = oArgnsModel.ARGNS_MODEL_INSTCollection.Where(c => c.LineId != mTechPackLineSAP.LineId).ToArray();
                        }
                    }

                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST[] newLines = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST[oArgnsModel.ARGNS_MODEL_INSTCollection.Length + pObject.Lines.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsModel.ARGNS_MODEL_INSTCollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsModel.ARGNS_MODEL_INSTCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSTechPackLine mTechPackLine in pObject.Lines.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST lineTechPack = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST();
                        lineTechPack.Code = mTechPackLine.Code;
                        lineTechPack.LineId = mTechPackLine.LineId;
                        lineTechPack.LineIdSpecified = true;
                        lineTechPack.U_Sector = mTechPackLine.U_Sector;
                        lineTechPack.U_InstCod = mTechPackLine.U_InstCod;
                        lineTechPack.U_Instuct = mTechPackLine.U_Instuct;
                        lineTechPack.U_Descrip = mTechPackLine.U_Descrip;
                        lineTechPack.U_Imag = mTechPackLine.U_Imag;
                        lineTechPack.U_Active = mTechPackLine.U_Active;
                        newLines[positionNewLine] = lineTechPack;
                        positionNewLine += 1;
                    }
                    oArgnsModel.ARGNS_MODEL_INSTCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST[newLines.Length];
                    oArgnsModel.ARGNS_MODEL_INSTCollection = newLines;
                }

                XmlDocument mXmlDoc = Utils.GetSoapStructure(true, oArgnsModelMsgHeader, oArgnsModel);

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceSL -> UpdateTechPack :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion

        public List<ARGNSCatalog> GetCatalogListSearch(CompanyConn pCompanyParam, 
            string pCatalogCode = "", 
            string pCatalogName = "", 
            int? pSalesEmployee = null)
        {
            string mUrl = string.Empty;
            string mFilters, mCodeFilters; 

            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<ARGNSCatalog> mCatalogList = new List<ARGNSCatalog>();

                mCodeFilters = " 1 eq 1 ";

                if (!pCatalogCode.Equals(""))
                {
                    mCodeFilters = mCodeFilters + string.Format (" and Code eq '{0}'", pCatalogCode.Trim()); 
                }

                if (!pCatalogName.Equals(""))
                {
                    mCodeFilters = mCodeFilters + string.Format(" and indexof(Name, '{0}') gt -1", pCatalogName.Trim());
                }

                if (pSalesEmployee != null)
                {
                    mFilters = "salesPrson eq " + pSalesEmployee;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", mFilters);
                    List<OHEM> mEmployeeList = serializer.Deserialize<List<OHEM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    string mInternaFilter = string.Empty;
                    mFilters = string.Empty;
                    foreach (OHEM item in mEmployeeList)
                    {
                        if (string.IsNullOrEmpty(mInternaFilter))
                        {
                            mInternaFilter = "U_empId eq '" + item.empID + "'";
                        }
                        else
                        {
                            mInternaFilter = mInternaFilter + " or U_empId eq '" + item.empID + "'";
                        }
                    }

                    if (!string.IsNullOrEmpty(mInternaFilter))
                    {
                        mFilters = mFilters + " (" + mInternaFilter + ")";
                    }
                    else
                    {
                        mFilters = mFilters + " and (1 eq 0) ";
                    }

                    if (!pCatalogName.Equals(""))
                    {
                        mFilters = mFilters + string.Format(" and indexof(Code, '{0}') gt -1", pCatalogName.Trim());
                    }

                    if (!pCatalogName.Equals(""))
                    {
                        mFilters = mFilters + string.Format(" and indexof(Code, '{0}') gt -1", pCatalogName.Trim());
                    }

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_FASEMP", mFilters, "", true, false, false, 1, 20);
                    List<C_ARGNS_FASEMP> mEmployeeCatalogueList = serializer.Deserialize<List<C_ARGNS_FASEMP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mInternaFilter = string.Empty;

                    foreach (C_ARGNS_FASEMP item in mEmployeeCatalogueList)
                    {
                        if (string.IsNullOrEmpty(mInternaFilter))
                        {
                            mInternaFilter = "Code eq '" + item.U_FasCode + "'";
                        }
                        else
                        {
                            mInternaFilter = mInternaFilter + " or Code eq '" + item.U_FasCode + "'";
                        }
                    }

                    mFilters = mCodeFilters + " and U_Active eq 'Y'";

                    if (!string.IsNullOrEmpty(mInternaFilter))
                    {
                        mFilters = mFilters + " and (" + mInternaFilter + ")";
                    }
                    else
                    {
                        mFilters = mFilters + " and (1 eq 0) ";
                    }

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_FBOOK", mFilters);
                    mCatalogList = serializer.Deserialize<List<ARGNSCatalog>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                }
                else
                {
                    mFilters = mCodeFilters + " and U_Active eq 'Y'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ARGNS_FBOOK", mFilters);
                    mCatalogList = serializer.Deserialize<List<ARGNSCatalog>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }

                return mCatalogList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceSL -> GetCatalogListSearch :" + ex.Message);
                return null;
            }
        }

        private XmlDocument GetSoapStructure(bool pIsUpdate, object pHeader, object pBody)
        {

            XmlSerializer mySerializer = new XmlSerializer(pBody.GetType(), @"http://www.sap.com/SBO/DIS");
            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);
            xns.Add(string.Empty, @"http://www.sap.com/SBO/DIS");
            MemoryStream streamDocs = new MemoryStream();
            mySerializer.Serialize(streamDocs, pBody, xns);
            XmlDocument mXmlDocument = new XmlDocument();
            mXmlDocument.LoadXml(@"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Header></soap:Header><soap:Body><" + (pIsUpdate ? "Update" : "Add") + @" xmlns=""" + pHeader.GetType().GetProperty("ServiceName").GetValue(pHeader, null) + @"""></" + (pIsUpdate ? "Update" : "Add") + "></soap:Body></soap:Envelope>");
            XmlElement mBodyElement = mXmlDocument.DocumentElement;

            mBodyElement.SelectSingleNode("//*[local-name()='" + (pIsUpdate ? "Update" : "Add") + "']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            streamDocs = new MemoryStream();

            mySerializer = new XmlSerializer(pHeader.GetType(), @"http://www.sap.com/SBO/DIS");
            mySerializer.Serialize(streamDocs, pHeader, xns);

            mBodyElement.SelectSingleNode("//*[local-name()='Header']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            return mXmlDocument;

        }

        public string GetCurrency(string pCurrType, string pSession)
        {
            string mXmlResult = string.Empty;
            string ret = string.Empty;
            string pXmlString = string.Empty;
            string AddCmd = string.Empty;
            SBODI_Server.Node mServerNode = null;
            XmlDocument mResultXML = null;

            try
            {
                mResultXML = new XmlDocument();
                mServerNode = new SBODI_Server.Node();

                //Poner Enum
                if (pCurrType == WebServices.UtilWeb.CurrentType.SystemCurrency.ToString())
                {
                    AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                    @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                    "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                    @"<env:Body><dis:GetSystemCurrency xmlns:dis=""http://www.sap.com/SBO/DIS""></dis:GetSystemCurrency></env:Body></env:Envelope>";
                }
                else
                {
                    AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                    @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                     "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                    @"<env:Body><dis:GetLocalCurrency xmlns:dis=""http://www.sap.com/SBO/DIS""></dis:GetLocalCurrency></env:Body></env:Envelope>";

                }


                //Envio Solicitud al DI Server y Obtengo la Respuesta
                mXmlResult = mServerNode.Interact(AddCmd);
                mResultXML.LoadXml(mXmlResult);

                //Recupero los Valores de la Respuesta                
                return UtilWeb.GetResult(mResultXML, pCurrType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetCurrency:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }
    }
}