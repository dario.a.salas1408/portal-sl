﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.PDM.MaterialDetails;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace WebServices.MasterData.General
{
    [ServiceContract]
    public interface IMasterDataService
    {

        #region Common Method
        //[OperationContract]
        //List<ARGNSBrand> GetBrandList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSDivision> GetDivisionList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSModelGroup> GetModelGroupList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSProductLine> GetProductLineList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSSeason> GetSeasonList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSStyleStatus> GetStyleStatusList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSYear> GetYearList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ItemGroup> GetItemGroupList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<PriceList> GetPriceLiList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSCollection> GetCollectionList(CompanyConn pCompanyParam);

        //[OperationContract]
        //List<ARGNSSubCollection> GetSubCollectionList(CompanyConn pCompanyParam);

        [OperationContract]
        List<ARGNSSegmentation> GetSegmentationList(CompanyConn pCompanyParam);

        [OperationContract]
        ARGNSSegmentation GetSegmentationById(CompanyConn pCompanyParam, string pCode);

        [OperationContract]
        List<ARGNSModelFile> GetModelFileList(CompanyConn pCompanyParam, string pCode, string pU_ModCode);

        [OperationContract]
        List<ARGNSCrPath> GetModelProjectList(CompanyConn pCompanyParam, string pModCode);

        [OperationContract]
        List<ARGNSSize> GetScaleDescriptionListSearch(CompanyConn pCompanyParam, string pScaleCode = "");

        #endregion


        #region Product Data

        [OperationContract]
        List<ARGNSModel> GetPDMListSearch(CompanyConn pCompanyParam, ref PDMSAPCombo combo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "",
			int pStart = 0, int pLength = 0);

        [OperationContract]
        JsonObjectResult GetPDMListDesc(CompanyConn pCompanyParam, 
            int pStart, int pLength, string pCodeModel = "", 
            string pNameModel = "", string pSeasonModel = "", 
            string pGroupModel = "", string pBrand = "", 
            string pCollection = "", string pSubCollection = "", 
            string pCatalogCode = "");

        [OperationContract]
        ModelDesc GetModelDesc(CompanyConn pCompanyParam, string pModCode);

        [OperationContract]
        ARGNSModel GetModelById(CompanyConn pCompanyParam, string pCode, string pModCode, List<UDF_ARGNS> pListUDFModel = null);

        [OperationContract]
        string UpdateModel(ARGNSModel pObject, CompanyConn pCompanyParam);

        [OperationContract]
        JsonObjectResult AddModel(ARGNSModel pObject, CompanyConn pCompanyParam);

        [OperationContract]
        bool GetModelExist(CompanyConn pCompanyParam, string pModCode);

        #endregion


        #region Design

        [OperationContract]
        ARGNSModel GetDesignById(CompanyConn pCompanyParam, string pCode);

        [OperationContract]
        List<ARGNSModel> GetDesignListSearch(CompanyConn pCompanyParam, ref PDMSAPCombo combo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "", int pStart = 0, int pLength = 0);

        [OperationContract]
        string UpdateDesign(ARGNSModel pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string AddDesign(ARGNSModel pObject, CompanyConn pCompanyParam);

        #endregion


        #region Design

        [OperationContract]
        List<ARGNSModelLogo> GetLogoList(CompanyConn pCompanyParam, string pCode);

        [OperationContract]
        ARGNSLogo GetLogoById(CompanyConn pCompanyParam, string pCode);

        #endregion


        #region Color Master

        [OperationContract]
        ARGNSColor GetColorByCode(CompanyConn pCompanyParam, string pColorCode);

        [OperationContract]
        List<ARGNSColor> GetColorMasterListSearch(CompanyConn pCompanyParam, string pColorCode = "", string pColorName = "");

        [OperationContract]
        List<ARGNSModelColor> GetColorByModel(CompanyConn pCompanyParam, string pModel);

        [OperationContract]
        string AddColor(ARGNSColor pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateColor(ARGNSColor pObject, CompanyConn pCompanyParam);

        #endregion


        #region Scale Master

        [OperationContract]
        List<ARGNSScale> GetScaleMasterListSearch(CompanyConn pCompanyParam, string pScaleCode = "", string pScaleName = "");

        [OperationContract]
        List<ARGNSSize> GetSizeByScale(CompanyConn pCompanyParam, string pScaleCode = "", string pModeCode = "");

        #endregion


        #region Variable Master

        [OperationContract]
        List<ARGNSVariable> GetVariableListSearch(CompanyConn pCompanyParam, ref List<ARGNSSegmentation> pSegmentationList, string pVarCode = "", string pVarName = "", string pVarSegmentation = "");

        #endregion


        #region SKU & Prepacks

        [OperationContract]
        List<SkuModel> GetSkuModelList(CompanyConn pCompanyParam, string pModCode, ref List<ARGNSModelImg> pImgList);

        [OperationContract]
        List<ARGNSPrepack> GetModelPrepacks(CompanyConn pCompanyParam, string pModCode);

        [OperationContract]
        List<ARGNSSizeRun> GetSizeRunsByScale(CompanyConn pCompanyParam, string pScaleCode);
        #endregion


        #region Comment

        [OperationContract]
        List<ARGNSModelComment> GetModelComment(CompanyConn pCompanyParam, string pModCode);

        [OperationContract]
        string AddLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam);

        #endregion


        #region CostSheet

        [OperationContract]
        List<ARGNSCostSheet> GetModelCostSheet(CompanyConn pCompanyParam, string pModCode);

        [OperationContract]
        ARGNSCostSheet GetModelCostSheetByCode(CompanyConn pCompanyParam, string pCode, string pModelCode);

        [OperationContract]
        List<ARGNSCostSheet> GetCostSheetListSearch(CompanyConn pCompanyParam, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "");

        [OperationContract]
        List<ARGNSSchema> GetCSSchemaList(CompanyConn pCompanyParam, string pCode, string pName);

        [OperationContract]
        List<ARGNSSchemaLine> GetCSSchemaLineList(CompanyConn pCompanyParam, string pCode);
        
        [OperationContract]
        List<ARGNSOperTemplate> GetCSOperationTemplateList(CompanyConn pCompanyParam, string pCode, string pName);

        [OperationContract]
        List<ARGNSOperTemplateLine> GetCSOperationTemplateLineList(CompanyConn pCompanyParam, string pCode);

        [OperationContract]
        List<ARGNSPatternTemplate> GetCSPatternList(CompanyConn pCompanyParam, string pCode, string pName);

        [OperationContract]
        List<ARGNSPatternTemplateLine> GetCSPatternLineList(CompanyConn pCompanyParam, string pCode);

        [OperationContract]
        string UpdateCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam);

        [OperationContract]
        JsonObjectResult AddCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam);

        #endregion


        #region POM Master

        [OperationContract]
        List<ARGNSModelPom> GetModelPomList(CompanyConn pCompanyParam, string pModCode);

        [OperationContract]
        List<ARGNSModelPomTemplate> GetModelPomTemplateList(CompanyConn pCompanyParam, List<string> pSclcodeList, List<string> pPomCodeList);

        [OperationContract]
        List<ARGNSPomTemplateLines> GetModelPomTemplateLines(CompanyConn pCompanyParam, string pPOMCode);

        #endregion


        #region CriticalPath

        [OperationContract]
        List<ARGNSCrPath> GetCriticalPathListSearch(CompanyConn pCompanyParam, string pCollCode = "", string pSubCollCode = "", string pSeasonCode = "", string pModelCode = "", string pProjectCode = "", string pVendorCode = "", string pCustomerCode = "");

        #endregion

        #region Projects

        [OperationContract]
        ARGNSProject GetProjectByCode(CompanyConn pCompanyParam, string pCode, string pModelId);

        [OperationContract]
        List<ARGNSRoutingLine> GetWorkflowLines(CompanyConn pCompanyParam, string pWorkflow);

        [OperationContract]
        string UpdateActivityCodeInProjectLines(CompanyConn pCompanyParam, Dictionary<string, string> pActivityCodeList, string pProjectCode);

        [OperationContract]
        string UpdateProject(CompanyConn pCompanyParam, ARGNSProject pObject);

        [OperationContract]
        JsonObjectResult AddProject(CompanyConn pCompanyParam, ARGNSProject pObject);

        [OperationContract]
        string GetCriticalPathDefaultBP(CompanyConn pCompanyParam);
        #endregion

        #region MyStyles

        [OperationContract]
        List<ARGNSModel> GetBPPDMListSearch(CompanyConn pCompanyParam, string pBPCode, 
			ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "", 
			string pNameModel = "", string pSeasonModel = "", 
			string pGroupModel = "", string pBrand = "", 
			string pCollection = "", string pSubCollection = "", 
			int pStart = 0, int pLength = 0);

        [OperationContract]
        JsonObjectResult GetUserPDMListSearch(CompanyConn pCompanyParam, string pUserCode, 
			ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "", 
			string pNameModel = "", string pSeasonModel = "", 
			string pGroupModel = "", string pBrand = "", 
			string pCollection = "", string pSubCollection = "",
			int pStart = 0, int pLength = 0);

        [OperationContract]
        List<ARGNSCostSheet> GetUserCostSheetListSearch(CompanyConn pCompanyParam, string pUserCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "");

        [OperationContract]
        List<ARGNSCostSheet> GetBPCostSheetListSearch(CompanyConn pCompanyParam, string pBPCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "");

        #endregion


        #region Samples
        [OperationContract]
        List<ARGNSModelSample> GetModelSapmples(CompanyConn pCompanyParam, string pModCode);

        [OperationContract]
        ARGNSModelSample GetSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode);

        [OperationContract]
        string AddSample(ARGNSModelSample pObject, CompanyConn pCompanyParam);


        [OperationContract]
        string UpdateSample(ARGNSModelSample pObject, CompanyConn pCompanyParam);

        [OperationContract]
        ARGNSModelSample GetSampleByCodSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode);
        #endregion


        #region PDC

        [OperationContract]
        List<ARGNSPDC> GetPDCList(CompanyConn pCompanyParam, ref PDCSAPCombo combo, DateTime? pPD, string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "");

        [OperationContract]
        ARGNSPDC GetPDCById(CompanyConn pCompanyParam, string pPDCCode, ref PDCSAPCombo combo);

        [OperationContract]
        PDCSAPCombo GetPDCCombo(CompanyConn pCompanyParam);

        [OperationContract]
        ARGNSPDCLine GetPDCLineBarCode(CompanyConn pCompanyParam, string CodeBar);

        [OperationContract]
        List<ARGNSPDCLine> GetPDCLineCutTick(CompanyConn pCompanyParam, string CutTick);

        [OperationContract]
        string AddPDC(CompanyConn pCompanyParam, ARGNSPDC pPDC);

        #endregion


        #region Model LOGs

        [OperationContract]
        List<ARGNSModelHistory> GetModelHistoryList(CompanyConn pCompanyParam, string pModelCode = "");


        [OperationContract]
        List<SAPLogs> GetSAPModelLogs(CompanyConn pCompanyParam, string pModelCode = "", string pLogInst = "");


        #endregion


        #region Range Plan

        [OperationContract]
        List<ARGNSRangePlan> GetRangePlanListSearch(CompanyConn pCompanyParam, ref RangePlanCombo mRangePlanCombo, string pCode = "", string pSeason = "", string pCollection = "", string pEmployee = "");

        [OperationContract]
        ARGNSRangePlan GetRangePlanById(CompanyConn pCompanyParam, string pCode);

        [OperationContract]
        RangePlanCombo GetRangePlanCombo(CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateRangePlan(ARGNSRangePlan pObject, CompanyConn pCompanyParam);

        #endregion


        #region Material Details

        [OperationContract]
        ARGNSMaterialDetail GetMaterialDetails(CompanyConn pCompanyParam, MaterialDetailsUDF mMaterialDetailsUDF, string pModCode = "");

        [OperationContract]
        string UpdateMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail);

        [OperationContract]
        string AddMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail);

        #endregion


        #region Shipment

        [OperationContract]
        ARGNSContainer GetContainerById(CompanyConn pCompanyParam, int pDocEntry);

        [OperationContract]
        List<ARGNSContainer> GetContainerList(CompanyConn pCompanyParam, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA);

        [OperationContract]
        List<ARGNSContainer> GetMyShipmentListSearchBP(CompanyConn pCompanyParam, string pBP, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA);

        [OperationContract]
        List<ARGNSContainerPackage> GetContainerPackageList(CompanyConn pCompanyParam, int pDocEntry);

        [OperationContract]
        List<ARGNSContainerSetup> GetContainerSetupList(CompanyConn pCompanyParam);

        [OperationContract]
        List<ARGNSPSlipPackage> GetPSlipPackageList(CompanyConn pCompanyParam, int pContainerID, int pShipmentID);

        [OperationContract]
        List<ARGNSPSlipPackageType> GetPSlipPackageTypeList(CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment);

        [OperationContract]
        string AddShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment);

        [OperationContract]
        List<Draft> GetDocumentWhitOpenLines(CompanyConn pCompanyParam, string pObjType);

        [OperationContract]
        List<DraftLine> GetDocumentOpenLines(CompanyConn pCompanyParam, string pObjType, List<int> pDocEntryList);

        [OperationContract]
        List<Draft> GetShipmentDocument(CompanyConn pCompanyParam, string pShipmentID, string pObjType);

        [OperationContract]
        ShipmentMembers GetShipmentMembersObject(CompanyConn pCompanyParam);

        #endregion


        #region Tech Pack

        [OperationContract]
        ARGNSTechPack GetTechPack(CompanyConn pCompanyParam, int pId);

        [OperationContract]
        string UpdateTechPack(ARGNSTechPack pObject, CompanyConn pCompanyParam);

        #endregion

        [OperationContract]
        List<ARGNSCatalog> GetCatalogListSearch(CompanyConn pCompanyParam, string pCatalogCode = "", string pCatalogName = "", int? pSalesEmployee = null);
    }
}
