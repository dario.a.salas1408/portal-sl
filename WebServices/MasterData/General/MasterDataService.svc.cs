﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.PDM.MaterialDetails;

namespace WebServices.MasterData.General
{
	public class MasterDataService : IMasterDataService
	{
		private IMasterDataService mServFunction = null;
		public MasterDataService()
		{
		}
		private void InitializeService(CompanyConn pCompanyParam)
		{
			if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
			{
				mServFunction = new MasterDataServiceSL();
			}
			else
			{
				mServFunction = new MasterDataServiceDS();
			}
		}

		#region Common Method
		public List<ARGNSSegmentation> GetSegmentationList(CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetSegmentationList(pCompanyParam);
		}

		public ARGNSSegmentation GetSegmentationById(CompanyConn pCompanyParam, string pCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetSegmentationById(pCompanyParam, pCode);
		}

		public List<ARGNSModelFile> GetModelFileList(CompanyConn pCompanyParam, string pCode, string pU_ModCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetModelFileList(pCompanyParam, pCode, pU_ModCode);
		}

		public List<ARGNSCrPath> GetModelProjectList(CompanyConn pCompanyParam, string pModCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetModelProjectList(pCompanyParam, pModCode);
		}
		#endregion

		#region Product Data
		public List<ARGNSModel> GetPDMListSearch(CompanyConn pCompanyParam, ref PDMSAPCombo combo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "",
			int pStart = 0, int pLength = 0)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetPDMListSearch(pCompanyParam, ref combo, 
				pCodeModel, pNameModel, pSeasonModel, 
				pGroupModel, pBrand, pCollection, 
				pSubCollection, pStart, pLength);
		}

		public JsonObjectResult GetPDMListDesc(CompanyConn pCompanyParam, 
            int pStart, int pLength, string pCodeModel = "", 
            string pNameModel = "", string pSeasonModel = "", 
            string pGroupModel = "", string pBrand = "", 
            string pCollection = "", string pSubCollection = "", 
            string pCatalogCode = "")
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetPDMListDesc(pCompanyParam, pStart, pLength, 
                pCodeModel, pNameModel, pSeasonModel, pGroupModel, 
                pBrand, pCollection, pSubCollection, pCatalogCode);
		}

		public ModelDesc GetModelDesc(CompanyConn pCompanyParam, string pModCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetModelDesc(pCompanyParam, pModCode);
		}

		public ARGNSModel GetModelById(CompanyConn pCompanyParam, string pCode, string pModCode, List<UDF_ARGNS> pListUDFModel = null)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetModelById(pCompanyParam, pCode, pModCode, pListUDFModel);
		}

		public string UpdateModel(ARGNSModel pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateModel(pObject, pCompanyParam);
		}

		public JsonObjectResult AddModel(ARGNSModel pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.AddModel(pObject, pCompanyParam);
		}
		public bool GetModelExist(CompanyConn pCompanyParam, string pModCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetModelExist(pCompanyParam, pModCode);
		}

		#endregion

		#region Design
		public ARGNSModel GetDesignById(CompanyConn pCompanyParam, string pCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetDesignById(pCompanyParam, pCode);
		}

		public List<ARGNSModel> GetDesignListSearch(CompanyConn pCompanyParam, ref PDMSAPCombo combo,
			string pCodeModel = "", string pNameModel = "",
			string pSeasonModel = "", string pGroupModel = "",
			string pBrand = "", string pCollection = "",
			string pSubCollection = "", int pStart = 0, int pLength = 0)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetDesignListSearch(pCompanyParam, ref combo,
				pCodeModel, pNameModel, pSeasonModel,
				pGroupModel, pBrand,
				pCollection, pSubCollection,
				pStart, pLength);
		}

		public string AddDesign(ARGNSModel pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.AddDesign(pObject, pCompanyParam);
		}

		public string UpdateDesign(ARGNSModel pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateDesign(pObject, pCompanyParam);
		}

		#endregion

		#region Logos
		public List<ARGNSModelLogo> GetLogoList(CompanyConn pCompanyParam, string pCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetLogoList(pCompanyParam, pCode);
		}

		public ARGNSLogo GetLogoById(CompanyConn pCompanyParam, string pCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetLogoById(pCompanyParam, pCode);
		}
		#endregion

		#region Color Master

		public ARGNSColor GetColorByCode(CompanyConn pCompanyParam, string pColorCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetColorByCode(pCompanyParam, pColorCode);

		}

		public List<ARGNSColor> GetColorMasterListSearch(CompanyConn pCompanyParam, string pColorCode = "", string pColorName = "")
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetColorMasterListSearch(pCompanyParam, pColorCode, pColorName);
		}

		public List<ARGNSModelColor> GetColorByModel(CompanyConn pCompanyParam, string pModel)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetColorByModel(pCompanyParam, pModel);
		}

		public string AddColor(ARGNSColor pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.AddColor(pObject, pCompanyParam);
		}

		public string UpdateColor(ARGNSColor pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateColor(pObject, pCompanyParam);
		}

		#endregion

		#region Scale Master

		public List<ARGNSScale> GetScaleMasterListSearch(CompanyConn pCompanyParam, string pScaleCode = "", string pScaleName = "")
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetScaleMasterListSearch(pCompanyParam, pScaleCode, pScaleName);
		}

		public List<ARGNSSize> GetScaleDescriptionListSearch(CompanyConn pCompanyParam, string pScaleCode = "")
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetScaleDescriptionListSearch(pCompanyParam, pScaleCode);
		}

		public List<ARGNSSize> GetSizeByScale(CompanyConn pCompanyParam, string pScaleCode = "", string pModeCode = "")
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetSizeByScale(pCompanyParam, pScaleCode, pModeCode);
		}

		#endregion

		#region Variable Master
		public List<ARGNSVariable> GetVariableListSearch(CompanyConn pCompanyParam, ref List<ARGNSSegmentation> pSegmentationList, string pVarCode = "", string pVarName = "", string pVarSegmentation = "")
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetVariableListSearch(pCompanyParam, ref pSegmentationList, pVarCode, pVarName, pVarSegmentation);
		}
		#endregion

		#region SKU & Prepacks

		public List<SkuModel> GetSkuModelList(CompanyConn pCompanyParam, string pModCode, ref List<ARGNSModelImg> pImgList)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetSkuModelList(pCompanyParam, pModCode, ref pImgList);
		}

		public List<ARGNSPrepack> GetModelPrepacks(CompanyConn pCompanyParam, string pModCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetModelPrepacks(pCompanyParam, pModCode);
		}

		public List<ARGNSSizeRun> GetSizeRunsByScale(CompanyConn pCompanyParam, string pScaleCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetSizeRunsByScale(pCompanyParam, pScaleCode);
		}

		#endregion

		#region Comments
		public List<ARGNSModelComment> GetModelComment(CompanyConn pCompanyParam, string pModCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetModelComment(pCompanyParam, pModCode);
		}

		public string AddLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.AddLogCommentMaster(pObject, pCompanyParam);
		}

		public string UpdateLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateLogCommentMaster(pObject, pCompanyParam);
		}
		#endregion

		#region Cost Sheet

		public List<ARGNSCostSheet> GetModelCostSheet(CompanyConn pCompanyParam, string pModCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetModelCostSheet(pCompanyParam, pModCode);
		}

		public ARGNSCostSheet GetModelCostSheetByCode(CompanyConn pCompanyParam, string pCode, string pModelCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetModelCostSheetByCode(pCompanyParam, pCode, pModelCode);
		}

		public List<ARGNSCostSheet> GetCostSheetListSearch(CompanyConn pCompanyParam, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCostSheetListSearch(pCompanyParam, txtStyle, txtCodeCostSheet, txtDescription);
		}

		public List<ARGNSSchema> GetCSSchemaList(CompanyConn pCompanyParam, string pCode, string pName)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCSSchemaList(pCompanyParam, pCode, pName);
		}

		public List<ARGNSSchemaLine> GetCSSchemaLineList(CompanyConn pCompanyParam, string pCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCSSchemaLineList(pCompanyParam, pCode);
		}

		public List<ARGNSOperTemplate> GetCSOperationTemplateList(CompanyConn pCompanyParam, string pCode, string pName)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCSOperationTemplateList(pCompanyParam, pCode, pName);
		}

		public List<ARGNSOperTemplateLine> GetCSOperationTemplateLineList(CompanyConn pCompanyParam, string pCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCSOperationTemplateLineList(pCompanyParam, pCode);
		}

		public List<ARGNSPatternTemplate> GetCSPatternList(CompanyConn pCompanyParam, string pCode, string pName)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCSPatternList(pCompanyParam, pCode, pName);
		}

		public List<ARGNSPatternTemplateLine> GetCSPatternLineList(CompanyConn pCompanyParam, string pCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCSPatternLineList(pCompanyParam, pCode);
		}

		public string UpdateCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam)
		{
			InitializeService(pCompanyParam);
			return mServFunction.UpdateCostSheet(pObject, pCompanyParam);
		}

		public JsonObjectResult AddCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam)
		{
			InitializeService(pCompanyParam);
			return mServFunction.AddCostSheet(pObject, pCompanyParam);
		}
		#endregion

		#region POM Master

		public List<ARGNSModelPom> GetModelPomList(CompanyConn pCompanyParam, string pModCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetModelPomList(pCompanyParam, pModCode);
		}

		public List<ARGNSModelPomTemplate> GetModelPomTemplateList(CompanyConn pCompanyParam, List<string> pSclcodeList, List<string> pPomCodeList)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetModelPomTemplateList(pCompanyParam, pSclcodeList, pPomCodeList);
		}

		public List<ARGNSPomTemplateLines> GetModelPomTemplateLines(CompanyConn pCompanyParam, string pPOMCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetModelPomTemplateLines(pCompanyParam, pPOMCode);
		}

		#endregion

		public List<ARGNSCrPath> GetCriticalPathListSearch(CompanyConn pCompanyParam, string pCollCode = "", string pSubCollCode = "", string pSeasonCode = "", string pModelCode = "", string pProjectCode = "", string pVendorCode = "", string pCustomerCode = "")
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCriticalPathListSearch(pCompanyParam, pCollCode, pSubCollCode, pSeasonCode, pModelCode, pProjectCode, pVendorCode, pCustomerCode);
		}

		#region Projects

		public ARGNSProject GetProjectByCode(CompanyConn pCompanyParam, string pCode, string pModelId)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetProjectByCode(pCompanyParam, pCode, pModelId);
		}

		public List<ARGNSRoutingLine> GetWorkflowLines(CompanyConn pCompanyParam, string pWorkflow)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetWorkflowLines(pCompanyParam, pWorkflow);
		}

		public string UpdateActivityCodeInProjectLines(CompanyConn pCompanyParam, Dictionary<string, string> pActivityCodeList, string pProjectCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.UpdateActivityCodeInProjectLines(pCompanyParam, pActivityCodeList, pProjectCode);
		}

		public string UpdateProject(CompanyConn pCompanyParam, ARGNSProject pObject)
		{
			InitializeService(pCompanyParam);
			return mServFunction.UpdateProject(pCompanyParam, pObject);
		}

		public JsonObjectResult AddProject(CompanyConn pCompanyParam, ARGNSProject pObject)
		{
			InitializeService(pCompanyParam);
			return mServFunction.AddProject(pCompanyParam, pObject);
		}

		public string GetCriticalPathDefaultBP(CompanyConn pCompanyParam)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCriticalPathDefaultBP(pCompanyParam);
		}

		#endregion

		#region MyStyles

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pBPCode"></param>
		/// <param name="mPDMSAPCombo"></param>
		/// <param name="pCodeModel"></param>
		/// <param name="pNameModel"></param>
		/// <param name="pSeasonModel"></param>
		/// <param name="pGroupModel"></param>
		/// <param name="pBrand"></param>
		/// <param name="pCollection"></param>
		/// <param name="pSubCollection"></param>
		/// <returns></returns>
		public List<ARGNSModel> GetBPPDMListSearch(CompanyConn pCompanyParam, string pBPCode, 
			ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "", 
			string pNameModel = "", string pSeasonModel = "", 
			string pGroupModel = "", string pBrand = "", 
			string pCollection = "", string pSubCollection = "", 
			int pStart = 0, int pLength = 0)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetBPPDMListSearch(pCompanyParam, pBPCode, 
				ref mPDMSAPCombo, pCodeModel, pNameModel, 
				pSeasonModel, pGroupModel, pBrand, 
				pCollection, pSubCollection, pStart, pLength);
		}

		public JsonObjectResult GetUserPDMListSearch(CompanyConn pCompanyParam, string pUserCode, 
			ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "", 
			string pNameModel = "", string pSeasonModel = "", 
			string pGroupModel = "", string pBrand = "", string pCollection = "", 
			string pSubCollection = "", int pStart = 0, int pLength = 0)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetUserPDMListSearch(pCompanyParam, pUserCode, 
				ref mPDMSAPCombo, pCodeModel, pNameModel, pSeasonModel, 
				pGroupModel, pBrand, pCollection, pSubCollection, 
				pStart, pLength );
		}

		public List<ARGNSCostSheet> GetUserCostSheetListSearch(CompanyConn pCompanyParam, string pUserCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetUserCostSheetListSearch(pCompanyParam, pUserCode, txtStyle, txtCodeCostSheet, txtDescription);
		}

		public List<ARGNSCostSheet> GetBPCostSheetListSearch(CompanyConn pCompanyParam, string pBPCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetBPCostSheetListSearch(pCompanyParam, pBPCode, txtStyle, txtCodeCostSheet, txtDescription);
		}

		#endregion


		public List<ARGNSModelSample> GetModelSapmples(CompanyConn pCompanyParam, string pModCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetModelSapmples(pCompanyParam, pModCode);
		}

		public ARGNSModelSample GetSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetSample(pCompanyParam, pSampleCode, pModCode);
		}

		public string AddSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
		{
			InitializeService(pCompanyParam);
			return mServFunction.AddSample(pObject, pCompanyParam);
		}

		public string UpdateSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
		{
			InitializeService(pCompanyParam);
			return mServFunction.UpdateSample(pObject, pCompanyParam);
		}

		public ARGNSModelSample GetSampleByCodSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetSampleByCodSample(pCompanyParam, pSampleCode, pModCode);
		}

		public List<ARGNSPDC> GetPDCList(CompanyConn pCompanyParam, ref PDCSAPCombo combo, DateTime? pPD, string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "")
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetPDCList(pCompanyParam, ref combo, pPD, pCode, pEmployee, pShift, pVendor);
		}

		public ARGNSPDC GetPDCById(CompanyConn pCompanyParam, string pPDCCode, ref PDCSAPCombo combo)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetPDCById(pCompanyParam, pPDCCode, ref combo);
		}

		public PDCSAPCombo GetPDCCombo(CompanyConn pCompanyParam)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetPDCCombo(pCompanyParam);
		}

		public ARGNSPDCLine GetPDCLineBarCode(CompanyConn pCompanyParam, string CodeBar)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetPDCLineBarCode(pCompanyParam, CodeBar);
		}

		public List<ARGNSPDCLine> GetPDCLineCutTick(CompanyConn pCompanyParam, string CutTick)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetPDCLineCutTick(pCompanyParam, CutTick);
		}

		public string AddPDC(CompanyConn pCompanyParam, ARGNSPDC pPDC)
		{
			InitializeService(pCompanyParam);
			return mServFunction.AddPDC(pCompanyParam, pPDC);
		}

		#region Model LOGs

		public List<ARGNSModelHistory> GetModelHistoryList(CompanyConn pCompanyParam, string pModelCode = "")
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetModelHistoryList(pCompanyParam, pModelCode);

		}


		public List<SAPLogs> GetSAPModelLogs(CompanyConn pCompanyParam, string pModelCode = "", string pLogInst = "")
		{

			InitializeService(pCompanyParam);
			return mServFunction.GetSAPModelLogs(pCompanyParam, pModelCode, pLogInst);

		}
		#endregion


		#region Range Plan

		public List<ARGNSRangePlan> GetRangePlanListSearch(CompanyConn pCompanyParam, ref RangePlanCombo mRangePlanCombo, string pCode = "", string pSeason = "", string pCollection = "", string pEmployee = "")
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetRangePlanListSearch(pCompanyParam, ref mRangePlanCombo, pCode, pSeason, pCollection, pEmployee);
		}

		public ARGNSRangePlan GetRangePlanById(CompanyConn pCompanyParam, string pCode)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetRangePlanById(pCompanyParam, pCode);
		}

		public RangePlanCombo GetRangePlanCombo(CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetRangePlanCombo(pCompanyParam);
		}

		public string UpdateRangePlan(ARGNSRangePlan pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateRangePlan(pObject, pCompanyParam);
		}

		#endregion


		#region Material Detail

		public ARGNSMaterialDetail GetMaterialDetails(CompanyConn pCompanyParam, MaterialDetailsUDF mMaterialDetailsUDF, string pModCode = "")
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetMaterialDetails(pCompanyParam, mMaterialDetailsUDF, pModCode);
		}

		public string UpdateMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateMaterialDetail(pCompanyParam, pMaterialDetail);
		}

		public string AddMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.AddMaterialDetail(pCompanyParam, pMaterialDetail);
		}
		#endregion


		#region Shipment

		public ARGNSContainer GetContainerById(CompanyConn pCompanyParam, int pDocEntry)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetContainerById(pCompanyParam, pDocEntry);
		}

		public List<ARGNSContainer> GetContainerList(CompanyConn pCompanyParam, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetContainerList(pCompanyParam, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA);
		}

		public List<ARGNSContainer> GetMyShipmentListSearchBP(CompanyConn pCompanyParam, string pBP, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetMyShipmentListSearchBP(pCompanyParam, pBP, txtShipmentNum, txtStatus, txtPOE, dateASD, dateADA);
		}

		public List<ARGNSContainerPackage> GetContainerPackageList(CompanyConn pCompanyParam, int pDocEntry)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetContainerPackageList(pCompanyParam, pDocEntry);
		}

		public List<ARGNSContainerSetup> GetContainerSetupList(CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetContainerSetupList(pCompanyParam);
		}

		public List<ARGNSPSlipPackage> GetPSlipPackageList(CompanyConn pCompanyParam, int pContainerID, int pShipmentID)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetPSlipPackageList(pCompanyParam, pContainerID, pShipmentID);
		}

		public List<ARGNSPSlipPackageType> GetPSlipPackageTypeList(CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetPSlipPackageTypeList(pCompanyParam);
		}

		public string UpdateShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateShipment(pCompanyParam, pShipment);
		}

		public string AddShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.AddShipment(pCompanyParam, pShipment);
		}

		public List<Draft> GetDocumentWhitOpenLines(CompanyConn pCompanyParam, string pObjType)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetDocumentWhitOpenLines(pCompanyParam, pObjType);
		}

		public List<DraftLine> GetDocumentOpenLines(CompanyConn pCompanyParam, string pObjType, List<int> pDocEntryList)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetDocumentOpenLines(pCompanyParam, pObjType, pDocEntryList);
		}

		public List<Draft> GetShipmentDocument(CompanyConn pCompanyParam, string pShipmentID, string pObjType)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetShipmentDocument(pCompanyParam, pShipmentID, pObjType);
		}

		public ShipmentMembers GetShipmentMembersObject(CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetShipmentMembersObject(pCompanyParam);
		}

		#endregion

		#region Tech Pack

		public ARGNSTechPack GetTechPack(CompanyConn pCompanyParam, int pId)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.GetTechPack(pCompanyParam, pId);
		}

		public string UpdateTechPack(ARGNSTechPack pObject, CompanyConn pCompanyParam)
		{
			this.InitializeService(pCompanyParam);
			return mServFunction.UpdateTechPack(pObject, pCompanyParam);
		}

		#endregion

		public List<ARGNSCatalog> GetCatalogListSearch(CompanyConn pCompanyParam, string pCatalogCode = "", string pCatalogName = "", int? pSalesEmployee = null)
		{
			InitializeService(pCompanyParam);
			return mServFunction.GetCatalogListSearch(pCompanyParam, pCatalogCode, pCatalogName, pSalesEmployee);
		}
	}
}
