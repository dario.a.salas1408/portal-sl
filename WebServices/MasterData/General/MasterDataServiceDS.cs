﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations;
using WebServices.Model;
using AutoMapper;
using System.Data.Entity;
using WebServices.Model.Tables;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using WebServices.User;
using System.Collections;
using System.Xml;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.PDM.MaterialDetails;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using ARGNS.View;

namespace WebServices.MasterData.General
{
	public class MasterDataServiceDS : IMasterDataService
    {
        private DataBase mDbContext;
        private B1WSHandler mB1WSHandler;

        public MasterDataServiceDS()
        {
            Database.SetInitializer<DataBase>(null);
            mDbContext = new DataBase();
            mB1WSHandler = new B1WSHandler();
        }

        #region  Common Method
        public List<ARGNSSegmentation> GetSegmentationList(CompanyConn pCompanyParam)
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_APPTEXGRPS, ARGNSSegmentation>();
                return Mapper.Map<List<ARGNSSegmentation>>(mDbContext.C_ARGNS_APPTEXGRPS.ToList());
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetSegmentationListDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public ARGNSSegmentation GetSegmentationById(CompanyConn pCompanyParam, string pCode)
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_APPTEXGRPS, ARGNSSegmentation>();

                return Mapper.Map<ARGNSSegmentation>(mDbContext.C_ARGNS_APPTEXGRPS.Where(c => c.Code == pCode && c.U_Active == "Y").SingleOrDefault());
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetSegmentationById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSModelFile> GetModelFileList(CompanyConn pCompanyParam, string pCode, string pU_ModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_MODEL_FILE, ARGNSModelFile>();
                List<ARGNSModelFile> FileList = Mapper.Map<List<ARGNSModelFile>>(mDbContext.C_ARGNS_MODEL_FILE.Where(c => c.Code == pCode).ToList());
                FileList = FileList.Select(c => { c.U_ModCode = pU_ModCode; return c; }).ToList();
                return FileList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetModelFileListDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSCrPath> GetModelProjectList(CompanyConn pCompanyParam, string pModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_CRPATH, ARGNSCrPath>();
                return Mapper.Map<List<ARGNSCrPath>>(mDbContext.C_ARGNS_CRPATH.Where(c => c.U_Model == pModCode).ToList());
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetModelProjectListDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        #endregion

        #region Product Data

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="combo"></param>
		/// <param name="pCodeModel"></param>
		/// <param name="pNameModel"></param>
		/// <param name="pSeasonModel"></param>
		/// <param name="pGroupModel"></param>
		/// <param name="pBrand"></param>
		/// <param name="pCollection"></param>
		/// <param name="pSubCollection"></param>
		/// <param name="pStart"></param>
		/// <param name="pLength"></param>
		/// <returns></returns>
        public List<ARGNSModel> GetPDMListSearch(CompanyConn pCompanyParam, ref PDMSAPCombo combo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "",
			int pStart = 0, int pLength = 0)
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<C_ARGNS_MODEL, ARGNSModel>();

                //List<OPOR> ListRestu = mDbContext.OPOR.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) || (pDate == null ? c.DocDate != null : (c.DocDate == pDate))).ToList();
                ARGNSSeason mSeason = Mapper.Map<ARGNSSeason>(mDbContext.C_ARGNS_SEASON.Where(c => c.Name.ToUpper().Contains(pSeasonModel.ToUpper())).FirstOrDefault());
                string mSeasonCode = (mSeason == null ? null : mSeason.Code);
                ARGNSModelGroup mGroupModelList = Mapper.Map<ARGNSModelGroup>(mDbContext.C_ARGNS_MODELGRP.Where(c => c.Name.ToUpper().Contains(pGroupModel.ToUpper())).FirstOrDefault());
                string mModelGroupCode = (mGroupModelList == null ? null : mGroupModelList.Code);
                ARGNSBrand mBrandList = Mapper.Map<ARGNSBrand>(mDbContext.C_ARGNS_BRAND.Where(c => c.Name.ToUpper().Contains(pBrand.ToUpper())).FirstOrDefault());
                string mBrandCode = (mBrandList == null ? null : mBrandList.Code);
                ARGNSCollection mCollList = Mapper.Map<ARGNSCollection>(mDbContext.C_ARGNS_COLLECTION.Where(c => c.U_Desc.ToUpper().Contains(pCollection.ToUpper())).FirstOrDefault());
                string mCollCode = (mCollList == null ? null : mCollList.U_CollCode);
                ARGNSSubCollection mSubCollList = Mapper.Map<ARGNSSubCollection>(mDbContext.C_ARGNS_AMBIENT.Where(c => c.U_Desc.ToUpper().Contains(pSubCollection.ToUpper())).FirstOrDefault());
                string mSubCollCode = (mSubCollList == null ? null : mSubCollList.U_AmbCode);

                //string code = (mCode == null ? null : mCode.Code);
                //TODO Aca voy a llamar al metodo que trae todos los modelos y despues voy a filtrar en base a los anteriores parametros en visual estudio
                List<C_ARGNS_MODEL> listRestu = mDbContext.C_ARGNS_MODEL
					.Where(c => (c.U_ModCode.Contains(pCodeModel)) && 
							(pNameModel != "" ? c.U_ModDesc.Contains(pNameModel) : true) && 
							(pSeasonModel != "" ? c.U_Season.Contains(mSeasonCode) : true) && 
							(pGroupModel != "" ? c.U_ModGrp.Contains(mModelGroupCode) : true) && 
							(pBrand != "" ? c.U_Brand.Contains(mBrandCode) : true) && 
							(pCollection != "" ? c.U_CollCode.Contains(mCollCode) : true) 
							&& (pSubCollection != "" ? c.U_AmbCode.Contains(mSubCollCode) : true) && (c.U_Status != "-1"))
							.OrderBy(o=>o.U_ModCode)
							.ToList();

                //Lleno el combo con los datos
                combo.ListBrand = Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList());
                combo.ListCollection = Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList());
                combo.ListModelGroup = Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList());
                combo.ListSeason = Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList());
                combo.ListSubCollection = Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList());

                return Mapper.Map<List<ARGNSModel>>(listRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceDS -> GetPDMListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pCodeModel"></param>
		/// <param name="pNameModel"></param>
		/// <param name="pSeasonModel"></param>
		/// <param name="pGroupModel"></param>
		/// <param name="pBrand"></param>
		/// <param name="pCollection"></param>
		/// <param name="pSubCollection"></param>
		/// <param name="pCatalogCode"></param>
		/// <returns></returns>
        public JsonObjectResult GetPDMListDesc(CompanyConn pCompanyParam, 
            int pStart, int pLength, 
            string pCodeModel = "", 
            string pNameModel = "", 
            string pSeasonModel = "", 
            string pGroupModel = "", 
            string pBrand = "", 
            string pCollection = "", 
            string pSubCollection = "", 
            string pCatalogCode = "")
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //TODO Idem al anteior metodo pero creando un nuevo objeto ModelDesc
                //Obtengo Todos Los Modelos por Filtro
                mJsonObjectResult.ModelDescViewList = (from mModel in mDbContext.C_ARGNS_MODEL
                                                       join mSea in mDbContext.C_ARGNS_SEASON on mModel.U_Season equals mSea.Code into group1
                                                       from mGroup1 in group1.DefaultIfEmpty()
                                                       join mColl in mDbContext.C_ARGNS_COLLECTION on mModel.U_CollCode equals mColl.U_CollCode into group2
                                                       from mGroup2 in group2.DefaultIfEmpty()
                                                       join mSubColl in mDbContext.C_ARGNS_AMBIENT on mModel.U_AmbCode equals mSubColl.U_AmbCode into group3
                                                       from mGroup3 in group3.DefaultIfEmpty()
                                                       join mBrand in mDbContext.C_ARGNS_BRAND on mModel.U_Brand equals mBrand.Code into group4
                                                       from mGroup4 in group4.DefaultIfEmpty()
                                                       join mProdGrp in mDbContext.C_ARGNS_MODELGRP on mModel.U_ModGrp equals mProdGrp.Code into group5
                                                       from mGroup5 in group5.DefaultIfEmpty()
                                                       join mCatalog in mDbContext.C_ARGNS_FBOOKD on mModel.U_ModCode equals mCatalog.U_ModCode into group6
                                                       from mGroup6 in group6.DefaultIfEmpty()
                                                       where
                                                       (
                                                             (string.IsNullOrEmpty(pCodeModel) ? true : mModel.U_ModCode.ToUpper().Contains(pCodeModel.ToUpper()))
                                                          && (string.IsNullOrEmpty(pNameModel) ? true : mModel.U_ModDesc.ToUpper().Contains(pNameModel.ToUpper()))
                                                           && (string.IsNullOrEmpty(pSeasonModel) ? true : mGroup1.Name.ToUpper().Contains(pSeasonModel.ToUpper()))
                                                           && (string.IsNullOrEmpty(pCollection) ? true : mGroup2.U_Desc.ToUpper().Contains(pCollection.ToUpper()))
                                                           && (string.IsNullOrEmpty(pSubCollection) ? true : mGroup3.U_Desc.ToUpper().Contains(pSubCollection.ToUpper()))
                                                           && (string.IsNullOrEmpty(pBrand) ? true : mGroup4.Name.ToUpper().Contains(pBrand.ToUpper()))
                                                           && (string.IsNullOrEmpty(pGroupModel) ? true : mGroup5.Name.ToUpper().Contains(pGroupModel.ToUpper()))
                                                           && (string.IsNullOrEmpty(pCatalogCode) ? true : mGroup6.U_ModCode == mModel.U_ModCode)
                                                           && (string.IsNullOrEmpty(pCatalogCode) ? true : mGroup6.Code.ToUpper().Contains(pCatalogCode.ToUpper()))
                                                           && (mModel.U_Status != "-1")
                                                       )
                                                       select new ModelDesc()
                                                       {
                                                           Code = mModel.Code,
                                                           ModelCode = mModel.U_ModCode,
                                                           ModelImage = mModel.U_Pic,
                                                           Modelprice = mModel.U_Price ?? 0,
                                                           ModDescription = mModel.U_ModDesc,
                                                           SeasonDesc = mGroup1.Name,
                                                           CollDesc = mGroup2.U_Desc,
                                                           SubCollDesc = mGroup3.U_Desc,
                                                           BrandDesc = mGroup4.Name,
                                                           ProdGroupDesc = mGroup5.Name,
                                                           CatalogCode = pCatalogCode
                                                       })
                                                       .OrderBy(o => o.ModelCode)
                                                       .Skip(pStart).Take(pLength)
                                                       .ToList();

                int mCounter = (from mModel in mDbContext.C_ARGNS_MODEL
                                join mSea in mDbContext.C_ARGNS_SEASON on mModel.U_Season equals mSea.Code into group1
                                from mGroup1 in group1.DefaultIfEmpty()
                                join mColl in mDbContext.C_ARGNS_COLLECTION on mModel.U_CollCode equals mColl.U_CollCode into group2
                                from mGroup2 in group2.DefaultIfEmpty()
                                join mSubColl in mDbContext.C_ARGNS_AMBIENT on mModel.U_AmbCode equals mSubColl.U_AmbCode into group3
                                from mGroup3 in group3.DefaultIfEmpty()
                                join mBrand in mDbContext.C_ARGNS_BRAND on mModel.U_Brand equals mBrand.Code into group4
                                from mGroup4 in group4.DefaultIfEmpty()
                                join mProdGrp in mDbContext.C_ARGNS_MODELGRP on mModel.U_ModGrp equals mProdGrp.Code into group5
                                from mGroup5 in group5.DefaultIfEmpty()
                                join mCatalog in mDbContext.C_ARGNS_FBOOKD on mModel.U_ModCode equals mCatalog.U_ModCode into group6
                                from mGroup6 in group6.DefaultIfEmpty()
                                where
                                (
                                      (string.IsNullOrEmpty(pCodeModel) ? true : mModel.U_ModCode.ToUpper().Contains(pCodeModel.ToUpper()))
                                   && (string.IsNullOrEmpty(pNameModel) ? true : mModel.U_ModDesc.ToUpper().Contains(pNameModel.ToUpper()))
                                    && (string.IsNullOrEmpty(pSeasonModel) ? true : mGroup1.Name.ToUpper().Contains(pSeasonModel.ToUpper()))
                                    && (string.IsNullOrEmpty(pCollection) ? true : mGroup2.U_Desc.ToUpper().Contains(pCollection.ToUpper()))
                                    && (string.IsNullOrEmpty(pSubCollection) ? true : mGroup3.U_Desc.ToUpper().Contains(pSubCollection.ToUpper()))
                                    && (string.IsNullOrEmpty(pBrand) ? true : mGroup4.Name.ToUpper().Contains(pBrand.ToUpper()))
                                    && (string.IsNullOrEmpty(pGroupModel) ? true : mGroup5.Name.ToUpper().Contains(pGroupModel.ToUpper()))
                                    && (string.IsNullOrEmpty(pCatalogCode) ? true : mGroup6.U_ModCode == mModel.U_ModCode)
                                    && (string.IsNullOrEmpty(pCatalogCode) ? true : mGroup6.Code.ToUpper().Contains(pCatalogCode.ToUpper()))
                                    && (mModel.U_Status != "-1")
                                )
                                select new ModelDesc()
                                {
                                    Code = mModel.Code,
                                }).Count();

                mJsonObjectResult.Others.Add("TotalRecords", mCounter.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceDS -> GetPDMListDesc :" + ex.Message);
                return new JsonObjectResult();
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pModCode"></param>
		/// <returns></returns>
        public ModelDesc GetModelDesc(CompanyConn pCompanyParam, string pModCode)
        {
            List<ModelDesc> ret = null;
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //TODO Idem al anteior metodo pero creando un nuevo objeto ModelDesc
                //Obtengo Todos Los Modelos por Filtro
                ret = (from mModel in mDbContext.C_ARGNS_MODEL
                       join mSea in mDbContext.C_ARGNS_SEASON on mModel.U_Season equals mSea.Code into group1
                       from mGroup1 in group1.DefaultIfEmpty()
                       join mColl in mDbContext.C_ARGNS_COLLECTION on mModel.U_CollCode equals mColl.U_CollCode into group2
                       from mGroup2 in group2.DefaultIfEmpty()
                       join mSubColl in mDbContext.C_ARGNS_AMBIENT on mModel.U_AmbCode equals mSubColl.U_AmbCode into group3
                       from mGroup3 in group3.DefaultIfEmpty()
                       join mBrand in mDbContext.C_ARGNS_BRAND on mModel.U_Brand equals mBrand.Code into group4
                       from mGroup4 in group4.DefaultIfEmpty()
                       join mProdGrp in mDbContext.C_ARGNS_MODELGRP on mModel.U_ModGrp equals mProdGrp.Code into group5
                       from mGroup5 in group5.DefaultIfEmpty()
                       join mCatalog in mDbContext.C_ARGNS_FBOOKD on mModel.U_ModCode equals mCatalog.U_ModCode into group6
                       from mGroup6 in group6.DefaultIfEmpty()
                       where
                       (
                             mModel.U_ModCode == pModCode && (mModel.U_Status != "-1")
                       )
                       select new ModelDesc()
                       {
                           Code = mModel.Code,
                           ModelCode = mModel.U_ModCode,
                           ModDescription = mModel.U_ModDesc,
                           SeasonDesc = mGroup1.Name,
                           CollDesc = mGroup2.U_Desc,
                           SubCollDesc = mGroup3.U_Desc,
                           BrandDesc = mGroup4.Name,
                           ProdGroupDesc = mGroup5.Name
                       }).ToList();

                return ret.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelDesc :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pCode"></param>
		/// <param name="pModCode"></param>
		/// <param name="pListUDFModel"></param>
		/// <returns></returns>
        public ARGNSModel GetModelById(CompanyConn pCompanyParam, string pCode, string pModCode, List<UDF_ARGNS> pListUDFModel = null)
        {
            ARGNSModel ret = null;
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                string mConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.ConnectionString = mConnectionString;
                mDbContext.Database.Connection.Open();

                #region Mapper declaration
                Mapper.CreateMap<C_ARGNS_MODEL, ARGNSModel>();
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<C_ARGNS_DIV, ARGNSDivision>();
                Mapper.CreateMap<OITB, ItemGroup>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<C_ARGNS_PRODLINE, ARGNSProductLine>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<C_ARGNS_STYLE_STATUS, ARGNSStyleStatus>();
                Mapper.CreateMap<C_ARGNS_YEAR, ARGNSYear>();
                Mapper.CreateMap<OPLN, PriceListSAP>();
                Mapper.CreateMap<C_ARGNS_APPTEXGRPS, ARGNSSegmentation>();
                Mapper.CreateMap<C_ARGNS_APRTEX_SETUP, AdministrationApparel>();
                Mapper.CreateMap<C_ARGNS_MODELPOM, ARGNSModelPom>();
                Mapper.CreateMap<C_ARGNS_MODEL_SIZE, ARGNSModelSize>();
                Mapper.CreateMap<C_ARGNS_MODEL_VAR, ARGNSModelVar>();
                Mapper.CreateMap<C_ARGNS_MODEL_IMG, ARGNSModelImg>();
                Mapper.CreateMap<C_ARGNS_MODEL_FILE, ARGNSModelFile>();
                Mapper.CreateMap<C_ARGNS_MODEL_COLOR, ARGNSModelColor>();
                Mapper.CreateMap<C_ARGNS_MODEL_BOM, ARGNSModelBom>();
                Mapper.CreateMap<C_ARGNS_MODEL_FITT, ARGNSModelFitt>();
                Mapper.CreateMap<C_ARGNS_MODEL_INST, ARGNSModelInst>();
                Mapper.CreateMap<C_ARGNS_MODELWFLOW, ARGNSModelWFLOW>();
                Mapper.CreateMap<C_ARGNS_MODEL_LOGOS, ARGNSModelLogos>();
                Mapper.CreateMap<C_ARGNS_MODEL_CS, ARGNSModelCostSheet>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<C_ARGNS_COMPOSITION, ARGNSComposition>();
                Mapper.CreateMap<C_ARGNS_SIMBGRP, ARGNSSimbol>();
                Mapper.CreateMap<OCRD, BusinessPartnerSAP>();
                Mapper.CreateMap<OCRN, CurrencySAP>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                Mapper.CreateMap<UFD1_SAP, UFD1>();
                Mapper.CreateMap<UFD1, UFD1_SAP>();
                #endregion

                string mWhere = "[@ARGNS_MODEL].[Code] = " + pCode;
                C_ARGNS_MODEL mC_ARGNS_MODEL = UDFUtil.GetObjectListWithUDF(pListUDFModel, mWhere, typeof(C_ARGNS_MODEL), mConnectionString, "@ARGNS_MODEL").Cast<C_ARGNS_MODEL>().FirstOrDefault();
                ret = Mapper.Map<ARGNSModel>(mC_ARGNS_MODEL);

                if (ret != null)
                {
                    ret.ModelSizeList = Mapper.Map<List<ARGNSModelSize>>(mDbContext.C_ARGNS_MODEL_SIZE.Where(c => c.Code == pCode).ToList());
                    ret.ModelVariableList = Mapper.Map<List<ARGNSModelVar>>(mDbContext.C_ARGNS_MODEL_VAR.Where(c => c.Code == pCode).ToList());
                    ret.ModelImageList = Mapper.Map<List<ARGNSModelImg>>(mDbContext.C_ARGNS_MODEL_IMG.Where(c => c.Code == pCode).ToList());
                    ret.ModelFileList = Mapper.Map<List<ARGNSModelFile>>(mDbContext.C_ARGNS_MODEL_FILE.Where(c => c.Code == pCode).ToList());

                    List<ARGNSModelColor> mColorList = (from mCol in mDbContext.C_ARGNS_COLOR
                                                        join mMolCol in mDbContext.C_ARGNS_MODEL_COLOR on mCol.U_ColCode equals mMolCol.U_ColCode
                                                        where mMolCol.Code == pCode
                                                        select new ARGNSModelColor
                                                        {
                                                            LineId = mMolCol.LineId,
                                                            U_ColCode = mCol.U_ColCode,
                                                            ColorDesc = mCol.U_ColDesc,
                                                            U_Active = mMolCol.U_Active,
                                                            U_Argb = mCol.U_Argb
                                                        }).ToList();
                    ret.ModelColorList = mColorList;
                    //TODO hasta aca se migraron las vistas a hana
                    List<StockModel> mStockModel = (from mOITB in mDbContext.OITW
                                                    join mOITM in mDbContext.OITM on mOITB.ItemCode equals mOITM.ItemCode
                                                    join mScale in mDbContext.C_ARGNS_SCALE on mOITM.U_ARGNS_SCL equals mScale.U_SclCode into group3
                                                    from mGroup3 in group3.DefaultIfEmpty()
                                                    join mSize in mDbContext.C_ARGNS_SIZE on mGroup3.Code equals mSize.Code into group4
                                                    from mGroup4 in group4.DefaultIfEmpty()
                                                    join mColor in mDbContext.C_ARGNS_COLOR on mOITM.U_ARGNS_COL equals mColor.U_ColCode into group1
                                                    from mGroup1 in group1.DefaultIfEmpty()
                                                    join mVariable in mDbContext.C_ARGNS_VARIABLE on mOITM.U_ARGNS_VAR equals mVariable.U_VarCode into group2
                                                    from mGroup2 in group2.DefaultIfEmpty()
                                                    where mOITM.U_ARGNS_MOD == pModCode &&
                                                    (!string.IsNullOrEmpty(mOITM.U_ARGNS_SIZE) ? mOITM.U_ARGNS_SIZE == mGroup4.U_SizeCode : true) &&
                                                    mOITM.U_ARGNS_ITYPE != "P"
                                                    select new StockModel()
                                                    {
                                                        ItemCode = mOITB.ItemCode,
                                                        WhsCode = mOITB.WhsCode,
                                                        OnHand = mOITB.OnHand,
                                                        IsCommited = mOITB.IsCommited,
                                                        OnOrder = mOITB.OnOrder,
                                                        U_ARGNS_SIZE = mOITM.U_ARGNS_SIZE,
                                                        U_ARGNS_SIZEVO = mGroup4.U_VOrder,
                                                        U_ARGNS_COL = mOITM.U_ARGNS_COL,
                                                        U_ARGNS_VAR = mOITM.U_ARGNS_VAR

                                                    }).ToList();

                    ret.ModelStockList = mStockModel;
                    ret.administrationApparel = Mapper.Map<AdministrationApparel>(mDbContext.C_ARGNS_APRTEX_SETUP.SingleOrDefault());
                    ret.administrationSAP = (from mOADM in mDbContext.OADM
                                             select new AdministrationSAP()
                                             {
                                                 Code = mOADM.Code,
                                                 TimeFormat = mOADM.TimeFormat,
                                                 DateFormat = mOADM.DateFormat,
                                                 DateSep = mOADM.DateSep,
                                                 SumDec = mOADM.SumDec,
                                                 PriceDec = mOADM.PriceDec,
                                                 RateDec = mOADM.RateDec,
                                                 QtyDec = mOADM.QtyDec,
                                                 PercentDec = mOADM.PercentDec,
                                                 MeasureDec = mOADM.MeasureDec,
                                                 QueryDec = mOADM.QueryDec,
                                                 DecSep = mOADM.DecSep,
                                             }).SingleOrDefault();

                    ret.ModelPomList = Mapper.Map<List<ARGNSModelPom>>(mDbContext.C_ARGNS_MODELPOM.Where(c => c.Code == pCode).ToList());
                    // ret.ModelColorList = Mapper.Map<List<ARGNSModelColor>>(mDbContext.C_ARGNS_MODEL_COLOR.Where(c => c.Code == pCode).ToList());
                    ret.ModelBomList = Mapper.Map<List<ARGNSModelBom>>(mDbContext.C_ARGNS_MODEL_BOM.Where(c => c.Code == pCode).ToList());
                    ret.ModelFittList = Mapper.Map<List<ARGNSModelFitt>>(mDbContext.C_ARGNS_MODEL_FITT.Where(c => c.Code == pCode).ToList());
                    ret.ModelInstList = Mapper.Map<List<ARGNSModelInst>>(mDbContext.C_ARGNS_MODEL_INST.Where(c => c.Code == pCode).ToList());
                    ret.ModelWFLOWList = Mapper.Map<List<ARGNSModelWFLOW>>(mDbContext.C_ARGNS_MODELWFLOW.Where(c => c.Code == pCode).ToList());
                    ret.ModelLogosList = Mapper.Map<List<ARGNSModelLogos>>(mDbContext.C_ARGNS_MODEL_LOGOS.Where(c => c.Code == pCode).ToList());
                    ret.ModelCSList = Mapper.Map<List<ARGNSModelCostSheet>>(mDbContext.C_ARGNS_MODEL_CS.Where(c => c.Code == pCode).ToList());
                    ret.ModelSegmentation = Mapper.Map<ARGNSSegmentation>(mDbContext.C_ARGNS_APPTEXGRPS.Where(c => c.Code == ret.U_ATGrp && c.U_Active == "Y").SingleOrDefault());

                }
                else
                {
                    ret = new ARGNSModel();
                }

                //Lista de Todos los Combos del Modelo
                mPDMSAPCombo.ListBrand.AddRange(Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList()));
                mPDMSAPCombo.ListDivision.AddRange(Mapper.Map<List<ARGNSDivision>>(mDbContext.C_ARGNS_DIV.ToList()));
                mPDMSAPCombo.ListItemGroup = Mapper.Map<List<ItemGroup>>(mDbContext.OITB.ToList());
                mPDMSAPCombo.ListModelGroup.AddRange(Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList()));
                mPDMSAPCombo.ListProductLine.AddRange(Mapper.Map<List<ARGNSProductLine>>(mDbContext.C_ARGNS_PRODLINE.ToList()));
                mPDMSAPCombo.ListSeason.AddRange(Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList()));
                mPDMSAPCombo.ListStatus.AddRange(Mapper.Map<List<ARGNSStyleStatus>>(mDbContext.C_ARGNS_STYLE_STATUS.ToList()));
                mPDMSAPCombo.ListYear.AddRange(Mapper.Map<List<ARGNSYear>>(mDbContext.C_ARGNS_YEAR.ToList()));
                mPDMSAPCombo.PriceList.AddRange(Mapper.Map<List<PriceListSAP>>(mDbContext.OPLN.ToList()));
                mPDMSAPCombo.ListSegmentation.AddRange(Mapper.Map<List<ARGNSSegmentation>>(mDbContext.C_ARGNS_APPTEXGRPS.ToList()));
                mPDMSAPCombo.ListCollection.AddRange(Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList()));
                mPDMSAPCombo.ListSubCollection.AddRange(Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList()));
                mPDMSAPCombo.CompositionList.AddRange(Mapper.Map<List<ARGNSComposition>>(mDbContext.C_ARGNS_COMPOSITION.ToList()));
                mPDMSAPCombo.SimbolList.AddRange(Mapper.Map<List<ARGNSSimbol>>(mDbContext.C_ARGNS_SIMBGRP.ToList()));
                mPDMSAPCombo.BusinessPartnerList.AddRange(Mapper.Map<List<BusinessPartnerSAP>>(mDbContext.OCRD.ToList()));
                mPDMSAPCombo.CurrencyList.AddRange(Mapper.Map<List<CurrencySAP>>(mDbContext.OCRN.ToList()));
                var mDesigner = (from mDesig in mDbContext.OHEM
                                 select new EmployeeSAP()
                                 {
                                     empID = mDesig.empID,
                                     firstName = mDesig.firstName,
                                     lastName = mDesig.lastName,
                                     completeName = mDesig.lastName + " " + mDesig.firstName

                                 }).ToList();
                mPDMSAPCombo.DesignerList.AddRange(mDesigner);

                mPDMSAPCombo.DocNumbering = (from mDocNum in mDbContext.C_ARGNS_DOCNUMBER
                                             join mDocLine in mDbContext.C_ARGNS_DOCNUMBER_LIN on mDocNum.Code equals mDocLine.Code
                                             where mDocNum.Code == "1" // Agregar Un ENUM
                                             select new ARGNSDocNumber
                                             {
                                                 Code = mDocLine.LineId.ToString(),
                                                 U_Desc = mDocLine.U_Desc
                                             }).ToList();

                ret.ListPDMSAPCombo = mPDMSAPCombo;

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetModelById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pObject"></param>
		/// <param name="pCompanyParam"></param>
		/// <returns></returns>
        public string UpdateModel(ARGNSModel pObject, CompanyConn pCompanyParam)
        {

            int counter;
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;
            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = (long)(pObject.U_PList.HasValue ? pObject.U_PList : 1);
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;

                #region Child

                //Sizes
                if (pObject.ModelSizeList.Count > 0)
                {
                    foreach (ARGNSModelSize item in pObject.ModelSizeList)
                    {

                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE mSizeModel = oArgnsModel.ARGNS_MODEL_SIZECollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mSizeModel.U_SizeCod = item.U_SizeCod;
                        mSizeModel.U_Selected = item.U_Selected;
                        mSizeModel.U_SclCode = item.U_SclCode;
                        if (item.U_VOrder != null)
                            mSizeModel.U_VOrder = (long)item.U_VOrder;
                    }

                }

                //Color
                //counter = 0;
                if (pObject.ModelColorList.Count > 0)
                {

                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR> mColorList = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR mColorModel;

                    foreach (ARGNSModelColor item in pObject.ModelColorList)
                    {
                        mColorModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR();

                        mColorModel.LineIdSpecified = true;
                        mColorModel.LineId = item.LineId;
                        mColorModel.U_ColCode = item.U_ColCode;
                        mColorModel.U_Active = item.U_Active;
                        mColorList.Add(mColorModel);
                        //counter++;
                    }

                    oArgnsModel.ARGNS_MODEL_COLORCollection = mColorList.ToArray();
                }


                //Variable
                if (pObject.ModelVariableList.Count > 0)
                {
                    foreach (ARGNSModelVar item in pObject.ModelVariableList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR mVarModel = oArgnsModel.ARGNS_MODEL_VARCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mVarModel.U_VarCode = item.U_VarCode;
                        mVarModel.U_Selected = item.U_Selected;
                        mVarModel.U_VarDesc = item.U_VarDesc;
                    }

                }

                //Image
                if (pObject.ModelImageList.Count > 0)
                {

                    oArgnsModel.ARGNS_MODEL_IMGCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG[pObject.ModelImageList.Count];
                    counter = 0;
                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG mImageModelAdd = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG() { Code = item.Code, LineId = item.LineId, LineIdSpecified = true, U_File = item.U_File, U_FType = item.U_FType, U_Path = item.U_Path };
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter] = mImageModelAdd;
                        counter++;
                    }

                }


                //Attachment
                if (pObject.ModelFileList.Count > 0)
                {

                    oArgnsModel.ARGNS_MODEL_FILECollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FILE[pObject.ModelFileList.Count];
                    counter = 0;
                    foreach (ARGNSModelFile item in pObject.ModelFileList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FILE mFileModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FILE();
                        mFileModel.U_File = item.U_File;
                        mFileModel.U_Path = item.U_Path;
                        mFileModel.U_FType = "FIL";
                        mFileModel.U_DesignTool = "";
                        oArgnsModel.ARGNS_MODEL_FILECollection[counter] = mFileModel;

                    }

                }

                //ModelInstruction
                if (pObject.ModelInstList.Count > 0)
                {
                    foreach (ARGNSModelInst item in pObject.ModelInstList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST mModelInst = oArgnsModel.ARGNS_MODEL_INSTCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mModelInst.U_Sector = item.U_Sector;
                        mModelInst.U_ModCode = item.U_ModCode;
                        mModelInst.U_InstCod = item.U_InstCod;
                        mModelInst.U_Instuct = item.U_Instuct;
                        mModelInst.U_Descrip = item.U_Descrip;
                        mModelInst.U_Imag = item.U_Imag;
                        mModelInst.U_Version = item.U_Version;
                        mModelInst.U_Active = item.U_Active;
                    }

                }

                //ModelFitt
                if (pObject.ModelFittList.Count > 0)
                {
                    foreach (ARGNSModelFitt item in pObject.ModelFittList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_FITT mModelFitt = oArgnsModel.ARGNS_MODEL_FITTCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                        mModelFitt.U_ModCode = item.U_ModCode;
                        mModelFitt.U_FittCod = item.U_FittCod;
                        mModelFitt.U_Fitting = item.U_Fitting;
                    }
                }


                //ModelBom                
                if (pObject.ModelBomList.Count > 0)
                {
                    foreach (ARGNSModelBom item in pObject.ModelBomList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_BOM mModelBom = oArgnsModel.ARGNS_MODEL_BOMCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();

                        mModelBom.U_ItemCode = item.U_ItemCode;
                        mModelBom.U_Quantity = item.U_Quantity.HasValue ? Convert.ToDouble(item.U_Quantity.Value, System.Globalization.CultureInfo.InvariantCulture) : 0;
                        mModelBom.U_UOM = item.U_UOM;
                        mModelBom.U_Whse = item.U_Whse;
                        mModelBom.U_IssueMth = item.U_IssueMth;
                        mModelBom.U_PList = item.U_PList.HasValue ? (long)item.U_PList.Value : 0;
                        mModelBom.U_UPrice = item.U_UPrice.HasValue ? Convert.ToDouble(item.U_UPrice.Value, System.Globalization.CultureInfo.InvariantCulture) : 0;
                        mModelBom.U_Total = item.U_Total.HasValue ? Convert.ToDouble(item.U_Total.Value, System.Globalization.CultureInfo.InvariantCulture) : 0;
                        mModelBom.U_Comments = item.U_Comments;
                        mModelBom.U_TreeType = item.U_TreeType;
                        mModelBom.U_PVendor = item.U_PVendor;
                    }
                }


                ////POM
                //if (pObject.ModelPomList.Count > 0)
                //{
                //    foreach (ARGNSModelPom item in pObject.ModelPomList)
                //    {
                //        ARGNSModelServiceWeb.ArgnsModelARGNS_MODELPOM mPomModel = oArgnsModel.ARGNS_MODELPOMCollection.Where(c => c.LineId == item.LineId).FirstOrDefault();
                //        if (item.U_Value != null)
                //            mPomModel.U_Value = (double)item.U_Value;
                //        if (item.U_TolPosit != null)
                //            mPomModel.U_TolPosit = (double)item.U_TolPosit;
                //        if (item.U_TolNeg != null)
                //            mPomModel.U_TolNeg = (double)item.U_TolNeg;
                //        mPomModel.U_QAPoint = !string.IsNullOrEmpty(item.U_QAPoint.Trim()) ? item.U_QAPoint : "N";
                //    }

                //}

                if (pObject.ModelPomList.Count > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mPOMSAP in oArgnsModel.ARGNS_MODELPOMCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSModelPom mPOM = pObject.ModelPomList.Where(c => c.LineId == mPOMSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPOM != null)
                        {
                            if (mPOM.U_Value != null)
                            {
                                mPOMSAP.U_Value = (double)mPOM.U_Value;
                                mPOMSAP.U_ValueSpecified = true;
                            }
                            if (mPOM.U_TolPosit != null)
                            {
                                mPOMSAP.U_TolPosit = (double)mPOM.U_TolPosit;
                                mPOMSAP.U_TolPositSpecified = true;
                            }
                            if (mPOM.U_TolNeg != null)
                            {
                                mPOMSAP.U_TolNeg = (double)mPOM.U_TolNeg;
                                mPOMSAP.U_TolNegSpecified = true;
                            }
                            mPOMSAP.U_QAPoint = !string.IsNullOrEmpty(mPOM.U_QAPoint.Trim()) ? mPOM.U_QAPoint : "N";
                        }
                    }

                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODELPOM[] newLines = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODELPOM[oArgnsModel.ARGNS_MODELPOMCollection.Length + pObject.ModelPomList.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsModel.ARGNS_MODELPOMCollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsModel.ARGNS_MODELPOMCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSModelPom mPOM in pObject.ModelPomList.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODELPOM linePOM = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODELPOM();
                        linePOM.LineId = mPOM.LineId;
                        linePOM.LineIdSpecified = true;
                        linePOM.U_POM = mPOM.U_POM;
                        linePOM.U_Desc = mPOM.U_Desc;
                        linePOM.U_SizeCode = mPOM.U_SizeCode;
                        linePOM.U_SizeDesc = mPOM.U_SizeDesc;
                        if (mPOM.U_Value != null)
                        {
                            linePOM.U_Value = (double)mPOM.U_Value;
                            linePOM.U_ValueSpecified = true;
                        }
                        if (mPOM.U_TolPosit != null)
                        {
                            linePOM.U_TolPosit = (double)mPOM.U_TolPosit;
                            linePOM.U_TolPositSpecified = true;
                        }
                        if (mPOM.U_TolNeg != null)
                        {
                            linePOM.U_TolNeg = (double)mPOM.U_TolNeg;
                            linePOM.U_TolNegSpecified = true;
                        }
                        linePOM.U_QAPoint = !string.IsNullOrEmpty(mPOM.U_QAPoint.Trim()) ? mPOM.U_QAPoint : "N";
                        linePOM.U_SclPom = mPOM.U_SclPom;
                        linePOM.U_PomCode = mPOM.U_PomCode;
                        newLines[positionNewLine] = linePOM;
                        positionNewLine += 1;
                    }
                    oArgnsModel.ARGNS_MODELPOMCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODELPOM[newLines.Length];
                    oArgnsModel.ARGNS_MODELPOMCollection = newLines;
                }

                #endregion

                XmlDocument mXmlDoc = Utils.GetSoapStructure(true, oArgnsModelMsgHeader, oArgnsModel);
                if (pObject.MappedUdf.Count > 0)
                {
                    mXmlDoc.SelectSingleNode("//*[local-name()='ArgnsModel']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='ArgnsModel']").InnerXml, pObject.MappedUdf);
                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pObject"></param>
		/// <param name="pCompanyParam"></param>
		/// <returns></returns>
        public JsonObjectResult AddModel(ARGNSModel pObject, CompanyConn pCompanyParam)
        {
            int counter;
            string ret = string.Empty;
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Obtengo la Lista de Precio Por Defecto
                short mDefaultList = mDbContext.C_ARGNS_APRTEX_SETUP.SingleOrDefault().U_BOMDSPL ?? 0;

                oArgnsModel.Code = (mDbContext.C_ARGNS_MODEL.Max(c => c.DocEntry) + 1).ToString();
                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = mDefaultList;
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;



                #region Child

                //Sizes
                if (pObject.ModelSizeList.Count > 0)
                {
                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE> mSizeList = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE mSizeModel;

                    foreach (ARGNSModelSize item in pObject.ModelSizeList)
                    {

                        mSizeModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_SIZE();
                        mSizeModel.U_SizeCod = item.U_SizeCod;
                        mSizeModel.U_Selected = item.U_Selected;
                        mSizeModel.U_SclCode = item.U_SclCode;
                        if (item.U_VOrder != null)
                            mSizeModel.U_VOrder = (long)item.U_VOrder;

                        mSizeList.Add(mSizeModel);
                    }

                    oArgnsModel.ARGNS_MODEL_SIZECollection = mSizeList.ToArray();
                }

                //Color               
                if (pObject.ModelColorList.Count > 0)
                {
                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR> mColorList = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR mColorModel;

                    foreach (ARGNSModelColor item in pObject.ModelColorList)
                    {
                        mColorModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_COLOR();

                        mColorModel.LineIdSpecified = true;
                        mColorModel.LineId = item.LineId;
                        mColorModel.U_ColCode = item.U_ColCode;
                        mColorModel.U_Active = item.U_Active;
                        mColorList.Add(mColorModel);
                    }
                    oArgnsModel.ARGNS_MODEL_COLORCollection = mColorList.ToArray();
                }


                //Variable
                if (pObject.ModelVariableList.Count > 0)
                {

                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR> mVarList = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR mVarModel;

                    foreach (ARGNSModelVar item in pObject.ModelVariableList)
                    {
                        mVarModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_VAR();
                        mVarModel.U_VarCode = item.U_VarCode;
                        mVarModel.U_Selected = item.U_Selected;
                        mVarModel.U_VarDesc = item.U_VarDesc;

                        mVarList.Add(mVarModel);
                    }

                    oArgnsModel.ARGNS_MODEL_VARCollection = mVarList.ToArray();
                }

                //Image
                if (pObject.ModelImageList.Count > 0)
                {
                    oArgnsModel.ARGNS_MODEL_IMGCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG[pObject.ModelImageList.Count];
                    counter = 0;
                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG mImageModelAdd = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG() { Code = item.Code, LineId = item.LineId, LineIdSpecified = true, U_File = item.U_File, U_FType = item.U_FType, U_Path = item.U_Path };
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter] = mImageModelAdd;
                        counter++;
                    }
                }

                #endregion

                //Inserto las Listas de Precios
                CreatePriceList(pObject.U_ModCode, mDefaultList, Convert.ToDouble(pObject.U_Price), pCompanyParam.DSSessionId);

                WebServices.ARGNSModelServiceWeb.ArgnsModelParams mModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
                mModelParams = oArgnsModelService.Add(oArgnsModel);
                mJsonObjectResult.Code = mModelParams.Code;
                mJsonObjectResult.Others.Add("U_ModCode", mDbContext.C_ARGNS_MODEL.Where(c => c.Code == mModelParams.Code).FirstOrDefault().U_ModCode);
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
            return mJsonObjectResult;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pModCode"></param>
		/// <returns></returns>
        public bool GetModelExist(CompanyConn pCompanyParam, string pModCode)
        {
            bool ret = true;
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = (mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pModCode).ToList().Count() != 0) ? true : false;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetModelExist :" + ex.Message);
                ret = true;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
            return ret;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pModCode"></param>
		/// <param name="pPriceList"></param>
		/// <param name="pPrice"></param>
		/// <param name="pSession"></param>
		/// <returns></returns>
        public string CreatePriceList(string pModCode, short pPriceList, double pPrice, string pSession)
        {
            SBODI_Server.Node mServerNode = null;
            XmlDocument mResultXML = null;
            XmlNodeList mElement;
            string ret = string.Empty;
            string mQuery;
            try
            {
                mResultXML = new XmlDocument();
                mServerNode = new SBODI_Server.Node();


                //Obtengo Todas as Listas de Precio de SAP
                mQuery = "SELECT A.LISTNUM as LISTNUM , A.LISTNAME as LISTNAME, (SELECT B.ListName FROM OPLN B WHERE B.ListNum = A.BASE_NUM) As 'BASE_Name', A.Factor as Factor, (SELECT B.ListNum FROM OPLN B WHERE B.ListNum = A.BASE_NUM) As 'BASE_NUM' FROM  OPLN A ORDER BY A.LISTNUM";
                mResultXML.LoadXml(mServerNode.Interact(UtilWeb.ExecuteQuery(pSession, mQuery)));
                mElement = mResultXML.GetElementsByTagName("row");

                if (mElement.Count > 0)
                {
                    string mCode;
                    string mInsertQuery = "INSERT INTO [dbo].[@ARGNS_MODLIST] (Code,Name ,U_ModCode,U_ListNum,U_ListName,U_Price,U_DefList,U_BasList ,U_BASE_NUM ,U_Factor,U_Manual) VALUES ('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}','{8}','{9}','{10}')";
                    foreach (XmlNode item in mElement)
                    {
                        mCode = (mDbContext.C_ARGNS_MODLIST.ToList().Max(c => Convert.ToInt32(c.Code)) + 1).ToString();

                        if (pPriceList == Convert.ToInt32(item["LISTNUM"].InnerText))
                        {
                            mQuery = string.Format(mInsertQuery, mCode, mCode, pModCode, item["LISTNUM"].InnerText, item["LISTNAME"].InnerText, pPrice, "Y", item["BASE_Name"].InnerText, item["BASE_NUM"].InnerText, item["Factor"].InnerText, "Y");
                            mResultXML.LoadXml(mServerNode.Interact(UtilWeb.ExecuteQuery(pSession, mQuery)));
                        }
                        else
                        {
                            mQuery = string.Format(mInsertQuery, mCode, mCode, pModCode, item["LISTNUM"].InnerText, item["LISTNAME"].InnerText, 0, "N", item["BASE_Name"].InnerText, item["BASE_NUM"].InnerText, item["Factor"].InnerText, "N");
                            mResultXML.LoadXml(mServerNode.Interact(UtilWeb.ExecuteQuery(pSession, mQuery)));
                        }
                    }
                }

                ret = "Ok";
            }
            catch (Exception ex)
            {
                ret = "Error:" + ex.Message;
            }
            finally
            {
                Utils.ReleaseObject((object)mServerNode);
            }

            return ret;
        }

        #endregion  

        #region Design

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pCode"></param>
		/// <returns></returns>
        public ARGNSModel GetDesignById(CompanyConn pCompanyParam, string pCode)
        {
            ARGNSModel ret = null;
            PDMSAPCombo mPDMSAPCombo = new PDMSAPCombo();
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                #region Mapper declaration
                Mapper.CreateMap<C_ARGNS_MODEL, ARGNSModel>();
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<C_ARGNS_DIV, ARGNSDivision>();
                Mapper.CreateMap<OITB, ItemGroup>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<C_ARGNS_PRODLINE, ARGNSProductLine>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<C_ARGNS_STYLE_STATUS, ARGNSStyleStatus>();
                Mapper.CreateMap<C_ARGNS_YEAR, ARGNSYear>();
                Mapper.CreateMap<OPLN, PriceListSAP>();
                Mapper.CreateMap<C_ARGNS_APPTEXGRPS, ARGNSSegmentation>();
                Mapper.CreateMap<C_ARGNS_APRTEX_SETUP, AdministrationApparel>();
                Mapper.CreateMap<C_ARGNS_MODEL_IMG, ARGNSModelImg>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<C_ARGNS_COMPOSITION, ARGNSComposition>();
                Mapper.CreateMap<C_ARGNS_SIMBOLS, ARGNSSimbol>();
                Mapper.CreateMap<OCRD, BusinessPartnerSAP>();
                Mapper.CreateMap<OCRN, CurrencySAP>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                #endregion

                ret = Mapper.Map<ARGNSModel>(mDbContext.C_ARGNS_MODEL.Where(c => c.Code == pCode).SingleOrDefault());

                if (ret != null)
                {
                    ret.ModelImageList = Mapper.Map<List<ARGNSModelImg>>(mDbContext.C_ARGNS_MODEL_IMG.Where(c => c.Code == pCode).ToList());
                }
                else
                {
                    ret = new ARGNSModel();
                }
                //Lista de Todos los Combos del Modelo
                mPDMSAPCombo.ListBrand.AddRange(Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList()));
                mPDMSAPCombo.ListDivision.AddRange(Mapper.Map<List<ARGNSDivision>>(mDbContext.C_ARGNS_DIV.ToList()));
                mPDMSAPCombo.ListItemGroup = Mapper.Map<List<ItemGroup>>(mDbContext.OITB.ToList());
                mPDMSAPCombo.ListModelGroup.AddRange(Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList()));
                mPDMSAPCombo.ListProductLine.AddRange(Mapper.Map<List<ARGNSProductLine>>(mDbContext.C_ARGNS_PRODLINE.ToList()));
                mPDMSAPCombo.ListSeason.AddRange(Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList()));
                mPDMSAPCombo.ListStatus.AddRange(Mapper.Map<List<ARGNSStyleStatus>>(mDbContext.C_ARGNS_STYLE_STATUS.ToList()));
                mPDMSAPCombo.ListYear.AddRange(Mapper.Map<List<ARGNSYear>>(mDbContext.C_ARGNS_YEAR.ToList()));
                mPDMSAPCombo.PriceList.AddRange(Mapper.Map<List<PriceListSAP>>(mDbContext.OPLN.ToList()));
                mPDMSAPCombo.ListSegmentation.AddRange(Mapper.Map<List<ARGNSSegmentation>>(mDbContext.C_ARGNS_APPTEXGRPS.ToList()));
                mPDMSAPCombo.ListCollection.AddRange(Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList()));
                mPDMSAPCombo.ListSubCollection.AddRange(Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList()));
                mPDMSAPCombo.CompositionList.AddRange(Mapper.Map<List<ARGNSComposition>>(mDbContext.C_ARGNS_COMPOSITION.ToList()));
                mPDMSAPCombo.SimbolList.AddRange(Mapper.Map<List<ARGNSSimbol>>(mDbContext.C_ARGNS_SIMBOLS.ToList()));
                mPDMSAPCombo.BusinessPartnerList.AddRange(Mapper.Map<List<BusinessPartnerSAP>>(mDbContext.OCRD.ToList()));
                mPDMSAPCombo.CurrencyList.AddRange(Mapper.Map<List<CurrencySAP>>(mDbContext.OCRN.ToList()));
                var mDesigner = (from mDesig in mDbContext.OHEM
                                 select new EmployeeSAP()
                                 {
                                     empID = mDesig.empID,
                                     firstName = mDesig.firstName,
                                     lastName = mDesig.lastName,
                                     completeName = mDesig.lastName + " " + mDesig.firstName

                                 }).ToList();
                mPDMSAPCombo.DesignerList.AddRange(mDesigner);
                ret.ListPDMSAPCombo = mPDMSAPCombo;

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetDesignByIdDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="combo"></param>
		/// <param name="pCodeModel"></param>
		/// <param name="pNameModel"></param>
		/// <param name="pSeasonModel"></param>
		/// <param name="pGroupModel"></param>
		/// <param name="pBrand"></param>
		/// <param name="pCollection"></param>
		/// <param name="pSubCollection"></param>
		/// <param name="pStart"></param>
		/// <param name="pLength"></param>
		/// <returns></returns>
        public List<ARGNSModel> GetDesignListSearch(CompanyConn pCompanyParam, 
			ref PDMSAPCombo combo, 
			string pCodeModel = "", string pNameModel = "", 
			string pSeasonModel = "", string pGroupModel = "", 
			string pBrand = "", string pCollection = "", 
			string pSubCollection = "", int pStart = 0, int pLength = 0)
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<C_ARGNS_MODEL, ARGNSModel>();

                //List<OPOR> ListRestu = mDbContext.OPOR.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) || (pDate == null ? c.DocDate != null : (c.DocDate == pDate))).ToList();
                ARGNSSeason mSeason = Mapper.Map<ARGNSSeason>(mDbContext.C_ARGNS_SEASON.Where(c => c.Name.ToUpper().Contains(pSeasonModel.ToUpper())).FirstOrDefault());
                string mSeasonCode = (mSeason == null ? null : mSeason.Code);
                ARGNSModelGroup mGroupModelList = Mapper.Map<ARGNSModelGroup>(mDbContext.C_ARGNS_MODELGRP.Where(c => c.Name.ToUpper().Contains(pGroupModel.ToUpper())).FirstOrDefault());
                string mModelGroupCode = (mGroupModelList == null ? null : mGroupModelList.Code);
                ARGNSBrand mBrandList = Mapper.Map<ARGNSBrand>(mDbContext.C_ARGNS_BRAND.Where(c => c.Name.ToUpper().Contains(pBrand.ToUpper())).FirstOrDefault());
                string mBrandCode = (mBrandList == null ? null : mBrandList.Code);
                ARGNSCollection mCollList = Mapper.Map<ARGNSCollection>(mDbContext.C_ARGNS_COLLECTION.Where(c => c.U_Desc.ToUpper().Contains(pCollection.ToUpper())).FirstOrDefault());
                string mCollCode = (mCollList == null ? null : mCollList.U_CollCode);
                ARGNSSubCollection mSubCollList = Mapper.Map<ARGNSSubCollection>(mDbContext.C_ARGNS_AMBIENT.Where(c => c.U_Desc.ToUpper().Contains(pSubCollection.ToUpper())).FirstOrDefault());
                string mSubCollCode = (mSubCollList == null ? null : mSubCollList.U_AmbCode);

                //string code = (mCode == null ? null : mCode.Code);
                //TODO Aca voy a llamar al metodo que trae todos los modelos y despues voy a filtrar en base a los anteriores parametros en visual estudio
                List<C_ARGNS_MODEL> listRestu = mDbContext.C_ARGNS_MODEL
					.Where(c => (c.U_ModCode.Contains(pCodeModel)) && 
					(pNameModel != "" ? c.U_ModDesc.Contains(pNameModel) : true) && 
					(pSeasonModel != "" ? c.U_Season.Contains(mSeasonCode) : true) && 
					(pGroupModel != "" ? c.U_ModGrp.Contains(mModelGroupCode) : true) && 
					(pBrand != "" ? c.U_Brand.Contains(mBrandCode) : true) && 
					(pCollection != "" ? c.U_CollCode.Contains(mCollCode) : true) && 
					(pSubCollection != "" ? c.U_AmbCode.Contains(mSubCollCode) : true) && 
					(c.U_Status.Contains("-1")))
					.OrderBy(o=> o.U_ModCode)
					.ToList();

                //Lleno el combo con los datos
                combo.ListBrand = Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList());
                combo.ListCollection = Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList());
                combo.ListModelGroup = Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList());
                combo.ListSeason = Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList());
                combo.ListSubCollection = Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList());

                return Mapper.Map<List<ARGNSModel>>(listRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceDS -> GetDesignListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pObject"></param>
		/// <param name="pCompanyParam"></param>
		/// <returns></returns>
        public string AddDesign(ARGNSModel pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                oArgnsModel.Code = (mDbContext.C_ARGNS_MODEL.Max(c => c.DocEntry) + 1).ToString();
                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = (long)(pObject.U_PList.HasValue ? pObject.U_PList : 1);
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;

                int counter = 0;
                //Image
                if (pObject.ModelImageList.Count > 0)
                {
                    oArgnsModel.ARGNS_MODEL_IMGCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG[pObject.ModelImageList.Count];
                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter] = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG();
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].LineIdSpecified = true;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_ModCode = item.U_ModCode;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_File = item.U_File;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_FType = item.U_FType;
                        oArgnsModel.ARGNS_MODEL_IMGCollection[counter].U_Path = item.U_Path;
                        counter++;
                    }
                }

                oArgnsModelService.Add(oArgnsModel);
                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pObject"></param>
		/// <param name="pCompanyParam"></param>
		/// <returns></returns>
        public string UpdateDesign(ARGNSModel pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                oArgnsModel.Name = pObject.Name;
                oArgnsModel.U_ModCode = pObject.U_ModCode;
                oArgnsModel.U_ModDesc = pObject.U_ModDesc;
                oArgnsModel.U_DocNumb = pObject.U_DocNumb;
                oArgnsModel.U_Status = string.IsNullOrEmpty(pObject.U_Status) ? "2" : pObject.U_Status;
                oArgnsModel.U_Currency = pObject.U_Currency;
                oArgnsModel.U_GrpSCod = pObject.U_GrpSCod;
                oArgnsModel.U_Customer = pObject.U_Customer;
                oArgnsModel.U_Designer = pObject.U_Designer;
                oArgnsModel.U_Brand = pObject.U_Brand;
                oArgnsModel.U_AmbCode = pObject.U_AmbCode;
                oArgnsModel.U_CompCode = pObject.U_CompCode;
                oArgnsModel.U_CollCode = pObject.U_CollCode;
                oArgnsModel.U_Price = (double)(pObject.U_Price.HasValue ? pObject.U_Price.Value : 0);
                oArgnsModel.U_PicR = pObject.U_PicR;
                oArgnsModel.U_Approved = string.IsNullOrEmpty(pObject.U_Approved) ? "Y" : pObject.U_Approved;
                oArgnsModel.U_Owner = pObject.U_Owner;
                oArgnsModel.U_Comments = pObject.U_Comments;
                oArgnsModel.U_MainWhs = pObject.U_MainWhs;
                oArgnsModel.U_WhsNewI = pObject.U_WhsNewI;
                oArgnsModel.U_Vendor = pObject.U_Vendor;
                oArgnsModel.U_LineCode = pObject.U_LineCode;
                oArgnsModel.U_SclCode = pObject.U_SclCode;
                oArgnsModel.U_ChartCod = pObject.U_ChartCod;
                oArgnsModel.U_ModGrp = pObject.U_ModGrp;
                oArgnsModel.U_FrgnDesc = pObject.U_FrgnDesc;
                oArgnsModel.U_Season = pObject.U_Season;
                oArgnsModel.U_Division = pObject.U_Division;
                oArgnsModel.U_PList = (long)(pObject.U_PList.HasValue ? pObject.U_PList : 1);
                oArgnsModel.U_SapGrp = (long)(pObject.U_SapGrp.HasValue ? pObject.U_SapGrp : 100);
                oArgnsModel.U_Active = string.IsNullOrEmpty(pObject.U_Active) ? "Y" : pObject.U_Active;
                oArgnsModel.U_Pic = pObject.U_Pic;
                oArgnsModel.U_Year = pObject.U_Year;
                oArgnsModel.U_ATGrp = pObject.U_ATGrp;
                oArgnsModel.U_COO = pObject.U_COO;
                oArgnsModel.U_RMaterial = pObject.U_RMaterial;
                oArgnsModel.U_SclPOM = pObject.U_SclPOM;
                oArgnsModel.U_CodePOM = pObject.U_CodePOM;
                if (pObject.U_SSDate != null)
                    oArgnsModel.U_SSDate = pObject.U_SSDate.Value;
                if (pObject.U_SCDate != null)
                    oArgnsModel.U_SCDate = pObject.U_SCDate.Value;

                //Image
                int mCount = 0;
                if (pObject.ModelImageList.Count > 0)
                {
                    List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG> mListImg = new List<ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG>();
                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG mImageModel;

                    foreach (ARGNSModelImg item in pObject.ModelImageList)
                    {
                        mImageModel = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_IMG();
                        mImageModel.LineIdSpecified = true;
                        mImageModel.LineId = mCount + 1;
                        mImageModel.U_File = item.U_File;
                        mImageModel.U_FType = item.U_FType;
                        mImageModel.U_Path = item.U_Path;
                        mListImg.Add(mImageModel);
                        mCount++;
                    }
                    oArgnsModel.ARGNS_MODEL_IMGCollection = mListImg.ToArray();
                }

                oArgnsModelService.Update(oArgnsModel);
                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region Logos
        public List<ARGNSModelLogo> GetLogoList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_MODEL_LOGOS, ARGNSModelLogo>();
                List<ARGNSModelLogo> listReturn = Mapper.Map<List<ARGNSModelLogo>>(mDbContext.C_ARGNS_MODEL_LOGOS.Where(c => c.Code == pCode).ToList());
                return listReturn.Where(w => w.U_CodeLogo != null).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("ScaleMasterService -> GetLogoListDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public ARGNSLogo GetLogoById(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_LOGO, ARGNSLogo>();
                ARGNSLogo mReturn = Mapper.Map<ARGNSLogo>(mDbContext.C_ARGNS_LOGO.Where(c => c.U_CodeLogo == pCode).SingleOrDefault());

                return mReturn;

            }
            catch (Exception ex)
            {
                Logger.WriteError("ScaleMasterService -> GetLogoByIdDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }
        #endregion

        #region Color Master

        public ARGNSColor GetColorByCode(CompanyConn pCompanyParam, string pColorCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_COLOR, ARGNSColor>();

                ARGNSColor mColor = null;

                mColor = Mapper.Map<ARGNSColor>(mDbContext.C_ARGNS_COLOR.Where(c => c.U_ColCode == pColorCode).FirstOrDefault());

                if (mColor == null)
                {
                    mColor = new ARGNSColor();
                }

                return mColor;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ColorMasterService -> GetColorByCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSColor> GetColorMasterListSearch(CompanyConn pCompanyParam, string pColorCode = "", string pColorName = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_COLOR, ARGNSColor>();

                return Mapper.Map<List<ARGNSColor>>(mDbContext.C_ARGNS_COLOR.Where(c => (c.U_ColCode.Contains(pColorCode)) && (pColorName != "" ? c.U_ColDesc.Contains(pColorName) : true))).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("ColorMasterService -> GetColorMasterListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSModelColor> GetColorByModel(CompanyConn pCompanyParam, string pModel)
        {
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            List<ARGNSModelColor> mColorList = (from mCol in mDbContext.C_ARGNS_COLOR
                                                join mMolCol in mDbContext.C_ARGNS_MODEL_COLOR on mCol.U_ColCode equals mMolCol.U_ColCode into group1
                                                from mGroup1 in group1.DefaultIfEmpty()
                                                join mModel in mDbContext.C_ARGNS_MODEL on mGroup1.Code equals mModel.Code
                                                where mModel.U_ModCode == pModel
                                                select new ARGNSModelColor
                                                {
                                                    U_ColCode = mCol.U_ColCode,
                                                    ColorDesc = mCol.U_ColDesc,
                                                    U_Active = mGroup1.U_Active,
                                                    U_Argb = mCol.U_Argb
                                                }).Distinct().ToList();
            return mColorList;
        }

        public string AddColor(ARGNSColor pObject, CompanyConn pCompanyParam)
        {
            ARGNSColorServiceWeb.ArgnsColor oArgnsColor = new ARGNSColorServiceWeb.ArgnsColor();
            ARGNSColorServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSColorServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSColorServiceWeb.MsgHeaderServiceName.ARGNS_COLOR;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSColorServiceWeb.ARGNS_COLOR oArgnsColorService = new ARGNSColorServiceWeb.ARGNS_COLOR();
            oArgnsColorService.MsgHeaderValue = oArgnsModelMsgHeader;
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                oArgnsColor.Code = (mDbContext.C_ARGNS_COLOR.Max(c => c.DocEntry) + 1).ToString();
                oArgnsColor.U_Active = pObject.U_Active;
                oArgnsColor.U_Argb = pObject.U_Argb;
                oArgnsColor.U_Attrib1 = pObject.U_Attrib1;
                oArgnsColor.U_Attrib2 = pObject.U_Attrib2;
                oArgnsColor.U_ColCode = pObject.U_ColCode;
                oArgnsColor.U_ColDesc = pObject.U_ColDesc;
                oArgnsColor.U_NRFCode = pObject.U_NRFCode;
                oArgnsColor.U_Pic = pObject.U_Pic;
                oArgnsColor.U_Shade = pObject.U_Shade;

                oArgnsColorService.Add(oArgnsColor);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdateColor(ARGNSColor pObject, CompanyConn pCompanyParam)
        {
            ARGNSColorServiceWeb.ArgnsColor oArgnsColor = new ARGNSColorServiceWeb.ArgnsColor();
            ARGNSColorServiceWeb.ArgnsColorParams oArgnsColorParams = new ARGNSColorServiceWeb.ArgnsColorParams();
            ARGNSColorServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSColorServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSColorServiceWeb.MsgHeaderServiceName.ARGNS_COLOR;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSColorServiceWeb.ARGNS_COLOR oArgnsColorService = new ARGNSColorServiceWeb.ARGNS_COLOR();
            oArgnsColorService.MsgHeaderValue = oArgnsModelMsgHeader;
            try
            {
                oArgnsColorParams.Code = pObject.Code;
                oArgnsColor = oArgnsColorService.GetByParams(oArgnsColorParams);

                oArgnsColor.U_Active = pObject.U_Active;
                oArgnsColor.U_Argb = pObject.U_Argb;
                oArgnsColor.U_Attrib1 = pObject.U_Attrib1;
                oArgnsColor.U_Attrib2 = pObject.U_Attrib2;
                oArgnsColor.U_ColCode = pObject.U_ColCode;
                oArgnsColor.U_ColDesc = pObject.U_ColDesc;
                oArgnsColor.U_NRFCode = pObject.U_NRFCode;
                oArgnsColor.U_Pic = pObject.U_Pic;
                oArgnsColor.U_Shade = pObject.U_Shade;

                oArgnsColorService.Update(oArgnsColor);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region Scale Master

        public List<ARGNSScale> GetScaleMasterListSearch(CompanyConn pCompanyParam, string pScaleCode = "", string pScaleName = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_SCALE, ARGNSScale>();

                return Mapper.Map<List<ARGNSScale>>(mDbContext.C_ARGNS_SCALE.Where(c => (c.U_SclCode.Contains(pScaleCode)) && (pScaleName != "" ? c.U_SclDesc.Contains(pScaleName) : true))).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetScaleMasterListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSSize> GetScaleDescriptionListSearch(CompanyConn pCompanyParam, string pScaleCode = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_SIZE, ARGNSSize>();

                return Mapper.Map<List<ARGNSSize>>(mDbContext.C_ARGNS_SIZE.Where(c => c.Code == pScaleCode).ToList()); ;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetScaleDescriptionListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSSize> GetSizeByScale(CompanyConn pCompanyParam, string pScaleCode = "", string pModeCode = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_MODEL_SIZE, ARGNSSize>();

                C_ARGNS_MODEL modelAux = mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pModeCode).FirstOrDefault();
                return Mapper.Map<List<ARGNSSize>>(mDbContext.C_ARGNS_MODEL_SIZE.Where(c => c.Code == modelAux.Code && c.U_SclCode == pScaleCode).ToList());
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetScaleDescriptionListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        #endregion

        #region Variable Master
        public List<ARGNSVariable> GetVariableListSearch(CompanyConn pCompanyParam, ref List<ARGNSSegmentation> pSegmentationList, string pVarCode = "", string pVarName = "", string pVarSegmentation = "")
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_VARIABLE, ARGNSVariable>();

                ARGNSSegmentation mSegmentationList = Mapper.Map<ARGNSSegmentation>(mDbContext.C_ARGNS_APPTEXGRPS.Where(c => c.Name.ToUpper().Contains(pVarSegmentation.ToUpper())).FirstOrDefault());
                string mSegmentationCode = (mSegmentationList == null ? null : mSegmentationList.Code);
                return Mapper.Map<List<ARGNSVariable>>(mDbContext.C_ARGNS_VARIABLE.Where(c => (c.U_VarCode.Contains(pVarCode)) && (pVarName != "" ? c.U_VarDesc.Contains(pVarName) : true) && (pVarSegmentation != "" ? c.U_ATGrp.Contains(mSegmentationCode) : true))).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("VariableMasterService -> GetVariablerListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }
        #endregion

        #region SKU & Prepacks

        public List<SkuModel> GetSkuModelList(CompanyConn pCompanyParam, string pModCode, ref List<ARGNSModelImg> pImgList)
        {
            List<SkuModel> result = null;
            string mCode;
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_MODEL_IMG, ARGNSModelImg>();

                result = (from mOITM in mDbContext.OITM
                          join mColor in mDbContext.C_ARGNS_COLOR on mOITM.U_ARGNS_COL equals mColor.U_ColCode into group1
                          from mGroup1 in group1.DefaultIfEmpty()
                          join mVar in mDbContext.C_ARGNS_VARIABLE on mOITM.U_ARGNS_VAR equals mVar.U_VarCode into group2
                          from mGroup2 in group2.DefaultIfEmpty()
                          where mOITM.U_ARGNS_MOD == pModCode /*&&*/
                                //mOITM.U_ARGNS_ITYPE != "P"
                          select new SkuModel()
                          {
                              ItemCode = mOITM.ItemCode,
                              U_ARGNS_COL = mOITM.U_ARGNS_COL,
                              U_ARGNS_VAR = mOITM.U_ARGNS_VAR,
                              U_ARGNS_SIZE = mOITM.U_ARGNS_SIZE,
                              U_ARGNS_SIZEVO = mOITM.U_ARGNS_SIZEVO,
                              U_ARGNS_SCL = mOITM.U_ARGNS_SCL,
                              U_ARGNS_ITYPE = mOITM.U_ARGNS_ITYPE,
                              ColorDesc = mGroup1.U_ColDesc,
                              VarDesc = mGroup2.U_VarDesc,
                              U_PpDesc = mOITM.ItemName
                          }).Distinct().ToList();
                //Buscar codigo size run del prepack
                foreach (SkuModel mSku in result.Where(c => c.U_ARGNS_ITYPE == "P"))
                {
                    OITT mBillOfMaterialItem = mDbContext.OITT.Where(c => c.Code == mSku.ItemCode).FirstOrDefault();
                    if (mBillOfMaterialItem != null)
                    {
                        string ppCode = mDbContext.C_ARGNS_PREPACK.Where(c => c.U_PpCode == mBillOfMaterialItem.U_ARGNS_PPCode).FirstOrDefault().Code;
                        List<C_ARGNS_PREPACKLNS> mLines = mDbContext.C_ARGNS_PREPACKLNS.Where(c => c.Code == ppCode).ToList();
                        string mSizeRunCode = "";
                        foreach(C_ARGNS_PREPACKLNS mLine in mLines.OrderBy(c => c.LineId))
                        {
                            mSizeRunCode += Convert.ToDouble(mLine.U_Value).ToString();
                        }
                        mSku.U_PpSizeRun = mSizeRunCode;
                    }
                }

                mCode = mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pModCode).SingleOrDefault().Code;
                pImgList = Mapper.Map<List<ARGNSModelImg>>(mDbContext.C_ARGNS_MODEL_IMG.Where(c => c.Code == mCode).ToList());
                pImgList = pImgList.Select(c => { c.U_ModCode = pModCode; return c; }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetSkuModelListDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSPrepack> GetModelPrepacks(CompanyConn pCompanyParam, string pModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_PREPACK, ARGNSPrepack>(); cfg.CreateMap<C_ARGNS_PREPACKLNS, ARGNSPrepackLines>(); }).CreateMapper();

                List<ARGNSPrepack> mPrepackList = mapper.Map<List<ARGNSPrepack>>(mDbContext.C_ARGNS_PREPACK.Where(c => c.U_ModCode == pModCode).ToList());
                foreach(ARGNSPrepack mPrepack in mPrepackList)
                { 
                    mPrepack.PrepackLines = mapper.Map<List<ARGNSPrepackLines>>(mDbContext.C_ARGNS_PREPACKLNS.Where(c => c.Code == mPrepack.Code).ToList());
                }

                return mPrepackList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetModelPrepacksDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSSizeRun> GetSizeRunsByScale(CompanyConn pCompanyParam, string pScaleCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_SIZERUNSCALE, ARGNSSizeRun>(); cfg.CreateMap<C_ARGNS_SZRUNSCALELN, ARGNSSizeRunLine>(); }).CreateMapper();

                List<ARGNSSizeRun> mSizeRunList = mapper.Map<List<ARGNSSizeRun>>(mDbContext.C_ARGNS_SIZERUNSCALE.Where(c => c.U_SclCode == pScaleCode).ToList());
                foreach (ARGNSSizeRun mSizeRun in mSizeRunList)
                {
                    mSizeRun.Lines = mapper.Map<List<ARGNSSizeRunLine>>(mDbContext.C_ARGNS_SZRUNSCALELN.Where(c => c.Code == mSizeRun.Code).ToList());
                }

                return mSizeRunList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetSizeRunsByScale :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        #endregion

        #region Comments
        public List<ARGNSModelComment> GetModelComment(CompanyConn pCompanyParam, string pModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_COMMLOGMODEL, ARGNSModelComment>();
                return Mapper.Map<List<ARGNSModelComment>>(mDbContext.C_ARGNS_COMMLOGMODEL.Where(c => c.U_MODCODE == pModCode).ToList());

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataService -> GetModelCommentDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public string AddLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam)
        {
            int mLastCode;
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel oArgnsModelComment = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel();
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams oArgnsModelParams = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams();
            ARGNSModelCommentServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelCommentServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelCommentServiceWeb.MsgHeaderServiceName.ARGNS_UDOCLMODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL oArgnsModelCommentService = new ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL();
            oArgnsModelCommentService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                if (mDbContext.C_ARGNS_COMMLOGMODEL.Count() > 0)
                    mLastCode = mDbContext.C_ARGNS_COMMLOGMODEL.Max(c => c.DocEntry) + 1;
                else
                    mLastCode = 1;
                oArgnsModelComment.Code = mLastCode.ToString();
                oArgnsModelComment.Name = mLastCode.ToString();
                oArgnsModelComment.U_MODCODE = pObject.U_MODCODE;
                oArgnsModelComment.U_DATE = System.DateTime.Now;
                oArgnsModelComment.U_DATESpecified = true;
                oArgnsModelComment.U_USER = pObject.U_USER;
                oArgnsModelComment.U_COMMENT = pObject.U_COMMENT;
                oArgnsModelComment.U_FILE = pObject.U_FILE;
                oArgnsModelComment.U_FILEPATH = pObject.U_FILEPATH;

                oArgnsModelParams = oArgnsModelCommentService.Add(oArgnsModelComment);
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }

            return "OK";
        }

        public string UpdateLogCommentMaster(ARGNSModelComment pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel oArgnsModelComment = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModel();
            ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams oArgnsModelParams = new ARGNSModelCommentServiceWeb.ArgnsTableCommentlogModelParams();
            ARGNSModelCommentServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelCommentServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelCommentServiceWeb.MsgHeaderServiceName.ARGNS_UDOCLMODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL oArgnsModelCommentService = new ARGNSModelCommentServiceWeb.ARGNS_UDOCLMODEL();
            oArgnsModelCommentService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModelComment = oArgnsModelCommentService.GetByParams(oArgnsModelParams);

                oArgnsModelComment.Code = pObject.Code;
                oArgnsModelComment.Name = pObject.Name;
                oArgnsModelComment.U_COMMENT = pObject.U_COMMENT;
                oArgnsModelComment.U_DATE = System.DateTime.Now;
                oArgnsModelComment.U_FILE = pObject.U_FILE;
                oArgnsModelComment.U_FILEPATH = pObject.U_FILEPATH;
                oArgnsModelComment.U_MODCODE = pObject.U_MODCODE;
                oArgnsModelComment.U_USER = pObject.U_USER;

                oArgnsModelCommentService.Update(oArgnsModelComment);
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }

            return "OK";
        }
        #endregion

        #region Cost Sheet

        public List<ARGNSCostSheet> GetModelCostSheet(CompanyConn pCompanyParam, string pModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_COST_SHEET, ARGNSCostSheet>();

                List<ARGNSCostSheet> result = Mapper.Map<List<ARGNSCostSheet>>(mDbContext.C_ARGNS_COST_SHEET.Where(c => c.U_ModCode == pModCode).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelCostSheet :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public ARGNSCostSheet GetModelCostSheetByCode(CompanyConn pCompanyParam, string pCode, string pModelCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_COST_SHEET, ARGNSCostSheet>();
                Mapper.CreateMap<C_ARGNS_CS_MATERIALS, ARGNSCSMaterial>();
                Mapper.CreateMap<C_ARGNS_CS_OPERATIONS, ARGNSCSOperation>();
                Mapper.CreateMap<C_ARGNS_CS_SCHEMAS, ARGNSCSSchema>();
                Mapper.CreateMap<C_ARGNS_CS_PATTERNS, ARGNSCSPatterns>();
                Mapper.CreateMap<OCRD, BusinessPartnerSAP>();
                Mapper.CreateMap<OCRN, CurrencySAP>();
                Mapper.CreateMap<OPLN, PriceListSAP>();


                ARGNSCostSheet result = Mapper.Map<ARGNSCostSheet>(mDbContext.C_ARGNS_COST_SHEET.Where(c => c.Code == pCode).FirstOrDefault());
                if (result != null)
                {
                    result.ListMaterial = Mapper.Map<List<ARGNSCSMaterial>>(mDbContext.C_ARGNS_CS_MATERIALS.ToList().Where(c => c.Code == result.Code).ToList());
                    result.ListOperation = Mapper.Map<List<ARGNSCSOperation>>(mDbContext.C_ARGNS_CS_OPERATIONS.Where(c => c.Code == result.Code).ToList());
                    result.ListSchema = Mapper.Map<List<ARGNSCSSchema>>(mDbContext.C_ARGNS_CS_SCHEMAS.Where(c => c.Code == result.Code).ToList());
                    result.ListPattern = Mapper.Map<List<ARGNSCSPatterns>>(mDbContext.C_ARGNS_CS_PATTERNS.Where(c => c.Code == result.Code).ToList());
                }
                else
                {
                    result = new ARGNSCostSheet();
                    if (pModelCode != "0")
                    {
                        result.U_ModCode = mDbContext.C_ARGNS_MODEL.ToList().Where(c => c.Code == pModelCode).FirstOrDefault().U_ModCode;
                    }
                }

                result.CostSheetCombo.PriceList = Mapper.Map<List<PriceListSAP>>(mDbContext.OPLN.ToList());
                result.CostSheetCombo.CurrencyList = mDbContext.OCRN.Select(c => new CurrencySAP { CurrCode = c.CurrCode, CurrName = c.CurrName }).ToList();
                result.CostSheetCombo.VendorList = Mapper.Map<List<BusinessPartnerSAP>>(mDbContext.OCRD.Where(c => c.CardType == "S").ToList());
                result.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelCostSheetByCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSCostSheet> GetCostSheetListSearch(CompanyConn pCompanyParam, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<ARGNSCostSheet> ListRestu = (from mARGNS_MODEL in mDbContext.C_ARGNS_MODEL
                                                  join mARGNS_COST_SHEET in mDbContext.C_ARGNS_COST_SHEET on mARGNS_MODEL.U_ModCode equals mARGNS_COST_SHEET.U_ModCode
                                                  //TODO Esta parte del where la voy a hacer aca en el visual studio, el metodo GetCostSheetListSearch en el B1if trae la union entre la model y la costsheet nomas
                                                  where
                                                  (
                                                     (txtStyle != "" ? mARGNS_COST_SHEET.U_ModCode.Contains(txtStyle) : true) &&
                                                     (txtCodeCostSheet != "" ? mARGNS_COST_SHEET.Name.Contains(txtCodeCostSheet) : true) &&
                                                     (txtDescription != "" ? mARGNS_COST_SHEET.U_Description.Contains(txtDescription) : true)
                                                  )
                                                  select new ARGNSCostSheet()
                                                  {
                                                      Code = mARGNS_COST_SHEET.Code,
                                                      DocEntry = mARGNS_COST_SHEET.DocEntry,
                                                      Name = mARGNS_COST_SHEET.Name,
                                                      U_ModCode = mARGNS_COST_SHEET.U_ModCode,
                                                      U_Currency = mARGNS_COST_SHEET.U_Currency,
                                                      U_Description = mARGNS_COST_SHEET.U_Description,
                                                      U_OTCode = mARGNS_COST_SHEET.U_OTCode,
                                                      U_SchCode = mARGNS_COST_SHEET.U_SchCode,
                                                      ModeDBCode = mARGNS_MODEL.Code
                                                  }).ToList();
                return ListRestu;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCostSheetListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSSchema> GetCSSchemaList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_SCHEMA, ARGNSSchema>(); }).CreateMapper();

                List<ARGNSSchema> result = mapper.Map<List<ARGNSSchema>>(mDbContext.C_ARGNS_SCHEMA.Where(c => c.U_SchCode.Contains(pCode) && c.Name.Contains(pName)).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCSSchemaList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSSchemaLine> GetCSSchemaLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_SCHEMAL, ARGNSSchemaLine>(); }).CreateMapper();

                List<ARGNSSchemaLine> result = mapper.Map<List<ARGNSSchemaLine>>(mDbContext.C_ARGNS_SCHEMAL.Where(c => c.Code == pCode).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCSSchemaLineList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSOperTemplate> GetCSOperationTemplateList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_OPERTEMPLATE, ARGNSOperTemplate>(); }).CreateMapper();

                List<ARGNSOperTemplate> result = mapper.Map<List<ARGNSOperTemplate>>(mDbContext.C_ARGNS_OPERTEMPLATE.Where(c => c.U_OTCode.Contains(pCode) && c.Name.Contains(pName)).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCSOperationTemplateList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSOperTemplateLine> GetCSOperationTemplateLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_OPERTEMPLATEL, ARGNSOperTemplateLine>(); }).CreateMapper();

                List<ARGNSOperTemplateLine> result = mapper.Map<List<ARGNSOperTemplateLine>>(mDbContext.C_ARGNS_OPERTEMPLATEL.Where(c => c.Code == pCode).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCSOperationTemplateLineList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSPatternTemplate> GetCSPatternList(CompanyConn pCompanyParam, string pCode, string pName)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_PATTEMP, ARGNSPatternTemplate>(); }).CreateMapper();

                List<ARGNSPatternTemplate> result = mapper.Map<List<ARGNSPatternTemplate>>(mDbContext.C_ARGNS_PATTEMP.Where(c => c.U_CodeTmpl.Contains(pCode) && c.U_Desc.Contains(pName)).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCSPatternList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSPatternTemplateLine> GetCSPatternLineList(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_PATTEMPLNS, ARGNSPatternTemplateLine>(); }).CreateMapper();

                List<ARGNSPatternTemplateLine> result = mapper.Map<List<ARGNSPatternTemplateLine>>(mDbContext.C_ARGNS_PATTEMPLNS.Where(c => c.Code == pCode).ToList());

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCSPatternLineList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public string UpdateCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam)
        {
            try
            {
                ARGNSCostSheetServiceWeb.ArgnsCostSheet oCostSheet = new ARGNSCostSheetServiceWeb.ArgnsCostSheet();
                ARGNSCostSheetServiceWeb.ArgnsCostSheetParams oCostSheetParams = new ARGNSCostSheetServiceWeb.ArgnsCostSheetParams();
                ARGNSCostSheetServiceWeb.MsgHeader oCostSheetMsgHeader = new ARGNSCostSheetServiceWeb.MsgHeader();

                oCostSheetMsgHeader.ServiceName = ARGNSCostSheetServiceWeb.MsgHeaderServiceName.ARGNS_COST_SHEET;
                oCostSheetMsgHeader.ServiceNameSpecified = true;
                oCostSheetMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET oCostSheetService = new ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET();
                oCostSheetService.MsgHeaderValue = oCostSheetMsgHeader;


                oCostSheetParams.Code = pObject.Code;
                oCostSheet = oCostSheetService.GetByParams(oCostSheetParams);

                oCostSheet.U_OTCode = pObject.U_OTCode;
                oCostSheet.U_SchCode = pObject.U_SchCode;
                oCostSheet.U_CodeTmpl = pObject.U_CodeTmpl;
                oCostSheet.U_UserText1 = pObject.U_UserText1;
                oCostSheet.U_UserText2 = pObject.U_UserText2;
                oCostSheet.U_UserText3 = pObject.U_UserText3;
                if (pObject.U_PurchPrice != null)
                {
                    oCostSheet.U_PurchPrice = (double)pObject.U_PurchPrice;
                    oCostSheet.U_PurchPriceSpecified = true;
                }
                if (pObject.U_TMaterials != null)
                {
                    oCostSheet.U_TMaterials = (double)pObject.U_TMaterials;
                    oCostSheet.U_TMaterialsSpecified = true;
                }
                if (pObject.U_TOperations != null)
                {
                    oCostSheet.U_TOperations = (double)pObject.U_TOperations;
                    oCostSheet.U_TOperationsSpecified = true;
                }
                if (pObject.U_IndrCost != null)
                {
                    oCostSheet.U_IndrCost = (double)pObject.U_IndrCost;
                    oCostSheet.U_IndrCostSpecified = true;
                }
                if (pObject.U_TCost != null)
                {
                    oCostSheet.U_TCost = (double)pObject.U_TCost;
                    oCostSheet.U_TCostSpecified = true;
                }
                if (pObject.U_Duty != null)
                {
                    oCostSheet.U_Duty = (double)pObject.U_Duty;
                    oCostSheet.U_DutySpecified = true;
                }
                if (pObject.U_IntTransp != null)
                {
                    oCostSheet.U_IntTransp = (double)pObject.U_IntTransp;
                    oCostSheet.U_IntTranspSpecified = true;
                }
                if (pObject.U_Freight != null)
                {
                    oCostSheet.U_Freight = (double)pObject.U_Freight;
                    oCostSheet.U_FreightSpecified = true;
                }
                if (pObject.U_Margin != null)
                {
                    oCostSheet.U_Margin = (double)pObject.U_Margin;
                    oCostSheet.U_MarginSpecified = true;
                }
                if (pObject.U_DDP != null)
                {
                    oCostSheet.U_DDP = (double)pObject.U_DDP;
                    oCostSheet.U_DDPSpecified = true;
                }
                if (pObject.U_CostPercent != null)
                {
                    oCostSheet.U_CostPercent = (double)pObject.U_CostPercent;
                    oCostSheet.U_CostPercentSpecified = true;
                }
                if (pObject.U_SlsPercent != null)
                {
                    oCostSheet.U_SlsPercent = (double)pObject.U_SlsPercent;
                    oCostSheet.U_SlsPercentSpecified = true;
                }
                if (pObject.U_StrFactor != null)
                {
                    oCostSheet.U_StrFactor = (double)pObject.U_StrFactor;
                    oCostSheet.U_StrFactorSpecified = true;
                }
                if (pObject.U_TMargin != null)
                {
                    oCostSheet.U_TMargin = (double)pObject.U_TMargin;
                    oCostSheet.U_TMarginSpecified = true;
                }
                if (pObject.U_CIF != null)
                {
                    oCostSheet.U_CIF = (double)pObject.U_CIF;
                    oCostSheet.U_CIFSpecified = true;
                }
                if (pObject.U_FPrice != null)
                {
                    oCostSheet.U_FPrice = (double)pObject.U_FPrice;
                    oCostSheet.U_FPriceSpecified = true;
                }

                #region Materials

                if (pObject.ListMaterial.Count > 0 || oCostSheet.ARGNS_CS_MATERIALSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mMaterialSAP in oCostSheet.ARGNS_CS_MATERIALSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSMaterial mMaterial = pObject.ListMaterial.Where(c => c.LineId == mMaterialSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mMaterial != null)
                        {
                            mMaterialSAP.U_ItemCode = mMaterial.U_ItemCode;
                            mMaterialSAP.U_ItemName = mMaterial.U_ItemName;
                            if (mMaterial.U_Quantity.HasValue)
                            {
                                mMaterialSAP.U_Quantity = (double)mMaterial.U_Quantity;
                                mMaterialSAP.U_QuantitySpecified = true;
                            }
                            mMaterialSAP.U_CxT = mMaterial.U_CxT;
                            mMaterialSAP.U_CxS = mMaterial.U_CxS;
                            mMaterialSAP.U_CxC = mMaterial.U_CxC;
                            mMaterialSAP.U_CxGT = mMaterial.U_CxGT;
                            mMaterialSAP.U_UoM = mMaterial.U_UoM;
                            mMaterialSAP.U_Whse = mMaterial.U_Whse;
                            mMaterialSAP.U_OcrCode = mMaterial.U_OcrCode;
                            if (mMaterial.U_PList.HasValue)
                            {
                                mMaterialSAP.U_PList = (long)mMaterial.U_PList;
                                mMaterialSAP.U_PListSpecified = true;
                            }
                            mMaterialSAP.U_Currency = mMaterial.U_Currency;
                            if (mMaterial.U_UPrice.HasValue)
                            {
                                mMaterialSAP.U_UPrice = (double)mMaterial.U_UPrice;
                                mMaterialSAP.U_UPriceSpecified = true;
                            }
                            mMaterialSAP.U_PVendor = mMaterial.U_PVendor;
                            mMaterialSAP.U_Comments = mMaterial.U_Comments;
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_MATERIALSCollection = oCostSheet.ARGNS_CS_MATERIALSCollection.Where(c => c.LineId != mMaterialSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[oCostSheet.ARGNS_CS_MATERIALSCollection.Length + pObject.ListMaterial.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_MATERIALSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_MATERIALSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSMaterial mMaterial in pObject.ListMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS lineMaterial = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_ItemCode = mMaterial.U_ItemCode;
                        lineMaterial.U_ItemName = mMaterial.U_ItemName;
                        if (mMaterial.U_Quantity.HasValue)
                        {
                            lineMaterial.U_Quantity = (double)mMaterial.U_Quantity;
                            lineMaterial.U_QuantitySpecified = true;
                        }
                        lineMaterial.U_CxT = mMaterial.U_CxT;
                        lineMaterial.U_CxS = mMaterial.U_CxS;
                        lineMaterial.U_CxC = mMaterial.U_CxC;
                        lineMaterial.U_CxGT = mMaterial.U_CxGT;
                        lineMaterial.U_UoM = mMaterial.U_UoM;
                        lineMaterial.U_Whse = mMaterial.U_Whse;
                        lineMaterial.U_OcrCode = mMaterial.U_OcrCode;
                        if (mMaterial.U_PList.HasValue)
                        {
                            lineMaterial.U_PList = (long)mMaterial.U_PList;
                            lineMaterial.U_PListSpecified = true;
                        }
                        lineMaterial.U_Currency = mMaterial.U_Currency;
                        if (mMaterial.U_UPrice.HasValue)
                        {
                            lineMaterial.U_UPrice = (double)mMaterial.U_UPrice;
                            lineMaterial.U_UPriceSpecified = true;
                        }
                        lineMaterial.U_PVendor = mMaterial.U_PVendor;
                        lineMaterial.U_Comments = mMaterial.U_Comments;

                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_MATERIALSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[newLines.Length];
                    oCostSheet.ARGNS_CS_MATERIALSCollection = newLines;
                }

                #endregion

                #region Operations

                if (pObject.ListOperation.Count > 0 || oCostSheet.ARGNS_CS_OPERATIONSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mOperationSAP in oCostSheet.ARGNS_CS_OPERATIONSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSOperation mOperation = pObject.ListOperation.Where(c => c.LineId == mOperationSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mOperation != null)
                        {
                            mOperationSAP.U_ItemCode = mOperation.U_ItemCode;
                            mOperationSAP.U_ItemName = mOperation.U_ItemName;
                            if (mOperation.U_Quantity.HasValue)
                            {
                                mOperationSAP.U_Quantity = (double)mOperation.U_Quantity;
                                mOperationSAP.U_QuantitySpecified = true;
                            }
                            mOperationSAP.U_UoM = mOperation.U_UoM;
                            if (mOperation.U_PList.HasValue)
                            {
                                mOperationSAP.U_PList = (long)mOperation.U_PList;
                                mOperationSAP.U_PListSpecified = true;
                            }
                            mOperationSAP.U_Currency = mOperation.U_Currency;
                            if (mOperation.U_Price.HasValue)
                            {
                                mOperationSAP.U_Price = (double)mOperation.U_Price;
                                mOperationSAP.U_PriceSpecified = true;
                            }
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_OPERATIONSCollection = oCostSheet.ARGNS_CS_OPERATIONSCollection.Where(c => c.LineId != mOperationSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[oCostSheet.ARGNS_CS_OPERATIONSCollection.Length + pObject.ListOperation.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_OPERATIONSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_OPERATIONSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSOperation mOperation in pObject.ListOperation.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS lineOperation = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS();
                        lineOperation.LineId = mOperation.LineId;
                        lineOperation.LineIdSpecified = true;
                        lineOperation.U_ItemCode = mOperation.U_ItemCode;
                        lineOperation.U_ItemName = mOperation.U_ItemName;
                        if (mOperation.U_Quantity.HasValue)
                        {
                            lineOperation.U_Quantity = (double)mOperation.U_Quantity;
                            lineOperation.U_QuantitySpecified = true;
                        }
                        lineOperation.U_UoM = mOperation.U_UoM;
                        if (mOperation.U_PList.HasValue)
                        {
                            lineOperation.U_PList = (long)mOperation.U_PList;
                            lineOperation.U_PListSpecified = true;
                        }
                        lineOperation.U_Currency = mOperation.U_Currency;
                        if (mOperation.U_Price.HasValue)
                        {
                            lineOperation.U_Price = (double)mOperation.U_Price;
                            lineOperation.U_PriceSpecified = true;
                        }

                        newLines[positionNewLine] = lineOperation;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[newLines.Length];
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = newLines;
                }

                #endregion

                #region Schemas

                if (pObject.ListSchema.Count > 0 || oCostSheet.ARGNS_CS_SCHEMASCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mSchemaSAP in oCostSheet.ARGNS_CS_SCHEMASCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSSchema mSchema = pObject.ListSchema.Where(c => c.LineId == mSchemaSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mSchema != null)
                        {
                            if (mSchema.U_Percent.HasValue)
                            {
                                mSchemaSAP.U_Percent = (double)mSchema.U_Percent;
                                mSchemaSAP.U_PercentSpecified = true;
                            }
                            if (mSchema.U_PList.HasValue)
                            {
                                mSchemaSAP.U_PList = (long)mSchema.U_PList;
                                mSchemaSAP.U_PListSpecified = true;
                            }
                            mSchemaSAP.U_Currency = mSchema.U_Currency;
                            if (mSchema.U_Amount.HasValue)
                            {
                                mSchemaSAP.U_Amount = (double)mSchema.U_Amount;
                                mSchemaSAP.U_AmountSpecified = true;
                            }
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_SCHEMASCollection = oCostSheet.ARGNS_CS_SCHEMASCollection.Where(c => c.LineId != mSchemaSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[oCostSheet.ARGNS_CS_SCHEMASCollection.Length + pObject.ListSchema.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_SCHEMASCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_SCHEMASCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSSchema mSchema in pObject.ListSchema.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS lineSchema = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS();
                        lineSchema.LineId = mSchema.LineId;
                        lineSchema.LineIdSpecified = true;
                        if (mSchema.U_Percent.HasValue)
                        {
                            lineSchema.U_Percent = (double)mSchema.U_Percent;
                            lineSchema.U_PercentSpecified = true;
                        }
                        if (mSchema.U_PList.HasValue)
                        {
                            lineSchema.U_PList = (long)mSchema.U_PList;
                            lineSchema.U_PListSpecified = true;
                        }
                        lineSchema.U_Currency = mSchema.U_Currency;
                        if (mSchema.U_Amount.HasValue)
                        {
                            lineSchema.U_Amount = (double)mSchema.U_Amount;
                            lineSchema.U_AmountSpecified = true;
                        }

                        newLines[positionNewLine] = lineSchema;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_SCHEMASCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[newLines.Length];
                    oCostSheet.ARGNS_CS_SCHEMASCollection = newLines;
                }

                #endregion

                #region Patterns

                if (pObject.ListPattern.Count > 0 || oCostSheet.ARGNS_CS_PATTERNSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mPatternSAP in oCostSheet.ARGNS_CS_PATTERNSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCSPatterns mPattern = pObject.ListPattern.Where(c => c.LineId == mPatternSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPattern != null)
                        {
                            mPatternSAP.U_PattCode = mPattern.U_PattCode;
                            mPatternSAP.U_Desc = mPattern.U_Desc;
                            mPatternSAP.U_ItemCode = mPattern.U_ItemCode;
                            mPatternSAP.U_ItemName = mPattern.U_ItemName;
                            if (mPattern.U_Quantity.HasValue)
                            {
                                mPatternSAP.U_Quantity = (double)mPattern.U_Quantity;
                                mPatternSAP.U_QuantitySpecified = true;
                            }
                            mPatternSAP.U_UoM = mPattern.U_UoM;
                            if (mPattern.U_PattQty.HasValue)
                            {
                                mPatternSAP.U_PattQty = (long)mPattern.U_PattQty;
                                mPatternSAP.U_PattQtySpecified = true;
                            }
                        }
                        //Sino la borro
                        else
                        {
                            oCostSheet.ARGNS_CS_PATTERNSCollection = oCostSheet.ARGNS_CS_PATTERNSCollection.Where(c => c.LineId != mPatternSAP.LineId).ToArray();
                        }
                    }

                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[oCostSheet.ARGNS_CS_PATTERNSCollection.Length + pObject.ListPattern.Where(c => c.IsNew == true).ToList().Count];
                    oCostSheet.ARGNS_CS_PATTERNSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oCostSheet.ARGNS_CS_PATTERNSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSPatterns mPattern in pObject.ListPattern.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS linePattern = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS();
                        linePattern.LineId = mPattern.LineId;
                        linePattern.LineIdSpecified = true;
                        linePattern.U_PattCode = mPattern.U_PattCode;
                        linePattern.U_Desc = mPattern.U_Desc;
                        linePattern.U_ItemCode = mPattern.U_ItemCode;
                        linePattern.U_ItemName = mPattern.U_ItemName;
                        if (mPattern.U_Quantity.HasValue)
                        {
                            linePattern.U_Quantity = (double)mPattern.U_Quantity;
                            linePattern.U_QuantitySpecified = true;
                        }
                        linePattern.U_UoM = mPattern.U_UoM;
                        if (mPattern.U_PattQty.HasValue)
                        {
                            linePattern.U_PattQty = (long)mPattern.U_PattQty;
                            linePattern.U_PattQtySpecified = true;
                        }

                        newLines[positionNewLine] = linePattern;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_PATTERNSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[newLines.Length];
                    oCostSheet.ARGNS_CS_PATTERNSCollection = newLines;
                }

                #endregion

                oCostSheetService.Update(oCostSheet);
                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateCostSheet :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public JsonObjectResult AddCostSheet(ARGNSCostSheet pObject, CompanyConn pCompanyParam)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            try
            {
                ARGNSCostSheetServiceWeb.ArgnsCostSheet oCostSheet = new ARGNSCostSheetServiceWeb.ArgnsCostSheet();
                ARGNSCostSheetServiceWeb.ArgnsCostSheetParams oCostSheetParams = new ARGNSCostSheetServiceWeb.ArgnsCostSheetParams();
                ARGNSCostSheetServiceWeb.MsgHeader oCostSheetMsgHeader = new ARGNSCostSheetServiceWeb.MsgHeader();

                oCostSheetMsgHeader.ServiceName = ARGNSCostSheetServiceWeb.MsgHeaderServiceName.ARGNS_COST_SHEET;
                oCostSheetMsgHeader.ServiceNameSpecified = true;
                oCostSheetMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET oCostSheetService = new ARGNSCostSheetServiceWeb.ARGNS_COST_SHEET();
                oCostSheetService.MsgHeaderValue = oCostSheetMsgHeader;

                oCostSheet.Code = GetLastCostSheetCode(pCompanyParam);
                oCostSheet.Name = pObject.Name;
                oCostSheet.U_ModCode = pObject.U_ModCode;
                oCostSheet.U_Currency = pObject.U_Currency;
                oCostSheet.U_Description = pObject.U_Description;
                oCostSheet.U_OTCode = pObject.U_OTCode;
                oCostSheet.U_SchCode = pObject.U_SchCode;
                oCostSheet.U_CodeTmpl = pObject.U_CodeTmpl;
                oCostSheet.U_UserText1 = pObject.U_UserText1;
                oCostSheet.U_UserText2 = pObject.U_UserText2;
                oCostSheet.U_UserText3 = pObject.U_UserText3;
                if (pObject.U_PurchPrice != null)
                {
                    oCostSheet.U_PurchPrice = (double)pObject.U_PurchPrice;
                    oCostSheet.U_PurchPriceSpecified = true;
                }
                if (pObject.U_TMaterials != null)
                {
                    oCostSheet.U_TMaterials = (double)pObject.U_TMaterials;
                    oCostSheet.U_TMaterialsSpecified = true;
                }
                if (pObject.U_TOperations != null)
                {
                    oCostSheet.U_TOperations = (double)pObject.U_TOperations;
                    oCostSheet.U_TOperationsSpecified = true;
                }
                if (pObject.U_IndrCost != null)
                {
                    oCostSheet.U_IndrCost = (double)pObject.U_IndrCost;
                    oCostSheet.U_IndrCostSpecified = true;
                }
                if (pObject.U_TCost != null)
                {
                    oCostSheet.U_TCost = (double)pObject.U_TCost;
                    oCostSheet.U_TCostSpecified = true;
                }
                if (pObject.U_Duty != null)
                {
                    oCostSheet.U_Duty = (double)pObject.U_Duty;
                    oCostSheet.U_DutySpecified = true;
                }
                if (pObject.U_IntTransp != null)
                {
                    oCostSheet.U_IntTransp = (double)pObject.U_IntTransp;
                    oCostSheet.U_IntTranspSpecified = true;
                }
                if (pObject.U_Freight != null)
                {
                    oCostSheet.U_Freight = (double)pObject.U_Freight;
                    oCostSheet.U_FreightSpecified = true;
                }
                if (pObject.U_Margin != null)
                {
                    oCostSheet.U_Margin = (double)pObject.U_Margin;
                    oCostSheet.U_MarginSpecified = true;
                }
                if (pObject.U_DDP != null)
                {
                    oCostSheet.U_DDP = (double)pObject.U_DDP;
                    oCostSheet.U_DDPSpecified = true;
                }
                if (pObject.U_CostPercent != null)
                {
                    oCostSheet.U_CostPercent = (double)pObject.U_CostPercent;
                    oCostSheet.U_CostPercentSpecified = true;
                }
                if (pObject.U_SlsPercent != null)
                {
                    oCostSheet.U_SlsPercent = (double)pObject.U_SlsPercent;
                    oCostSheet.U_SlsPercentSpecified = true;
                }
                if (pObject.U_StrFactor != null)
                {
                    oCostSheet.U_StrFactor = (double)pObject.U_StrFactor;
                    oCostSheet.U_StrFactorSpecified = true;
                }
                if (pObject.U_TMargin != null)
                {
                    oCostSheet.U_TMargin = (double)pObject.U_TMargin;
                    oCostSheet.U_TMarginSpecified = true;
                }
                if (pObject.U_CIF != null)
                {
                    oCostSheet.U_CIF = (double)pObject.U_CIF;
                    oCostSheet.U_CIFSpecified = true;
                }
                if (pObject.U_FPrice != null)
                {
                    oCostSheet.U_FPrice = (double)pObject.U_FPrice;
                    oCostSheet.U_FPriceSpecified = true;
                }

                #region Materials

                if (pObject.ListMaterial.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[pObject.ListMaterial.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSMaterial mMaterial in pObject.ListMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS lineMaterial = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_ItemCode = mMaterial.U_ItemCode;
                        lineMaterial.U_ItemName = mMaterial.U_ItemName;
                        if (mMaterial.U_Quantity.HasValue)
                        {
                            lineMaterial.U_Quantity = (double)mMaterial.U_Quantity;
                            lineMaterial.U_QuantitySpecified = true;
                        }
                        lineMaterial.U_CxT = mMaterial.U_CxT;
                        lineMaterial.U_CxS = mMaterial.U_CxS;
                        lineMaterial.U_CxC = mMaterial.U_CxC;
                        lineMaterial.U_CxGT = mMaterial.U_CxGT;
                        lineMaterial.U_UoM = mMaterial.U_UoM;
                        lineMaterial.U_Whse = mMaterial.U_Whse;
                        lineMaterial.U_OcrCode = mMaterial.U_OcrCode;
                        if (mMaterial.U_PList.HasValue)
                        {
                            lineMaterial.U_PList = (long)mMaterial.U_PList;
                            lineMaterial.U_PListSpecified = true;
                        }
                        lineMaterial.U_Currency = mMaterial.U_Currency;
                        if (mMaterial.U_UPrice.HasValue)
                        {
                            lineMaterial.U_UPrice = (double)mMaterial.U_UPrice;
                            lineMaterial.U_UPriceSpecified = true;
                        }
                        lineMaterial.U_PVendor = mMaterial.U_PVendor;
                        lineMaterial.U_Comments = mMaterial.U_Comments;

                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_MATERIALSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_MATERIALS[newLines.Length];
                    oCostSheet.ARGNS_CS_MATERIALSCollection = newLines;
                }

                #endregion

                #region Operations

                if (pObject.ListOperation.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[pObject.ListOperation.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSOperation mOperation in pObject.ListOperation.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS lineOperation = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS();
                        lineOperation.LineId = mOperation.LineId;
                        lineOperation.LineIdSpecified = true;
                        lineOperation.U_ItemCode = mOperation.U_ItemCode;
                        lineOperation.U_ItemName = mOperation.U_ItemName;
                        if (mOperation.U_Quantity.HasValue)
                        {
                            lineOperation.U_Quantity = (double)mOperation.U_Quantity;
                            lineOperation.U_QuantitySpecified = true;
                        }
                        lineOperation.U_UoM = mOperation.U_UoM;
                        if (mOperation.U_PList.HasValue)
                        {
                            lineOperation.U_PList = (long)mOperation.U_PList;
                            lineOperation.U_PListSpecified = true;
                        }
                        lineOperation.U_Currency = mOperation.U_Currency;
                        if (mOperation.U_Price.HasValue)
                        {
                            lineOperation.U_Price = (double)mOperation.U_Price;
                            lineOperation.U_PriceSpecified = true;
                        }

                        newLines[positionNewLine] = lineOperation;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_OPERATIONS[newLines.Length];
                    oCostSheet.ARGNS_CS_OPERATIONSCollection = newLines;
                }

                #endregion

                #region Schemas

                if (pObject.ListSchema.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[pObject.ListSchema.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSSchema mSchema in pObject.ListSchema.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS lineSchema = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS();
                        lineSchema.LineId = mSchema.LineId;
                        lineSchema.LineIdSpecified = true;
                        if (mSchema.U_Percent.HasValue)
                        {
                            lineSchema.U_Percent = (double)mSchema.U_Percent;
                            lineSchema.U_PercentSpecified = true;
                        }
                        if (mSchema.U_PList.HasValue)
                        {
                            lineSchema.U_PList = (long)mSchema.U_PList;
                            lineSchema.U_PListSpecified = true;
                        }
                        lineSchema.U_Currency = mSchema.U_Currency;
                        if (mSchema.U_Amount.HasValue)
                        {
                            lineSchema.U_Amount = (double)mSchema.U_Amount;
                            lineSchema.U_AmountSpecified = true;
                        }

                        newLines[positionNewLine] = lineSchema;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_SCHEMASCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_SCHEMAS[newLines.Length];
                    oCostSheet.ARGNS_CS_SCHEMASCollection = newLines;
                }

                #endregion

                #region Patterns

                if (pObject.ListPattern.Count > 0)
                {
                    ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[] newLines = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[pObject.ListPattern.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCSPatterns mPattern in pObject.ListPattern.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS linePattern = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS();
                        linePattern.LineId = mPattern.LineId;
                        linePattern.LineIdSpecified = true;
                        linePattern.U_PattCode = mPattern.U_PattCode;
                        linePattern.U_Desc = mPattern.U_Desc;
                        linePattern.U_ItemCode = mPattern.U_ItemCode;
                        linePattern.U_ItemName = mPattern.U_ItemName;
                        if (mPattern.U_Quantity.HasValue)
                        {
                            linePattern.U_Quantity = (double)mPattern.U_Quantity;
                            linePattern.U_QuantitySpecified = true;
                        }
                        linePattern.U_UoM = mPattern.U_UoM;
                        if (mPattern.U_PattQty.HasValue)
                        {
                            linePattern.U_PattQty = (long)mPattern.U_PattQty;
                            linePattern.U_PattQtySpecified = true;
                        }

                        newLines[positionNewLine] = linePattern;
                        positionNewLine += 1;
                    }
                    oCostSheet.ARGNS_CS_PATTERNSCollection = new ARGNSCostSheetServiceWeb.ArgnsCostSheetARGNS_CS_PATTERNS[newLines.Length];
                    oCostSheet.ARGNS_CS_PATTERNSCollection = newLines;
                }

                #endregion

                oCostSheetParams = oCostSheetService.Add(oCostSheet);
                mJsonObjectResult.Code = oCostSheetParams.Code;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                string mModelCodeDB = mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pObject.U_ModCode).FirstOrDefault().Code;
                mJsonObjectResult.Others.Add("ModelCodeDB", mModelCodeDB);

                this.UpdateCostSheetModelLine(pCompanyParam, pObject);
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public string GetLastCostSheetCode(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                return (mDbContext.C_ARGNS_COST_SHEET.Max(c => c.DocEntry) + 1).ToString();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetLastCostSheetCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        private string UpdateCostSheetModelLine(CompanyConn pCompanyParam, ARGNSCostSheet pObject)
        {
            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;
            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                string mModelCodeDB = mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pObject.U_ModCode).FirstOrDefault().Code;

                oArgnsModelParams.Code = mModelCodeDB;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS mNewLine = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS();
                mNewLine.Code = mModelCodeDB;
                if (oArgnsModel.ARGNS_MODEL_CSCollection.Count() > 0)
                    mNewLine.LineId = oArgnsModel.ARGNS_MODEL_CSCollection.Max(c => c.LineId) + 1;
                else
                    mNewLine.LineId = 1;
                mNewLine.U_CSCode = pObject.Name;
                mNewLine.U_Desc = pObject.U_Description;
                mNewLine.U_Selected = "N";

                ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS[] newLines = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS[oArgnsModel.ARGNS_MODEL_CSCollection.Length + 1];
                oArgnsModel.ARGNS_MODEL_CSCollection.CopyTo(newLines, 0);
                int positionNewLine = oArgnsModel.ARGNS_MODEL_CSCollection.Length;
                newLines[positionNewLine] = mNewLine;
                oArgnsModel.ARGNS_MODEL_CSCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_CS[newLines.Length];
                oArgnsModel.ARGNS_MODEL_CSCollection = newLines;

                oArgnsModelService.Update(oArgnsModel);

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        #endregion

        #region POM

        public List<ARGNSModelPom> GetModelPomList(CompanyConn pCompanyParam, string pModCode)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                C_ARGNS_MODEL model = mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pModCode).FirstOrDefault();
                List<ARGNSModelPom> result = (model != null ? Mapper.Map<List<ARGNSModelPom>>(mDbContext.C_ARGNS_MODELPOM.Where(c => c.Code == model.Code).ToList()) : null);
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelPomList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSModelPomTemplate> GetModelPomTemplateList(CompanyConn pCompanyParam, List<string> pSclcodeList, List<string> pPomCodeList)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_POMTEMPLATE, ARGNSModelPomTemplate>(); }).CreateMapper();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                List<ARGNSModelPomTemplate> result = mapper.Map<List<ARGNSModelPomTemplate>>(mDbContext.C_ARGNS_POMTEMPLATE.Where(c => pSclcodeList.Contains(c.U_Scale) && !pPomCodeList.Contains(c.U_CodeTmpl)).ToList());
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelPomTemplateList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSPomTemplateLines> GetModelPomTemplateLines(CompanyConn pCompanyParam, string pPOMCode)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_POMTEMPLNS, ARGNSPomTemplateLines>(); }).CreateMapper();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                List<ARGNSPomTemplateLines> result = mapper.Map<List<ARGNSPomTemplateLines>>(mDbContext.C_ARGNS_POMTEMPLNS.Where(c => c.Code == pPOMCode).ToList());
                List<C_ARGNS_POMTEMPLATE> mPomTemplateListAux = mDbContext.C_ARGNS_POMTEMPLATE.Where(c => c.Code == pPOMCode).ToList();
                foreach (ARGNSPomTemplateLines mARGNSPomTemplateLinesAux in result)
                {
                    mARGNSPomTemplateLinesAux.U_PomCode = mPomTemplateListAux.Where(c => c.Code == mARGNSPomTemplateLinesAux.Code).FirstOrDefault().U_CodeTmpl;
                    mARGNSPomTemplateLinesAux.IsNew = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelPomTemplateLines :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        #endregion

        #region Critical Path

        public List<ARGNSCrPath> GetCriticalPathListSearch(CompanyConn pCompanyParam, string pCollCode = "", string pSubCollCode = "", string pSeasonCode = "", string pModelCode = "", string pProjectCode = "", string pVendorCode = "", string pCustomerCode = "")
        {
            int SalesCode, ActivityCode;
            List<ARGNSCrPath> ret = null;
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                Mapper.CreateMap<C_ARGNS_CRPATHACT, ARGNSCrPathActivities>();

                //TODO aca voy a llamar a la funcion que devuelve todos los BP y hago estos 2 filtros en C#
                var mBPVen = from mVen in mDbContext.OCRD where mVen.CardType == "S" && mVen.CardName.ToUpper().Contains(pVendorCode.ToUpper()) select mVen.CardCode;
                var mBPCust = from mCust in mDbContext.OCRD where mCust.CardType == "C" && mCust.CardName.ToUpper().Contains(pCustomerCode.ToUpper()) select mCust.CardCode;

                //TODO aca voy a obtener toda la query y despues voy a filtrar el where en C#
                //Obtengo Todos Los Critical Path por Filtros
                ret = (from pCr in mDbContext.C_ARGNS_CRPATH
                       join mModel in mDbContext.C_ARGNS_MODEL on pCr.U_Model equals mModel.U_ModCode
                       join mSea in mDbContext.C_ARGNS_SEASON on mModel.U_Season equals mSea.Code into group1
                       from mGroup1 in group1.DefaultIfEmpty()
                       join mColl in mDbContext.C_ARGNS_COLLECTION on mModel.U_CollCode equals mColl.U_CollCode into group2
                       from mGroup2 in group2.DefaultIfEmpty()
                       join mSubColl in mDbContext.C_ARGNS_AMBIENT on mModel.U_AmbCode equals mSubColl.U_AmbCode into group3
                       from mGroup3 in group3.DefaultIfEmpty()
                       where
                       (
                           (string.IsNullOrEmpty(pProjectCode) ? true : pCr.U_Desc.Contains(pProjectCode))
                           && (string.IsNullOrEmpty(pModelCode) ? true : mModel.U_ModDesc.Contains(pModelCode))
                           && (string.IsNullOrEmpty(pSeasonCode) ? true : mGroup1.Name.Contains(pSeasonCode))
                           && (string.IsNullOrEmpty(pCollCode) ? true : mGroup2.U_Desc.Contains(pCollCode))
                           && (string.IsNullOrEmpty(pSubCollCode) ? true : mGroup3.U_Desc.Contains(pSubCollCode))
                           && (string.IsNullOrEmpty(pVendorCode) ? true : mBPVen.Contains(mModel.U_Vendor))
                           && (string.IsNullOrEmpty(pCustomerCode) ? true : mBPCust.Contains(mModel.U_Customer))
                       )
                       select new ARGNSCrPath()
                       {
                           Code = pCr.Code,
                           U_Desc = pCr.U_Desc,
                           Name = pCr.Name,
                           U_Model = pCr.U_Model,
                           U_ProyCode = pCr.U_ProyCode,
                           U_Planning = pCr.U_Planning,
                           U_Status = pCr.U_Status,
                           ModelPicture = mModel.U_Pic,
                           SalesOrderNum = pCr.U_SalesO,
                           SeasonDesc = mGroup1.Name,
                           CollectionDesc = mGroup2.U_Desc,
                           ModelDesc = mModel.U_ModDesc

                       }).ToList();


                //TODO aca voy a obtener todas las sales order y despues las filtro en C#
                foreach (ARGNSCrPath item in ret)
                {

                    //Obtengo la Sales Order Asociada al Critical Path
                    if (!string.IsNullOrEmpty(item.SalesOrderNum))
                    {
                        SalesCode = Convert.ToInt32(item.SalesOrderNum);
                        var mSales = mDbContext.ORDR.Where(c => c.DocNum == SalesCode).SingleOrDefault();
                        if (mSales != null)
                        {
                            item.SalesCardCode = mSales.CardCode;
                            item.SalesCardName = mSales.CardName;
                            item.SalesDelivDate = mSales.DocDueDate;
                            item.SalesPostingDate = mSales.DocDate;
                            item.SalesQty = mSales.DocTotal;
                        }
                    }


                    //TODO Obtengo todas las actividades y despues las filtro en C#
                    //Listo Todas las Acividades Del Critical Path Para Obtener Datos
                    item.ActivitiesList = Mapper.Map<List<ARGNSCrPathActivities>>(mDbContext.C_ARGNS_CRPATHACT.Where(t => t.Code == item.Code).ToList());
                    foreach (var Act in item.ActivitiesList)
                    {
                        if (!string.IsNullOrEmpty(Act.U_ActSAP))
                        {
                            ActivityCode = Convert.ToInt32(Act.U_ActSAP);

                            var mAct = mDbContext.OCLG.Where(c => c.ClgCode == ActivityCode).SingleOrDefault();
                            if (mAct != null)
                            {
                                var mOCLA = mDbContext.OCLA.Where(c => c.statusID == mAct.status).FirstOrDefault();

                                Act.Closed = mAct.Closed == "Y" ? true : false;

                                if (mOCLA != null)
                                {
                                    Act.U_Status = mOCLA.name;
                                }
                                else
                                {
                                    Act.U_Status = "";
                                }


                            }
                        }
                    }


                }


                return ret;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCriticalPathList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        #endregion

        #region Projects

        public ARGNSProject GetProjectByCode(CompanyConn pCompanyParam, string pCode, string pModelId)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_CRPATH, ARGNSProject>(); cfg.CreateMap<C_ARGNS_CRPATHACT, ARGNSCrPathActivities>(); cfg.CreateMap<C_ARGNS_ROUTING, ARGNSRouting>(); cfg.CreateMap<OUDP, Department>(); cfg.CreateMap<OHTY, RolSAP>(); cfg.CreateMap<OCLG, ActivitiesSAP>(); }).CreateMapper();

                ARGNSProject mProject = mapper.Map<ARGNSProject>(mDbContext.C_ARGNS_CRPATH.Where(c => c.Code == pCode).FirstOrDefault());

                if(mProject != null)
                {
                    mProject.ActivitiesList = mapper.Map<List<ARGNSCrPathActivities>>(mDbContext.C_ARGNS_CRPATHACT.Where(t => t.Code == pCode).ToList());
                    foreach(ARGNSCrPathActivities mCRActivity in mProject.ActivitiesList)
                    {
                        if(!string.IsNullOrEmpty(mCRActivity.U_ActSAP))
                        {
                            mCRActivity.ActivitySAP = mapper.Map<ActivitiesSAP>(mDbContext.OCLG.Where(c => c.ClgCode.ToString() == mCRActivity.U_ActSAP).FirstOrDefault());
                        }
                    }
                }
                else
                {
                    mProject = new ARGNSProject();
                }
                mProject.RoutingList = mapper.Map<List<ARGNSRouting>>(mDbContext.C_ARGNS_ROUTING.Where(t => t.U_Active == "Y").ToList());
                mProject.DepartmentList = mapper.Map<List<Department>>(mDbContext.OUDP.ToList());
                mProject.RolSAPList = mapper.Map<List<RolSAP>>(mDbContext.OHTY.ToList());
                C_ARGNS_MODEL mModel = mDbContext.C_ARGNS_MODEL.Where(c => c.Code == pModelId).FirstOrDefault();
                if(mModel != null)
                {
                    mProject.U_Pic = mModel.U_Pic;
                    mProject.u_ModelDesc = mModel.U_ModDesc;
                }

                return mProject;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetProjectByCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<ARGNSRoutingLine> GetWorkflowLines(CompanyConn pCompanyParam, string pWorkflow)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_ROUTLINES, ARGNSRoutingLine>(); }).CreateMapper();

                List<ARGNSRoutingLine> mWorkflowLines = mapper.Map<List<ARGNSRoutingLine>>(mDbContext.C_ARGNS_ROUTLINES.Where(c => c.Code == pWorkflow).ToList());


                return mWorkflowLines;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetWorkflowLines :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public string GetCriticalPathDefaultBP(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                string mDefaultBP = mDbContext.C_ARGNS_APRTEX_SETUP.FirstOrDefault().U_BPDef;

                return mDefaultBP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetCriticalPathDefaultBP :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public string UpdateProject(CompanyConn pCompanyParam, ARGNSProject pObject)
        {
            try
            {
                ARGNSProjectServiceWeb.ArgnsCriticalPath oProject = new ARGNSProjectServiceWeb.ArgnsCriticalPath();
                ARGNSProjectServiceWeb.ArgnsCriticalPathParams oProjectParams = new ARGNSProjectServiceWeb.ArgnsCriticalPathParams();
                ARGNSProjectServiceWeb.MsgHeader oProjectMsgHeader = new ARGNSProjectServiceWeb.MsgHeader();

                oProjectMsgHeader.ServiceName = ARGNSProjectServiceWeb.MsgHeaderServiceName.ARGNS_CRPATH;
                oProjectMsgHeader.ServiceNameSpecified = true;
                oProjectMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSProjectServiceWeb.ARGNS_CRPATH oProjectService = new ARGNSProjectServiceWeb.ARGNS_CRPATH();
                oProjectService.MsgHeaderValue = oProjectMsgHeader;


                oProjectParams.Code = pObject.Code;
                oProject = oProjectService.GetByParams(oProjectParams);

                oProject.U_Desc = pObject.U_Desc;
                oProject.U_Status = pObject.U_Status;
                oProject.U_Calendar = pObject.U_Calendar;
                oProject.U_SDate = pObject.U_SDate.Value;
                oProject.U_SDateSpecified = true;
                
                #region Activity List

                if (pObject.ActivitiesList.Count > 0 || oProject.ARGNS_CRPATHACTCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en la CostSheet
                    foreach (var mActivitySAP in oProject.ARGNS_CRPATHACTCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSCrPathActivities mActivity = pObject.ActivitiesList.Where(c => c.LineId == mActivitySAP.LineId).FirstOrDefault();
                        if (mActivity != null)
                        {
                            mActivitySAP.U_Depart = mActivity.U_Depart;
                            mActivitySAP.U_Role = mActivity.U_Role;
                            mActivitySAP.U_Manager = mActivity.U_Manager;
                            mActivitySAP.U_User = mActivity.U_User;
                            mActivitySAP.U_Bp = mActivity.U_Bp;
                            if (mActivity.U_PlSDate.HasValue)
                            {
                                mActivitySAP.U_PlSDate = mActivity.U_PlSDate.Value;
                                mActivitySAP.U_PlSDateSpecified = true;
                            }
                            if (mActivity.U_PlCDate.HasValue)
                            {
                                mActivitySAP.U_PlCDate = mActivity.U_PlCDate.Value;
                                mActivitySAP.U_PlCDateSpecified = true;
                            }
                        }
                    }
                }

                #endregion

                oProjectService.Update(oProject);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateProject :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public JsonObjectResult AddProject(CompanyConn pCompanyParam, ARGNSProject pObject)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            try
            {
                ARGNSProjectServiceWeb.ArgnsCriticalPath oProject = new ARGNSProjectServiceWeb.ArgnsCriticalPath();
                ARGNSProjectServiceWeb.ArgnsCriticalPathParams oProjectParams = new ARGNSProjectServiceWeb.ArgnsCriticalPathParams();
                ARGNSProjectServiceWeb.MsgHeader oProjectMsgHeader = new ARGNSProjectServiceWeb.MsgHeader();

                oProjectMsgHeader.ServiceName = ARGNSProjectServiceWeb.MsgHeaderServiceName.ARGNS_CRPATH;
                oProjectMsgHeader.ServiceNameSpecified = true;
                oProjectMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSProjectServiceWeb.ARGNS_CRPATH oProjectService = new ARGNSProjectServiceWeb.ARGNS_CRPATH();
                oProjectService.MsgHeaderValue = oProjectMsgHeader;

                oProject.Code = GetLastProjectCode(pCompanyParam);
                oProject.U_Desc = pObject.U_Desc;
                oProject.U_Model = pObject.U_Model;
                oProject.U_Workflow = pObject.U_Workflow;                
                oProject.U_Status = pObject.U_Status;
                oProject.U_Calendar = pObject.U_Calendar;
                oProject.U_Planning = pObject.U_Planning;
                oProject.U_SDate = pObject.U_SDate.Value;
                oProject.U_SDateSpecified = true;
                oProject.U_SalesO = pObject.U_SalesO;
                oProject.U_ProyCode = pObject.ModelID + "-" + pObject.U_Model + "-" + oProject.Code;

                #region Materials

                if (pObject.ActivitiesList.Count > 0)
                {
                    ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT[] newLines = new ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT[pObject.ActivitiesList.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSCrPathActivities mActivity in pObject.ActivitiesList.ToList())
                    {
                        ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT lineActivity = new ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT();
                        lineActivity.LineId = mActivity.LineId;
                        lineActivity.LineIdSpecified = true;

                        lineActivity.U_Updated = mActivity.U_Updated;
                        lineActivity.U_PhaseId = mActivity.U_PhaseId;
                        lineActivity.U_Desc = mActivity.U_Desc;
                        lineActivity.U_Depart = mActivity.U_Depart;
                        lineActivity.U_Role = mActivity.U_Role;
                        lineActivity.U_Manager = mActivity.U_Manager;
                        lineActivity.U_User = mActivity.U_User;
                        lineActivity.U_Bp = mActivity.U_Bp;
                        lineActivity.U_ActSAP = mActivity.U_ActSAP;
                        if (mActivity.U_PlanLead.HasValue)
                        {
                            lineActivity.U_PlanLead = (double)mActivity.U_PlanLead;
                            lineActivity.U_PlanLeadSpecified = true;
                            lineActivity.U_DelivDay = (double)mActivity.U_PlanLead;
                            lineActivity.U_DelivDaySpecified = true;
                        }
                        if (mActivity.U_PlSDate.HasValue)
                        {
                            lineActivity.U_PlSDate = mActivity.U_PlSDate.Value;
                            lineActivity.U_PlSDateSpecified = true;
                            lineActivity.U_SDate = mActivity.U_PlSDate.Value;
                            lineActivity.U_SDateSpecified = true;
                        }
                        if (mActivity.U_PlCDate.HasValue)
                        {
                            lineActivity.U_PlCDate = mActivity.U_PlCDate.Value;
                            lineActivity.U_PlCDateSpecified = true;
                            lineActivity.U_CDate = mActivity.U_PlCDate.Value;
                            lineActivity.U_CDateSpecified = true;
                        }

                        newLines[positionNewLine] = lineActivity;
                        positionNewLine += 1;
                    }
                    oProject.ARGNS_CRPATHACTCollection = new ARGNSProjectServiceWeb.ArgnsCriticalPathARGNS_CRPATHACT[newLines.Length];
                    oProject.ARGNS_CRPATHACTCollection = newLines;
                }

                #endregion

                oProjectParams = oProjectService.Add(oProject);
                mJsonObjectResult.Code = oProjectParams.Code;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public string UpdateActivityCodeInProjectLines(CompanyConn pCompanyParam, Dictionary<string,string> pActivityCodeList, string pProjectCode)
        {
            try
            {
                ARGNSProjectServiceWeb.ArgnsCriticalPath oProject = new ARGNSProjectServiceWeb.ArgnsCriticalPath();
                ARGNSProjectServiceWeb.ArgnsCriticalPathParams oProjectParams = new ARGNSProjectServiceWeb.ArgnsCriticalPathParams();
                ARGNSProjectServiceWeb.MsgHeader oProjectMsgHeader = new ARGNSProjectServiceWeb.MsgHeader();

                oProjectMsgHeader.ServiceName = ARGNSProjectServiceWeb.MsgHeaderServiceName.ARGNS_CRPATH;
                oProjectMsgHeader.ServiceNameSpecified = true;
                oProjectMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSProjectServiceWeb.ARGNS_CRPATH oProjectService = new ARGNSProjectServiceWeb.ARGNS_CRPATH();
                oProjectService.MsgHeaderValue = oProjectMsgHeader;


                oProjectParams.Code = pProjectCode;
                oProject = oProjectService.GetByParams(oProjectParams);

                #region Activity List

                if (oProject.ARGNS_CRPATHACTCollection.Count() > 0)
                {
                    foreach (var mActivitySAP in oProject.ARGNS_CRPATHACTCollection)
                    {      
                       mActivitySAP.U_ActSAP = pActivityCodeList["LineId_" + mActivitySAP.LineId];
                    }
                }

                #endregion

                oProjectService.Update(oProject);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateActivityCodeInProjectLines :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string GetLastProjectCode(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                int mCode = mDbContext.C_ARGNS_CRPATH.Max(c => c.DocEntry);
                if (mCode > 0)
                {
                    return (mCode + 1).ToString();
                }
                else
                {
                    return "1";
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetLastProjectCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        #endregion

        #region MyStyles
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pBPCode"></param>
		/// <param name="mPDMSAPCombo"></param>
		/// <param name="pCodeModel"></param>
		/// <param name="pNameModel"></param>
		/// <param name="pSeasonModel"></param>
		/// <param name="pGroupModel"></param>
		/// <param name="pBrand"></param>
		/// <param name="pCollection"></param>
		/// <param name="pSubCollection"></param>
		/// <returns></returns>
        public List<ARGNSModel> GetBPPDMListSearch(CompanyConn pCompanyParam, string pBPCode, 
			ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "", 
			string pNameModel = "", string pSeasonModel = "", 
			string pGroupModel = "", string pBrand = "", 
			string pCollection = "", string pSubCollection = "", 
			int pStart = 0, int pLength = 0)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<C_ARGNS_MODEL, ARGNSModel>();

                ARGNSSeason mSeason = Mapper.Map<ARGNSSeason>(mDbContext.C_ARGNS_SEASON.Where(c => c.Name.ToUpper().Contains(pSeasonModel.ToUpper())).FirstOrDefault());
                string mSeasonCode = (mSeason == null ? null : mSeason.Code);
                ARGNSModelGroup mGroupModelList = Mapper.Map<ARGNSModelGroup>(mDbContext.C_ARGNS_MODELGRP.Where(c => c.Name.ToUpper().Contains(pGroupModel.ToUpper())).FirstOrDefault());
                string mModelGroupCode = (mGroupModelList == null ? null : mGroupModelList.Code);
                ARGNSBrand mBrandList = Mapper.Map<ARGNSBrand>(mDbContext.C_ARGNS_BRAND.Where(c => c.Name.ToUpper().Contains(pBrand.ToUpper())).FirstOrDefault());
                string mBrandCode = (mBrandList == null ? null : mBrandList.Code);
                ARGNSCollection mCollList = Mapper.Map<ARGNSCollection>(mDbContext.C_ARGNS_COLLECTION.Where(c => c.U_Desc.ToUpper().Contains(pCollection.ToUpper())).FirstOrDefault());
                string mCollCode = (mCollList == null ? null : mCollList.U_CollCode);
                ARGNSSubCollection mSubCollList = Mapper.Map<ARGNSSubCollection>(mDbContext.C_ARGNS_AMBIENT.Where(c => c.U_Desc.ToUpper().Contains(pSubCollection.ToUpper())).FirstOrDefault());
                string mSubCollCode = (mSubCollList == null ? null : mSubCollList.U_AmbCode);

                //Como estaba esta consulta despues se cambio a la que sigue para solucionar bug: http://argentisdevelop.com.ar:8080/tfs/Argentis/WebPortal/_workitems#_a=edit&id=1529
                //var mSubQuery = (from mCust in mDbContext.C_ARGNS_MODVENDOR
                //                 where mCust.U_CardType == "C" && mCust.U_CardCode == pBPCode
                //                 select mCust.U_ModCode).ToList();

                var mSubQuery = (from mCust in mDbContext.C_ARGNS_MODVENDOR
                                 where (mCust.U_CardType == "C" || mCust.U_CardType == "S") && mCust.U_CardCode == pBPCode
                                 select mCust.U_ModCode).ToList();


                List<C_ARGNS_MODEL> ListRestu = (from mModel in mDbContext.C_ARGNS_MODEL
                                                 where mModel.U_Customer == pBPCode || mSubQuery.Contains(mModel.U_ModCode)
                                                 select mModel).ToList();

                ListRestu = ListRestu.Where(c => (c.U_ModCode.Contains(pCodeModel)) && (pNameModel != "" ? c.U_ModDesc.Contains(pNameModel) : true) && (pSeasonModel != "" ? c.U_Season == mSeasonCode && c.U_Season != null : true) && (pGroupModel != "" ? c.U_ModGrp == mModelGroupCode && c.U_ModGrp != null : true) && (pBrand != "" ? c.U_Brand == mBrandCode && c.U_Brand != null : true) && (pCollection != "" ? c.U_CollCode == mCollCode && c.U_CollCode != null : true) && (pSubCollection != "" ? c.U_AmbCode == mSubCollCode && c.U_AmbCode != null : true) && (c.U_Status != "-1")).ToList();


                //Lleno el combo con los datos
                mPDMSAPCombo.ListBrand = Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList());
                mPDMSAPCombo.ListCollection = Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList());
                mPDMSAPCombo.ListModelGroup = Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList());
                mPDMSAPCombo.ListSeason = Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList());
                mPDMSAPCombo.ListSubCollection = Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList());

                return Mapper.Map<List<ARGNSModel>>(ListRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceDS -> GetUserPDMListSearchDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserCode"></param>
        /// <param name="mPDMSAPCombo"></param>
        /// <param name="pCodeModel"></param>
        /// <param name="pNameModel"></param>
        /// <param name="pSeasonModel"></param>
        /// <param name="pGroupModel"></param>
        /// <param name="pBrand"></param>
        /// <param name="pCollection"></param>
        /// <param name="pSubCollection"></param>
        /// <returns></returns>
        public JsonObjectResult GetUserPDMListSearch(CompanyConn pCompanyParam,
            string pUserCode,
            ref PDMSAPCombo mPDMSAPCombo, string pCodeModel = "",
            string pNameModel = "", string pSeasonModel = "",
            string pGroupModel = "", string pBrand = "",
            string pCollection = "", string pSubCollection = "",
            int pStart = 0, int pLength = 0)
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            IMapper mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ARGNSModel, ARGNSModelView>();
                cfg.CreateMap<ARGNSSegmentation, ARGNSSegmentationView>();
            }).CreateMapper();

            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<C_ARGNS_MODEL, ARGNSModel>();

                //List<OPOR> ListRestu = mDbContext.OPOR.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) || (pDate == null ? c.DocDate != null : (c.DocDate == pDate))).ToList();
                ARGNSSeason mSeason = Mapper.Map<ARGNSSeason>(mDbContext.C_ARGNS_SEASON.Where(c => c.Name.ToUpper().Contains(pSeasonModel.ToUpper())).FirstOrDefault());
                string mSeasonCode = (mSeason == null ? null : mSeason.Code);

                ARGNSModelGroup mGroupModelList = Mapper.Map<ARGNSModelGroup>(mDbContext.C_ARGNS_MODELGRP.Where(c => c.Name.ToUpper().Contains(pGroupModel.ToUpper())).FirstOrDefault());
                string mModelGroupCode = (mGroupModelList == null ? null : mGroupModelList.Code);

                ARGNSBrand mBrandList = Mapper.Map<ARGNSBrand>(mDbContext.C_ARGNS_BRAND.Where(c => c.Name.ToUpper().Contains(pBrand.ToUpper())).FirstOrDefault());
                string mBrandCode = (mBrandList == null ? null : mBrandList.Code);

                ARGNSCollection mCollList = Mapper.Map<ARGNSCollection>(mDbContext.C_ARGNS_COLLECTION.Where(c => c.U_Desc.ToUpper().Contains(pCollection.ToUpper())).FirstOrDefault());
                string mCollCode = (mCollList == null ? null : mCollList.U_CollCode);

                ARGNSSubCollection mSubCollList = Mapper.Map<ARGNSSubCollection>(mDbContext.C_ARGNS_AMBIENT.Where(c => c.U_Desc.ToUpper().Contains(pSubCollection.ToUpper())).FirstOrDefault());
                string mSubCollCode = (mSubCollList == null ? null : mSubCollList.U_AmbCode);

                //string code = (mCode == null ? null : mCode.Code);
                //TODO Aca voy a llamar al metodo que trae todos los modelos y despues voy a filtrar en base a los anteriores parametros en visual estudio
                mJsonObjectResult.ARGNSModelList = Mapper.Map<List<ARGNSModel>>(mDbContext.C_ARGNS_MODEL
                    .Where(c => (c.U_Owner == pUserCode) && (c.U_ModCode.Contains(pCodeModel)) &&
                    (pNameModel != "" ? c.U_ModDesc.Contains(pNameModel) : true) &&
                    (pSeasonModel != "" ? c.U_Season.Contains(mSeasonCode) : true) &&
                    (pGroupModel != "" ? c.U_ModGrp.Contains(mModelGroupCode) : true) &&
                    (pBrand != "" ? c.U_Brand.Contains(mBrandCode) : true) &&
                    (pCollection != "" ? c.U_CollCode.Contains(mCollCode) : true) &&
                    (pSubCollection != "" ? c.U_AmbCode.Contains(mSubCollCode) : true) &&
                    (c.U_Status != "-1"))
                    .OrderBy(o => o.U_ModCode)
                    .Skip(pStart)
                    .Take(pLength)
                    .ToList());

                int mTotalRecords = mDbContext.C_ARGNS_MODEL
                    .Where(c => (c.U_Owner == pUserCode) && (c.U_ModCode.Contains(pCodeModel)) &&
                    (pNameModel != "" ? c.U_ModDesc.Contains(pNameModel) : true) &&
                    (pSeasonModel != "" ? c.U_Season.Contains(mSeasonCode) : true) &&
                    (pGroupModel != "" ? c.U_ModGrp.Contains(mModelGroupCode) : true) &&
                    (pBrand != "" ? c.U_Brand.Contains(mBrandCode) : true) &&
                    (pCollection != "" ? c.U_CollCode.Contains(mCollCode) : true) &&
                    (pSubCollection != "" ? c.U_AmbCode.Contains(mSubCollCode) : true) &&
                    (c.U_Status != "-1")).Count();

                //Lleno el combo con los datos
                mPDMSAPCombo.ListBrand = Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList());
                mPDMSAPCombo.ListCollection = Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList());
                mPDMSAPCombo.ListModelGroup = Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList());
                mPDMSAPCombo.ListSeason = Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList());
                mPDMSAPCombo.ListSubCollection = Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList());

                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceDS -> GetUserPDMListSearchDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserCode"></param>
        /// <param name="txtStyle"></param>
        /// <param name="txtCodeCostSheet"></param>
        /// <param name="txtDescription"></param>
        /// <returns></returns>
        public List<ARGNSCostSheet> GetUserCostSheetListSearch(CompanyConn pCompanyParam, string pUserCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();


                var mQuery = (from mCostSheet in mDbContext.C_ARGNS_COST_SHEET
                              join mModel in mDbContext.C_ARGNS_MODEL on mCostSheet.U_ModCode equals mModel.U_ModCode
                              select new { mCostSheet.Code, mCostSheet.DocEntry, mCostSheet.Name, mCostSheet.U_ModCode, mCostSheet.U_Description, ModeDBCode = mModel.Code, mModel.U_Owner });

                List<ARGNSCostSheet> listReturn = mQuery.Where(c => (c.U_Owner == pUserCode) &&
                                 (txtStyle != "" ? c.U_ModCode.Contains(txtStyle) : true) &&
                                 (txtCodeCostSheet != "" ? c.Name.Contains(txtCodeCostSheet) : true) &&
                                 (txtDescription != "" ? c.U_Description.Contains(txtDescription) : true)).Select(j => new ARGNSCostSheet()
                                 {
                                     Code = j.Code,
                                     DocEntry = j.DocEntry,
                                     Name = j.Name,
                                     U_ModCode = j.U_ModCode,
                                     U_Description = j.U_Description,
                                     ModeDBCode = j.ModeDBCode
                                 }).ToList();
                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetUserCostSheetListSearchDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSCostSheet> GetBPCostSheetListSearch(CompanyConn pCompanyParam, string pBPCode, string txtStyle = "", string txtCodeCostSheet = "", string txtDescription = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();


                var mSubQuery = (from mCust in mDbContext.C_ARGNS_MODVENDOR
                                 where mCust.U_CardType == "S" && mCust.U_CardCode == pBPCode
                                 select mCust.U_ModCode).ToList();

                var mQuery = (from mCostSheet in mDbContext.C_ARGNS_COST_SHEET
                              join mModel in mDbContext.C_ARGNS_MODEL on mCostSheet.U_ModCode equals mModel.U_ModCode
                              where mModel.U_Customer == pBPCode || mSubQuery.Contains(mModel.U_ModCode)
                              select new
                              {
                                  mCostSheet.Code,
                                  mCostSheet.DocEntry,
                                  mCostSheet.Name,
                                  mCostSheet.U_ModCode,
                                  mCostSheet.U_Description,
                                  ModeDBCode = mModel.Code,
                                  mModel.U_Owner
                              });

                List<ARGNSCostSheet> listReturn = mQuery.Where(c => (txtStyle != "" ? c.U_ModCode.Contains(txtStyle) : true) &&
                                 (txtCodeCostSheet != "" ? c.Name.Contains(txtCodeCostSheet) : true) &&
                                 (txtDescription != "" ? c.U_Description.Contains(txtDescription) : true)).Select(j => new ARGNSCostSheet()
                                 {
                                     Code = j.Code,
                                     DocEntry = j.DocEntry,
                                     Name = j.Name,
                                     U_ModCode = j.U_ModCode,
                                     U_Description = j.U_Description,
                                     ModeDBCode = j.ModeDBCode
                                 }).ToList();
                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataService -> GetUserCostSheetListSearchDS :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }
        #endregion

        #region Samples

        public List<ARGNSModelSample> GetModelSapmples(CompanyConn pCompanyParam, string pModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_SAMPLES, ARGNSModelSample>();
                List<ARGNSModelSample> listResult = Mapper.Map<List<ARGNSModelSample>>(mDbContext.C_ARGNS_SAMPLES.Where(c => c.U_ModCode == pModCode).ToList());
                List<OUSR> listUser = mDbContext.OUSR.ToList();
                foreach (ARGNSModelSample sampleAux in listResult)
                {
                    OUSR userAux = listUser.Where(j => j.USERID.ToString() == sampleAux.U_User).FirstOrDefault();
                    sampleAux.USER_CODE = (userAux != null) ? userAux.USER_CODE : "";
                }
                return listResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelSapmples :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }

        }

        public ARGNSModelSample GetSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_SAMPLES, ARGNSModelSample>();
                Mapper.CreateMap<C_ARGNS_SAMPEVAL, ARGNS_SampleVal>();
                Mapper.CreateMap<OUSR, UserSAP>();
                Mapper.CreateMap<C_ARGNS_POMTEMPLATE, ARGNSModelPomTemplate>();
                Mapper.CreateMap<C_ARGNS_SAMPLESLINES, ARGNSModelSampleLine>();

                ARGNSModelSample mResult = Mapper.Map<ARGNSModelSample>(mDbContext.C_ARGNS_SAMPLES.Where(c => c.Code == pSampleCode).FirstOrDefault());

                if (mResult == null)
                {
                    mResult = new ARGNSModelSample();
                }

                mResult.ListSampleVal = Mapper.Map<List<ARGNS_SampleVal>>(mDbContext.C_ARGNS_SAMPEVAL.ToList());
                mResult.ListUser = Mapper.Map<List<UserSAP>>(mDbContext.OUSR.ToList());
                mResult.ListPOM = Mapper.Map<List<ARGNSModelPomTemplate>>(mDbContext.C_ARGNS_POMTEMPLATE.ToList());
                mResult.ListScale = (from mARGNS_MODEL in mDbContext.C_ARGNS_MODEL
                                     join mSize in mDbContext.C_ARGNS_MODEL_SIZE on mARGNS_MODEL.Code equals mSize.Code
                                     //TODO Esta parte del where la voy a hacer aca en el visual studio, el metodo GetCostSheetListSearch en el B1if trae la union entre la model y la costsheet nomas
                                     where
                                     (
                                        (mARGNS_MODEL.U_ModCode == pModCode)
                                     )
                                     select new ARGNSScale()
                                     {
                                         U_SclCode = mSize.U_SclCode
                                     }).Distinct().ToList();
                mResult.ListLine = Mapper.Map<List<ARGNSModelSampleLine>>(mDbContext.C_ARGNS_SAMPLESLINES.Where(c => c.Code == pSampleCode).ToList());

                return mResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetSample :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public ARGNSModelSample GetSampleByCodSample(CompanyConn pCompanyParam, string pSampleCode, string pModCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_SAMPLES, ARGNSModelSample>();
                ARGNSModelSample listResult = Mapper.Map<ARGNSModelSample>(mDbContext.C_ARGNS_SAMPLES.Where(c => c.U_ModCode == pModCode && c.U_SampCode == pSampleCode).FirstOrDefault());
                return listResult;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetSampleByCodSample :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public string GetLastSampleCode(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                return (mDbContext.C_ARGNS_SAMPLES.Max(c => c.DocEntry) + 1).ToString();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetLastSampleCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public string AddSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            ARGNSSampleServiceWeb.ArgnsStyleSample oSample = new ARGNSSampleServiceWeb.ArgnsStyleSample();
            ARGNSSampleServiceWeb.MsgHeader oSampleMsgHeader = new ARGNSSampleServiceWeb.MsgHeader();

            oSampleMsgHeader.ServiceName = ARGNSSampleServiceWeb.MsgHeaderServiceName.ARGNS_SAMPLES;
            oSampleMsgHeader.ServiceNameSpecified = true;
            oSampleMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSSampleServiceWeb.ARGNS_SAMPLES oSampleServiceWeb = new ARGNSSampleServiceWeb.ARGNS_SAMPLES();
            oSampleServiceWeb.MsgHeaderValue = oSampleMsgHeader;
            try
            {
                oSample.U_DateSpecified = true;
                oSample.U_ApproDateSpecified = true;

                oSample.Code = GetLastSampleCode(pCompanyParam);
                oSample.U_ModCode = pObject.U_ModCode;
                oSample.U_Type = pObject.U_Type;
                oSample.U_SampCode = pObject.U_SampCode;
                oSample.U_Status = pObject.U_Status;
                oSample.U_SclCode = pObject.U_SclCode;
                oSample.U_POM = pObject.U_POM;
                oSample.U_Date = pObject.U_Date;
                oSample.U_User = pObject.U_User;
                oSample.U_ApproDate = pObject.U_ApproDate;
                oSample.U_Active = pObject.U_Active;
                oSample.U_Comments = pObject.U_Comments;
                oSample.U_BPCustomer = pObject.U_BPCustomer;
                oSample.U_BPSupp = pObject.U_BPSupp;
                oSample.U_Picture = pObject.U_Picture;

                int counter = 0;
                if (pObject.ListLine.Count > 0)
                {
                    oSample.ARGNS_SAMPLESLINESCollection = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES[pObject.ListLine.Count];
                    foreach (ARGNSModelSampleLine item in pObject.ListLine)
                    {
                        oSample.ARGNS_SAMPLESLINESCollection[counter] = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES();

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineIdSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_TargetSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_ActualSpecified = true;

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineId = item.LineId;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeCode = item.U_SizeCode;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Desc = item.U_Desc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_POM = item.U_POM;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeDesc = item.U_SizeDesc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Actual = (double)item.U_Actual;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Target = (double)item.U_Target;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Revision = item.U_Revision;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SclCode = item.U_SclCode;
                        counter++;
                    }
                }


                oSampleServiceWeb.Add(oSample);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataDS -> AddSample :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string UpdateSample(ARGNSModelSample pObject, CompanyConn pCompanyParam)
        {
            ARGNSSampleServiceWeb.ArgnsStyleSample oSample = new ARGNSSampleServiceWeb.ArgnsStyleSample();
            ARGNSSampleServiceWeb.ArgnsStyleSampleParams oSampleParams = new ARGNSSampleServiceWeb.ArgnsStyleSampleParams();
            ARGNSSampleServiceWeb.MsgHeader oSampleMsgHeader = new ARGNSSampleServiceWeb.MsgHeader();

            oSampleMsgHeader.ServiceName = ARGNSSampleServiceWeb.MsgHeaderServiceName.ARGNS_SAMPLES;
            oSampleMsgHeader.ServiceNameSpecified = true;
            oSampleMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSSampleServiceWeb.ARGNS_SAMPLES oSampleServiceWeb = new ARGNSSampleServiceWeb.ARGNS_SAMPLES();
            oSampleServiceWeb.MsgHeaderValue = oSampleMsgHeader;

            try
            {
                oSampleParams.Code = pObject.Code;
                oSample = oSampleServiceWeb.GetByParams(oSampleParams);

                oSample.U_DateSpecified = true;
                oSample.U_ApproDateSpecified = true;

                oSample.U_ModCode = pObject.U_ModCode;
                oSample.U_Type = pObject.U_Type;
                oSample.U_SampCode = pObject.U_SampCode;
                oSample.U_Status = pObject.U_Status;
                oSample.U_SclCode = pObject.U_SclCode;
                oSample.U_POM = pObject.U_POM;
                oSample.U_Date = pObject.U_Date;
                oSample.U_User = pObject.U_User;
                oSample.U_ApproDate = pObject.U_ApproDate;
                oSample.U_Active = pObject.U_Active;
                oSample.U_Comments = pObject.U_Comments;
                oSample.U_BPCustomer = pObject.U_BPCustomer;
                oSample.U_BPSupp = pObject.U_BPSupp;
                oSample.U_Picture = pObject.U_Picture;

                //oSample.ARGNS_SAMPLESLINESCollection = null;
                //oSampleServiceWeb.Update(oSample);

                int counter = 0;
                if (pObject.ListLine.Count > 0)
                {

                    oSample.ARGNS_SAMPLESLINESCollection = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES[pObject.ListLine.Count];
                    foreach (ARGNSModelSampleLine item in pObject.ListLine)
                    {
                        oSample.ARGNS_SAMPLESLINESCollection[counter] = new ARGNSSampleServiceWeb.ArgnsStyleSampleARGNS_SAMPLESLINES();

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineIdSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_TargetSpecified = true;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_ActualSpecified = true;

                        oSample.ARGNS_SAMPLESLINESCollection[counter].LineId = counter + 1;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeCode = item.U_SizeCode;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Desc = item.U_Desc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_POM = item.U_POM;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SizeDesc = item.U_SizeDesc;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Target = (double)item.U_Target;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Actual = (double)item.U_Actual;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_Revision = item.U_Revision;
                        oSample.ARGNS_SAMPLESLINESCollection[counter].U_SclCode = item.U_SclCode;
                        counter++;
                    }
                }

                oSampleServiceWeb.Update(oSample);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataDS -> UpdateSample :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region PDC

        public List<ARGNSPDC> GetPDCList(CompanyConn pCompanyParam, ref PDCSAPCombo combo, DateTime? pPD, string pCode = "", string pEmployee = "", string pShift = "", string pVendor = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_PROD_DATACOL, ARGNSPDC>();
                Mapper.CreateMap<C_ARGNS_SHIFTS, ARGNSShifts>();
                Mapper.CreateMap<OCRD, BusinessPartnerSAP>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();

                combo.ListShifts = Mapper.Map<List<ARGNSShifts>>(mDbContext.C_ARGNS_SHIFTS.ToList());
                combo.ListBusinessPartner = Mapper.Map<List<BusinessPartnerSAP>>(mDbContext.OCRD.ToList());
                combo.ListEmployee = Mapper.Map<List<EmployeeSAP>>(mDbContext.OHEM.ToList());
                List<ARGNSPDC> listReturn = Mapper.Map<List<ARGNSPDC>>(mDbContext.C_ARGNS_PROD_DATACOL.Where(c => c.Code.Contains(pCode) && (pPD != null ? c.U_Date == pPD : true) && ((pEmployee != "" && pEmployee != "-1") ? c.U_empID.ToString() == pEmployee : true) && ((pShift != "" && pShift != "-1") ? c.U_ShiftCode == pShift : true) && (c.U_Vendor != null ? c.U_Vendor.Contains(pVendor) : true)).ToList());
                return listReturn;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetPDCList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public ARGNSPDC GetPDCById(CompanyConn pCompanyParam, string pPDCCode, ref PDCSAPCombo combo)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_PROD_DATACOL, ARGNSPDC>();
                Mapper.CreateMap<C_ARGNS_SHIFTS, ARGNSShifts>();
                Mapper.CreateMap<OCRD, BusinessPartnerSAP>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                Mapper.CreateMap<C_ARGNS_WORKCENTERPRO, ARGNSWorkCenter>();
                Mapper.CreateMap<C_ARGNS_PRODCOLLNS, ARGNSPDCLine>();

                combo.ListShifts = Mapper.Map<List<ARGNSShifts>>(mDbContext.C_ARGNS_SHIFTS.ToList());
                combo.ListBusinessPartner = Mapper.Map<List<BusinessPartnerSAP>>(mDbContext.OCRD.ToList());
                combo.ListEmployee = Mapper.Map<List<EmployeeSAP>>(mDbContext.OHEM.ToList());
                combo.ListWorkCenter = Mapper.Map<List<ARGNSWorkCenter>>(mDbContext.C_ARGNS_WORKCENTERPRO.ToList());
                ARGNSPDC mReturn = new ARGNSPDC();
                if (Convert.ToUInt32(pPDCCode) != 0)
                {
                    mReturn = Mapper.Map<ARGNSPDC>(mDbContext.C_ARGNS_PROD_DATACOL.Where(c => c.Code == pPDCCode).FirstOrDefault());
                    mReturn.Lines = Mapper.Map<List<ARGNSPDCLine>>(mDbContext.C_ARGNS_PRODCOLLNS.Where(c => c.Code == pPDCCode).ToList());
                }
                return mReturn;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetPDCById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public PDCSAPCombo GetPDCCombo(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<C_ARGNS_SHIFTS, ARGNSShifts>();
                Mapper.CreateMap<OCRD, BusinessPartnerSAP>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();

                PDCSAPCombo combo = new PDCSAPCombo();
                combo.ListShifts = Mapper.Map<List<ARGNSShifts>>(mDbContext.C_ARGNS_SHIFTS.ToList());
                combo.ListBusinessPartner = Mapper.Map<List<BusinessPartnerSAP>>(mDbContext.OCRD.ToList());
                combo.ListEmployee = Mapper.Map<List<EmployeeSAP>>(mDbContext.OHEM.ToList());
                return combo;

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetPDCCombo :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public ARGNSPDCLine GetPDCLineBarCode(CompanyConn pCompanyParam, string CodeBar)
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<C_ARGNS_PRODCOLLNS> listAuxARGNS_PRODCOLLNS = mDbContext.C_ARGNS_PRODCOLLNS.Where(j => j.U_CodeBars == CodeBar).ToList();
                decimal? sumBadQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_BadQty);
                decimal? sumQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_Qty);

                ARGNSPDCLine mPDCLineReturn = (from mARGNS_SENDOUT_PAR in mDbContext.C_ARGNS_SENDOUT_PAR
                                               join mARGNS_SENDOUT_STG in mDbContext.C_ARGNS_SENDOUT_STG on mARGNS_SENDOUT_PAR.DocEntry equals mARGNS_SENDOUT_STG.DocEntry into group1
                                               from mGroup1 in group1.DefaultIfEmpty()
                                               join mOITM in mDbContext.OITM on mARGNS_SENDOUT_PAR.U_ItemCode equals mOITM.ItemCode into group2
                                               from mGroup2 in group2.DefaultIfEmpty()
                                               join mARGNS_MODEL in mDbContext.C_ARGNS_MODEL on mGroup2.U_ARGNS_MOD equals mARGNS_MODEL.U_ModCode into group3
                                               from mGroup3 in group3.DefaultIfEmpty()
                                               where
                                               (
                                                 (mARGNS_SENDOUT_PAR.U_BarCode == CodeBar)
                                               )
                                               select new ARGNSPDCLine()
                                               {
                                                   U_ProdOrd = mGroup1.U_CutTic.ToString(),
                                                   U_Model = mGroup2.U_ARGNS_MOD,
                                                   U_Color = mGroup2.U_ARGNS_COL,
                                                   U_Scale = mGroup2.U_ARGNS_SCL,
                                                   U_Size = mGroup2.U_ARGNS_SIZE,
                                                   U_Variable = (string.IsNullOrEmpty(mGroup2.U_ARGNS_VAR) ? "" : mGroup2.U_ARGNS_VAR),
                                                   U_OperCode = mGroup1.U_Oper,
                                                   U_IssQty = mARGNS_SENDOUT_PAR.U_IssQty.ToString(),
                                                   U_Qty = (((sumQty + sumBadQty) > 0) ? mARGNS_SENDOUT_PAR.U_IssQty - (sumQty + sumBadQty) : mARGNS_SENDOUT_PAR.U_IssQty).ToString(),
                                                   U_Stage = mGroup1.DocEntry.ToString()
                                               }).FirstOrDefault();
                if (mPDCLineReturn != null)
                {
                    List<string> auxQuery =
                               (from mARGNS_OPERTEMPLATE in mDbContext.C_ARGNS_OPERTEMPLATE
                                join mARGNS_OPERTEMPLATEL in mDbContext.C_ARGNS_OPERTEMPLATEL on mARGNS_OPERTEMPLATE.Code equals mARGNS_OPERTEMPLATEL.Code into internalGroup1
                                from minternalGroup1 in internalGroup1.DefaultIfEmpty()
                                join mARGNS_CUTTIC in mDbContext.C_ARGNS_CUTTIC on mARGNS_OPERTEMPLATE.U_OTCode equals mARGNS_CUTTIC.U_OperNo into internalGroup2
                                from minternalGroup2 in internalGroup2.DefaultIfEmpty()
                                join mARGNS_RESOURCE in mDbContext.C_ARGNS_RESOURCE on minternalGroup1.U_ResCode equals mARGNS_RESOURCE.U_ResCode into internalGroup3
                                from minternalGroup3 in internalGroup3.DefaultIfEmpty()
                                where (
                                       (minternalGroup2.DocEntry.ToString() == mPDCLineReturn.U_ProdOrd) &&
                                       (minternalGroup1.U_ItemCode == mPDCLineReturn.U_OperCode)
                                   )
                                select new List<string> { minternalGroup3.U_WorkCePrCode, minternalGroup3.U_ResCode, minternalGroup3.U_ResName }
                   ).FirstOrDefault();

                    mPDCLineReturn.U_WorkCtr = auxQuery[0];
                    mPDCLineReturn.U_ResourID = auxQuery[1];
                    mPDCLineReturn.U_ResName = auxQuery[2];
                    mPDCLineReturn.U_CodeBars = CodeBar;
                    mPDCLineReturn.U_BadQty = "0";
                }

                return mPDCLineReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetPDCLineBarCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<ARGNSPDCLine> GetPDCLineCutTick(CompanyConn pCompanyParam, string CutTick)
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<ARGNSPDCLine> mPDCListReturn = (from mARGNS_SENDOUT_PAR in mDbContext.C_ARGNS_SENDOUT_PAR
                                                     join mARGNS_SENDOUT_STG in mDbContext.C_ARGNS_SENDOUT_STG on mARGNS_SENDOUT_PAR.DocEntry equals mARGNS_SENDOUT_STG.DocEntry into group1
                                                     from mGroup1 in group1.DefaultIfEmpty()
                                                     join mOITM in mDbContext.OITM on mARGNS_SENDOUT_PAR.U_ItemCode equals mOITM.ItemCode into group2
                                                     from mGroup2 in group2.DefaultIfEmpty()
                                                     join mARGNS_MODEL in mDbContext.C_ARGNS_MODEL on mGroup2.U_ARGNS_MOD equals mARGNS_MODEL.U_ModCode into group3
                                                     from mGroup3 in group3.DefaultIfEmpty()
                                                     where
                                                     (
                                                       (mGroup1.U_CutTic.ToString() == CutTick)
                                                     )
                                                     select new ARGNSPDCLine()
                                                     {
                                                         U_ProdOrd = mGroup1.U_CutTic.ToString(),
                                                         U_CodeBars = mARGNS_SENDOUT_PAR.U_BarCode,
                                                         U_Model = mGroup2.U_ARGNS_MOD,
                                                         U_Color = mGroup2.U_ARGNS_COL,
                                                         U_Scale = mGroup2.U_ARGNS_SCL,
                                                         U_Size = mGroup2.U_ARGNS_SIZE,
                                                         U_Variable = (string.IsNullOrEmpty(mGroup2.U_ARGNS_VAR) ? "" : mGroup2.U_ARGNS_VAR),
                                                         U_OperCode = mGroup1.U_Oper,
                                                         U_IssQty = mARGNS_SENDOUT_PAR.U_IssQty.ToString(),
                                                         //U_Qty = (((sumQty + sumBadQty) > 0) ? mARGNS_SENDOUT_PAR.U_IssQty - (sumQty + sumBadQty) : mARGNS_SENDOUT_PAR.U_IssQty).ToString(),
                                                         U_Stage = mGroup1.DocEntry.ToString()
                                                     }).ToList();

                foreach (ARGNSPDCLine line in mPDCListReturn)
                {
                    List<C_ARGNS_PRODCOLLNS> listAuxARGNS_PRODCOLLNS = mDbContext.C_ARGNS_PRODCOLLNS.Where(j => j.U_CodeBars == line.U_CodeBars).ToList();
                    decimal? sumBadQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_BadQty);
                    decimal? sumQty = listAuxARGNS_PRODCOLLNS.Sum(c => c.U_Qty);

                    List<string> auxQuery =
                               (from mARGNS_OPERTEMPLATE in mDbContext.C_ARGNS_OPERTEMPLATE
                                join mARGNS_OPERTEMPLATEL in mDbContext.C_ARGNS_OPERTEMPLATEL on mARGNS_OPERTEMPLATE.Code equals mARGNS_OPERTEMPLATEL.Code into internalGroup1
                                from minternalGroup1 in internalGroup1.DefaultIfEmpty()
                                join mARGNS_CUTTIC in mDbContext.C_ARGNS_CUTTIC on mARGNS_OPERTEMPLATE.U_OTCode equals mARGNS_CUTTIC.U_OperNo into internalGroup2
                                from minternalGroup2 in internalGroup2.DefaultIfEmpty()
                                join mARGNS_RESOURCE in mDbContext.C_ARGNS_RESOURCE on minternalGroup1.U_ResCode equals mARGNS_RESOURCE.U_ResCode into internalGroup3
                                from minternalGroup3 in internalGroup3.DefaultIfEmpty()
                                where (
                                       (minternalGroup2.DocEntry.ToString() == line.U_ProdOrd) &&
                                       (minternalGroup1.U_ItemCode == line.U_OperCode)
                                   )
                                select new List<string> { minternalGroup3.U_WorkCePrCode, minternalGroup3.U_ResCode, minternalGroup3.U_ResName }
                   ).FirstOrDefault();

                    line.U_WorkCtr = auxQuery[0];
                    line.U_ResourID = auxQuery[1];
                    line.U_ResName = auxQuery[2];
                    line.U_BadQty = "0";
                    line.U_Qty = (((sumQty + sumBadQty) > 0) ? Convert.ToDecimal(line.U_IssQty) - (sumQty + sumBadQty) : Convert.ToDecimal(line.U_IssQty)).ToString();
                }
                mPDCListReturn = mPDCListReturn.Where(c => Convert.ToDecimal(c.U_Qty) != 0).ToList();


                return mPDCListReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetPDCLineCutTick :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public string AddPDC(CompanyConn pCompanyParam, ARGNSPDC pPDC)
        {
            ARGNSPDCServiceWeb.ArgnsProdDataCollection oPDC = new ARGNSPDCServiceWeb.ArgnsProdDataCollection();
            ARGNSPDCServiceWeb.MsgHeader oPDCMsgHeader = new ARGNSPDCServiceWeb.MsgHeader();

            oPDCMsgHeader.ServiceName = ARGNSPDCServiceWeb.MsgHeaderServiceName.ARGNS_PROD_DATACOL;
            oPDCMsgHeader.ServiceNameSpecified = true;
            oPDCMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSPDCServiceWeb.ARGNS_PROD_DATACOL oPDCServiceWeb = new ARGNSPDCServiceWeb.ARGNS_PROD_DATACOL();
            oPDCServiceWeb.MsgHeaderValue = oPDCMsgHeader;
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                oPDC.Code = (mDbContext.C_ARGNS_PROD_DATACOL.ToList().Max(c => Convert.ToInt32(c.Code)) + 1).ToString();
                oPDC.Canceled = "N";
                oPDC.Object = "ARGNS_PROD_DATACOL";
                oPDC.CreateDate = System.DateTime.Now;
                oPDC.DataSource = "I";
                oPDC.Transfered = "N";
                oPDC.U_empID = (long)pPDC.U_empID;
                oPDC.U_empIDSpecified = true;
                oPDC.U_ShiftCode = pPDC.U_ShiftCode;
                oPDC.U_Vendor = pPDC.U_Vendor;
                if (pPDC.U_Date != null)
                {
                    oPDC.U_Date = pPDC.U_Date.Value;
                }
                oPDC.U_DateSpecified = true;

                if (pPDC.U_UnprodMins != null && !string.IsNullOrEmpty(pPDC.U_UnprodMins))
                {
                    oPDC.U_UnprodMins = Convert.ToInt32(pPDC.U_UnprodMins);
                    oPDC.U_UnprodMinsSpecified = true;
                }
                if (pPDC.U_MinPres != null && !string.IsNullOrEmpty(pPDC.U_MinPres))
                {
                    oPDC.U_MinPres = Convert.ToInt32(pPDC.U_MinPres);
                    oPDC.U_MinPresSpecified = true;
                }

                int counter = 0;
                if (pPDC.Lines.Count > 0)
                {
                    oPDC.ARGNS_PRODCOLLNSCollection = new ARGNSPDCServiceWeb.ArgnsProdDataCollectionARGNS_PRODCOLLNS[pPDC.Lines.Count];
                    foreach (ARGNSPDCLine item in pPDC.Lines)
                    {
                        oPDC.ARGNS_PRODCOLLNSCollection[counter] = new ARGNSPDCServiceWeb.ArgnsProdDataCollectionARGNS_PRODCOLLNS();
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].LineId = Convert.ToInt32(item.LineId);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].LineIdSpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].Object = "ARGNS_PROD_DATACOL";
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_BadQty = Convert.ToDouble(item.U_BadQty);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_BadQtySpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_CodeBars = item.U_CodeBars;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Color = item.U_Color;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Model = item.U_Model;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_OperCode = item.U_OperCode;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_ProdOrd = Convert.ToUInt32(item.U_ProdOrd);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_ProdOrdSpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Qty = Convert.ToDouble(item.U_Qty);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_QtySpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Scale = item.U_Scale;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Size = item.U_Size;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Stage = Convert.ToUInt32(item.U_Stage);
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_StageSpecified = true;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_Variable = item.U_Variable;
                        oPDC.ARGNS_PRODCOLLNSCollection[counter].U_WorkCtr = item.U_WorkCtr;
                        counter++;
                    }
                }
                oPDCServiceWeb.Add(oPDC);

                ARGNSSendOutServiceWeb.ArgnsSendOut oSendOut = null;
                ARGNSSendOutServiceWeb.ArgnsSendOutParams oSendOutParams = new ARGNSSendOutServiceWeb.ArgnsSendOutParams();
                ARGNSSendOutServiceWeb.MsgHeader oSendOutMsgHeader = new ARGNSSendOutServiceWeb.MsgHeader();

                oSendOutMsgHeader.ServiceName = ARGNSSendOutServiceWeb.MsgHeaderServiceName.ARGNS_SENDOUT_STG;
                oSendOutMsgHeader.ServiceNameSpecified = true;
                oSendOutMsgHeader.SessionID = pCompanyParam.DSSessionId;

                ARGNSSendOutServiceWeb.ARGNS_SENDOUT_STG oSendOutServiceWeb = new ARGNSSendOutServiceWeb.ARGNS_SENDOUT_STG();
                oSendOutServiceWeb.MsgHeaderValue = oSendOutMsgHeader;

                foreach (ARGNSPDCLine item in pPDC.Lines)
                {
                    oSendOut = new ARGNSSendOutServiceWeb.ArgnsSendOut();
                    int auxDocEntry = mDbContext.C_ARGNS_SENDOUT_PAR.Where(c => c.U_BarCode == item.U_CodeBars).FirstOrDefault().DocEntry;
                    oSendOutParams.DocEntry = auxDocEntry;
                    oSendOutParams.DocEntrySpecified = true;
                    oSendOut = oSendOutServiceWeb.GetByParams(oSendOutParams);

                    ARGNSSendOutServiceWeb.ArgnsSendOutARGNS_SENDOUT_PAR auxPAR = oSendOut.ARGNS_SENDOUT_PARCollection.Where(c => c.U_BarCode == item.U_CodeBars).FirstOrDefault();
                    long auxProdOrderNum = auxPAR.U_ProdOrder;
                    auxPAR.U_RecQty = auxPAR.U_RecQty + Convert.ToDouble(item.U_Qty);
                    auxPAR.U_BadQty = auxPAR.U_BadQty + Convert.ToDouble(item.U_BadQty);

                    oSendOutServiceWeb.Update(oSendOut);

                    //Si tengo Bad Qty debo restarle dicha cantidad a las Issue Qty a la operacion siguiente (DocEntry +1 y ProdOrder igual a la linea donde estoy parado)
                    if (Convert.ToDouble(item.U_BadQty) > 0)
                    {
                        oSendOut = new ARGNSSendOutServiceWeb.ArgnsSendOut();
                        oSendOutParams.DocEntry = auxDocEntry + 1;
                        oSendOutParams.DocEntrySpecified = true;
                        try
                        {
                            oSendOut = oSendOutServiceWeb.GetByParams(oSendOutParams);
                        }
                        catch (Exception)
                        {
                            oSendOut = null;
                        }

                        if (oSendOut != null)
                        {
                            auxPAR = null;
                            auxPAR = oSendOut.ARGNS_SENDOUT_PARCollection.Where(c => c.U_ProdOrder == auxProdOrderNum).FirstOrDefault();
                            if (auxPAR != null)
                            {
                                auxPAR.U_IssQty = auxPAR.U_IssQty - Convert.ToDouble(item.U_BadQty);
                            }
                            oSendOutServiceWeb.Update(oSendOut);
                        }
                    }
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataService -> PDC :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region Range Plan

        public List<ARGNSRangePlan> GetRangePlanListSearch(CompanyConn pCompanyParam, ref RangePlanCombo mRangePlanCombo, string pCode = "", string pSeason = "", string pCollection = "", string pEmployee = "")
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_RANGEP, ARGNSRangePlan>();
                Mapper.CreateMap<ARGNSRangePlan, C_ARGNS_RANGEP>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<ARGNSSeason, C_ARGNS_SEASON>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<ARGNSCollection, C_ARGNS_COLLECTION>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                Mapper.CreateMap<EmployeeSAP, OHEM>();

                List<C_ARGNS_RANGEP> ListRestu = mDbContext.C_ARGNS_RANGEP.Where(c => (pCode != "" ? c.Code == pCode : true) && ((pSeason != "" && pSeason != "-1") ? c.U_Season == pSeason : true) && ((pCollection != "" && pCollection != "-1") ? c.U_Coll == pCollection : true) && ((pEmployee != "" && pEmployee != "-1") ? c.U_RPEmpl == pEmployee : true)).ToList();
                mRangePlanCombo.ListSeason = Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList());
                mRangePlanCombo.ListCollection = Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList());
                mRangePlanCombo.ListEmployee = Mapper.Map<List<EmployeeSAP>>(mDbContext.OHEM.ToList());

                return Mapper.Map<List<ARGNSRangePlan>>(ListRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetRangePlanListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pCode"></param>
		/// <returns></returns>
        public ARGNSRangePlan GetRangePlanById(CompanyConn pCompanyParam, string pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_RANGEP, ARGNSRangePlan>();
                Mapper.CreateMap<ARGNSRangePlan, C_ARGNS_RANGEP>();
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<ARGNSBrand, C_ARGNS_BRAND>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<ARGNSCollection, C_ARGNS_COLLECTION>();
                Mapper.CreateMap<C_ARGNS_DIV, ARGNSDivision>();
                Mapper.CreateMap<ARGNSDivision, C_ARGNS_DIV>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<ARGNSModelGroup, C_ARGNS_MODELGRP>();
                Mapper.CreateMap<C_ARGNS_PRODLINE, ARGNSProductLine>();
                Mapper.CreateMap<ARGNSProductLine, C_ARGNS_PRODLINE>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<ARGNSSeason, C_ARGNS_SEASON>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<ARGNSSubCollection, C_ARGNS_AMBIENT>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                Mapper.CreateMap<EmployeeSAP, OHEM>();
                Mapper.CreateMap<C_ARGNS_RANGEPDETAIL, ARGNSRangePlanDetail>();
                Mapper.CreateMap<ARGNSRangePlanDetail, C_ARGNS_RANGEPDETAIL>();

                //TODO Aca voy a llamar al metodo que trae todos los modelos y despues voy a filtrar en base a los anteriores parametros en visual estudio
                ARGNSRangePlan mRangePlanReturn = Mapper.Map<ARGNSRangePlan>(mDbContext.C_ARGNS_RANGEP.Where(c => c.Code == pCode).FirstOrDefault());
                mRangePlanReturn.Lines = Mapper.Map<List<ARGNSRangePlanDetail>>(mDbContext.C_ARGNS_RANGEPDETAIL.Where(c => c.Code == pCode).ToList());
                mRangePlanReturn.RangePlanCombo.ListBrand = Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList());
                mRangePlanReturn.RangePlanCombo.ListCollection = Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList());
                mRangePlanReturn.RangePlanCombo.ListDivision = Mapper.Map<List<ARGNSDivision>>(mDbContext.C_ARGNS_DIV.ToList());
                mRangePlanReturn.RangePlanCombo.ListModelGroup = Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList());
                mRangePlanReturn.RangePlanCombo.ListProductLine = Mapper.Map<List<ARGNSProductLine>>(mDbContext.C_ARGNS_PRODLINE.ToList());
                mRangePlanReturn.RangePlanCombo.ListSeason = Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList());
                mRangePlanReturn.RangePlanCombo.ListSubCollection = Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList());
                mRangePlanReturn.RangePlanCombo.ListEmployee = Mapper.Map<List<EmployeeSAP>>(mDbContext.OHEM.ToList());
                mRangePlanReturn.RangePlanCombo.ListProject = Mapper.Map<List<ProjectSAP>>(mDbContext.OPRJ.ToList());

                return mRangePlanReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetRangePlanById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <returns></returns>
        public RangePlanCombo GetRangePlanCombo(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                //Mapper
                Mapper.CreateMap<C_ARGNS_BRAND, ARGNSBrand>();
                Mapper.CreateMap<ARGNSBrand, C_ARGNS_BRAND>();
                Mapper.CreateMap<C_ARGNS_COLLECTION, ARGNSCollection>();
                Mapper.CreateMap<ARGNSCollection, C_ARGNS_COLLECTION>();
                Mapper.CreateMap<C_ARGNS_DIV, ARGNSDivision>();
                Mapper.CreateMap<ARGNSDivision, C_ARGNS_DIV>();
                Mapper.CreateMap<C_ARGNS_MODELGRP, ARGNSModelGroup>();
                Mapper.CreateMap<ARGNSModelGroup, C_ARGNS_MODELGRP>();
                Mapper.CreateMap<C_ARGNS_PRODLINE, ARGNSProductLine>();
                Mapper.CreateMap<ARGNSProductLine, C_ARGNS_PRODLINE>();
                Mapper.CreateMap<C_ARGNS_SEASON, ARGNSSeason>();
                Mapper.CreateMap<ARGNSSeason, C_ARGNS_SEASON>();
                Mapper.CreateMap<C_ARGNS_AMBIENT, ARGNSSubCollection>();
                Mapper.CreateMap<ARGNSSubCollection, C_ARGNS_AMBIENT>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                Mapper.CreateMap<EmployeeSAP, OHEM>();
                Mapper.CreateMap<OPRJ, ProjectSAP>();

                //TODO Aca voy a llamar al metodo que trae todos los modelos y despues voy a filtrar en base a los anteriores parametros en visual estudio
                RangePlanCombo mCombo = new RangePlanCombo();
                mCombo.ListBrand = Mapper.Map<List<ARGNSBrand>>(mDbContext.C_ARGNS_BRAND.ToList());
                mCombo.ListCollection = Mapper.Map<List<ARGNSCollection>>(mDbContext.C_ARGNS_COLLECTION.ToList());
                mCombo.ListDivision = Mapper.Map<List<ARGNSDivision>>(mDbContext.C_ARGNS_DIV.ToList());
                mCombo.ListModelGroup = Mapper.Map<List<ARGNSModelGroup>>(mDbContext.C_ARGNS_MODELGRP.ToList());
                mCombo.ListProductLine = Mapper.Map<List<ARGNSProductLine>>(mDbContext.C_ARGNS_PRODLINE.ToList());
                mCombo.ListSeason = Mapper.Map<List<ARGNSSeason>>(mDbContext.C_ARGNS_SEASON.ToList());
                mCombo.ListSubCollection = Mapper.Map<List<ARGNSSubCollection>>(mDbContext.C_ARGNS_AMBIENT.ToList());
                mCombo.ListEmployee = Mapper.Map<List<EmployeeSAP>>(mDbContext.OHEM.ToList());
                mCombo.ListProject = Mapper.Map<List<ProjectSAP>>(mDbContext.OPRJ.ToList());

                return mCombo;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetRangePlanCombo :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pObject"></param>
		/// <param name="pCompanyParam"></param>
		/// <returns></returns>
        public string UpdateRangePlan(ARGNSRangePlan pObject, CompanyConn pCompanyParam)
        {
            ARGNSRangePlanServiceWeb.ArgnsRangePlan oRangePlan = new ARGNSRangePlanServiceWeb.ArgnsRangePlan();
            ARGNSRangePlanServiceWeb.ArgnsRangePlanParams oRangePlanParams = new ARGNSRangePlanServiceWeb.ArgnsRangePlanParams();
            ARGNSRangePlanServiceWeb.MsgHeader oRangePlanMsgHeader = new ARGNSRangePlanServiceWeb.MsgHeader();

            oRangePlanMsgHeader.ServiceName = ARGNSRangePlanServiceWeb.MsgHeaderServiceName.ARGNS_RANGEP;
            oRangePlanMsgHeader.ServiceNameSpecified = true;
            oRangePlanMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSRangePlanServiceWeb.ARGNS_RANGEP oRangePlanServiceWeb = new ARGNSRangePlanServiceWeb.ARGNS_RANGEP();
            oRangePlanServiceWeb.MsgHeaderValue = oRangePlanMsgHeader;

            try
            {
                oRangePlanParams.Code = pObject.Code;
                oRangePlan = oRangePlanServiceWeb.GetByParams(oRangePlanParams);

                oRangePlan.U_Season = pObject.U_Season;
                oRangePlan.U_RangeDesc = pObject.U_RangeDesc;
                oRangePlan.U_Coll = pObject.U_Coll;
                oRangePlan.U_PrjCode = pObject.U_PrjCode;
                oRangePlan.U_RPEmpl = pObject.U_RPEmpl;
                if (pObject.U_SDate.HasValue && pObject.U_SDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    oRangePlan.U_SDate = pObject.U_SDate.Value;
                    oRangePlan.U_SDateSpecified = true;
                }
                if (pObject.U_DTo.HasValue && pObject.U_DTo.Value.ToString("yyyyMMdd") != "00010101")
                {
                    oRangePlan.U_DTo = pObject.U_DTo.Value;
                    oRangePlan.U_DToSpecified = true;
                }


                //Range Plan Details
                int mCount = 0;
                if (pObject.Lines.Count > 0)
                {
                    List<ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL> mRangeLinesList = new List<ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL>();
                    ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL mRangeLines;

                    foreach (ARGNSRangePlanDetail portalLine in pObject.Lines)
                    {
                        mRangeLines = new ARGNSRangePlanServiceWeb.ArgnsRangePlanARGNS_RANGEPDETAIL();
                        mRangeLines.Code = pObject.Code;
                        mRangeLines.LineId = mCount + 1;
                        mRangeLines.LineIdSpecified = true;
                        mRangeLines.U_ModCode = portalLine.U_ModCode;
                        mRangeLines.U_ModDesc = portalLine.U_ModDesc;
                        mRangeLines.U_ModPic = portalLine.U_ModPic;
                        mRangeLines.U_Comments = portalLine.U_Comments;

                        mRangeLinesList.Add(mRangeLines);
                        mCount++;
                    }
                    oRangePlan.ARGNS_RANGEPDETAILCollection = mRangeLinesList.ToArray();
                }

                oRangePlanServiceWeb.Update(oRangePlan);
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataDS -> UpdateRangePlan :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region Material Detail
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="mMaterialDetailsUDF"></param>
		/// <param name="pModCode"></param>
		/// <returns></returns>
        public ARGNSMaterialDetail GetMaterialDetails(CompanyConn pCompanyParam, MaterialDetailsUDF mMaterialDetailsUDF, string pModCode = "")
        {
            Mapper.CreateMap<ARGNSMaterialDetail, C_ARGNS_MAT_DET>();
            Mapper.CreateMap<C_ARGNS_MAT_DET, ARGNSMaterialDetail>();

            ARGNSMaterialDetail mMaterialDetail = new ARGNSMaterialDetail();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();
            try
            {
                string mWhere = "[@ARGNS_MAT_DET].[U_ModCode] = '" + pModCode + "'";
                C_ARGNS_MAT_DET mMAT_DET = UDFUtil.GetObjectListWithUDF(null, mWhere, typeof(C_ARGNS_MAT_DET), mConnectionString, "@ARGNS_MAT_DET").Cast<C_ARGNS_MAT_DET>().FirstOrDefault();

                if (mMAT_DET != null)
                {
                    mMaterialDetail = Mapper.Map<ARGNSMaterialDetail>(mMAT_DET);

                    //Setting Material Details Lines
                    #region Material Details Lines
                    //Material Detail Fabric
                    Mapper.CreateMap<C_ARGNS_MD_FABRIC, ARGNSMDFabric>();
                    mWhere = "[@ARGNS_MD_FABRIC].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListFabric = Mapper.Map<List<ARGNSMDFabric>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFFabric, mWhere, typeof(C_ARGNS_MD_FABRIC), mConnectionString, "@ARGNS_MD_FABRIC").Cast<C_ARGNS_MD_FABRIC>().ToList());

                    //Material Detail Accessories
                    Mapper.CreateMap<C_ARGNS_MD_ACCESS, ARGNSMDAccess>();
                    mWhere = "[@ARGNS_MD_ACCESS].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListAccessories = Mapper.Map<List<ARGNSMDAccess>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFAccessories, mWhere, typeof(C_ARGNS_MD_ACCESS), mConnectionString, "@ARGNS_MD_ACCESS").Cast<C_ARGNS_MD_ACCESS>().ToList());

                    //Material Detail Care Instructions
                    Mapper.CreateMap<C_ARGNS_MD_CARE_INST, ARGNSMDCareInst>();
                    mWhere = "[@ARGNS_MD_CARE_INST].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListCareInstructions = Mapper.Map<List<ARGNSMDCareInst>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFCareInstructions, mWhere, typeof(C_ARGNS_MD_CARE_INST), mConnectionString, "@ARGNS_MD_CARE_INST").Cast<C_ARGNS_MD_CARE_INST>().ToList());

                    //Material Detail Labelling
                    Mapper.CreateMap<C_ARGNS_MD_LABELLING, ARGNSMDLabelling>();
                    mWhere = "[@ARGNS_MD_LABELLING].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListLabelling = Mapper.Map<List<ARGNSMDLabelling>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFLabelling, mWhere, typeof(C_ARGNS_MD_LABELLING), mConnectionString, "@ARGNS_MD_LABELLING").Cast<C_ARGNS_MD_LABELLING>().ToList());

                    //Material Detail Packaging
                    Mapper.CreateMap<C_ARGNS_MD_PACKAGING, ARGNSMDPackaging>();
                    mWhere = "[@ARGNS_MD_PACKAGING].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListPackaging = Mapper.Map<List<ARGNSMDPackaging>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFPackaging, mWhere, typeof(C_ARGNS_MD_PACKAGING), mConnectionString, "@ARGNS_MD_PACKAGING").Cast<C_ARGNS_MD_PACKAGING>().ToList());

                    //Material Detail Footwear Material
                    Mapper.CreateMap<C_ARGNS_MD_FOOTWEAR, ARGNSMDFootwearMaterial>();
                    mWhere = "[@ARGNS_MD_FOOTWEAR].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListFootwearMaterial = Mapper.Map<List<ARGNSMDFootwearMaterial>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFFootwearMaterial, mWhere, typeof(C_ARGNS_MD_FOOTWEAR), mConnectionString, "@ARGNS_MD_FOOTWEAR").Cast<C_ARGNS_MD_FOOTWEAR>().ToList());

                    //Material Detail Footwear Details
                    Mapper.CreateMap<C_ARGNS_MD_FDETAILS, ARGNSMDFootwearDetails>();
                    mWhere = "[@ARGNS_MD_FDETAILS].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListFootwearDetail = Mapper.Map<List<ARGNSMDFootwearDetails>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFFootwearDetail, mWhere, typeof(C_ARGNS_MD_FDETAILS), mConnectionString, "@ARGNS_MD_FDETAILS").Cast<C_ARGNS_MD_FDETAILS>().ToList());

                    //Material Detail Footwear Packaging
                    Mapper.CreateMap<C_ARGNS_MD_FPACKAGING, ARGNSMDFootwearPackaging>();
                    mWhere = "[@ARGNS_MD_FPACKAGING].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListFootwearPackaging = Mapper.Map<List<ARGNSMDFootwearPackaging>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFFootwearPackaging, mWhere, typeof(C_ARGNS_MD_FPACKAGING), mConnectionString, "@ARGNS_MD_FPACKAGING").Cast<C_ARGNS_MD_FPACKAGING>().ToList());

                    //Material Detail Footwear Pictogram
                    Mapper.CreateMap<C_ARGNS_MD_FPICTOGRAM, ARGNSMDFootwearPictogram>();
                    mWhere = "[@ARGNS_MD_FPICTOGRAM].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListFootwearPictogram = Mapper.Map<List<ARGNSMDFootwearPictogram>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFFootwearPictogram, mWhere, typeof(C_ARGNS_MD_FPICTOGRAM), mConnectionString, "@ARGNS_MD_FPICTOGRAM").Cast<C_ARGNS_MD_FPICTOGRAM>().ToList());

                    //Material Detail Footwear Size Range
                    Mapper.CreateMap<C_ARGNS_MD_FSRANGE, ARGNSMDFootwearSRange>();
                    mWhere = "[@ARGNS_MD_FSRANGE].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListFootwearSizeRange = Mapper.Map<List<ARGNSMDFootwearSRange>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFFootwearSizeRange, mWhere, typeof(C_ARGNS_MD_FSRANGE), mConnectionString, "@ARGNS_MD_FSRANGE").Cast<C_ARGNS_MD_FSRANGE>().ToList());

                    //Material Detail Footwear Accessories
                    Mapper.CreateMap<C_ARGNS_MD_FACCESS, ARGNSMDFootwearAccess>();
                    mWhere = "[@ARGNS_MD_FACCESS].[Code] = " + mMAT_DET.Code;
                    mMaterialDetail.ListFootwearAccessories = Mapper.Map<List<ARGNSMDFootwearAccess>>(UDFUtil.GetObjectListWithUDF(mMaterialDetailsUDF.ListUDFFootwearAccessories, mWhere, typeof(C_ARGNS_MD_FACCESS), mConnectionString, "@ARGNS_MD_FACCESS").Cast<C_ARGNS_MD_FACCESS>().ToList());
                    #endregion

                    //Setteo la imagen del modelo 
                    C_ARGNS_MODEL mModel = mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pModCode).FirstOrDefault();
                    if (mModel != null)
                        mMaterialDetail.U_Pic = mModel.U_Pic;
                }
                else
                {
                    mMaterialDetail = new ARGNSMaterialDetail();
                    //Setteo la imagen del modelo 
                    C_ARGNS_MODEL mModel = mDbContext.C_ARGNS_MODEL.Where(c => c.U_ModCode == pModCode).FirstOrDefault();
                    if (mModel != null)
                        mMaterialDetail.U_Pic = mModel.U_Pic;
                }

                mDbContext.Dispose();

                return mMaterialDetail;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetMaterialDetails :" + ex.Message);
                return null;
            }
        }

        public string UpdateMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail)
        {
            ARGNSMaterialDetailServiceWeb.ArgnsMatDet oMatDet = new ARGNSMaterialDetailServiceWeb.ArgnsMatDet();
            ARGNSMaterialDetailServiceWeb.ArgnsMatDetParams oMatDetParams = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetParams();
            ARGNSMaterialDetailServiceWeb.MsgHeader oMatDetParamsMsgHeader = new ARGNSMaterialDetailServiceWeb.MsgHeader();

            oMatDetParamsMsgHeader.ServiceName = ARGNSMaterialDetailServiceWeb.MsgHeaderServiceName.ARGNS_MAT_DET;
            oMatDetParamsMsgHeader.ServiceNameSpecified = true;
            oMatDetParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET oMatDetService = new ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET();
            oMatDetService.MsgHeaderValue = oMatDetParamsMsgHeader;

            try
            {
                oMatDetParams.Code = pMaterialDetail.Code;
                oMatDet = oMatDetService.GetByParams(oMatDetParams);

                oMatDet.U_Description = pMaterialDetail.U_Description;

                #region Fabric

                if (pMaterialDetail.ListFabric.Count > 0 || oMatDet.ARGNS_MD_FABRICCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mFabricSAP in oMatDet.ARGNS_MD_FABRICCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFabric mFabric = pMaterialDetail.ListFabric.Where(c => c.LineId == mFabricSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mFabric != null)
                        {
                            mFabricSAP.U_ColCode = mFabric.U_ColCode;
                            mFabricSAP.U_CompCode = mFabric.U_CompCode;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FABRICCollection = oMatDet.ARGNS_MD_FABRICCollection.Where(c => c.LineId != mFabricSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[oMatDet.ARGNS_MD_FABRICCollection.Length + pMaterialDetail.ListFabric.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FABRICCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FABRICCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFabric mFabric in pMaterialDetail.ListFabric.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC lineFabric = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC();
                        lineFabric.LineId = mFabric.LineId;
                        lineFabric.LineIdSpecified = true;
                        lineFabric.U_ColCode = mFabric.U_ColCode;
                        lineFabric.U_CompCode = mFabric.U_CompCode;
                        newLines[positionNewLine] = lineFabric;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FABRICCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[newLines.Length];
                    oMatDet.ARGNS_MD_FABRICCollection = newLines;
                }

                #endregion

                #region Accessories

                if (pMaterialDetail.ListAccessories.Count > 0 || oMatDet.ARGNS_MD_ACCESSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mAccessoriesSAP in oMatDet.ARGNS_MD_ACCESSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDAccess mAccessories = pMaterialDetail.ListAccessories.Where(c => c.LineId == mAccessoriesSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mAccessories != null)
                        {
                            mAccessoriesSAP.U_Buttons = mAccessories.U_Buttons;
                            mAccessoriesSAP.U_Trims = mAccessories.U_Trims;
                            mAccessoriesSAP.U_Zippers = mAccessories.U_Zippers;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_ACCESSCollection = oMatDet.ARGNS_MD_ACCESSCollection.Where(c => c.LineId != mAccessoriesSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[oMatDet.ARGNS_MD_ACCESSCollection.Length + pMaterialDetail.ListAccessories.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_ACCESSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_ACCESSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDAccess mAccessory in pMaterialDetail.ListAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS lineAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS();
                        lineAccessories.LineId = mAccessory.LineId;
                        lineAccessories.LineIdSpecified = true;
                        lineAccessories.U_Buttons = mAccessory.U_Buttons;
                        lineAccessories.U_Trims = mAccessory.U_Trims;
                        newLines[positionNewLine] = lineAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_ACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_ACCESSCollection = newLines;
                }

                #endregion

                #region Care Instruction

                if (pMaterialDetail.ListCareInstructions.Count > 0 || oMatDet.ARGNS_MD_CARE_INSTCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mCISAP in oMatDet.ARGNS_MD_CARE_INSTCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDCareInst mCI = pMaterialDetail.ListCareInstructions.Where(c => c.LineId == mCISAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mCI != null)
                        {
                            mCISAP.U_ColCode = mCI.U_ColCode;
                            mCISAP.U_CareText = mCI.U_CareText;
                            mCISAP.U_CareCode = mCI.U_CareCode;
                            mCISAP.U_CareLbl = mCI.U_CareLbl;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_CARE_INSTCollection = oMatDet.ARGNS_MD_CARE_INSTCollection.Where(c => c.LineId != mCISAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[oMatDet.ARGNS_MD_CARE_INSTCollection.Length + pMaterialDetail.ListCareInstructions.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_CARE_INSTCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_CARE_INSTCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDCareInst mCI in pMaterialDetail.ListCareInstructions.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST lineCI = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST();
                        lineCI.LineId = mCI.LineId;
                        lineCI.LineIdSpecified = true;
                        lineCI.U_ColCode = mCI.U_ColCode;
                        lineCI.U_CareText = mCI.U_CareText;
                        lineCI.U_CareCode = mCI.U_CareCode;
                        lineCI.U_CareLbl = mCI.U_CareLbl;
                        newLines[positionNewLine] = lineCI;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_CARE_INSTCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[newLines.Length];
                    oMatDet.ARGNS_MD_CARE_INSTCollection = newLines;
                }

                #endregion

                #region Labelling

                if (pMaterialDetail.ListLabelling.Count > 0 || oMatDet.ARGNS_MD_LABELLINGCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mLabellingSAP in oMatDet.ARGNS_MD_LABELLINGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDLabelling mCI = pMaterialDetail.ListLabelling.Where(c => c.LineId == mLabellingSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mCI != null)
                        {
                            mLabellingSAP.U_LImage = mCI.U_LImage;
                            mLabellingSAP.U_LSize = mCI.U_LSize;
                            mLabellingSAP.U_MLabel = mCI.U_MLabel;
                            mLabellingSAP.U_Position = mCI.U_Position;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_LABELLINGCollection = oMatDet.ARGNS_MD_LABELLINGCollection.Where(c => c.LineId != mLabellingSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[oMatDet.ARGNS_MD_LABELLINGCollection.Length + pMaterialDetail.ListLabelling.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_LABELLINGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_LABELLINGCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDLabelling mLabelling in pMaterialDetail.ListLabelling.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING lineLabelling = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING();
                        lineLabelling.LineId = mLabelling.LineId;
                        lineLabelling.LineIdSpecified = true;
                        lineLabelling.U_LImage = mLabelling.U_LImage;
                        lineLabelling.U_LSize = mLabelling.U_LSize;
                        lineLabelling.U_MLabel = mLabelling.U_MLabel;
                        lineLabelling.U_Position = mLabelling.U_Position;
                        newLines[positionNewLine] = lineLabelling;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_LABELLINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[newLines.Length];
                    oMatDet.ARGNS_MD_LABELLINGCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListPackaging.Count > 0 || oMatDet.ARGNS_MD_PACKAGINGCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mPackagingSAP in oMatDet.ARGNS_MD_PACKAGINGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDPackaging mPackaging = pMaterialDetail.ListPackaging.Where(c => c.LineId == mPackagingSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPackaging != null)
                        {
                            mPackagingSAP.U_Hangtag = mPackaging.U_Hangtag;
                            mPackagingSAP.U_Others = mPackaging.U_Others;
                            mPackagingSAP.U_OutPack = mPackaging.U_OutPack;
                            mPackagingSAP.U_Polybag = mPackaging.U_Polybag;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_PACKAGINGCollection = oMatDet.ARGNS_MD_PACKAGINGCollection.Where(c => c.LineId != mPackagingSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[oMatDet.ARGNS_MD_PACKAGINGCollection.Length + pMaterialDetail.ListPackaging.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_PACKAGINGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_PACKAGINGCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDPackaging mPackaging in pMaterialDetail.ListPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_Polybag = mPackaging.U_Polybag;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_PACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_PACKAGINGCollection = newLines;
                }

                #endregion

                #region Material

                if (pMaterialDetail.ListFootwearMaterial.Count > 0 || oMatDet.ARGNS_MD_FOOTWEARCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mMaterialSAP in oMatDet.ARGNS_MD_FOOTWEARCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearMaterial mMaterial = pMaterialDetail.ListFootwearMaterial.Where(c => c.LineId == mMaterialSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mMaterial != null)
                        {
                            mMaterialSAP.U_Heel = mMaterial.U_Heel;
                            mMaterialSAP.U_Insole = mMaterial.U_Insole;
                            mMaterialSAP.U_Lining = mMaterial.U_Lining;
                            mMaterialSAP.U_Outsole = mMaterial.U_Outsole;
                            mMaterialSAP.U_Sfabric = mMaterial.U_Sfabric;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FOOTWEARCollection = oMatDet.ARGNS_MD_FOOTWEARCollection.Where(c => c.LineId != mMaterialSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[oMatDet.ARGNS_MD_FOOTWEARCollection.Length + pMaterialDetail.ListFootwearMaterial.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FOOTWEARCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FOOTWEARCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearMaterial mMaterial in pMaterialDetail.ListFootwearMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR lineMaterial = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_Heel = mMaterial.U_Heel;
                        lineMaterial.U_Insole = mMaterial.U_Insole;
                        lineMaterial.U_Lining = mMaterial.U_Lining;
                        lineMaterial.U_Outsole = mMaterial.U_Outsole;
                        lineMaterial.U_Sfabric = mMaterial.U_Sfabric;
                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FOOTWEARCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[newLines.Length];
                    oMatDet.ARGNS_MD_FOOTWEARCollection = newLines;
                }

                #endregion

                #region Detail

                if (pMaterialDetail.ListFootwearDetail.Count > 0 || oMatDet.ARGNS_MD_FDETAILSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mDetailSAP in oMatDet.ARGNS_MD_FDETAILSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearDetails mDetail = pMaterialDetail.ListFootwearDetail.Where(c => c.LineId == mDetailSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mDetail != null)
                        {
                            mDetailSAP.U_BHeight = mDetail.U_BHeight;
                            mDetailSAP.U_BWidth = mDetail.U_BWidth;
                            mDetailSAP.U_HHeight = mDetail.U_HHeight;
                            mDetailSAP.U_Others = mDetail.U_Others;
                            mDetailSAP.U_Seams = mDetail.U_Seams;
                            mDetailSAP.U_Strap = mDetail.U_Strap;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FDETAILSCollection = oMatDet.ARGNS_MD_FDETAILSCollection.Where(c => c.LineId != mDetailSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[oMatDet.ARGNS_MD_FDETAILSCollection.Length + pMaterialDetail.ListFootwearDetail.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FDETAILSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FDETAILSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearDetails mDetail in pMaterialDetail.ListFootwearDetail.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS lineDetail = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS();
                        lineDetail.LineId = mDetail.LineId;
                        lineDetail.LineIdSpecified = true;
                        lineDetail.U_BHeight = mDetail.U_BHeight;
                        lineDetail.U_BWidth = mDetail.U_BWidth;
                        lineDetail.U_HHeight = mDetail.U_HHeight;
                        lineDetail.U_Others = mDetail.U_Others;
                        lineDetail.U_Seams = mDetail.U_Seams;
                        lineDetail.U_Strap = mDetail.U_Strap;
                        newLines[positionNewLine] = lineDetail;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FDETAILSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[newLines.Length];
                    oMatDet.ARGNS_MD_FDETAILSCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListFootwearPackaging.Count > 0 || oMatDet.ARGNS_MD_FPACKAGINGCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mPackagingSAP in oMatDet.ARGNS_MD_FPACKAGINGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearPackaging mPackaging = pMaterialDetail.ListFootwearPackaging.Where(c => c.LineId == mPackagingSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPackaging != null)
                        {
                            mPackagingSAP.U_Hangtag = mPackaging.U_Hangtag;
                            mPackagingSAP.U_Others = mPackaging.U_Others;
                            mPackagingSAP.U_OutPack = mPackaging.U_OutPack;
                            mPackagingSAP.U_PolybagSB = mPackaging.U_PolybagSB;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FPACKAGINGCollection = oMatDet.ARGNS_MD_FPACKAGINGCollection.Where(c => c.LineId != mPackagingSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[oMatDet.ARGNS_MD_FPACKAGINGCollection.Length + pMaterialDetail.ListFootwearPackaging.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FPACKAGINGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FPACKAGINGCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPackaging mPackaging in pMaterialDetail.ListFootwearPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_PolybagSB = mPackaging.U_PolybagSB;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = newLines;
                }

                #endregion

                #region Pictogram

                if (pMaterialDetail.ListFootwearPictogram.Count > 0 || oMatDet.ARGNS_MD_FPICTOGRAMCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mPictogramSAP in oMatDet.ARGNS_MD_FPICTOGRAMCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearPictogram mPictogram = pMaterialDetail.ListFootwearPictogram.Where(c => c.LineId == mPictogramSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPictogram != null)
                        {
                            mPictogramSAP.U_Pictogram = mPictogram.U_Pictogram;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FPICTOGRAMCollection = oMatDet.ARGNS_MD_FPICTOGRAMCollection.Where(c => c.LineId != mPictogramSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[oMatDet.ARGNS_MD_FPICTOGRAMCollection.Length + pMaterialDetail.ListFootwearPictogram.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FPICTOGRAMCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPictogram mPictogram in pMaterialDetail.ListFootwearPictogram.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM linePictogram = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM();
                        linePictogram.LineId = mPictogram.LineId;
                        linePictogram.LineIdSpecified = true;
                        linePictogram.U_Pictogram = mPictogram.U_Pictogram;
                        newLines[positionNewLine] = linePictogram;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[newLines.Length];
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = newLines;
                }

                #endregion

                #region Size Range

                if (pMaterialDetail.ListFootwearSizeRange.Count > 0 || oMatDet.ARGNS_MD_FSRANGECollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mSRangeSAP in oMatDet.ARGNS_MD_FSRANGECollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearSRange mSRange = pMaterialDetail.ListFootwearSizeRange.Where(c => c.LineId == mSRangeSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mSRange != null)
                        {
                            mSRangeSAP.U_SRange = mSRange.U_SRange;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FSRANGECollection = oMatDet.ARGNS_MD_FSRANGECollection.Where(c => c.LineId != mSRangeSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[oMatDet.ARGNS_MD_FSRANGECollection.Length + pMaterialDetail.ListFootwearSizeRange.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FSRANGECollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FSRANGECollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearSRange mSRange in pMaterialDetail.ListFootwearSizeRange.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE lineSRange = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE();
                        lineSRange.LineId = mSRange.LineId;
                        lineSRange.LineIdSpecified = true;
                        lineSRange.U_SRange = mSRange.U_SRange;
                        newLines[positionNewLine] = lineSRange;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FSRANGECollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[newLines.Length];
                    oMatDet.ARGNS_MD_FSRANGECollection = newLines;
                }

                #endregion

                #region Footware Accessories

                if (pMaterialDetail.ListFootwearAccessories.Count > 0 || oMatDet.ARGNS_MD_FACCESSCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el MaterialDetail
                    foreach (var mFAccessoriesSAP in oMatDet.ARGNS_MD_FACCESSCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSMDFootwearAccess mFAccessories = pMaterialDetail.ListFootwearAccessories.Where(c => c.LineId == mFAccessoriesSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mFAccessories != null)
                        {
                            mFAccessoriesSAP.U_Buckles = mFAccessories.U_Buckles;
                            mFAccessoriesSAP.U_Enclosures = mFAccessories.U_Enclosures;
                            mFAccessoriesSAP.U_Eyelets = mFAccessories.U_Eyelets;
                            mFAccessoriesSAP.U_Labelling = mFAccessories.U_Labelling;
                            mFAccessoriesSAP.U_Others = mFAccessories.U_Others;
                            mFAccessoriesSAP.U_SLaces = mFAccessories.U_SLaces;
                        }
                        //Sino la borro
                        else
                        {
                            oMatDet.ARGNS_MD_FACCESSCollection = oMatDet.ARGNS_MD_FACCESSCollection.Where(c => c.LineId != mFAccessoriesSAP.LineId).ToArray();
                        }
                    }

                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[oMatDet.ARGNS_MD_FACCESSCollection.Length + pMaterialDetail.ListFootwearAccessories.Where(c => c.IsNew == true).ToList().Count];
                    oMatDet.ARGNS_MD_FACCESSCollection.CopyTo(newLines, 0);
                    int positionNewLine = oMatDet.ARGNS_MD_FACCESSCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearAccess mFAccessories in pMaterialDetail.ListFootwearAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS lineFAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS();
                        lineFAccessories.LineId = mFAccessories.LineId;
                        lineFAccessories.LineIdSpecified = true;
                        lineFAccessories.U_Buckles = mFAccessories.U_Buckles;
                        lineFAccessories.U_Enclosures = mFAccessories.U_Enclosures;
                        lineFAccessories.U_Eyelets = mFAccessories.U_Eyelets;
                        lineFAccessories.U_Labelling = mFAccessories.U_Labelling;
                        lineFAccessories.U_Others = mFAccessories.U_Others;
                        lineFAccessories.U_SLaces = mFAccessories.U_SLaces;
                        newLines[positionNewLine] = lineFAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_FACCESSCollection = newLines;
                }

                #endregion


                XmlDocument mXmlDoc = GetSoapStructure(true, oMatDetParamsMsgHeader, oMatDet);

                #region AddUDFToXML

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FABRIC']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFabric mAuxLine = pMaterialDetail.ListFabric.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_ACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDAccess mAuxLine = pMaterialDetail.ListAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_CARE_INST']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDCareInst mAuxLine = pMaterialDetail.ListCareInstructions.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_LABELLING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDLabelling mAuxLine = pMaterialDetail.ListLabelling.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_PACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDPackaging mAuxLine = pMaterialDetail.ListPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearAccess mAuxLine = pMaterialDetail.ListFootwearAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FDETAILS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearDetails mAuxLine = pMaterialDetail.ListFootwearDetail.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FOOTWEAR']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearMaterial mAuxLine = pMaterialDetail.ListFootwearMaterial.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPackaging mAuxLine = pMaterialDetail.ListFootwearPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPICTOGRAM']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPictogram mAuxLine = pMaterialDetail.ListFootwearPictogram.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FSRANGE']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearSRange mAuxLine = pMaterialDetail.ListFootwearSizeRange.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }

                #endregion

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateMaterialDetail :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string AddMaterialDetail(CompanyConn pCompanyParam, ARGNSMaterialDetail pMaterialDetail)
        {
            ARGNSMaterialDetailServiceWeb.ArgnsMatDet oMatDet = new ARGNSMaterialDetailServiceWeb.ArgnsMatDet();
            ARGNSMaterialDetailServiceWeb.MsgHeader oMatDetParamsMsgHeader = new ARGNSMaterialDetailServiceWeb.MsgHeader();

            oMatDetParamsMsgHeader.ServiceName = ARGNSMaterialDetailServiceWeb.MsgHeaderServiceName.ARGNS_MAT_DET;
            oMatDetParamsMsgHeader.ServiceNameSpecified = true;
            oMatDetParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET oMatDetService = new ARGNSMaterialDetailServiceWeb.ARGNS_MAT_DET();
            oMatDetService.MsgHeaderValue = oMatDetParamsMsgHeader;
            try
            {
                oMatDet = new ARGNSMaterialDetailServiceWeb.ArgnsMatDet();

                oMatDet.Code = GetLastMaterialDetailCode(pCompanyParam);
                oMatDet.U_ModCode = pMaterialDetail.U_ModCode;
                oMatDet.U_Description = pMaterialDetail.U_Description;

                #region Fabric

                if (pMaterialDetail.ListFabric.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[pMaterialDetail.ListFabric.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFabric mFabric in pMaterialDetail.ListFabric.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC lineFabric = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC();
                        lineFabric.LineId = mFabric.LineId;
                        lineFabric.LineIdSpecified = true;
                        lineFabric.U_ColCode = mFabric.U_ColCode;
                        lineFabric.U_CompCode = mFabric.U_CompCode;
                        newLines[positionNewLine] = lineFabric;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FABRICCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FABRIC[newLines.Length];
                    oMatDet.ARGNS_MD_FABRICCollection = newLines;
                }

                #endregion

                #region Accessories

                if (pMaterialDetail.ListAccessories.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[pMaterialDetail.ListAccessories.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDAccess mAccesory in pMaterialDetail.ListAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS lineAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS();
                        lineAccessories.LineId = mAccesory.LineId;
                        lineAccessories.LineIdSpecified = true;
                        lineAccessories.U_Buttons = mAccesory.U_Buttons;
                        lineAccessories.U_Trims = mAccesory.U_Trims;
                        newLines[positionNewLine] = lineAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_ACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_ACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_ACCESSCollection = newLines;
                }

                #endregion

                #region Care Instruction

                if (pMaterialDetail.ListCareInstructions.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[pMaterialDetail.ListCareInstructions.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDCareInst mCI in pMaterialDetail.ListCareInstructions.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST lineCI = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST();
                        lineCI.LineId = mCI.LineId;
                        lineCI.LineIdSpecified = true;
                        lineCI.U_ColCode = mCI.U_ColCode;
                        lineCI.U_CareText = mCI.U_CareText;
                        lineCI.U_CareCode = mCI.U_CareCode;
                        lineCI.U_CareLbl = mCI.U_CareLbl;
                        newLines[positionNewLine] = lineCI;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_CARE_INSTCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_CARE_INST[newLines.Length];
                    oMatDet.ARGNS_MD_CARE_INSTCollection = newLines;
                }

                #endregion

                #region Labelling

                if (pMaterialDetail.ListLabelling.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[pMaterialDetail.ListLabelling.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDLabelling mLabelling in pMaterialDetail.ListLabelling.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING lineLabelling = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING();
                        lineLabelling.LineId = mLabelling.LineId;
                        lineLabelling.LineIdSpecified = true;
                        lineLabelling.U_LImage = mLabelling.U_LImage;
                        lineLabelling.U_LSize = mLabelling.U_LSize;
                        lineLabelling.U_MLabel = mLabelling.U_MLabel;
                        lineLabelling.U_Position = mLabelling.U_Position;
                        newLines[positionNewLine] = lineLabelling;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_LABELLINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_LABELLING[newLines.Length];
                    oMatDet.ARGNS_MD_LABELLINGCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListPackaging.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[pMaterialDetail.ListPackaging.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDPackaging mPackaging in pMaterialDetail.ListPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_Polybag = mPackaging.U_Polybag;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_PACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_PACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_PACKAGINGCollection = newLines;
                }

                #endregion

                #region Material

                if (pMaterialDetail.ListFootwearMaterial.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[pMaterialDetail.ListFootwearMaterial.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearMaterial mMaterial in pMaterialDetail.ListFootwearMaterial.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR lineMaterial = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR();
                        lineMaterial.LineId = mMaterial.LineId;
                        lineMaterial.LineIdSpecified = true;
                        lineMaterial.U_Heel = mMaterial.U_Heel;
                        lineMaterial.U_Insole = mMaterial.U_Insole;
                        lineMaterial.U_Lining = mMaterial.U_Lining;
                        lineMaterial.U_Outsole = mMaterial.U_Outsole;
                        lineMaterial.U_Sfabric = mMaterial.U_Sfabric;
                        newLines[positionNewLine] = lineMaterial;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FOOTWEARCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FOOTWEAR[newLines.Length];
                    oMatDet.ARGNS_MD_FOOTWEARCollection = newLines;
                }

                #endregion

                #region Detail

                if (pMaterialDetail.ListFootwearDetail.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[pMaterialDetail.ListFootwearDetail.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearDetails mDetail in pMaterialDetail.ListFootwearDetail.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS lineDetail = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS();
                        lineDetail.LineId = mDetail.LineId;
                        lineDetail.LineIdSpecified = true;
                        lineDetail.U_BHeight = mDetail.U_BHeight;
                        lineDetail.U_BWidth = mDetail.U_BWidth;
                        lineDetail.U_HHeight = mDetail.U_HHeight;
                        lineDetail.U_Others = mDetail.U_Others;
                        lineDetail.U_Seams = mDetail.U_Seams;
                        lineDetail.U_Strap = mDetail.U_Strap;
                        newLines[positionNewLine] = lineDetail;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FDETAILSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FDETAILS[newLines.Length];
                    oMatDet.ARGNS_MD_FDETAILSCollection = newLines;
                }

                #endregion

                #region Packaging

                if (pMaterialDetail.ListFootwearPackaging.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[pMaterialDetail.ListFootwearPackaging.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPackaging mPackaging in pMaterialDetail.ListFootwearPackaging.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING linePackaging = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING();
                        linePackaging.LineId = mPackaging.LineId;
                        linePackaging.LineIdSpecified = true;
                        linePackaging.U_Hangtag = mPackaging.U_Hangtag;
                        linePackaging.U_Others = mPackaging.U_Others;
                        linePackaging.U_OutPack = mPackaging.U_OutPack;
                        linePackaging.U_PolybagSB = mPackaging.U_PolybagSB;
                        newLines[positionNewLine] = linePackaging;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPACKAGING[newLines.Length];
                    oMatDet.ARGNS_MD_FPACKAGINGCollection = newLines;
                }

                #endregion

                #region Pictogram

                if (pMaterialDetail.ListFootwearPictogram.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[pMaterialDetail.ListFootwearPictogram.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearPictogram mPictogram in pMaterialDetail.ListFootwearPictogram.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM linePictogram = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM();
                        linePictogram.LineId = mPictogram.LineId;
                        linePictogram.LineIdSpecified = true;
                        linePictogram.U_Pictogram = mPictogram.U_Pictogram;
                        newLines[positionNewLine] = linePictogram;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FPICTOGRAM[newLines.Length];
                    oMatDet.ARGNS_MD_FPICTOGRAMCollection = newLines;
                }

                #endregion

                #region Size Range

                if (pMaterialDetail.ListFootwearSizeRange.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[pMaterialDetail.ListFootwearSizeRange.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearSRange mSRange in pMaterialDetail.ListFootwearSizeRange.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE lineSRange = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE();
                        lineSRange.LineId = mSRange.LineId;
                        lineSRange.LineIdSpecified = true;
                        lineSRange.U_SRange = mSRange.U_SRange;
                        newLines[positionNewLine] = lineSRange;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FSRANGECollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FSRANGE[newLines.Length];
                    oMatDet.ARGNS_MD_FSRANGECollection = newLines;
                }

                #endregion

                #region Footware Accessories

                if (pMaterialDetail.ListFootwearAccessories.Count > 0)
                {
                    ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[] newLines = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[pMaterialDetail.ListFootwearAccessories.Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSMDFootwearAccess mFAccessories in pMaterialDetail.ListFootwearAccessories.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS lineFAccessories = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS();
                        lineFAccessories.LineId = mFAccessories.LineId;
                        lineFAccessories.LineIdSpecified = true;
                        lineFAccessories.U_Buckles = mFAccessories.U_Buckles;
                        lineFAccessories.U_Enclosures = mFAccessories.U_Enclosures;
                        lineFAccessories.U_Eyelets = mFAccessories.U_Eyelets;
                        lineFAccessories.U_Labelling = mFAccessories.U_Labelling;
                        lineFAccessories.U_Others = mFAccessories.U_Others;
                        lineFAccessories.U_SLaces = mFAccessories.U_SLaces;
                        newLines[positionNewLine] = lineFAccessories;
                        positionNewLine += 1;
                    }
                    oMatDet.ARGNS_MD_FACCESSCollection = new ARGNSMaterialDetailServiceWeb.ArgnsMatDetARGNS_MD_FACCESS[newLines.Length];
                    oMatDet.ARGNS_MD_FACCESSCollection = newLines;
                }

                #endregion

                XmlDocument mXmlDoc = GetSoapStructure(false, oMatDetParamsMsgHeader, oMatDet);

                #region AddUDFToXML

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FABRIC']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFabric mAuxLine = pMaterialDetail.ListFabric.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_ACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDAccess mAuxLine = pMaterialDetail.ListAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_CARE_INST']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDCareInst mAuxLine = pMaterialDetail.ListCareInstructions.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_LABELLING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDLabelling mAuxLine = pMaterialDetail.ListLabelling.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_PACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDPackaging mAuxLine = pMaterialDetail.ListPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FACCESS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearAccess mAuxLine = pMaterialDetail.ListFootwearAccessories.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FDETAILS']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearDetails mAuxLine = pMaterialDetail.ListFootwearDetail.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FOOTWEAR']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearMaterial mAuxLine = pMaterialDetail.ListFootwearMaterial.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPACKAGING']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPackaging mAuxLine = pMaterialDetail.ListFootwearPackaging.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FPICTOGRAM']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearPictogram mAuxLine = pMaterialDetail.ListFootwearPictogram.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }
                mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='ARGNS_MD_FSRANGE']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    ARGNSMDFootwearSRange mAuxLine = pMaterialDetail.ListFootwearSizeRange.Where(c => c.LineId.ToString() == mAuxNode["LineId"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }
                    if (mAuxLine.IsNew == true)
                    {
                        int firstIndex = mAuxNode.InnerXml.IndexOf("<LineId");
                        int lastIndex = mAuxNode.InnerXml.LastIndexOf("</LineId>");
                        //Remuevo el Nodo LineId puesto que es una linea nueva y SAP le asignara el numero
                        mAuxNode.InnerXml = mAuxNode.InnerXml.Replace(mAuxNode.InnerXml.Substring(firstIndex, (lastIndex - firstIndex + 9)), "");
                    }

                }

                #endregion

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> AddMaterialDetail :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string GetLastMaterialDetailCode(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                return (mDbContext.C_ARGNS_MAT_DET.Max(c => c.DocEntry) + 1).ToString();
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetLastMaterialDetailCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        #endregion

        #region Shipment

        public ARGNSContainer GetContainerById(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<ARGNSContainer, C_ARGNS_CNTM>();
            Mapper.CreateMap<C_ARGNS_CNTM, ARGNSContainer>();
            Mapper.CreateMap<ARGNSContainerLine, C_ARGNS_CNTM_LINE>();
            Mapper.CreateMap<C_ARGNS_CNTM_LINE, ARGNSContainerLine>();
            Mapper.CreateMap<ARGNSContainerPort, C_ARGNS_CNTM_PORT>();
            Mapper.CreateMap<C_ARGNS_CNTM_PORT, ARGNSContainerPort>();
            Mapper.CreateMap<ARGNSContainerStatus, C_ARGNS_CNTM_STATUS>();
            Mapper.CreateMap<C_ARGNS_CNTM_STATUS, ARGNSContainerStatus>();
            Mapper.CreateMap<ARGNSContainerPackage, C_ARGNS_CNTM_OCPK>();
            Mapper.CreateMap<C_ARGNS_CNTM_OCPK, ARGNSContainerPackage>();
            Mapper.CreateMap<ARGNSContainerPackageLine, C_ARGNS_CNTM_CPK1>();
            Mapper.CreateMap<C_ARGNS_CNTM_CPK1, ARGNSContainerPackageLine>();
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();
            Mapper.CreateMap<POR1, DraftLine>();
            Mapper.CreateMap<DraftLine, POR1>();
            Mapper.CreateMap<RDR1, DraftLine>();
            Mapper.CreateMap<DraftLine, RDR1>();

            ARGNSContainer mARGNSContainer = new ARGNSContainer();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mARGNSContainer = Mapper.Map<ARGNSContainer>(mDbContext.C_ARGNS_CNTM.Where(c => c.DocEntry == pDocEntry).FirstOrDefault());

                if (mARGNSContainer != null)
                {
                    //Setting Material Details Lines
                    mARGNSContainer.Lines = Mapper.Map<List<ARGNSContainerLine>>(mDbContext.C_ARGNS_CNTM_LINE.Where(c => c.DocEntry == pDocEntry).ToList());
                    foreach (ARGNSContainerLine mContainerLine in mARGNSContainer.Lines)
                    {
                        if (mContainerLine.U_ObjType == "17")
                            mContainerLine.DraftLine = Mapper.Map<DraftLine>(mDbContext.RDR1.Where(c => c.DocEntry.ToString() == mContainerLine.U_BaseEntry && c.LineNum == mContainerLine.U_BaseLine).FirstOrDefault());
                        if (mContainerLine.U_ObjType == "22")
                            mContainerLine.DraftLine = Mapper.Map<DraftLine>(mDbContext.POR1.Where(c => c.DocEntry.ToString() == mContainerLine.U_BaseEntry && c.LineNum == mContainerLine.U_BaseLine).FirstOrDefault());
                    }
                    mARGNSContainer.ListPackage = Mapper.Map<List<ARGNSContainerPackage>>(mDbContext.C_ARGNS_CNTM_OCPK.Where(c => c.DocEntry == pDocEntry).ToList());
                    foreach (ARGNSContainerPackage mPackage in mARGNSContainer.ListPackage)
                    {
                        mPackage.Lines = Mapper.Map<List<ARGNSContainerPackageLine>>(mDbContext.C_ARGNS_CNTM_CPK1.Where(c => c.U_CntnerNum == mPackage.U_CntnerNum && c.DocEntry == pDocEntry).ToList());
                        foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines)
                        {
                            ARGNSContainerLine mContainerLineAux = mARGNSContainer.Lines.Where(c => c.U_BaseEntry == mPackageLine.U_BaseEntry && c.U_BaseLine.ToString() == mPackageLine.U_BaseLine).FirstOrDefault();
                            mPackageLine.U_ARGNS_MOD = mContainerLineAux.U_ARGNS_MOD;
                            mPackageLine.U_ARGNS_COL = mContainerLineAux.U_ARGNS_COL;
                            mPackageLine.U_ARGNS_SIZE = mContainerLineAux.U_ARGNS_SIZE;
                            mPackageLine.U_ItemName = mContainerLineAux.U_ItemName;
                        }
                    }
                }
                else
                {
                    mARGNSContainer = new ARGNSContainer();
                    mARGNSContainer.Lines = new List<ARGNSContainerLine>();
                }

                mARGNSContainer.ListStatus = Mapper.Map<List<ARGNSContainerStatus>>(mDbContext.C_ARGNS_CNTM_STATUS.ToList());
                mARGNSContainer.ListPort = Mapper.Map<List<ARGNSContainerPort>>(mDbContext.C_ARGNS_CNTM_PORT.ToList());
                mARGNSContainer.ListFreight = Mapper.Map<List<Freight>>(mDbContext.OEXD.ToList());

                mDbContext.Dispose();

                return mARGNSContainer;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetContainerById :" + ex.Message);
                return null;
            }

        }

        public List<ARGNSContainer> GetContainerList(CompanyConn pCompanyParam, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            Mapper.CreateMap<ARGNSContainer, C_ARGNS_CNTM>();
            Mapper.CreateMap<C_ARGNS_CNTM, ARGNSContainer>();

            List<ARGNSContainer> mARGNSContainerList = new List<ARGNSContainer>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mARGNSContainerList = Mapper.Map<List<ARGNSContainer>>(mDbContext.C_ARGNS_CNTM.Where(c => (txtShipmentNum != null && txtShipmentNum != "" ? c.U_Shipment.Contains(txtShipmentNum) : true) && (txtStatus != null && txtStatus != "" ? c.U_Status == txtStatus : true) && (txtPOE != null && txtPOE != "" ? c.U_POE == txtPOE : true) && (dateASD != null ? c.U_ASD == dateASD : true) && (dateADA != null ? c.U_ADA == dateADA : true)).ToList());

                mDbContext.Dispose();

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetContainerList :" + ex.Message);
                return null;
            }

        }

        public List<ARGNSContainer> GetMyShipmentListSearchBP(CompanyConn pCompanyParam, string pBP, string txtShipmentNum, string txtStatus, string txtPOE, DateTime? dateASD, DateTime? dateADA)
        {
            Mapper.CreateMap<ARGNSContainer, C_ARGNS_CNTM>();
            Mapper.CreateMap<C_ARGNS_CNTM, ARGNSContainer>();

            List<ARGNSContainer> mARGNSContainerList = new List<ARGNSContainer>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                List<int> mListShipmentDocEntryList = mDbContext.C_ARGNS_CNTM_LINE.Where(c => c.U_CardCode == pBP).Select(c => c.DocEntry).Distinct().ToList();
                mARGNSContainerList = Mapper.Map<List<ARGNSContainer>>(mDbContext.C_ARGNS_CNTM.Where(c => (mListShipmentDocEntryList.Contains(c.DocEntry)) && (txtShipmentNum != null && txtShipmentNum != "" ? c.U_Shipment.Contains(txtShipmentNum) : true) && (txtStatus != null && txtStatus != "" ? c.U_Status == txtStatus : true) && (txtPOE != null && txtPOE != "" ? c.U_POE == txtPOE : true) && (dateASD != null ? c.U_ASD == dateASD : true) && (dateADA != null ? c.U_ADA == dateADA : true)).ToList());

                mDbContext.Dispose();

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetMyShipmentListSearchBP :" + ex.Message);
                return null;
            }

        }

        public List<ARGNSContainerPackage> GetContainerPackageList(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<ARGNSContainerPackage, C_ARGNS_CNTM_OCPK>();
            Mapper.CreateMap<C_ARGNS_CNTM_OCPK, ARGNSContainerPackage>();
            Mapper.CreateMap<ARGNSContainerPackageLine, C_ARGNS_CNTM_CPK1>();
            Mapper.CreateMap<C_ARGNS_CNTM_CPK1, ARGNSContainerPackageLine>();

            List<ARGNSContainerPackage> mARGNSContainerPackageList = new List<ARGNSContainerPackage>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mARGNSContainerPackageList = Mapper.Map<List<ARGNSContainerPackage>>(mDbContext.C_ARGNS_CNTM_OCPK.Where(c => c.DocEntry == pDocEntry).ToList());
                foreach (ARGNSContainerPackage mPackage in mARGNSContainerPackageList)
                {
                    mPackage.Lines = Mapper.Map<List<ARGNSContainerPackageLine>>(mDbContext.C_ARGNS_CNTM_CPK1.Where(c => c.DocEntry == pDocEntry && c.U_CntnerNum == mPackage.U_CntnerNum).ToList());
                    foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines)
                    {
                        mPackageLine.U_ItemName = mDbContext.OITM.Where(c => c.ItemCode == mPackageLine.U_ItemCode).FirstOrDefault().ItemName;
                    }
                }

                mDbContext.Dispose();

                return mARGNSContainerPackageList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetContainerPackageList :" + ex.Message);
                return null;
            }

        }

        public List<ARGNSPSlipPackage> GetPSlipPackageList(CompanyConn pCompanyParam, int pContainerID, int pShipmentID)
        {
            Mapper.CreateMap<ARGNSPSlipPackage, C_ARGNS_CNTM_OPKG>();
            Mapper.CreateMap<C_ARGNS_CNTM_OPKG, ARGNSPSlipPackage>();
            Mapper.CreateMap<ARGNSPSlipPackageLine, C_ARGNS_CNTM_PKG1>();
            Mapper.CreateMap<C_ARGNS_CNTM_PKG1, ARGNSPSlipPackageLine>();

            List<ARGNSPSlipPackage> mARGNSPSlipPackageList = new List<ARGNSPSlipPackage>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mARGNSPSlipPackageList = Mapper.Map<List<ARGNSPSlipPackage>>(mDbContext.C_ARGNS_CNTM_OPKG.Where(c => c.U_CntnerNum == pContainerID && c.DocEntry == pShipmentID).ToList());
                foreach (ARGNSPSlipPackage mPSlip in mARGNSPSlipPackageList)
                {
                    mPSlip.Lines = Mapper.Map<List<ARGNSPSlipPackageLine>>(mDbContext.C_ARGNS_CNTM_PKG1.Where(c => c.U_CntnerNum == pContainerID && c.U_PackageNum == mPSlip.U_PackageNum && c.DocEntry == mPSlip.DocEntry).ToList());
                }

                mDbContext.Dispose();

                return mARGNSPSlipPackageList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetPSlipPackageList :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSContainerSetup> GetContainerSetupList(CompanyConn pCompanyParam)
        {
            Mapper.CreateMap<ARGNSContainerSetup, C_ARGNS_CPKG>();
            Mapper.CreateMap<C_ARGNS_CPKG, ARGNSContainerSetup>();

            List<ARGNSContainerSetup> mARGNSContainerList = new List<ARGNSContainerSetup>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mARGNSContainerList = Mapper.Map<List<ARGNSContainerSetup>>(mDbContext.C_ARGNS_CPKG.ToList());

                mDbContext.Dispose();

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetContainerSetupList :" + ex.Message);
                return null;
            }
        }

        public List<ARGNSPSlipPackageType> GetPSlipPackageTypeList(CompanyConn pCompanyParam)
        {
            Mapper.CreateMap<ARGNSPSlipPackageType, OPKG>();
            Mapper.CreateMap<OPKG, ARGNSPSlipPackageType>();

            List<ARGNSPSlipPackageType> mARGNSContainerList = new List<ARGNSPSlipPackageType>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mARGNSContainerList = Mapper.Map<List<ARGNSPSlipPackageType>>(mDbContext.OPKG.ToList());

                mDbContext.Dispose();

                return mARGNSContainerList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetPSlipPackageTypeList :" + ex.Message);
                return null;
            }
        }

        public string UpdateShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment)
        {
            ARGNSContainerServiceWeb.ArgnsContainer oArgnsContainer = new ARGNSContainerServiceWeb.ArgnsContainer();
            ARGNSContainerServiceWeb.ArgnsContainerParams oArgnsContainerParams = new ARGNSContainerServiceWeb.ArgnsContainerParams();
            ARGNSContainerServiceWeb.MsgHeader oArgnsContainerParamsMsgHeader = new ARGNSContainerServiceWeb.MsgHeader();

            oArgnsContainerParamsMsgHeader.ServiceName = ARGNSContainerServiceWeb.MsgHeaderServiceName.ARGNS_CNTM;
            oArgnsContainerParamsMsgHeader.ServiceNameSpecified = true;
            oArgnsContainerParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSContainerServiceWeb.ARGNS_CNTM oArgnsContainerService = new ARGNSContainerServiceWeb.ARGNS_CNTM();
            oArgnsContainerService.MsgHeaderValue = oArgnsContainerParamsMsgHeader;

            try
            {
                oArgnsContainerParams.DocEntry = pShipment.DocEntry;
                oArgnsContainerParams.DocEntrySpecified = true;
                oArgnsContainer = oArgnsContainerService.GetByParams(oArgnsContainerParams);

                oArgnsContainer.U_Shipment = pShipment.U_Shipment;
                oArgnsContainer.U_Broker = pShipment.U_Broker;
                oArgnsContainer.U_Status = pShipment.U_Status;
                oArgnsContainer.U_ShipVia = pShipment.U_ShipVia;
                oArgnsContainer.U_POE = pShipment.U_POE;
                oArgnsContainer.U_UserTxt1 = pShipment.U_UserTxt1;
                oArgnsContainer.U_UserTxt2 = pShipment.U_UserTxt2;
                oArgnsContainer.U_UserTxt3 = pShipment.U_UserTxt3;
                oArgnsContainer.U_UserTxt4 = pShipment.U_UserTxt4;
                decimal? mQuantity = pShipment.Lines.Sum(c => c.DraftLine.Quantity);
                if (mQuantity != null)
                {
                    oArgnsContainer.U_Quantity = (double)mQuantity;
                    oArgnsContainer.U_QuantitySpecified = true;
                }
                if (pShipment.U_ESD != null)
                    oArgnsContainer.U_ESD = pShipment.U_ESD.Value;
                if (pShipment.U_EDA != null)
                    oArgnsContainer.U_EDA = pShipment.U_EDA.Value;
                if (pShipment.U_EDW != null)
                    oArgnsContainer.U_EDW = pShipment.U_EDW.Value;
                if (pShipment.U_ASD != null)
                    oArgnsContainer.U_ASD = pShipment.U_ASD.Value;
                if (pShipment.U_ADA != null)
                    oArgnsContainer.U_ADA = pShipment.U_ADA.Value;
                if (pShipment.U_ADW != null)
                    oArgnsContainer.U_ADW = pShipment.U_ADW.Value;

                #region Shipment Plan Details

                if (pShipment.Lines.Count > 0 || oArgnsContainer.ARGNS_CNTM_LINECollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el Shipment
                    foreach (var mContainerLineSAP in oArgnsContainer.ARGNS_CNTM_LINECollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSContainerLine mContainerLine = pShipment.Lines.Where(c => c.LineId == mContainerLineSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mContainerLine != null)
                        {
                            //En este caso no modificamos nada porque en el portal los campos de las lineas son read only
                        }
                        //Sino la borro
                        else
                        {
                            oArgnsContainer.ARGNS_CNTM_LINECollection = oArgnsContainer.ARGNS_CNTM_LINECollection.Where(c => c.LineId != mContainerLineSAP.LineId).ToArray();
                        }
                    }

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[oArgnsContainer.ARGNS_CNTM_LINECollection.Length + pShipment.Lines.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsContainer.ARGNS_CNTM_LINECollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsContainer.ARGNS_CNTM_LINECollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSContainerLine mContainerLine in pShipment.Lines.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE lineContainer = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE();
                        lineContainer.LineId = mContainerLine.LineId;
                        lineContainer.LineIdSpecified = true;
                        lineContainer.U_BaseDocNo = mContainerLine.U_BaseDocNo;
                        lineContainer.U_BaseEntry = mContainerLine.U_BaseEntry;
                        lineContainer.U_BaseLine = mContainerLine.U_BaseLine.Value;
                        lineContainer.U_BaseLineSpecified = true;
                        lineContainer.U_ARGNS_COL = mContainerLine.U_ARGNS_COL;
                        lineContainer.U_ARGNS_MOD = mContainerLine.U_ARGNS_MOD;
                        lineContainer.U_ARGNS_SEASON = mContainerLine.U_ARGNS_SEASON;
                        lineContainer.U_ARGNS_SIZE = mContainerLine.U_ARGNS_SIZE;
                        lineContainer.U_ItemName = mContainerLine.U_ItemName;
                        lineContainer.U_ItemCode = mContainerLine.U_ItemCode;
                        lineContainer.U_CardCode = mContainerLine.U_CardCode;
                        lineContainer.U_CardName = mContainerLine.U_CardName;
                        lineContainer.U_LineStatus = mContainerLine.U_LineStatus;
                        lineContainer.U_ObjType = mContainerLine.U_ObjType;
                        lineContainer.U_WhsCode = mContainerLine.U_WhsCode;

                        newLines[positionNewLine] = lineContainer;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_LINECollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_LINECollection = newLines;
                }

                #endregion


                #region Shipment Containers

                if (pShipment.ListPackage.Count > 0 || oArgnsContainer.ARGNS_CNTM_OCPKCollection.Count() > 0)
                {

                    #region Containers ya existentes

                    //Actualizo y elimino los container que ya estaban creados
                    foreach (var mPackageSAP in oArgnsContainer.ARGNS_CNTM_OCPKCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSContainerPackage mPackage = pShipment.ListPackage.Where(c => c.LineId == mPackageSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mPackage != null)
                        {
                            if (mPackage.Lines.Count > 0)
                            {
                                //Actualizo y elimino las lineas que ya estaban creadas en el Container
                                foreach (var mPackageLineSAP in oArgnsContainer.ARGNS_CNTM_CPK1Collection.Where(c => c.DocEntry == mPackage.DocEntry && c.U_CntnerNum == mPackage.U_CntnerNum))
                                {
                                    //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                                    ARGNSContainerPackageLine mPackageLine = mPackage.Lines.Where(c => c.LineId == mPackageLineSAP.LineId && c.IsNew == false).FirstOrDefault();
                                    if (mPackageLine != null)
                                    {
                                        //Aca va lo que se pueda modificar en la linea desde el portal
                                        mPackageLineSAP.U_Quantity = (double)mPackageLine.U_Quantity;
                                        mPackageLineSAP.U_QuantitySpecified = true;
                                    }
                                    //Sino la borro
                                    else
                                    {
                                        oArgnsContainer.ARGNS_CNTM_CPK1Collection = oArgnsContainer.ARGNS_CNTM_CPK1Collection.Where(c => c.LineId != mPackageLineSAP.LineId && c.U_CntnerNum == mPackageLineSAP.U_CntnerNum).ToArray();
                                    }
                                }

                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[] newPackageLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length + mPackage.Lines.Where(c => c.IsNew == true).ToList().Count];
                                oArgnsContainer.ARGNS_CNTM_CPK1Collection.CopyTo(newPackageLines, 0);
                                int positionNewPackageLine = oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length;

                                //Agrego las lineas nuevas del container
                                foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines.Where(c => c.IsNew == true).ToList())
                                {
                                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1 linePackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1();
                                    linePackage.LineId = mPackageLine.LineId;
                                    linePackage.LineIdSpecified = true;
                                    linePackage.U_BaseEntry = mPackageLine.U_BaseEntry;
                                    linePackage.U_BaseLine = mPackageLine.U_BaseLine;
                                    linePackage.U_BaseRef = mPackageLine.U_BaseRef;
                                    linePackage.U_CntnerNum = mPackageLine.U_CntnerNum.Value;
                                    linePackage.U_CntnerNumSpecified = true;
                                    linePackage.U_ItemCode = mPackageLine.U_ItemCode;
                                    linePackage.U_Quantity = (double)mPackageLine.U_Quantity.Value;
                                    linePackage.U_QuantitySpecified = true;

                                    newPackageLines[positionNewPackageLine] = linePackage;
                                    positionNewPackageLine += 1;
                                }
                                oArgnsContainer.ARGNS_CNTM_CPK1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[newPackageLines.Length];
                                oArgnsContainer.ARGNS_CNTM_CPK1Collection = newPackageLines;
                            }
                        }
                        //Sino borro el container
                        else
                        {
                            oArgnsContainer.ARGNS_CNTM_OCPKCollection = oArgnsContainer.ARGNS_CNTM_OCPKCollection.Where(c => c.LineId != mPackageSAP.LineId).ToArray();
                        }
                    }

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[oArgnsContainer.ARGNS_CNTM_OCPKCollection.Length + pShipment.ListPackage.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsContainer.ARGNS_CNTM_OCPKCollection.Length;

                    #endregion

                    #region Containers nuevos

                    //Agrego los container nuevos
                    foreach (ARGNSContainerPackage mPackage in pShipment.ListPackage.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK auxPackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK();
                        auxPackage.LineId = mPackage.LineId;
                        auxPackage.LineIdSpecified = true;
                        auxPackage.U_CntnerNum = mPackage.U_CntnerNum.Value;
                        auxPackage.U_CntnerNumSpecified = true;
                        auxPackage.U_CntnerTyp = mPackage.U_CntnerTyp;
                        auxPackage.U_Weight = 0;
                        auxPackage.U_WeightSpecified = true;


                        if (mPackage.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[] newPackageLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length + mPackage.Lines.Where(c => c.IsNew == true).ToList().Count];
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection.CopyTo(newPackageLines, 0);
                            int positionNewPackageLine = oArgnsContainer.ARGNS_CNTM_CPK1Collection.Length;

                            //Agrego las lineas nuevas del container
                            foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1 linePackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1();
                                linePackage.LineId = mPackageLine.LineId;
                                linePackage.LineIdSpecified = true;
                                linePackage.U_BaseEntry = mPackageLine.U_BaseEntry;
                                linePackage.U_BaseLine = mPackageLine.U_BaseLine;
                                linePackage.U_BaseRef = mPackageLine.U_BaseRef;
                                linePackage.U_CntnerNum = mPackageLine.U_CntnerNum.Value;
                                linePackage.U_CntnerNumSpecified = true;
                                linePackage.U_ItemCode = mPackageLine.U_ItemCode;
                                linePackage.U_Quantity = (double)mPackageLine.U_Quantity.Value;
                                linePackage.U_QuantitySpecified = true;
                                linePackage.U_UomEntry = -1;
                                linePackage.U_UomEntrySpecified = true;
                                linePackage.U_NumPerMsr = 1;
                                linePackage.U_NumPerMsrSpecified = true;

                                newPackageLines[positionNewPackageLine] = linePackage;
                                positionNewPackageLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[newPackageLines.Length];
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = newPackageLines;
                        }

                        newLines[positionNewLine] = auxPackage;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = newLines;

                    #endregion

                }

                #endregion


                #region Shipment Containers Packagin Slip

                if (pShipment.ListPackaginSlip.Count > 0 || oArgnsContainer.ARGNS_CNTM_OPKGCollection.Count() > 0)
                {

                    #region Packagin Slip ya existentes

                    //Actualizo y elimino los packagin slip que ya estaban creados
                    foreach (var mPackaginSlipSAP in oArgnsContainer.ARGNS_CNTM_OPKGCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSPSlipPackage mPackaginSlip = pShipment.ListPackaginSlip.Where(c => c.LineId == mPackaginSlipSAP.LineId && c.DocEntry == mPackaginSlipSAP.DocEntry && c.IsNew == false).FirstOrDefault();
                        if (mPackaginSlip != null)
                        {
                            if (mPackaginSlip.Lines.Count > 0)
                            {
                                //Actualizo y elimino las lineas que ya estaban creadas en el Packagin Slip
                                foreach (var mPackagingSlipLineSAP in oArgnsContainer.ARGNS_CNTM_PKG1Collection.Where(c => c.DocEntry == mPackaginSlip.DocEntry && c.U_CntnerNum == mPackaginSlip.U_CntnerNum && c.U_PackageNum == mPackaginSlip.U_PackageNum))
                                {
                                    //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                                    ARGNSPSlipPackageLine mPackagingSlipLine = mPackaginSlip.Lines.Where(c => c.LineId == mPackagingSlipLineSAP.LineId && c.DocEntry == mPackagingSlipLineSAP.DocEntry && c.IsNew == false).FirstOrDefault();
                                    if (mPackagingSlipLine != null)
                                    {
                                        //Aca va lo que se pueda modificar en la linea desde el portal
                                        mPackagingSlipLineSAP.U_Quantity = (double)mPackagingSlipLine.U_Quantity;
                                        mPackagingSlipLineSAP.U_QuantitySpecified = true;
                                    }
                                    //Sino la borro
                                    else
                                    {
                                        oArgnsContainer.ARGNS_CNTM_PKG1Collection = oArgnsContainer.ARGNS_CNTM_PKG1Collection.Where(c => c.LineId != mPackagingSlipLineSAP.LineId && c.U_CntnerNum == mPackagingSlipLineSAP.U_CntnerNum && c.U_PackageNum == mPackagingSlipLineSAP.U_PackageNum).ToArray();
                                    }
                                }

                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[] newPackagingSlipLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length + mPackaginSlip.Lines.Where(c => c.IsNew == true).ToList().Count];
                                oArgnsContainer.ARGNS_CNTM_PKG1Collection.CopyTo(newPackagingSlipLines, 0);
                                int positionNewPackagingSlipLine = oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length;

                                //Agrego las lineas nuevas del packaging slip
                                foreach (ARGNSPSlipPackageLine mPackagingSlipLine in mPackaginSlip.Lines.Where(c => c.IsNew == true).ToList())
                                {
                                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1 linePackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1();
                                    linePackagingSlip.LineId = mPackagingSlipLine.LineId;
                                    linePackagingSlip.LineIdSpecified = true;
                                    linePackagingSlip.U_BaseEntry = mPackagingSlipLine.U_BaseEntry;
                                    linePackagingSlip.U_BaseLine = mPackagingSlipLine.U_BaseLine;
                                    linePackagingSlip.U_BaseRef = mPackagingSlipLine.U_BaseRef;
                                    linePackagingSlip.U_CntnerNum = mPackagingSlipLine.U_CntnerNum.Value;
                                    linePackagingSlip.U_CntnerNumSpecified = true;
                                    linePackagingSlip.U_PackageNum = mPackagingSlipLine.U_PackageNum.Value;
                                    linePackagingSlip.U_PackageNumSpecified = true;
                                    linePackagingSlip.U_ItemCode = mPackagingSlipLine.U_ItemCode;
                                    linePackagingSlip.U_Quantity = (double)mPackagingSlipLine.U_Quantity.Value;
                                    linePackagingSlip.U_QuantitySpecified = true;

                                    newPackagingSlipLines[positionNewPackagingSlipLine] = linePackagingSlip;
                                    positionNewPackagingSlipLine += 1;
                                }
                                oArgnsContainer.ARGNS_CNTM_PKG1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[newPackagingSlipLines.Length];
                                oArgnsContainer.ARGNS_CNTM_PKG1Collection = newPackagingSlipLines;
                            }
                        }
                        //Sino borro el packaging slip
                        else
                        {
                            oArgnsContainer.ARGNS_CNTM_OPKGCollection = oArgnsContainer.ARGNS_CNTM_OPKGCollection.Where(c => c.LineId != mPackaginSlipSAP.LineId && c.DocEntry == mPackaginSlipSAP.DocEntry).ToArray();
                        }
                    }

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[oArgnsContainer.ARGNS_CNTM_OPKGCollection.Length + pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsContainer.ARGNS_CNTM_OPKGCollection.Length;

                    #endregion

                    #region Packaging Slip nuevos

                    //Agrego los packaging slip nuevos
                    foreach (ARGNSPSlipPackage mPackagingSlip in pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG auxPackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG();
                        auxPackagingSlip.LineId = mPackagingSlip.LineId;
                        auxPackagingSlip.LineIdSpecified = true;
                        auxPackagingSlip.U_CntnerNum = mPackagingSlip.U_CntnerNum.Value;
                        auxPackagingSlip.U_CntnerNumSpecified = true;
                        auxPackagingSlip.U_PackageNum = mPackagingSlip.U_PackageNum.Value;
                        auxPackagingSlip.U_PackageNumSpecified = true;
                        auxPackagingSlip.U_PackageTyp = mPackagingSlip.U_PackageTyp;
                        auxPackagingSlip.U_Weight = 0;
                        auxPackagingSlip.U_WeightSpecified = true;
                        auxPackagingSlip.U_WeightUnit = "";

                        if (mPackagingSlip.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[] newPackagingSlipLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length + mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList().Count];
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection.CopyTo(newPackagingSlipLines, 0);
                            int positionNewPackagingSlipLine = oArgnsContainer.ARGNS_CNTM_PKG1Collection.Length;

                            //Agrego las lineas nuevas del packagin slip
                            foreach (ARGNSPSlipPackageLine mPackagingSlipLine in mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1 linePackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1();
                                linePackagingSlip.LineId = mPackagingSlipLine.LineId;
                                linePackagingSlip.LineIdSpecified = true;
                                linePackagingSlip.U_BaseEntry = mPackagingSlipLine.U_BaseEntry;
                                linePackagingSlip.U_BaseLine = mPackagingSlipLine.U_BaseLine;
                                linePackagingSlip.U_BaseRef = mPackagingSlipLine.U_BaseRef;
                                linePackagingSlip.U_CntnerNum = mPackagingSlipLine.U_CntnerNum.Value;
                                linePackagingSlip.U_CntnerNumSpecified = true;
                                linePackagingSlip.U_PackageNum = mPackagingSlipLine.U_PackageNum.Value;
                                linePackagingSlip.U_PackageNumSpecified = true;
                                linePackagingSlip.U_ItemCode = mPackagingSlipLine.U_ItemCode;
                                linePackagingSlip.U_Quantity = (double)mPackagingSlipLine.U_Quantity.Value;
                                linePackagingSlip.U_QuantitySpecified = true;
                                linePackagingSlip.U_UomEntry = -1;
                                linePackagingSlip.U_UomEntrySpecified = true;
                                linePackagingSlip.U_NumPerMsr = 1;
                                linePackagingSlip.U_NumPerMsrSpecified = true;

                                newPackagingSlipLines[positionNewPackagingSlipLine] = linePackagingSlip;
                                positionNewPackagingSlipLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[newPackagingSlipLines.Length];
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = newPackagingSlipLines;
                        }

                        newLines[positionNewLine] = auxPackagingSlip;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = newLines;

                    #endregion

                }

                #endregion


                oArgnsContainerService.Update(oArgnsContainer);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateShipment :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string AddShipment(CompanyConn pCompanyParam, ARGNSContainer pShipment)
        {
            ARGNSContainerServiceWeb.ArgnsContainer oArgnsContainer = new ARGNSContainerServiceWeb.ArgnsContainer();
            ARGNSContainerServiceWeb.MsgHeader oArgnsContainerParamsMsgHeader = new ARGNSContainerServiceWeb.MsgHeader();

            oArgnsContainerParamsMsgHeader.ServiceName = ARGNSContainerServiceWeb.MsgHeaderServiceName.ARGNS_CNTM;
            oArgnsContainerParamsMsgHeader.ServiceNameSpecified = true;
            oArgnsContainerParamsMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSContainerServiceWeb.ARGNS_CNTM oArgnsContainerService = new ARGNSContainerServiceWeb.ARGNS_CNTM();
            oArgnsContainerService.MsgHeaderValue = oArgnsContainerParamsMsgHeader;

            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                oArgnsContainer.U_Shipment = pShipment.U_Shipment;
                oArgnsContainer.U_Broker = pShipment.U_Broker;
                oArgnsContainer.U_Status = pShipment.U_Status;
                oArgnsContainer.U_ShipVia = pShipment.U_ShipVia;
                oArgnsContainer.U_POE = pShipment.U_POE;
                oArgnsContainer.U_UserTxt1 = pShipment.U_UserTxt1;
                oArgnsContainer.U_UserTxt2 = pShipment.U_UserTxt2;
                oArgnsContainer.U_UserTxt3 = pShipment.U_UserTxt3;
                oArgnsContainer.U_UserTxt4 = pShipment.U_UserTxt4;
                decimal? mQuantity = pShipment.Lines.Sum(c => c.DraftLine.Quantity);
                if (mQuantity != null)
                {
                    oArgnsContainer.U_Quantity = (double)mQuantity;
                    oArgnsContainer.U_QuantitySpecified = true;
                }
                if (pShipment.U_ESD != null)
                    oArgnsContainer.U_ESD = pShipment.U_ESD.Value;
                if (pShipment.U_EDA != null)
                    oArgnsContainer.U_EDA = pShipment.U_EDA.Value;
                if (pShipment.U_EDW != null)
                    oArgnsContainer.U_EDW = pShipment.U_EDW.Value;
                if (pShipment.U_ASD != null)
                    oArgnsContainer.U_ASD = pShipment.U_ASD.Value;
                if (pShipment.U_ADA != null)
                    oArgnsContainer.U_ADA = pShipment.U_ADA.Value;
                if (pShipment.U_ADW != null)
                    oArgnsContainer.U_ADW = pShipment.U_ADW.Value;

                #region Shipment Plan Details

                if (pShipment.Lines.Count > 0)
                {
                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[pShipment.Lines.Where(c => c.IsNew == true).ToList().Count];
                    int positionNewLine = 0;

                    //Agrego las lineas nuevas
                    foreach (ARGNSContainerLine mContainerLine in pShipment.Lines.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE lineContainer = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE();
                        lineContainer.LineId = mContainerLine.LineId;
                        lineContainer.LineIdSpecified = true;
                        lineContainer.U_BaseDocNo = mContainerLine.U_BaseDocNo;
                        lineContainer.U_BaseEntry = mContainerLine.U_BaseEntry;
                        lineContainer.U_BaseLine = mContainerLine.U_BaseLine.Value;
                        lineContainer.U_BaseLineSpecified = true;
                        lineContainer.U_ARGNS_COL = mContainerLine.U_ARGNS_COL;
                        lineContainer.U_ARGNS_MOD = mContainerLine.U_ARGNS_MOD;
                        lineContainer.U_ARGNS_SEASON = mContainerLine.U_ARGNS_SEASON;
                        lineContainer.U_ARGNS_SIZE = mContainerLine.U_ARGNS_SIZE;
                        lineContainer.U_ItemName = mContainerLine.U_ItemName;
                        lineContainer.U_ItemCode = mContainerLine.U_ItemCode;
                        lineContainer.U_CardCode = mContainerLine.U_CardCode;
                        lineContainer.U_CardName = mContainerLine.U_CardName;
                        lineContainer.U_LineStatus = mContainerLine.U_LineStatus;
                        lineContainer.U_ObjType = mContainerLine.U_ObjType;
                        lineContainer.U_WhsCode = mContainerLine.U_WhsCode;

                        newLines[positionNewLine] = lineContainer;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_LINECollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_LINE[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_LINECollection = newLines;
                }

                #endregion

                #region Shipment Containers

                if (pShipment.ListPackage.Count > 0)
                {

                    #region Containers nuevos

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[pShipment.ListPackage.Where(c => c.IsNew == true).ToList().Count];
                    int positionNewLine = 0;

                    //Agrego los container nuevos
                    foreach (ARGNSContainerPackage mPackage in pShipment.ListPackage.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK auxPackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK();
                        auxPackage.LineId = mPackage.LineId;
                        auxPackage.LineIdSpecified = true;
                        auxPackage.U_CntnerNum = mPackage.U_CntnerNum.Value;
                        auxPackage.U_CntnerNumSpecified = true;
                        auxPackage.U_CntnerTyp = mPackage.U_CntnerTyp;
                        auxPackage.U_Weight = 0;
                        auxPackage.U_WeightSpecified = true;


                        if (mPackage.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[] newPackageLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[mPackage.Lines.Where(c => c.IsNew == true).ToList().Count];
                            int positionNewPackageLine = 0;

                            //Agrego las lineas nuevas del container
                            foreach (ARGNSContainerPackageLine mPackageLine in mPackage.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1 linePackage = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1();
                                linePackage.LineId = mPackageLine.LineId;
                                linePackage.LineIdSpecified = true;
                                linePackage.U_BaseEntry = mPackageLine.U_BaseEntry;
                                linePackage.U_BaseLine = mPackageLine.U_BaseLine;
                                linePackage.U_BaseRef = mPackageLine.U_BaseRef;
                                linePackage.U_CntnerNum = mPackageLine.U_CntnerNum.Value;
                                linePackage.U_CntnerNumSpecified = true;
                                linePackage.U_ItemCode = mPackageLine.U_ItemCode;
                                linePackage.U_Quantity = (double)mPackageLine.U_Quantity.Value;
                                linePackage.U_QuantitySpecified = true;
                                linePackage.U_UomEntry = -1;
                                linePackage.U_UomEntrySpecified = true;
                                linePackage.U_NumPerMsr = 1;
                                linePackage.U_NumPerMsrSpecified = true;

                                newPackageLines[positionNewPackageLine] = linePackage;
                                positionNewPackageLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_CPK1[newPackageLines.Length];
                            oArgnsContainer.ARGNS_CNTM_CPK1Collection = newPackageLines;
                        }

                        newLines[positionNewLine] = auxPackage;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OCPK[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OCPKCollection = newLines;

                    #endregion

                }

                #endregion

                #region Shipment Containers Packagin Slip

                if (pShipment.ListPackaginSlip.Count > 0)
                {

                    #region Packaging Slip nuevos

                    ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[] newLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList().Count];
                    int positionNewLine = 0;

                    //Agrego los packaging slip nuevos
                    foreach (ARGNSPSlipPackage mPackagingSlip in pShipment.ListPackaginSlip.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG auxPackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG();
                        auxPackagingSlip.LineId = mPackagingSlip.LineId;
                        auxPackagingSlip.LineIdSpecified = true;
                        auxPackagingSlip.U_CntnerNum = mPackagingSlip.U_CntnerNum.Value;
                        auxPackagingSlip.U_CntnerNumSpecified = true;
                        auxPackagingSlip.U_PackageNum = mPackagingSlip.U_PackageNum.Value;
                        auxPackagingSlip.U_PackageNumSpecified = true;
                        auxPackagingSlip.U_PackageTyp = mPackagingSlip.U_PackageTyp;
                        auxPackagingSlip.U_Weight = 0;
                        auxPackagingSlip.U_WeightSpecified = true;
                        auxPackagingSlip.U_WeightUnit = "";

                        if (mPackagingSlip.Lines.Count > 0)
                        {
                            ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[] newPackagingSlipLines = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList().Count];
                            int positionNewPackagingSlipLine = 0;

                            //Agrego las lineas nuevas del packaging slip
                            foreach (ARGNSPSlipPackageLine mPackagingSlipLine in mPackagingSlip.Lines.Where(c => c.IsNew == true).ToList())
                            {
                                ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1 linePackagingSlip = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1();
                                linePackagingSlip.LineId = mPackagingSlipLine.LineId;
                                linePackagingSlip.LineIdSpecified = true;
                                linePackagingSlip.U_BaseEntry = mPackagingSlipLine.U_BaseEntry;
                                linePackagingSlip.U_BaseLine = mPackagingSlipLine.U_BaseLine;
                                linePackagingSlip.U_BaseRef = mPackagingSlipLine.U_BaseRef;
                                linePackagingSlip.U_CntnerNum = mPackagingSlipLine.U_CntnerNum.Value;
                                linePackagingSlip.U_CntnerNumSpecified = true;
                                linePackagingSlip.U_PackageNum = mPackagingSlipLine.U_PackageNum.Value;
                                linePackagingSlip.U_PackageNumSpecified = true;
                                linePackagingSlip.U_ItemCode = mPackagingSlipLine.U_ItemCode;
                                linePackagingSlip.U_Quantity = (double)mPackagingSlipLine.U_Quantity.Value;
                                linePackagingSlip.U_QuantitySpecified = true;
                                linePackagingSlip.U_UomEntry = -1;
                                linePackagingSlip.U_UomEntrySpecified = true;
                                linePackagingSlip.U_NumPerMsr = 1;
                                linePackagingSlip.U_NumPerMsrSpecified = true;

                                newPackagingSlipLines[positionNewPackagingSlipLine] = linePackagingSlip;
                                positionNewPackagingSlipLine += 1;
                            }
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_PKG1[newPackagingSlipLines.Length];
                            oArgnsContainer.ARGNS_CNTM_PKG1Collection = newPackagingSlipLines;
                        }

                        newLines[positionNewLine] = auxPackagingSlip;
                        positionNewLine += 1;
                    }
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = new ARGNSContainerServiceWeb.ArgnsContainerARGNS_CNTM_OPKG[newLines.Length];
                    oArgnsContainer.ARGNS_CNTM_OPKGCollection = newLines;

                    #endregion

                }

                #endregion


                oArgnsContainerService.Add(oArgnsContainer);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> AddShipment :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pObjType"></param>
		/// <returns></returns>
        public List<Draft> GetDocumentWhitOpenLines(CompanyConn pCompanyParam, string pObjType)
        {
            Mapper.CreateMap<OPOR, Draft>();
            Mapper.CreateMap<Draft, OPOR>();
            Mapper.CreateMap<ORDR, Draft>();
            Mapper.CreateMap<Draft, ORDR>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                List<Draft> mDrafList = new List<Draft>();
                List<int> mDocEntryList = new List<int>();
                if (pObjType == "22")
                {
                    mDocEntryList = mDbContext.POR1.Where(c => c.LineStatus == "O").Select(p => p.DocEntry).Distinct().ToList();
                    mDrafList = Mapper.Map<List<Draft>>(mDbContext.OPOR.Where(c => mDocEntryList.Contains(c.DocEntry)).ToList());
                }
                if (pObjType == "17")
                {
                    mDocEntryList = mDbContext.RDR1.Where(c => c.LineStatus == "O").Select(p => p.DocEntry).Distinct().ToList();
                    mDrafList = Mapper.Map<List<Draft>>(mDbContext.ORDR.Where(c => mDocEntryList.Contains(c.DocEntry)).ToList());
                }

                mDbContext.Dispose();

                return mDrafList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetDocumentWhitOpenLines :" + ex.Message);
                return null;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pObjType"></param>
		/// <param name="pDocEntryList"></param>
		/// <returns></returns>
        public List<DraftLine> GetDocumentOpenLines(CompanyConn pCompanyParam, string pObjType, List<int> pDocEntryList)
        {
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                List<DraftLine> mDrafLineList = new List<DraftLine>();
                if (pObjType == "22")
                {
                    //mDrafLineList = Mapper.Map<List<DraftLine>>(mDbContext.POR1.Where(c => pDocEntryList.Contains(c.DocEntry) && c.LineStatus == "O").ToList());
                    mDrafLineList = (from mPOR1 in mDbContext.POR1
                                     join mOPOR in mDbContext.OPOR on mPOR1.DocEntry equals mOPOR.DocEntry into group1
                                     from mGroup1 in group1.DefaultIfEmpty()
                                     join mOITM in mDbContext.OITM on mPOR1.ItemCode equals mOITM.ItemCode into group2
                                     from mGroup2 in group2.DefaultIfEmpty()
                                     join mARGNS_MODEL in mDbContext.C_ARGNS_MODEL on mGroup2.U_ARGNS_MOD equals mARGNS_MODEL.U_ModCode into group3
                                     from mGroup3 in group3.DefaultIfEmpty()
                                     where
                                         (
                                         (pDocEntryList.Contains(mPOR1.DocEntry) &&
                                         mPOR1.LineStatus == "O")
                                         )
                                     select new DraftLine()
                                     {
                                         DocEntry = mPOR1.DocEntry,
                                         LineNum = mPOR1.LineNum,
                                         ItemCode = mPOR1.ItemCode,
                                         ItemName = mGroup2.ItemName,
                                         Quantity = mPOR1.Quantity,
                                         CardCode = mGroup1.CardCode,
                                         CardName = mGroup1.CardName,
                                         DocNum = mGroup1.DocNum,
                                         WhsCode = mPOR1.WhsCode,
                                         ShipToCode = mPOR1.ShipToCode,
                                         U_ARGNS_MOD = mGroup2.U_ARGNS_MOD,
                                         U_ARGNS_COL = mGroup2.U_ARGNS_COL,
                                         U_ARGNS_SIZE = mGroup2.U_ARGNS_SIZE,
                                         U_ARGNS_SEASON = mGroup3.U_Season
                                     }).ToList();
                }
                if (pObjType == "17")
                {
                    //mDrafLineList = Mapper.Map<List<DraftLine>>(mDbContext.RDR1.Where(c => pDocEntryList.Contains(c.DocEntry) && c.LineStatus == "O").ToList());
                    mDrafLineList = (from mRDR1 in mDbContext.RDR1
                                     join mORDR in mDbContext.ORDR on mRDR1.DocEntry equals mORDR.DocEntry into group1
                                     from mGroup1 in group1.DefaultIfEmpty()
                                     join mOITM in mDbContext.OITM on mRDR1.ItemCode equals mOITM.ItemCode into group2
                                     from mGroup2 in group2.DefaultIfEmpty()
                                     join mARGNS_MODEL in mDbContext.C_ARGNS_MODEL on mGroup2.U_ARGNS_MOD equals mARGNS_MODEL.U_ModCode into group3
                                     from mGroup3 in group3.DefaultIfEmpty()
                                     where
                                         (
                                         (pDocEntryList.Contains(mRDR1.DocEntry) &&
                                         mRDR1.LineStatus == "O")
                                         )
                                     select new DraftLine()
                                     {
                                         DocEntry = mRDR1.DocEntry,
                                         LineNum = mRDR1.LineNum,
                                         ItemCode = mRDR1.ItemCode,
                                         ItemName = mGroup2.ItemName,
                                         Quantity = mRDR1.Quantity,
                                         CardCode = mGroup1.CardCode,
                                         CardName = mGroup1.CardName,
                                         DocNum = mGroup1.DocNum,
                                         WhsCode = mRDR1.WhsCode,
                                         ShipToCode = mRDR1.ShipToCode,
                                         U_ARGNS_MOD = mGroup2.U_ARGNS_MOD,
                                         U_ARGNS_COL = mGroup2.U_ARGNS_COL,
                                         U_ARGNS_SIZE = mGroup2.U_ARGNS_SIZE,
                                         DelivrdQty = mRDR1.DelivrdQty,
                                         U_ARGNS_SEASON = mGroup3.U_Season
                                     }).ToList();
                }

                mDbContext.Dispose();

                return mDrafLineList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetDocumentOpenLines :" + ex.Message);
                return null;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pShipmentID"></param>
		/// <param name="pObjType"></param>
		/// <returns></returns>
        public List<Draft> GetShipmentDocument(CompanyConn pCompanyParam, string pShipmentID, string pObjType)
        {
            Mapper.CreateMap<Draft, ODRF>();
            Mapper.CreateMap<ODRF, Draft>();

            try
            {
                string mConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.ConnectionString = mConnectionString;
                mDbContext.Database.Connection.Open();
                List<ODRF> mDocumentList = new List<ODRF>();

                if (pObjType == "22")
                {
                    string mWhere = "[OPDN].[U_ARGNS_SHPCNO] = " + pShipmentID;
                    mDocumentList = UDFUtil.GetObjectListWithUDF(null, mWhere, typeof(ODRF), mConnectionString, "OPDN").Cast<ODRF>().ToList();
                }
                if (pObjType == "17")
                {
                    string mWhere = "[ODLN].[U_ARGNS_SHPCNO] = " + pShipmentID;
                    mDocumentList = UDFUtil.GetObjectListWithUDF(null, mWhere, typeof(ODRF), mConnectionString, "ODLN").Cast<ODRF>().ToList();
                }

                return Mapper.Map<List<Draft>>(mDocumentList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetShipmentDocument :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }

        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <returns></returns>
        public ShipmentMembers GetShipmentMembersObject(CompanyConn pCompanyParam)
        {
            Mapper.CreateMap<ARGNSContainerStatus, C_ARGNS_CNTM_STATUS>();
            Mapper.CreateMap<C_ARGNS_CNTM_STATUS, ARGNSContainerStatus>();
            Mapper.CreateMap<ARGNSContainerPort, C_ARGNS_CNTM_PORT>();
            Mapper.CreateMap<C_ARGNS_CNTM_PORT, ARGNSContainerPort>();
            Mapper.CreateMap<Freight, OEXD>();
            Mapper.CreateMap<OEXD, Freight>();

            ShipmentMembers mShipmentMembers = new ShipmentMembers();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mShipmentMembers.ListStatus.Add(new ARGNSContainerStatus("", ""));
                mShipmentMembers.ListStatus.AddRange(Mapper.Map<List<ARGNSContainerStatus>>(mDbContext.C_ARGNS_CNTM_STATUS.ToList()));
                mShipmentMembers.ListPort.Add(new ARGNSContainerPort("", ""));
                mShipmentMembers.ListPort.AddRange(Mapper.Map<List<ARGNSContainerPort>>(mDbContext.C_ARGNS_CNTM_PORT.ToList()));
                mShipmentMembers.ListFreight.Add(new Freight(null, ""));
                mShipmentMembers.ListFreight.AddRange(Mapper.Map<List<Freight>>(mDbContext.OEXD.ToList()));

                mDbContext.Dispose();

                return mShipmentMembers;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetShipmentMembersObject :" + ex.Message);
                return null;
            }
        }

        #endregion

        #region Tech Pack

        public ARGNSTechPack GetTechPack(CompanyConn pCompanyParam, int pId)
        {
            Mapper.CreateMap<ARGNSTechPackLine, C_ARGNS_MODEL_INST>();
            Mapper.CreateMap<C_ARGNS_MODEL_INST, ARGNSTechPackLine>();
            Mapper.CreateMap<ARGNSTechPackSection, C_ARGNS_INSTRUCTION>();
            Mapper.CreateMap<C_ARGNS_INSTRUCTION, ARGNSTechPackSection>();

            ARGNSTechPack mARGNSTechPack = new ARGNSTechPack();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            try
            {
                mARGNSTechPack.Lines = Mapper.Map<List<ARGNSTechPackLine>>(mDbContext.C_ARGNS_MODEL_INST.Where(c => c.Code == pId.ToString()).ToList());

                mARGNSTechPack.Sections = Mapper.Map<List<ARGNSTechPackSection>>(mDbContext.C_ARGNS_INSTRUCTION.ToList());
                C_ARGNS_MODEL mModel = mDbContext.C_ARGNS_MODEL.Where(c => c.Code == pId.ToString()).FirstOrDefault();
                mARGNSTechPack.Code = pId.ToString();
                mARGNSTechPack.ModelCode = mModel.U_ModCode;
                mARGNSTechPack.ModelDesc = mModel.U_ModDesc;
                mARGNSTechPack.Lines = mARGNSTechPack.Lines.Select(c => { c.U_ModCode = mModel.U_ModCode; return c; }).ToList();
                mDbContext.Dispose();

                return mARGNSTechPack;
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetTechPack :" + ex.Message);
                return null;
            }

        }

        public string UpdateTechPack(ARGNSTechPack pObject, CompanyConn pCompanyParam)
        {
            ARGNSModelServiceWeb.ArgnsModel oArgnsModel = new ARGNSModelServiceWeb.ArgnsModel();
            ARGNSModelServiceWeb.ArgnsModelParams oArgnsModelParams = new ARGNSModelServiceWeb.ArgnsModelParams();
            ARGNSModelServiceWeb.MsgHeader oArgnsModelMsgHeader = new ARGNSModelServiceWeb.MsgHeader();

            oArgnsModelMsgHeader.ServiceName = ARGNSModelServiceWeb.MsgHeaderServiceName.ARGNS_MODEL;
            oArgnsModelMsgHeader.ServiceNameSpecified = true;
            oArgnsModelMsgHeader.SessionID = pCompanyParam.DSSessionId;

            ARGNSModelServiceWeb.ARGNS_MODEL oArgnsModelService = new ARGNSModelServiceWeb.ARGNS_MODEL();
            oArgnsModelService.MsgHeaderValue = oArgnsModelMsgHeader;

            try
            {
                oArgnsModelParams.Code = pObject.Code;
                oArgnsModel = oArgnsModelService.GetByParams(oArgnsModelParams);

                if (pObject.Lines.Count > 0 || oArgnsModel.ARGNS_MODEL_INSTCollection.Count() > 0)
                {
                    //Actualizo y elimino las lineas que ya estaban creadas en el TechPack
                    foreach (var mTechPackLineSAP in oArgnsModel.ARGNS_MODEL_INSTCollection)
                    {
                        //Si ya existia en SAP y sigue estando en lo que traigo del portal la modifico
                        ARGNSTechPackLine mTechPackLine = pObject.Lines.Where(c => c.LineId == mTechPackLineSAP.LineId && c.IsNew == false).FirstOrDefault();
                        if (mTechPackLine != null)
                        {
                            mTechPackLineSAP.U_Sector = mTechPackLine.U_Sector;
                            mTechPackLineSAP.U_InstCod = mTechPackLine.U_InstCod;
                            mTechPackLineSAP.U_Instuct = mTechPackLine.U_Instuct;
                            mTechPackLineSAP.U_Descrip = mTechPackLine.U_Descrip;
                            if (mTechPackLine.ImgChanged == true)
                                mTechPackLineSAP.U_Imag = mTechPackLine.U_Imag;
                            mTechPackLineSAP.U_Active = mTechPackLine.U_Active;
                        }
                        //Sino la borro
                        else
                        {
                            oArgnsModel.ARGNS_MODEL_INSTCollection = oArgnsModel.ARGNS_MODEL_INSTCollection.Where(c => c.LineId != mTechPackLineSAP.LineId).ToArray();
                        }
                    }

                    ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST[] newLines = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST[oArgnsModel.ARGNS_MODEL_INSTCollection.Length + pObject.Lines.Where(c => c.IsNew == true).ToList().Count];
                    oArgnsModel.ARGNS_MODEL_INSTCollection.CopyTo(newLines, 0);
                    int positionNewLine = oArgnsModel.ARGNS_MODEL_INSTCollection.Length;

                    //Agrego las lineas nuevas
                    foreach (ARGNSTechPackLine mTechPackLine in pObject.Lines.Where(c => c.IsNew == true).ToList())
                    {
                        ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST lineTechPack = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST();
                        lineTechPack.Code = mTechPackLine.Code;
                        lineTechPack.LineId = mTechPackLine.LineId;
                        lineTechPack.LineIdSpecified = true;
                        lineTechPack.U_Sector = mTechPackLine.U_Sector;
                        lineTechPack.U_InstCod = mTechPackLine.U_InstCod;
                        lineTechPack.U_Instuct = mTechPackLine.U_Instuct;
                        lineTechPack.U_Descrip = mTechPackLine.U_Descrip;
                        lineTechPack.U_Imag = mTechPackLine.U_Imag;
                        lineTechPack.U_Active = mTechPackLine.U_Active;
                        newLines[positionNewLine] = lineTechPack;
                        positionNewLine += 1;
                    }
                    oArgnsModel.ARGNS_MODEL_INSTCollection = new ARGNSModelServiceWeb.ArgnsModelARGNS_MODEL_INST[newLines.Length];
                    oArgnsModel.ARGNS_MODEL_INSTCollection = newLines;
                }

                XmlDocument mXmlDoc = Utils.GetSoapStructure(true, oArgnsModelMsgHeader, oArgnsModel);

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> UpdateTechPack :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pIsUpdate"></param>
		/// <param name="pHeader"></param>
		/// <param name="pBody"></param>
		/// <returns></returns>
        private XmlDocument GetSoapStructure(bool pIsUpdate, object pHeader, object pBody)
        {

            XmlSerializer mySerializer = new XmlSerializer(pBody.GetType(), @"http://www.sap.com/SBO/DIS");
            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);
            xns.Add(string.Empty, @"http://www.sap.com/SBO/DIS");
            MemoryStream streamDocs = new MemoryStream();
            mySerializer.Serialize(streamDocs, pBody, xns);
            XmlDocument mXmlDocument = new XmlDocument();
            mXmlDocument.LoadXml(@"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Header></soap:Header><soap:Body><" + (pIsUpdate ? "Update" : "Add") + @" xmlns=""" + pHeader.GetType().GetProperty("ServiceName").GetValue(pHeader, null) + @"""></" + (pIsUpdate ? "Update" : "Add") + "></soap:Body></soap:Envelope>");
            XmlElement mBodyElement = mXmlDocument.DocumentElement;

            mBodyElement.SelectSingleNode("//*[local-name()='" + (pIsUpdate ? "Update" : "Add") + "']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            streamDocs = new MemoryStream();

            mySerializer = new XmlSerializer(pHeader.GetType(), @"http://www.sap.com/SBO/DIS");
            mySerializer.Serialize(streamDocs, pHeader, xns);

            mBodyElement.SelectSingleNode("//*[local-name()='Header']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            return mXmlDocument;

        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCurrType"></param>
		/// <param name="pSession"></param>
		/// <returns></returns>
        public string GetCurrency(string pCurrType, string pSession)
        {
            string mXmlResult = string.Empty;
            string ret = string.Empty;
            string pXmlString = string.Empty;
            string AddCmd = string.Empty;
            SBODI_Server.Node mServerNode = null;
            XmlDocument mResultXML = null;

            try
            {
                mResultXML = new XmlDocument();
                mServerNode = new SBODI_Server.Node();

                //Poner Enum
                if (pCurrType == WebServices.UtilWeb.CurrentType.SystemCurrency.ToString())
                {
                    AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                    @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                    "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                    @"<env:Body><dis:GetSystemCurrency xmlns:dis=""http://www.sap.com/SBO/DIS""></dis:GetSystemCurrency></env:Body></env:Envelope>";
                }
                else
                {
                    AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                    @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                     "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                    @"<env:Body><dis:GetLocalCurrency xmlns:dis=""http://www.sap.com/SBO/DIS""></dis:GetLocalCurrency></env:Body></env:Envelope>";

                }


                //Envio Solicitud al DI Server y Obtengo la Respuesta
                mXmlResult = mServerNode.Interact(AddCmd);
                mResultXML.LoadXml(mXmlResult);

                //Recupero los Valores de la Respuesta                
                return UtilWeb.GetResult(mResultXML, pCurrType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetCurrency:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pModelCode"></param>
		/// <returns></returns>
        public List<ARGNSModelHistory> GetModelHistoryList(CompanyConn pCompanyParam, string pModelCode = "")
        {
            string mSqlQuery;
            List<ARGNSModelHistory> ret = null;
            try
            {
                mSqlQuery = "SELECT T0.Code,T0.LogInst,T0.UpdateDate,ISNULL((Select OUSR.U_NAME From OUSR Where OUSR.USERID=T0.UserSign),'manager') as 'UserSignName'  FROM [@AARGNS_MODEL] T0 WHERE T0.[Code] = '" + pModelCode + "' AND  T0.[Object] = 'ARGNS_MODEL'";
                UserDS pUserServ = new UserDS();
                ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModelHistory>>(pUserServ.GetQueryResult(pCompanyParam, mSqlQuery));
            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetModelHistoryList :" + ex.Message);
                ret = new List<ARGNSModelHistory>();
            }

            return ret;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pCatalogCode"></param>
		/// <param name="pCatalogName"></param>
		/// <param name="pSalesEmployee"></param>
		/// <returns></returns>
        public List<ARGNSCatalog> GetCatalogListSearch(CompanyConn pCompanyParam, string pCatalogCode = "", string pCatalogName = "", int? pSalesEmployee = null)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<C_ARGNS_FBOOK, ARGNSCatalog>(); }).CreateMapper();
                List<ARGNSCatalog> mCatalogList = new List<ARGNSCatalog>();

                if (pSalesEmployee != null)
                {
                    List<OHEM> mEmployeeList = mDbContext.OHEM.Where(c => c.salesPrson == pSalesEmployee).ToList();
                    List<string> mEmployeeIdList = mEmployeeList.Select(j => j.empID.ToString()).ToList();
                    List<C_ARGNS_FASEMP> mEmployeeCatalogueList = mDbContext.C_ARGNS_FASEMP.Where(c => mEmployeeIdList.Contains(c.U_empId)).ToList();
                    List<string> mCatalogIdList = mEmployeeCatalogueList.Select(j => j.U_FasCode).ToList();
                    mCatalogList = mapper.Map<List<ARGNSCatalog>>(
                        mDbContext.C_ARGNS_FBOOK.Where(c => c.U_Active == "Y" && 
                        mCatalogIdList.Contains(c.Code) 
                            && (c.Code == pCatalogCode || pCatalogCode=="") 
                            && (c.Name.Contains(pCatalogName.Trim()) || pCatalogName.Trim() == ""))
                        .ToList());
                }
                else
                {
                    mCatalogList = mapper.Map<List<ARGNSCatalog>>(mDbContext.C_ARGNS_FBOOK
                        .Where(c => c.U_Active == "Y"
                            && (c.Code == pCatalogCode || pCatalogCode == "")
                            && (c.Name.Contains(pCatalogName.Trim()) || pCatalogName.Trim() == ""))
                        .ToList());
                }

                return mCatalogList;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ProductDataServiceDS -> GetCatalogListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pCompanyParam"></param>
		/// <param name="pModelCode"></param>
		/// <param name="pLogInst"></param>
		/// <returns></returns>
        public List<SAPLogs> GetSAPModelLogs(CompanyConn pCompanyParam, string pModelCode = "", string pLogInst = "")
        {
            string mSqlQuery;
            List<ARGNSModel> mRet = null;
            List<ARGNSModel> mretAux = null;
            List<SAPLogs> mResult = null;
            try
            {
                UserDS pUserServ = new UserDS();
                ArrayList mInstList = new ArrayList(pLogInst.Split(',').ToArray());
                mResult = new List<SAPLogs>();

                if (mInstList.Count > 1)
                {

                    mSqlQuery = "SELECT T0.*  FROM [@AARGNS_MODEL] T0 WHERE T0.[LogInst] = " + mInstList[0] + "  AND  T0.[Code] = '" + pModelCode + "'  AND  T0.[Object] = 'ARGNS_MODEL'";
                    mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(pUserServ.GetQueryResult(pCompanyParam, mSqlQuery));

                    mSqlQuery = "SELECT T0.*  FROM [@AARGNS_MODEL] T0 WHERE T0.[LogInst] = " + mInstList[1] + "  AND  T0.[Code] = '" + pModelCode + "'  AND  T0.[Object] = 'ARGNS_MODEL'";
                    mretAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(pUserServ.GetQueryResult(pCompanyParam, mSqlQuery));

                    Compare(mRet.SingleOrDefault(), mretAux.SingleOrDefault(), Convert.ToInt32(mInstList[1]), ref mResult);
                }
                else
                {
                    mSqlQuery = "SELECT T0.*  FROM [@AARGNS_MODEL] T0 WHERE T0.[LogInst] >= " + mInstList[0] + "  AND  T0.[Code] = '" + pModelCode + "'  AND  T0.[Object] = 'ARGNS_MODEL' ORDER BY T0.[LogInst] ASC";
                    mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ARGNSModel>>(pUserServ.GetQueryResult(pCompanyParam, mSqlQuery));

                    int mCount = Convert.ToInt32(mInstList[0]);


                    for (int i = 0; i < mRet.Count - 1; i++)
                    {
                        mCount++;

                        if (i < mRet.Count - 1)
                        {
                            Compare(mRet[i], mRet[i + 1], mCount, ref mResult);
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("MasterDataServiceDS -> GetSAPModelLogs :" + ex.Message);
                mResult = null;
            }

            return mResult;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="Object1"></param>
		/// <param name="object2"></param>
		/// <param name="pNum"></param>
		/// <param name="mResult"></param>
        static void Compare<T>(T Object1, T object2, int pNum, ref List<SAPLogs> mResult)
        {
            //Get the type of the object
            Type type = typeof(T);
            SAPLogs mLog;

            ////return false if any of the object is false
            //if (Object1 == null || object2 == null)
            //    return null;

            //Loop through each properties inside class and get values for the property from both the objects and compare
            foreach (System.Reflection.PropertyInfo property in type.GetProperties())
            {
                if (property.Name != "ExtensionData")
                {
                    string Object1Value = string.Empty;
                    string Object2Value = string.Empty;

                    if (type.GetProperty(property.Name).GetValue(Object1, null) != null)
                        Object1Value = type.GetProperty(property.Name).GetValue(Object1, null).ToString();

                    if (type.GetProperty(property.Name).GetValue(object2, null) != null)
                        Object2Value = type.GetProperty(property.Name).GetValue(object2, null).ToString();

                    if (Object1Value.Trim() != Object2Value.Trim())
                    {
                        mLog = new SAPLogs();
                        mLog.Id = pNum;
                        mLog.ChangedField = property.Name;
                        mLog.OldValue = Object1Value.Trim();
                        mLog.NewValue = Object2Value.Trim();

                        mResult.Add(mLog);
                    }
                }
            }

        }
    }
}