﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebServices.MasterData.DataProcess
{
    public class CostSheetProcess
    {
        public string UpdateCostSheet(ARGNSCostSheet pCostSheet, CompanyConn pCompanyParam)
        {
            try
            {
                B1IFApparelService.UpdateCostSheetType request = new B1IFApparelService.UpdateCostSheetType();
                B1IFApparelService.UpdateCostSheetResponseType response = new B1IFApparelService.UpdateCostSheetResponseType();
                B1IFApparelService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFApparelService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();


                B1IFApparelService.UpdateCostSheetTypeXmlToUpdate mCostSheetObj = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdate();
                mCostSheetObj.ArgnsCostSheet = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdateArgnsCostSheet();
                mCostSheetObj.ArgnsCostSheet.Code = pCostSheet.Code;

                if (pCostSheet.ListMaterial.Count > 0)
                {
                    mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdateArgnsCostSheetARGNS_CS_MATERIALS[pCostSheet.ListMaterial.Count];
                    int counter = 0;
                    foreach (ARGNSCSMaterial material in pCostSheet.ListMaterial)
                    {
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter] = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdateArgnsCostSheetARGNS_CS_MATERIALS();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].Code = material.Code;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].LineId = material.LineId;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].LineIdSpecified = true;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].Object = material.Object;
                        if(material.LogInst.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].LogInst = material.LogInst.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_ItemCode = material.U_ItemCode;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_ItemName = material.U_ItemName;
                        if (material.U_Quantity.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_Quantity = material.U_Quantity.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_UoM = material.U_UoM;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_Whse = material.U_Whse;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_IssueMth = material.U_IssueMth;
                        if (material.U_PList.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_PList = material.U_PList.ToString();
                        if (material.U_UPrice.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_UPrice = material.U_UPrice.ToString();
                        if (material.U_Total.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_Total = material.U_Total.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_Comments = material.U_Comments;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_TreeType = material.U_TreeType;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_PVendor = material.U_PVendor;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_FCode = material.U_FCode;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_OcrCode = material.U_OcrCode;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_Currency = material.U_Currency;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_ItmLinkO = material.U_ItmLinkO;
                        if (material.U_QtyTime.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_QtyTime = material.U_QtyTime.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_CxT = material.U_CxT;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_CxS = material.U_CxS;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_CxC = material.U_CxC;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_MATERIALSCollection[counter].U_CxGT = material.U_CxGT;
                        counter++;
                    }
                }

                if (pCostSheet.ListOperation.Count > 0)
                {
                    mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdateArgnsCostSheetARGNS_CS_OPERATIONS[pCostSheet.ListOperation.Count];
                    int counter = 0;
                    foreach (ARGNSCSOperation operation in pCostSheet.ListOperation)
                    {
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter] = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdateArgnsCostSheetARGNS_CS_OPERATIONS();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].Code = operation.Code;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].LineId = operation.LineId;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].LineIdSpecified = true;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].Object = operation.Object;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].LogInst = operation.LogInst.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_ItemCode = operation.U_ItemCode;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_ItemName = operation.U_ItemName;
                        if (operation.U_Quantity.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_Quantity = operation.U_Quantity.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_UoM = operation.U_UoM;
                        if (operation.U_PList.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_PList = operation.U_PList.ToString();
                        if (operation.U_Price.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_Price = operation.U_Price.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_Currency = operation.U_Currency;
                        if (operation.U_TRAPlatz.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_TRAPlatz = operation.U_TRAPlatz.ToString();
                        if (operation.U_TR2APlat.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_TR2APlat = operation.U_TR2APlat.ToString();
                        if (operation.U_TEAPlatz.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_TEAPlatz = operation.U_TEAPlatz.ToString();
                        if (operation.U_menge_je.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_menge_je = operation.U_menge_je.ToString();
                        if (operation.U_menge_ze.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_menge_ze = operation.U_menge_ze.ToString();
                        if (operation.U_nutzen.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_nutzen = operation.U_nutzen.ToString();
                        if (operation.U_anzahl.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_anzahl = operation.U_anzahl.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_bde = operation.U_bde;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_aplatz_i = operation.U_aplatz_i;
                        if (operation.U_SAM.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_SAM = operation.U_SAM.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_ResCode = operation.U_ResCode;
                        if (operation.U_LeadTime.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_LeadTime = operation.U_LeadTime.ToString();
                        if (operation.U_Seconds.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_OPERATIONSCollection[counter].U_Seconds = operation.U_Seconds.ToString();
                        counter++;
                    }

                }

                if (pCostSheet.ListSchema.Count > 0)
                {
                    mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdateArgnsCostSheetARGNS_CS_SCHEMAS[pCostSheet.ListSchema.Count];
                    int counter = 0;
                    foreach (ARGNSCSSchema schema in pCostSheet.ListSchema)
                    {
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter] = new B1IFApparelService.UpdateCostSheetTypeXmlToUpdateArgnsCostSheetARGNS_CS_SCHEMAS();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].Code = schema.Code;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].LineId = schema.LineId;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].LineIdSpecified = true;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].Object = schema.Object;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].LogInst = schema.LogInst.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_SchCode = schema.U_SchCode;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_ItemCode = schema.U_ItemCode;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_ItemName = schema.U_ItemName;
                        if (schema.U_Percent.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_Percent = schema.U_Percent.ToString();
                        if (schema.U_Amount.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_Amount = schema.U_Amount.ToString();
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_AplicTo = schema.U_AplicTo;
                        mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_Currency = schema.U_Currency;
                        if (schema.U_PList.HasValue)
                            mCostSheetObj.ArgnsCostSheet.ARGNS_CS_SCHEMASCollection[counter].U_PList = schema.U_PList.ToString();
                        counter++;
                    }
                }
                request.XmlToUpdate = mCostSheetObj;
                request.Code = pCostSheet.Code;
                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSUpdateCostSheet(request);

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }
    }
}