﻿using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebServices.MasterData.DataProcess
{
    public class PomMasterProcess
    {

        //private string mXmlFile;
        //private XmlDocument mXmlDocument;

        //#region DI Server

        ////Agregar y Actualizar BP
        //public string AddUpdatePomMaster(ARGNSModelPom pObject, string pSession, bool pUpdate, string pUser)
        //{
        //    string mXmlResult = string.Empty;
        //    string ret = string.Empty;
        //    string pXmlString = string.Empty;
        //    string AddCmd = string.Empty;
        //    SBODI_Server.Node mServerNode = null;
        //    XmlDocument mResultXML = null, pXML = null;
        //    int mTransType;
        //    try
        //    {
        //        if (SaveXMLFile(pObject, out pXmlString, pUser))
        //        {
        //            mResultXML = new XmlDocument();
        //            mServerNode = new SBODI_Server.Node();

        //            //Leo el XML File Generado
        //            pXML = new System.Xml.XmlDocument();
        //            pXML.LoadXml(pXmlString);

        //            //Cambio y Elimino los Nodos para hacerlo Compatible con DI Server                   
        //            pXML.RemoveChild(pXML.FirstChild);

        //            //Armo la cabecera del XML
        //            mTransType = pUpdate ? (int)UtilWeb.TransactionType.Update : (int)UtilWeb.TransactionType.Add;
        //            AddCmd = UtilWeb.GetHeader(pSession, pXML, mTransType, "ARGNS_MODELPOM");

        //            //Envio Solicitud al DI Server y Obtengo la Respuesta
        //            mXmlResult = mServerNode.Interact(AddCmd);
        //            mResultXML.LoadXml(mXmlResult);
        //        }

        //        //Recupero los Valores de la Respuesta                
        //        return UtilWeb.GetResult(mResultXML); ;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error:" + ex.Message;
        //    }
        //}


        //#endregion


        //#region DI API

        //public string AddUpdatePomMaster(ARGNSModelPom pObject, string pUser, bool pUpdate, SapConnection mSapConn)
        //{
        //    string ret = string.Empty;
        //    string mXmlDoc;
        //    SAPbobsCOM.GeneralService oGeneralService = null;
        //    SAPbobsCOM.GeneralData oGeneralData = null;
        //    SAPbobsCOM.CompanyService oCompanyService = null;

        //    try
        //    {
        //        if (SaveXMLFile(pObject, out mXmlDoc, pUser))
        //        {

        //            //Obtengo el Servicio
        //            oCompanyService = mSapConn.oCompaniaSAP.GetCompanyService();
        //            //Obtengo la Tabla
        //            oGeneralService = oCompanyService.GetGeneralService("ARGNS_MODELPOM");
        //            //Create data for new row in main UDO
        //            oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);
        //            //Export Import File
        //            mSapConn.oCompaniaSAP.XmlExportType = SAPbobsCOM.BoXmlExportTypes.xet_ExportImportMode;
        //            //Leo XML
        //            oGeneralData.FromXMLFile(AppDomain.CurrentDomain.BaseDirectory + "\\PomMaster" + pUser + ".xml");

        //            //Guardo Modelo
        //            if (pUpdate)
        //            {
        //                oGeneralService.Update(oGeneralData);
        //                ret = pObject.Code;
        //            }
        //            else
        //            {
        //                oGeneralService.Add(oGeneralData);
        //                mSapConn.oCompaniaSAP.GetNewObjectCode(out ret);
        //            }
        //        }

        //        return ret;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error:" + ex.Message;
        //    }
        //    finally
        //    {
        //        Utils.ReleaseObject((object)oGeneralService);
        //        Utils.ReleaseObject((object)oGeneralData);
        //        Utils.ReleaseObject((object)oCompanyService);
        //    }
        //}


        //#endregion

        //private bool SaveXMLFile(ARGNSModelPom pObject, out string pXmlFile, string pUser)
        //{
        //    XmlNode xmlRow;
        //    bool ret = false;         
        //    try
        //    {
        //        mXmlDocument = new XmlDocument();
        //        mXmlFile = "<?xml version='1.0' encoding='UTF-16'?><ArgnsPom></ArgnsPom>";
        //        mXmlDocument.LoadXml(mXmlFile);


        //        #region Header

        //        xmlRow = mXmlDocument.SelectSingleNode("ArgnsPom");

        //        //Code                
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("Code")).InnerText = pObject.Code;
        //        //LineId                
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("LineId")).InnerText = pObject.LineId.ToString();
        //        //Object                
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("Object")).InnerText = "ARGNS_MODELPOM";
        //        //U_Desc
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_Desc")).InnerText = pObject.U_Desc;
        //        //U_SizeCode
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_SizeCode")).InnerText = pObject.U_SizeCode;
        //        //U_SizeDesc
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_SizeDesc")).InnerText = pObject.U_SizeDesc;
        //        //U_Value
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_Value")).InnerText = pObject.U_Value.ToString();
        //        //U_TolPosit
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_TolPosit")).InnerText = pObject.U_TolPosit.ToString();
        //        //U_TolNeg
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_TolNeg")).InnerText = pObject.U_TolNeg.ToString();
        //        //U_QAPoint
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_QAPoint")).InnerText = pObject.U_QAPoint;
        //        //U_POM
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("U_POM")).InnerText = pObject.U_POM;


        //        #endregion


        //        //Grabo el XML File
        //        mXmlDocument.Save(AppDomain.CurrentDomain.BaseDirectory + "\\PomMaster" + pUser + ".xml");
        //        pXmlFile = mXmlDocument.InnerXml;
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        pXmlFile = string.Empty;
        //        return ret;
        //    }
        //}
    }
}