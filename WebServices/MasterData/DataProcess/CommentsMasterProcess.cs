﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebServices.MasterData
{
    public class CommentsMasterProcess
    {
        public string AddComment(ARGNSModelComment pObject, CompanyConn pCompanyParam)
        {
            try
            {
                B1IFApparelService.AddCommentsType request = new B1IFApparelService.AddCommentsType();
                B1IFApparelService.AddCommentsResponseType response = new B1IFApparelService.AddCommentsResponseType();
                B1IFApparelService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFApparelService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();


                B1IFApparelService.AddCommentsTypeXmlToAdd CommentsObj = new B1IFApparelService.AddCommentsTypeXmlToAdd();
                CommentsObj.ArgnsTableCommentlogModel = new B1IFApparelService.AddCommentsTypeXmlToAddArgnsTableCommentlogModel();

                CommentsObj.ArgnsTableCommentlogModel.Code = pObject.Code;
                CommentsObj.ArgnsTableCommentlogModel.Name = pObject.Name;
                CommentsObj.ArgnsTableCommentlogModel.U_COMMENT = pObject.U_COMMENT;
                CommentsObj.ArgnsTableCommentlogModel.U_DATE = System.DateTime.Now.ToString("yyyy-MM-dd");
                if (pObject.U_FILE != null && pObject.U_FILE != "")
                    CommentsObj.ArgnsTableCommentlogModel.U_FILE = pObject.U_FILE;
                else
                    CommentsObj.ArgnsTableCommentlogModel.U_FILE = " ";

                if (pObject.U_FILEPATH != null && pObject.U_FILEPATH != "")
                    CommentsObj.ArgnsTableCommentlogModel.U_FILEPATH = pObject.U_FILEPATH;
                else
                    CommentsObj.ArgnsTableCommentlogModel.U_FILEPATH = " ";
                CommentsObj.ArgnsTableCommentlogModel.U_MODCODE = pObject.U_MODCODE;
                CommentsObj.ArgnsTableCommentlogModel.U_USER = pObject.U_USER;

                request.XmlToAdd = CommentsObj;
                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSAddComments(request);

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }


        }

        public string UpdateComment(ARGNSModelComment pObject, CompanyConn pCompanyParam)
        {
            try
            {
                B1IFApparelService.UpdateCommentsType request = new B1IFApparelService.UpdateCommentsType();
                B1IFApparelService.UpdateCommentsResponseType response = new B1IFApparelService.UpdateCommentsResponseType();
                B1IFApparelService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFApparelService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();


                B1IFApparelService.UpdateCommentsTypeXmlToUpdate CommentsObj = new B1IFApparelService.UpdateCommentsTypeXmlToUpdate();
                CommentsObj.ArgnsTableCommentlogModel = new B1IFApparelService.UpdateCommentsTypeXmlToUpdateArgnsTableCommentlogModel();

                CommentsObj.ArgnsTableCommentlogModel.Code = pObject.Code;
                CommentsObj.ArgnsTableCommentlogModel.Name = pObject.Name;
                CommentsObj.ArgnsTableCommentlogModel.U_COMMENT = pObject.U_COMMENT;
                CommentsObj.ArgnsTableCommentlogModel.U_DATE = System.DateTime.Now.ToString("yyyy-MM-dd");
                CommentsObj.ArgnsTableCommentlogModel.U_FILE = pObject.U_FILE;
                CommentsObj.ArgnsTableCommentlogModel.U_FILEPATH = pObject.U_FILEPATH;
                CommentsObj.ArgnsTableCommentlogModel.U_MODCODE = pObject.U_MODCODE;
                CommentsObj.ArgnsTableCommentlogModel.U_USER = pObject.U_USER;

                request.XmlToUpdate = CommentsObj;
                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSUpdateComments(request);
                
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }

        }
    }
}