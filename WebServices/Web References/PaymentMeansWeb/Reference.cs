﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace WebServices.PaymentMeansWeb {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="ARGNS_POPAYMENTMEASoap", Namespace="ARGNS_POPAYMENTMEA")]
    public partial class ARGNS_POPAYMENTMEA : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private MsgHeader msgHeaderValueField;
        
        private System.Threading.SendOrPostCallback AddOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetByParamsOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetListOperationCompleted;
        
        private System.Threading.SendOrPostCallback InvokeMethodOperationCompleted;
        
        private System.Threading.SendOrPostCallback RemoveOperationCompleted;
        
        private System.Threading.SendOrPostCallback CancelOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ARGNS_POPAYMENTMEA() {
            this.Url = global::WebServices.Properties.Settings.Default.WebServices_PaymentMeansWeb_ARGNS_POPAYMENTMEA;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public MsgHeader MsgHeaderValue {
            get {
                return this.msgHeaderValueField;
            }
            set {
                this.msgHeaderValueField = value;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event AddCompletedEventHandler AddCompleted;
        
        /// <remarks/>
        public event UpdateCompletedEventHandler UpdateCompleted;
        
        /// <remarks/>
        public event GetByParamsCompletedEventHandler GetByParamsCompleted;
        
        /// <remarks/>
        public event GetListCompletedEventHandler GetListCompleted;
        
        /// <remarks/>
        public event InvokeMethodCompletedEventHandler InvokeMethodCompleted;
        
        /// <remarks/>
        public event RemoveCompletedEventHandler RemoveCompleted;
        
        /// <remarks/>
        public event CancelCompletedEventHandler CancelCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MsgHeaderValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ARGNS_POPAYMENTMEA/Add", RequestNamespace="ARGNS_POPAYMENTMEA", ResponseNamespace="http://www.sap.com/SBO/DIS", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("ARGNS_POPAYMENTMEAParams")]
        public ARGNS_POPAYMENTMEAParams Add([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.sap.com/SBO/DIS")] ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA) {
            object[] results = this.Invoke("Add", new object[] {
                        ARGNS_POPAYMENTMEA});
            return ((ARGNS_POPAYMENTMEAParams)(results[0]));
        }
        
        /// <remarks/>
        public void AddAsync(ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA) {
            this.AddAsync(ARGNS_POPAYMENTMEA, null);
        }
        
        /// <remarks/>
        public void AddAsync(ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA, object userState) {
            if ((this.AddOperationCompleted == null)) {
                this.AddOperationCompleted = new System.Threading.SendOrPostCallback(this.OnAddOperationCompleted);
            }
            this.InvokeAsync("Add", new object[] {
                        ARGNS_POPAYMENTMEA}, this.AddOperationCompleted, userState);
        }
        
        private void OnAddOperationCompleted(object arg) {
            if ((this.AddCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.AddCompleted(this, new AddCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MsgHeaderValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ARGNS_POPAYMENTMEA/Update", RequestNamespace="ARGNS_POPAYMENTMEA", ResponseNamespace="http://www.sap.com/SBO/DIS", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void Update([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.sap.com/SBO/DIS")] ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA) {
            this.Invoke("Update", new object[] {
                        ARGNS_POPAYMENTMEA});
        }
        
        /// <remarks/>
        public void UpdateAsync(ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA) {
            this.UpdateAsync(ARGNS_POPAYMENTMEA, null);
        }
        
        /// <remarks/>
        public void UpdateAsync(ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA, object userState) {
            if ((this.UpdateOperationCompleted == null)) {
                this.UpdateOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateOperationCompleted);
            }
            this.InvokeAsync("Update", new object[] {
                        ARGNS_POPAYMENTMEA}, this.UpdateOperationCompleted, userState);
        }
        
        private void OnUpdateOperationCompleted(object arg) {
            if ((this.UpdateCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MsgHeaderValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ARGNS_POPAYMENTMEA/GetByParams", RequestNamespace="ARGNS_POPAYMENTMEA", ResponseNamespace="http://www.sap.com/SBO/DIS", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("ARGNS_POPAYMENTMEA")]
        public ARGNS_POPAYMENTMEA1 GetByParams([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.sap.com/SBO/DIS")] ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams) {
            object[] results = this.Invoke("GetByParams", new object[] {
                        ARGNS_POPAYMENTMEAParams});
            return ((ARGNS_POPAYMENTMEA1)(results[0]));
        }
        
        /// <remarks/>
        public void GetByParamsAsync(ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams) {
            this.GetByParamsAsync(ARGNS_POPAYMENTMEAParams, null);
        }
        
        /// <remarks/>
        public void GetByParamsAsync(ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams, object userState) {
            if ((this.GetByParamsOperationCompleted == null)) {
                this.GetByParamsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetByParamsOperationCompleted);
            }
            this.InvokeAsync("GetByParams", new object[] {
                        ARGNS_POPAYMENTMEAParams}, this.GetByParamsOperationCompleted, userState);
        }
        
        private void OnGetByParamsOperationCompleted(object arg) {
            if ((this.GetByParamsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetByParamsCompleted(this, new GetByParamsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MsgHeaderValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ARGNS_POPAYMENTMEA/GetList", RequestNamespace="ARGNS_POPAYMENTMEA", ResponseNamespace="http://www.sap.com/SBO/DIS", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlArrayAttribute("ARGNS_POPAYMENTMEAsParams")]
        [return: System.Xml.Serialization.XmlArrayItemAttribute("ARGNS_POPAYMENTMEAParams", IsNullable=false)]
        public ARGNS_POPAYMENTMEAsParamsARGNS_POPAYMENTMEAParams[] GetList() {
            object[] results = this.Invoke("GetList", new object[0]);
            return ((ARGNS_POPAYMENTMEAsParamsARGNS_POPAYMENTMEAParams[])(results[0]));
        }
        
        /// <remarks/>
        public void GetListAsync() {
            this.GetListAsync(null);
        }
        
        /// <remarks/>
        public void GetListAsync(object userState) {
            if ((this.GetListOperationCompleted == null)) {
                this.GetListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetListOperationCompleted);
            }
            this.InvokeAsync("GetList", new object[0], this.GetListOperationCompleted, userState);
        }
        
        private void OnGetListOperationCompleted(object arg) {
            if ((this.GetListCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetListCompleted(this, new GetListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MsgHeaderValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ARGNS_POPAYMENTMEA/InvokeMethod", RequestNamespace="ARGNS_POPAYMENTMEA", ResponseNamespace="http://www.sap.com/SBO/DIS", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void InvokeMethod([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.sap.com/SBO/DIS")] ref InvokeParams InvokeParams, [System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.sap.com/SBO/DIS")] ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA) {
            object[] results = this.Invoke("InvokeMethod", new object[] {
                        InvokeParams,
                        ARGNS_POPAYMENTMEA});
            InvokeParams = ((InvokeParams)(results[0]));
        }
        
        /// <remarks/>
        public void InvokeMethodAsync(InvokeParams InvokeParams, ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA) {
            this.InvokeMethodAsync(InvokeParams, ARGNS_POPAYMENTMEA, null);
        }
        
        /// <remarks/>
        public void InvokeMethodAsync(InvokeParams InvokeParams, ARGNS_POPAYMENTMEA1 ARGNS_POPAYMENTMEA, object userState) {
            if ((this.InvokeMethodOperationCompleted == null)) {
                this.InvokeMethodOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInvokeMethodOperationCompleted);
            }
            this.InvokeAsync("InvokeMethod", new object[] {
                        InvokeParams,
                        ARGNS_POPAYMENTMEA}, this.InvokeMethodOperationCompleted, userState);
        }
        
        private void OnInvokeMethodOperationCompleted(object arg) {
            if ((this.InvokeMethodCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InvokeMethodCompleted(this, new InvokeMethodCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MsgHeaderValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ARGNS_POPAYMENTMEA/Remove", RequestNamespace="ARGNS_POPAYMENTMEA", ResponseNamespace="http://www.sap.com/SBO/DIS", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void Remove([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.sap.com/SBO/DIS")] ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams) {
            this.Invoke("Remove", new object[] {
                        ARGNS_POPAYMENTMEAParams});
        }
        
        /// <remarks/>
        public void RemoveAsync(ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams) {
            this.RemoveAsync(ARGNS_POPAYMENTMEAParams, null);
        }
        
        /// <remarks/>
        public void RemoveAsync(ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams, object userState) {
            if ((this.RemoveOperationCompleted == null)) {
                this.RemoveOperationCompleted = new System.Threading.SendOrPostCallback(this.OnRemoveOperationCompleted);
            }
            this.InvokeAsync("Remove", new object[] {
                        ARGNS_POPAYMENTMEAParams}, this.RemoveOperationCompleted, userState);
        }
        
        private void OnRemoveOperationCompleted(object arg) {
            if ((this.RemoveCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.RemoveCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("MsgHeaderValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("ARGNS_POPAYMENTMEA/Cancel", RequestNamespace="ARGNS_POPAYMENTMEA", ResponseNamespace="http://www.sap.com/SBO/DIS", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void Cancel([System.Xml.Serialization.XmlElementAttribute(Namespace="http://www.sap.com/SBO/DIS")] ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams) {
            this.Invoke("Cancel", new object[] {
                        ARGNS_POPAYMENTMEAParams});
        }
        
        /// <remarks/>
        public void CancelAsync(ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams) {
            this.CancelAsync(ARGNS_POPAYMENTMEAParams, null);
        }
        
        /// <remarks/>
        public void CancelAsync(ARGNS_POPAYMENTMEAParams ARGNS_POPAYMENTMEAParams, object userState) {
            if ((this.CancelOperationCompleted == null)) {
                this.CancelOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCancelOperationCompleted);
            }
            this.InvokeAsync("Cancel", new object[] {
                        ARGNS_POPAYMENTMEAParams}, this.CancelOperationCompleted, userState);
        }
        
        private void OnCancelOperationCompleted(object arg) {
            if ((this.CancelCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CancelCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public void CancelAsync1(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.sap.com/SBO/DIS")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://www.sap.com/SBO/DIS", IsNullable=false)]
    public partial class MsgHeader : System.Web.Services.Protocols.SoapHeader {
        
        private string sessionIDField;
        
        private MsgHeaderServiceName serviceNameField;
        
        private bool serviceNameFieldSpecified;
        
        /// <remarks/>
        public string SessionID {
            get {
                return this.sessionIDField;
            }
            set {
                this.sessionIDField = value;
            }
        }
        
        /// <remarks/>
        public MsgHeaderServiceName ServiceName {
            get {
                return this.serviceNameField;
            }
            set {
                this.serviceNameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ServiceNameSpecified {
            get {
                return this.serviceNameFieldSpecified;
            }
            set {
                this.serviceNameFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.sap.com/SBO/DIS")]
    public enum MsgHeaderServiceName {
        
        /// <remarks/>
        ARGNS_POPAYMENTMEA,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.sap.com/SBO/DIS")]
    public partial class ARGNS_POPAYMENTMEA1 {
        
        private string codeField;
        
        private string nameField;
        
        private long docEntryField;
        
        private bool docEntryFieldSpecified;
        
        private string canceledField;
        
        private string objectField;
        
        private long logInstField;
        
        private bool logInstFieldSpecified;
        
        private long userSignField;
        
        private bool userSignFieldSpecified;
        
        private string transferedField;
        
        private System.DateTime createDateField;
        
        private bool createDateFieldSpecified;
        
        private System.DateTime createTimeField;
        
        private bool createTimeFieldSpecified;
        
        private System.DateTime updateDateField;
        
        private bool updateDateFieldSpecified;
        
        private System.DateTime updateTimeField;
        
        private bool updateTimeFieldSpecified;
        
        private string dataSourceField;
        
        private double u_AmountRuleField;
        
        private bool u_AmountRuleFieldSpecified;
        
        private double u_SurchargeField;
        
        private bool u_SurchargeFieldSpecified;
        
        private long u_QuotasField;
        
        private bool u_QuotasFieldSpecified;
        
        private string u_BranchCodeField;
        
        /// <remarks/>
        public string Code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
        
        /// <remarks/>
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        public long DocEntry {
            get {
                return this.docEntryField;
            }
            set {
                this.docEntryField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DocEntrySpecified {
            get {
                return this.docEntryFieldSpecified;
            }
            set {
                this.docEntryFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string Canceled {
            get {
                return this.canceledField;
            }
            set {
                this.canceledField = value;
            }
        }
        
        /// <remarks/>
        public string Object {
            get {
                return this.objectField;
            }
            set {
                this.objectField = value;
            }
        }
        
        /// <remarks/>
        public long LogInst {
            get {
                return this.logInstField;
            }
            set {
                this.logInstField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LogInstSpecified {
            get {
                return this.logInstFieldSpecified;
            }
            set {
                this.logInstFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public long UserSign {
            get {
                return this.userSignField;
            }
            set {
                this.userSignField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UserSignSpecified {
            get {
                return this.userSignFieldSpecified;
            }
            set {
                this.userSignFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string Transfered {
            get {
                return this.transferedField;
            }
            set {
                this.transferedField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime CreateDate {
            get {
                return this.createDateField;
            }
            set {
                this.createDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreateDateSpecified {
            get {
                return this.createDateFieldSpecified;
            }
            set {
                this.createDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime CreateTime {
            get {
                return this.createTimeField;
            }
            set {
                this.createTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreateTimeSpecified {
            get {
                return this.createTimeFieldSpecified;
            }
            set {
                this.createTimeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime UpdateDate {
            get {
                return this.updateDateField;
            }
            set {
                this.updateDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UpdateDateSpecified {
            get {
                return this.updateDateFieldSpecified;
            }
            set {
                this.updateDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime UpdateTime {
            get {
                return this.updateTimeField;
            }
            set {
                this.updateTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UpdateTimeSpecified {
            get {
                return this.updateTimeFieldSpecified;
            }
            set {
                this.updateTimeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string DataSource {
            get {
                return this.dataSourceField;
            }
            set {
                this.dataSourceField = value;
            }
        }
        
        /// <remarks/>
        public double U_AmountRule {
            get {
                return this.u_AmountRuleField;
            }
            set {
                this.u_AmountRuleField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool U_AmountRuleSpecified {
            get {
                return this.u_AmountRuleFieldSpecified;
            }
            set {
                this.u_AmountRuleFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public double U_Surcharge {
            get {
                return this.u_SurchargeField;
            }
            set {
                this.u_SurchargeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool U_SurchargeSpecified {
            get {
                return this.u_SurchargeFieldSpecified;
            }
            set {
                this.u_SurchargeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public long U_Quotas {
            get {
                return this.u_QuotasField;
            }
            set {
                this.u_QuotasField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool U_QuotasSpecified {
            get {
                return this.u_QuotasFieldSpecified;
            }
            set {
                this.u_QuotasFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string U_BranchCode {
            get {
                return this.u_BranchCodeField;
            }
            set {
                this.u_BranchCodeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.sap.com/SBO/DIS")]
    public partial class ARGNS_POPAYMENTMEAParams {
        
        private string codeField;
        
        /// <remarks/>
        public string Code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.sap.com/SBO/DIS")]
    public partial class ARGNS_POPAYMENTMEAsParamsARGNS_POPAYMENTMEAParams {
        
        private string codeField;
        private string NameField;

        /// <remarks/>
        public string Code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
            }
        }

        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                this.NameField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3761.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://www.sap.com/SBO/DIS")]
    public partial class InvokeParams {
        
        private string valueField;
        
        /// <remarks/>
        public string Value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void AddCompletedEventHandler(object sender, AddCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class AddCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal AddCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ARGNS_POPAYMENTMEAParams Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ARGNS_POPAYMENTMEAParams)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void UpdateCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void GetByParamsCompletedEventHandler(object sender, GetByParamsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetByParamsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetByParamsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ARGNS_POPAYMENTMEA1 Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ARGNS_POPAYMENTMEA1)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void GetListCompletedEventHandler(object sender, GetListCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ARGNS_POPAYMENTMEAsParamsARGNS_POPAYMENTMEAParams[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ARGNS_POPAYMENTMEAsParamsARGNS_POPAYMENTMEAParams[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void InvokeMethodCompletedEventHandler(object sender, InvokeMethodCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InvokeMethodCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal InvokeMethodCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public InvokeParams InvokeParams {
            get {
                this.RaiseExceptionIfNecessary();
                return ((InvokeParams)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void RemoveCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3761.0")]
    public delegate void CancelCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
}

#pragma warning restore 1591