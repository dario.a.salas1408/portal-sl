﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebServices.BusinessPartner
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBPService" in both code and config file together.
    [ServiceContract]
    public interface IBPService
    {

        [OperationContract]
        List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCompanyParam);

        [OperationContract]
        List<BusinessPartnerSAP> GetBusinessPartnerListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pNameBP = "", string pTypeBP = "");

        [OperationContract]
        BusinessPartnerSAP GetBusinessPartnerById(CompanyConn pCompanyParam, string pCode, List<UDF_ARGNS> pListUDFCRD1 = null, List<UDF_ARGNS> pListUDFs = null);

        [OperationContract]
        List<BusinessPartnerSAP> GetBusinessPartnerListByType(CompanyConn pCompanyParam, Enums.BpType pType);

        [OperationContract]
        List<StateSAP> GetStateList(CompanyConn pCompanyParam, string pCountry);

        [OperationContract]
        JsonObjectResult GetCustomerAgingReport(CompanyConn pCompanyParam, List<string> pBP = null);

        [OperationContract]
        string AddBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam);

        [OperationContract]
        JsonObjectResult GetBusinessPartnerListByUser(CompanyConn pCompanyParam, 
            Enums.BpType pType, bool pOnlyActiveBPs,  string pCodeBP = "", string pNameBP = "", 
            bool FilterByUser = false, int? pSECode = null, 
            string[] pBPGroupCardCodeList = null, bool pGetLeads = false, 
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null, bool pFilterByBP = false, string licTradNum = "");

        [OperationContract]
        BusinessPartnerSAP GetBusinessPartnerMinimalData(CompanyConn pCompanyParam, string pCardCode);

    }
}
