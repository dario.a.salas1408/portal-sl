﻿using System;
using System.Collections.Generic;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations;
using ARGNS.Util;
using ARGNS.Model.Implementations.View;

namespace WebServices.BusinessPartner
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BPService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BPService.svc or BPService.svc.cs at the Solution Explorer and start debugging.
    public class BPService : IBPService
    {
        private IBPService mBPService = null;

        public BPService()
        {
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        private void InitializeService(CompanyConn pCompanyParam)
        {
            if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
            {
                mBPService = new BPProcessSL();
            }
            else
            {
                mBPService = new BPProcessDS();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetBusinessPartnerList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetBusinessPartnerList :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pListUDFCRD1"></param>
        /// <returns></returns>
        public BusinessPartnerSAP GetBusinessPartnerById(CompanyConn pCompanyParam, string pCode, List<UDF_ARGNS> pListUDFCRD1 = null, List<UDF_ARGNS> pListUDFs = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetBusinessPartnerById(pCompanyParam, pCode, pListUDFCRD1, pListUDFs);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetBusinessPartnerById :" + ex.Message);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerListByType(CompanyConn pCompanyParam, Enums.BpType pType)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetBusinessPartnerListByType(pCompanyParam, pType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetBusinessPartnerListByType :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pTypeBP"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pNameBP = "", string pTypeBP = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetBusinessPartnerListSearch(pCompanyParam, pCodeBP, pNameBP, pTypeBP);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetBusinessPartnerListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCountry"></param>
        /// <returns></returns>
        public List<StateSAP> GetStateList(CompanyConn pCompanyParam, string pCountry)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetStateList(pCompanyParam, pCountry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetStateList :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBP"></param>
        /// <returns></returns>
        public JsonObjectResult GetCustomerAgingReport(CompanyConn pCompanyParam, List<string> pBP = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetCustomerAgingReport(pCompanyParam, pBP);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetCustomerAgingReport :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBP"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string AddBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.AddBusinessPartner(pBP, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> AddBusinessPartner :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBP"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.UpdateBusinessPartner(pBP, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> UpdateBusinessPartner :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pType"></param>
        /// <param name="pOnlyActiveBPs"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="FilterByUser"></param>
        /// <param name="pSECode"></param>
        /// <param name="pBPGroupCardCodeList"></param>
        /// <param name="pGetLeads"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetBusinessPartnerListByUser(CompanyConn pCompanyParam, 
            Enums.BpType pType, bool pOnlyActiveBPs, 
            string pCodeBP = "", string pNameBP = "", 
            bool FilterByUser = false, int? pSECode = null, 
            string[] pBPGroupCardCodeList = null, bool pGetLeads = false, 
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null, bool pFilterByBP = false, string licTradNum = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetBusinessPartnerListByUser(pCompanyParam, pType, pOnlyActiveBPs,
                    pCodeBP, pNameBP, FilterByUser, pSECode, pBPGroupCardCodeList, pGetLeads, pStart, pLength, pOrderColumn, pFilterByBP, licTradNum);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetBusinessPartnerListByUser :" + ex.Message);
                return null;
            }
        }

        public BusinessPartnerSAP GetBusinessPartnerMinimalData(CompanyConn pCompanyParam, string pCardCode)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mBPService.GetBusinessPartnerMinimalData(pCompanyParam, pCardCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPService -> GetBusinessPartnerMinimalData :" + ex.Message);
                return null;
            }
        }
    }
}
