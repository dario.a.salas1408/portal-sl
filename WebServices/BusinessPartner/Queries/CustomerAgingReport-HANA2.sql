insert into "{0}".TEMP_DUMMY_AGINGREPORT (
SELECT CASE T10."MONEDA" WHEN 'USD' THEN T10."MONEDA" ELSE '$' END AS DOCCUR, *
FROM(SELECT '' AS "SUCURSAL", IFNULL(T28."Name", T20."CardName") AS "AFILIACION", T29."ExtraDays" AS "DIASCRED", 
    T20."CreditLine" AS "LIMITECRED", 
    CASE T0."TransType" 
        WHEN '13' THEN 'FACTURA' 
        WHEN '14' THEN 'NOTA CREDITO' 
        WHEN '30' THEN 'POLIZA' 
        WHEN '-2' THEN 'SI' 
        WHEN '-3' THEN 'CB' 
        WHEN '24' THEN 'PAGO RECIBIDO' 
        WHEN '203' THEN 'ANTICIPO' 
        WHEN '46' THEN 'PAGO EFECTUADO' 
        ELSE CAST(T0."TransType" AS NVARCHAR) 
    END AS "TIPO", 
    CASE T0."TransType" 
        WHEN '13' THEN T22."DocDate" 
        WHEN '14' THEN T23."DocDate" 
        WHEN '30' THEN T0."RefDate" 
        WHEN '-2' THEN T0."RefDate" 
        WHEN '-3' THEN T0."RefDate" 
        WHEN '24' THEN T25."DocDate" 
        WHEN '203' THEN T24."DocDate" 
        WHEN '46' THEN T26."DocDate" 
    END AS "FECHACONT", 
    CASE T0."TransType" 
        WHEN '13' THEN T22."DocDueDate" 
        WHEN '14' THEN T23."DocDueDate" 
        WHEN '30' THEN T0."DueDate" 
        WHEN '-2' THEN T0."DueDate" 
        WHEN '-3' THEN T0."DueDate" 
        WHEN '24' THEN T25."DocDueDate" 
        WHEN '203' THEN T24."DocDueDate" 
        WHEN '46' THEN T26."DocDueDate" 
    END AS "FECHAVENC", T20."CardCode" AS "CODIGO", T20."CardName" AS "NOMBRE", 
    CASE T0."TransType" 
        WHEN '13' THEN T22."Comments" 
        WHEN '14' THEN T23."Comments" 
        WHEN '30' THEN T21."Memo"
        WHEN '-2' THEN T21."Memo"
        WHEN '-3' THEN T21."Memo" 
        WHEN '24' THEN T25."Comments" 
        WHEN '203' THEN T24."Comments" 
        WHEN '46' THEN T26."Comments" 
    END AS "COMENTARIOS", 
    CASE T0."TransType" 
        WHEN '13' THEN T22."DocNum" 
        WHEN '14' THEN T23."DocNum" 
        WHEN '30' THEN T21."TransId" 
        WHEN '-2' THEN T21."TransId" 
        WHEN '-3' THEN T21."TransId" 
        WHEN '24' THEN T25."DocNum" 
        WHEN '203' THEN T24."DocNum" 
        WHEN '46' THEN T26."DocNum"
    END AS "MOVSAP", 
    CASE T0."TransType" 
        WHEN '13' THEN 
        CASE T21."PTICode"
            WHEN '9005' THEN '0'
            ELSE CAST(T21."FolNumFrom" AS NVARCHAR) 
        END 
        WHEN '14' THEN CAST(T23."FolNumFrom" AS NVARCHAR) 
        WHEN '30' THEN CAST(T21."Ref1" AS NVARCHAR)
        WHEN '-2' THEN CAST(T21."Ref1" AS NVARCHAR)
        WHEN '-3' THEN CAST(T21."Ref1" AS NVARCHAR) 
        WHEN '24' THEN CAST(T25."CounterRef" AS NVARCHAR)
        WHEN '203' THEN CAST(T24."FolNumFrom" AS NVARCHAR) 
        WHEN '46' THEN CAST(T26."CounterRef" AS NVARCHAR) 
    END AS "DOCUMENTO", 
    CASE T0."TransType" 
        WHEN '13' THEN T22."DocCur"
        WHEN '14' THEN T23."DocCur" 
        WHEN '30' THEN T0."FCCurrency"
        WHEN '-2' THEN T0."FCCurrency"
        WHEN '-3' THEN T0."FCCurrency"
        WHEN '24' THEN T25."DocCurr"
        WHEN '203' THEN T24."DocCur"
        WHEN '46' THEN T26."DocCurr" 
    END AS "MONEDA", 
    CASE 
        WHEN T0."Debit" <> 0 THEN T0."Debit" - IFNULL(T11."ReconSum", 0) 
        ELSE IFNULL(T11."ReconSum", 0) - T0."Credit"
    END AS "SALDOMXP", 
    CASE 
        WHEN T0."Debit" <> 0 THEN T0."FCDebit" - IFNULL(T11."ReconSumFC", 0) 
        ELSE IFNULL(T11."ReconSumFC", 0) - T0."FCCredit" 
    END AS "SALDOUSD", 
    CASE T0."TransType" 
        WHEN '13' THEN 'AR Invoice' 
        WHEN '14' THEN 'AR Cred Memo' 
        WHEN '24' THEN 'Payment' 
        WHEN '203' THEN 'AR Down Payment' 
        ELSE 'Other' 
    END AS "TransTypeDesc", OINV."FolNumFrom" AS "FolNumFromOINV", OINV."FolNumTo" AS "FolNumToOINV", 
    OINV."DocSubType" AS "DocSubTypeOINV", ORIN."FolNumFrom" AS "FolNumFromORIN", ORIN."FolNumTo" AS "FolNumToORIN", 
    ODPI."FolNumFrom" AS "FolNumFromODPI", ODPI."FolNumTo" AS "FolNumToODPI", T0."TransType" AS "TransTypeNum" 
FROM "{0}".JDT1 T0 
    LEFT OUTER JOIN (SELECT T11."ShortName", T11."TransId", T11."SrcObjTyp", T11."TransRowId", SUM(T11."ReconSum") AS "ReconSum", SUM(T11."ReconSumFC") AS "ReconSumFC" 
					 FROM "{0}".ITR1 T11 INNER JOIN "{0}".OITR T12 ON T11."ReconNum" = T12."ReconNum" 
					 WHERE T12."ReconDate" <= CAST('/*DateToFilter*/' AS timestamp) 
					 GROUP BY T11."ShortName", T11."TransId", T11."SrcObjTyp", T11."TransRowId") AS T11
    ON T11."ShortName" = T0."ShortName" AND T11."TransId" = T0."TransId" AND T11."SrcObjTyp" = T0."TransType" AND T11."TransRowId" = T0."Line_ID" 
    INNER JOIN "{0}".OCRD T20 ON T20."CardCode" = T0."ShortName"
    INNER JOIN "{0}".OJDT T21 ON T0."TransId" = T21."TransId" 
    LEFT OUTER JOIN "{0}".OINV T22 ON T22."DocEntry" = T0."CreatedBy" AND T22."ObjType" = T0."TransType" 
    LEFT OUTER JOIN "{0}".ORIN T23 ON T23."DocEntry" = T0."CreatedBy" AND T23."ObjType" = T0."TransType" 
    LEFT OUTER JOIN "{0}".ODPI T24 ON T24."DocEntry" = T0."CreatedBy" AND T24."ObjType" = T0."TransType" 
    LEFT OUTER JOIN "{0}".ORCT T25 ON T25."DocEntry" = T0."CreatedBy" AND T25."ObjType" = T0."TransType" 
    LEFT OUTER JOIN "{0}".OVPM T26 ON T26."DocEntry" = T0."CreatedBy" AND T26."ObjType" = T0."TransType" 
    INNER JOIN "{0}".OSLP T27 ON T27."SlpCode" = T20."SlpCode" 
    LEFT OUTER JOIN "{0}".OIDC T28 ON T20."Indicator" = T28."Code" 
    LEFT OUTER JOIN "{0}".OCTG T29 ON T20."GroupNum" = T29."GroupNum" 
    LEFT OUTER JOIN "{0}".OINV ON OINV."DocEntry" = T0."SourceID" 
    LEFT OUTER JOIN "{0}".ORIN ON ORIN."DocEntry" = T0."SourceID" 
    LEFT OUTER JOIN "{0}".ODPI ON ODPI."DocEntry" = T0."SourceID" 
WHERE T20."CardType" = 'C' AND T0."RefDate" <= CAST('/*DateToFilter*/' AS timestamp) AND (T0."Debit" + T0."Credit" - IFNULL(T11."ReconSum", 0)) <> 0  /*CardCodeFilter*/ ) T10)