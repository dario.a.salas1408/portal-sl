﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using WebServices.Model.Tables;
using WebServices.User;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Text;
using System.Dynamic;
using Newtonsoft.Json;

namespace WebServices.BusinessPartner
{
    public class BPProcessSL : IBPService
    {
        public List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCompanyParam)
        {
            List<BusinessPartnerSAP> mListBusinessPartnerSAP = new List<BusinessPartnerSAP>();
            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP");
                mListBusinessPartnerSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                return mListBusinessPartnerSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetBusinessPartnerList :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pTypeBP"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerListSearch(CompanyConn pCompanyParam, string pCodeBP = "", string pNameBP = "", string pTypeBP = "")
        {
            List<BusinessPartnerSAP> mListBusinessPartnerSAP = new List<BusinessPartnerSAP>();
            string mUrl = string.Empty;
            try
            {
                string mFilters = "1 eq 1 ";
                if (!string.IsNullOrEmpty(pCodeBP))
                {
                    mFilters = mFilters + "and substringof(tolower('" + pCodeBP + "'), tolower(CardCode)) ";
                }
                if (!string.IsNullOrEmpty(pNameBP))
                {
                    mFilters = mFilters + "and substringof(tolower('" + pNameBP + "'), tolower(CardName))";
                }
                if (!string.IsNullOrEmpty(pTypeBP))
                {
                    mFilters = mFilters + "and substringof(tolower('" + pTypeBP + "'), tolower(CardType))";
                }
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                mListBusinessPartnerSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mListBusinessPartnerSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetBusinessPartnerListSearch :" + ex.Message);
                return new List<BusinessPartnerSAP>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pListUDFCRD1"></param>
        /// <returns></returns>
        public BusinessPartnerSAP GetBusinessPartnerById(CompanyConn pCompanyParam, string pCode, List<UDF_ARGNS> pListUDFCRD1 = null, List<UDF_ARGNS> pListUDFs = null)
        {
            BusinessPartnerSAP mBusinessPartnerSAP = new BusinessPartnerSAP();
            string mUrl = string.Empty;

            try
            {
                OCRD mOCRD = new OCRD();
                string mFilters;
                List<UFD1_SAP> mListUFD1FiscId = new List<UFD1_SAP>();
                UserSL pUserServ = new UserSL();
                string mQuery = string.Empty;
                string mSelect = "Select \"OCRD\".\"CardCode\", \"OCRD\".\"CardName\", \"OCRD\".\"CardType\", \"OCRD\".\"Address\", \"OCRD\".\"ZipCode\", \"OCRD\".\"MailAddres\", \"OCRD\".\"Currency\", " +
                                    "\"OCRD\".\"ShipToDef\", \"OCRD\".\"BillToDef\", \"OCRD\".\"ListNum\", \"OCRD\".\"ShipType\", \"OCRD\".\"Notes\", \"OCRD\".\"SlpCode\", \"OCRD\".\"QryGroup1\", " +
                                    "\"OCRD\".\"QryGroup2\", \"OCRD\".\"QryGroup3\", \"OCRD\".\"QryGroup4\", \"OCRD\".\"QryGroup5\", \"OCRD\".\"QryGroup6\", \"OCRD\".\"QryGroup7\", \"OCRD\".\"QryGroup8\", " +
                                    "\"OCRD\".\"QryGroup9\", \"OCRD\".\"QryGroup10\", \"OCRD\".\"QryGroup11\", \"OCRD\".\"QryGroup12\", \"OCRD\".\"QryGroup13\", \"OCRD\".\"QryGroup14\", \"OCRD\".\"QryGroup15\", " +
                                    "\"OCRD\".\"QryGroup16\", \"OCRD\".\"QryGroup17\", \"OCRD\".\"QryGroup18\", \"OCRD\".\"QryGroup19\", \"OCRD\".\"QryGroup20\", \"OCRD\".\"QryGroup21\", \"OCRD\".\"QryGroup22\", " +
                                    "\"OCRD\".\"QryGroup23\", \"OCRD\".\"QryGroup24\", \"OCRD\".\"QryGroup25\", \"OCRD\".\"QryGroup26\", \"OCRD\".\"QryGroup27\", \"OCRD\".\"QryGroup28\", \"OCRD\".\"QryGroup29\", " +
                                    "\"OCRD\".\"QryGroup30\", \"OCRD\".\"QryGroup31\", \"OCRD\".\"QryGroup32\", \"OCRD\".\"QryGroup33\", \"OCRD\".\"QryGroup34\", \"OCRD\".\"QryGroup35\", \"OCRD\".\"QryGroup36\", " +
                                    "\"OCRD\".\"QryGroup37\", \"OCRD\".\"QryGroup38\", \"OCRD\".\"QryGroup39\", \"OCRD\".\"QryGroup40\", \"OCRD\".\"QryGroup41\", \"OCRD\".\"QryGroup42\", \"OCRD\".\"QryGroup43\", " +
                                    "\"OCRD\".\"QryGroup44\", \"OCRD\".\"QryGroup45\", \"OCRD\".\"QryGroup46\", \"OCRD\".\"QryGroup47\", \"OCRD\".\"QryGroup48\", \"OCRD\".\"QryGroup49\", \"OCRD\".\"QryGroup50\", " +
                                    "\"OCRD\".\"QryGroup51\", \"OCRD\".\"QryGroup52\", \"OCRD\".\"QryGroup53\", \"OCRD\".\"QryGroup54\", \"OCRD\".\"QryGroup55\", \"OCRD\".\"QryGroup56\", \"OCRD\".\"QryGroup57\", " +
                                    "\"OCRD\".\"QryGroup58\", \"OCRD\".\"QryGroup59\", \"OCRD\".\"QryGroup60\", \"OCRD\".\"QryGroup61\", \"OCRD\".\"QryGroup62\", \"OCRD\".\"QryGroup63\", \"OCRD\".\"QryGroup64\", " +
                                    "\"OCRD\".\"Balance\", \"OCRD\".\"DNotesBal\", \"OCRD\".\"OrdersBal\", \"OCRD\".\"OprCount\", \"OCRD\".\"LicTradNum\", \"OCRD\".\"GroupNum\", " +
                                    "\"OCRD\".\"PymCode\", \"OCRD\".\"VatStatus\", \"OCRD\".\"Discount\", \"OCRD\".\"Phone1\", \"OCRD\".\"Phone2\", \"OCRD\".\"Fax\", \"OCRD\".\"Cellular\", \"OCRD\".\"E_Mail\", \"OCRD\".\"IntrntSite\" ";
                string mWhere = " WHERE \"OCRD\".\"CardCode\" = '" + pCode + "' ";
                string mFrom = " FROM  \"{0}\".\"OCRD\" ";

                if (pCompanyParam.CompanySAPConfig.DocumentSettingsSAP.LawsSet == "AR")
                {
                    mSelect += ", \"OCRD\".\"U_B1SYS_FiscIdType\" ";

                    string mSelectAux = "SELECT * FROM \"{0}\".\"UFD1\" WHERE \"UFD1\".\"FieldID\" IN ( SELECT \"FieldID\" FROM \"{0}\".\"CUFD\" WHERE \"CUFD\".\"AliasID\" = 'B1SYS_FiscIdType' AND \"CUFD\".\"TableID\" = 'OCRD') AND \"UFD1\".\"TableID\" = 'OCRD'";
                    mSelectAux = String.Format(mSelectAux, pCompanyParam.CompanyDB);
                    mListUFD1FiscId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(pUserServ.GetQueryResult(pCompanyParam, mSelectAux)).ToList();
                }

                mQuery = SQLQueryUtil.getSelectQuery2(mSelect, mFrom, null, mWhere, null, null, "HANA");
                mQuery = String.Format(mQuery, pCompanyParam.CompanyDB);
                mBusinessPartnerSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(pUserServ.GetQueryResult(pCompanyParam, mQuery)).FirstOrDefault();

                if (mBusinessPartnerSAP != null)
                {
                    mFilters = "CardCode eq '" + pCode + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BPAddresses", mFilters);
                    string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    mBusinessPartnerSAP.Addresses = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BPAddressesSAP>>(mResultJson).ToList();
                    for (int i = 0; i < mBusinessPartnerSAP.Addresses.Count(); i++)
                    {
                        if (mBusinessPartnerSAP.Addresses[i] != null)
                        {
                            mFilters = "TableID eq 'CRD1'";
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                            List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                            Newtonsoft.Json.Linq.JArray mJArrayCRD1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                            mBusinessPartnerSAP.Addresses[i].MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFCRD1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayCRD1[i]);
                        }
                    }

                    mBusinessPartnerSAP.PaymentMethodList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMethod>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CRD2", "CardCode eq '" + mBusinessPartnerSAP.CardCode + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }
                else
                {
                    mBusinessPartnerSAP = new BusinessPartnerSAP();
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BPProperties");
                mBusinessPartnerSAP.PropertiesList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BPPropertiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Currency");
                mBusinessPartnerSAP.CurrencyList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrencySAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType");
                mBusinessPartnerSAP.ShipTypeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ShippingType>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PriceList");
                mBusinessPartnerSAP.PriceList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PriceListSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee");
                mBusinessPartnerSAP.EmployeeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesEmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Country");
                mBusinessPartnerSAP.CountryList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CountrySAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mFilters = "CardCode eq '" + HttpUtility.UrlEncode(pCode) + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ContacPerson", mFilters);
                mBusinessPartnerSAP.ListContact = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContacPerson>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OADM");
                List<OADM> mListOADM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OADM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mBusinessPartnerSAP.CompanyCountry = mListOADM.FirstOrDefault().Country;

                mBusinessPartnerSAP.BPCombo = this.GetCombo();
                mBusinessPartnerSAP.ListFiscIdValues = mListUFD1FiscId;

                if (pCode != "0")
                {

                    mWhere = "CardCode eq '" + HttpUtility.UrlEncode(pCode) + "'";

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mWhere);

                    string mResultJsonOCRD = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                    Newtonsoft.Json.Linq.JArray mJArrayOINV = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonOCRD);

                    List<UFD1_SAP> mLUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mBusinessPartnerSAP.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFs, mLUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOINV[0]);

                }
                else
                {
                    mBusinessPartnerSAP.MappedUdf = pListUDFs;
                }


                return mBusinessPartnerSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetBusinessPartnerById :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerListByType(CompanyConn pCompanyParam, Enums.BpType pType)
        {
            List<BusinessPartnerSAP> mListBusinessPartnerSAP = new List<BusinessPartnerSAP>();
            string mUrl = string.Empty;
            try
            {
                string mtype = pType.ToDescriptionString();
                string mFilters = "substringof(tolower('" + mtype + "'), tolower(CardType))";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                mListBusinessPartnerSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return mListBusinessPartnerSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetBusinessPartnerListByType :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCountry"></param>
        /// <returns></returns>
        public List<StateSAP> GetStateList(CompanyConn pCompanyParam, string pCountry)
        {
            string mUrl = string.Empty;
            try
            {
                string mFilters = "Country eq '" + pCountry + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "State", mFilters);
                List<StateSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StateSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return ListRestu;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetStateList :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBP"></param>
        /// <returns></returns>
        public JsonObjectResult GetCustomerAgingReport(CompanyConn pCompanyParam, List<string> pBP = null)
        {
            UserSL pUserServ = new UserSL();

            try
            {
                //Create the auxiliar table.
                string mQueryCAR = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\BusinessPartner\Queries\CustomerAgingReport-HANA1.sql");
                mQueryCAR = String.Format(mQueryCAR, pCompanyParam.CompanyDB);
                pUserServ.GetSAPQueryResult(pCompanyParam, mQueryCAR);

                //Insert data to auxiliar table
                mQueryCAR = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\BusinessPartner\Queries\CustomerAgingReport-HANA2.sql");
                mQueryCAR = mQueryCAR.Replace("/*DateToFilter*/", DateTime.Now.ToString("yyyy-MM-dd"));

                if (pBP != null)
                {
                    string mIn = "(";
                    for (int i = 0; i < pBP.Count; i++)
                    {
                        if (i == 0)
                            mIn += "'" + pBP[i] + "'";
                        else
                            mIn += ",'" + pBP[i] + "'";
                    }
                    mIn += ")";
                    mQueryCAR = mQueryCAR.Replace("/*CardCodeFilter*/", "AND T20.\"CardCode\" IN " + mIn);
                }
                mQueryCAR = String.Format(mQueryCAR, pCompanyParam.CompanyDB);
                pUserServ.GetSAPQueryResult(pCompanyParam, mQueryCAR);

                //Get data
                mQueryCAR = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\BusinessPartner\Queries\CustomerAgingReport-HANA3.sql");
                mQueryCAR = mQueryCAR.Replace("/*DateToFilter*/", DateTime.Now.ToString("yyyy-MM-dd"));

                if (pBP != null)
                {
                    string mIn = "(";
                    for (int i = 0; i < pBP.Count; i++)
                    {
                        if (i == 0)
                            mIn += "'" + pBP[i] + "'";
                        else
                            mIn += ",'" + pBP[i] + "'";
                    }
                    mIn += ")";
                    mQueryCAR = mQueryCAR.Replace("/*CardCodeFilter*/", "AND T20.\"CardCode\" IN " + mIn);
                }
                mQueryCAR = String.Format(mQueryCAR, pCompanyParam.CompanyDB);
                JsonObjectResult mResult = pUserServ.GetSAPQueryResult(pCompanyParam, mQueryCAR);

                //Remove auxiliar table
                mQueryCAR = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\BusinessPartner\Queries\CustomerAgingReport-HANA4.sql");
                mQueryCAR = String.Format(mQueryCAR, pCompanyParam.CompanyDB);
                pUserServ.GetSAPQueryResult(pCompanyParam, mQueryCAR);

                return mResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetCustomerAgingReport(Message) :" + ex.Message);
                Logger.WriteError("BPProcessSL -> GetCustomerAgingReport(InnerException) :" + ex.InnerException);

                //Remove auxiliar table in case of error
                string mQueryCAR = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\BusinessPartner\Queries\CustomerAgingReport-HANA4.sql");
                mQueryCAR = String.Format(mQueryCAR, pCompanyParam.CompanyDB);
                pUserServ.GetSAPQueryResult(pCompanyParam, mQueryCAR);

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBP"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string AddBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam)
        {
            //B1WSHandler mB1WSHandler = new B1WSHandler();
            BusinessPartnersServiceWeb.BusinessPartner oBP = new BusinessPartnersServiceWeb.BusinessPartner();
            //BusinessPartnersServiceWeb.BusinessPartnerParams oBPParams = new BusinessPartnersServiceWeb.BusinessPartnerParams();
            //BusinessPartnersServiceWeb.MsgHeader oBPMsgHeader = new BusinessPartnersServiceWeb.MsgHeader();

            //oBPMsgHeader.ServiceName = BusinessPartnersServiceWeb.MsgHeaderServiceName.BusinessPartnersService;
            //oBPMsgHeader.ServiceNameSpecified = true;
            //oBPMsgHeader.SessionID = pCompanyParam.DSSessionId;

            //BusinessPartnersServiceWeb.BusinessPartnersService oBPService = new BusinessPartnersServiceWeb.BusinessPartnersService();
            //oBPService.MsgHeaderValue = oBPMsgHeader;
            oBP.CardCode = pBP.CardCode;
            oBP.BPAddresses = new BusinessPartnersServiceWeb.BusinessPartnerBPAddress[pBP.Addresses.Count];
            try
            {
                int counter = 0;
                foreach (BPAddressesSAP address in pBP.Addresses)
                {
                    if (address.LineNum == -1 && (address.Address != null && address.Address != ""))
                        addNewAddress(address, oBP, counter);
                    counter++;
                }
                oBP.CardName = pBP.CardName;
                switch (pBP.CardType)
                {
                    case "C":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cCustomer;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "S":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cSupplier;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "L":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cLid;
                        oBP.CardTypeSpecified = true;
                        break;
                }
                oBP.MailAddress = pBP.MailAddres;
                oBP.Currency = pBP.Currency;
                oBP.Phone1 = pBP.Phone1;
                oBP.Phone2 = pBP.Phone2;
                oBP.Cellular = pBP.Cellular;
                oBP.Fax = pBP.Fax;
                oBP.EmailAddress = pBP.E_Mail;
                oBP.Website = pBP.IntrntSite;

                if (pBP.ShipType != null)
                {
                    oBP.ShippingType = (long)pBP.ShipType;
                    oBP.ShippingTypeSpecified = true;
                }

                if (pBP.SlpCode != null)
                {
                    oBP.SalesPersonCode = (long)pBP.SlpCode;
                    oBP.SalesPersonCodeSpecified = true;
                }

                if (pBP.ListNum != null)
                {
                    oBP.PriceListNum = (long)pBP.ListNum;
                    oBP.PriceListNumSpecified = true;
                }

                if (!string.IsNullOrEmpty(pBP.LicTradNum))
                {
                    oBP.FederalTaxID = pBP.LicTradNum;
                }

                setProperties(oBP, pBP);

                dynamic documentToAdd = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)documentToAdd;

                foreach (var property in oBP.GetType().GetProperties())
                {
                    if (!property.Name.EndsWith("Specified"))
                    {
                        string value = (property.GetValue(oBP) ?? "").ToString();

                        if (value != "0" && value != "0.0" && value != "")
                        {
                            if (value == "tYES")
                            {
                                dictionary.Add(property.Name, "Y");
                            }
                            else
                            {
                                if (property.GetValue(oBP).GetType().Name == "String"
                                    || property.GetValue(oBP).GetType().Name == "Boolean"
                                    || property.GetValue(oBP).GetType().Name == "Int32"
                                    || property.GetValue(oBP).GetType().Name == "Decimal"
                                    || property.GetValue(oBP).GetType().Name == "Double"
                                    || property.GetValue(oBP).GetType().Name == "Int64")
                                {
                                    dictionary.Add(property.Name, property.GetValue(oBP));
                                }
                                else
                                {
                                    switch (property.Name)
                                    {
                                        case "CardType":
                                            dictionary.Add(property.Name, pBP.CardType);
                                            break;

                                    }
                                }
                            }
                        }
                    }
                }

                List<dynamic> linesToAdd = new List<dynamic>();

                foreach (var item in oBP.BPAddresses)
                {
                    if (item != null)
                    {
                        dynamic documentLine = new ExpandoObject();
                        var dictionaryLine = (IDictionary<string, object>)documentLine;

                        foreach (var property in item.GetType().GetProperties())
                        {
                            if (!property.Name.EndsWith("Specified"))
                            {
                                string value = (property.GetValue(item) ?? "").ToString();

                                if (value != "0" && value != "0.0" && value != "")
                                {

                                    if (property.GetValue(item).GetType().Name == "String"
                                            || property.GetValue(item).GetType().Name == "Boolean"
                                            || property.GetValue(item).GetType().Name == "Int32"
                                            || property.GetValue(item).GetType().Name == "Decimal"
                                            || property.GetValue(item).GetType().Name == "Double"
                                            || property.GetValue(item).GetType().Name == "Int64")
                                    {
                                        dictionaryLine.Add(property.Name, property.GetValue(item));
                                    }
                                    else
                                    {
                                        switch (property.Name)
                                        {
                                            case "AddressType":

                                                dictionaryLine.Add(property.Name, (property.GetValue(item).ToString() == "bo_BillTo" ? "B" : "S"));
                                                break;
                                        }
                                    }



                                }
                            }
                        }

                        linesToAdd.Add(documentLine);
                    }
                }

                dictionary.Add("BPAddresses", linesToAdd.ToArray());

                if (!string.IsNullOrEmpty(pBP.U_B1SYS_FiscIdType))
                {
                    dictionary.Add("U_B1SYS_FiscIdType", pBP.U_B1SYS_FiscIdType);
                }

                foreach (UDF_ARGNS mUDF in pBP.MappedUdf)
                {
                    dictionary.Add(mUDF.UDFName, mUDF.Value);
                }

                var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

                var result = SLManager.callAPI(pCompanyParam, "BusinessPartners", RestSharp.Method.POST, jsonToAdd);

                var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                if (errorAPI.error != null)
                {
                    return "Error:" + errorAPI.error.message.value;
                }

                //var resultObject = JsonConvert.DeserializeObject<OCRD>(result.Content);

                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> AddBusinessPartner :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBP"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            var result2 = SLManager.callAPI(pCompanyParam, $"BusinessPartners('{pBP.CardCode}')", RestSharp.Method.GET, "");
            var docSAP = JsonConvert.DeserializeObject<BusinessPartnersServiceWeb.BusinessPartner>(result2.Content, settings);
            BusinessPartnersServiceWeb.BusinessPartner oBP = new BusinessPartnersServiceWeb.BusinessPartner();

            //B1WSHandler mB1WSHandler = new B1WSHandler();
            //BusinessPartnersServiceWeb.BusinessPartner oBP = new BusinessPartnersServiceWeb.BusinessPartner();
            //BusinessPartnersServiceWeb.BusinessPartnerParams oBPParams = new BusinessPartnersServiceWeb.BusinessPartnerParams();
            //BusinessPartnersServiceWeb.MsgHeader oBPMsgHeader = new BusinessPartnersServiceWeb.MsgHeader();

            //oBPMsgHeader.ServiceName = BusinessPartnersServiceWeb.MsgHeaderServiceName.BusinessPartnersService;
            //oBPMsgHeader.ServiceNameSpecified = true;
            //oBPMsgHeader.SessionID = pCompanyParam.DSSessionId;

            //BusinessPartnersServiceWeb.BusinessPartnersService oBPService = new BusinessPartnersServiceWeb.BusinessPartnersService();
            //oBPService.MsgHeaderValue = oBPMsgHeader;

            oBP = docSAP;
            oBP.CardCode = pBP.CardCode;
            try
            {
                //oBPParams.CardCode = pBP.CardCode;
                //oBP = oBPService.GetByParams(oBPParams);
                //Actualizo las lineas que ya existian en el objeto
                foreach (BPAddressesSAP address in pBP.Addresses.Where(c => c.LineNum != -1).ToList())
                {
                    if (address.LineNum != -1 && (address.Address != null && address.Address != ""))
                        updateAddress(address, oBP);
                    pBP.Addresses.Remove(address);
                }
                BusinessPartnersServiceWeb.BusinessPartnerBPAddress[] newAddresses = new BusinessPartnersServiceWeb.BusinessPartnerBPAddress[oBP.BPAddresses.Length + pBP.Addresses.Count];
                oBP.BPAddresses.CopyTo(newAddresses, 0);
                int positionNewAddress = oBP.BPAddresses.Length;
                oBP.BPAddresses = newAddresses;
                foreach (BPAddressesSAP address in pBP.Addresses)
                {
                    if (address.LineNum == -1 && (address.Address != null && address.Address != ""))
                        addNewAddress(address, oBP, positionNewAddress);
                    positionNewAddress++;
                }
                oBP.CardName = pBP.CardName;

                switch (pBP.CardType)
                {
                    case "C":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cCustomer;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "S":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cSupplier;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "L":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cLid;
                        oBP.CardTypeSpecified = true;
                        break;
                }
                oBP.MailAddress = pBP.MailAddres;
                oBP.Currency = pBP.Currency;
                oBP.Phone1 = pBP.Phone1;
                oBP.Phone2 = pBP.Phone2;
                oBP.Cellular = pBP.Cellular;
                oBP.Fax = pBP.Fax;
                oBP.EmailAddress = pBP.E_Mail;
                oBP.Website = pBP.IntrntSite;

                if (pBP.ShipType != null)
                {
                    oBP.ShippingType = (long)pBP.ShipType;
                    oBP.ShippingTypeSpecified = true;
                }

                if (pBP.SlpCode != null)
                {
                    oBP.SalesPersonCode = (long)pBP.SlpCode;
                    oBP.SalesPersonCodeSpecified = true;
                }

                if (pBP.ListNum != null)
                {
                    oBP.PriceListNum = (long)pBP.ListNum;
                    oBP.PriceListNumSpecified = true;
                }

                if (!string.IsNullOrEmpty(pBP.LicTradNum))
                {
                    oBP.FederalTaxID = pBP.LicTradNum;
                }

                setProperties(oBP, pBP);

                //XmlDocument mXmlDoc = Utils.GetSoapStructure(true, oBPMsgHeader, oBP);

                //if (!string.IsNullOrEmpty(pBP.U_B1SYS_FiscIdType))
                //{ mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml = mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml + "<U_B1SYS_FiscIdType>" + pBP.U_B1SYS_FiscIdType + "</U_B1SYS_FiscIdType>"; }

                //if (pBP.MappedUdf.Count > 0)
                //{ mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml, pBP.MappedUdf); }


                //mB1WSHandler.ProcessDoc(mXmlDoc);

                dynamic documentToAdd = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)documentToAdd;

                foreach (var property in oBP.GetType().GetProperties())
                {
                    if (!property.Name.EndsWith("Specified"))
                    {
                        string value = (property.GetValue(oBP) ?? "").ToString();

                        if (value != "0" && value != "0.0" && value != "")
                        {
                            if (value == "tYES")
                            {
                                dictionary.Add(property.Name, "Y");
                            }
                            else
                            {
                                if (property.GetValue(oBP).GetType().Name == "String"
                                    || property.GetValue(oBP).GetType().Name == "Boolean"
                                    || property.GetValue(oBP).GetType().Name == "Int32"
                                    || property.GetValue(oBP).GetType().Name == "Decimal"
                                    || property.GetValue(oBP).GetType().Name == "Double"
                                    || property.GetValue(oBP).GetType().Name == "Int64")
                                {
                                    dictionary.Add(property.Name, property.GetValue(oBP));
                                }
                                else
                                {
                                    switch (property.Name)
                                    {
                                        case "CardType":
                                            dictionary.Add(property.Name, pBP.CardType);
                                            break;

                                    }
                                }
                            }
                        }
                    }
                }

                List<dynamic> linesToAdd = new List<dynamic>();

                foreach (var item in oBP.BPAddresses)
                {
                    if (item != null)
                    {
                        dynamic documentLine = new ExpandoObject();
                        var dictionaryLine = (IDictionary<string, object>)documentLine;

                        foreach (var property in item.GetType().GetProperties())
                        {
                            if (!property.Name.EndsWith("Specified"))
                            {
                                string value = (property.GetValue(item) ?? "").ToString();

                                if (value != "0" && value != "0.0" && value != "" || property.Name == "RowNum")
                                {

                                    if (property.GetValue(item).GetType().Name == "String"
                                            || property.GetValue(item).GetType().Name == "Boolean"
                                            || property.GetValue(item).GetType().Name == "Int32"
                                            || property.GetValue(item).GetType().Name == "Decimal"
                                            || property.GetValue(item).GetType().Name == "Double"
                                            || property.GetValue(item).GetType().Name == "Int64")
                                    {
                                        dictionaryLine.Add(property.Name, property.GetValue(item));
                                    }
                                    else
                                    {
                                        switch (property.Name)
                                        {
                                            case "AddressType":

                                                dictionaryLine.Add(property.Name, (property.GetValue(item).ToString() == "bo_BillTo" ? "B" : "S"));
                                                break;
                                        }
                                    }



                                }
                            }
                        }

                        linesToAdd.Add(documentLine);
                    }
                }

                dictionary.Add("BPAddresses", linesToAdd.ToArray());

                if (!string.IsNullOrEmpty(pBP.U_B1SYS_FiscIdType))
                {
                    dictionary.Add("U_B1SYS_FiscIdType", pBP.U_B1SYS_FiscIdType);
                }

                foreach (UDF_ARGNS mUDF in pBP.MappedUdf)
                {
                    dictionary.Add(mUDF.UDFName, mUDF.Value);
                }

                var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

                var result = SLManager.callAPI(pCompanyParam, $"BusinessPartners('{ oBP.CardCode }')", RestSharp.Method.PATCH, jsonToAdd);

                var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                if (errorAPI != null)
                {
                    return "Error:" + errorAPI.error.message.value;
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> UpdateBusinessPartner :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oBP"></param>
        /// <param name="pBP"></param>
        private void setProperties(BusinessPartnersServiceWeb.BusinessPartner oBP, BusinessPartnerSAP pBP)
        {
            oBP.Properties1 = (pBP.QryGroup1.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties1.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties1.tNO);
            oBP.Properties1Specified = true;
            oBP.Properties2 = (pBP.QryGroup2.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties2.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties2.tNO);
            oBP.Properties2Specified = true;
            oBP.Properties3 = (pBP.QryGroup3.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties3.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties3.tNO);
            oBP.Properties3Specified = true;
            oBP.Properties4 = (pBP.QryGroup4.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties4.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties4.tNO);
            oBP.Properties4Specified = true;
            oBP.Properties5 = (pBP.QryGroup5.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties5.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties5.tNO);
            oBP.Properties5Specified = true;
            oBP.Properties6 = (pBP.QryGroup6.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties6.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties6.tNO);
            oBP.Properties6Specified = true;
            oBP.Properties7 = (pBP.QryGroup7.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties7.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties7.tNO);
            oBP.Properties7Specified = true;
            oBP.Properties8 = (pBP.QryGroup8.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties8.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties8.tNO);
            oBP.Properties8Specified = true;
            oBP.Properties9 = (pBP.QryGroup9.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties9.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties9.tNO);
            oBP.Properties9Specified = true;
            oBP.Properties10 = (pBP.QryGroup10.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties10.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties10.tNO);
            oBP.Properties10Specified = true;
            oBP.Properties11 = (pBP.QryGroup11.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties11.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties11.tNO);
            oBP.Properties11Specified = true;
            oBP.Properties12 = (pBP.QryGroup12.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties12.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties12.tNO);
            oBP.Properties12Specified = true;
            oBP.Properties13 = (pBP.QryGroup13.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties13.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties13.tNO);
            oBP.Properties13Specified = true;
            oBP.Properties14 = (pBP.QryGroup14.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties14.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties14.tNO);
            oBP.Properties14Specified = true;
            oBP.Properties15 = (pBP.QryGroup15.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties15.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties15.tNO);
            oBP.Properties15Specified = true;
            oBP.Properties16 = (pBP.QryGroup16.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties16.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties16.tNO);
            oBP.Properties16Specified = true;
            oBP.Properties17 = (pBP.QryGroup17.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties17.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties17.tNO);
            oBP.Properties17Specified = true;
            oBP.Properties18 = (pBP.QryGroup18.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties18.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties18.tNO);
            oBP.Properties18Specified = true;
            oBP.Properties19 = (pBP.QryGroup19.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties19.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties19.tNO);
            oBP.Properties19Specified = true;
            oBP.Properties20 = (pBP.QryGroup20.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties20.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties20.tNO);
            oBP.Properties20Specified = true;
            oBP.Properties21 = (pBP.QryGroup21.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties21.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties21.tNO);
            oBP.Properties21Specified = true;
            oBP.Properties22 = (pBP.QryGroup22.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties22.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties22.tNO);
            oBP.Properties22Specified = true;
            oBP.Properties23 = (pBP.QryGroup23.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties23.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties23.tNO);
            oBP.Properties23Specified = true;
            oBP.Properties24 = (pBP.QryGroup24.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties24.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties24.tNO);
            oBP.Properties24Specified = true;
            oBP.Properties25 = (pBP.QryGroup25.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties25.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties25.tNO);
            oBP.Properties25Specified = true;
            oBP.Properties26 = (pBP.QryGroup26.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties26.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties26.tNO);
            oBP.Properties26Specified = true;
            oBP.Properties27 = (pBP.QryGroup27.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties27.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties27.tNO);
            oBP.Properties27Specified = true;
            oBP.Properties28 = (pBP.QryGroup28.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties28.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties28.tNO);
            oBP.Properties28Specified = true;
            oBP.Properties29 = (pBP.QryGroup29.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties29.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties29.tNO);
            oBP.Properties29Specified = true;
            oBP.Properties30 = (pBP.QryGroup30.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties30.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties30.tNO);
            oBP.Properties30Specified = true;
            oBP.Properties31 = (pBP.QryGroup31.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties31.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties31.tNO);
            oBP.Properties31Specified = true;
            oBP.Properties32 = (pBP.QryGroup32.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties32.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties32.tNO);
            oBP.Properties32Specified = true;
            oBP.Properties33 = (pBP.QryGroup33.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties33.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties33.tNO);
            oBP.Properties33Specified = true;
            oBP.Properties34 = (pBP.QryGroup34.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties34.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties34.tNO);
            oBP.Properties34Specified = true;
            oBP.Properties35 = (pBP.QryGroup35.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties35.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties35.tNO);
            oBP.Properties35Specified = true;
            oBP.Properties36 = (pBP.QryGroup36.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties36.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties36.tNO);
            oBP.Properties36Specified = true;
            oBP.Properties37 = (pBP.QryGroup37.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties37.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties37.tNO);
            oBP.Properties37Specified = true;
            oBP.Properties38 = (pBP.QryGroup38.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties38.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties38.tNO);
            oBP.Properties38Specified = true;
            oBP.Properties39 = (pBP.QryGroup39.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties39.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties39.tNO);
            oBP.Properties39Specified = true;
            oBP.Properties40 = (pBP.QryGroup40.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties40.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties40.tNO);
            oBP.Properties40Specified = true;
            oBP.Properties41 = (pBP.QryGroup41.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties41.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties41.tNO);
            oBP.Properties41Specified = true;
            oBP.Properties42 = (pBP.QryGroup42.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties42.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties42.tNO);
            oBP.Properties42Specified = true;
            oBP.Properties43 = (pBP.QryGroup43.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties43.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties43.tNO);
            oBP.Properties43Specified = true;
            oBP.Properties44 = (pBP.QryGroup44.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties44.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties44.tNO);
            oBP.Properties44Specified = true;
            oBP.Properties45 = (pBP.QryGroup45.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties45.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties45.tNO);
            oBP.Properties45Specified = true;
            oBP.Properties46 = (pBP.QryGroup46.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties46.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties46.tNO);
            oBP.Properties46Specified = true;
            oBP.Properties47 = (pBP.QryGroup47.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties47.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties47.tNO);
            oBP.Properties47Specified = true;
            oBP.Properties48 = (pBP.QryGroup48.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties48.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties48.tNO);
            oBP.Properties48Specified = true;
            oBP.Properties49 = (pBP.QryGroup49.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties49.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties49.tNO);
            oBP.Properties49Specified = true;
            oBP.Properties50 = (pBP.QryGroup50.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties50.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties50.tNO);
            oBP.Properties50Specified = true;
            oBP.Properties51 = (pBP.QryGroup51.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties51.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties51.tNO);
            oBP.Properties51Specified = true;
            oBP.Properties52 = (pBP.QryGroup52.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties52.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties52.tNO);
            oBP.Properties52Specified = true;
            oBP.Properties53 = (pBP.QryGroup53.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties53.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties53.tNO);
            oBP.Properties53Specified = true;
            oBP.Properties54 = (pBP.QryGroup54.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties54.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties54.tNO);
            oBP.Properties54Specified = true;
            oBP.Properties55 = (pBP.QryGroup55.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties55.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties55.tNO);
            oBP.Properties55Specified = true;
            oBP.Properties56 = (pBP.QryGroup56.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties56.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties56.tNO);
            oBP.Properties56Specified = true;
            oBP.Properties57 = (pBP.QryGroup57.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties57.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties57.tNO);
            oBP.Properties57Specified = true;
            oBP.Properties58 = (pBP.QryGroup58.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties58.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties58.tNO);
            oBP.Properties58Specified = true;
            oBP.Properties59 = (pBP.QryGroup59.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties59.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties59.tNO);
            oBP.Properties59Specified = true;
            oBP.Properties60 = (pBP.QryGroup60.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties60.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties60.tNO);
            oBP.Properties60Specified = true;
            oBP.Properties61 = (pBP.QryGroup61.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties61.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties61.tNO);
            oBP.Properties61Specified = true;
            oBP.Properties62 = (pBP.QryGroup62.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties62.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties62.tNO);
            oBP.Properties62Specified = true;
            oBP.Properties63 = (pBP.QryGroup63.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties63.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties63.tNO);
            oBP.Properties63Specified = true;
            oBP.Properties64 = (pBP.QryGroup64.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties64.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties64.tNO);
            oBP.Properties64Specified = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPAddress"></param>
        /// <param name="oBP"></param>
        /// <param name="index"></param>
        private void addNewAddress(BPAddressesSAP pBPAddress, BusinessPartnersServiceWeb.BusinessPartner oBP, int index)
        {
            BusinessPartnersServiceWeb.BusinessPartnerBPAddress BPAddress = new BusinessPartnersServiceWeb.BusinessPartnerBPAddress();
            BPAddress.AddressName = pBPAddress.Address;
            BPAddress.Block = pBPAddress.Block;
            BPAddress.BuildingFloorRoom = pBPAddress.Building;
            BPAddress.City = pBPAddress.City;
            BPAddress.Country = pBPAddress.Country;
            BPAddress.County = pBPAddress.County;
            BPAddress.FederalTaxID = pBPAddress.LicTradNum;
            BPAddress.GlobalLocationNumber = pBPAddress.GlblLocNum;
            BPAddress.State = pBPAddress.State;
            BPAddress.Street = pBPAddress.Street;
            BPAddress.StreetNo = pBPAddress.StreetNo;
            BPAddress.TaxCode = pBPAddress.TaxCode;
            BPAddress.ZipCode = pBPAddress.ZipCode;
            BPAddress.AddressType = (pBPAddress.AdresType == "B" ? BusinessPartnersServiceWeb.BusinessPartnerBPAddressAddressType.bo_BillTo : BusinessPartnersServiceWeb.BusinessPartnerBPAddressAddressType.bo_ShipTo);
            BPAddress.AddressTypeSpecified = true;

            oBP.BPAddresses[index] = BPAddress;
            if (oBP.ShipToDefault == null && pBPAddress.AdresType == "S")
                oBP.ShipToDefault = pBPAddress.Address;
            if (oBP.BilltoDefault == null && pBPAddress.AdresType == "B")
                oBP.BilltoDefault = pBPAddress.Address;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPAddress"></param>
        /// <param name="oBP"></param>
        private void updateAddress(BPAddressesSAP pBPAddress, BusinessPartnersServiceWeb.BusinessPartner oBP)
        {
            BusinessPartnersServiceWeb.BusinessPartnerBPAddress BPAddress = oBP.BPAddresses.Where(c => c.RowNum == pBPAddress.LineNum && c.BPCode == pBPAddress.CardCode).FirstOrDefault();
            BPAddress.Block = pBPAddress.Block;
            BPAddress.BuildingFloorRoom = pBPAddress.Building;
            BPAddress.City = pBPAddress.City;
            BPAddress.Country = pBPAddress.Country;
            BPAddress.County = pBPAddress.County;
            BPAddress.FederalTaxID = pBPAddress.LicTradNum;
            BPAddress.GlobalLocationNumber = pBPAddress.GlblLocNum;
            BPAddress.State = pBPAddress.State;
            BPAddress.Street = pBPAddress.Street;
            BPAddress.StreetNo = pBPAddress.StreetNo;
            BPAddress.TaxCode = pBPAddress.TaxCode;
            BPAddress.ZipCode = pBPAddress.ZipCode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pType"></param>
        /// <param name="pOnlyActiveBPs"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pFilterByUser"></param>
        /// <param name="pSECode"></param>
        /// <param name="pBPGroupCardCodeList"></param>
        /// <param name="pGetLeads"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetBusinessPartnerListByUser(CompanyConn pCompanyParam,
            Enums.BpType pType, bool pOnlyActiveBPs,
            string pCodeBP = "", string pNameBP = "",
            bool pFilterByUser = false, int? pSECode = null,
            string[] pBPGroupCardCodeList = null, bool pGetLeads = false,
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null, bool pFilterByBP = false, string licTradNum = "")
        {
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();
            UserSL pUserServ = new UserSL();
            CounterHANA mCounterHANA = new CounterHANA();
            try
            {

                string mQuery = string.Empty;
                string mSelectQuery = "SELECT \"OCRD\".\"CardCode\", \"OCRD\".\"CardName\", \"OCRD\".\"CardType\", \"OCRD\".\"Address\", \"OCRD\".\"ZipCode\", \"OCRD\".\"MailAddres\", \"OCRD\".\"Currency\", \"OCRD\".\"ShipToDef\", \"OCRD\".\"BillToDef\", \"OCRD\".\"ListNum\", \"OCRD\".\"ShipType\", \"OCRD\".\"Notes\", \"OCRD\".\"SlpCode\", \"OCRD\".\"QryGroup64\", \"OCRD\".\"QryGroup63\"," +
                    "\"OCRD\".\"QryGroup62\", \"OCRD\".\"QryGroup61\", \"OCRD\".\"QryGroup60\", \"OCRD\".\"QryGroup59\", \"OCRD\".\"QryGroup58\", \"OCRD\".\"QryGroup57\", \"OCRD\".\"QryGroup56\", \"OCRD\".\"QryGroup55\", \"OCRD\".\"QryGroup54\", \"OCRD\".\"QryGroup53\", \"OCRD\".\"QryGroup52\", \"OCRD\".\"QryGroup51\", \"OCRD\".\"QryGroup50\"," +
                    "\"OCRD\".\"QryGroup49\", \"OCRD\".\"QryGroup48\", \"OCRD\".\"QryGroup47\", \"OCRD\".\"QryGroup46\", \"OCRD\".\"QryGroup45\", \"OCRD\".\"QryGroup44\", \"OCRD\".\"QryGroup43\", \"OCRD\".\"QryGroup42\", \"OCRD\".\"QryGroup41\", \"OCRD\".\"QryGroup40\", \"OCRD\".\"QryGroup39\", \"OCRD\".\"QryGroup38\", \"OCRD\".\"QryGroup37\"," +
                    "\"OCRD\".\"QryGroup36\", \"OCRD\".\"QryGroup35\", \"OCRD\".\"QryGroup34\", \"OCRD\".\"QryGroup33\", \"OCRD\".\"QryGroup32\", \"OCRD\".\"QryGroup31\", \"OCRD\".\"QryGroup30\", \"OCRD\".\"QryGroup29\", \"OCRD\".\"QryGroup28\", \"OCRD\".\"QryGroup27\", \"OCRD\".\"QryGroup26\", \"OCRD\".\"QryGroup25\", \"OCRD\".\"QryGroup24\"," +
                    "\"OCRD\".\"QryGroup23\", \"OCRD\".\"QryGroup22\", \"OCRD\".\"QryGroup21\", \"OCRD\".\"QryGroup20\", \"OCRD\".\"QryGroup19\", \"OCRD\".\"QryGroup18\", \"OCRD\".\"QryGroup17\", \"OCRD\".\"QryGroup16\", \"OCRD\".\"QryGroup15\", \"OCRD\".\"QryGroup14\", \"OCRD\".\"QryGroup13\", \"OCRD\".\"QryGroup12\", \"OCRD\".\"QryGroup11\"," +
                    "\"OCRD\".\"QryGroup10\", \"OCRD\".\"QryGroup9\", \"OCRD\".\"QryGroup8\", \"OCRD\".\"QryGroup7\", \"OCRD\".\"QryGroup6\", \"OCRD\".\"QryGroup5\", \"OCRD\".\"QryGroup4\", \"OCRD\".\"QryGroup3\", \"OCRD\".\"QryGroup2\", \"OCRD\".\"QryGroup1\", \"OCRD\".\"Balance\", \"OCRD\".\"DNotesBal\", \"OCRD\".\"OrdersBal\"," +
                    "\"OCRD\".\"LicTradNum\", \"OCRD\".\"OprCount\", \"OCRD\".\"Phone1\", \"OCRD\".\"GroupNum\", \"OCRD\".\"VatStatus\", \"OCRD\".\"Discount\", \"OCRD\".\"Phone2\", \"OCRD\".\"Fax\", \"OCRD\".\"Cellular\", \"OCRD\".\"E_Mail\", \"OCRD\".\"IntrntSite\"";
                string mFromQuery = " FROM \"{0}\".\"OCRD\"";

                string mWhereQuery = " WHERE 1 = 1 ";
                if (pOnlyActiveBPs)
                {
                    mWhereQuery = mWhereQuery + " AND \"OCRD\".\"frozenFor\" = 'N' ";
                }

                string mOrderByQuery = (pLength > 0 ? " ORDER BY \"OCRD\"." + Utils.OrderString(pOrderColumn, "HANA") + " LIMIT " + pLength + " OFFSET " + pStart : "");

                string mFilters = "";
                string mSecFilter = string.Empty;

                if (!string.IsNullOrEmpty(pCodeBP))
                {
                    if (pFilterByBP)
                        mFilters = mFilters + " and \"OCRD\".\"CardCode\" = '" + ChangeUTFCharacter(pCodeBP) + "'";
                    else
                        mFilters = mFilters + " and \"OCRD\".\"CardCode\" like '%" + ChangeUTFCharacter(pCodeBP) + "%'";
                }

                if (!string.IsNullOrEmpty(pNameBP))
                {
                    mFilters = mFilters + " and \"OCRD\".\"CardName\" like '%" + ChangeUTFCharacter(pNameBP) + "%'";
                }

                if (!string.IsNullOrEmpty(licTradNum))
                {
                    mFilters = mFilters + " and \"OCRD\".\"LicTradNum\" like '%" + ChangeUTFCharacter(licTradNum) + "%'";
                }

                string mtype = pType.ToDescriptionString();

                if (!string.IsNullOrEmpty(mtype))
                {
                    if (mtype != "A")
                    {
                        mFilters = mFilters + " and (\"OCRD\".\"CardType\" ='" + mtype + "'" + (pGetLeads ? " or \"OCRD\".\"CardType\" = 'L'" : "") + ")";
                    }
                }

                if (pFilterByUser)
                {
                    mFilters = mFilters + " and (" + (pSECode != null ? ("\"OCRD\".\"SlpCode\" =" + pSECode.Value.ToString()) : "1 = 1");

                    if (pBPGroupCardCodeList != null)
                    {
                        if (pBPGroupCardCodeList.Count() > 0)
                        {
                            mFilters += " or (";
                            foreach (string item in pBPGroupCardCodeList)
                            {
                                if (string.IsNullOrEmpty(mSecFilter))
                                {
                                    mSecFilter = " \"OCRD\".\"CardCode\" = '" + item + "'";
                                }
                                else
                                {
                                    mSecFilter = mSecFilter + " or \"OCRD\".\"CardCode\" = '" + item + "'";
                                }
                            }

                            mFilters = mFilters + mSecFilter + ")";
                        }
                    }
                    mFilters += ")";
                }

                mWhereQuery += mFilters;

                mQuery = SQLQueryUtil.getSelectQuery2(mSelectQuery, mFromQuery, null, mWhereQuery, mOrderByQuery);
                mQuery = String.Format(mQuery, pCompanyParam.CompanyDB);
                string mJsonResult = pUserServ.GetQueryResult(pCompanyParam, mQuery);
                mJsonObjectResult.BusinessPartnerSAPList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(mJsonResult);

                mQuery = SQLQueryUtil.getSelectQuery2(mSelectQuery, mFromQuery, null, mWhereQuery);
                mQuery = SQLQueryUtil.getSelectQuery2("Select count(*) as Counter ", "from(" + mQuery + ") as TT", null);
                mQuery = String.Format(mQuery, pCompanyParam.CompanyDB);
                mCounterHANA = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CounterHANA>>(pUserServ.GetQueryResult(pCompanyParam, mQuery)).FirstOrDefault();
                mJsonObjectResult.Others.Add("TotalRecords", mCounterHANA.Counter);

                return mJsonObjectResult;

            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetBusinessPartnerListByUser :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bp"></param>
        /// <returns></returns>
        public string ChangeUTFCharacter(string bp)
        {
            bp = bp.Replace("Ñ", "%")
                .Replace("ñ", "%")
                .Replace("ã", "%")
                .Replace("ç", "%")
                .Trim();

            Regex regex = new Regex(@"[^a-zA-Z0-9\s]", (RegexOptions)0);
            return regex.Replace(bp, "%");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public BusinessPartnerCombo GetCombo()
        {
            BusinessPartnerCombo ret = new BusinessPartnerCombo();

            ret.BPTypeList = new List<BusinessPartnerType>()
            {
                new BusinessPartnerType(){Code="C",Name="Customer"},
                   new BusinessPartnerType(){Code="S",Name="Vendor"},
                      new BusinessPartnerType(){Code="L",Name="Lead"},
            };

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        private class CounterHANA
        {
            public string Counter { get; set; }
        }

        public BusinessPartnerSAP GetBusinessPartnerMinimalData(CompanyConn pCompanyParam, string pCardCode)
        {
            BusinessPartnerSAP mListBusinessPartnerSAP = new BusinessPartnerSAP();
            string mUrl = string.Empty;
            try
            {
                string mFilters = "CardCode eq '" + pCardCode + "'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BP", mFilters);
                mListBusinessPartnerSAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BusinessPartnerSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                return mListBusinessPartnerSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessSL -> GetBusinessPartnerMinimalData :" + ex.Message);
                return null;
            }
        }
    }
}