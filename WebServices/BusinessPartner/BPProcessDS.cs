﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Xml;
using WebServices.Model;
using WebServices.Model.Tables;
using System.Linq.Dynamic;

namespace WebServices.BusinessPartner
{
    public class BPProcessDS : IBPService
    {
        private DataBase mDbContext;

        public BPProcessDS()
        {
            Database.SetInitializer<DataBase>(null);
            mDbContext = new DataBase();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerList(CompanyConn pCompanyParam)
        {
            List<BusinessPartnerSAP> mListBusinessPartnerSAP = new List<BusinessPartnerSAP>();

            Mapper.CreateMap<BusinessPartnerSAP, OCRD>();
            Mapper.CreateMap<OCRD, BusinessPartnerSAP>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            List<OCRD> mListOCRD = mDbContext.OCRD.ToList();

            mListBusinessPartnerSAP = Mapper.Map<List<BusinessPartnerSAP>>(mListOCRD);

            return mListBusinessPartnerSAP;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="pTypeBP"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerListSearch(CompanyConn pCompanyParam,
            string pCodeBP = "", string pNameBP = "", string pTypeBP = "")
        {
            List<BusinessPartnerSAP> mListBusinessPartnerSAP = new List<BusinessPartnerSAP>();

            Mapper.CreateMap<BusinessPartnerSAP, OCRD>();
            Mapper.CreateMap<OCRD, BusinessPartnerSAP>();

            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OCRD> mListOCRD = mDbContext.OCRD.Where(c => (c.CardCode.Contains(pCodeBP)) && (c.CardName.Contains(pNameBP)) && (c.CardType.Contains(pTypeBP))).ToList();

                mListBusinessPartnerSAP = Mapper.Map<List<BusinessPartnerSAP>>(mListOCRD);

                return mListBusinessPartnerSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessDS -> GetBusinessPartnerListSearch :" + ex.Message);
                return new List<BusinessPartnerSAP>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pListUDFCRD1"></param>
        /// <returns></returns>
        public BusinessPartnerSAP GetBusinessPartnerById(CompanyConn pCompanyParam, string pCode, List<UDF_ARGNS> pListUDFCRD1 = null, List<UDF_ARGNS> pListUDFs = null)
        {
            BusinessPartnerSAP mBusinessPartnerSAP = new BusinessPartnerSAP();

            Mapper.CreateMap<BusinessPartnerSAP, OCRD>();
            Mapper.CreateMap<OCRD, BusinessPartnerSAP>();

            Mapper.CreateMap<BPPropertiesSAP, OCQG>();
            Mapper.CreateMap<OCQG, BPPropertiesSAP>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<PriceListSAP, OPLN>();
            Mapper.CreateMap<OPLN, PriceListSAP>();

            Mapper.CreateMap<SalesEmployeeSAP, OSLP>();
            Mapper.CreateMap<OSLP, SalesEmployeeSAP>();

            Mapper.CreateMap<CountrySAP, OCRY>();
            Mapper.CreateMap<OCRY, CountrySAP>();

            Mapper.CreateMap<BPAddressesSAP, CRD1>();
            Mapper.CreateMap<CRD1, BPAddressesSAP>();

            Mapper.CreateMap<ContacPerson, OCPR>();
            Mapper.CreateMap<OCPR, ContacPerson>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            OCRD mOCRD = new OCRD();
            string mQuery = string.Empty;
            List<UFD1_SAP> mListUFD1FiscId = new List<UFD1_SAP>();

            string mSelect = "Select [OCRD].[CardCode], [OCRD].[CardName], [OCRD].[CardType], [OCRD].[Address], [OCRD].[ZipCode], [OCRD].[MailAddres], [OCRD].[Currency], " +
                                "[OCRD].[ShipToDef], [OCRD].[BillToDef], [OCRD].[ListNum], [OCRD].[ShipType], [OCRD].[Notes], [OCRD].[SlpCode], [OCRD].[QryGroup1], " +
                                "[OCRD].[QryGroup2], [OCRD].[QryGroup3], [OCRD].[QryGroup4], [OCRD].[QryGroup5], [OCRD].[QryGroup6], [OCRD].[QryGroup7], [OCRD].[QryGroup8], " +
                                "[OCRD].[QryGroup9], [OCRD].[QryGroup10], [OCRD].[QryGroup11], [OCRD].[QryGroup12], [OCRD].[QryGroup13], [OCRD].[QryGroup14], [OCRD].[QryGroup15], " +
                                "[OCRD].[QryGroup16], [OCRD].[QryGroup17], [OCRD].[QryGroup18], [OCRD].[QryGroup19], [OCRD].[QryGroup20], [OCRD].[QryGroup21], [OCRD].[QryGroup22], " +
                                "[OCRD].[QryGroup23], [OCRD].[QryGroup24], [OCRD].[QryGroup25], [OCRD].[QryGroup26], [OCRD].[QryGroup27], [OCRD].[QryGroup28], [OCRD].[QryGroup29], " +
                                "[OCRD].[QryGroup30], [OCRD].[QryGroup31], [OCRD].[QryGroup32], [OCRD].[QryGroup33], [OCRD].[QryGroup34], [OCRD].[QryGroup35], [OCRD].[QryGroup36], " +
                                "[OCRD].[QryGroup37], [OCRD].[QryGroup38], [OCRD].[QryGroup39], [OCRD].[QryGroup40], [OCRD].[QryGroup41], [OCRD].[QryGroup42], [OCRD].[QryGroup43], " +
                                "[OCRD].[QryGroup44], [OCRD].[QryGroup45], [OCRD].[QryGroup46], [OCRD].[QryGroup47], [OCRD].[QryGroup48], [OCRD].[QryGroup49], [OCRD].[QryGroup50], " +
                                "[OCRD].[QryGroup51], [OCRD].[QryGroup52], [OCRD].[QryGroup53], [OCRD].[QryGroup54], [OCRD].[QryGroup55], [OCRD].[QryGroup56], [OCRD].[QryGroup57], " +
                                "[OCRD].[QryGroup58], [OCRD].[QryGroup59], [OCRD].[QryGroup60], [OCRD].[QryGroup61], [OCRD].[QryGroup62], [OCRD].[QryGroup63], [OCRD].[QryGroup64], " +
                                "[OCRD].[Balance], [OCRD].[DNotesBal], [OCRD].[OrdersBal], [OCRD].[OprCount], [OCRD].[LicTradNum], [OCRD].[GroupNum], " +
                                "[OCRD].[PymCode], [OCRD].[VatStatus], [OCRD].[Discount], [OCRD].[Phone1], [OCRD].[Phone2], [OCRD].[Fax], [OCRD].[Cellular], [OCRD].[E_Mail], [OCRD].[IntrntSite] ";
            string mWhere = " WHERE [OCRD].[CardCode] = '" + pCode + "' ";
            string mFrom = " FROM [OCRD] ";

            if (pCompanyParam.CompanySAPConfig.DocumentSettingsSAP.LawsSet == "AR")
            {
                mSelect += ", [OCRD].[U_B1SYS_FiscIdType] ";

                string mSelectAux = "SELECT * FROM [UFD1] WHERE [UFD1].[FieldID] IN ( SELECT [FieldID] FROM [CUFD] WHERE [CUFD].[AliasID] = 'B1SYS_FiscIdType' AND [CUFD].[TableID] = 'OCRD') AND [UFD1].TableID = 'OCRD'";
                mListUFD1FiscId = UDFUtil.GetDBObjectList(mSelectAux, Utils.ConnectionString(pCompanyParam), null, typeof(UFD1_SAP), "UFD1").Cast<UFD1_SAP>().ToList();
            }
            mQuery = SQLQueryUtil.getSelectQuery2(mSelect, mFrom, null, mWhere, null, null, "SQL");
            mOCRD = UDFUtil.GetDBObjectList(mQuery, Utils.ConnectionString(pCompanyParam), null, typeof(OCRD), "OCRD").Cast<OCRD>().FirstOrDefault();

            if (mOCRD != null)
            {
                mBusinessPartnerSAP = Mapper.Map<BusinessPartnerSAP>(mOCRD);

                mWhere = "[CRD1].[CardCode] = '" + mBusinessPartnerSAP.CardCode + "'";
                List<CRD1> mListCRD1 = UDFUtil.GetObjectListWithUDF(pListUDFCRD1, mWhere, typeof(CRD1), mConnectionString, "CRD1").Cast<CRD1>().ToList();
                mBusinessPartnerSAP.Addresses = Mapper.Map<List<BPAddressesSAP>>(mListCRD1);

                mBusinessPartnerSAP.PaymentMethodList.AddRange((from mCRD2 in mDbContext.CRD2
                                                                join mOYPM in mDbContext.OPYM on mCRD2.PymCode equals mOYPM.PayMethCod
                                                                where mCRD2.CardCode == mBusinessPartnerSAP.CardCode
                                                                select new PaymentMethod()
                                                                {
                                                                    PayMethCod = mOYPM.PayMethCod,
                                                                    Descript = mOYPM.Descript

                                                                }).ToList());
            }

            List<OCQG> mListOCQG = mDbContext.OCQG.ToList();
            mBusinessPartnerSAP.PropertiesList = Mapper.Map<List<BPPropertiesSAP>>(mListOCQG);

            List<OCRN> mListOCRN = mDbContext.OCRN.ToList();
            mBusinessPartnerSAP.CurrencyList = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            List<OSHP> mListOSHP = mDbContext.OSHP.ToList();
            mBusinessPartnerSAP.ShipTypeList = Mapper.Map<List<ShippingType>>(mListOSHP);

            List<OPLN> mListOPLN = mDbContext.OPLN.ToList();
            mBusinessPartnerSAP.PriceList = Mapper.Map<List<PriceListSAP>>(mListOPLN);

            List<OSLP> mListSalesEmplpyee = mDbContext.OSLP.ToList();
            mBusinessPartnerSAP.EmployeeList = Mapper.Map<List<SalesEmployeeSAP>>(mListSalesEmplpyee);

            List<OCRY> mListOCRY = mDbContext.OCRY.ToList();
            mBusinessPartnerSAP.CountryList = Mapper.Map<List<CountrySAP>>(mListOCRY);

            List<OCPR> mListOCPR = mDbContext.OCPR.Where(c => c.CardCode == pCode).ToList();
            mBusinessPartnerSAP.ListContact = Mapper.Map<List<ContacPerson>>(mListOCPR);

            List<OADM> mListOADM = mDbContext.OADM.ToList();

            mBusinessPartnerSAP.CompanyCountry = mListOADM.FirstOrDefault().Country;
            mBusinessPartnerSAP.BPCombo = this.GetCombo();
            mBusinessPartnerSAP.ListFiscIdValues = mListUFD1FiscId;

            var OCRDUdfs = UDFUtil.GetObjectListWithUDF(pListUDFs, "CardCode = '" + pCode + "'", typeof(OCRD), mConnectionString, "OCRD").Cast<OCRD>().FirstOrDefault();

            mBusinessPartnerSAP.MappedUdf = OCRDUdfs == null ? pListUDFs : OCRDUdfs.MappedUdf;

            return mBusinessPartnerSAP;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pType"></param>
        /// <returns></returns>
        public List<BusinessPartnerSAP> GetBusinessPartnerListByType(CompanyConn pCompanyParam, Enums.BpType pType)
        {

            List<BusinessPartnerSAP> mListBusinessPartnerSAP = new List<BusinessPartnerSAP>();

            Mapper.CreateMap<BusinessPartnerSAP, OCRD>();
            Mapper.CreateMap<OCRD, BusinessPartnerSAP>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            string mtype = pType.ToDescriptionString();
            List<OCRD> mListOCRD = mDbContext.OCRD.Where(c => c.CardType == mtype).ToList();
            mListBusinessPartnerSAP = Mapper.Map<List<BusinessPartnerSAP>>(mListOCRD);

            return mListBusinessPartnerSAP;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCountry"></param>
        /// <returns></returns>
        public List<StateSAP> GetStateList(CompanyConn pCompanyParam, string pCountry)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<StateSAP> ListRestu = mDbContext.OCST.Where(c => c.Country == pCountry).Select(p => new StateSAP { Code = p.Code, Name = p.Name, Country = p.Country }).ToList();

                return ListRestu;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessDS -> GetStateList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBP"></param>
        /// <returns></returns>
        public JsonObjectResult GetCustomerAgingReport(CompanyConn pCompanyParam, List<string> pBP = null)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                User.UserService mUserService = new User.UserService();

                string mQueryCAR = System.IO.File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\BusinessPartner\Queries\CustomerAgingReport-SQL.sql");
                if (pBP != null)
                {
                    string mIn = "(";
                    for (int i = 0; i < pBP.Count; i++)
                    {
                        if (i == 0)
                            mIn += "'" + pBP[i] + "'";
                        else
                            mIn += ",'" + pBP[i] + "'";
                    }
                    mIn += ")";
                    mQueryCAR = mQueryCAR.Replace("/*CardCodeFilter*/", "AND T20.CardCode IN " + mIn);
                }
                JsonObjectResult mResult = mUserService.GetSAPQueryResult(pCompanyParam, mQueryCAR);

                return mResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessDS -> GetCustomerAgingReport(Message) :" + ex.Message);
                Logger.WriteError("BPProcessDS -> GetCustomerAgingReport(InnerException) :" + ex.InnerException);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBP"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string AddBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam)
        {
            B1WSHandler mB1WSHandler = new B1WSHandler();
            BusinessPartnersServiceWeb.BusinessPartner oBP = new BusinessPartnersServiceWeb.BusinessPartner();
            BusinessPartnersServiceWeb.BusinessPartnerParams oBPParams = new BusinessPartnersServiceWeb.BusinessPartnerParams();
            BusinessPartnersServiceWeb.MsgHeader oBPMsgHeader = new BusinessPartnersServiceWeb.MsgHeader();

            oBPMsgHeader.ServiceName = BusinessPartnersServiceWeb.MsgHeaderServiceName.BusinessPartnersService;
            oBPMsgHeader.ServiceNameSpecified = true;
            oBPMsgHeader.SessionID = pCompanyParam.DSSessionId;

            BusinessPartnersServiceWeb.BusinessPartnersService oBPService = new BusinessPartnersServiceWeb.BusinessPartnersService();
            oBPService.MsgHeaderValue = oBPMsgHeader;
            oBP.CardCode = pBP.CardCode;
            oBP.BPAddresses = new BusinessPartnersServiceWeb.BusinessPartnerBPAddress[pBP.Addresses.Count];
            try
            {
                int counter = 0;
                foreach (BPAddressesSAP address in pBP.Addresses)
                {
                    if (address.LineNum == -1 && (address.Address != null && address.Address != ""))
                        AddNewAddress(address, oBP, counter);
                    counter++;
                }
                oBP.CardName = pBP.CardName;
                switch (pBP.CardType)
                {
                    case "C":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cCustomer;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "S":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cSupplier;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "L":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cLid;
                        oBP.CardTypeSpecified = true;
                        break;
                }
                oBP.MailAddress = pBP.MailAddres;
                oBP.Currency = pBP.Currency;
                oBP.Phone1 = pBP.Phone1;
                oBP.Phone2 = pBP.Phone2;
                oBP.Cellular = pBP.Cellular;
                oBP.Fax = pBP.Fax;
                oBP.EmailAddress = pBP.E_Mail;
                oBP.Website = pBP.IntrntSite;
                //oBP.Series = 79;
                //oBP.SeriesSpecified = true;

                if (pBP.ShipType != null)
                {
                    oBP.ShippingType = (long)pBP.ShipType;
                    oBP.ShippingTypeSpecified = true;
                }

                if (pBP.SlpCode != null)
                {
                    oBP.SalesPersonCode = (long)pBP.SlpCode;
                    oBP.SalesPersonCodeSpecified = true;
                }

                if (pBP.ListNum != null)
                {
                    oBP.PriceListNum = (long)pBP.ListNum;
                    oBP.PriceListNumSpecified = true;
                }

                if (!string.IsNullOrEmpty(pBP.LicTradNum))
                {
                    oBP.FederalTaxID = pBP.LicTradNum;
                }

                SetProperties(oBP, pBP);

                XmlDocument mXmlDoc = Utils.GetSoapStructure(false, oBPMsgHeader, oBP);

                if (!string.IsNullOrEmpty(pBP.U_B1SYS_FiscIdType))
                { mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml = mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml + "<U_B1SYS_FiscIdType>" + pBP.U_B1SYS_FiscIdType + "</U_B1SYS_FiscIdType>"; }

                if (pBP.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml, pBP.MappedUdf); }


                string mResponse = mB1WSHandler.ProcessBPWithError(mXmlDoc);

                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessDS -> AddBusinessPartner :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBP"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateBusinessPartner(BusinessPartnerSAP pBP, CompanyConn pCompanyParam)
        {
            B1WSHandler mB1WSHandler = new B1WSHandler();
            BusinessPartnersServiceWeb.BusinessPartner oBP = new BusinessPartnersServiceWeb.BusinessPartner();
            BusinessPartnersServiceWeb.BusinessPartnerParams oBPParams = new BusinessPartnersServiceWeb.BusinessPartnerParams();
            BusinessPartnersServiceWeb.MsgHeader oBPMsgHeader = new BusinessPartnersServiceWeb.MsgHeader();

            oBPMsgHeader.ServiceName = BusinessPartnersServiceWeb.MsgHeaderServiceName.BusinessPartnersService;
            oBPMsgHeader.ServiceNameSpecified = true;
            oBPMsgHeader.SessionID = pCompanyParam.DSSessionId;
           

            BusinessPartnersServiceWeb.BusinessPartnersService oBPService = new BusinessPartnersServiceWeb.BusinessPartnersService();
            oBPService.MsgHeaderValue = oBPMsgHeader;
            oBP.CardCode = pBP.CardCode;
            try
            {
                oBPParams.CardCode = pBP.CardCode;
                oBP = oBPService.GetByParams(oBPParams);
                //Actualizo las lineas que ya existian en el objeto
                foreach (BPAddressesSAP address in pBP.Addresses.Where(c => c.LineNum != -1).ToList())
                {
                    if (address.LineNum != -1 && (address.Address != null && address.Address != ""))
                        UpdateAddress(address, oBP);
                    pBP.Addresses.Remove(address);
                }
                BusinessPartnersServiceWeb.BusinessPartnerBPAddress[] newAddresses = new BusinessPartnersServiceWeb.BusinessPartnerBPAddress[oBP.BPAddresses.Length + pBP.Addresses.Count];
                oBP.BPAddresses.CopyTo(newAddresses, 0);
                int positionNewAddress = oBP.BPAddresses.Length;
                oBP.BPAddresses = newAddresses;
                foreach (BPAddressesSAP address in pBP.Addresses)
                {
                    if (address.LineNum == -1 && (address.Address != null && address.Address != ""))
                        AddNewAddress(address, oBP, positionNewAddress);
                    positionNewAddress++;
                }
                oBP.CardName = pBP.CardName;
                switch (pBP.CardType)
                {
                    case "C":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cCustomer;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "S":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cSupplier;
                        oBP.CardTypeSpecified = true;
                        break;
                    case "L":
                        oBP.CardType = BusinessPartnersServiceWeb.BusinessPartnerCardType.cLid;
                        oBP.CardTypeSpecified = true;
                        break;
                }
                oBP.MailAddress = pBP.MailAddres;
                oBP.Currency = pBP.Currency;
                oBP.Phone1 = pBP.Phone1;
                oBP.Phone2 = pBP.Phone2;
                oBP.Cellular = pBP.Cellular;
                oBP.Fax = pBP.Fax;
                oBP.EmailAddress = pBP.E_Mail;
                oBP.Website = pBP.IntrntSite;
                
                if (pBP.ShipType != null)
                {
                    oBP.ShippingType = (long)pBP.ShipType;
                    oBP.ShippingTypeSpecified = true;
                }

                if (pBP.SlpCode != null)
                {
                    oBP.SalesPersonCode = (long)pBP.SlpCode;
                    oBP.SalesPersonCodeSpecified = true;
                }

                if (pBP.ListNum != null)
                {
                    oBP.PriceListNum = (long)pBP.ListNum;
                    oBP.PriceListNumSpecified = true;
                }

                if (!string.IsNullOrEmpty(pBP.LicTradNum))
                {
                    oBP.FederalTaxID = pBP.LicTradNum;
                }

                SetProperties(oBP, pBP);

                XmlDocument mXmlDoc = Utils.GetSoapStructure(true, oBPMsgHeader, oBP);
                

                if (!string.IsNullOrEmpty(pBP.U_B1SYS_FiscIdType))
                { mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml = mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml + "<U_B1SYS_FiscIdType>" + pBP.U_B1SYS_FiscIdType + "</U_B1SYS_FiscIdType>"; }

                if (pBP.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='BusinessPartner']").InnerXml, pBP.MappedUdf); }


                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                Logger.WriteError("BPProcessDS -> UpdateBusinessPartner :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oBP"></param>
        /// <param name="pBP"></param>
        private void SetProperties(BusinessPartnersServiceWeb.BusinessPartner oBP, BusinessPartnerSAP pBP)
        {
            oBP.Properties1 = (pBP.QryGroup1.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties1.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties1.tNO);
            oBP.Properties1Specified = true;
            oBP.Properties2 = (pBP.QryGroup2.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties2.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties2.tNO);
            oBP.Properties2Specified = true;
            oBP.Properties3 = (pBP.QryGroup3.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties3.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties3.tNO);
            oBP.Properties3Specified = true;
            oBP.Properties4 = (pBP.QryGroup4.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties4.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties4.tNO);
            oBP.Properties4Specified = true;
            oBP.Properties5 = (pBP.QryGroup5.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties5.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties5.tNO);
            oBP.Properties5Specified = true;
            oBP.Properties6 = (pBP.QryGroup6.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties6.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties6.tNO);
            oBP.Properties6Specified = true;
            oBP.Properties7 = (pBP.QryGroup7.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties7.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties7.tNO);
            oBP.Properties7Specified = true;
            oBP.Properties8 = (pBP.QryGroup8.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties8.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties8.tNO);
            oBP.Properties8Specified = true;
            oBP.Properties9 = (pBP.QryGroup9.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties9.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties9.tNO);
            oBP.Properties9Specified = true;
            oBP.Properties10 = (pBP.QryGroup10.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties10.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties10.tNO);
            oBP.Properties10Specified = true;
            oBP.Properties11 = (pBP.QryGroup11.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties11.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties11.tNO);
            oBP.Properties11Specified = true;
            oBP.Properties12 = (pBP.QryGroup12.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties12.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties12.tNO);
            oBP.Properties12Specified = true;
            oBP.Properties13 = (pBP.QryGroup13.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties13.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties13.tNO);
            oBP.Properties13Specified = true;
            oBP.Properties14 = (pBP.QryGroup14.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties14.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties14.tNO);
            oBP.Properties14Specified = true;
            oBP.Properties15 = (pBP.QryGroup15.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties15.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties15.tNO);
            oBP.Properties15Specified = true;
            oBP.Properties16 = (pBP.QryGroup16.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties16.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties16.tNO);
            oBP.Properties16Specified = true;
            oBP.Properties17 = (pBP.QryGroup17.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties17.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties17.tNO);
            oBP.Properties17Specified = true;
            oBP.Properties18 = (pBP.QryGroup18.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties18.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties18.tNO);
            oBP.Properties18Specified = true;
            oBP.Properties19 = (pBP.QryGroup19.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties19.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties19.tNO);
            oBP.Properties19Specified = true;
            oBP.Properties20 = (pBP.QryGroup20.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties20.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties20.tNO);
            oBP.Properties20Specified = true;
            oBP.Properties21 = (pBP.QryGroup21.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties21.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties21.tNO);
            oBP.Properties21Specified = true;
            oBP.Properties22 = (pBP.QryGroup22.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties22.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties22.tNO);
            oBP.Properties22Specified = true;
            oBP.Properties23 = (pBP.QryGroup23.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties23.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties23.tNO);
            oBP.Properties23Specified = true;
            oBP.Properties24 = (pBP.QryGroup24.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties24.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties24.tNO);
            oBP.Properties24Specified = true;
            oBP.Properties25 = (pBP.QryGroup25.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties25.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties25.tNO);
            oBP.Properties25Specified = true;
            oBP.Properties26 = (pBP.QryGroup26.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties26.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties26.tNO);
            oBP.Properties26Specified = true;
            oBP.Properties27 = (pBP.QryGroup27.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties27.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties27.tNO);
            oBP.Properties27Specified = true;
            oBP.Properties28 = (pBP.QryGroup28.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties28.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties28.tNO);
            oBP.Properties28Specified = true;
            oBP.Properties29 = (pBP.QryGroup29.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties29.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties29.tNO);
            oBP.Properties29Specified = true;
            oBP.Properties30 = (pBP.QryGroup30.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties30.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties30.tNO);
            oBP.Properties30Specified = true;
            oBP.Properties31 = (pBP.QryGroup31.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties31.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties31.tNO);
            oBP.Properties31Specified = true;
            oBP.Properties32 = (pBP.QryGroup32.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties32.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties32.tNO);
            oBP.Properties32Specified = true;
            oBP.Properties33 = (pBP.QryGroup33.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties33.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties33.tNO);
            oBP.Properties33Specified = true;
            oBP.Properties34 = (pBP.QryGroup34.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties34.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties34.tNO);
            oBP.Properties34Specified = true;
            oBP.Properties35 = (pBP.QryGroup35.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties35.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties35.tNO);
            oBP.Properties35Specified = true;
            oBP.Properties36 = (pBP.QryGroup36.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties36.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties36.tNO);
            oBP.Properties36Specified = true;
            oBP.Properties37 = (pBP.QryGroup37.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties37.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties37.tNO);
            oBP.Properties37Specified = true;
            oBP.Properties38 = (pBP.QryGroup38.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties38.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties38.tNO);
            oBP.Properties38Specified = true;
            oBP.Properties39 = (pBP.QryGroup39.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties39.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties39.tNO);
            oBP.Properties39Specified = true;
            oBP.Properties40 = (pBP.QryGroup40.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties40.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties40.tNO);
            oBP.Properties40Specified = true;
            oBP.Properties41 = (pBP.QryGroup41.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties41.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties41.tNO);
            oBP.Properties41Specified = true;
            oBP.Properties42 = (pBP.QryGroup42.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties42.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties42.tNO);
            oBP.Properties42Specified = true;
            oBP.Properties43 = (pBP.QryGroup43.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties43.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties43.tNO);
            oBP.Properties43Specified = true;
            oBP.Properties44 = (pBP.QryGroup44.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties44.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties44.tNO);
            oBP.Properties44Specified = true;
            oBP.Properties45 = (pBP.QryGroup45.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties45.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties45.tNO);
            oBP.Properties45Specified = true;
            oBP.Properties46 = (pBP.QryGroup46.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties46.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties46.tNO);
            oBP.Properties46Specified = true;
            oBP.Properties47 = (pBP.QryGroup47.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties47.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties47.tNO);
            oBP.Properties47Specified = true;
            oBP.Properties48 = (pBP.QryGroup48.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties48.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties48.tNO);
            oBP.Properties48Specified = true;
            oBP.Properties49 = (pBP.QryGroup49.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties49.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties49.tNO);
            oBP.Properties49Specified = true;
            oBP.Properties50 = (pBP.QryGroup50.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties50.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties50.tNO);
            oBP.Properties50Specified = true;
            oBP.Properties51 = (pBP.QryGroup51.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties51.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties51.tNO);
            oBP.Properties51Specified = true;
            oBP.Properties52 = (pBP.QryGroup52.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties52.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties52.tNO);
            oBP.Properties52Specified = true;
            oBP.Properties53 = (pBP.QryGroup53.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties53.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties53.tNO);
            oBP.Properties53Specified = true;
            oBP.Properties54 = (pBP.QryGroup54.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties54.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties54.tNO);
            oBP.Properties54Specified = true;
            oBP.Properties55 = (pBP.QryGroup55.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties55.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties55.tNO);
            oBP.Properties55Specified = true;
            oBP.Properties56 = (pBP.QryGroup56.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties56.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties56.tNO);
            oBP.Properties56Specified = true;
            oBP.Properties57 = (pBP.QryGroup57.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties57.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties57.tNO);
            oBP.Properties57Specified = true;
            oBP.Properties58 = (pBP.QryGroup58.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties58.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties58.tNO);
            oBP.Properties58Specified = true;
            oBP.Properties59 = (pBP.QryGroup59.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties59.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties59.tNO);
            oBP.Properties59Specified = true;
            oBP.Properties60 = (pBP.QryGroup60.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties60.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties60.tNO);
            oBP.Properties60Specified = true;
            oBP.Properties61 = (pBP.QryGroup61.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties61.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties61.tNO);
            oBP.Properties61Specified = true;
            oBP.Properties62 = (pBP.QryGroup62.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties62.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties62.tNO);
            oBP.Properties62Specified = true;
            oBP.Properties63 = (pBP.QryGroup63.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties63.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties63.tNO);
            oBP.Properties63Specified = true;
            oBP.Properties64 = (pBP.QryGroup64.ToUpper() == "TRUE" ? BusinessPartnersServiceWeb.BusinessPartnerProperties64.tYES : BusinessPartnersServiceWeb.BusinessPartnerProperties64.tNO);
            oBP.Properties64Specified = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPAddress"></param>
        /// <param name="oBP"></param>
        /// <param name="index"></param>
        private void AddNewAddress(BPAddressesSAP pBPAddress, BusinessPartnersServiceWeb.BusinessPartner oBP, int index)
        {
            BusinessPartnersServiceWeb.BusinessPartnerBPAddress BPAddress = new BusinessPartnersServiceWeb.BusinessPartnerBPAddress();
            BPAddress.AddressName = pBPAddress.Address;
            BPAddress.Block = pBPAddress.Block;
            BPAddress.BuildingFloorRoom = pBPAddress.Building;
            BPAddress.City = pBPAddress.City;
            BPAddress.Country = pBPAddress.Country;
            BPAddress.County = pBPAddress.County;
            BPAddress.FederalTaxID = pBPAddress.LicTradNum;
            BPAddress.GlobalLocationNumber = pBPAddress.GlblLocNum;
            BPAddress.State = pBPAddress.State;
            BPAddress.Street = pBPAddress.Street;
            BPAddress.StreetNo = pBPAddress.StreetNo;
            BPAddress.TaxCode = pBPAddress.TaxCode;
            BPAddress.ZipCode = pBPAddress.ZipCode;
            BPAddress.AddressType = (pBPAddress.AdresType == "B" ? BusinessPartnersServiceWeb.BusinessPartnerBPAddressAddressType.bo_BillTo : BusinessPartnersServiceWeb.BusinessPartnerBPAddressAddressType.bo_ShipTo);
            BPAddress.AddressTypeSpecified = true;

            oBP.BPAddresses[index] = BPAddress;
            if (oBP.ShipToDefault == null && pBPAddress.AdresType == "S")
                oBP.ShipToDefault = pBPAddress.Address;
            if (oBP.BilltoDefault == null && pBPAddress.AdresType == "B")
                oBP.BilltoDefault = pBPAddress.Address;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pBPAddress"></param>
        /// <param name="oBP"></param>
        private void UpdateAddress(BPAddressesSAP pBPAddress, BusinessPartnersServiceWeb.BusinessPartner oBP)
        {
            BusinessPartnersServiceWeb.BusinessPartnerBPAddress BPAddress = oBP.BPAddresses.Where(c => c.RowNum == pBPAddress.LineNum && c.BPCode == pBPAddress.CardCode).FirstOrDefault();
            BPAddress.Block = pBPAddress.Block;
            BPAddress.BuildingFloorRoom = pBPAddress.Building;
            BPAddress.City = pBPAddress.City;
            BPAddress.Country = pBPAddress.Country;
            BPAddress.County = pBPAddress.County;
            BPAddress.FederalTaxID = pBPAddress.LicTradNum;
            BPAddress.GlobalLocationNumber = pBPAddress.GlblLocNum;
            BPAddress.State = pBPAddress.State;
            BPAddress.Street = pBPAddress.Street;
            BPAddress.StreetNo = pBPAddress.StreetNo;
            BPAddress.TaxCode = pBPAddress.TaxCode;
            BPAddress.ZipCode = pBPAddress.ZipCode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pType"></param>
        /// <param name="pCodeBP"></param>
        /// <param name="pNameBP"></param>
        /// <param name="FilterByUser"></param>
        /// <param name="pSECode"></param>
        /// <param name="pBPGroupCardCodeList"></param>
        /// <param name="pGetLeads"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetBusinessPartnerListByUser(CompanyConn pCompanyParam,
            Enums.BpType pType, bool pOnlyActiveBPs, string pCodeBP = "",
            string pNameBP = "", bool FilterByUser = false,
            int? pSECode = null, string[] pBPGroupCardCodeList = null,
            bool pGetLeads = false, int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null, bool pFilterByBP = false, string licTradNum = "")
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<OCRD, BusinessPartnerSAP>(); }).CreateMapper();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                List<OCRD> mListOCRD = new List<OCRD>();

                string mtype = pType.ToDescriptionString();

                int mTotalRecords = mDbContext.OCRD
                    .Where(c => ((FilterByUser == true ?
                    (c.SlpCode == pSECode || (pBPGroupCardCodeList.Count() > 0 ? pBPGroupCardCodeList.Contains(c.CardCode) : true)) : true)
                    && (pFilterByBP ? c.CardCode == pCodeBP : c.CardCode.Contains(pCodeBP))) && (c.CardName.Contains(pNameBP)) && (c.LicTradNum.Contains(licTradNum))
                    && (!pOnlyActiveBPs || c.frozenFor == "N")
                    && ((mtype != "A") ? (c.CardType.Contains(mtype) || (pGetLeads ? c.CardType == "L" : false)) : true)).Count();

                if (pLength != 0)
                {
                    mListOCRD = mDbContext.OCRD
                        .Where(c => ((FilterByUser == true ?
                            (c.SlpCode == pSECode || (pBPGroupCardCodeList.Count() > 0 ? pBPGroupCardCodeList.Contains(c.CardCode) : true)) : true)
                            && (pFilterByBP ? c.CardCode == pCodeBP : c.CardCode.Contains(pCodeBP) ))
                            && (c.CardName.Contains(pNameBP))
                            && (c.LicTradNum.Contains(licTradNum))
                            && (!pOnlyActiveBPs || c.frozenFor == "N")
                            && ((mtype != "A") ? (c.CardType.Contains(mtype) || (pGetLeads ? c.CardType == "L" : false)) : true))
                            .OrderBy("CardCode"/*Utils.OrderString(pOrderColumn)*/).Skip(pStart).Take(pLength).ToList();
                }
                else
                {
                    mListOCRD = mDbContext.OCRD
                        .Where(c => ((FilterByUser == true ? (c.SlpCode == pSECode || (pBPGroupCardCodeList.Count() > 0 ?
                        pBPGroupCardCodeList.Contains(c.CardCode) : true)) : true) && (pFilterByBP ? c.CardCode == pCodeBP : c.CardCode.Contains(pCodeBP)))
                        && (!pOnlyActiveBPs || c.frozenFor == "N")
                        && (c.CardName.Contains(pNameBP)) && (c.LicTradNum.Contains(licTradNum)) && ((mtype != "A") ? (c.CardType.Contains(mtype) || (pGetLeads ? c.CardType == "L" : false)) : true))
                        .OrderBy("CardCode"/*Utils.OrderString(pOrderColumn)*/).ToList();
                }

                mJsonObjectResult.BusinessPartnerSAPList = mapper.Map<List<BusinessPartnerSAP>>(mListOCRD);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessDS -> GetBusinessPartnerListByUser :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public BusinessPartnerCombo GetCombo()
        {
            BusinessPartnerCombo ret = new BusinessPartnerCombo();

            ret.BPTypeList = new List<BusinessPartnerType>()
            {
                     new BusinessPartnerType(){Code="C",Name="Customer"},
                     new BusinessPartnerType(){Code="S",Name="Vendor"},
                     new BusinessPartnerType(){Code="L",Name="Lead"},
            };

            return ret;
        }

        public BusinessPartnerSAP GetBusinessPartnerMinimalData(CompanyConn pCompanyParam, string pCardCode)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<OCRD, BusinessPartnerSAP>(); }).CreateMapper();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            try
            {
                OCRD mOCRDReturn = mDbContext.OCRD.Where(c => c.CardCode == pCardCode).FirstOrDefault();

                return mapper.Map<BusinessPartnerSAP>(mOCRDReturn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("BPProcessDS -> GetBusinessPartnerMinimalData :" + ex.Message);
                return null;
            }
        }
    }
}