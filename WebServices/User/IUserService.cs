﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM.ComboList;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebServices.User
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserService" in both code and config file together.
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        List<UserSAP> GetUserList(CompanyConn pCompanyParam);

        [OperationContract]
        UserSAP GetUserById(CompanyConn pCompanyParam,short pCode);

        [OperationContract]
        List<EmployeeSAP> GetEmployeeList(CompanyConn pCompanyParam);

        [OperationContract]
        List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "", short? pDepartment = null, bool pHaveUserCode = true);

        [OperationContract]
        List<UserSAP> GetUserSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "");

        [OperationContract]
        UserSAP GetUserSAPSearchByName(CompanyConn pCc, string pUserName);

        [OperationContract]
        EmployeeSAP GetEmployeeById(CompanyConn pCompanyParam, int? pCode);

        [OperationContract]
        List<EmployeeSAP> GetAllEmployee(CompanyConn pCompanyParam);

        [OperationContract]
        List<SalesEmployeeSAP> GetAllSalesEmployee(CompanyConn pCompanyParam);

        [OperationContract]
        List<Warehouse> GetWarehouseList(CompanyConn pCompanyParam);

        [OperationContract]
        List<DistrRuleSAP> GetDistributionRuleList(CompanyConn pCompanyParam);

        [OperationContract]
        List<UDF_SAP> GetUDFByTableID(CompanyConn pCompanyParam, string pTableID);

        [OperationContract]
        List<UFD1_SAP> GetUDF1ByTableID(CompanyConn pCompanyParam, string pTableID);

        [OperationContract]
        string GetQueryResult(CompanyConn pCompanyParam, string pQuery);

        [OperationContract]
        List<AlertSAP> GetAlertsByUser(CompanyConn pCompanyParam, int pUser);

        [OperationContract]
        AlertSAP GetAlertById(CompanyConn pCompanyParam, int pAlertId);

        [OperationContract]
        List<SalesEmployeeSAP> GetSalesEmployeeSearchByName(CompanyConn pCompanyParam, string pSEName);

        [OperationContract]
        List<Warehouse> GetWarehouseListSearch(CompanyConn pCompanyParam, string WhsCode, string WhsName);

        [OperationContract]
        List<QuerySAP> GetSAPQueryListSearch(CompanyConn pCompanyParam);

        [OperationContract]
        JsonObjectResult GetSAPQueryResult(CompanyConn pCompanyParam, string pQuery);

        [OperationContract]
        List<PriceListSAP> GetAllPriceListSAP(CompanyConn pCompanyParam);
    }
}
