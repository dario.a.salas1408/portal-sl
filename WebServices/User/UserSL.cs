﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ARGNS.Model.Implementations.SAP;
using System.Data.Entity;
using WebServices.Model;
using WebServices.Model.Tables;
using AutoMapper;
using ARGNS.Util;
using ARGNS.Model.Implementations;
using WebServices.B1IFUserService;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Dynamic;
using System.Web;
using ARGNS.Model.Implementations.View;
using WebServices.CompanyServiceWeb;
using ARGNS.Model.Implementations.PDM.ComboList;
using WebServices.Query;

namespace WebServices.User
{
    public class UserSL : IUserService
    {
        public List<UserSAP> GetUserList(CompanyConn pCompanyParam)
        {
            List<UserSAP> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetUserList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public UserSAP GetUserById(CompanyConn pCompanyParam, short pCode)
        {
            UserSAP mRet = null;
            string mUrl;
            string mFilters;
            try
            {
                mFilters = "USERID eq " + pCode + "";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User", mFilters);
                List<UserSAP> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRetList.SingleOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetUserById :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<EmployeeSAP> GetEmployeeList(CompanyConn pCompanyParam)
        {
            List<EmployeeSAP> mRet = null;
            string mUrl;
            string mFilters;
            try
            {
                mFilters = "userId eq 0";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetEmployeeList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyConn pCompanyParam, string pRequesterCode = "", string pRequesterName = "", short? pDepartment = null, bool pHaveUserCode = true)
        {
            List<EmployeeSAP> mRet = null;
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                int empCode = (pRequesterCode != "" ? Convert.ToInt32(pRequesterCode) : 0);

                if (!string.IsNullOrEmpty(pRequesterName))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pRequesterName + "'), tolower(firstName)) or  substringof(tolower('" + pRequesterName + "'), tolower(lastName))";
                }

                if (!string.IsNullOrEmpty(pRequesterCode))
                {
                    mFilters = mFilters + " and empID eq " + empCode + "";
                }

                if (pDepartment != null)
                {
                    mFilters = mFilters + " and dept eq " + pDepartment + "";
                }

                if (pHaveUserCode == false)
                {
                    mFilters = mFilters + " and userId eq null";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetEmployeeSAPSearchByCode :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<UserSAP> GetUserSAPSearchByCode(CompanyConn pCompanyParam, string pRequesterCode = "", string pRequesterName = "")
        {
            List<UserSAP> mRet = null;
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(pRequesterCode))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pRequesterCode) + "'), tolower(USER_CODE))";
                }

                if (!string.IsNullOrEmpty(pRequesterName))
                {
                    mFilters = mFilters + "and substringof(tolower('" + HttpUtility.UrlEncode(pRequesterName) + "'), tolower(U_NAME))";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetUserSAPSearchByCode :" + ex.Message);
                return null;
            }
            return mRet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserName"></param>
        /// <returns></returns>
        public UserSAP GetUserSAPSearchByName(CompanyConn pCompanyParam, string pUserName)
        {
            UserSAP mRet = null;
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(pUserName))
                {
                    mFilters = mFilters + "and U_NAME eq '" + pUserName + "'";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "User", mFilters);
                List<UserSAP> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UserSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRetList.SingleOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetUserSAPSearchByName :" + ex.Message);
                return null;
            }
            return mRet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public EmployeeSAP GetEmployeeById(CompanyConn pCompanyParam, int? pCode)
        {
            EmployeeSAP mRet = null;
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                mFilters = mFilters + "empID eq " + pCode + "";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", mFilters);
                List<EmployeeSAP> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRet = mRetList.SingleOrDefault();
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetEmployeeById :" + ex.Message);
                return null;
            }
            return mRet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<EmployeeSAP> GetAllEmployee(CompanyConn pCompanyParam)
        {
            List<EmployeeSAP> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetAllEmployee :" + ex.Message);
                return null;
            }
            return mRet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<SalesEmployeeSAP> GetAllSalesEmployee(CompanyConn pCompanyParam)
        {
            List<SalesEmployeeSAP> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesEmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                if (mRet != null)
                {
                    return mRet;
                }
                else
                {
                    return new List<SalesEmployeeSAP>();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetAllSalesEmployee :" + ex.Message);
                return new List<SalesEmployeeSAP>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSEName"></param>
        /// <returns></returns>
        public List<SalesEmployeeSAP> GetSalesEmployeeSearchByName(CompanyConn pCompanyParam, string pSEName)
        {
            List<SalesEmployeeSAP> mRet = null;
            string mUrl;
            string mFilters = "substringof(tolower('" + pSEName + "'), tolower(SlpName))";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesEmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetAllSalesEmployee :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<Warehouse> GetWarehouseList(CompanyConn pCompanyParam)
        {
            List<Warehouse> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Warehouse>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetWarehouseList :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<DistrRuleSAP> GetDistributionRuleList(CompanyConn pCompanyParam)
        {
            List<DistrRuleSAP> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DistrRuleSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetAllSalesEmployee :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<UDF_SAP> GetUDFByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            List<UDF_SAP> mRet = new List<UDF_SAP>();
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(pTableID))
                {
                    if (pTableID.Contains("@ARGNS"))
                        mFilters = mFilters + "and substringof(tolower('ARGNS'), tolower(AliasID))";

                    mFilters = mFilters + "and substringof(tolower('" + pTableID + "'), tolower(TableID))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "CUFD", mFilters);
                    mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UDF_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetUDFByTableID :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<UFD1_SAP> GetUDF1ByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            List<UFD1_SAP> mRet = new List<UFD1_SAP>();
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(pTableID))
                {
                    mFilters = mFilters + "and substringof(tolower('" + pTableID + "'), tolower(TableID))";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetUDF1ByTableID :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public string GetQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            try
            {
                dynamic mJsonObj = new ExpandoObject();
                mJsonObj.paramSQL = Convert.ToString(pQuery);
                var data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(mJsonObj));

                var request = (HttpWebRequest)WebRequest.Create(pCompanyParam.UrlHanaGetQueryResult);
                request.Method = "POST";
                request.ContentType = "application/json;odata=minimalmetadata";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                Uri uri = new Uri(pCompanyParam.UrlHanaGetQueryResult);
                var cache = new CredentialCache();
                cache.Add(uri, "Basic", new NetworkCredential(pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                request.Credentials = cache;

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return responseString;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetQueryResult :" + ex.Message);
                return null;
            }
        }

        public List<AlertSAP> GetAlertsByUser(CompanyConn pCompanyParam, int pUser)
        {
            List<AlertSAP> mRet = new List<AlertSAP>();
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                mFilters = mFilters + "and WasRead eq 'N' and UserSignAIB eq " + pUser;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "AlertsList", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AlertSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetAlertsByUser :" + ex.Message);
                return null;
            }

            return mRet;
        }

        public AlertSAP GetAlertById(CompanyConn pCompanyParam, int pAlertId)
        {
            AlertSAP mRet = new AlertSAP();
            string mUrl;
            string mFilters = "Code eq " + pAlertId;
            try
            {
                mRet.AlertCode = pAlertId;
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ALR2", mFilters);
                mRet.AlertColumns = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AlertColumnSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ALR3", mFilters);
                mRet.AlertRowData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AlertRowDataSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetAlertById :" + ex.Message);
                return null;
            }

            return mRet;
        }

        public List<Warehouse> GetWarehouseListSearch(CompanyConn pCompanyParam, string WhsCode, string WhsName)
        {
            List<Warehouse> mRet = null;
            string mUrl;
            string mFilters = "1 eq 1 ";
            try
            {
                if (!string.IsNullOrEmpty(WhsCode))
                {
                    mFilters = mFilters + " and substringof(tolower('" + WhsCode + "'), tolower(WhsCode))";
                }
                if (!string.IsNullOrEmpty(WhsName))
                {
                    mFilters = mFilters + " and substringof(tolower('" + WhsName + "'), tolower(WhsName))";
                }
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", mFilters);
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Warehouse>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetWarehouseListSearch :" + ex.Message);
                return null;
            }
            return mRet;
        }

        public List<QuerySAP> GetSAPQueryListSearch(CompanyConn pCompanyParam)
        {
            List<QuerySAP> mListQuerySAP = null;
            List<QueryCategorySAP> mListQueryCategorySAP = null;
            string mUrl;
            string mFilters = "substringof(tolower('Y'), tolower(PermMask)) ";
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OQCN", mFilters);
                mListQueryCategorySAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<QueryCategorySAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                List<int> mListAuxQueryCatIds = mListQueryCategorySAP.Select(j => j.CategoryId).ToList();

                mFilters = "(";
                string mSecFilter = string.Empty;
                foreach (int item in mListAuxQueryCatIds)
                {
                    if (string.IsNullOrEmpty(mSecFilter))
                    {
                        mSecFilter = " QCategory eq " + item;
                    }
                    else
                    {
                        mSecFilter = mSecFilter + " or QCategory eq " + item;
                    }

                }
                mFilters = mFilters + mSecFilter + ")";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUQR", mFilters);
                mListQuerySAP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<QuerySAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mListQuerySAP.ForEach(c => c.CategorySAP = mListQueryCategorySAP.Where(j => j.CategoryId == c.QCategory).FirstOrDefault());

                return mListQuerySAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetSAPQueryListSearch:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pQuery"></param>
        /// <returns></returns>
        public JsonObjectResult GetSAPQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            CompanyService oService = new CompanyService();

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();


            try
            {

                Recordset mQueryResult = null;

                dynamic result = JsonConvert.DeserializeObject(SLManager.GetQueryResult(pCompanyParam, pQuery));

                int countRows = 0;

                foreach (var item in result)
                {
                    countRows++;
                }

                mQueryResult = new Recordset();

                mQueryResult.Row = new RecordsetRow[countRows];

                int rowNum = 0;

                foreach (var itemRow in result)
                {
                    var rows = JsonConvert.DeserializeObject(itemRow.ToString());

                    Dictionary<string, string> property = new Dictionary<string, string>();

                    foreach (var row in rows)
                    {
                        var objs = row.ToString().Split(':');

                        if (objs.Length > 1)
                        {
                            property.Add(objs[0].Replace('\"', ' ').TrimEnd().TrimStart(), objs[1].Replace('\"', ' ').TrimEnd().TrimStart());
                        }
                    }

                    mQueryResult.Row[rowNum] = new RecordsetRow();

                    mQueryResult.Row[rowNum].Property = new RecordsetRowProperty[property.Count -1];

                    for (int i = 0; i < property.Count - 1; i++)
                    {
                        mQueryResult.Row[rowNum].Property[i] = new RecordsetRowProperty();
                        mQueryResult.Row[rowNum].Property[i].Name = property.ElementAt(i).Key;
                        mQueryResult.Row[rowNum].Property[i].Value = property.ElementAt(i).Value;
                    }

                    rowNum++;
                }




                if (mQueryResult.Row == null)
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToString();
                    mJsonObjectResult.ErrorMsg = "No records found";
                    mJsonObjectResult.Others.Add("jsonQueryResult", JsonConvert.SerializeObject(mQueryResult));
                }
                else
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
                    mJsonObjectResult.Others.Add("jsonQueryResult", JsonConvert.SerializeObject(mQueryResult));
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetSAPQueryResult :" + ex.Message);
                mJsonObjectResult.Others.Add("jsonQueryResult", null);
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<PriceListSAP> GetAllPriceListSAP(CompanyConn pCompanyParam)
        {
            List<PriceListSAP> mRet = null;
            string mUrl;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PriceList");
                mRet = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PriceListSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserSL -> GetAllPriceListSAP:" + ex.Message);
                return null;
            }
            return mRet;
        }

    }
}