﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ARGNS.Model.Implementations.SAP;
using System.Data.Entity;
using WebServices.Model;
using WebServices.Model.Tables;
using AutoMapper;
using ARGNS.Util;
using ARGNS.Model.Implementations;
using WebServices.B1IFUserService;
using System.Data;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.PDM.ComboList;

namespace WebServices.User
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select UserService.svc or UserService.svc.cs at the Solution Explorer and start debugging.
    public class UserService : IUserService
    {
        private IUserService mUserService = null;
        private DataBase mDbContext;
       
        public UserService()
        {
            Database.SetInitializer<DataBase>(null);
            mDbContext = new DataBase();
        }

        
        private void InitializeService(CompanyConn pCompanyParam)
        {
            if (pCompanyParam.OnDemand)
            {
                mUserService = new UserIF();
            }
            else if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
            {
                mUserService = new UserSL();
            }
            else
            {
                mUserService = new UserDS();
            }
        }

        public List<UserSAP> GetUserList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetUserList(pCompanyParam);
            }
            catch (Exception ex)
            {
                if (ex.InnerException.Message == "The remote server returned an error: (401) Unauthorized.")
                    return new List<UserSAP>();
                Logger.WriteError("UserService -> GetUserList :" + ex.Message);
                return null;
            }    
        }


        public List<EmployeeSAP> GetEmployeeList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetEmployeeList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetEmployeeList :" + ex.Message);
                return null;
            }   
        
        }

        public List<EmployeeSAP> GetAllEmployee(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetAllEmployee(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetAllEmployee :" + ex.Message);
                return null;
            }  
        }

        public List<SalesEmployeeSAP> GetAllSalesEmployee(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetAllSalesEmployee(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetAllSalesEmployee :" + ex.Message);
                return null;
            }

        }

        public List<SalesEmployeeSAP> GetSalesEmployeeSearchByName(CompanyConn pCompanyParam, string pSEName)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetSalesEmployeeSearchByName(pCompanyParam, pSEName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetSalesEmployeeSearchByName :" + ex.Message);
                return null;
            }
        }

        public UserSAP GetUserById(CompanyConn pCompanyParam,short pCode)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetUserById(pCompanyParam, pCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetUserById :" + ex.Message);
                return null;
            }
        }

        public List<UserSAP> GetUserSAPSearchByCode(CompanyConn pCompanyParam, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetUserSAPSearchByCode(pCompanyParam, pRequesterCode, pRequesterName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetUserSAPSearchByCode :" + ex.Message);
                return null;
            }
        }

        public UserSAP GetUserSAPSearchByName(CompanyConn pCompanyParam, string pUserName)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetUserSAPSearchByName(pCompanyParam, pUserName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetUserSAPSearchByName :" + ex.Message);
                return null;
            }
        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "", short? pDepartment = null, bool pHaveUserCode = true)
        {
            try
            {
                InitializeService(pCc);
                return mUserService.GetEmployeeSAPSearchByCode(pCc, pRequesterCode, pRequesterName, pDepartment, pHaveUserCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetEmployeeSAPSearchByCode :" + ex.Message);
                return null;
            }
        }

        public EmployeeSAP GetEmployeeById(CompanyConn pCompanyParam, int? pCode)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetEmployeeById(pCompanyParam, pCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetEmployeeById :" + ex.Message);
                return null;
            }
        }

        public List<Warehouse> GetWarehouseList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetWarehouseList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetWarehouseList :" + ex.Message);
                return null;
            }
        }

        public List<DistrRuleSAP> GetDistributionRuleList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetDistributionRuleList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetDistributionRuleList :" + ex.Message);
                return null;
            }
        }

        public List<UDF_SAP> GetUDFByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetUDFByTableID(pCompanyParam, pTableID);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetUDFByTableID :" + ex.Message);
                return null;
            }
        }

        public List<UFD1_SAP> GetUDF1ByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetUDF1ByTableID(pCompanyParam, pTableID);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetUDF1ByTableID :" + ex.Message);
                return null;
            }
        }

        public string GetQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetQueryResult(pCompanyParam, pQuery);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetQueryResult :" + ex.Message);
                return null;
            }
        }

        public List<AlertSAP> GetAlertsByUser(CompanyConn pCompanyParam, int pUser)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetAlertsByUser(pCompanyParam, pUser);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetAlertsByUser :" + ex.Message);
                return null;
            }
        }

        public AlertSAP GetAlertById(CompanyConn pCompanyParam, int pAlertId)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetAlertById(pCompanyParam, pAlertId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetAlertById :" + ex.Message);
                return null;
            }
        }

        public List<Warehouse> GetWarehouseListSearch(CompanyConn pCompanyParam, string WhsCode, string WhsName)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetWarehouseListSearch(pCompanyParam, WhsCode, WhsName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetWarehouseListSearch :" + ex.Message);
                return null;
            }
        }

        public List<QuerySAP> GetSAPQueryListSearch(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetSAPQueryListSearch(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetSAPQueryListSearch :" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult GetSAPQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetSAPQueryResult(pCompanyParam, pQuery);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetSAPQueryResult :" + ex.Message);
                return null;
            }
        }

        public List<PriceListSAP> GetAllPriceListSAP(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mUserService.GetAllPriceListSAP(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserService -> GetAllPriceListSAP :" + ex.Message);
                return null;
            }
        }
    }
}
