﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ARGNS.Model.Implementations.SAP;
using System.Data.Entity;
using WebServices.Model;
using WebServices.Model.Tables;
using AutoMapper;
using ARGNS.Util;
using ARGNS.Model.Implementations;
using WebServices.B1IFUserService;
using System.Data;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Implementations.PDM.ComboList;

namespace WebServices.User
{
    public class UserIF: IUserService
    {
        public List<UserSAP> GetUserList(CompanyConn pCompanyParam)
        {
            try
            { 
                Mapper.CreateMap<UserSAP, GetUserList1ResponseTypeRow>();
                Mapper.CreateMap<GetUserList1ResponseTypeRow, UserSAP>();

                B1IFUserService.GetUserList1Type request = new B1IFUserService.GetUserList1Type();
                B1IFUserService.GetUserList1ResponseType response = new B1IFUserService.GetUserList1ResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetUserList1(request);
                List<UserSAP> listUsers = Mapper.Map<List<UserSAP>>(response.GetUserList1Result.ToList());
                return listUsers;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetUserList :" + ex.Message);
                return null;
            }
        }

        public UserSAP GetUserById(CompanyConn pCompanyParam, short pCode)
        {
            try
            {
                Mapper.CreateMap<UserSAP, GetUserByIdResponseTypeGetUserByIdResultRow>();
                Mapper.CreateMap<GetUserByIdResponseTypeGetUserByIdResultRow, UserSAP>();

                B1IFUserService.GetUserByIdType request = new B1IFUserService.GetUserByIdType();
                B1IFUserService.GetUserByIdResponseType response = new B1IFUserService.GetUserByIdResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                request.UserCode = pCode.ToString();
                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetUserById(request);
                UserSAP mUser = Mapper.Map<UserSAP>(response.GetUserByIdResult.row);
                return mUser;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetUserById :" + ex.Message);
                return null;
            }
        }

        public List<EmployeeSAP> GetEmployeeList(CompanyConn pCompanyParam)
        {
            try
            { 
                Mapper.CreateMap<EmployeeSAP, GetEmployeeListResponseTypeRow>();
                Mapper.CreateMap<GetEmployeeListResponseTypeRow, EmployeeSAP>();

                B1IFUserService.GetEmployeeListType request = new B1IFUserService.GetEmployeeListType();
                B1IFUserService.GetEmployeeListResponseType response = new B1IFUserService.GetEmployeeListResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetEmployeeList(request);
                List<EmployeeSAP> listEmployee = Mapper.Map<List<EmployeeSAP>>(response.GetEmployeeListResult.Where(c => c.userId.HasValue == false).ToList());
                return listEmployee;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetEmployeeList :" + ex.Message);
                return null;
            }

        }

        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "", short? pDepartment = null, bool pHaveUserCode = true)
        {
            throw new NotImplementedException();
        }

        public List<UserSAP> GetUserSAPSearchByCode(CompanyConn pCc, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                Mapper.CreateMap<UserSAP, GetUserList1ResponseTypeRow>();
                Mapper.CreateMap<GetUserList1ResponseTypeRow, UserSAP>();

                B1IFUserService.GetUserList1Type request = new B1IFUserService.GetUserList1Type();
                B1IFUserService.GetUserList1ResponseType response = new B1IFUserService.GetUserList1ResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCc.SAPLanguaje + "/" + pCc.UserName + "/" + pCc.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCc.Password;
                response = llamadaServicio.ARGNSGetUserList1(request);
                List<UserSAP> listUsers = Mapper.Map<List<UserSAP>>(response.GetUserList1Result.Where(c => (pRequesterCode != "" ? (c.USER_CODE.Contains(pRequesterCode)) : true) && (pRequesterName != "" ? (c.U_NAME.Contains(pRequesterName)) : true)).ToList());
                return listUsers;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetUserSAPSearchByCode :" + ex.Message);
                return null;
            }
        }

        public UserSAP GetUserSAPSearchByName(CompanyConn pCc, string pUserName)
        {
            try
            {
                Mapper.CreateMap<UserSAP, GetUserList1ResponseTypeRow>();
                Mapper.CreateMap<GetUserList1ResponseTypeRow, UserSAP>();

                B1IFUserService.GetUserList1Type request = new B1IFUserService.GetUserList1Type();
                B1IFUserService.GetUserList1ResponseType response = new B1IFUserService.GetUserList1ResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCc.SAPLanguaje + "/" + pCc.UserName + "/" + pCc.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCc.Password;
                response = llamadaServicio.ARGNSGetUserList1(request);
                UserSAP mUser = Mapper.Map<UserSAP>(response.GetUserList1Result.Where(c => (pUserName != "" ? (c.U_NAME == pUserName) : true)).FirstOrDefault());
                return mUser;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetUserSAPSearchByName :" + ex.Message);
                return null;
            }
        }

        public EmployeeSAP GetEmployeeById(CompanyConn pCompanyParam, int? pCode)
        {
            try
            {
                Mapper.CreateMap<EmployeeSAP, GetEmployeeByIdResponseTypeGetEmployeeByIdResultRow>();
                Mapper.CreateMap<GetEmployeeByIdResponseTypeGetEmployeeByIdResultRow, EmployeeSAP>();

                B1IFUserService.GetEmployeeByIdType request = new B1IFUserService.GetEmployeeByIdType();
                B1IFUserService.GetEmployeeByIdResponseType response = new B1IFUserService.GetEmployeeByIdResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                request.Code = pCode.ToString();
                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetEmployeeById(request);

                EmployeeSAP mEmployee = Mapper.Map<EmployeeSAP>(response.GetEmployeeByIdResult.row);
                return mEmployee;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetEmployeeById :" + ex.Message);
                return null;
            }
        }

        public List<EmployeeSAP> GetAllEmployee(CompanyConn pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<EmployeeSAP, GetAllEmployeeResponseTypeRow>();
                Mapper.CreateMap<GetAllEmployeeResponseTypeRow, EmployeeSAP>();

                B1IFUserService.GetAllEmployeeType request = new B1IFUserService.GetAllEmployeeType();
                B1IFUserService.GetAllEmployeeResponseType response = new B1IFUserService.GetAllEmployeeResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetAllEmployee(request);
                List<EmployeeSAP> listEmployee = Mapper.Map<List<EmployeeSAP>>(response.GetAllEmployeeResult.ToList());
                return listEmployee;

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetAllEmployee :" + ex.Message);
                return null;
            }
        }

        public List<SalesEmployeeSAP> GetAllSalesEmployee(CompanyConn pCompanyParam)
        {
            try
            { 
                Mapper.CreateMap<SalesEmployeeSAP, GetAllSalEmployeeResponseTypeRow>();
                Mapper.CreateMap<GetAllSalEmployeeResponseTypeRow, SalesEmployeeSAP>();

                B1IFUserService.GetAllSalEmployeeType request = new B1IFUserService.GetAllSalEmployeeType();
                B1IFUserService.GetAllSalEmployeeResponseType response = new B1IFUserService.GetAllSalEmployeeResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetAllSalEmployee(request);
                List<SalesEmployeeSAP> listSalEmployee = Mapper.Map<List<SalesEmployeeSAP>>(response.GetAllSalEmployeeResult.ToList());
                return listSalEmployee;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetAllSalesEmployee :" + ex.Message);
				return new List<SalesEmployeeSAP>();
			}
        }

        public List<Warehouse> GetWarehouseList(CompanyConn pCompanyParam)
        {
            try 
            {
                Mapper.CreateMap<Warehouse, GetWarehouseListResponseTypeRow>();
                Mapper.CreateMap<GetWarehouseListResponseTypeRow, Warehouse>();

                B1IFUserService.GetWarehouseListType request = new B1IFUserService.GetWarehouseListType();
                B1IFUserService.GetWarehouseListResponseType response = new B1IFUserService.GetWarehouseListResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetWarehouseList(request);

                List<Warehouse> mListWarehouse = Mapper.Map<List<Warehouse>>(response.GetWarehouseListResult.ToList());
                return mListWarehouse;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetWarehouseList :" + ex.Message);
                return null;
            }
        }

        public List<DistrRuleSAP> GetDistributionRuleList(CompanyConn pCompanyParam)
        {
            try
            { 
                Mapper.CreateMap<DistrRuleSAP, GetDistrbRuleListResponseTypeRow>();
                Mapper.CreateMap<GetDistrbRuleListResponseTypeRow, DistrRuleSAP>();

                B1IFUserService.GetDistrbRuleListType request = new B1IFUserService.GetDistrbRuleListType();
                B1IFUserService.GetDistrbRuleListResponseType response = new B1IFUserService.GetDistrbRuleListResponseType();
                B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFUserService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();

                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetDistrbRuleList(request);

                List<DistrRuleSAP> mListDistRule = Mapper.Map<List<DistrRuleSAP>>(response.GetDistrbRuleListResult.ToList());
                return mListDistRule;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetDistributionRuleList :" + ex.Message);
                return null;
            }
        }

        public List<UDF_SAP> GetUDFByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserIF -> GetUDFByTableID :" + ex.Message);
                return null;
            }
        }

        public string GetQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            throw new NotImplementedException();
        }

        public List<UFD1_SAP> GetUDF1ByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            throw new NotImplementedException();
        }
        
        public List<AlertSAP> GetAlertsByUser(CompanyConn pCompanyParam, int pUser)
        {
            throw new NotImplementedException();
        }

        public AlertSAP GetAlertById(CompanyConn pCompanyParam, int pAlertId)
        {
            throw new NotImplementedException();
        }

        public List<SalesEmployeeSAP> GetSalesEmployeeSearchByName(CompanyConn pCompanyParam, string pSEName)
        {
            throw new NotImplementedException();
        }

        public List<Warehouse> GetWarehouseListSearch(CompanyConn pCompanyParam, string WhsCode, string WhsName)
        {
            throw new NotImplementedException();
        }

        public List<QuerySAP> GetSAPQueryListSearch(CompanyConn pCompanyParam)
        {
            throw new NotImplementedException();
        }

        public JsonObjectResult GetSAPQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            throw new NotImplementedException();
        }

        public List<PriceListSAP> GetAllPriceListSAP(CompanyConn pCompanyParam)
        {
            throw new NotImplementedException();
        }
    }
}