﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ARGNS.Model.Implementations.SAP;
using System.Data.Entity;
using WebServices.Model;
using WebServices.Model.Tables;
using AutoMapper;
using ARGNS.Util;
using ARGNS.Model.Implementations;
using WebServices.B1IFUserService;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Dynamic;
using Newtonsoft.Json;
using ARGNS.Model.Implementations.View;
using WebServices.CompanyServiceWeb;
using ARGNS.Model.Implementations.PDM.ComboList;

namespace WebServices.User
{
    public class UserDS : IUserService
    {
        private DataBase mDbContext;

        public UserDS()
        {
            Database.SetInitializer<DataBase>(null);
            mDbContext = new DataBase();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<UserSAP> GetUserList(CompanyConn pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<UserSAP, OUSR>();
                Mapper.CreateMap<OUSR, UserSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OUSR> mOUSR = mDbContext.OUSR.ToList();

                return Mapper.Map<List<UserSAP>>(mOUSR);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> Connection :" + mDbContext.Database.Connection.ConnectionString);
                Logger.WriteError("UserDS -> GetUserList :" + ex.Message);
                return new List<UserSAP>(); // in case one database's name is incorrect it was returning null
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }

        }

        public UserSAP GetUserById(CompanyConn pCompanyParam, short pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<UserSAP, OUSR>();
                Mapper.CreateMap<OUSR, UserSAP>();

                return Mapper.Map<UserSAP>(mDbContext.OUSR.Where(c => c.USERID == pCode).Single());

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetUserById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<EmployeeSAP> GetEmployeeList(CompanyConn pCompanyParam)
        {
            try
            {
                List<EmployeeSAP> mListEmployeeSAP = new List<EmployeeSAP>();

                Mapper.CreateMap<EmployeeSAP, OHEM>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OHEM> mListOHEM = mDbContext.OHEM.Where(c => c.userId == null && c.userId.HasValue == false).ToList();

                mListEmployeeSAP = Mapper.Map<List<EmployeeSAP>>(mListOHEM);

                return mListEmployeeSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetEmployeeList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pRequesterCode"></param>
        /// <param name="pRequesterName"></param>
        /// <param name="pDepartment"></param>
        /// <param name="pHaveUserCode"></param>
        /// <returns></returns>
        public List<EmployeeSAP> GetEmployeeSAPSearchByCode(CompanyConn pCompanyParam, string pRequesterCode = "", string pRequesterName = "", short? pDepartment = null, bool pHaveUserCode = true)
        {
            try
            {
                List<EmployeeSAP> mListEmployeeSAP = new List<EmployeeSAP>();

                Mapper.CreateMap<EmployeeSAP, OHEM>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                int empCode = (pRequesterCode != "" ? Convert.ToInt32(pRequesterCode) : 0);

                List<OHEM> mListOHEM = mDbContext.OHEM.Where(c => (pRequesterName != "" ? (c.firstName.Contains(pRequesterName) || c.lastName.Contains(pRequesterName)) : true) && (pRequesterCode != "" ? (c.empID == empCode) : true) && (pDepartment != null ? c.dept == pDepartment : true) && (pHaveUserCode == false ? c.userId == null : true)).ToList();

                mListEmployeeSAP = Mapper.Map<List<EmployeeSAP>>(mListOHEM);

                return mListEmployeeSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetEmployeeSAPSearchByCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pRequesterCode"></param>
        /// <param name="pRequesterName"></param>
        /// <returns></returns>
        public List<UserSAP> GetUserSAPSearchByCode(CompanyConn pCompanyParam, string pRequesterCode = "", string pRequesterName = "")
        {
            try
            {
                Mapper.CreateMap<UserSAP, OUSR>();
                Mapper.CreateMap<OUSR, UserSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OUSR> mOUSR = mDbContext.OUSR.Where(c => (pRequesterCode != "" ? (c.USER_CODE.Contains(pRequesterCode)) : true) && (pRequesterName != "" ? (c.U_NAME.Contains(pRequesterName)) : true)).ToList();

                return Mapper.Map<List<UserSAP>>(mOUSR);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetUserSAPSearchByCode :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCc"></param>
        /// <param name="pUserName"></param>
        /// <returns></returns>
        public UserSAP GetUserSAPSearchByName(CompanyConn pCc, string pUserName)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCc);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<UserSAP, OUSR>();
                Mapper.CreateMap<OUSR, UserSAP>();

                return Mapper.Map<UserSAP>(mDbContext.OUSR.Where(c => (pUserName != "" ? (c.U_NAME == pUserName) : true)).FirstOrDefault());

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetUserSAPSearchByName :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public EmployeeSAP GetEmployeeById(CompanyConn pCompanyParam, int? pCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<EmployeeSAP, OHEM>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();

                return Mapper.Map<EmployeeSAP>(mDbContext.OHEM.Where(c => c.empID == pCode).Single());

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetEmployeeById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<EmployeeSAP> GetAllEmployee(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<EmployeeSAP, OHEM>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();

                return Mapper.Map<List<EmployeeSAP>>(mDbContext.OHEM.ToList());

            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetAllEmployee :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<SalesEmployeeSAP> GetAllSalesEmployee(CompanyConn pCompanyParam)
        {
            try
            {
                List<SalesEmployeeSAP> mListSalesEmployeeSAP = new List<SalesEmployeeSAP>();

                Mapper.CreateMap<SalesEmployeeSAP, OSLP>();
                Mapper.CreateMap<OSLP, SalesEmployeeSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OSLP> mListOSLP = mDbContext.OSLP.ToList();

                mListSalesEmployeeSAP = Mapper.Map<List<SalesEmployeeSAP>>(mListOSLP);

                return mListSalesEmployeeSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetAllSalesEmployee :" + ex.Message);
				return new List<SalesEmployeeSAP>();
			}
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSEName"></param>
        /// <returns></returns>
        public List<SalesEmployeeSAP> GetSalesEmployeeSearchByName(CompanyConn pCompanyParam, string pSEName)
        {
            try
            {
                List<SalesEmployeeSAP> mListSalesEmployeeSAP = new List<SalesEmployeeSAP>();

                Mapper.CreateMap<SalesEmployeeSAP, OSLP>();
                Mapper.CreateMap<OSLP, SalesEmployeeSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OSLP> mListOSLP = mDbContext.OSLP.Where(c => c.SlpName.Contains(pSEName)).ToList();

                mListSalesEmployeeSAP = Mapper.Map<List<SalesEmployeeSAP>>(mListOSLP);

                return mListSalesEmployeeSAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetAllSalesEmployee :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<Warehouse> GetWarehouseList(CompanyConn pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<Warehouse, OWHS>();
                Mapper.CreateMap<OWHS, Warehouse>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OWHS> mOWHS = mDbContext.OWHS.ToList();

                return Mapper.Map<List<Warehouse>>(mOWHS);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetWarehouseList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="WhsCode"></param>
        /// <param name="WhsName"></param>
        /// <returns></returns>
        public List<Warehouse> GetWarehouseListSearch(CompanyConn pCompanyParam, string WhsCode, string WhsName)
        {
            try
            {
                Mapper.CreateMap<Warehouse, OWHS>();
                Mapper.CreateMap<OWHS, Warehouse>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OWHS> mOWHS = mDbContext.OWHS.Where(c => (!string.IsNullOrEmpty(WhsCode) ? c.WhsCode == WhsCode : true) && (!string.IsNullOrEmpty(WhsName) ? c.WhsName.Contains(WhsName) : true)).ToList();

                return Mapper.Map<List<Warehouse>>(mOWHS);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetWarehouseListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<DistrRuleSAP> GetDistributionRuleList(CompanyConn pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<DistrRuleSAP, OOCR>();
                Mapper.CreateMap<OOCR, DistrRuleSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OOCR> mOOCR = mDbContext.OOCR.ToList();

                return Mapper.Map<List<DistrRuleSAP>>(mOOCR);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetDistributionRuleList :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pTableID"></param>
        /// <returns></returns>
        public List<UDF_SAP> GetUDFByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            try
            {
                Mapper.CreateMap<UDF_SAP, CUFD>();
                Mapper.CreateMap<CUFD, UDF_SAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<CUFD> mCUFD = mDbContext.CUFD.Where(c => c.TableID == pTableID && (pTableID.Contains("@ARGNS") ? c.AliasID.Contains("ARGNS") : true)).ToList();

                return Mapper.Map<List<UDF_SAP>>(mCUFD);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetUDFByTableID :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pTableID"></param>
        /// <returns></returns>
        public List<UFD1_SAP> GetUDF1ByTableID(CompanyConn pCompanyParam, string pTableID)
        {
            try
            {
                Mapper.CreateMap<UFD1_SAP, UFD1>();
                Mapper.CreateMap<UFD1, UFD1_SAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<UFD1> mUFD1 = mDbContext.UFD1.Where(c => c.TableID == pTableID).ToList();

                return Mapper.Map<List<UFD1_SAP>>(mUFD1);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetUDF1ByTableID :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pQuery"></param>
        /// <returns></returns>
        public string GetQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            try
            {
                DataTable dtSQLResults = new DataTable();
                List<Object> mListReturn = new List<Object>();

                using (SqlConnection oSQLConnection = new SqlConnection(Utils.ConnectionString(pCompanyParam)))
                {
                    oSQLConnection.Open();
                    SqlDataAdapter datosSQLDA = new SqlDataAdapter();

                    System.Data.SqlClient.SqlCommand comando = new SqlCommand();
                    comando.CommandType = CommandType.Text;

                    comando.CommandText = pQuery;
                    comando.Connection = oSQLConnection;
                    datosSQLDA = new SqlDataAdapter(comando);
                    datosSQLDA.Fill(dtSQLResults);
                }

                var list = new List<Dictionary<string, object>>();
                foreach (DataRow row in dtSQLResults.Rows)
                {
                    var dict = new Dictionary<string, object>();
                    foreach (DataColumn col in dtSQLResults.Columns)
                    {
                        dict[col.ColumnName] = row[col];
                    }
                    list.Add(dict);
                }

                string mJsonToReturn = JsonConvert.SerializeObject(list);
                return mJsonToReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetQueryResult :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public List<AlertSAP> GetAlertsByUser(CompanyConn pCompanyParam, int pUser)
        {
            try
            {

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<AlertSAP> ret = new List<AlertSAP>();

                ret = (from mOALR in mDbContext.OALR
                       join mOAIB in mDbContext.OAIB on mOALR.Code equals mOAIB.AlertCode
                       join mOUSR in mDbContext.OUSR on mOALR.UserSign equals mOUSR.USERID into group1
                       from mGroup1 in group1.DefaultIfEmpty()
                       where
                       (
                             mOAIB.WasRead == "N" &&
                             mOAIB.UserSign == pUser
                       )
                       select new AlertSAP()
                       {
                           AlertCode = mOALR.Code,
                           UserText = mOALR.UserText,
                           Type = mOALR.Type,
                           RecDate = mOAIB.RecDate,
                           Subject = mOALR.Subject,
                           UserSign = mOALR.UserSign,
                           UserName = (mOALR.UserSign != -1) ? mGroup1.U_NAME : "Server"
                       }).ToList();

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetAlertsByUser :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pAlertId"></param>
        /// <returns></returns>
        public AlertSAP GetAlertById(CompanyConn pCompanyParam, int pAlertId)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<ALR2, AlertColumnSAP>();
                Mapper.CreateMap<AlertColumnSAP, ALR2>();
                Mapper.CreateMap<ALR3, AlertRowDataSAP>();
                Mapper.CreateMap<AlertRowDataSAP, ALR3>();

                AlertSAP ret = new AlertSAP();
                ret.AlertCode = pAlertId;
                ret.AlertColumns = Mapper.Map<List<AlertColumnSAP>>(mDbContext.ALR2.Where(c => c.Code == pAlertId).ToList());
                ret.AlertRowData = Mapper.Map<List<AlertRowDataSAP>>(mDbContext.ALR3.Where(c => c.Code == pAlertId).ToList());

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetAlertById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<QuerySAP> GetSAPQueryListSearch(CompanyConn pCompanyParam)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<OUQR, QuerySAP>();
                Mapper.CreateMap<QuerySAP, OUQR>();
                Mapper.CreateMap<OQCN, QueryCategorySAP>();
                Mapper.CreateMap<QueryCategorySAP, OQCN>();

                List<QuerySAP> mListQuerySAP = new List<QuerySAP>();
                List<QueryCategorySAP> mListQueryCategorySAP = new List<QueryCategorySAP>();
                mListQueryCategorySAP = Mapper.Map<List<QueryCategorySAP>>(mDbContext.OQCN.Where(c => c.PermMask.Contains("Y")).ToList());
                List<int> mListAuxQueryCatIds = mListQueryCategorySAP.Select(j => j.CategoryId).ToList();
                mListQuerySAP = Mapper.Map<List<QuerySAP>>(mDbContext.OUQR.Where(c => mListAuxQueryCatIds.Contains(c.QCategory)).ToList());
                mListQuerySAP.ForEach(c => c.CategorySAP = mListQueryCategorySAP.Where(j => j.CategoryId == c.QCategory).FirstOrDefault());

                return mListQuerySAP;
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetSAPQueryListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pQuery"></param>
        /// <returns></returns>
        public JsonObjectResult GetSAPQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            this.TestGetItemPrice(pCompanyParam);

            CompanyServiceWeb.CompanyService oService = new CompanyServiceWeb.CompanyService();

            CompanyServiceWeb.MsgHeader header = new CompanyServiceWeb.MsgHeader();
            header.ServiceName = CompanyServiceWeb.MsgHeaderServiceName.CompanyService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;
            oService.MsgHeaderValue = header;

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            try
            {
                CompanyServiceWeb.RecordsetParams query = new CompanyServiceWeb.RecordsetParams();
                query.Query = pQuery;
                Recordset mQueryResult = oService.Query(query);
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();
                mJsonObjectResult.Others.Add("jsonQueryResult", JsonConvert.SerializeObject(mQueryResult));
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetSAPQueryResult :" + ex.Message);
                mJsonObjectResult.Others.Add("jsonQueryResult", null);
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        private void TestGetItemPrice(CompanyConn pCompanyParam)
        {
            CompanyServiceWeb.CompanyService oService = new CompanyServiceWeb.CompanyService();

            CompanyServiceWeb.MsgHeader header = new CompanyServiceWeb.MsgHeader();
            header.ServiceName = CompanyServiceWeb.MsgHeaderServiceName.CompanyService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;
            oService.MsgHeaderValue = header;

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            try
            {
                ItemPriceParams mItemPriceParams = new ItemPriceParams();
                mItemPriceParams.ItemCode = "COMPRA";
                mItemPriceParams.CardCode = "C00001";
                mItemPriceParams.Currency = "$";
                mItemPriceParams.Date = DateTime.Today;
                mItemPriceParams.DateSpecified = true;
                mItemPriceParams.InventoryQuantity = 1;
                mItemPriceParams.InventoryQuantitySpecified = true;
                mItemPriceParams.PriceList = 1;
                mItemPriceParams.PriceListSpecified = true;
                mItemPriceParams.UoMEntry = 1;
                mItemPriceParams.UoMEntrySpecified = true;
                mItemPriceParams.UoMQuantity = 1;
                mItemPriceParams.UoMQuantitySpecified = true;


                ItemPriceReturnParams mResult = oService.GetItemPrice(mItemPriceParams);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetSAPQueryResult :" + ex.Message);
            }
        }

        public List<PriceListSAP> GetAllPriceListSAP(CompanyConn pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<PriceListSAP, OPLN>();
                Mapper.CreateMap<OPLN, PriceListSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OPLN> mOPLNList = mDbContext.OPLN.ToList();

                return Mapper.Map<List<PriceListSAP>>(mOPLNList);
            }
            catch (Exception ex)
            {
                Logger.WriteError("UserDS -> GetAllPriceListSAP :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }
    }
}