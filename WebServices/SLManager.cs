﻿using ARGNS.Model.Implementations;
using ARGNS.Util;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace WebServices
{
    public static class SLManager
    {

        public static IRestResponse callAPI(CompanyConn companyConn, string endPoint, Method method, string body)
        {
            EnableTrustedHosts();

            string urlEndPoint = companyConn.UrlSL + endPoint;

            var client = new RestClient(urlEndPoint);

            var request = new RestRequest();

            request.Method = method;
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("B1SESSION", companyConn.DSSessionId, ParameterType.Cookie);
            request.AddParameter("application/json", body, ParameterType.RequestBody);

            if (method == Method.PATCH)
            {
                request.AddHeader("B1S-ReplaceCollectionsOnPatch", "true");
            }

            var response = client.Execute(request);

            return response;
        }

        public static string login(CompanyConn companyConn)
        {
            EnableTrustedHosts();

            string urlEndPoint = companyConn.UrlSL + "Login";

            RestClient client = new RestClient(urlEndPoint);

            var request = new RestRequest(urlEndPoint, Method.POST)
            {
                RequestFormat = DataFormat.Json
            };

            var body = "{\"CompanyDB\":\"" + companyConn.CompanyDB + "\",\"Password\":\"" + companyConn.Password + "\",\"UserName\": \"" + companyConn.UserName + "\"}";
            request.AddParameter("application/json", body, ParameterType.RequestBody);

            var response = client.Execute(request);

            var result = JsonConvert.DeserializeObject<resultLogin>(response.Content);

            return result.SessionId;

        }

        public static string GetQueryResult(CompanyConn pCompanyParam, string pQuery)
        {
            try
            {
                dynamic mJsonObj = new ExpandoObject();
                mJsonObj.paramSQL = Convert.ToString(pQuery);
                mJsonObj.paramSchema = pCompanyParam.CompanyDB;
                var data = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(mJsonObj));

                var request = (HttpWebRequest)WebRequest.Create(pCompanyParam.UrlHanaGetQueryResult);
                request.Method = "POST";
                request.ContentType = "application/json;odata=minimalmetadata";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                Uri uri = new Uri(pCompanyParam.UrlHanaGetQueryResult);
                var cache = new CredentialCache();
                cache.Add(uri, "Basic", new NetworkCredential(pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                request.Credentials = cache;

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


                return responseString;

            }
            catch (Exception ex)
            {
                Logger.WriteError("SLManager -> GetQueryResult :" + ex.Message);
                return null;
            }
        }

        private static void EnableTrustedHosts()
        {
            ServicePointManager.ServerCertificateValidationCallback =
            (sender, certificate, chain, errors) =>
            {
                return true;
            };
        }

        private class resultLogin
        {
            public string SessionId { get; set; }
            public string Version { get; set; }
            public long SessionTimeout { get; set; }
        }

        public class Message
        {
            public string lang { get; set; }
            public string value { get; set; }
        }

        public class Error
        {
            public int code { get; set; }
            public Message message { get; set; }
        }

        public class Root
        {
            public Error error { get; set; }
        }

        private class message
        {
            public string value { get; set; }
        }




        public static string getMasterData(CompanyConn pCompanyParam, string table, string condition, bool HANA)
        {

            try
            {

                string from = HANA == true ? $"\"@{table}\"" : $"[@{table}]";
                string where = condition == string.Empty ? string.Empty : $"where {condition}";

                string query = $"select * from {pCompanyParam.CompanyDB}.{from} {where}";

                dynamic value = JsonConvert.DeserializeObject(GetQueryResult(pCompanyParam, query));

                foreach (var masterDetail in value)
                {
                    return (string)masterDetail.Name;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("SLManager -> getMasterData:" + ex.Message);
            }
            return "";
        }

    }




}