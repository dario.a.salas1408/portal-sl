﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using static ARGNS.Util.Enums;

namespace WebServices.DocumentsSAP.GeneralDocuments
{
    [ServiceContract]
    public interface IDocumentService
    {

        #region Common Method

        [OperationContract]
        DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCompanyParam);

        [OperationContract]
        List<GLAccountSAP> GetGLAccountList(CompanyConn pCompanyParam);

        // GetSystemCurrency ------------------------------------------------
        [OperationContract]
        string GetSystemCurrency(CompanyConn pCompanyParam);

        // GetLocalCurrency ------------------------------------------------
        [OperationContract]
        string GetLocalCurrency(CompanyConn pCompanyParam);

        // GetCurrencyRate  ------------------------------------------------
        [OperationContract]
        string GetCurrencyRate(string pCurrency, DateTime pDate, CompanyConn pCompanyParam);

        //GetTaxCode-------------------------------------------------------
        [OperationContract]
        List<SalesTaxCodesSAP> GetTaxCode(CompanyConn pCompanyParam);

        //GetBranches-------------------------------------------------------
        [OperationContract]
        List<BranchesSAP> GetBranchesByUser(CompanyConn pCompanyParam, string userSAP);

        //GetSerialesByDocument-------------------------------------------------------
        [OperationContract]
        SerialSAP GetSerialsByDocument(CompanyConn pCompanyParam, int docNum, int line, string whsCode, DocumentsType docType, string itemCode, List<UDF_ARGNS> pListUDF = null, int anio = 0);


        //GetPaymentMeans-------------------------------------------------------
        [OperationContract]
        List<PaymentMean> GetPaymentMeans(CompanyConn pCompanyParam, string branchCode);

        //GetPaymentMeanType-------------------------------------------------------
        [OperationContract]
        List<PaymentMeanType> GetPaymentMeanType(CompanyConn pCompanyParam);


        #endregion


        #region Purchase Order

        [OperationContract]
        List<Freight> GetPurchaseOrderFreights(CompanyConn pCompanyParam, int pDocEntry);

        [OperationContract]
        JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        //[OperationContract]
        //PurchaseOrderSAP GetPurchaseOrderById(CompanyConn pCompanyParam, int pCode);

        [OperationContract]
        string AddPurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdatePurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam);

        #endregion


        #region Purchase Quotation

        [OperationContract]
        List<Freight> GetPurchaseQuotationFreights(CompanyConn pCompanyParam, int pDocEntry);

        [OperationContract]
        JsonObjectResult GetPurchaseQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        List<PurchaseQuotationSAPLine> GetPurchaseQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments);

        [OperationContract]
        string AddPurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdatePurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam);

        #endregion


        #region Purchase Invoice

        [OperationContract]
        JsonObjectResult GetPurchaseInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        //[OperationContract]
        //PurchaseInvoiceSAP GetPurchaseInvoiceById(CompanyConn pCompanyParam, int pCode);

        #endregion


        #region Sales Order

        [OperationContract]
        List<Freight> GetSalesOrderFreights(CompanyConn pCompanyParam, int pDocEntry);

        //[OperationContract]
        //SaleOrderSAP GetSaleOrderById(CompanyConn pCompanyParam, int pCode);

        [OperationContract]
        JsonObjectResult AddSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        JsonObjectResult GetSalesOrderListSearch(CompanyConn pCompanyParam,
            bool pShowOpenQuantityStatus, string pCodeVendor = "",
            DateTime? pDate = null, int? pDocNum = null,
            string pDocStatus = "", string pOwnerCode = "",
            string pSECode = "", int pStart = 0,
            int pLength = 0, OrderColumn pOrderColumn = null);

        #endregion


        #region Sales Quotation

        [OperationContract]
        List<Freight> GetSalesQuotationFreights(CompanyConn pCompanyParam, int pDocEntry);

        //[OperationContract]
        //SalesQuotationSAP GetSalesQuotationById(CompanyConn pCompanyParam, int pCode);

        [OperationContract]
        JsonObjectResult AddSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        JsonObjectResult GetSalesQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        List<SalesQuotationSAPLine> GetSalesQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments);

        #endregion


        #region Sales Invoice

        [OperationContract]
        JsonObjectResult GetSalesInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        //[OperationContract]
        //SalesInvoiceSAP GetSalesInvoiceById(CompanyConn pCompanyParam, int pCode);

        #endregion


        #region PurchaseRequest

        [OperationContract]
        JsonObjectResult GetPurchaseRequestListSearch(CompanyConn pCompanyParam, string pRequest = "", DateTime? pDate = null, int? pDocNum = null, int? pDepartment = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        List<PurchaseRequestLineSAP> GetPurchaseRequestLinesSearch(CompanyConn pCompanyParam, string[] pDocuments);

        //[OperationContract]
        //PurchaseRequestSAP GetPurchaseRequestById(CompanyConn pCompanyParam, int pCode);

        [OperationContract]
        PurchaseRequestCombo GetPurchaseRequestCombo(CompanyConn pCompanyParam);

        [OperationContract]
        List<Freight> GetPurchaseRequestFreights(CompanyConn pCompanyParam, int pDocEntry);

        [OperationContract]
        string AddPurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdatePurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam);

        #endregion


        #region Approval

        [OperationContract]
        List<DocumentConfirmationLines> GetPurchaseApprovalListSearch(CompanyConn pCompanyParam, int pUserSign,
            DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "");

        [OperationContract]
        List<DocumentConfirmationLines> GetPurchaseApprovalByOriginator(CompanyConn pCompanyParam, int pUserSign,
            DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "");

        [OperationContract]
        List<DocumentConfirmationLines> GetSalesApprovalListSearch(CompanyConn pCompanyParam,
            int pUserSign, DateTime? pDocDate = null,
            string pDocStatus = "", string pObjectId = "");

        [OperationContract]
        List<DocumentConfirmationLines> GetSalesApprovalByOriginator(CompanyConn pCompanyParam, int pUserSign,
            DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "");

        [OperationContract]
        List<DocumentConfirmationLines> GetApprovalListByID(CompanyConn pCompanyParam, int pWddCode);

        [OperationContract]
        List<DocumentConfirmationLines> GetApprovalListByDocumentId(CompanyConn pCompanyParam, int pDocEntry, string pObjType);

        [OperationContract]
        List<StageSAP> GetStageList(CompanyConn pCompanyParam);

        [OperationContract(Name = "SaveApprovalResponseDIServer")]
        string SaveApprovalResponse(int owwdCode, string remark, string UserNameSAP, string UserPasswordSAP, string approvalCode, CompanyConn pCompanyParam);

        #endregion


        #region Draft

        [OperationContract]
        JsonObjectResult GetPurchaseUserDraftListSearch(CompanyConn pCompanyParam, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        JsonObjectResult GetPurchaseSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        JsonObjectResult GetPurchaseCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        JsonObjectResult GetSalesUserDraftListSearch(CompanyConn pCompanyParam, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        JsonObjectResult GetSalesSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        JsonObjectResult GetSalesCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null);

        [OperationContract]
        Draft GetDraftById(CompanyConn pCompanyParam, int pCode);

        [OperationContract(Name = "CreateDocumentDIServer")]
        string CreateDocument(CompanyConn pCompanyParam, int docEntry, string pSession);



        #endregion


        #region Warehouse

        [OperationContract]
        List<Warehouse> GetListWarehouse(CompanyConn pCompanyParam);

        #endregion


        #region

        [OperationContract]
        PurchaseOrderSAP GetAllbyPO(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPOR = null, List<UDF_ARGNS> pListUDFPOR1 = null);
        [OperationContract]
        PurchaseQuotationSAP GetAllbyPQ(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPQT = null, List<UDF_ARGNS> pListUDFPQT1 = null);
        [OperationContract]
        PurchaseRequestSAP GetAllbyPR(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOPRQ = null, List<UDF_ARGNS> pListUDFPRQ1 = null);
        [OperationContract]
        PurchaseInvoiceSAP GetAllbyPI(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOPCH = null, List<UDF_ARGNS> pListUDFPCH1 = null);
        [OperationContract]
        SaleOrderSAP GetAllbySO(CompanyConn pCompanyParam, int pCode, bool pShowOpenQuantity, List<UDF_ARGNS> pListUDFORDR = null, List<UDF_ARGNS> pListUDFRDR1 = null, List<UDF_ARGNS> pListUDFCRD1 = null);
        [OperationContract]
        SalesQuotationSAP GetAllbySQ(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOQUT = null, List<UDF_ARGNS> pListUDFQUT1 = null);
        [OperationContract]
        SalesInvoiceSAP GetAllbySI(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOINV = null, List<UDF_ARGNS> pListUDFINV1 = null);
        #endregion


        #region Opornutities

        [OperationContract]
        OpportunitiesSAPLine GetOpportunityLine(CompanyConn pCompanyParam);

        [OperationContract]
        OpportunityStage GetStageById(CompanyConn pCompanyParam, int pId);

        [OperationContract]
        OpportunitiesSAP GetOportunityById(CompanyConn pCompanyParam, int pId);

        [OperationContract]
        string AddOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        string UpdateOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam);

        [OperationContract]
        List<OpportunitiesSAP> GetOpportunityListSearch(CompanyConn pCompanyParam, string pCodeBP = "", int? pSalesEmp = null, string pOpportunityName = "", int? pOwner = null, DateTime? pDate = null, string pStatus = "");


        #endregion

        #region Projects

        [OperationContract]
        List<ProjectSAP> GetProjectsSAPList(CompanyConn pCompanyParam, string pProjectCode, string pProjectName);

        #endregion

        #region Payment Means
        [OperationContract]
        List<PaymentMeanOrder> GetForDraft(CompanyConn pCompanyParam, int pCode);
        #endregion

    }
}
