﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.PDM;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Model.Interfaces.SAP;
using ARGNS.Util;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WebServices.B1IFDocumentService;
using WebServices.DocumentsSAP.DocumentProcess;
using WebServices.Model;
using WebServices.Model.Tables;
using static ARGNS.Util.Enums;

namespace WebServices.DocumentsSAP.GeneralDocuments
{
    public class DocumentService : IDocumentService
    {
        private IDocumentService mDocumentService = null;
        private DataBase mDbContext;

        public DocumentService()
        {
            Database.SetInitializer<DataBase>(null);
            mDbContext = new DataBase();

            Mapper.CreateMap<OUDP, Department>();
            Mapper.CreateMap<OUBR, Branch>();
            Mapper.CreateMap<OACT, GLAccountSAP>();
        }

        private void InitializeService(CompanyConn pCompanyParam)
        {
            if (pCompanyParam.ServerType == (int)Enums.eServerType.dst_HANA)
            {
                mDocumentService = new DocumentServiceSL();
            }
            else
            {
                mDocumentService = new DocumentServiceDS();
            }
        }


        #region Common Method

        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetDocumentSAPCombo(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetDocumentSAPCombo :" + ex.Message);
                return null;
            }
        }

        public List<GLAccountSAP> GetGLAccountList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetGLAccountList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetGLAccountList :" + ex.Message);
                return null;
            }
        }

        public string GetSystemCurrency(CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mDocumentService.GetSystemCurrency(pCompanyParam);
        }

        public string GetLocalCurrency(CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mDocumentService.GetLocalCurrency(pCompanyParam);
        }

        public string GetCurrencyRate(string pCurrency, DateTime pDate, CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mDocumentService.GetCurrencyRate(pCurrency, pDate, pCompanyParam);
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyConn pCompanyParam)
        {
            InitializeService(pCompanyParam);
            return mDocumentService.GetTaxCode(pCompanyParam);
        }

        public List<BranchesSAP> GetBranchesByUser(CompanyConn pCompanyParam, string userSAP)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetBranchesByUser(pCompanyParam, userSAP);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetBranchesByUser :" + ex.Message);
                return null;
            }
        }

        public SerialSAP GetSerialsByDocument(CompanyConn pCompanyParam, int docNum, int line, string whsCode, DocumentsType docType, string itemCode, List<UDF_ARGNS> pListUDF = null, int anio = 0)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSerialsByDocument(pCompanyParam, docNum, line, whsCode, docType, itemCode, pListUDF, anio);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSerialsByDocument :" + ex.Message);
                return null;
            }
        }

        public List<PaymentMean> GetPaymentMeans(CompanyConn pCompanyParam, string branchCode)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPaymentMeans(pCompanyParam, branchCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPaymentMeans :" + ex.Message);
                return null;
            }
        }

        public List<PaymentMeanType> GetPaymentMeanType(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPaymentMeanType(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPaymentMeanType :" + ex.Message);
                return null;
            }
        }


       
        public List<PaymentMeanOrder> GetForDraft(CompanyConn pCompanyParam, int pCode)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetForDraft(pCompanyParam, pCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetForDraft :" + ex.Message);
                return null;
            }
        }
       

        #endregion


        #region Purchase Order

        public List<Freight> GetPurchaseOrderFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseOrderFreights(pCompanyParam, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetFreights :" + ex.Message);
                return null;
            }
        }

        public List<Freight> GetSalesOrderFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesOrderFreights(pCompanyParam, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesOrderFreights :" + ex.Message);
                return null;
            }
        }

        public List<Freight> GetSalesQuotationFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesQuotationFreights(pCompanyParam, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesQuotationFreights :" + ex.Message);
                return null;
            }
        }

        public List<Freight> GetPurchaseRequestFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseRequestFreights(pCompanyParam, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseRequestFreights :" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseOrderListSearch(pCompanyParam, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderService -> GetPurchaseOrderList :" + ex.Message);
                return null;
            }
        }

        public PurchaseOrderSAP GetAllbyPO(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPOR = null, List<UDF_ARGNS> pListUDFPOR1 = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetAllbyPO(pCompanyParam, pDocEntry, pListUDFOPOR, pListUDFPOR1);

            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderService -> GetAllbyPO :" + ex.Message);

                return null;
            }
        }

        public string AddPurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.AddPurchaseOrder(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.UpdatePurchaseOrder(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> UpdatePurchaseOrder :" + ex.Message);
                return "Error:" + ex.Message;
            }
        }


        #endregion


        #region Purchase Quotation

        public JsonObjectResult GetPurchaseQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseQuotationListSearch(pCompanyParam, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseQuotationListSearch :" + ex.Message);
                return null;
            }
        }

        public List<PurchaseQuotationSAPLine> GetPurchaseQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseQuotationLinesSearch(pCompanyParam, pDocuments);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseQuotationLinesSearch :" + ex.Message);
                return null;
            }
        }

        public PurchaseQuotationSAP GetAllbyPQ(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPQT = null, List<UDF_ARGNS> pListUDFPQT1 = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetAllbyPQ(pCompanyParam, pDocEntry, pListUDFOPQT, pListUDFPQT1);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetAllbyPQ :" + ex.Message);
                return null;
            }
        }

        public List<Freight> GetPurchaseQuotationFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseQuotationFreights(pCompanyParam, pDocEntry);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseQuotationFreights :" + ex.Message);
                return null;
            }
        }

        public string AddPurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.AddPurchaseQuotation(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                return "DocumentService -> AddPurchaseQuotation" + ex.Message;
            }
        }

        public string UpdatePurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.UpdatePurchaseQuotation(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                return "DocumentService -> UpdatePurchaseQuotation" + ex.Message;
            }
        }

        #endregion


        #region Purchase Request

        public JsonObjectResult GetPurchaseRequestListSearch(CompanyConn pCompanyParam, string pRequester = "", DateTime? pDate = null, int? pDocNum = null, int? pDepartment = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseRequestListSearch(pCompanyParam, pRequester, pDate, pDocNum, pDepartment, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderService -> GetPurchaseRequestListSearch :" + ex.Message);
                return null;
            }
        }

        public List<PurchaseRequestLineSAP> GetPurchaseRequestLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseRequestLinesSearch(pCompanyParam, pDocuments);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseRequestLinesSearch :" + ex.Message);
                return null;
            }
        }

        public PurchaseRequestSAP GetAllbyPR(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOPRQ = null, List<UDF_ARGNS> pListUDFPRQ1 = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetAllbyPR(pCompanyParam, pCode, pListUDFOPRQ, pListUDFPRQ1);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetAllbyPR :" + ex.Message);
                return null;
            }
        }

        public PurchaseRequestCombo GetPurchaseRequestCombo(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseRequestCombo(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseRequestCombo :" + ex.Message);
                return null;
            }
        }

        public string AddPurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.AddPurchaseRequest(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                return "DocumentService -> AddPurchaseRequest" + ex.Message;
            }
        }

        public string UpdatePurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.UpdatePurchaseRequest(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                return "DocumentService -> UpdatePurchaseRequest" + ex.Message;
            }
        }

        #endregion


        #region Purchase Invoice

        public JsonObjectResult GetPurchaseInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseInvoiceListSearch(pCompanyParam, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseInvoiceListSearch :" + ex.Message);
                return null;
            }
        }

        public PurchaseInvoiceSAP GetAllbyPI(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOPCH = null, List<UDF_ARGNS> pListUDFPCH1 = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetAllbyPI(pCompanyParam, pCode, pListUDFOPCH, pListUDFPCH1);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseInvoiceById :" + ex.Message);
                return null;
            }
        }

        #endregion


        #region Sales Order

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesOrderListSearch(CompanyConn pCompanyParam,
            bool pShowOpenQuantityStatus,
            string pCodeVendor = "", DateTime? pDate = null,
            int? pDocNum = null, string pDocStatus = "",
            string pOwnerCode = "", string pSECode = "",
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesOrderListSearch(pCompanyParam,
                    pShowOpenQuantityStatus, pCodeVendor,
                    pDate, pDocNum, pDocStatus,
                    pOwnerCode, pSECode, pStart,
                    pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesOrderService -> GetSalesOrderListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pListUDFORDR"></param>
        /// <param name="pListUDFRDR1"></param>
        /// <param name="pListUDFCRD1"></param>
        /// <returns></returns>
        public SaleOrderSAP GetAllbySO(CompanyConn pCompanyParam, int pCode,
            bool pShowOpenQuantity,
            List<UDF_ARGNS> pListUDFORDR = null,
            List<UDF_ARGNS> pListUDFRDR1 = null,
            List<UDF_ARGNS> pListUDFCRD1 = null)
        {

            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetAllbySO(pCompanyParam, pCode,
                    pShowOpenQuantity, pListUDFORDR, pListUDFRDR1, pListUDFCRD1);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderService -> GetAllbySO :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult AddSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.AddSaleOrder(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SaleOrderService -> AddSaleOrder :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.UpdateSaleOrder(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                return "DocumentService -> UpdateSaleOrder" + ex.Message;
            }

        }

        #endregion


        #region Sales Quotation

        public JsonObjectResult GetSalesQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesQuotationListSearch(pCompanyParam, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesQuotationService -> GetSalesQuotationListSearch :" + ex.Message);
                return null;
            }
        }

        public List<SalesQuotationSAPLine> GetSalesQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesQuotationLinesSearch(pCompanyParam, pDocuments);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderService -> GetSalesQuotationLinesSearch :" + ex.Message);
                return null;
            }
        }

        public SalesQuotationSAP GetAllbySQ(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOQUT = null, List<UDF_ARGNS> pListUDFQUT1 = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetAllbySQ(pCompanyParam, pCode, pListUDFOQUT, pListUDFQUT1);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesQuotationService -> GetAllbySQ :" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult AddSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.AddSalesQuotation(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesQuotationService -> AddSalesQuotation :" + ex.Message);
                return null;
            }
        }

        public string UpdateSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.UpdateSalesQuotation(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("SalesQuotationService -> UpdateSalesQuotation :" + ex.Message);
                return null;
            }
        }

        #endregion


        #region Sales Invoice

        public JsonObjectResult GetSalesInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesInvoiceListSearch(pCompanyParam, pCodeVendor, pDate, pDocNum, pDocStatus, pOwnerCode, pSECode, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesInvoiceListSearch :" + ex.Message);
                return null;
            }
        }

        public SalesInvoiceSAP GetAllbySI(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOINV = null, List<UDF_ARGNS> pListUDFINV1 = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetAllbySI(pCompanyParam, pCode, pListUDFOINV, pListUDFINV1);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetAllbySI :" + ex.Message);
                return null;
            }
        }

        #endregion

        #region Draft

        public JsonObjectResult GetPurchaseUserDraftListSearch(CompanyConn pCompanyParam, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseUserDraftListSearch(pCompanyParam, pUserSign, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseUserDraftListSearch:" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult GetPurchaseSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseSalesEmployeeDraftListSearch(pCompanyParam, pSalesEmployee, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseSalesEmployeeDraftListSearch:" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult GetPurchaseCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseCustomerDraftListSearch(pCompanyParam, pBP, pDocDateFrom, pDocDateTo, pReqDate, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseCustomerDraftListSearch:" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult GetSalesUserDraftListSearch(CompanyConn pCompanyParam, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesUserDraftListSearch(pCompanyParam, pUserSign, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesUserDraftListSearch:" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult GetSalesSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesSalesEmployeeDraftListSearch(pCompanyParam, pSalesEmployee, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesSalesEmployeeDraftListSearch:" + ex.Message);
                return null;
            }
        }

        public JsonObjectResult GetSalesCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesCustomerDraftListSearch(pCompanyParam, pBP, pDocDateFrom, pDocDateTo, pDocType, pDocStatus, pCodeVendor, pDocNum, pStart, pLength, pOrderColumn);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesCustomerDraftListSearch:" + ex.Message);
                return null;
            }
        }

        public Draft GetDraftById(CompanyConn pCompanyParam, int pCode)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetDraftById(pCompanyParam, pCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetDraftById:" + ex.Message);
                return null;
            }
        }

        public string CreateDocument(CompanyConn pCompanyParam, int docEntry, string pSession)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.CreateDocument(pCompanyParam, docEntry, pSession);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> CreateDocument:" + ex.Message);
                return null;
            }
        }

        #endregion


        #region Approval

        public List<DocumentConfirmationLines> GetPurchaseApprovalListSearch(CompanyConn pCompanyParam, int pUserSign,
            DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseApprovalListSearch(pCompanyParam, pUserSign, pDocDate, pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseApprovalListSearch:" + ex.Message);
                return null;
            }
        }

        public List<DocumentConfirmationLines> GetPurchaseApprovalByOriginator(CompanyConn pCompanyParam, int pUserSign,
            DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetPurchaseApprovalByOriginator(pCompanyParam, pUserSign, pDocDate, pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetPurchaseApprovalByOriginator:" + ex.Message);
                return null;
            }
        }


        public List<DocumentConfirmationLines> GetSalesApprovalListSearch(CompanyConn pCompanyParam,
            int pUserSign, DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesApprovalListSearch(pCompanyParam, pUserSign,
                    pDocDate, pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesApprovalListSearch:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDate"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pObjectId"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetSalesApprovalByOriginator(CompanyConn pCompanyParam,
            int pUserSign, DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetSalesApprovalByOriginator(pCompanyParam, pUserSign, pDocDate, pDocStatus, pObjectId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetSalesApprovalByOriginator:" + ex.Message);
                return null;
            }
        }

        public List<DocumentConfirmationLines> GetApprovalListByID(CompanyConn pCompanyParam, int pWddCode)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetApprovalListByID(pCompanyParam, pWddCode);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetApprovalListByID:" + ex.Message);
                return null;
            }
        }

        public List<DocumentConfirmationLines> GetApprovalListByDocumentId(CompanyConn pCompanyParam, int pDocEntry, string pObjType)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetApprovalListByDocumentId(pCompanyParam, pDocEntry, pObjType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetApprovalListByDocumentId:" + ex.Message);
                return null;
            }
        }

        public List<StageSAP> GetStageList(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetStageList(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetStageList:" + ex.Message);
                return null;
            }
        }

        public string SaveApprovalResponse(int owwdCode, string remark, string UserNameSAP, string UserPasswordSAP, string approvalCode, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.SaveApprovalResponse(owwdCode, remark, UserNameSAP, UserPasswordSAP, approvalCode, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> SaveApprovalResponse:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion


        #region Warehouse

        public List<Warehouse> GetListWarehouse(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetListWarehouse(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetListWarehouse:" + ex.Message);
                return null;
            }

        }

        #endregion


        #region Oportunities

        public OpportunitiesSAPLine GetOpportunityLine(CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetOpportunityLine(pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetStageById :" + ex.Message);
                return null;
            }
        }

        public OpportunityStage GetStageById(CompanyConn pCompanyParam, int pId)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetStageById(pCompanyParam, pId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetStageById :" + ex.Message);
                return null;
            }
        }


        public OpportunitiesSAP GetOportunityById(CompanyConn pCompanyParam, int pId)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetOportunityById(pCompanyParam, pId);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetOportunityById :" + ex.Message);
                return null;
            }
        }


        public string AddOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.AddOportunity(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> AddOportunity :" + ex.Message);
                return null;
            }
        }


        public string UpdateOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.UpdateOportunity(pObject, pCompanyParam);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> UpdateOportunity :" + ex.Message);
                return null;
            }
        }


        public List<OpportunitiesSAP> GetOpportunityListSearch(CompanyConn pCompanyParam, string pCodeBP = "", int? pSalesEmp = null, string pOpportunityName = "", int? pOwner = null, DateTime? pDate = null, string pStatus = "")
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetOpportunityListSearch(pCompanyParam, pCodeBP, pSalesEmp, pOpportunityName, pOwner, pDate, pStatus);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetOpportunityListSearch :" + ex.Message);
                return null;
            }
        }


        #endregion


        #region Oportunities

        public List<ProjectSAP> GetProjectsSAPList(CompanyConn pCompanyParam, string pProjectCode, string pProjectName)
        {
            try
            {
                InitializeService(pCompanyParam);
                return mDocumentService.GetProjectsSAPList(pCompanyParam, pProjectCode, pProjectName);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentService -> GetProjectsSAPList :" + ex.Message);
                return null;
            }
        }

        #endregion


    


    }
}
