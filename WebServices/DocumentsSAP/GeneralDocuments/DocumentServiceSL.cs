﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using WebServices.Model.Tables;
using AutoMapper;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Linq.Dynamic;
using ARGNS.Model.Interfaces.SAP;
using static ARGNS.Util.Enums;
using System.Xml.Linq;
using System.Web.Configuration;
using WebServices.Query;
using WebServices.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Dynamic;

namespace WebServices.DocumentsSAP.GeneralDocuments
{
    public class DocumentServiceSL : IDocumentService
    {
        private B1WSHandler mB1WSHandler;

        public DocumentServiceSL()
        {
            mB1WSHandler = new B1WSHandler();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCompanyParam)
        {

            DocumentSAPCombo retCb = new DocumentSAPCombo();
            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();
            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();
            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();
            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();
            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();
            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            try
            {
                //Buyer
                List<OSLP> mLisOSLP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSLP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Currency
                List<OCRN> mListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //PaymentMethod
                List<OCTG> mListOCTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMethod", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //ShippingType
                List<OSHP> mListOSHP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSHP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Taxt
                List<OVTG> mListOVTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OVTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Taxt", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

                retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

                retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

                retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

                retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

                retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

                return retCb;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetDocumentSAPCombo :" + ex.Message);
                return null;
            }
        }

        #region Common Method
        public List<GLAccountSAP> GetGLAccountList(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            string mFilters = "1 eq 1";

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", mFilters);

            List<GLAccountSAP> result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GLAccountSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCurrType"></param>
        /// <param name="pSession"></param>
        /// <returns></returns>
        public string GetCurrency(bool getLocalCurrency, CompanyConn pCompanyParam)
        {

            string returnAPI = string.Empty;

            try
            {

                //Poner Enum
                if (!getLocalCurrency)
                {

                    returnAPI = SLManager.callAPI(pCompanyParam, "SBOBobService_GetSystemCurrency", RestSharp.Method.POST, "").Content;

                }
                else
                {
                    returnAPI = SLManager.callAPI(pCompanyParam, "SBOBobService_GetLocalCurrency", RestSharp.Method.POST, "").Content;

                }

                try
                {
                    var result = JsonConvert.DeserializeObject<SLManager.Root>(returnAPI);

                    returnAPI = "Error:" + result.error.message.value;
                }
                catch (Exception)
                {

                }

                return returnAPI;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetCurrency:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCurrency"></param>
        /// <param name="pDate"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string GetCurrencyRate(string pCurrency, DateTime pDate, CompanyConn pCompanyParam)
        {
            //string mXmlResult = string.Empty;
            string returnAPI = string.Empty;
            //string pXmlString = string.Empty;
            //string AddCmd = string.Empty;
            //SBODI_Server.Node mServerNode = null;
            //XmlDocument mResultXML = null;

            try
            {
                //mResultXML = new XmlDocument();
                //mServerNode = new SBODI_Server.Node();


                //AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                //@"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                //"<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                //@"<env:Body><dis:GetCurrencyRate xmlns:dis=""http://www.sap.com/SBO/DIS""><Currency>" + pCurrency + "</Currency><Date>" + pDate.ToString("yyyyMMdd") + "</Date></dis:GetCurrencyRate></env:Body></env:Envelope>";

                var body = "{\"Currency\":\"" + pCurrency + "\",\"Date\":\"" + pDate.ToString("yyyyMMdd") + "\"}";
                returnAPI = SLManager.callAPI(pCompanyParam, "SBOBobService_GetCurrencyRate", RestSharp.Method.POST, body).Content;

                try
                {
                    var result = JsonConvert.DeserializeObject<SLManager.Root>(returnAPI);

                    returnAPI = "Error:" + result.error.message.value;
                }
                catch (Exception)
                {

                }

                ////Envio Solicitud al DI Server y Obtengo la Respuesta
                //mXmlResult = mServerNode.Interact(AddCmd);
                //mResultXML.LoadXml(mXmlResult);

                ////Recupero los Valores de la Respuesta                
                //return UtilWeb.GetResult(mResultXML, WebServices.UtilWeb.CurrentType.CurrencyRate.ToString());
                return returnAPI;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetCurrencyRate:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string GetLocalCurrency(CompanyConn pCompanyParam)
        {
            return GetCurrency(true, pCompanyParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string GetSystemCurrency(CompanyConn pCompanyParam)
        {
            return GetCurrency(false, pCompanyParam);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<SalesTaxCodesSAP> GetTaxCode(CompanyConn pCompanyParam)
        {
            List<SalesTaxCodesSAP> ret = null;
            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesTaxCodes");
                ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesTaxCodesSAP>>(RESTService.GetRequestJson(mUrl,
                    WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetTaxCode:" + ex.Message);
            }

            return ret;
        }

        public List<BranchesSAP> GetBranchesByUser(CompanyConn pCompanyParam, string userSAP)
        {
            List<BranchesSAP> ret = null;
            string mUrl = string.Empty;
            try
            {

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OBPL", "Disabled eq 'N'");
                var OBPL = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OBPL>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "USR6", "UserCode eq '" + userSAP + "'");
                var USR6 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<USR6>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                ret = (from c in USR6 join j in OBPL on c.BPLId equals j.BPLId where c.UserCode == userSAP && j.Disabled == "N" select new BranchesSAP { BPLId = j.BPLId, BPLName = j.BPLName }).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetBranchesByUser:" + ex.Message);
            }

            return ret;
        }

        public SerialSAP GetSerialsByDocument(CompanyConn pCompanyParam, int docNum, int line, string whsCode, DocumentsType docType, string itemCode, List<UDF_ARGNS> pListUDF = null, int anio = 0)
        {
            SerialSAP serialSAP = new SerialSAP();
            var listMemory = new List<ObjectMemory>();

            try
            {
                string mUrl = string.Empty;
                var whsUseBin = false;
                var where = string.Empty;
                var wherebySAP = string.Empty;
                var whereLine = string.Empty;
                var whereAnio = string.Empty;
                //var itemCode = string.Empty;
                serialSAP.SerialsSAP = new List<Serial>();

                mB1WSHandler = new B1WSHandler();

                XDocument docQuery = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "/Query/HANA.xml");

                //Run query
                var QueryUDFs = (from UdfQuerySerials in docQuery.Elements("Query").Elements("UdfQuerySerials").Elements("value")
                                 select UdfQuerySerials.Value).FirstOrDefault();


                if (QueryUDFs != "")
                {
                    QueryUDFs = "AND " + QueryUDFs + "  ";
                }

                if (line < 0)
                {
                    whereLine = QueryResource.QueryGet("GetSerialsByDocument01") + $" > {line}";
                }
                else
                {
                    whereLine = QueryResource.QueryGet("GetSerialsByDocument01") + $" = {line}";
                }

                if (!string.IsNullOrEmpty(whsCode))
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", $"WhsCode eq '{whsCode}'");
                    var OWHS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWHS>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();
                    whsUseBin = OWHS.FirstOrDefault().BinActivat == "Y" ? true : false;
                }

                //where = $"WHERE T0.DocEntry = {docNum} AND T0.DocType = {(int)docType} AND {whereLine}";
                where = string.Format(QueryResource.QueryGet("GetSerialsByDocument02"), docNum, (int)docType, whereLine);

                if (whsUseBin)
                {
                    wherebySAP = string.Format(QueryResource.QueryGet("GetSerialsByDocument03"), whsCode, itemCode, pCompanyParam.CompanyDB);
                }
                else
                {
                    wherebySAP = string.Format(QueryResource.QueryGet("GetSerialsByDocument04"), itemCode, pCompanyParam.CompanyDB);
                }


                var filterByAnio = Convert.ToBoolean(WebConfigurationManager.AppSettings["filterByAnio"]);

                if (docNum > 0 && filterByAnio)
                {
                    var queryAnio = string.Format(QueryResource.QueryGet("FilterAnioByLine"), docNum, line);

                    XmlDocument xmlAnio = new XmlDocument();
                    xmlAnio.LoadXml(mB1WSHandler.ExecuetQuery(pCompanyParam.DSSessionId, queryAnio));

                    var nodesAnio = xmlAnio.SelectNodes("//*[local-name()='row']");
                    foreach (XmlNode row in nodesAnio)
                    {

                        anio = Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='U_anioSelected']").InnerText);
                    }
                }

                string rubro = string.Empty;
                var queryrubro = string.Format(QueryResource.QueryGet("getRubro"), itemCode);
                //XmlDocument mxlRubro = new XmlDocument();
                //mxlRubro.LoadXml(mB1WSHandler.ExecuetQuery(pCompanyParam.DSSessionId, queryrubro));

                dynamic rubroReturn = JsonConvert.DeserializeObject(SLManager.GetQueryResult(pCompanyParam, queryrubro));

                foreach (var item in rubroReturn)
                {
                    rubro = (string)item["U_Rubro"];
                }

                //var nodesRubro = mxlRubro.SelectNodes("//*[local-name()='row']");
                //foreach (XmlNode row in nodesRubro)
                //{
                //    rubro = row.SelectSingleNode(".//*[local-name()='U_Rubro']").InnerText;
                //}

                var avoidFilterAniobyRubro = Convert.ToString(WebConfigurationManager.AppSettings["avoidFilterAnioRubros"] == "" ? "" : WebConfigurationManager.AppSettings["avoidFilterAnioRubros"]).Split(',');

                if (anio != 0 && filterByAnio && !avoidFilterAniobyRubro.Contains(rubro))
                {
                    whereAnio = string.Format(QueryResource.QueryGet("FilterAnio"), anio);
                }


                var query = string.Format(QueryResource.QueryGet("GetSerialsByDocument05"), pCompanyParam.CompanyDB) + QueryUDFs + wherebySAP + whereAnio;
                query = query + QueryResource.QueryGet("GetSerialsByDocument06");

                var ListSerialSAP = JsonConvert.DeserializeObject<List<Serial>>(SLManager.GetQueryResult(pCompanyParam, query));


                serialSAP.SerialsSAP = ListSerialSAP;

                query = string.Format(QueryResource.QueryGet("GetSerialsByDocument07"), pCompanyParam.CompanyDB) + QueryUDFs + where;
                query = query + QueryResource.QueryGet("GetSerialsByDocument08");

                var SerialsInDocument = JsonConvert.DeserializeObject<List<Serial>>(SLManager.GetQueryResult(pCompanyParam, query));

                serialSAP.SerialsInDocument = SerialsInDocument;


                string distNumber = string.Empty;
                string sysNumber = string.Empty;

                if (serialSAP.SerialsSAP.Count() > 0)
                {

                    foreach (var item in serialSAP.SerialsSAP)
                    {
                        if (distNumber == "")
                        {
                            distNumber = "'" + item.DistNumber + "'";
                        }
                        else { distNumber = distNumber + ",'" + item.DistNumber + "'"; }

                        if (sysNumber == "")
                        {
                            sysNumber = item.SysNumber.ToString();
                        }
                        else { sysNumber = sysNumber + "," + item.SysNumber.ToString() + ""; }


                    }

                    query = string.Format(QueryResource.QueryGet("GetSerialsByDocument09"), distNumber, itemCode, sysNumber, pCompanyParam.CompanyDB);

                    dynamic Serials = JsonConvert.DeserializeObject(SLManager.GetQueryResult(pCompanyParam, query));

                    foreach (var itemSerial in Serials)
                    {
                        var inteDistNumber = (string)itemSerial["DistNumber"];
                        var intesysNumber = (int)itemSerial["SysNumber"];

                        var item = serialSAP.SerialsSAP.Where(c => c.SysNumber == intesysNumber && c.DistNumber == inteDistNumber).FirstOrDefault();

                        item.UDFs = new List<UDF_ARGNS>();

                        foreach (var itemUdf in pListUDF)
                        {
                            var value = (string)itemSerial[itemUdf.UDFName];

                            var memory = listMemory.Where(c => c.Objet1 == itemUdf.RTable && c.Value1 == value).FirstOrDefault();

                            if (value != string.Empty && itemUdf.RTable != null)
                            {
                                if (memory == null)
                                {
                                    var masterData = SLManager.getMasterData(pCompanyParam, itemUdf.RTable, $" \"Code\" = '{value}' ", true);

                                    var newMemory = new ObjectMemory { Objet1 = itemUdf.RTable, Value1 = value };

                                    value = masterData;

                                    newMemory.Value2 = masterData;

                                    listMemory.Add(newMemory);
                                }
                                else
                                {
                                    value = memory.Value2;
                                }
                            }
                            else
                            {
                                if (value == string.Empty)
                                    value = "??";
                            }

                            item.UDFs.Add(new UDF_ARGNS
                            {
                                Value = value,
                                UDFName = itemUdf.UDFName,
                                RTable = itemUdf.RTable,
                                Document = itemUdf.Document,
                                FieldID = itemUdf.FieldID,
                                TypeID = itemUdf.TypeID,
                                UDFDesc = itemUdf.UDFDesc
                            });
                        }
                    }

                }

                distNumber = "";
                sysNumber = "";

                if (serialSAP.SerialsInDocument.Count() > 0)
                {

                    foreach (var item in serialSAP.SerialsInDocument)
                    {
                        if (distNumber == "")
                        {
                            distNumber = "'" + item.DistNumber + "'";
                        }
                        else { distNumber = distNumber + ",'" + item.DistNumber + "'"; }

                        if (sysNumber == "")
                        {
                            sysNumber = item.SysNumber.ToString();
                        }
                        else { sysNumber = sysNumber + "," + item.SysNumber.ToString() + ""; }
                    }

                    query = string.Format(QueryResource.QueryGet("GetSerialsByDocument09"), distNumber, itemCode, sysNumber, pCompanyParam.CompanyDB);

                    dynamic Serials = JsonConvert.DeserializeObject(SLManager.GetQueryResult(pCompanyParam, query));

                    foreach (var itemSerial in Serials)
                    {
                        var inteDistNumber = (string)itemSerial["DistNumber"];
                        var intesysNumber = (int)itemSerial["SysNumber"];

                        var item = serialSAP.SerialsInDocument.Where(c => c.SysNumber == intesysNumber && c.DistNumber == inteDistNumber).FirstOrDefault();

                        item.UDFs = new List<UDF_ARGNS>();

                        foreach (var itemUdf in pListUDF)
                        {
                            var value = (string)itemSerial[itemUdf.UDFName];

                            var memory = listMemory.Where(c => c.Objet1 == itemUdf.RTable && c.Value1 == value).FirstOrDefault();

                            if (value != string.Empty && itemUdf.RTable != null)
                            {
                                if (memory == null)
                                {
                                    var masterData = SLManager.getMasterData(pCompanyParam, itemUdf.RTable, $" \"Code\" = '{value}' ", true);

                                    var newMemory = new ObjectMemory { Objet1 = itemUdf.RTable, Value1 = value };

                                    //value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");

                                    newMemory.Value2 = masterData;

                                    listMemory.Add(newMemory);
                                }
                                else
                                {
                                    value = memory.Value2;
                                }
                            }
                            else
                            {
                                if (value == string.Empty)
                                    value = "??";
                            }

                            item.UDFs.Add(new UDF_ARGNS
                            {
                                Value = value,
                                UDFName = itemUdf.UDFName,
                                RTable = itemUdf.RTable,
                                Document = itemUdf.Document,
                                FieldID = itemUdf.FieldID,
                                TypeID = itemUdf.TypeID,
                                UDFDesc = itemUdf.UDFDesc
                            });
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSerialsByDocument:" + ex.Message);
            }

            return serialSAP;
        }

        public List<PaymentMean> GetPaymentMeans(CompanyConn pCompanyParam, string branchCode)
        {
            List<PaymentMean> paymentMeans = new List<PaymentMean>();
            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMeans", null);
                paymentMeans = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMean>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                foreach (var item in paymentMeans)
                {
                    item.U_AmountRule = item.U_AmountRule ?? 0;
                    item.U_Surcharge = item.U_Surcharge ?? 0;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetPaymentMeans:" + ex.Message);
            }

            return paymentMeans;
        }

        public List<PaymentMeanType> GetPaymentMeanType(CompanyConn pCompanyParam)
        {
            List<PaymentMeanType> paymentMeanType = new List<PaymentMeanType>();
            string mUrl = string.Empty;
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMeanTypes", null);
                paymentMeanType = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMeanType>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPaymentMeanType:" + ex.Message);
            }

            return paymentMeanType;
        }
        #endregion

        #region Approval

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDate"></param>
        /// <param name="pDocStatus"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetPurchaseApprovalListSearch(CompanyConn pCompanyParam, int pUserSign,
        DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                string mUrl = string.Empty;
                string mFiltersIni = "1 eq 1 ";
                string mFilters = string.Empty;


                if (!string.IsNullOrEmpty(pDocStatus))
                {
                    mFiltersIni = mFiltersIni + " and Status eq '" + pDocStatus + "'";
                }
                else
                {
                    mFiltersIni = mFiltersIni + " and Status ne 'Null'";
                }

                if (pDocDate != null)
                {
                    DateTime mDate = pDocDate ?? DateTime.Now;

                    mFiltersIni = mFiltersIni + " and  CreateDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                //else
                //{
                //    mFiltersIni = mFiltersIni + " and CreateDateNull ne 'Null'";
                //}

                if (pObjectId == "")
                {
                    mFilters = mFiltersIni + " and UserID eq " + pUserSign +
                        " and ObjType_1 eq '112' and  (ObjType eq '22' or ObjType eq '540000006' or ObjType eq '1470000113' or ObjType eq '18')";
                }
                else
                {
                    mFilters = mFiltersIni + " and UserID eq " + pUserSign + " and ObjType_1 eq '112' and  (ObjType eq '" + pObjectId + "')";
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentConfirmationLines", mFilters);

                List<DocumentConfirmationLines> approvalList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>
                    (RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                if (pObjectId == "")
                {
                    mFilters = mFiltersIni + " and UserID eq " + pUserSign + " and ObjType_1 eq ObjType and  (ObjType eq '22' or ObjType eq '540000006' or ObjType eq '1470000113' or ObjType eq '18')";
                }
                else
                {
                    mFilters = mFiltersIni + " and UserID eq " + pUserSign + " and ObjType_1 eq ObjType  and  (ObjType eq '" + pObjectId + "')";
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentConfirmationLinesDocNum", mFilters);

                List<DocumentConfirmationLines> approvalListAddRange = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                approvalList.AddRange((approvalListAddRange).ToList().GroupBy(x => x.WddCode).Select(g => g.First()));

                return approvalList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseApprovalListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDate"></param>
        /// <param name="pDocStatus"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetPurchaseApprovalByOriginator(CompanyConn pCompanyParam, int pUserSign,
            DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                string mUrl = string.Empty;
                string mFiltersIni = "1 eq 1 ";
                string mFilters = string.Empty;

                if (!string.IsNullOrEmpty(pDocStatus))
                {
                    mFiltersIni = mFiltersIni + " and Status eq '" + pDocStatus + "'";
                }
                else
                {
                    mFiltersIni = mFiltersIni + " and Status ne 'Null'";
                }

                if (pDocDate != null)
                {
                    DateTime mDate = pDocDate ?? DateTime.Now;

                    mFiltersIni = mFiltersIni + " and  CreateDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                //else
                //{
                //    mFiltersIni = mFiltersIni + " and CreateDateNull ne 'Null'";
                //}

                if (pObjectId == "")
                {
                    mFilters = mFiltersIni + @" and UserSign eq " + pUserSign + " and DraftType eq '112' and (ObjType eq '22' or ObjType eq '540000006' or ObjType eq '1470000113' or ObjType eq '18')";
                }
                else
                {
                    mFilters = mFiltersIni + @" and UserSign eq " + pUserSign + " and DraftType eq '112' and (ObjType eq '" + pObjectId + "')";
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentConfirmationLinesByOriginator", mFilters);

                List<DocumentConfirmationLines> approvalList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return approvalList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetPurchaseApprovalByOriginator :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDate"></param>
        /// <param name="pDocStatus"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetSalesApprovalListSearch(CompanyConn pCompanyParam,
            int pUserSign,
            DateTime? pDocDate = null,
            string pDocStatus = "",
            string pObjectId = "")
        {
            try
            {
                string mUrl = string.Empty;
                string mFiltersIni = "1 eq 1 ";
                string mFilters = string.Empty;


                if (!string.IsNullOrEmpty(pDocStatus))
                {
                    mFiltersIni = mFiltersIni + " and Status eq '" + pDocStatus + "'";
                }
                else
                {
                    mFiltersIni = mFiltersIni + " and Status ne 'Null'";
                }

                if (pDocDate != null)
                {
                    DateTime mDate = pDocDate ?? DateTime.Now;

                    mFiltersIni = mFiltersIni + " and  CreateDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (pObjectId == "")
                {
                    mFilters = mFiltersIni + " and UserID eq " + pUserSign + " and DraftType eq '112' and  (ObjType eq '23' or ObjType eq '17' or ObjType eq '13')";
                }
                else
                {
                    mFilters = mFiltersIni + " and UserID eq " + pUserSign + " and DraftType eq '112' and  (ObjType eq '" + pObjectId + "')";
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentConfirmationLines", mFilters);

                List<DocumentConfirmationLines> approvalsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>(
                    RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


                //mFilters = mFiltersIni + " and UserID eq " + pUserSign + " and ObjType_1 eq ObjType and  (ObjType eq '23' or ObjType eq '17')";

                //mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentConfirmationLinesDocNum", mFilters);

                //List<DocumentConfirmationLines> approvalListAddRange = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>(
                //		RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //approvalsList.AddRange((approvalListAddRange).ToList().GroupBy(x => x.WddCode).Select(g => g.First()));

                return approvalsList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetSalesApprovalListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDate"></param>
        /// <param name="pDocStatus"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetSalesApprovalByOriginator(CompanyConn pCompanyParam,
            int pUserSign, DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                string mUrl = string.Empty;
                string mFiltersIni = "1 eq 1 ";
                string mFilters = string.Empty;

                if (!string.IsNullOrEmpty(pDocStatus))
                {
                    mFiltersIni = mFiltersIni + " and Status eq '" + pDocStatus + "'";
                }
                else
                {
                    mFiltersIni = mFiltersIni + " and Status ne 'Null'";
                }

                if (pDocDate != null)
                {
                    DateTime mDate = pDocDate ?? DateTime.Now;

                    mFiltersIni = mFiltersIni + " and  CreateDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                //else
                //{
                //    mFiltersIni = mFiltersIni + " and CreateDate ne 'Null'";
                //}
                if (pObjectId == "")
                {
                    mFilters = mFiltersIni + " and UserSign eq " + pUserSign + " and DraftType eq '112' and  (ObjType eq '23' or ObjType eq '17' or ObjType eq '13')";
                }
                else
                {
                    mFilters = mFiltersIni + string.Format(" and UserSign eq " + pUserSign + " and DraftType eq '112' and  (ObjType eq '{0}')", pObjectId);
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentConfirmationLinesByOriginator", mFilters);

                List<DocumentConfirmationLines> approvalList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>(
                        RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET,
                        pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return approvalList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetSalesApprovalByOriginator :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pWddCode"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetApprovalListByID(CompanyConn pCompanyParam, int pWddCode)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "WddCode eq " + pWddCode;

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "WDD1", mFilters);

                List<DocumentConfirmationLines> listReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetApprovalListByID:" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pDocEntry"></param>
        /// <param name="pObjType"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetApprovalListByDocumentId(CompanyConn pCompanyParam, int pDocEntry, string pObjType)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "DocEntry eq and ObjType eq '" + pObjType + "'";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OWDD", mFilters);

                List<OWDD> listOWDD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWDD>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                List<DocumentConfirmationLines> listReturn = new List<DocumentConfirmationLines>();
                foreach (OWDD aux in listOWDD)
                {
                    mFilters = "WddCode eq " + aux.WddCode;

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "WDD1", mFilters);

                    listReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentConfirmationLines>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetApprovalListByDocumentId:" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<StageSAP> GetStageList(CompanyConn pCompanyParam)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "1 eq 1";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Stage", mFilters);

                List<StageSAP> listReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StageSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetStageList:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owwdCode"></param>
        /// <param name="remark"></param>
        /// <param name="UserNameSAP"></param>
        /// <param name="UserPasswordSAP"></param>
        /// <param name="approvalCode"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string SaveApprovalResponse(int owwdCode, string remark, string UserNameSAP, string UserPasswordSAP, string approvalCode, CompanyConn pCompanyParam)
        {
            ApprovalRequestsServiceWeb.ApprovalRequest oApprovalRequest = new ApprovalRequestsServiceWeb.ApprovalRequest();
            ApprovalRequestsServiceWeb.ApprovalRequestParams oApprovalRequestParams = new ApprovalRequestsServiceWeb.ApprovalRequestParams();
            ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecision oApprovalRequestDecision = new ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecision();
            ApprovalRequestsServiceWeb.MsgHeader oApprovalRequestMsgHeader = new ApprovalRequestsServiceWeb.MsgHeader();

            oApprovalRequestMsgHeader.SessionID = pCompanyParam.DSSessionId;
            oApprovalRequestMsgHeader.ServiceName = ApprovalRequestsServiceWeb.MsgHeaderServiceName.ApprovalRequestsService;
            oApprovalRequestMsgHeader.ServiceNameSpecified = true;

            ApprovalRequestsServiceWeb.ApprovalRequestsService oApprovalRequestsService = new ApprovalRequestsServiceWeb.ApprovalRequestsService();

            oApprovalRequestsService.MsgHeaderValue = oApprovalRequestMsgHeader;

            //.- LINK HERE -.\\
            oApprovalRequestParams.Code = owwdCode;
            oApprovalRequestParams.CodeSpecified = true;

            //.- Get the approval request -.\\
            oApprovalRequest = oApprovalRequestsService.GetApprovalRequest(oApprovalRequestParams);

            //.- Set approval status -.\\
            switch (approvalCode)
            {
                case "W":
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardPending;
                    break;
                case "Y":
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardApproved;
                    break;
                case "N":
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardNotApproved;
                    break;
                default:
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardNotApproved;
                    break;

            }
            oApprovalRequestDecision.StatusSpecified = true;
            oApprovalRequestDecision.Remarks = remark;
            oApprovalRequestDecision.ApproverUserName = UserNameSAP;
            oApprovalRequestDecision.ApproverPassword = UserPasswordSAP;

            oApprovalRequest.ApprovalRequestDecisions = new ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecision[1];

            //.- Set the approval request statuts -.\\
            oApprovalRequest.ApprovalRequestDecisions.SetValue(oApprovalRequestDecision, 0);
            try
            {
                oApprovalRequestsService.UpdateRequest(oApprovalRequest);
                oApprovalRequest = oApprovalRequestsService.GetApprovalRequest(oApprovalRequestParams);

                if (oApprovalRequest.Status == ApprovalRequestsServiceWeb.ApprovalRequestStatus.arsApproved)
                {
                    DraftsServiceWeb.Document oDraftDocument = new DraftsServiceWeb.Document();
                    DraftsServiceWeb.DocumentParams oDraftDocumentParams = new DraftsServiceWeb.DocumentParams();
                    DraftsServiceWeb.MsgHeader oDraftDocumentMsgHeader = new DraftsServiceWeb.MsgHeader();



                    oDraftDocumentMsgHeader.ServiceName = DraftsServiceWeb.MsgHeaderServiceName.DraftsService;
                    oDraftDocumentMsgHeader.ServiceNameSpecified = true;
                    oDraftDocumentMsgHeader.SessionID = pCompanyParam.DSSessionId;

                    DraftsServiceWeb.DraftsService oDrafService = new DraftsServiceWeb.DraftsService();
                    oDrafService.MsgHeaderValue = oDraftDocumentMsgHeader;

                    //.- LINK HERE -.\\
                    oDraftDocumentParams.DocEntry = oApprovalRequest.ObjectEntry;
                    oDraftDocumentParams.DocEntrySpecified = true;

                    oDraftDocument = oDrafService.GetByParams(oDraftDocumentParams);
                    oDrafService.SaveDraftToDocument(oDraftDocument);
                }
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> SaveApprovalResponse:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region Draft

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseUserDraftListSearch(CompanyConn pCompanyParam,
            int pUserSign, DateTime? pDocDateFrom = null,
            DateTime? pDocDateTo = null, DateTime? pReqDate = null,
            string pDocType = "", string pDocStatus = "",
            string pCodeVendor = "", int? pDocNum = null,
            int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                string mUrl = string.Empty;
                List<DraftPortal> ListReturn = new List<DraftPortal>();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                string mFilter = "UserSign eq " + pUserSign;

                if (pDocDateFrom != null)
                {
                    DateTime mDate = pDocDateFrom ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate ge datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                if (pDocDateTo != null)
                {
                    DateTime mDate = pDocDateTo ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate le datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (pReqDate != null)
                {
                    DateTime mDate = pReqDate ?? DateTime.Now;
                    mFilter = mFilter + " and  ReqDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilter = mFilter + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }

                if (pDocNum != null && pDocNum != 0)
                {
                    mFilter = mFilter + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDocType == "" || pDocType == "22")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrder", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "540000006")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseQuotation", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "1470000113")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseRequest", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "18")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseInvoice", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                mJsonObjectResult.DraftPortalList = ListReturn.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.Others.Add("TotalRecords", ListReturn.Count().ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseUserDraftListSearch :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSalesEmployee"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                string mUrl = string.Empty;
                List<DraftPortal> ListReturn = new List<DraftPortal>();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                string mFilter = "SlpCode eq " + pSalesEmployee;

                if (pDocDateFrom != null)
                {
                    DateTime mDate = pDocDateFrom ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate ge datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                if (pDocDateTo != null)
                {
                    DateTime mDate = pDocDateTo ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate le datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (pReqDate != null)
                {
                    DateTime mDate = pReqDate ?? DateTime.Now;
                    mFilter = mFilter + " and  ReqDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilter = mFilter + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }

                if (pDocNum != null && pDocNum != 0)
                {
                    mFilter = mFilter + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDocType == "" || pDocType == "22")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrder", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "540000006")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseQuotation", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "1470000113")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseRequest", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "18")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseInvoice", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                mJsonObjectResult.DraftPortalList = ListReturn.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.Others.Add("TotalRecords", ListReturn.Count().ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseSalesEmployeeDraftListSearch :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBP"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                string mUrl = string.Empty;
                List<DraftPortal> ListReturn = new List<DraftPortal>();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                string mFilter = "CardCode eq '" + pBP + "'";
                if (pDocDateFrom != null)
                {
                    DateTime mDate = pDocDateFrom ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate ge datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                if (pDocDateTo != null)
                {
                    DateTime mDate = pDocDateTo ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate le datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (pReqDate != null)
                {
                    DateTime mDate = pReqDate ?? DateTime.Now;
                    mFilter = mFilter + " and  ReqDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilter = mFilter + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }

                if (pDocNum != null && pDocNum != 0)
                {
                    mFilter = mFilter + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDocType == "" || pDocType == "22")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrder", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "540000006")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseQuotation", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "1470000113")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseRequest", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "18")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseInvoice", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }


                mJsonObjectResult.DraftPortalList = ListReturn.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.Others.Add("TotalRecords", ListReturn.Count().ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseCustomerDraftListSearch :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesUserDraftListSearch(CompanyConn pCompanyParam, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                string mUrl = string.Empty;
                List<DraftPortal> ListReturn = new List<DraftPortal>();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                string mFilter = "UserSign eq " + pUserSign;
                bool oOpenNotDelivered;

                if (pDocDateFrom != null)
                {
                    DateTime mDate = pDocDateFrom ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate ge datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                if (pDocDateTo != null)
                {
                    DateTime mDate = pDocDateTo ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate le datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilter = mFilter + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }

                if (pDocNum != null && pDocNum != 0)
                {
                    mFilter = mFilter + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDocType == "" || pDocType == "17")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrder", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "23")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleQuotation", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "13")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesInvoice", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                mJsonObjectResult.DraftPortalList = ListReturn.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.Others.Add("TotalRecords", ListReturn.Count().ToString());


                foreach (DraftPortal order in mJsonObjectResult.DraftPortalList)
                {
                    if (order.ObjType == "17")
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            string mResultJsonLines = RESTService.GetRequestJson(
                             RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrderLine", "DocEntry eq " + order.DocEntry),
                             WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                            Newtonsoft.Json.Linq.JArray mJArrayRDR1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                            List<RDR1> mLisRDR1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR1>>(mResultJsonLines);

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && DateTime.Compare(line.ShipDate.Value.Date, DateTime.Now.Date) < 0)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }


                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesUserDraftListSearch :" + ex.Message);
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSalesEmployee"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                string mUrl = string.Empty;
                List<DraftPortal> ListReturn = new List<DraftPortal>();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                string mFilter = "SlpCode eq " + pSalesEmployee;
                bool oOpenNotDelivered;

                if (pDocDateFrom != null)
                {
                    DateTime mDate = pDocDateFrom ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate ge datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                if (pDocDateTo != null)
                {
                    DateTime mDate = pDocDateTo ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate le datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilter = mFilter + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }

                if (pDocNum != null && pDocNum != 0)
                {
                    mFilter = mFilter + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDocType == "" || pDocType == "17")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrder", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }
                if (pDocType == "" || pDocType == "23")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleQuotation", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }
                if (pDocType == "" || pDocType == "13")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesInvoice", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                mJsonObjectResult.DraftPortalList = ListReturn.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.Others.Add("TotalRecords", ListReturn.Count().ToString());


                foreach (DraftPortal order in mJsonObjectResult.DraftPortalList)
                {
                    if (order.ObjType == "17")
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            string mResultJsonLines = RESTService.GetRequestJson(
                               RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrderLine", "DocEntry eq " + order.DocEntry),
                               WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                            Newtonsoft.Json.Linq.JArray mJArrayRDR1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                            List<RDR1> mLisRDR1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR1>>(mResultJsonLines);

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && DateTime.Compare(line.ShipDate.Value.Date, DateTime.Now.Date) < 0)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }



                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesSalesEmployeeDraftListSearch :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBP"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                string mUrl = string.Empty;
                List<DraftPortal> ListReturn = new List<DraftPortal>();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                string mFilter = "CardCode eq '" + pBP + "'";
                bool oOpenNotDelivered;

                if (pDocDateFrom != null)
                {
                    DateTime mDate = pDocDateFrom ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate ge datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }
                if (pDocDateTo != null)
                {
                    DateTime mDate = pDocDateTo ?? DateTime.Now;
                    mFilter = mFilter + " and  DocDate le datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilter = mFilter + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }

                if (pDocNum != null && pDocNum != 0)
                {
                    mFilter = mFilter + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDocType == "" || pDocType == "17")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrder", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "23")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleQuotation", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                if (pDocType == "" || pDocType == "13")
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesInvoice", mFilter);
                    ListReturn.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftPortal>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }

                mJsonObjectResult.DraftPortalList = ListReturn.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.Others.Add("TotalRecords", ListReturn.Count().ToString());

                foreach (DraftPortal order in mJsonObjectResult.DraftPortalList)
                {
                    if (order.ObjType == "17")
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            string mResultJsonLines = RESTService.GetRequestJson(
                               RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrderLine", "DocEntry eq " + order.DocEntry),
                               WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                            Newtonsoft.Json.Linq.JArray mJArrayRDR1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                            List<RDR1> mLisRDR1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR1>>(mResultJsonLines);

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && DateTime.Compare(line.ShipDate.Value.Date, DateTime.Now.Date) < 0)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesCustomerDraftListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public Draft GetDraftById(CompanyConn pCompanyParam, int pCode)
        {
            Draft ret = null;
            try
            {
                string mUrl = string.Empty;
                string mFilters = "DocEntry eq " + pCode;

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ODRF", mFilters);

                //ret = Mapper.Map<Draft>(mDbContext.ODRF.Where(c => c.DocEntry == pCode).SingleOrDefault());

                ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Draft>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                if (ret != null)
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DRF1", mFilters);

                    ret.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DraftLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    //ret.Lines = Mapper.Map<List<DraftLine>>(mDbContext.DRF1.Where(c => c.DocEntry == pCode).ToList());
                }
                else
                { ret = new Draft(); }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentSettings");

                ret.SAPDocSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentSettingsSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                //ret.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mDbContext.CINF.FirstOrDefault());
                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetDraftById :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="docEntry"></param>
        /// <param name="pSession"></param>
        /// <returns></returns>
        public string CreateDocument(CompanyConn pCompanyParam, int docEntry, string pSession)
        {
            DraftsServiceWeb.Document oDraftDocument = new DraftsServiceWeb.Document();
            DraftsServiceWeb.DocumentParams oDraftDocumentParams = new DraftsServiceWeb.DocumentParams();
            DraftsServiceWeb.MsgHeader oDraftDocumentMsgHeader = new DraftsServiceWeb.MsgHeader();

            oDraftDocumentMsgHeader.ServiceName = DraftsServiceWeb.MsgHeaderServiceName.DraftsService;
            oDraftDocumentMsgHeader.ServiceNameSpecified = true;
            oDraftDocumentMsgHeader.SessionID = pSession;

            DraftsServiceWeb.DraftsService oDrafService = new DraftsServiceWeb.DraftsService();
            oDrafService.MsgHeaderValue = oDraftDocumentMsgHeader;

            //.- LINK HERE -.\\
            oDraftDocumentParams.DocEntry = docEntry;
            oDraftDocumentParams.DocEntrySpecified = true;

            try
            {
                oDraftDocument = oDrafService.GetByParams(oDraftDocumentParams);
                oDrafService.SaveDraftToDocument(oDraftDocument);

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        #endregion

        #region Sales Invoice

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "1 eq 1 ";
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }


                if (pDocNum != null && pDocNum != 0)
                {
                    mFilters = mFilters + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDate != null)
                {
                    DateTime mDate = pDate ?? DateTime.Now;

                    mFilters = mFilters + " and  DocDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pDocStatus))
                {
                    mFilters = mFilters + " and  DocStatus eq '" + pDocStatus + "'";
                }

                if (!string.IsNullOrEmpty(pSECode))
                {
                    mFilters = mFilters + " and  SlpCode eq " + pSECode;
                }

                if ((pCompanyParam.IdBranchSelect ?? 0) > 0)
                {
                    mFilters = mFilters + " and  BPLId eq " + pCompanyParam.IdBranchSelect;
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesInvoice", mFilters);

                List<SalesInvoiceSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesInvoiceSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mJsonObjectResult.SalesInvoiceSAPList = ListRestu.OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.Others.Add("TotalRecords", ListRestu.Count.ToString());
                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesInvoiceListSearch :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <param name="pListUDFOINV"></param>
        /// <param name="pListUDFINV1"></param>
        /// <returns></returns>
        public SalesInvoiceSAP GetAllbySI(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOINV = null, List<UDF_ARGNS> pListUDFINV1 = null)
        {
            SalesInvoiceSAP ret = null;

            string mUrl = string.Empty;
            string mFilters = "DocEntry eq " + pCode;

            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesInvoice", mFilters);

                string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                ret = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesInvoiceSAP>>(mResultJson).FirstOrDefault();

                if (ret != null)
                {
                    ret = FillSIDocumentSAP(pCompanyParam, ret);
                    mFilters = "TableID eq 'OINV'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    Newtonsoft.Json.Linq.JArray mJArrayOINV = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    ret.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOINV, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOINV[0]);
                    mFilters = "DocEntry eq " + pCode;
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesInvoiceLine", mFilters);
                    string mResultJsonLines = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    Newtonsoft.Json.Linq.JArray mJArrayINV1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                    ret.Lines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesInvoiceLineSAP>>(mResultJsonLines);

                    foreach (SalesInvoiceLineSAP item in ret.Lines)
                    {
                        mFilters = "TableID eq 'INV1'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFINV1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayINV1.Where(c => (int)c["LineNum"] == item.LineNum).FirstOrDefault());
                        mFilters = "DocEntry eq " + pCode;
                    }

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "DocumentAddressSI", mFilters);
                    ret.SIAddress = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DocumentAddress>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                }
                else
                {
                    ret = new SalesInvoiceSAP();
                    ret = FillSIDocumentSAP(pCompanyParam, ret);
                }

                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetAllbySI :" + ex.Message);
                return null;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSaleInvSAP"></param>
        /// <returns></returns>
        public SalesInvoiceSAP FillSIDocumentSAP(CompanyConn pCompanyParam, SalesInvoiceSAP pSaleInvSAP)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            //Buyer
            List<OSLP> mLisOSLP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSLP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Currency
            List<OCRN> mListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentMethod
            List<OCTG> mListOCTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMethod", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentTerm
            // List<OPYM> mListOPYM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPYM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentTerm", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //ShippingType
            List<OSHP> mListOSHP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSHP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Taxt
            List<OVTG> mListOVTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OVTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Taxt", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pSaleInvSAP.CardCode))
            {
                retCb.ListPaymentMethod.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMethod>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CRD2", "CardCode eq '" + pSaleInvSAP.CardCode + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
            }

            // retCb.ListPaymentMethod = mListOPYM.Select(c => new PaymentMethod { PayMethCod = c.PayMethCod, Descript = c.Descript }).ToList(); 

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pSaleInvSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OACT>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", "FormatCode ne 'Null'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleInvSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //Warehouse
            List<OWHS> mListOWHS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWHS>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleInvSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pSaleInvSAP.LocalCurrency = GetCurrency(true, pCompanyParam);
            pSaleInvSAP.SystemCurrency = GetCurrency(false, pCompanyParam);

            //DistrbRule
            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleInvSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pSaleInvSAP;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        #region Sales Order

        public JsonObjectResult GetSalesOrderListSearch(CompanyConn pCompanyParam,
            bool pShowOpenQuantityStatus,
            string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null,
            string pDocStatus = "", string pOwnerCode = "", string pSECode = "",
            int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {

                string mUrl = string.Empty;
                string mFilters = "1 eq 1 ";
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                bool oOpenNotDelivered;

                if (!string.IsNullOrEmpty(pCodeVendor))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
                }


                if (pDocNum != null && pDocNum != 0)
                {
                    mFilters = mFilters + " and  DocNum  eq  " + pDocNum.ToString();
                }

                if (pDate != null)
                {
                    DateTime mDate = pDate ?? DateTime.Now;

                    mFilters = mFilters + " and  DocDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pDocStatus))
                {
                    mFilters = mFilters + " and  DocStatus eq '" + pDocStatus + "'";
                }

                if (!string.IsNullOrEmpty(pSECode))
                {
                    mFilters = mFilters + " and  SlpCode eq " + pSECode;
                }

                if ((pCompanyParam.IdBranchSelect ?? 0) > 0)
                {
                    mFilters = mFilters + " and  BPLId eq " + pCompanyParam.IdBranchSelect;
                }


                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrder", mFilters);

                List<SaleOrderSAP> mListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SaleOrderSAP>>
                    (RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET,
                    pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mJsonObjectResult.SalesOrderSAPList = mListRestu.OrderBy(
                    Utils.OrderString(pOrderColumn))
                    .Skip(pStart).Take(pLength).ToList();

                mJsonObjectResult.Others.Add("TotalRecords", mListRestu.Count.ToString());

                //
                if (pShowOpenQuantityStatus)
                {
                    foreach (SaleOrderSAP order in mJsonObjectResult.SalesOrderSAPList)
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            string mResultJsonLines = RESTService.GetRequestJson(
                            RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrderLine", "DocEntry eq " + order.DocEntry),
                            WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);

                            Newtonsoft.Json.Linq.JArray mJArrayRDR1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                            List<RDR1> mLisRDR1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR1>>(mResultJsonLines);

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && line.ShipDate.Value.Date < DateTime.Now.Date)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetSalesOrderListSearch :" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult AddSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam)
        {
            SalesOrderServiceWeb.MsgHeader header = new SalesOrderServiceWeb.MsgHeader();
            header.ServiceName = SalesOrderServiceWeb.MsgHeaderServiceName.OrdersService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesOrderServiceWeb.OrdersService oOrderService = new SalesOrderServiceWeb.OrdersService();
            oOrderService.MsgHeaderValue = header;

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            SaleOrderSAP mSOIni = new SaleOrderSAP();
            Mapper.CreateMap(typeof(SaleOrderSAP), typeof(SaleOrderSAP));
            Mapper.Map(pObject, mSOIni);

            SalesOrderServiceWeb.Document prDocument = new SalesOrderServiceWeb.Document();
            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;
            prDocument.ShipToCode = pObject.ShipToCode;
            prDocument.PayToCode = pObject.PayToCode;
            prDocument.DocObjectCode = "";
            prDocument.RelatedType = -1;

            if (pCompanyParam.IdBranchSelect != null && pCompanyParam.IdBranchSelect > 0)
            {
                prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
                prDocument.BPL_IDAssignedToInvoiceSpecified = true;
            }

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            //prDocument.DocTotal = (double)pObject.DocTotal;

            SalesOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesOrderServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.SOAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.SOAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.SOAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.SOAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.SOAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.SOAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.SOAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.SOAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.SOAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.SOAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.SOAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.SOAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.SOAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.SOAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.SOAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.SOAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.SOAddress.CountyB;
            oDocumentAddress.BillToState = pObject.SOAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.SOAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.SOAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                SalesOrderServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense();

                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocEntry = expenseLine.BaseAbsEnt ?? 0;
                    oDocumentExpenses.BaseDocEntrySpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocLine = expenseLine.BaseLnNum ?? 0;
                    oDocumentExpenses.BaseDocLineSpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocType = expenseLine.BaseType ?? 0;
                    oDocumentExpenses.BaseDocTypeSpecified = true;
                }


                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }


                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                if (expenseLine.ExpnsCode != null)
                {
                    oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode ?? 0;
                    oDocumentExpenses.ExpenseCodeSpecified = true;
                }
                oDocumentExpenses.LineNum = expenseLine.LineNum;
                oDocumentExpenses.LineNumSpecified = true;
                oDocumentExpenses.Project = expenseLine.Project;
                oDocumentExpenses.Remarks = expenseLine.Comments;

                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            SalesOrderServiceWeb.DocumentDocumentLine[] newLines = new SalesOrderServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;

            string mUrl = string.Empty;

            foreach (SaleOrderSAPLine portalLine in pObject.Lines)
            {
                SalesOrderServiceWeb.DocumentDocumentLine line = new SalesOrderServiceWeb.DocumentDocumentLine();

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Items", "ItemCode eq '" + portalLine.ItemCode + "'");

                ItemMasterSAP mOitm = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                if (mOitm == null)
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = "Invalid ItemCode";

                    return mJsonObjectResult;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUOM", "UomCode eq '" + portalLine.UomCode + "'");

                    OUOM mOUOM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUOM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                #region Line Freight

                line.DocumentLineAdditionalExpenses = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense[1];
                line.DocumentLineAdditionalExpenses[0] = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense();
                if (portalLine.Freight.Count > 0)
                {
                    if (portalLine.Freight[0].ExpnsCode.HasValue)
                    {
                        line.DocumentLineAdditionalExpenses[0].ExpenseCode = portalLine.Freight[0].ExpnsCode.Value;
                        line.DocumentLineAdditionalExpenses[0].ExpenseCodeSpecified = true;
                    }
                    if (portalLine.Freight[0].LineTotal.HasValue)
                    {
                        line.DocumentLineAdditionalExpenses[0].LineTotal = (double)portalLine.Freight[0].LineTotal.Value;
                        line.DocumentLineAdditionalExpenses[0].LineTotalSpecified = true;
                    }
                }

                #endregion Line Freight

                #region Line Batchs And Serials

                if (mOitm.ManBtchNum == "Y" && !string.IsNullOrEmpty(portalLine.SerialBatch))
                {
                    SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[] dBatchNumber = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[1];

                    dBatchNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber();

                    dBatchNumber[0].BaseLineNumber = portalLine.LineNum;

                    dBatchNumber[0].BaseLineNumberSpecified = true;

                    dBatchNumber[0].BatchNumber = portalLine.SerialBatch;

                    dBatchNumber[0].AddmisionDate = DateTime.Now;

                    dBatchNumber[0].AddmisionDateSpecified = true;

                    dBatchNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                    dBatchNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                    dBatchNumber[0].Notes = "";

                    dBatchNumber[0].Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);

                    dBatchNumber[0].QuantitySpecified = true;

                    line.BatchNumbers = dBatchNumber;

                }

                if (mOitm.ManSerNum == "Y")
                {

                    if (portalLine.Serials != null && portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count() > 0)
                    {
                        SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count()];
                        int i = 0;
                        foreach (Serial itemSerial in portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum))
                        {
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSRN", $"DistNumber eq '{itemSerial.DistNumber.Trim()}'");
                            var OSRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSRN>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();


                            dSerialNumber[i] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();
                            dSerialNumber[i].BaseLineNumber = portalLine.LineNum;
                            dSerialNumber[i].BaseLineNumberSpecified = true;
                            dSerialNumber[i].Quantity = Convert.ToDouble(1);
                            dSerialNumber[i].QuantitySpecified = true;
                            dSerialNumber[i].SystemSerialNumber = OSRN.FirstOrDefault().SysNumber;
                            dSerialNumber[i].SystemSerialNumberSpecified = true;

                            i++;
                        }

                        line.SerialNumbers = dSerialNumber;

                    }

                }

                //if (mOitm.ManSerNum == "Y" && !string.IsNullOrEmpty(portalLine.SerialBatch))
                //{
                //    SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[1];

                //    dSerialNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();

                //    dSerialNumber[0].BaseLineNumber = portalLine.LineNum;

                //    dSerialNumber[0].BaseLineNumberSpecified = true;

                //    dSerialNumber[0].BatchID = portalLine.SerialBatch;

                //    dSerialNumber[0].Quantity = Convert.ToDouble(1);

                //    dSerialNumber[0].QuantitySpecified = true;

                //    dSerialNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                //    dSerialNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                //    dSerialNumber[0].Notes = "";

                //    line.SerialNumbers = dSerialNumber;

                //}

                #endregion Line Batchs And Serials

                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.CostingCode = portalLine.OcrCode;
                line.CostingCode2 = portalLine.OcrCode2;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.BaseEntry != null)
                {
                    line.BaseEntry = (long)portalLine.BaseEntry;
                    line.BaseEntrySpecified = true;
                }
                if (portalLine.BaseLine != null)
                {
                    line.BaseLine = (long)portalLine.BaseLine;
                    line.BaseLineSpecified = true;
                }
                if (portalLine.BaseType != null)
                {
                    line.BaseType = (long)portalLine.BaseType;
                    line.BaseTypeSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }
                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new SalesOrderServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            //try
            //{
            //    XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

            //    if (pObject.MappedUdf.Count > 0)
            //    { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

            //    XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
            //    foreach (XmlNode mAuxNode in mAuxNodeList)
            //    {
            //        SaleOrderSAPLine mAuxLine = mSOIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

            //        if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
            //        { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

            //    }

            //    string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
            //    mJsonObjectResult.Code = mResponse;
            //    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

            //    var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

            //    if (udfIdPayment != "")
            //    {
            //        var idPayment = Convert.ToInt32(pObject.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

            //        AddPaymentByOrder(pCompanyParam, pObject.PaymentMeanOrder, 17, Convert.ToInt32(idPayment), Convert.ToInt32(idPayment));
            //    }


            //    //AddPaymentByOrder(pCompanyParam, pObject.PaymentMeanOrder, 17, Convert.ToInt32(mJsonObjectResult.Code), Convert.ToInt32(mJsonObjectResult.Code));

            //    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
            //    {
            //        string AddCmd = string.Empty;
            //        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
            //        XmlDocument mResultXML = new XmlDocument();
            //        AddCmd = @"<?xml version=""1.0"" encoding=""UTF-16""?>" +
            //                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
            //                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
            //                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
            //        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
            //        {
            //            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
            //        }

            //        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
            //        mResultXML.LoadXml(mServerNode.Interact(AddCmd));

            //        SalesOrderServiceWeb.DocumentParams parametros = new SalesOrderServiceWeb.DocumentParams();
            //        parametros.DocEntry = Convert.ToInt32(mJsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
            //        parametros.DocEntrySpecified = true;

            //        prDocument = oOrderService.GetByParams(parametros);
            //        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
            //        {
            //            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
            //            prDocument.AttachmentEntrySpecified = true;
            //        }
            //        oOrderService.Update(prDocument);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
            //    mJsonObjectResult.ErrorMsg = ex.Message;
            //}
            try
            {

                dynamic documentToAdd = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)documentToAdd;

                foreach (var property in prDocument.GetType().GetProperties())
                {
                    if (property.Name != "DocumentLines")
                    {
                        string value = (property.GetValue(prDocument) ?? "").ToString();

                        if (value != "0" && value != "0.0" && property.Name != "Letter")
                        {
                            dictionary.Add(property.Name, property.GetValue(prDocument));
                        }

                    }

                }

                List<dynamic> linesToAdd = new List<dynamic>();

                foreach (var item in prDocument.DocumentLines)
                {
                    SaleOrderSAPLine mAuxLine = mSOIni.Lines.Where(c => c.LineNum == item.LineNum).FirstOrDefault();

                    dynamic documentLine = new ExpandoObject();
                    var dictionaryLine = (IDictionary<string, object>)documentLine;

                    foreach (var property in item.GetType().GetProperties())
                    {
                        if (property.Name != "SerialNumbers")
                        {
                            if (property.Name != "LineNum")
                            {
                                string value = (property.GetValue(item) ?? "").ToString();

                                if (value != "0" && value != "0.0")
                                {
                                    dictionaryLine.Add(property.Name, property.GetValue(item));
                                }
                            }
                            else
                            {
                                dictionaryLine.Add(property.Name, property.GetValue(item));
                            }
                        }
                    }

                    List<dynamic> linesSerialToAdd = new List<dynamic>();

                    if (item.SerialNumbers != null)
                    {
                        foreach (var itemSer in item.SerialNumbers)
                        {
                            dynamic documentSerial = new ExpandoObject();
                            var dictionarySerial = (IDictionary<string, object>)documentSerial;

                            foreach (var property in itemSer.GetType().GetProperties())
                            {
                                //if (property.Name == "ItemCode" || property.Name == "Quantity" || property.Name == "BaseLineNumber" || property.Name == "Location" || property.Name == "Notes" || property.Name == "BatchID" || property.Name == "SystemSerialNumber")
                                if (property.Name == "Quantity" || property.Name == "BaseLineNumber" || property.Name == "Location" || property.Name == "Notes" || property.Name == "BatchID" || property.Name == "SystemSerialNumber")
                                {
                                    string value = (property.GetValue(itemSer) ?? "").ToString();

                                    dictionarySerial.Add(property.Name, property.GetValue(itemSer));

                                }

                            }

                            linesSerialToAdd.Add(documentSerial);

                        }

                        dictionaryLine.Add("SerialNumbers", linesSerialToAdd.ToArray());
                    }

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    {
                        foreach (var itemUdfLine in mAuxLine.MappedUdf)
                        {
                            dictionaryLine.Add(itemUdfLine.UDFName, itemUdfLine.Value);
                        }

                    }

                    linesToAdd.Add(documentLine);

                }

                dictionary.Add("DocumentLines", linesToAdd.ToArray());

                foreach (UDF_ARGNS mUDF in pObject.MappedUdf)
                {
                    dictionary.Add(mUDF.UDFName, mUDF.Value);
                }

                var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

                var result = SLManager.callAPI(pCompanyParam, "Orders", RestSharp.Method.POST, jsonToAdd);

                var resultObject = JsonConvert.DeserializeObject<ORDR>(result.Content);

                if (resultObject.DocEntry != 0)
                {
                    mJsonObjectResult.Code = resultObject.DocEntry.ToString();
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                    var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

                    if (udfIdPayment != "")
                    {
                        var idPayment = Convert.ToInt32(pObject.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

                        AddPaymentByOrder(pCompanyParam, pObject.PaymentMeanOrder, 17, Convert.ToInt32(idPayment), Convert.ToInt32(idPayment));
                    }

                }
                else
                {
                    var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = errorAPI.error.message.value;
                }

            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {

                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                SaleOrderSAP mSOIni = new SaleOrderSAP();
                Mapper.CreateMap(typeof(SaleOrderSAP), typeof(SaleOrderSAP));
                Mapper.Map(pObject, mSOIni);

                var result2 = SLManager.callAPI(pCompanyParam, $"Orders({pObject.DocEntry})", RestSharp.Method.GET, "");
                var docSAP = JsonConvert.DeserializeObject<SalesOrderServiceWeb.Document>(result2.Content, settings);

                SalesOrderServiceWeb.Document prDocument = docSAP;

                switch (pObject.DocType)
                {
                    case "I":
                        prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                        prDocument.DocTypeSpecified = true;
                        break;
                    case "S":
                        prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Service;
                        prDocument.DocTypeSpecified = true;
                        break;
                    default:
                        prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                        prDocument.DocTypeSpecified = true;
                        break;
                }
                if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.DocDate = pObject.DocDate.Value;
                    prDocument.DocDateSpecified = true;
                }
                if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.DocDueDate = pObject.DocDueDate.Value;
                    prDocument.DocDueDateSpecified = true;
                }
                if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.TaxDate = pObject.TaxDate.Value;
                    prDocument.TaxDateSpecified = true;
                }
                if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.CancelDate = pObject.CancelDate.Value;
                    prDocument.CancelDateSpecified = true;
                }
                if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.RequriedDate = pObject.ReqDate.Value;
                    prDocument.RequriedDateSpecified = true;
                }

                prDocument.CardCode = pObject.CardCode;
                prDocument.CardName = pObject.CardName;
                prDocument.Project = pObject.Project;
                prDocument.ShipToCode = pObject.ShipToCode;
                prDocument.PayToCode = pObject.PayToCode;

                switch (pObject.RevisionPo)
                {
                    case "Y":
                        prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tYES;
                        prDocument.RevisionPoSpecified = true;
                        break;
                    case "N":
                        prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                        prDocument.RevisionPoSpecified = true;
                        break;
                    default:
                        prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                        prDocument.RevisionPoSpecified = true;
                        break;
                }
                switch (pObject.SummryType)
                {
                    case "N":
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                    case "I":
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByItems;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                    case "D":
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByDocuments;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                    default:
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                }

                prDocument.NumAtCard = pObject.NumAtCard;
                prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                prDocument.SalesPersonCodeSpecified = true;

                if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
                {
                    prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                    prDocument.DocumentsOwnerSpecified = true;
                }
                prDocument.DocCurrency = pObject.DocCur;
                prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
                prDocument.DocRateSpecified = true;
                prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
                prDocument.ContactPersonCodeSpecified = true;
                prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
                prDocument.PaymentGroupCodeSpecified = true;
                prDocument.PaymentMethod = pObject.PeyMethod;

                if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
                {
                    prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                    prDocument.DiscountPercentSpecified = true;
                }
                prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
                prDocument.TransportationCodeSpecified = true;
                //prDocument.Address = pObject.Address;
                //prDocument.Address2 = pObject.Address2;
                prDocument.Comments = pObject.Comments;
                prDocument.JournalMemo = pObject.JrnlMemo;
                //prDocument.DocTotal = (double)pObject.DocTotal;

                SalesOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesOrderServiceWeb.DocumentAddressExtension();
                //Defino los campos del shipto
                oDocumentAddress.ShipToStreet = pObject.SOAddress.StreetS;
                oDocumentAddress.ShipToStreetNo = pObject.SOAddress.StreetNoS;
                oDocumentAddress.ShipToBlock = pObject.SOAddress.BlockS;
                oDocumentAddress.ShipToBuilding = pObject.SOAddress.BuildingS;
                oDocumentAddress.ShipToCity = pObject.SOAddress.CityS;
                oDocumentAddress.ShipToZipCode = pObject.SOAddress.ZipCodeS;
                oDocumentAddress.ShipToCounty = pObject.SOAddress.CountyS;
                oDocumentAddress.ShipToState = pObject.SOAddress.StateS;
                oDocumentAddress.ShipToCountry = pObject.SOAddress.CountryS;
                oDocumentAddress.ShipToGlobalLocationNumber = pObject.SOAddress.GlbLocNumS;
                //Defino los campos del billto
                oDocumentAddress.BillToStreet = pObject.SOAddress.StreetB;
                oDocumentAddress.BillToStreetNo = pObject.SOAddress.StreetNoB;
                oDocumentAddress.BillToBlock = pObject.SOAddress.BlockB;
                oDocumentAddress.BillToBuilding = pObject.SOAddress.BuildingB;
                oDocumentAddress.BillToCity = pObject.SOAddress.CityB;
                oDocumentAddress.BillToZipCode = pObject.SOAddress.ZipCodeB;
                oDocumentAddress.BillToCounty = pObject.SOAddress.CountyB;
                oDocumentAddress.BillToState = pObject.SOAddress.StateB;
                oDocumentAddress.BillToCountry = pObject.SOAddress.CountryB;
                oDocumentAddress.BillToGlobalLocationNumber = pObject.SOAddress.GlbLocNumB;

                prDocument.AddressExtension = oDocumentAddress;

                foreach (SalesOrderServiceWeb.DocumentDocumentAdditionalExpense expenseLine in prDocument.DocumentAdditionalExpenses)
                {
                    Freight portalExpenseLine = pObject.ListFreight.Where(c => c.LineNum == expenseLine.LineNum).FirstOrDefault();
                    if (portalExpenseLine != null)
                    {
                        switch (portalExpenseLine.DistrbMthd)
                        {
                            case "N":
                                expenseLine.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                                expenseLine.DistributionMethodSpecified = true;
                                break;
                            case "Q":
                                expenseLine.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                                expenseLine.DistributionMethodSpecified = true;
                                break;
                            case "V":
                                expenseLine.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                                expenseLine.DistributionMethodSpecified = true;
                                break;
                            case "W":
                                expenseLine.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                                expenseLine.DistributionMethodSpecified = true;
                                break;
                            case "E":
                                expenseLine.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                                expenseLine.DistributionMethodSpecified = true;
                                break;
                            case "T":
                                expenseLine.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                                expenseLine.DistributionMethodSpecified = true;
                                break;
                        }
                        expenseLine.ExpenseCode = portalExpenseLine.ExpnsCode.Value;
                        expenseLine.ExpenseCodeSpecified = true;
                        expenseLine.DistributionRule = portalExpenseLine.OcrCode;
                        expenseLine.Remarks = portalExpenseLine.Comments;
                        expenseLine.TaxCode = portalExpenseLine.TaxCode;
                        expenseLine.LineTotal = (double)portalExpenseLine.LineTotal;
                        expenseLine.LineTotalSpecified = true;
                        expenseLine.Project = portalExpenseLine.Project;
                    }

                }

                string mUrl = string.Empty;

                foreach (SalesOrderServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
                {
                    SaleOrderSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                    if (portalLine != null)
                    {
                        if (docLine.LineStatus != SalesOrderServiceWeb.DocumentDocumentLineLineStatus.bost_Close)
                        {
                            docLine.ItemCode = portalLine.ItemCode;
                            docLine.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                            docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                            docLine.QuantitySpecified = true;
                            docLine.SupplierCatNum = portalLine.SubCatNum;
                            docLine.AccountCode = portalLine.AcctCode;
                            docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                            docLine.PriceSpecified = true;
                            docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                            docLine.UnitPriceSpecified = true;
                            docLine.Currency = portalLine.Currency;
                            docLine.WarehouseCode = portalLine.WhsCode;
                            docLine.CostingCode = portalLine.OcrCode;
                            docLine.CostingCode2 = portalLine.OcrCode2;
                            docLine.TaxCode = portalLine.TaxCode;
                            docLine.ProjectCode = portalLine.Project;
                            docLine.FreeText = portalLine.FreeTxt;
                            docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                            docLine.SalesPersonCodeSpecified = true;

                            switch (portalLine.TreeType)
                            {
                                case "S":
                                    docLine.TreeType = SalesOrderServiceWeb.DocumentDocumentLineTreeType.iSalesTree;
                                    break;
                                case "I":
                                    docLine.TreeType = SalesOrderServiceWeb.DocumentDocumentLineTreeType.iIngredient;
                                    break;
                                default:
                                    docLine.TreeType = SalesOrderServiceWeb.DocumentDocumentLineTreeType.iNotATree;
                                    break;
                            }



                            if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                            {
                                docLine.ShipDate = portalLine.ShipDate.Value;
                                docLine.ShipDateSpecified = true;
                            }
                            if (portalLine.AcctCode != null)
                            {
                                docLine.AccountCode = portalLine.AcctCode;
                            }
                            if (portalLine.DiscPrcnt.HasValue)
                            {
                                docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                                docLine.DiscountPercentSpecified = true;
                            }

                            if (portalLine.VatPrcnt.HasValue)
                            {
                                docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                                docLine.TaxPercentagePerRowSpecified = true;
                            }

                            //ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == docLine.ItemCode).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Items", "ItemCode eq '" + docLine.ItemCode + "'");

                            ItemMasterSAP mOitm = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                            if (mOitm == null)
                            {
                                return "Error: Invalid ItemCode";
                            }

                            if (!string.IsNullOrEmpty(portalLine.UomCode))
                            {
                                //OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUOM", "UomCode eq '" + portalLine.UomCode + "'");

                                OUOM mOUOM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUOM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                                if (mOUOM != null)
                                {
                                    docLine.UoMCode = portalLine.UomCode;
                                    docLine.UoMEntry = mOUOM.UomEntry;
                                    docLine.UoMEntrySpecified = true;
                                }
                            }
                            #region Line Freight

                            if (docLine.DocumentLineAdditionalExpenses.Count() > 0)
                            {
                                if (portalLine.Freight[0].ExpnsCode.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCode = portalLine.Freight[0].ExpnsCode.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCodeSpecified = true;
                                }
                                if (portalLine.Freight[0].LineTotal.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotal = (double)portalLine.Freight[0].LineTotal.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotalSpecified = true;
                                }
                            }
                            else
                            {
                                docLine.DocumentLineAdditionalExpenses = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense[1];
                                docLine.DocumentLineAdditionalExpenses[0] = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense();
                                if (portalLine.Freight[0].ExpnsCode.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCode = portalLine.Freight[0].ExpnsCode.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCodeSpecified = true;
                                }
                                if (portalLine.Freight[0].LineTotal.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotal = (double)portalLine.Freight[0].LineTotal.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotalSpecified = true;
                                }
                            }

                            #endregion Line Freight

                            #region Line Batchs And Serials

                            if (mOitm.ManBtchNum == "Y")
                            {
                                if (docLine.BatchNumbers.Count() > 0)
                                {
                                    if (!string.IsNullOrEmpty(portalLine.SerialBatch))
                                    {
                                        docLine.BatchNumbers.FirstOrDefault().BatchNumber = portalLine.SerialBatch;
                                        docLine.BatchNumbers.FirstOrDefault().InternalSerialNumber = portalLine.SerialBatch;
                                        docLine.BatchNumbers.FirstOrDefault().ManufacturerSerialNumber = portalLine.SerialBatch;
                                        docLine.BatchNumbers.FirstOrDefault().Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                                    }
                                    else
                                    {
                                        docLine.BatchNumbers = null;
                                    }

                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(portalLine.SerialBatch))
                                    {
                                        SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[] dBatchNumber = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[1];

                                        dBatchNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber();

                                        dBatchNumber[0].BaseLineNumber = portalLine.LineNum;

                                        dBatchNumber[0].BaseLineNumberSpecified = true;

                                        dBatchNumber[0].BatchNumber = portalLine.SerialBatch;

                                        dBatchNumber[0].AddmisionDate = DateTime.Now;

                                        dBatchNumber[0].AddmisionDateSpecified = true;

                                        dBatchNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                                        dBatchNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                                        dBatchNumber[0].Notes = "";

                                        dBatchNumber[0].Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);

                                        dBatchNumber[0].QuantitySpecified = true;

                                        docLine.BatchNumbers = dBatchNumber;
                                    }
                                }
                            }

                            if (mOitm.ManSerNum == "Y")
                            {

                                if (portalLine.Serials != null && portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count() > 0)
                                {
                                    SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count()];
                                    int i = 0;
                                    foreach (Serial itemSerial in portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum))
                                    {
                                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSRN", $"DistNumber eq '{itemSerial.DistNumber.Trim()}'");
                                        var OSRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSRN>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();


                                        dSerialNumber[i] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();
                                        dSerialNumber[i].BaseLineNumber = portalLine.LineNum;
                                        dSerialNumber[i].BaseLineNumberSpecified = true;
                                        dSerialNumber[i].Quantity = Convert.ToDouble(1);
                                        dSerialNumber[i].QuantitySpecified = true;
                                        dSerialNumber[i].SystemSerialNumber = OSRN.FirstOrDefault().SysNumber;
                                        dSerialNumber[i].SystemSerialNumberSpecified = true;
                                        dSerialNumber[i].ItemCode = portalLine.ItemCode;

                                        i++;
                                    }

                                    docLine.SerialNumbers = dSerialNumber;

                                }

                            }

                            //if (mOitm.ManSerNum == "Y")
                            //{

                            //    if (docLine.SerialNumbers.Count() > 0)
                            //    {
                            //        if (!string.IsNullOrEmpty(portalLine.SerialBatch))
                            //        {
                            //            //OSRN mOSRN = mDbContext.OSRN.Where(c => c.DistNumber == portalLine.SerialBatch.ToString().Trim()).FirstOrDefault();

                            //            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSRN", "DistNumber eq '" + portalLine.SerialBatch.ToString().Trim() + "'");

                            //            OSRN mOSRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSRN>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                            //            if (mOSRN != null)
                            //            {
                            //                docLine.SerialNumbers.FirstOrDefault().SystemSerialNumber = mOSRN.SysNumber;
                            //            }

                            //            docLine.SerialNumbers.FirstOrDefault().InternalSerialNumber = portalLine.SerialBatch;
                            //        }
                            //        else
                            //        {
                            //            docLine.SerialNumbers = null;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        if (!string.IsNullOrEmpty(portalLine.SerialBatch))
                            //        {
                            //            SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[1];

                            //            dSerialNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();

                            //            dSerialNumber[0].BaseLineNumber = portalLine.LineNum;

                            //            dSerialNumber[0].BaseLineNumberSpecified = true;

                            //            dSerialNumber[0].BatchID = portalLine.SerialBatch;

                            //            dSerialNumber[0].Quantity = Convert.ToDouble(1);

                            //            dSerialNumber[0].QuantitySpecified = true;

                            //            dSerialNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                            //            dSerialNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                            //            dSerialNumber[0].Notes = "";

                            //            docLine.SerialNumbers = dSerialNumber;
                            //        }
                            //    }
                            //}

                            #endregion Line Batchs And Serials

                            docLine.ShipToDescription = Miscellaneous.ReplaceUTF(docLine.ShipToDescription);
                        }
                        pObject.Lines.Remove(portalLine);
                    }
                    else
                    {
                        //prDocument.DocumentLines.ToList().Remove(docLine);
                        prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                    }
                }
                SalesOrderServiceWeb.DocumentDocumentLine[] newLines = new SalesOrderServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
                prDocument.DocumentLines.CopyTo(newLines, 0);
                int positionNewLine = prDocument.DocumentLines.Length;
                foreach (SaleOrderSAPLine portalLine in pObject.Lines)
                {
                    SalesOrderServiceWeb.DocumentDocumentLine line = new SalesOrderServiceWeb.DocumentDocumentLine();
                    line.LineNum = portalLine.LineNum;
                    line.LineNumSpecified = true;
                    line.ItemCode = portalLine.ItemCode;
                    line.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                    line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    line.QuantitySpecified = true;
                    line.SupplierCatNum = portalLine.SubCatNum;
                    line.AccountCode = portalLine.AcctCode;
                    line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    line.PriceSpecified = true;
                    line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    line.UnitPriceSpecified = true;
                    line.Currency = portalLine.Currency;
                    line.WarehouseCode = portalLine.WhsCode;
                    line.CostingCode = portalLine.OcrCode;
                    line.CostingCode2 = portalLine.OcrCode2;
                    line.TaxCode = portalLine.TaxCode;
                    line.ProjectCode = portalLine.Project;
                    line.FreeText = portalLine.FreeTxt;
                    line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    line.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        line.ShipDate = portalLine.ShipDate.Value;
                        line.ShipDateSpecified = true;
                    }
                    if (portalLine.AcctCode != null)
                    {
                        line.AccountCode = portalLine.AcctCode;
                    }

                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        line.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        line.TaxPercentagePerRowSpecified = true;
                    }

                    //ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == portalLine.ItemCode).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Items", "ItemCode eq '" + portalLine.ItemCode + "'");

                    ItemMasterSAP mOitm = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ItemMasterSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                    if (!string.IsNullOrEmpty(portalLine.UomCode))
                    {
                        //OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUOM", "UomCode eq '" + portalLine.UomCode + "'");

                        OUOM mOUOM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUOM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                        if (mOUOM != null)
                        {
                            line.UoMCode = portalLine.UomCode;
                            line.UoMEntry = mOUOM.UomEntry;
                            line.UoMEntrySpecified = true;
                        }
                    }

                    if (mOitm.ManBtchNum == "Y" && !string.IsNullOrEmpty(portalLine.SerialBatch))
                    {
                        SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[] dBatchNumber = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[1];

                        dBatchNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber();

                        dBatchNumber[0].BaseLineNumber = portalLine.LineNum;

                        dBatchNumber[0].BaseLineNumberSpecified = true;

                        dBatchNumber[0].BatchNumber = portalLine.SerialBatch;

                        dBatchNumber[0].AddmisionDate = DateTime.Now;

                        dBatchNumber[0].AddmisionDateSpecified = true;

                        dBatchNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                        dBatchNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                        dBatchNumber[0].Notes = "";

                        dBatchNumber[0].Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);

                        dBatchNumber[0].QuantitySpecified = true;

                        line.BatchNumbers = dBatchNumber;

                    }

                    //if (mOitm.ManSerNum == "Y" && !string.IsNullOrEmpty(portalLine.SerialBatch))
                    //{
                    //    SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[1];

                    //    dSerialNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();

                    //    dSerialNumber[0].BaseLineNumber = portalLine.LineNum;

                    //    dSerialNumber[0].BaseLineNumberSpecified = true;

                    //    dSerialNumber[0].BatchID = portalLine.SerialBatch;

                    //    dSerialNumber[0].Quantity = Convert.ToDouble(1);

                    //    dSerialNumber[0].QuantitySpecified = true;

                    //    dSerialNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                    //    dSerialNumber[0].Notes = "";

                    //    line.SerialNumbers = dSerialNumber;

                    //}

                    if (mOitm.ManSerNum == "Y")
                    {

                        if (portalLine.Serials != null && portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count() > 0)
                        {
                            SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count()];
                            int i = 0;
                            foreach (Serial itemSerial in portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum))
                            {
                                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OSRN", $"DistNumber eq '{itemSerial.DistNumber.Trim()}'");
                                var OSRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSRN>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();


                                dSerialNumber[i] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();
                                dSerialNumber[i].BaseLineNumber = portalLine.LineNum;
                                dSerialNumber[i].BaseLineNumberSpecified = true;
                                dSerialNumber[i].Quantity = Convert.ToDouble(1);
                                dSerialNumber[i].QuantitySpecified = true;
                                dSerialNumber[i].SystemSerialNumber = OSRN.FirstOrDefault().SysNumber;
                                dSerialNumber[i].SystemSerialNumberSpecified = true;
                                dSerialNumber[i].ItemCode = portalLine.ItemCode;

                                i++;
                            }

                            line.SerialNumbers = dSerialNumber;

                        }

                    }

                    newLines[positionNewLine] = line;
                    positionNewLine += 1;
                }
                prDocument.DocumentLines = new SalesOrderServiceWeb.DocumentDocumentLine[newLines.Length];
                prDocument.DocumentLines = newLines;

                if (pObject.AttachmentList.Count <= 0)
                {
                    prDocument.AttachmentEntrySpecified = false;
                }

                //XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                //if (pObject.MappedUdf.Count > 0)
                //{ mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                //XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                //foreach (XmlNode mAuxNode in mAuxNodeList)
                //{
                //    SaleOrderSAPLine mAuxLine = mSOIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                //    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                //    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                //}

                //mB1WSHandler.ProcessDoc(mXmlDoc);

                //if (prDocument.AttachmentEntry != 0)
                //{
                //    string AddCmd = string.Empty;
                //    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                //    XmlDocument mResultXML = new XmlDocument();
                //    AddCmd = @"<?xml version=""1.0"" ?>" +
                //            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                //            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                //            @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
                //    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                //    {
                //        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
                //    }

                //    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
                //    mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                //}
                //else
                //{
                //    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                //    {
                //        string AddCmd = string.Empty;
                //        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                //        XmlDocument mResultXML = new XmlDocument();
                //        AddCmd = @"<?xml version=""1.0"" ?>" +
                //                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                //                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                //                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                //        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                //        {
                //            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                //        }

                //        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                //        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                //        prDocument = oSalesOrderService.GetByParams(parametros);
                //        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                //        {
                //            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                //            prDocument.AttachmentEntrySpecified = true;
                //        }
                //        oSalesOrderService.Update(prDocument);
                //    }
                //}


                //var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

                //if (udfIdPayment != "")
                //{
                //    var idPayment = Convert.ToInt32(pObject.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

                //    AddPaymentByOrder(pCompanyParam, pObject.PaymentMeanOrder, 17, Convert.ToInt32(idPayment), Convert.ToInt32(idPayment));
                //}

                //prDocument.ClosingOption = SalesOrderServiceWeb.DocumentClosingOption.coByCurrentSystemDate;

                var resultAPI = string.Empty;

                dynamic documentToAdd = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)documentToAdd;



                foreach (var property in prDocument.GetType().GetProperties())
                {
                    if (property.Name != "DocumentLines")
                    {
                        string value = (property.GetValue(prDocument) ?? "").ToString();

                        if (value != "0" && value != "0.0" && property.Name != "Letter" && property.Name != "EDocGenerationType" && property.Name != "StartFrom" && property.Name != "ClosingOption" && property.Name != "ApplyCurrentVATRatesForDownPaymentsToDraw" && property.Name != "DownPaymentStatus")
                        {
                            dictionary.Add(property.Name, property.GetValue(prDocument));
                        }

                    }

                }

                List<dynamic> linesToAdd = new List<dynamic>();

                foreach (var item in prDocument.DocumentLines)
                {
                    SaleOrderSAPLine mAuxLine = mSOIni.Lines.Where(c => c.LineNum == item.LineNum).FirstOrDefault();

                    dynamic documentLine = new ExpandoObject();
                    var dictionaryLine = (IDictionary<string, object>)documentLine;

                    foreach (var property in item.GetType().GetProperties())
                    {
                        if (property.Name != "SerialNumbers")
                        {
                            string value = (property.GetValue(item) ?? "").ToString();

                            if (value != "0" && value != "0.0" || property.Name == "LineNum")
                            {
                                var valueText = string.Empty;

                                if (property.Name == "TreeType")
                                {
                                    switch (value)
                                    {
                                        case "iSalesTree":
                                            valueText = "iSalesTree";
                                            break;
                                        case "iIngredient":
                                            valueText = "iIngredient";
                                            break;
                                        default:
                                            valueText = "iNotATree";
                                            break;
                                    }

                                    dictionaryLine.Add(property.Name, valueText);
                                }
                                else
                                {
                                    dictionaryLine.Add(property.Name, property.GetValue(item));
                                }


                                //switch (portalLine.TreeType)
                                //{
                                //    case "S":
                                //        docLine.TreeType = SalesOrderServiceWeb.DocumentDocumentLineTreeType.iSalesTree;
                                //        break;
                                //    case "I":
                                //        docLine.TreeType = SalesOrderServiceWeb.DocumentDocumentLineTreeType.iIngredient;
                                //        break;
                                //    default:
                                //        docLine.TreeType = SalesOrderServiceWeb.DocumentDocumentLineTreeType.iNotATree;
                                //        break;
                                //}



                            }
                        }

                    }
                    List<dynamic> linesSerialToAdd = new List<dynamic>();



                    if (item.SerialNumbers != null)
                    {
                        foreach (var itemSer in item.SerialNumbers)
                        {
                            dynamic documentSerial = new ExpandoObject();
                            var dictionarySerial = (IDictionary<string, object>)documentSerial;

                            foreach (var property in itemSer.GetType().GetProperties())
                            {
                                //if (property.Name == "ItemCode" || property.Name == "Quantity" || property.Name == "BaseLineNumber" || property.Name == "Location" || property.Name == "Notes" || property.Name == "BatchID" || property.Name == "SystemSerialNumber")
                                if (property.Name == "Quantity" || property.Name == "BaseLineNumber" || property.Name == "Location" || property.Name == "Notes" || property.Name == "BatchID" || property.Name == "SystemSerialNumber")
                                {
                                    string value = (property.GetValue(itemSer) ?? "").ToString();

                                    dictionarySerial.Add(property.Name, property.GetValue(itemSer));

                                }

                            }

                            linesSerialToAdd.Add(documentSerial);

                        }

                        dictionaryLine.Add("SerialNumbers", linesSerialToAdd.ToArray());
                    }

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    {
                        foreach (var itemUdfLine in mAuxLine.MappedUdf)
                        {
                            dictionaryLine.Add(itemUdfLine.UDFName, itemUdfLine.Value);
                        }

                    }

                    linesToAdd.Add(documentLine);

                }



                dictionary.Add("DocumentLines", linesToAdd.ToArray());

                foreach (UDF_ARGNS mUDF in pObject.MappedUdf)
                {
                    dictionary.Add(mUDF.UDFName, mUDF.Value);
                }

                var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

                var result = SLManager.callAPI(pCompanyParam, $"Orders({prDocument.DocEntry})", RestSharp.Method.PATCH, jsonToAdd);

                //var resultObject = JsonConvert.DeserializeObject<ORDR>(result.Content);

                if (result.Content == "")
                {

                    var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

                    if (udfIdPayment != "")
                    {
                        var idPayment = Convert.ToInt32(pObject.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

                        AddPaymentByOrder(pCompanyParam, pObject.PaymentMeanOrder, 17, Convert.ToInt32(idPayment), Convert.ToInt32(idPayment));
                    }


                    resultAPI = "Ok";

                }
                else
                {
                    var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                    resultAPI = "Error:" + errorAPI.error.message.value;

                }

                return resultAPI;
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pDocEntry"></param>
        /// <param name="pListUDFORDR"></param>
        /// <param name="pListUDFRDR1"></param>
        /// <param name="pListUDFCRD1"></param>
        /// <returns></returns>
		public SaleOrderSAP GetAllbySO(CompanyConn pCompanyParam,
            int pDocEntry, bool pShowOpenQuantity,
            List<UDF_ARGNS> pListUDFORDR = null,
            List<UDF_ARGNS> pListUDFRDR1 = null,
            List<UDF_ARGNS> pListUDFCRD1 = null)
        {
            string mUrl = string.Empty;
            string mFilters = "DocEntry eq " + pDocEntry;

            Mapper.CreateMap<SaleOrderSAP, ORDR>();
            Mapper.CreateMap<ORDR, SaleOrderSAP>();

            Mapper.CreateMap<SaleOrderSAPLine, RDR1>();
            Mapper.CreateMap<RDR1, SaleOrderSAPLine>();

            Mapper.CreateMap<DocumentAddress, RDR12>();
            Mapper.CreateMap<RDR12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            Mapper.CreateMap<SaleOrderSAPLineFreight, RDR2>();
            Mapper.CreateMap<RDR2, SaleOrderSAPLineFreight>();

            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            SaleOrderSAP mSaleOrderSAP = new SaleOrderSAP();
            try
            {
                string url = RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrder", mFilters);
                string mResultJson = RESTService.GetRequestJson(url, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                ORDR mOPOR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ORDR>>(mResultJson).FirstOrDefault();
                if (mOPOR != null)
                {
                    mFilters = "TableID eq 'ORDR'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    Newtonsoft.Json.Linq.JArray mJArrayORDR = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                    mOPOR.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFORDR, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayORDR[0]);
                    mFilters = "DocEntry eq '" + pDocEntry + "'";
                    mSaleOrderSAP = Mapper.Map<SaleOrderSAP>(mOPOR);
                    mSaleOrderSAP = FillSODocumentSAP(pCompanyParam, mSaleOrderSAP);
                    //DocumentLines 
                    string mResultJsonLines = RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SaleOrderLine", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                    Newtonsoft.Json.Linq.JArray mJArrayRDR1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                    List<RDR1> mLisRDR1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR1>>(mResultJsonLines);
                    mLisRDR1 = mLisRDR1.Where(c => c.TreeType != "I").ToList();
                    mSaleOrderSAP.Lines = Mapper.Map<List<SaleOrderSAPLine>>(mLisRDR1);

                    List<OITL> mListOITL = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITL>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "OITL", "DocEntry eq " + pDocEntry + " and  ApplyType eq " + 17), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                    mSaleOrderSAP.Lines = mSaleOrderSAP.Lines.Select(c => { c.GLAccount = mSaleOrderSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();

                    foreach (SaleOrderSAPLine item in mSaleOrderSAP.Lines)
                    {
                        mFilters = "TableID eq 'RDR1'";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                        mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFRDR1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayRDR1.Where(c => (int)c["LineNum"] == item.LineNum).FirstOrDefault());
                        List<OITM> mListOITM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OITM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Items", "ItemCode eq '" + HttpUtility.UrlEncode(item.ItemCode) + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFilters = "DocEntry eq " + item.DocEntry + " and LineNum eq " + item.LineNum + " and GroupNum eq 0";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "RDR2", mFilters);
                        item.Freight = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SaleOrderSAPLineFreight>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mFilters = "DocEntry eq '" + pDocEntry + "'";

                        ItemMasterSAP mOitm = mListOITM.Where(c => c.ItemCode == item.ItemCode).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum, UgpEntry = c.UgpEntry }).FirstOrDefault();

                        mFilters = "UgpEntry eq " + mOitm.UgpEntry;
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUGP", mFilters);
                        OUGP mOUGPAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUGP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                        if (mOUGPAux != null)
                        {
                            item.UomGroupName = mOUGPAux.UgpName;
                        }
                        item.UnitOfMeasureList = GetUOMLists(pCompanyParam, mOitm.UgpEntry);

                        int mLogEntry = mListOITL.Where(c => c.ItemCode == item.ItemCode && c.DocLine == item.LineNum).LastOrDefault() != null ? (mListOITL.Where(c => c.ItemCode == item.ItemCode && c.DocLine == item.LineNum).LastOrDefault().DocQty > 0 ? mListOITL.Where(c => c.ItemCode == item.ItemCode && c.DocLine == item.LineNum).LastOrDefault().LogEntry : 0) : 0;

                        int SysNum = 0;

                        if (mLogEntry != 0)
                        {

                            SysNum = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ITL1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ITL1", "LogEntry eq " + mLogEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault().SysNumber;

                            if (mOitm.ManBtchNum == "Y")
                            {
                                item.SerialBatch = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OBTN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "OBTN", "SysNumber eq " + SysNum), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault().DistNumber;
                            }
                            else
                            {
                                //item.SerialBatch = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "OSRN", "SysNumber eq " + SysNum), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault().DistNumber;
                            }

                        }

                        ///Open Quantity Status
                        if (pShowOpenQuantity)
                        {
                            if (item.LineStatus == "C")
                            {
                                item.OpenQuantityStatus = OpenQuantityStatus.Green;
                            }
                            else
                            {
                                if (item.OpenQty == 0)
                                {
                                    if (item.ShipDate < DateTime.Now)
                                    {
                                        item.OpenQuantityStatus = OpenQuantityStatus.Red;
                                    }
                                    else
                                    {
                                        item.OpenQuantityStatus = OpenQuantityStatus.Green;
                                    }
                                }
                                else
                                {
                                    if (item.ShipDate < DateTime.Now)
                                    {
                                        item.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                    }
                                    else
                                    {
                                        item.OpenQuantityStatus = OpenQuantityStatus.Green;
                                    }
                                }
                            }
                        }

                        item.ManBtchNum = mOitm.ManBtchNum;
                        item.ManSerNum = mOitm.ManSerNum;
                    }

                    //DocumentAddress
                    List<RDR12> mListRDR12 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR12>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentAddressSO", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSaleOrderSAP.SOAddress = Mapper.Map<DocumentAddress>(mListRDR12.FirstOrDefault());

                    if (pListUDFCRD1 != null)
                    {
                        if (!string.IsNullOrEmpty(mSaleOrderSAP.ShipToCode))
                        {
                            mFilters = "Address eq '" + mSaleOrderSAP.ShipToCode + "' and CardCode eq '" + mSaleOrderSAP.CardCode + "' and AdresType eq 'S'";
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BPAddresses", mFilters);
                            mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                            CRD1 mCRD1ShipToAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CRD1>>(mResultJson).FirstOrDefault();
                            if (mCRD1ShipToAux != null)
                            {
                                mFilters = "TableID eq 'CRD1'";
                                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                                mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                                Newtonsoft.Json.Linq.JArray mJArrayCRD1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                                mCRD1ShipToAux.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFCRD1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayCRD1[0]);
                                mSaleOrderSAP.SOAddress.MappedUdfShipTo = mCRD1ShipToAux.MappedUdf;
                            }
                        }
                        if (!string.IsNullOrEmpty(mSaleOrderSAP.PayToCode))
                        {
                            mFilters = "Address eq '" + mSaleOrderSAP.PayToCode + "' and CardCode eq '" + mSaleOrderSAP.CardCode + "' and AdresType eq 'B'";
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "BPAddresses", mFilters);
                            mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                            CRD1 mCRD1ShipToAux = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CRD1>>(mResultJson).FirstOrDefault();
                            if (mCRD1ShipToAux != null)
                            {
                                mFilters = "TableID eq 'CRD1'";
                                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                                mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                                Newtonsoft.Json.Linq.JArray mJArrayCRD1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                                mCRD1ShipToAux.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFCRD1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayCRD1[0]);
                                mSaleOrderSAP.SOAddress.MappedUdfBillTo = mCRD1ShipToAux.MappedUdf;
                            }
                        }
                    }

                    //Employees
                    OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (mOPOR.OwnerCode ?? 0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    if (mOHEM != null)
                    { mSaleOrderSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM); }

                    //Attachments
                    if (mSaleOrderSAP.AtcEntry != null)
                    {
                        List<ATC1> mATC1List = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ATC1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ATC1", "AbsEntry eq " + mSaleOrderSAP.AtcEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mSaleOrderSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);
                    }

                    mSaleOrderSAP.ListFreight = new List<Freight>();
                    mSaleOrderSAP.ListFreight.Add(new Freight());
                    mSaleOrderSAP.ListFreight.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Freight>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }
                else
                {
                    mSaleOrderSAP = FillSODocumentSAP(pCompanyParam, mSaleOrderSAP);

                    //CompanyAddress
                    List<ADM1> mListADM1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ADM1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CompanyAddress", "Code eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSaleOrderSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                    //Employees
                    OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                    if (mOHEM != null)
                    { mSaleOrderSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM); }

                    mSaleOrderSAP.ListFreight = new List<Freight>();
                    mSaleOrderSAP.ListFreight.Add(new Freight());
                    mSaleOrderSAP.ListFreight.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Freight>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
                }
                List<PaymentMeanOrder> paymentMeanOrder = new List<PaymentMeanOrder>();
                try
                {

                    var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

                    if (udfIdPayment != "")
                    {
                        var idPayment = Convert.ToInt32(mSaleOrderSAP.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMeanByOrder", $"U_DocType eq {17} and U_DocEntryOrder eq {idPayment}");
                        paymentMeanOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMeanOrder>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                    }

                    //mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMeanByOrder", $"U_DocType eq {17} and U_DocEntryOrder eq {mSaleOrderSAP.DocEntry}");
                    //paymentMeanOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMeanOrder>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                }
                catch (Exception)
                {

                }

                mSaleOrderSAP.PaymentMeanOrder = paymentMeanOrder;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetAllBySO :" + ex.Message);
                return null;
            }

            return mSaleOrderSAP;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSaleOrderSAP"></param>
        /// <returns></returns>
		public SaleOrderSAP FillSODocumentSAP(CompanyConn pCompanyParam,
            SaleOrderSAP pSaleOrderSAP)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();


            //Buyer
            List<OSLP> mLisOSLP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSLP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Currency
            List<OCRN> mListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentMethod
            List<OCTG> mListOCTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMethod", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentTerm
            //List<OPYM> mListOPYM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPYM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentTerm", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //ShippingType
            List<OSHP> mListOSHP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSHP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Taxt
            List<OVTG> mListOVTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OVTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Taxt", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);
            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();


            if (!string.IsNullOrEmpty(pSaleOrderSAP.CardCode))
            {
                retCb.ListPaymentMethod.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMethod>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CRD2", "CardCode eq '" + pSaleOrderSAP.CardCode + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
            }

            //retCb.ListPaymentMethod = mListOPYM.Select(c => new PaymentMethod { PayMethCod = c.PayMethCod, Descript = c.Descript }).ToList(); 

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pSaleOrderSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OACT>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", "FormatCode ne 'Null'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleOrderSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CINF>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentSettings", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleOrderSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWHS>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleOrderSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pSaleOrderSAP.LocalCurrency = GetCurrency(true, pCompanyParam);
            pSaleOrderSAP.SystemCurrency = GetCurrency(false, pCompanyParam);

            //DistrbRule
            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleOrderSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pSaleOrderSAP;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pDocEntry"></param>
        /// <returns></returns>
		public List<Freight> GetSalesOrderFreights(CompanyConn pCompanyParam, int pDocEntry)
        {

            List<RDR3> mListRDR3 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RDR3>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "RDR3", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OEXD> mListOEXD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OEXD>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<Freight> mListFreight = (from mRDR3 in mListRDR3
                                          join mOEXD in mListOEXD on mRDR3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mRDR3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mRDR3.DocEntry,
                                              ExpnsCode = mRDR3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mRDR3.LineTotal,
                                              TotalFrgn = mRDR3.TotalFrgn,
                                              Comments = mRDR3.Comments,
                                              ObjType = mRDR3.ObjType,
                                              DistrbMthd = mRDR3.DistrbMthd,
                                              VatSum = mRDR3.VatSum,
                                              TaxCode = mRDR3.TaxCode,
                                              LineNum = mRDR3.LineNum,
                                              Status = mRDR3.Status,
                                              OcrCode = mRDR3.OcrCode,
                                              TaxDistMtd = mRDR3.TaxDistMtd
                                          }).ToList();


            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }


            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OSTC> mListOSTC = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSTC>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesTaxCodes", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OPRJ> mListOPRJ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPRJ>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Project", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;

        }

        #endregion

        #region Sales Quotation

        public List<Freight> GetSalesQuotationFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            Mapper.CreateMap<OOCR, DistrRuleSAP>();
            Mapper.CreateMap<DistrRuleSAP, OOCR>();

            Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
            Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

            Mapper.CreateMap<OPRJ, ProjectSAP>();
            Mapper.CreateMap<ProjectSAP, OPRJ>();

            List<QUT3> mListQUT3 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<QUT3>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "QUT3", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OEXD> mListOEXD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OEXD>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<Freight> mListFreight = (from mQUT3 in mListQUT3
                                          join mOEXD in mListOEXD on mQUT3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mQUT3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mQUT3.DocEntry,
                                              ExpnsCode = mQUT3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mQUT3.LineTotal,
                                              TotalFrgn = mQUT3.TotalFrgn,
                                              Comments = mQUT3.Comments,
                                              ObjType = mQUT3.ObjType,
                                              DistrbMthd = mQUT3.DistrbMthd,
                                              VatSum = mQUT3.VatSum,
                                              TaxCode = mQUT3.TaxCode,
                                              LineNum = mQUT3.LineNum,
                                              Status = mQUT3.Status,
                                              OcrCode = mQUT3.OcrCode,
                                              TaxDistMtd = mQUT3.TaxDistMtd
                                          }).ToList();


            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }


            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OSTC> mListOSTC = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSTC>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesTaxCodes", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OPRJ> mListOPRJ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPRJ>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Project", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult AddSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            SalesQuotationServiceWeb.MsgHeader header = new SalesQuotationServiceWeb.MsgHeader();
            header.ServiceName = SalesQuotationServiceWeb.MsgHeaderServiceName.QuotationsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            SalesQuotationServiceWeb.QuotationsService oSalesQuotationService = new SalesQuotationServiceWeb.QuotationsService();
            oSalesQuotationService.MsgHeaderValue = header;

            SalesQuotationSAP mSQIni = new SalesQuotationSAP();
            Mapper.CreateMap(typeof(SalesQuotationSAP), typeof(SalesQuotationSAP));
            Mapper.Map(pObject, mSQIni);

            SalesQuotationServiceWeb.Document prDocument = new SalesQuotationServiceWeb.Document();
            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;

            if (pCompanyParam.IdBranchSelect != null && pCompanyParam.IdBranchSelect > 0)
            {
                prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
                prDocument.BPL_IDAssignedToInvoiceSpecified = true;
            }

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            //prDocument.DocTotal = (double)pObject.DocTotal;

            SalesQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.SQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.SQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.SQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.SQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.SQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.SQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.SQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.SQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.SQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.SQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.SQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.SQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.SQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.SQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.SQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.SQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.SQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.SQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.SQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.SQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense();

                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocEntry = expenseLine.BaseAbsEnt ?? 0;
                    oDocumentExpenses.BaseDocEntrySpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocLine = expenseLine.BaseLnNum ?? 0;
                    oDocumentExpenses.BaseDocLineSpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocType = expenseLine.BaseType ?? 0;
                    oDocumentExpenses.BaseDocTypeSpecified = true;
                }


                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }


                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                if (expenseLine.ExpnsCode != null)
                {
                    oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode ?? 0;
                    oDocumentExpenses.ExpenseCodeSpecified = true;
                }
                oDocumentExpenses.LineNum = expenseLine.LineNum;
                oDocumentExpenses.LineNumSpecified = true;
                oDocumentExpenses.Project = expenseLine.Project;
                oDocumentExpenses.Remarks = expenseLine.Comments;


                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            SalesQuotationServiceWeb.DocumentDocumentLine[] newLines = new SalesQuotationServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;
            foreach (SalesQuotationSAPLine portalLine in pObject.Lines)
            {
                SalesQuotationServiceWeb.DocumentDocumentLine line = new SalesQuotationServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.CostingCode2 = portalLine.OcrCode2;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUOM", "UomCode eq '" + portalLine.UomCode + "'");

                    OUOM mOUOM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUOM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }


                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new SalesQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            //try
            //{
            //    //oSalesQuotationService.Add(prDocument);

            //    XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

            //    if (pObject.MappedUdf.Count > 0)
            //    { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

            //    XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
            //    foreach (XmlNode mAuxNode in mAuxNodeList)
            //    {

            //        SalesQuotationSAPLine mAuxLine = mSQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

            //        if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
            //        { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

            //    }

            //    string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
            //    mJsonObjectResult.Code = mResponse;
            //    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

            //    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
            //    {
            //        string AddCmd = string.Empty;
            //        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
            //        XmlDocument mResultXML = new XmlDocument();
            //        AddCmd = @"<?xml version=""1.0"" encoding=""UTF-16""?>" +
            //                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
            //                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
            //                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
            //        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
            //        {
            //            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
            //        }

            //        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
            //        mResultXML.LoadXml(mServerNode.Interact(AddCmd));

            //        SalesQuotationServiceWeb.DocumentParams parametros = new SalesQuotationServiceWeb.DocumentParams();
            //        parametros.DocEntry = Convert.ToInt32(mJsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
            //        parametros.DocEntrySpecified = true;

            //        prDocument = oSalesQuotationService.GetByParams(parametros);
            //        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
            //        {
            //            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
            //            prDocument.AttachmentEntrySpecified = true;
            //        }
            //        oSalesQuotationService.Update(prDocument);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.WriteError("DocumentServiceSL - > AddSalesQuotation: " + ex.Message);
            //    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
            //    mJsonObjectResult.ErrorMsg = ex.Message;
            //}

            try
            {

                dynamic documentToAdd = new ExpandoObject();
                var dictionary = (IDictionary<string, object>)documentToAdd;

                foreach (var property in prDocument.GetType().GetProperties())
                {
                    if (property.Name != "DocumentLines")
                    {
                        string value = (property.GetValue(prDocument) ?? "").ToString();

                        if (value != "0" && value != "0.0" && property.Name != "Letter" && property.Name != "DocObjectCode")
                        {
                            dictionary.Add(property.Name, property.GetValue(prDocument));
                        }

                    }

                }

                List<dynamic> linesToAdd = new List<dynamic>();

                foreach (var item in prDocument.DocumentLines)
                {
                    SalesQuotationSAPLine mAuxLine = mSQIni.Lines.Where(c => c.LineNum == item.LineNum).FirstOrDefault();

                    dynamic documentLine = new ExpandoObject();
                    var dictionaryLine = (IDictionary<string, object>)documentLine;

                    foreach (var property in item.GetType().GetProperties())
                    {

                        if (property.Name != "LineNum")
                        {
                            string value = (property.GetValue(item) ?? "").ToString();

                            if (value != "0" && value != "0.0")
                            {
                                dictionaryLine.Add(property.Name, property.GetValue(item));
                            }
                        }
                        else
                        {
                            dictionaryLine.Add(property.Name, property.GetValue(item));
                        }
                    }

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    {
                        foreach (var itemUdfLine in mAuxLine.MappedUdf)
                        {
                            dictionaryLine.Add(itemUdfLine.UDFName, itemUdfLine.Value);
                        }

                    }

                    linesToAdd.Add(documentLine);

                }


                dictionary.Add("DocumentLines", linesToAdd.ToArray());

                foreach (UDF_ARGNS mUDF in pObject.MappedUdf)
                {
                    dictionary.Add(mUDF.UDFName, mUDF.Value);
                }

                var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

                var result = SLManager.callAPI(pCompanyParam, "Quotations", RestSharp.Method.POST, jsonToAdd);

                var resultObject = JsonConvert.DeserializeObject<ORDR>(result.Content);

                if (resultObject.DocEntry != 0)
                {
                    mJsonObjectResult.Code = resultObject.DocEntry.ToString();
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();



                }
                else
                {
                    var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = errorAPI.error.message.value;
                }

            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        public string UpdateSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            //SalesQuotationServiceWeb.MsgHeader header = new SalesQuotationServiceWeb.MsgHeader();
            //header.ServiceName = SalesQuotationServiceWeb.MsgHeaderServiceName.QuotationsService;
            //header.ServiceNameSpecified = true;
            //header.SessionID = pCompanyParam.DSSessionId;

            //SalesQuotationServiceWeb.QuotationsService oSalesQuotationService = new SalesQuotationServiceWeb.QuotationsService();
            //oSalesQuotationService.MsgHeaderValue = header;

            //SalesQuotationServiceWeb.DocumentParams parametros = new SalesQuotationServiceWeb.DocumentParams();
            //parametros.DocEntry = pObject.DocEntry;
            //parametros.DocEntrySpecified = true;

            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            SalesQuotationSAP mSQIni = new SalesQuotationSAP();
            Mapper.CreateMap(typeof(SalesQuotationSAP), typeof(SalesQuotationSAP));
            Mapper.Map(pObject, mSQIni);

            var result2 = SLManager.callAPI(pCompanyParam, $"Quotations({pObject.DocEntry})", RestSharp.Method.GET, "");
            var docSAP = JsonConvert.DeserializeObject<SalesQuotationServiceWeb.Document>(result2.Content, settings);

            SalesQuotationServiceWeb.Document prDocument = docSAP;


            //SalesQuotationServiceWeb.Document prDocument = oSalesQuotationService.GetByParams(parametros);

            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = pObject.CardName;
            prDocument.Project = pObject.Project;

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            //prDocument.DocTotal = (double)pObject.DocTotal;

            SalesQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.SQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.SQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.SQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.SQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.SQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.SQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.SQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.SQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.SQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.SQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.SQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.SQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.SQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.SQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.SQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.SQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.SQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.SQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.SQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.SQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            foreach (SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense expenseLine in prDocument.DocumentAdditionalExpenses)
            {
                Freight portalExpenseLine = pObject.ListFreight.Where(c => c.LineNum == expenseLine.LineNum).FirstOrDefault();
                if (portalExpenseLine != null)
                {
                    switch (portalExpenseLine.DistrbMthd)
                    {
                        case "N":
                            expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "Q":
                            expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "V":
                            expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "W":
                            expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "E":
                            expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "T":
                            expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                    }
                    expenseLine.ExpenseCode = portalExpenseLine.ExpnsCode.Value;
                    expenseLine.ExpenseCodeSpecified = true;
                    expenseLine.DistributionRule = portalExpenseLine.OcrCode;
                    expenseLine.Remarks = portalExpenseLine.Comments;
                    expenseLine.TaxCode = portalExpenseLine.TaxCode;
                    expenseLine.LineTotal = (double)portalExpenseLine.LineTotal;
                    expenseLine.LineTotalSpecified = true;
                    expenseLine.Project = portalExpenseLine.Project;
                }

            }

            foreach (SalesQuotationServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                SalesQuotationSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = portalLine.Dscription;
                    docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    docLine.QuantitySpecified = true;
                    docLine.SupplierCatNum = portalLine.SubCatNum;
                    docLine.AccountCode = portalLine.AcctCode;
                    docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    docLine.PriceSpecified = true;
                    docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    docLine.UnitPriceSpecified = true;
                    docLine.Currency = portalLine.Currency;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.TaxCode = portalLine.TaxCode;
                    docLine.ProjectCode = portalLine.Project;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.CostingCode2 = portalLine.OcrCode2;
                    docLine.FreeText = portalLine.FreeTxt;
                    docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    docLine.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.ShipDate = portalLine.ShipDate.Value;
                        docLine.ShipDateSpecified = true;
                    }
                    if (portalLine.AcctCode != null)
                    {
                        docLine.AccountCode = portalLine.AcctCode;
                    }
                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        docLine.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        docLine.TaxPercentagePerRowSpecified = true;
                    }

                    if (!string.IsNullOrEmpty(portalLine.UomCode))
                    {
                        //OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                        string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUOM", "UomCode eq '" + portalLine.UomCode + "'");

                        OUOM mOUOM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUOM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                        if (mOUOM != null)
                        {
                            docLine.UoMCode = portalLine.UomCode;
                            docLine.UoMEntry = mOUOM.UomEntry;
                            docLine.UoMEntrySpecified = true;
                        }
                    }

                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                }
            }
            SalesQuotationServiceWeb.DocumentDocumentLine[] newLines = new SalesQuotationServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (SalesQuotationSAPLine portalLine in pObject.Lines)
            {
                SalesQuotationServiceWeb.DocumentDocumentLine line = new SalesQuotationServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.CostingCode2 = portalLine.OcrCode2;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }
                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    //OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OUOM", "UomCode eq '" + portalLine.UomCode + "'");

                    OUOM mOUOM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OUOM>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new SalesQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            if (pObject.AttachmentList.Count <= 0)
            {
                prDocument.AttachmentEntrySpecified = false;
            }

            //try
            //{
            //    //oSalesQuotationService.Update(prDocument);


            //    XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

            //    if (pObject.MappedUdf.Count > 0)
            //    { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

            //    XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
            //    foreach (XmlNode mAuxNode in mAuxNodeList)
            //    {
            //        SalesQuotationSAPLine mAuxLine = mSQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

            //        if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
            //        { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

            //    }

            //    mB1WSHandler.ProcessDoc(mXmlDoc);

            //    if (prDocument.AttachmentEntry != 0)
            //    {
            //        string AddCmd = string.Empty;
            //        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
            //        XmlDocument mResultXML = new XmlDocument();
            //        AddCmd = @"<?xml version=""1.0"" ?>" +
            //                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
            //                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
            //                @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
            //        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
            //        {
            //            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
            //        }

            //        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
            //        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
            //    }
            //    else
            //    {
            //        if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
            //        {
            //            string AddCmd = string.Empty;
            //            SBODI_Server.Node mServerNode = new SBODI_Server.Node();
            //            XmlDocument mResultXML = new XmlDocument();
            //            AddCmd = @"<?xml version=""1.0"" ?>" +
            //                    @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
            //                    "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
            //                    @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
            //            foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
            //            {
            //                AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
            //            }

            //            AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
            //            mResultXML.LoadXml(mServerNode.Interact(AddCmd));
            //            prDocument = oSalesQuotationService.GetByParams(parametros);
            //            foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
            //            {
            //                prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
            //                prDocument.AttachmentEntrySpecified = true;
            //            }
            //            oSalesQuotationService.Update(prDocument);
            //        }
            //    }

            //    return "Ok";
            //}
            //catch (Exception ex)
            //{
            //    return "Error:" + ex.Message;
            //}

            var resultAPI = string.Empty;

            dynamic documentToAdd = new ExpandoObject();
            var dictionary = (IDictionary<string, object>)documentToAdd;



            foreach (var property in prDocument.GetType().GetProperties())
            {
                if (property.Name != "DocumentLines")
                {
                    string value = (property.GetValue(prDocument) ?? "").ToString();

                    if (value != "0" && value != "0.0" && property.Name != "Letter" && property.Name != "EDocGenerationType" && property.Name != "StartFrom" && property.Name != "ClosingOption" && property.Name != "ApplyCurrentVATRatesForDownPaymentsToDraw" && property.Name != "DownPaymentStatus")
                    {
                        dictionary.Add(property.Name, property.GetValue(prDocument));
                    }

                }

            }

            List<dynamic> linesToAdd = new List<dynamic>();

            foreach (var item in prDocument.DocumentLines)
            {
                SalesQuotationSAPLine mAuxLine = mSQIni.Lines.Where(c => c.LineNum == item.LineNum).FirstOrDefault();

                dynamic documentLine = new ExpandoObject();
                var dictionaryLine = (IDictionary<string, object>)documentLine;

                foreach (var property in item.GetType().GetProperties())
                {

                    string value = (property.GetValue(item) ?? "").ToString();

                    if (value != "0" && value != "0.0" || property.Name == "LineNum")
                    {
                        dictionaryLine.Add(property.Name, property.GetValue(item));
                    }
                }

                if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                {
                    foreach (var itemUdfLine in mAuxLine.MappedUdf)
                    {
                        dictionaryLine.Add(itemUdfLine.UDFName, itemUdfLine.Value);
                    }

                }

                linesToAdd.Add(documentLine);

            }

            dictionary.Add("DocumentLines", linesToAdd.ToArray());

            foreach (UDF_ARGNS mUDF in pObject.MappedUdf)
            {
                dictionary.Add(mUDF.UDFName, mUDF.Value);
            }

            var jsonToAdd = JsonConvert.SerializeObject(documentToAdd);

            var result = SLManager.callAPI(pCompanyParam, $"Quotations({prDocument.DocEntry})", RestSharp.Method.PATCH, jsonToAdd);

            //var resultObject = JsonConvert.DeserializeObject<ORDR>(result.Content);

            if (result.Content == "")
            {
                resultAPI = "Ok";

            }
            else
            {
                var errorAPI = JsonConvert.DeserializeObject<SLManager.Root>(result.Content);

                resultAPI = "Error:" + errorAPI.error.message.value;

            }

            return resultAPI;
        }

        public JsonObjectResult GetSalesQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (!string.IsNullOrEmpty(pCodeVendor))
            {
                mFilters = mFilters + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
            }


            if (pDocNum != null && pDocNum != 0)
            {
                mFilters = mFilters + " and  DocNum  eq  " + pDocNum.ToString();
            }

            if (pDate != null)
            {
                DateTime mDate = pDate ?? DateTime.Now;

                mFilters = mFilters + " and  DocDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
            }

            if (!string.IsNullOrEmpty(pDocStatus))
            {
                mFilters = mFilters + " and  DocStatus eq '" + pDocStatus + "'";
            }

            if (!string.IsNullOrEmpty(pSECode))
            {
                mFilters = mFilters + " and  SlpCode eq " + pSECode;
            }

            if (!string.IsNullOrEmpty(pOwnerCode))
            {
                mFilters = mFilters + " and  OwnerCode eq " + pOwnerCode;
            }

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleQuotation", mFilters);

            List<SalesQuotationSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesQuotationSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mJsonObjectResult.SalesQuotationSAPList = ListRestu.OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
            mJsonObjectResult.Others.Add("TotalRecords", ListRestu.Count.ToString());
            return mJsonObjectResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
		public List<SalesQuotationSAPLine> GetSalesQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {

                string mUrl = string.Empty;
                string mFilters = "LineStatus eq 'O' and (";
                string mSecFilter = string.Empty;

                foreach (string item in pDocuments)
                {
                    if (string.IsNullOrEmpty(mSecFilter))
                    {
                        mSecFilter = " DocEntry eq " + item;
                    }
                    else
                    {
                        mSecFilter = mSecFilter + " or DocEntry eq " + item;
                    }

                }

                mFilters = mFilters + mSecFilter + ")";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleQuotationLine", mFilters);

                List<SalesQuotationSAPLine> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesQuotationSAPLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return ListRestu;
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderService -> GetSalesQuotationLinesSearch :" + ex.Message);
                return null;
            }
        }

        public SalesQuotationSAP GetAllbySQ(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOQUT = null, List<UDF_ARGNS> pListUDFQUT1 = null)
        {
            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            Mapper.CreateMap<SalesQuotationSAP, OQUT>();
            Mapper.CreateMap<OQUT, SalesQuotationSAP>();

            Mapper.CreateMap<SalesQuotationSAPLine, QUT1>();
            Mapper.CreateMap<QUT1, SalesQuotationSAPLine>();

            Mapper.CreateMap<DocumentAddress, QUT12>();
            Mapper.CreateMap<QUT12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            SalesQuotationSAP mSaleQSAP = new SalesQuotationSAP();

            string mUrl = string.Empty;
            string mFilters = "DocEntry eq " + pDocEntry;

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SaleQuotation", mFilters);
            string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
            OQUT mOQUT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OQUT>>(mResultJson).FirstOrDefault();

            //OQUT mOQUT = this.GetObjectListWithUDF(pListUDFOQUT, mWhere, typeof(OQUT), mConnectionString).Cast<OQUT>().FirstOrDefault();

            if (mOQUT != null)
            {
                mFilters = "TableID eq 'OQUT'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                Newtonsoft.Json.Linq.JArray mJArrayOQUT = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                mOQUT.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOQUT, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOQUT[0]);
                mFilters = "DocEntry eq '" + pDocEntry + "'";

                mSaleQSAP = Mapper.Map<SalesQuotationSAP>(mOQUT);

                mSaleQSAP = FillSQDocumentSAP(pCompanyParam, mSaleQSAP);

                //DocumentLines 
                string mResultJsonLines = RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SaleQuotationLine", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                Newtonsoft.Json.Linq.JArray mJArrayQUT1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                List<QUT1> mLisQUT1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<QUT1>>(mResultJsonLines);
                mSaleQSAP.Lines = Mapper.Map<List<SalesQuotationSAPLine>>(mLisQUT1);

                mSaleQSAP.Lines = mSaleQSAP.Lines.Select(c => { c.GLAccount = mSaleQSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();

                foreach (SalesQuotationSAPLine item in mSaleQSAP.Lines)
                {
                    mFilters = "TableID eq 'QUT1'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFQUT1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayQUT1.Where(c => (int)c["LineNum"] == item.LineNum).FirstOrDefault());
                    mFilters = "DocEntry eq " + item.DocEntry + " and LineNum eq " + item.LineNum + " and GroupNum eq 0";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "QUT2", mFilters);
                    item.Freight = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SalesQuotationSAPLineFreight>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mFilters = "DocEntry eq '" + pDocEntry + "'";
                }

                //DocumentAddress
                List<QUT12> mListQUT12 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<QUT12>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentAddressSQ", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mSaleQSAP.SQAddress = Mapper.Map<DocumentAddress>(mListQUT12.FirstOrDefault());

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (mOQUT.OwnerCode ?? 0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                mSaleQSAP.ListFreight = new List<Freight>();
                mSaleQSAP.ListFreight.Add(new Freight());
                mSaleQSAP.ListFreight.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Freight>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));

                //Attachments
                if (mSaleQSAP.AtcEntry != null)
                {
                    List<ATC1> mATC1List = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ATC1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ATC1", "AbsEntry eq " + mSaleQSAP.AtcEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mSaleQSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);
                }
            }
            else
            {
                mSaleQSAP = FillSQDocumentSAP(pCompanyParam, mSaleQSAP);

                //CompanyAddress
                List<ADM1> mListADM1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ADM1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CompanyAddress", "Code eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mSaleQSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                if (mOHEM != null)
                { mSaleQSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM); }

                mSaleQSAP.ListFreight = new List<Freight>();
                mSaleQSAP.ListFreight.Add(new Freight());
                mSaleQSAP.ListFreight.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<Freight>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
            }

            return mSaleQSAP;
        }

        public SalesQuotationSAP FillSQDocumentSAP(CompanyConn pCompanyParam, SalesQuotationSAP pSaleQSAP)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();


            //Buyer
            List<OSLP> mLisOSLP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSLP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Currency
            List<OCRN> mListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentMethod
            List<OCTG> mListOCTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMethod", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentTerm
            // List<OPYM> mListOPYM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPYM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentTerm", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //ShippingType
            List<OSHP> mListOSHP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSHP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Taxt
            List<OVTG> mListOVTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OVTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Taxt", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pSaleQSAP.CardCode))
            {
                retCb.ListPaymentMethod.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMethod>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CRD2", "CardCode eq '" + pSaleQSAP.CardCode + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
            }
            // retCb.ListPaymentMethod = mListOPYM.Select(c => new PaymentMethod { PayMethCod = c.PayMethCod, Descript = c.Descript }).ToList(); 

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pSaleQSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OACT>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", "FormatCode ne 'Null'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleQSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CINF>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentSettings", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleQSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWHS>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleQSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pSaleQSAP.LocalCurrency = GetCurrency(true, pCompanyParam);
            pSaleQSAP.SystemCurrency = GetCurrency(false, pCompanyParam);

            //DistrbRule
            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pSaleQSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pSaleQSAP;
        }

        #endregion

        #region Purchase Order

        public List<Freight> GetPurchaseOrderFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            Mapper.CreateMap<OOCR, DistrRuleSAP>();
            Mapper.CreateMap<DistrRuleSAP, OOCR>();

            Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
            Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

            Mapper.CreateMap<OPRJ, ProjectSAP>();
            Mapper.CreateMap<ProjectSAP, OPRJ>();

            List<POR3> mListPOR3 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<POR3>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "POR3", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OEXD> mListOEXD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OEXD>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<Freight> mListFreight = (from mPOR3 in mListPOR3
                                          join mOEXD in mListOEXD on mPOR3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mPOR3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mPOR3.DocEntry,
                                              ExpnsCode = mPOR3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mPOR3.LineTotal,
                                              TotalFrgn = mPOR3.TotalFrgn,
                                              Comments = mPOR3.Comments,
                                              ObjType = mPOR3.ObjType,
                                              DistrbMthd = mPOR3.DistrbMthd,
                                              VatSum = mPOR3.VatSum,
                                              TaxCode = mPOR3.TaxCode,
                                              LineNum = mPOR3.LineNum,
                                              Status = mPOR3.Status,
                                              OcrCode = mPOR3.OcrCode,
                                              TaxDistMtd = mPOR3.TaxDistMtd
                                          }).ToList();


            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }

            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OSTC> mListOSTC = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSTC>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesTaxCodes", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OPRJ> mListOPRJ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPRJ>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Project", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;
        }

        public PurchaseOrderSAP GetAllbyPO(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPOR = null, List<UDF_ARGNS> pListUDFPOR1 = null)
        {
            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            Mapper.CreateMap<PurchaseOrderSAP, OPOR>();
            Mapper.CreateMap<OPOR, PurchaseOrderSAP>();

            Mapper.CreateMap<PurchaseOrderSAPLine, POR1>();
            Mapper.CreateMap<POR1, PurchaseOrderSAPLine>();

            Mapper.CreateMap<DocumentAddress, POR12>();
            Mapper.CreateMap<POR12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            PurchaseOrderSAP mPurchaseOrderSAP = new PurchaseOrderSAP();

            string mUrl = string.Empty;
            string mFilters = "DocEntry eq " + pDocEntry;

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrder", mFilters);

            string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
            OPOR mOPOR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPOR>>(mResultJson).FirstOrDefault();

            if (mOPOR != null)
            {
                mFilters = "TableID eq 'OPOR'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                Newtonsoft.Json.Linq.JArray mJArrayOPOR = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                mOPOR.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOPOR, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOPOR[0]);
                mFilters = "DocEntry eq '" + pDocEntry + "'";

                mPurchaseOrderSAP = Mapper.Map<PurchaseOrderSAP>(mOPOR);

                mPurchaseOrderSAP = FillDocumentSAP(pCompanyParam, mPurchaseOrderSAP);

                //DocumentLines 
                string mResultJsonLines = RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrderLine", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                Newtonsoft.Json.Linq.JArray mJArrayPOR1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                List<POR1> mLisPOR1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<POR1>>(mResultJsonLines);
                mPurchaseOrderSAP.Lines = Mapper.Map<List<PurchaseOrderSAPLine>>(mLisPOR1);
                mPurchaseOrderSAP.Lines = mPurchaseOrderSAP.Lines.Select(c => { c.GLAccount = mPurchaseOrderSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();

                foreach (PurchaseOrderSAPLine item in mPurchaseOrderSAP.Lines)
                {
                    mFilters = "TableID eq 'POR1'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFPOR1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayPOR1.Where(c => (int)c["LineNum"] == item.LineNum).FirstOrDefault());
                    mFilters = "DocEntry eq '" + pDocEntry + "'";
                }
                //DocumentAddress
                List<POR12> mListPOR12 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<POR12>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentAddressPO", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mPurchaseOrderSAP.POAddress = Mapper.Map<DocumentAddress>(mListPOR12.FirstOrDefault());

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (mOPOR.OwnerCode ?? 0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                //Attachments
                if (mPurchaseOrderSAP.AtcEntry != null)
                {
                    List<ATC1> mATC1List = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ATC1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ATC1", "AbsEntry eq " + mPurchaseOrderSAP.AtcEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mPurchaseOrderSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);
                }
            }
            else
            {
                mPurchaseOrderSAP = FillDocumentSAP(pCompanyParam, mPurchaseOrderSAP);

                //CompanyAddress
                List<ADM1> mListADM1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ADM1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CompanyAddress", "Code eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mPurchaseOrderSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                if (mOHEM != null)
                { mPurchaseOrderSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM); }

            }

            return mPurchaseOrderSAP;

        }

        public JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (!string.IsNullOrEmpty(pCodeVendor))
            {
                mFilters = mFilters + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
            }


            if (pDocNum != null && pDocNum != 0)
            {
                mFilters = mFilters + " and  DocNum  eq  " + pDocNum.ToString();
            }

            if (pDate != null)
            {
                DateTime mDate = pDate ?? DateTime.Now;

                mFilters = mFilters + " and  DocDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
            }

            if (!string.IsNullOrEmpty(pDocStatus))
            {
                mFilters = mFilters + " and  DocStatus eq '" + pDocStatus + "'";
            }

            if (!string.IsNullOrEmpty(pSECode))
            {
                mFilters = mFilters + " and  SlpCode eq " + pSECode;
            }

            if (!string.IsNullOrEmpty(pOwnerCode))
            {
                mFilters = mFilters + " and  OwnerCode eq " + pOwnerCode;
            }

            if ((pCompanyParam.IdBranchSelect ?? 0) > 0)
            {
                mFilters = mFilters + " and  BPLId eq " + pCompanyParam.IdBranchSelect;
            }

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseOrder", mFilters);

            List<PurchaseOrderSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            mJsonObjectResult.PurchaseOrderSAPList = ListRestu.OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
            mJsonObjectResult.Others.Add("TotalRecords", ListRestu.Count.ToString());
            return mJsonObjectResult;
        }

        public string AddPurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseOrderServiceWeb.MsgHeader header = new PurchaseOrderServiceWeb.MsgHeader();
            header.ServiceName = PurchaseOrderServiceWeb.MsgHeaderServiceName.PurchaseOrdersService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseOrderServiceWeb.PurchaseOrdersService oPurchaseOrderService = new PurchaseOrderServiceWeb.PurchaseOrdersService();
            oPurchaseOrderService.MsgHeaderValue = header;

            PurchaseOrderSAP mOPORIni = new PurchaseOrderSAP();
            Mapper.CreateMap(typeof(PurchaseOrderSAP), typeof(PurchaseOrderSAP));
            Mapper.Map(pObject, mOPORIni);

            PurchaseOrderServiceWeb.Document prDocument = new PurchaseOrderServiceWeb.Document();
            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;

            if (pCompanyParam.IdBranchSelect != null && pCompanyParam.IdBranchSelect > 0)
            {
                prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
                prDocument.BPL_IDAssignedToInvoiceSpecified = true;
            }

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseOrderServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.POAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.POAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.POAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.POAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.POAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.POAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.POAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.POAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.POAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.POAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.POAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.POAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.POAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.POAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.POAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.POAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.POAddress.CountyB;
            oDocumentAddress.BillToState = pObject.POAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.POAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.POAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense();
                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }
                oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode.Value;
                oDocumentExpenses.ExpenseCodeSpecified = true;
                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                oDocumentExpenses.Remarks = expenseLine.Comments;
                oDocumentExpenses.TaxCode = expenseLine.TaxCode;
                if (expenseLine.LineTotal != null)
                {
                    oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                    oDocumentExpenses.LineTotalSpecified = true;
                }
                oDocumentExpenses.Project = expenseLine.Project;
                //if (expenseLine.BaseAbsEnt != null)
                //{
                //    oDocumentExpenses.BaseDocType = (long)expenseLine.BaseType;
                //    oDocumentExpenses.BaseDocTypeSpecified = true;
                //    oDocumentExpenses.BaseDocLine = (long)expenseLine.BaseLnNum;
                //    oDocumentExpenses.BaseDocLineSpecified = true;
                //    oDocumentExpenses.BaseDocEntry = (long)expenseLine.BaseAbsEnt;
                //    oDocumentExpenses.BaseDocEntrySpecified = true;
                //}

                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            PurchaseOrderServiceWeb.DocumentDocumentLine[] newLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;
            foreach (PurchaseOrderSAPLine portalLine in pObject.Lines)
            {
                PurchaseOrderServiceWeb.DocumentDocumentLine line = new PurchaseOrderServiceWeb.DocumentDocumentLine();

                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.BaseEntry != null)
                {
                    line.BaseEntry = (long)portalLine.BaseEntry;
                    line.BaseEntrySpecified = true;
                }
                if (portalLine.BaseLine != null)
                {
                    line.BaseLine = (long)portalLine.BaseLine;
                    line.BaseLineSpecified = true;
                }
                if (portalLine.BaseType != null)
                {
                    line.BaseType = (long)portalLine.BaseType;
                    line.BaseTypeSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }
                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }
                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseOrderSAPLine mAuxLine = mOPORIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                mJsonObjectResult.Code = mResponse;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    PurchaseOrderServiceWeb.DocumentParams parametros = new PurchaseOrderServiceWeb.DocumentParams();
                    parametros.DocEntry = Convert.ToInt32(mJsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
                    parametros.DocEntrySpecified = true;

                    prDocument = oPurchaseOrderService.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        prDocument.AttachmentEntrySpecified = true;
                    }
                    oPurchaseOrderService.Update(prDocument);
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseOrderServiceWeb.MsgHeader header = new PurchaseOrderServiceWeb.MsgHeader();
            header.ServiceName = PurchaseOrderServiceWeb.MsgHeaderServiceName.PurchaseOrdersService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseOrderServiceWeb.PurchaseOrdersService oPurchaseOrderService = new PurchaseOrderServiceWeb.PurchaseOrdersService();
            oPurchaseOrderService.MsgHeaderValue = header;

            PurchaseOrderServiceWeb.DocumentParams parametros = new PurchaseOrderServiceWeb.DocumentParams();
            parametros.DocEntry = pObject.DocEntry;
            parametros.DocEntrySpecified = true;

            PurchaseOrderServiceWeb.Document prDocument = oPurchaseOrderService.GetByParams(parametros);

            PurchaseOrderSAP mOPORIni = new PurchaseOrderSAP();
            Mapper.CreateMap(typeof(PurchaseOrderSAP), typeof(PurchaseOrderSAP));
            Mapper.Map(pObject, mOPORIni);

            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = pObject.CardName;
            prDocument.Project = pObject.Project;

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseOrderServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.POAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.POAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.POAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.POAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.POAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.POAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.POAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.POAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.POAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.POAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.POAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.POAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.POAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.POAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.POAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.POAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.POAddress.CountyB;
            oDocumentAddress.BillToState = pObject.POAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.POAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.POAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            foreach (PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense expenseLine in prDocument.DocumentAdditionalExpenses)
            {
                Freight portalExpenseLine = pObject.ListFreight.Where(c => c.LineNum == expenseLine.LineNum).FirstOrDefault();
                if (portalExpenseLine != null)
                {
                    switch (portalExpenseLine.DistrbMthd)
                    {
                        case "N":
                            expenseLine.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "Q":
                            expenseLine.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "V":
                            expenseLine.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "W":
                            expenseLine.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "E":
                            expenseLine.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "T":
                            expenseLine.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                    }
                    expenseLine.ExpenseCode = portalExpenseLine.ExpnsCode.Value;
                    expenseLine.ExpenseCodeSpecified = true;
                    expenseLine.DistributionRule = portalExpenseLine.OcrCode;
                    expenseLine.Remarks = portalExpenseLine.Comments;
                    expenseLine.TaxCode = portalExpenseLine.TaxCode;
                    expenseLine.LineTotal = (double)portalExpenseLine.LineTotal;
                    expenseLine.LineTotalSpecified = true;
                    expenseLine.Project = portalExpenseLine.Project;
                }

            }

            foreach (PurchaseOrderServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                PurchaseOrderSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = portalLine.Dscription;
                    docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    docLine.QuantitySpecified = true;
                    docLine.SupplierCatNum = portalLine.SubCatNum;
                    docLine.AccountCode = portalLine.AcctCode;
                    docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    docLine.PriceSpecified = true;
                    docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    docLine.UnitPriceSpecified = true;
                    docLine.Currency = portalLine.Currency;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.TaxCode = portalLine.TaxCode;
                    docLine.ProjectCode = portalLine.Project;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.FreeText = portalLine.FreeTxt;
                    docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    docLine.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.ShipDate = portalLine.ShipDate.Value;
                        docLine.ShipDateSpecified = true;
                    }
                    if (portalLine.AcctCode != null)
                    {
                        //docLine.AccountCode = portalLine.AcctCode;
                    }
                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        docLine.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        docLine.TaxPercentagePerRowSpecified = true;
                    }
                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                }
            }
            PurchaseOrderServiceWeb.DocumentDocumentLine[] newLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (PurchaseOrderSAPLine portalLine in pObject.Lines)
            {
                PurchaseOrderServiceWeb.DocumentDocumentLine line = new PurchaseOrderServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    //line.AccountCode = portalLine.AcctCode;
                }
                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }
                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            if (pObject.AttachmentList.Count <= 0)
            {
                prDocument.AttachmentEntrySpecified = false;
            }

            try
            {
                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseOrderSAPLine mAuxLine = mOPORIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                if (prDocument.AttachmentEntry != 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                }
                else
                {
                    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                    {
                        string AddCmd = string.Empty;
                        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                        XmlDocument mResultXML = new XmlDocument();
                        AddCmd = @"<?xml version=""1.0"" ?>" +
                                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                        {
                            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                        }

                        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                        prDocument = oPurchaseOrderService.GetByParams(parametros);
                        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                        {
                            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                            prDocument.AttachmentEntrySpecified = true;
                        }
                        oPurchaseOrderService.Update(prDocument);
                    }
                }

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public PurchaseOrderSAP FillDocumentSAP(CompanyConn pCompanyParam, PurchaseOrderSAP pPurchaseOrderSAP)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();

            //Buyer
            List<OSLP> mLisOSLP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSLP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Currency
            List<OCRN> mListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentMethod
            List<OCTG> mListOCTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMethod", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentTerm
            //List<OPYM> mListOPYM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPYM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentTerm", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //ShippingType
            List<OSHP> mListOSHP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSHP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Taxt
            List<OVTG> mListOVTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OVTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Taxt", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pPurchaseOrderSAP.CardCode))
            {
                retCb.ListPaymentMethod.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMethod>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CRD2", "CardCode eq '" + pPurchaseOrderSAP.CardCode + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pPurchaseOrderSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OACT>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", "FormatCode ne 'Null'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPurchaseOrderSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CINF>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentSettings", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPurchaseOrderSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWHS>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPurchaseOrderSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);

            pPurchaseOrderSAP.LocalCurrency = GetCurrency(true, pCompanyParam);
            pPurchaseOrderSAP.SystemCurrency = GetCurrency(false, pCompanyParam);

            //DistrbRule
            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPurchaseOrderSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);


            return pPurchaseOrderSAP;
        }
        #endregion

        #region Purchase Quotation

        public List<Freight> GetPurchaseQuotationFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            Mapper.CreateMap<OOCR, DistrRuleSAP>();
            Mapper.CreateMap<DistrRuleSAP, OOCR>();

            Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
            Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

            Mapper.CreateMap<OPRJ, ProjectSAP>();
            Mapper.CreateMap<ProjectSAP, OPRJ>();

            List<PQT3> mListPQT3 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PQT3>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "POR3", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OEXD> mListOEXD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OEXD>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<Freight> mListFreight = (from mPQT3 in mListPQT3
                                          join mOEXD in mListOEXD on mPQT3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mPQT3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mPQT3.DocEntry,
                                              ExpnsCode = mPQT3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mPQT3.LineTotal,
                                              TotalFrgn = mPQT3.TotalFrgn,
                                              Comments = mPQT3.Comments,
                                              ObjType = mPQT3.ObjType,
                                              DistrbMthd = mPQT3.DistrbMthd,
                                              VatSum = mPQT3.VatSum,
                                              TaxCode = mPQT3.TaxCode,
                                              LineNum = mPQT3.LineNum,
                                              Status = mPQT3.Status,
                                              OcrCode = mPQT3.OcrCode,
                                              TaxDistMtd = mPQT3.TaxDistMtd
                                          }).ToList();


            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }

            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OSTC> mListOSTC = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSTC>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesTaxCodes", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<OPRJ> mListOPRJ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPRJ>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Project", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;
        }

        public JsonObjectResult GetPurchaseQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (!string.IsNullOrEmpty(pCodeVendor))
            {
                mFilters = mFilters + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
            }


            if (pDocNum != null && pDocNum != 0)
            {
                mFilters = mFilters + " and  DocNum  eq  " + pDocNum.ToString();
            }

            if (pDate != null)
            {
                DateTime mDate = pDate ?? DateTime.Now;

                mFilters = mFilters + " and  DocDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
            }

            if (!string.IsNullOrEmpty(pDocStatus))
            {
                mFilters = mFilters + " and  DocStatus eq '" + pDocStatus + "'";
            }

            if (!string.IsNullOrEmpty(pSECode))
            {
                mFilters = mFilters + " and  SlpCode eq " + pSECode;
            }

            if (!string.IsNullOrEmpty(pOwnerCode))
            {
                mFilters = mFilters + " and  OwnerCode eq " + pOwnerCode;
            }

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseQuotation", mFilters);

            List<PurchaseQuotationSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseQuotationSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mJsonObjectResult.PurchaseQuotationSAPList = ListRestu.OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
            mJsonObjectResult.Others.Add("TotalRecords", ListRestu.Count.ToString());
            return mJsonObjectResult;
        }

        public List<PurchaseQuotationSAPLine> GetPurchaseQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "LineStatus eq 'O' and (";
                string mSecFilter = string.Empty;

                foreach (string item in pDocuments)
                {
                    if (string.IsNullOrEmpty(mSecFilter))
                    {
                        mSecFilter = " DocEntry eq " + item;
                    }
                    else
                    {
                        mSecFilter = mSecFilter + " or DocEntry eq " + item;
                    }

                }

                mFilters = mFilters + mSecFilter + ")";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseQuotationLine", mFilters);

                List<PurchaseQuotationSAPLine> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseQuotationSAPLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return ListRestu;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseQuotationLinesSearch :" + ex.Message);
                return null;
            }

        }

        public string AddPurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseQuotationServiceWeb.MsgHeader header = new PurchaseQuotationServiceWeb.MsgHeader();
            header.ServiceName = PurchaseQuotationServiceWeb.MsgHeaderServiceName.PurchaseQuotationsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseQuotationServiceWeb.PurchaseQuotationsService oPurchaseQuotationService = new PurchaseQuotationServiceWeb.PurchaseQuotationsService();
            oPurchaseQuotationService.MsgHeaderValue = header;

            PurchaseQuotationSAP mPQIni = new PurchaseQuotationSAP();
            Mapper.CreateMap(typeof(PurchaseQuotationSAP), typeof(PurchaseQuotationSAP));
            Mapper.Map(pObject, mPQIni);

            PurchaseQuotationServiceWeb.Document prDocument = new PurchaseQuotationServiceWeb.Document();
            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;
            if (pCompanyParam.IdBranchSelect != null && pCompanyParam.IdBranchSelect > 0)
            {
                prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
                prDocument.BPL_IDAssignedToInvoiceSpecified = true;
            }
            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.PQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.PQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.PQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.PQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.PQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.PQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.PQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.PQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.PQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.PQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.PQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.PQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.PQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.PQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.PQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.PQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.PQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.PQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.PQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.PQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense();
                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }
                oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode.Value;
                oDocumentExpenses.ExpenseCodeSpecified = true;
                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                oDocumentExpenses.Remarks = expenseLine.Comments;
                oDocumentExpenses.TaxCode = expenseLine.TaxCode;
                oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                oDocumentExpenses.LineTotalSpecified = true;
                oDocumentExpenses.Project = expenseLine.Project;

                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            PurchaseQuotationServiceWeb.DocumentDocumentLine[] newLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;
            foreach (PurchaseQuotationSAPLine portalLine in pObject.Lines)
            {
                PurchaseQuotationServiceWeb.DocumentDocumentLine line = new PurchaseQuotationServiceWeb.DocumentDocumentLine();

                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.PQTReqDate.HasValue && portalLine.PQTReqDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.RequiredDate = portalLine.PQTReqDate.Value;
                    line.RequiredDateSpecified = true;
                }
                if (portalLine.PQTReqQty.HasValue)
                {
                    line.RequiredQuantity = (double)portalLine.PQTReqQty.Value;
                    line.RequiredQuantitySpecified = true;
                }
                if (portalLine.BaseEntry != null)
                {
                    line.BaseEntry = (long)portalLine.BaseEntry;
                    line.BaseEntrySpecified = true;
                }
                if (portalLine.BaseLine != null)
                {
                    line.BaseLine = (long)portalLine.BaseLine;
                    line.BaseLineSpecified = true;
                }
                if (portalLine.BaseType != null)
                {
                    line.BaseType = (long)portalLine.BaseType;
                    line.BaseTypeSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }
                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                //oPurchaseQuotationService.Add(prDocument);

                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseQuotationSAPLine mAuxLine = mPQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                mJsonObjectResult.Code = mResponse;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    PurchaseQuotationServiceWeb.DocumentParams parametros = new PurchaseQuotationServiceWeb.DocumentParams();
                    parametros.DocEntry = Convert.ToInt32(mJsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
                    parametros.DocEntrySpecified = true;

                    prDocument = oPurchaseQuotationService.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        prDocument.AttachmentEntrySpecified = true;
                    }
                    oPurchaseQuotationService.Update(prDocument);
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseQuotationServiceWeb.MsgHeader header = new PurchaseQuotationServiceWeb.MsgHeader();
            header.ServiceName = PurchaseQuotationServiceWeb.MsgHeaderServiceName.PurchaseQuotationsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseQuotationServiceWeb.PurchaseQuotationsService oPurchaseQuotationService = new PurchaseQuotationServiceWeb.PurchaseQuotationsService();
            oPurchaseQuotationService.MsgHeaderValue = header;

            PurchaseQuotationServiceWeb.DocumentParams parametros = new PurchaseQuotationServiceWeb.DocumentParams();
            parametros.DocEntry = pObject.DocEntry;
            parametros.DocEntrySpecified = true;

            PurchaseQuotationSAP mPQIni = new PurchaseQuotationSAP();
            Mapper.CreateMap(typeof(PurchaseQuotationSAP), typeof(PurchaseQuotationSAP));
            Mapper.Map(pObject, mPQIni);

            PurchaseQuotationServiceWeb.Document prDocument = oPurchaseQuotationService.GetByParams(parametros);

            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = pObject.CardName;
            prDocument.Project = pObject.Project;

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.PQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.PQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.PQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.PQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.PQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.PQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.PQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.PQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.PQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.PQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.PQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.PQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.PQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.PQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.PQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.PQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.PQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.PQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.PQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.PQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            foreach (PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense expenseLine in prDocument.DocumentAdditionalExpenses)
            {
                Freight portalExpenseLine = pObject.ListFreight.Where(c => c.LineNum == expenseLine.LineNum).FirstOrDefault();
                if (portalExpenseLine != null)
                {
                    switch (portalExpenseLine.DistrbMthd)
                    {
                        case "N":
                            expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "Q":
                            expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "V":
                            expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "W":
                            expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "E":
                            expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                        case "T":
                            expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                            expenseLine.DistributionMethodSpecified = true;
                            break;
                    }
                    expenseLine.ExpenseCode = portalExpenseLine.ExpnsCode.Value;
                    expenseLine.ExpenseCodeSpecified = true;
                    expenseLine.DistributionRule = portalExpenseLine.OcrCode;
                    expenseLine.Remarks = portalExpenseLine.Comments;
                    expenseLine.TaxCode = portalExpenseLine.TaxCode;
                    expenseLine.LineTotal = (double)portalExpenseLine.LineTotal;
                    expenseLine.LineTotalSpecified = true;
                    expenseLine.Project = portalExpenseLine.Project;
                }

            }

            foreach (PurchaseQuotationServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                PurchaseQuotationSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = portalLine.Dscription;
                    docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    docLine.QuantitySpecified = true;
                    docLine.SupplierCatNum = portalLine.SubCatNum;
                    docLine.AccountCode = portalLine.AcctCode;
                    docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    docLine.PriceSpecified = true;
                    docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    docLine.UnitPriceSpecified = true;
                    docLine.Currency = portalLine.Currency;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.TaxCode = portalLine.TaxCode;
                    docLine.ProjectCode = portalLine.Project;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.FreeText = portalLine.FreeTxt;
                    docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    docLine.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.ShipDate = portalLine.ShipDate.Value;
                        docLine.ShipDateSpecified = true;
                    }
                    if (portalLine.PQTReqDate.HasValue && portalLine.PQTReqDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.RequiredDate = portalLine.PQTReqDate.Value;
                        docLine.RequiredDateSpecified = true;
                    }
                    if (portalLine.PQTReqQty.HasValue)
                    {
                        docLine.RequiredQuantity = (double)portalLine.PQTReqQty.Value;
                        docLine.RequiredQuantitySpecified = true;
                    }
                    if (portalLine.AcctCode != null)
                    {
                        docLine.AccountCode = portalLine.AcctCode;
                    }
                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        docLine.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        docLine.TaxPercentagePerRowSpecified = true;
                    }
                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                }
            }
            PurchaseQuotationServiceWeb.DocumentDocumentLine[] newLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (PurchaseQuotationSAPLine portalLine in pObject.Lines)
            {
                PurchaseQuotationServiceWeb.DocumentDocumentLine line = new PurchaseQuotationServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.PQTReqDate.HasValue && portalLine.PQTReqDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.RequiredDate = portalLine.PQTReqDate.Value;
                    line.RequiredDateSpecified = true;
                }
                if (portalLine.PQTReqQty.HasValue)
                {
                    line.RequiredQuantity = (double)portalLine.PQTReqQty.Value;
                    line.RequiredQuantitySpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }
                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }
                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            if (pObject.AttachmentList.Count <= 0)
            {
                prDocument.AttachmentEntrySpecified = false;
            }

            try
            {
                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseQuotationSAPLine mAuxLine = mPQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                if (prDocument.AttachmentEntry != 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                }
                else
                {
                    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                    {
                        string AddCmd = string.Empty;
                        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                        XmlDocument mResultXML = new XmlDocument();
                        AddCmd = @"<?xml version=""1.0"" ?>" +
                                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                        {
                            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                        }

                        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                        prDocument = oPurchaseQuotationService.GetByParams(parametros);
                        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                        {
                            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                            prDocument.AttachmentEntrySpecified = true;
                        }
                        oPurchaseQuotationService.Update(prDocument);
                    }
                }

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public PurchaseQuotationSAP GetAllbyPQ(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPQT = null, List<UDF_ARGNS> pListUDFPQT1 = null)
        {
            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            Mapper.CreateMap<PurchaseQuotationSAP, OPQT>();
            Mapper.CreateMap<OPQT, PurchaseQuotationSAP>();

            Mapper.CreateMap<PurchaseQuotationSAPLine, PQT1>();
            Mapper.CreateMap<PQT1, PurchaseQuotationSAPLine>();

            Mapper.CreateMap<DocumentAddress, PQT12>();
            Mapper.CreateMap<PQT12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            PurchaseQuotationSAP mPQSAP = new PurchaseQuotationSAP();

            string mUrl = string.Empty;
            string mFilters = "DocEntry eq " + pDocEntry;

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseQuotation", mFilters);
            string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
            OPQT mOPQT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPQT>>(mResultJson).FirstOrDefault();

            if (mOPQT != null)
            {
                mFilters = "TableID eq 'OPQT'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                Newtonsoft.Json.Linq.JArray mJArrayOPQT = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                mOPQT.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOPQT, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOPQT[0]);
                mFilters = "DocEntry eq '" + pDocEntry + "'";

                mPQSAP = Mapper.Map<PurchaseQuotationSAP>(mOPQT);

                mPQSAP = FillPQDocumentSAP(pCompanyParam, mPQSAP);

                //DocumentLines 
                string mResultJsonLines = RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseQuotationLine", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                Newtonsoft.Json.Linq.JArray mJArrayPQT1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                List<PQT1> mLisPQT1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PQT1>>(mResultJsonLines);
                mPQSAP.Lines = Mapper.Map<List<PurchaseQuotationSAPLine>>(mLisPQT1);
                mPQSAP.Lines = mPQSAP.Lines.Select(c => { c.GLAccount = mPQSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();

                foreach (PurchaseQuotationSAPLine item in mPQSAP.Lines)
                {
                    mFilters = "TableID eq 'PQT1'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFPQT1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayPQT1.Where(c => (int)c["LineNum"] == item.LineNum).FirstOrDefault());
                    mFilters = "DocEntry eq '" + pDocEntry + "'";
                }

                //DocumentAddress
                List<PQT12> mListPQT12 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PQT12>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentAddressPQ", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mPQSAP.PQAddress = Mapper.Map<DocumentAddress>(mListPQT12.FirstOrDefault());

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (mOPQT.OwnerCode ?? 0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();

                //Attachments
                if (mPQSAP.AtcEntry != null)
                {
                    List<ATC1> mATC1List = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ATC1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ATC1", "AbsEntry eq " + mPQSAP.AtcEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    mPQSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);
                }
            }
            else
            {
                mPQSAP = FillPQDocumentSAP(pCompanyParam, mPQSAP);

                //CompanyAddress
                List<ADM1> mListADM1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ADM1>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CompanyAddress", "Code eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mPQSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                if (mOHEM != null)
                { mPQSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM); }

            }

            return mPQSAP;
        }

        public PurchaseQuotationSAP FillPQDocumentSAP(CompanyConn pCompanyParam, PurchaseQuotationSAP pPQSAP)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();

            //Buyer
            List<OSLP> mLisOSLP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSLP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Currency
            List<OCRN> mListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentMethod
            List<OCTG> mListOCTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMethod", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentTerm
            //List<OPYM> mListOPYM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPYM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentTerm", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //ShippingType
            List<OSHP> mListOSHP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSHP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Taxt
            List<OVTG> mListOVTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OVTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Taxt", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pPQSAP.CardCode))
            {
                retCb.ListPaymentMethod.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMethod>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CRD2", "CardCode eq '" + pPQSAP.CardCode + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pPQSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OACT>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", "FormatCode ne 'Null'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPQSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CINF>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentSettings", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPQSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWHS>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPQSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);

            pPQSAP.LocalCurrency = GetCurrency(true, pCompanyParam);
            pPQSAP.SystemCurrency = GetCurrency(false, pCompanyParam);

            //DistrbRule
            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPQSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pPQSAP;
        }

        #endregion

        #region Purchase Invoice

        public JsonObjectResult GetPurchaseInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (!string.IsNullOrEmpty(pCodeVendor))
            {
                mFilters = mFilters + " and substringof(tolower('" + pCodeVendor + "'), tolower(CardCode))";
            }


            if (pDocNum != null && pDocNum != 0)
            {
                mFilters = mFilters + " and  DocNum  eq  " + pDocNum.ToString();
            }

            if (pDate != null)
            {
                DateTime mDate = pDate ?? DateTime.Now;

                mFilters = mFilters + " and  DocDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
            }

            if (!string.IsNullOrEmpty(pDocStatus))
            {
                mFilters = mFilters + " and  DocStatus eq '" + pDocStatus + "'";
            }

            if (!string.IsNullOrEmpty(pSECode))
            {
                mFilters = mFilters + " and  SlpCode eq " + pSECode;
            }

            if (!string.IsNullOrEmpty(pOwnerCode))
            {
                mFilters = mFilters + " and  OwnerCode eq " + pOwnerCode;
            }

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseInvoice", mFilters);

            List<PurchaseInvoiceSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseInvoiceSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mJsonObjectResult.PurchaseInvoiceSAPList = ListRestu.OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
            mJsonObjectResult.Others.Add("TotalRecords", ListRestu.Count.ToString());

            return mJsonObjectResult;
        }

        public PurchaseInvoiceSAP GetAllbyPI(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPCH = null, List<UDF_ARGNS> pListUDFPCH1 = null)
        {
            Mapper.CreateMap<PurchaseInvoiceSAP, OPCH>();
            Mapper.CreateMap<OPCH, PurchaseInvoiceSAP>();

            Mapper.CreateMap<PurchaseInvoiceLineSAP, PCH1>();
            Mapper.CreateMap<PCH1, PurchaseInvoiceLineSAP>();

            Mapper.CreateMap<DocumentAddress, PCH12>();
            Mapper.CreateMap<PCH12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            PurchaseInvoiceSAP mPISAP = new PurchaseInvoiceSAP();

            string mUrl = string.Empty;
            string mFilters = "DocEntry eq " + pDocEntry;

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseInvoice", mFilters);
            string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
            OPCH mOPCH = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPCH>>(mResultJson).FirstOrDefault();

            if (mOPCH != null)
            {
                mFilters = "TableID eq 'OPCH'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                Newtonsoft.Json.Linq.JArray mJArrayOPCH = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                mOPCH.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOPCH, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOPCH[0]);
                mFilters = "DocEntry eq '" + pDocEntry + "'";

                mPISAP = Mapper.Map<PurchaseInvoiceSAP>(mOPCH);

                mPISAP = FillPIDocumentSAP(pCompanyParam, mPISAP);

                //DocumentLines 
                string mResultJsonLines = RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseInvoiceLine", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                Newtonsoft.Json.Linq.JArray mJArrayPCH1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                List<PCH1> mLisPCH1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PCH1>>(mResultJsonLines);
                mPISAP.Lines = Mapper.Map<List<PurchaseInvoiceLineSAP>>(mLisPCH1);

                foreach (PurchaseInvoiceLineSAP item in mPISAP.Lines)
                {
                    mFilters = "TableID eq 'PCH1'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFPCH1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayPCH1.Where(c => (int)c["LineNum"] == item.LineNum).FirstOrDefault());
                    mFilters = "DocEntry eq '" + pDocEntry + "'";
                }

                //DocumentAddress
                List<PCH12> mListPCH12 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PCH12>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentAddressPI", "DocEntry eq " + pDocEntry), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mPISAP.PIAddress = Mapper.Map<DocumentAddress>(mListPCH12.FirstOrDefault());

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (mPISAP.OwnerCode ?? 0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
            }
            else
            {
                mPISAP = FillPIDocumentSAP(pCompanyParam, mPISAP);

                //Employees
                OHEM mOHEM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OHEM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Employee", "empID eq " + (0)), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).FirstOrDefault();
                if (mOHEM != null)
                { mPISAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM); }
            }

            return mPISAP;
        }

        public PurchaseInvoiceSAP FillPIDocumentSAP(CompanyConn pCompanyParam, PurchaseInvoiceSAP pPISAP)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();


            //Buyer
            List<OSLP> mLisOSLP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSLP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Currency
            List<OCRN> mListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCRN>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentMethod
            List<OCTG> mListOCTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMethod", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //PaymentTerm
            //List<OPYM> mListOPYM = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPYM>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PaymentTerm", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //ShippingType
            List<OSHP> mListOSHP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSHP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "ShippingType", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            //Taxt
            List<OVTG> mListOVTG = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OVTG>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Taxt", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pPISAP.CardCode))
            {
                retCb.ListPaymentMethod.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMethod>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "CRD2", "CardCode eq '" + pPISAP.CardCode + "'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pPISAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OACT>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", "FormatCode ne 'Null'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPISAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //Warehouse
            List<OWHS> mListOWHS = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OWHS>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPISAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);

            pPISAP.LocalCurrency = GetCurrency(true, pCompanyParam);
            pPISAP.SystemCurrency = GetCurrency(false, pCompanyParam);

            //DistrbRule
            List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            pPISAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pPISAP;
        }

        #endregion

        #region Purchase Request

        public JsonObjectResult GetPurchaseRequestListSearch(CompanyConn pCompanyParam, string pRequest = "", DateTime? pDate = null, int? pDocNum = null, int? pDepartment = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            string mUrl = string.Empty;
            string mFilters = "1 eq 1 ";
            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            if (pDocNum != null && pDocNum != 0)
            {
                mFilters = mFilters + " and  DocNum  eq  " + pDocNum.ToString();
            }

            if (pDate != null)
            {
                DateTime mDate = pDate ?? DateTime.Now;

                mFilters = mFilters + " and  DocDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
            }

            if (!string.IsNullOrEmpty(pDocStatus))
            {
                mFilters = mFilters + " and  DocStatus eq '" + pDocStatus + "'";
            }

            if (!string.IsNullOrEmpty(pSECode))
            {
                mFilters = mFilters + " and  SlpCode eq " + pSECode;
            }

            if (!string.IsNullOrEmpty(pOwnerCode))
            {
                mFilters = mFilters + " and  OwnerCode eq " + pOwnerCode;
            }

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseRequest", mFilters);

            List<PurchaseRequestSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseRequestSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mJsonObjectResult.PurchaseRequestSAPList = ListRestu.OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
            mJsonObjectResult.Others.Add("TotalRecords", ListRestu.Count.ToString());

            return mJsonObjectResult;
        }

        public List<PurchaseRequestLineSAP> GetPurchaseRequestLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {

                string mUrl = string.Empty;
                string mFilters = "LineStatus eq 'O' and (";
                string mSecFilter = string.Empty;

                foreach (string item in pDocuments)
                {
                    if (string.IsNullOrEmpty(mSecFilter))
                    {
                        mSecFilter = " DocEntry eq " + item;
                    }
                    else
                    {
                        mSecFilter = mSecFilter + " or DocEntry eq " + item;
                    }

                }

                mFilters = mFilters + mSecFilter + ")";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseRequestLine", mFilters);

                List<PurchaseRequestLineSAP> ListRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseRequestLineSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return ListRestu;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseRequestLinesSearch :" + ex.Message);
                return null;
            }
        }

        public PurchaseRequestCombo GetPurchaseRequestCombo(CompanyConn pCompanyParam)
        {
            PurchaseRequestCombo mPurchaseRequestCombo = new PurchaseRequestCombo();

            List<CurrencySAP> ListOCRN = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CurrencySAP>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Currency", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<Department> ListOUDP = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Department>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Department", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            List<Branch> ListOUBR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Branch>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Branch", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            mPurchaseRequestCombo.CurrencyList = ListOCRN;
            mPurchaseRequestCombo.DepartmentList = ListOUDP;
            mPurchaseRequestCombo.BranchList = ListOUBR;

            return mPurchaseRequestCombo;
        }

        public List<Freight> GetPurchaseRequestFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            try
            {
                Mapper.CreateMap<OEXD, Freight>();
                Mapper.CreateMap<Freight, OEXD>();

                Mapper.CreateMap<OOCR, DistrRuleSAP>();
                Mapper.CreateMap<DistrRuleSAP, OOCR>();

                Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
                Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

                Mapper.CreateMap<OPRJ, ProjectSAP>();
                Mapper.CreateMap<ProjectSAP, OPRJ>();

                List<PRQ3> mListPRQ3 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PRQ3>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PRQ3", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                List<OEXD> mListOEXD = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OEXD>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Freight", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                List<Freight> mListFreight = (from mPRQ3 in mListPRQ3
                                              join mOEXD in mListOEXD on mPRQ3.ExpnsCode equals mOEXD.ExpnsCode
                                              where
                                              (
                                                  mPRQ3.DocEntry == pDocEntry
                                              )
                                              select new Freight()
                                              {
                                                  DocEntry = mPRQ3.DocEntry,
                                                  ExpnsCode = mPRQ3.ExpnsCode,
                                                  ExpnsName = mOEXD.ExpnsName,
                                                  LineTotal = mPRQ3.LineTotal,
                                                  TotalFrgn = mPRQ3.TotalFrgn,
                                                  Comments = mPRQ3.Comments,
                                                  ObjType = mPRQ3.ObjType,
                                                  DistrbMthd = mPRQ3.DistrbMthd,
                                                  VatSum = mPRQ3.VatSum,
                                                  TaxCode = mPRQ3.TaxCode,
                                                  LineNum = mPRQ3.LineNum,
                                                  Status = mPRQ3.Status,
                                                  OcrCode = mPRQ3.OcrCode,
                                                  TaxDistMtd = mPRQ3.TaxDistMtd
                                              }).ToList();


                if (mListFreight.Count == 0)
                {
                    mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                    mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
                }

                List<OOCR> mListOOCR = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OOCR>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DistributionRule", "Active eq 'Y'"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                List<OSTC> mListOSTC = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OSTC>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "SalesTaxCodes", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                List<OPRJ> mListOPRJ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPRJ>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "Project", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

                return mListFreight;

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseRequestFreights :" + ex.Message);
                return null;
            }
        }

        public PurchaseRequestSAP GetAllbyPR(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOPRQ = null, List<UDF_ARGNS> pListUDFPRQ1 = null)
        {
            PurchaseRequestSAP mPRSAP;

            Mapper.CreateMap<OPRQ, PurchaseRequestSAP>();
            Mapper.CreateMap<PurchaseRequestSAP, OPRQ>();

            Mapper.CreateMap<PRQ1, PurchaseRequestLineSAP>();
            Mapper.CreateMap<PurchaseRequestLineSAP, PRQ1>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();

            string mUrl = string.Empty;
            string mFilters = "DocEntry eq " + pCode;

            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseRequest", mFilters);
            string mResultJson = RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
            OPRQ mOPRQ = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OPRQ>>(mResultJson).FirstOrDefault();

            //GLAccount
            List<OACT> mListOACT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OACT>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "GLAccount", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            List<GLAccountSAP> listAuxGLA = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            if (mOPRQ != null)
            {
                mFilters = "TableID eq 'OPRQ'";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                List<UFD1_SAP> mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                Newtonsoft.Json.Linq.JArray mJArrayOPOR = Newtonsoft.Json.Linq.JArray.Parse(mResultJson);
                mOPRQ.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFOPRQ, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayOPOR[0]);
                mFilters = "DocEntry eq '" + pCode + "'";

                mPRSAP = Mapper.Map<PurchaseRequestSAP>(mOPRQ);

                //DocumentLines 
                string mResultJsonLines = RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "PurchaseRequestLine", "DocEntry eq " + pCode), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword);
                Newtonsoft.Json.Linq.JArray mJArrayPRQ1 = Newtonsoft.Json.Linq.JArray.Parse(mResultJsonLines);
                List<PRQ1> mLisPRQ1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PRQ1>>(mResultJsonLines);
                mPRSAP.Lines = Mapper.Map<List<PurchaseRequestLineSAP>>(mLisPRQ1);
                mPRSAP.Lines = mPRSAP.Lines.Select(c => { c.GLAccount = listAuxGLA.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();

                foreach (PurchaseRequestLineSAP item in mPRSAP.Lines)
                {
                    mFilters = "TableID eq 'PRQ1'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UFD1", mFilters);
                    mListUFD1 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UFD1_SAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                    item.MappedUdf = UDFUtil.GetObjectListWithUDFHana(pListUDFPRQ1, mListUFD1, (Newtonsoft.Json.Linq.JObject)mJArrayPRQ1.Where(c => (int)c["LineNum"] == item.LineNum).FirstOrDefault());
                    mFilters = "DocEntry eq '" + pCode + "'";
                }
            }
            else
            {
                mPRSAP = new PurchaseRequestSAP();
            }

            //DocumentSettings
            List<CINF> mListCINF = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CINF>>(RESTService.GetRequestJson(RESTService.GetURL(pCompanyParam.UrlHana, "DocumentSettings", "1 eq 1"), WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            mPRSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            return mPRSAP;
        }

        public string AddPurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseRequestServiceWeb.MsgHeader header = new PurchaseRequestServiceWeb.MsgHeader();
            header.ServiceName = PurchaseRequestServiceWeb.MsgHeaderServiceName.PurchaseRequestService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseRequestServiceWeb.PurchaseRequestService oPurchaseRequestService = new PurchaseRequestServiceWeb.PurchaseRequestService();
            oPurchaseRequestService.MsgHeaderValue = header;

            PurchaseRequestSAP mPRIni = new PurchaseRequestSAP();
            Mapper.CreateMap(typeof(PurchaseRequestSAP), typeof(PurchaseRequestSAP));
            Mapper.Map(pObject, mPRIni);

            PurchaseRequestServiceWeb.Document prDocument = new PurchaseRequestServiceWeb.Document();
            prDocument.Requester = pObject.Requester;
            prDocument.ReqType = (long)pObject.ReqType;
            prDocument.ReqTypeSpecified = true;
            prDocument.RequesterBranch = (long)pObject.Branch;
            prDocument.RequesterBranchSpecified = true;
            prDocument.RequesterDepartment = (long)pObject.Department;
            prDocument.RequesterDepartmentSpecified = true;
            prDocument.SalesPersonCode = -1;
            prDocument.SalesPersonCodeSpecified = true;
            prDocument.DocDate = pObject.DocDate.Value;
            prDocument.DocDateSpecified = true;
            prDocument.DocDueDate = pObject.DocDueDate.Value;
            prDocument.DocDueDateSpecified = true;
            prDocument.TaxDate = pObject.TaxDate.Value;
            prDocument.TaxDateSpecified = true;
            prDocument.RequriedDate = pObject.ReqDate.Value;
            prDocument.RequriedDateSpecified = true;
            prDocument.Comments = pObject.Comments;

            if (pCompanyParam.IdBranchSelect != null && pCompanyParam.IdBranchSelect > 0)
            {
                prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
                prDocument.BPL_IDAssignedToInvoiceSpecified = true;
            }
            //Creo las lineas del documento
            PurchaseRequestServiceWeb.DocumentDocumentLine[] newLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            int positionNewLine = 0;
            foreach (PurchaseRequestLineSAP portalLine in pObject.Lines)
            {
                PurchaseRequestServiceWeb.DocumentDocumentLine line = new PurchaseRequestServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                if (portalLine.Quantity != null)
                {
                    line.Quantity = (double)portalLine.Quantity;
                    line.QuantitySpecified = true;
                }
                if (portalLine.PriceBefDi != null)
                {
                    line.UnitPrice = (double)portalLine.PriceBefDi;
                    line.UnitPriceSpecified = true;
                }
                line.Currency = portalLine.Currency;
                line.RequiredDate = (portalLine.PQTReqDate == null ? prDocument.RequriedDate : portalLine.PQTReqDate.Value);
                line.RequiredDateSpecified = true;
                line.SalesPersonCode = -1;
                line.SalesPersonCodeSpecified = true;
                line.WarehouseCode = portalLine.WhsCode;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                //oPurchaseRequestService.Add(prDocument);

                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseRequestLineSAP mAuxLine = mPRIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseRequestServiceWeb.MsgHeader header = new PurchaseRequestServiceWeb.MsgHeader();
            header.ServiceName = PurchaseRequestServiceWeb.MsgHeaderServiceName.PurchaseRequestService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseRequestServiceWeb.PurchaseRequestService oPurchaseRequestService = new PurchaseRequestServiceWeb.PurchaseRequestService();
            oPurchaseRequestService.MsgHeaderValue = header;

            PurchaseRequestSAP mPRIni = new PurchaseRequestSAP();
            Mapper.CreateMap(typeof(PurchaseRequestSAP), typeof(PurchaseRequestSAP));
            Mapper.Map(pObject, mPRIni);

            PurchaseRequestServiceWeb.DocumentParams parametros = new PurchaseRequestServiceWeb.DocumentParams();
            parametros.DocEntry = pObject.DocEntry;
            parametros.DocEntrySpecified = true;

            PurchaseRequestServiceWeb.Document prDocument = oPurchaseRequestService.GetByParams(parametros);
            prDocument.Requester = pObject.Requester;
            prDocument.ReqType = (long)pObject.ReqType;
            prDocument.ReqTypeSpecified = true;
            prDocument.RequesterBranch = (long)pObject.Branch;
            prDocument.RequesterBranchSpecified = true;
            prDocument.RequesterDepartment = (long)pObject.Department;
            prDocument.RequesterDepartmentSpecified = true;
            prDocument.DocDate = pObject.DocDate.Value;
            prDocument.DocDateSpecified = true;
            prDocument.DocDueDate = pObject.DocDueDate.Value;
            prDocument.DocDueDateSpecified = true;
            prDocument.TaxDate = pObject.TaxDate.Value;
            prDocument.TaxDateSpecified = true;
            prDocument.RequriedDate = pObject.ReqDate.Value;
            prDocument.RequriedDateSpecified = true;
            prDocument.Comments = pObject.Comments;
            foreach (PurchaseRequestServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                PurchaseRequestLineSAP portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = portalLine.Dscription;
                    if (portalLine.Quantity != null)
                    {
                        docLine.Quantity = (double)portalLine.Quantity;
                        docLine.QuantitySpecified = true;
                    }
                    if (portalLine.PriceBefDi != null)
                    {
                        docLine.UnitPrice = (double)portalLine.PriceBefDi;
                        docLine.UnitPriceSpecified = true;
                    }
                    docLine.Currency = portalLine.Currency;
                    docLine.RequiredDate = (portalLine.PQTReqDate == null ? prDocument.RequriedDate : portalLine.PQTReqDate.Value);
                    docLine.RequiredDateSpecified = true;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.FreeText = portalLine.FreeTxt;
                    if (portalLine.AcctCode != null)
                    {
                        docLine.AccountCode = portalLine.AcctCode;
                    }

                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                }
            }
            PurchaseRequestServiceWeb.DocumentDocumentLine[] newLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (PurchaseRequestLineSAP portalLine in pObject.Lines)
            {
                PurchaseRequestServiceWeb.DocumentDocumentLine line = new PurchaseRequestServiceWeb.DocumentDocumentLine();
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                if (portalLine.Quantity != null)
                {
                    line.Quantity = (double)portalLine.Quantity;
                    line.QuantitySpecified = true;
                }
                if (portalLine.PriceBefDi != null)
                {
                    line.UnitPrice = (double)portalLine.PriceBefDi;
                    line.UnitPriceSpecified = true;
                }
                line.Currency = portalLine.Currency;
                line.RequiredDate = (portalLine.PQTReqDate == null ? prDocument.RequriedDate : portalLine.PQTReqDate.Value);
                line.RequiredDateSpecified = true;
                line.WarehouseCode = portalLine.WhsCode;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            try
            {
                //oPurchaseRequestService.Update(prDocument);


                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseRequestLineSAP mAuxLine = mPRIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);
                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }
        #endregion

        #region Warehouse

        public List<Warehouse> GetListWarehouse(CompanyConn pCompanyParam)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "1 eq 1";

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Warehouse", mFilters);

                List<Warehouse> result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Warehouse>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("GetListWarehouseDS -> GetListWarehouse :" + ex.Message);
                return null;
            }
        }

        #endregion


        #region Oportunities

        public OpportunitiesSAPLine GetOpportunityLine(CompanyConn pCompanyParam)
        {

            return GetLine(pCompanyParam);
        }

        private OpportunitiesSAPLine GetLine(CompanyConn pCompanyParam)
        {
            string mUrl = string.Empty;
            OpportunitiesSAPLine ret = null;
            try
            {

                ret = new OpportunitiesSAPLine();
                //Sales Employee 
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee");
                ret.SalesEmployeeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Buyer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                ret.SlpCode = -1;

                //Document Type
                ret.DocumentTypeList = GetDocumentType();

                //Stages
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OOST");
                ret.StageList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityStage>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                //Stage by Default
                if (ret.StageList != null)
                {
                    if (ret.StageList.Count > 0)
                    {
                        ret.Step_Id = ret.StageList.FirstOrDefault().StepId;
                        ret.ClosePrcnt = ret.StageList.FirstOrDefault().CloPrcnt;
                    }
                }

                //Date by Default
                ret.OpenDate = DateTime.Now;
                ret.CloseDate = DateTime.Now;

                //Status
                ret.Status = "O";

            }
            catch (Exception)
            {

                ret = null;
            }

            return ret;
        }

        private List<DocumentType> GetDocumentType()
        {
            string mUrl = string.Empty;
            List<DocumentType> ret = null;
            try
            {
                ret = new List<DocumentType>();
                ret.Add(new DocumentType() { Code = "", Name = "" });
                ret.Add(new DocumentType() { Code = "23", Name = "Sales Quotations" });
                ret.Add(new DocumentType() { Code = "17", Name = "Sales Orders" });
                ret.Add(new DocumentType() { Code = "13", Name = "Sales Invoices" });
                //ret.Add(new DocumentType() { Code = "22", Name = "Purchase Orders" });
                //ret.Add(new DocumentType() { Code = "18", Name = "Purchase Invoices" });
                //ret.Add(new DocumentType() { Code = "540000006", Name = "Purchase Quotations" });

            }
            catch (Exception)
            {
                ret = null;
            }
            return ret;
        }

        private List<OpportunityType> GetOpportunityType()
        {
            List<OpportunityType> ret = null;
            try
            {
                ret = new List<OpportunityType>();
                ret.Add(new OpportunityType() { Code = "R", Name = "Sales" });
                //ret.Add(new OpportunityType() { Code = "P", Name = "Purchasing" });
            }
            catch (Exception)
            {
                ret = null;
            }
            return ret;
        }

        public OpportunityStage GetStageById(CompanyConn pCompanyParam, int pId)
        {
            string mFilters = "";
            string mUrl = string.Empty;
            OpportunityStage ret = null;
            try
            {
                mFilters = "StepId eq " + pId + "";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OOST", mFilters);
                List<OpportunityStage> mRetList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityStage>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                ret = mRetList.SingleOrDefault();
            }
            catch (Exception)
            {
                ret = null;
            }

            return ret;
        }


        public OpportunitiesSAP GetOportunityById(CompanyConn pCompanyParam, int pId)
        {
            string mUrl = string.Empty;
            string mFilters = "";
            OpportunitiesSAP mRestu;
            try
            {
                mFilters = "OpprId eq " + pId + "";
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OOPR", mFilters);
                List<OpportunitiesSAP> mOpporList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                mRestu = mOpporList.SingleOrDefault();

                if (mRestu != null)
                {
                    //Contact Person
                    mFilters = "CardCode eq '" + mRestu.CardCode + "'";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "ContacPerson", mFilters);
                    mRestu.ContactPersonList.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContacPerson>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)));


                    //Owner
                    if (mRestu.Owner != null)
                    {
                        mFilters = "empID eq " + mRestu.Owner + "";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Employee", mFilters);
                        List<EmployeeSAP> mResultList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        mRestu.OwnerSAP = mResultList.SingleOrDefault();
                    }

                    //Opportunity Lines 
                    mFilters = "OpprId eq " + mRestu.OpprId + "";
                    mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OPR1", mFilters);
                    mRestu.OpportunitiesLines = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunitiesSAPLine>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));


                    foreach (var item in mRestu.OpportunitiesLines)
                    {
                        //Sales Employee 
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee");
                        item.SalesEmployeeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Buyer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        //Document Type
                        item.DocumentTypeList = GetDocumentType();

                        //Stage by Document Type
                        if (mRestu.OpprType == "R")
                        {
                            mFilters = "SalesStage eq 'Y'";
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OOST", mFilters);
                            item.StageList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityStage>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        }
                        else
                        {
                            mFilters = "PurStage eq 'Y'";
                            mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OOST", mFilters);
                            item.StageList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunityStage>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                        }


                        //Activities List
                        mFilters = "OprId eq " + mRestu.OpprId + " and OprLine eq " + item.Line + " ";
                        mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Activities", mFilters);
                        item.ActivitiesList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActivitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
                        //item.ActivitiesList = Mapper.Map<List<ActivitiesSAP>>(mDbContext.OCLG.Where(c => c.OprId == mRestu.OpprId && c.OprLine == item.Line).ToList());

                    }

                }
                else
                {
                    mRestu = new OpportunitiesSAP();
                    mRestu.OpportunitiesLines.Add(GetLine(pCompanyParam));
                }

                mRestu.OpportunityTypeList = GetOpportunityType();

                //Sales Employee     
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "SalesEmployee");
                mRestu.SalesEmployeeList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Buyer>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception ex)
            {
                return null;
            }

            return mRestu;
        }


        public string AddOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            SalesOpportunitiesServiceWeb.MsgHeader header = new SalesOpportunitiesServiceWeb.MsgHeader();
            header.ServiceName = SalesOpportunitiesServiceWeb.MsgHeaderServiceName.SalesOpportunitiesService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesOpportunitiesServiceWeb.SalesOpportunitiesService oOpportunitiesService = new SalesOpportunitiesServiceWeb.SalesOpportunitiesService();
            oOpportunitiesService.MsgHeaderValue = header;

            SalesOpportunitiesServiceWeb.SalesOpportunities mOpporDoc = new SalesOpportunitiesServiceWeb.SalesOpportunities();

            mOpporDoc.OpportunityName = pObject.Name;
            mOpporDoc.CardCode = pObject.CardCode;
            mOpporDoc.ContactPerson = pObject.CprCode ?? 0;
            mOpporDoc.SalesPerson = pObject.SlpCode ?? -1;
            mOpporDoc.SalesPersonSpecified = true;
            mOpporDoc.DataOwnershipfield = pObject.Owner ?? 0;
            mOpporDoc.DataOwnershipfieldSpecified = true;
            if (pObject.PredDate.HasValue)
            {
                mOpporDoc.PredictedClosingDate = pObject.PredDate.Value;
                mOpporDoc.PredictedClosingDateSpecified = true;
            }
            mOpporDoc.MaxLocalTotal = pObject.MaxSumLoc.HasValue ? Convert.ToDouble(pObject.MaxSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.MaxLocalTotalSpecified = true;
            mOpporDoc.WeightedSumLC = pObject.WtSumLoc.HasValue ? Convert.ToDouble(pObject.WtSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.WeightedSumLCSpecified = true;
            mOpporDoc.GrossProfit = pObject.PrcnProf.HasValue ? Convert.ToDouble(pObject.PrcnProf.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.GrossProfitSpecified = true;
            mOpporDoc.GrossProfitTotalLocal = pObject.SumProfL.HasValue ? Convert.ToDouble(pObject.SumProfL.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.GrossProfitTotalLocalSpecified = true;


            switch (pObject.Status)
            {
                case "O":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
                case "L":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Missed;
                    break;
                case "W":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Sold;
                    break;
                default:
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
            }
            mOpporDoc.StartDate = pObject.OpenDate ?? DateTime.Now;
            if (pObject.CloseDate.HasValue)
            {
                mOpporDoc.ClosingDate = pObject.CloseDate.Value;
                mOpporDoc.ClosingDateSpecified = true;
            }
            mOpporDoc.ClosingPercentage = pObject.CloPrcnt.HasValue ? Convert.ToDouble(pObject.CloPrcnt.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.ClosingPercentageSpecified = true;
            mOpporDoc.ProjectCode = pObject.PrjCode;
            mOpporDoc.Remarks = pObject.Memo;


            //Creo las lineas del documento
            SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[] newLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[pObject.OpportunitiesLines.Count];
            int positionNewLine = 0;
            foreach (OpportunitiesSAPLine portalLine in pObject.OpportunitiesLines)
            {
                SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine line = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine();

                line.StartDate = portalLine.OpenDate ?? DateTime.Now;
                line.StartDateSpecified = true;

                line.ClosingDate = portalLine.CloseDate ?? DateTime.Now;
                line.ClosingDateSpecified = true;

                line.SalesPerson = portalLine.SlpCode ?? -1;
                line.SalesPersonSpecified = true;

                line.StageKey = portalLine.Step_Id ?? 0;
                line.StageKeySpecified = true;

                line.PercentageRate = (double)(portalLine.ClosePrcnt.HasValue ? portalLine.ClosePrcnt.Value : 0);
                line.PercentageRateSpecified = true;

                line.MaxLocalTotal = (double)(portalLine.MaxSumLoc.HasValue ? portalLine.MaxSumLoc.Value : 0);
                line.MaxLocalTotalSpecified = true;

                line.WeightedAmountLocal = (double)(portalLine.WtSumLoc.HasValue ? portalLine.WtSumLoc.Value : 0);
                line.WeightedAmountLocalSpecified = true;

                switch (portalLine.ObjType)
                {
                    case "17":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Order;
                        break;
                    case "23":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Quotation;
                        break;
                    case "13":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Invoice;
                        break;
                    case "22":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseOrder;
                        break;
                    case "540000006":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseQutation;
                        break;
                    case "18":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseInvoice;
                        break;
                    default:
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_MinusOne;
                        break;
                }
                line.DocumentTypeSpecified = true;

                if (pObject.DocNum.HasValue)
                {
                    line.DocumentNumber = pObject.DocNum.Value;
                    line.DocumentNumberSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mOpporDoc.SalesOpportunitiesLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[newLines.Length];
            mOpporDoc.SalesOpportunitiesLines = newLines;
            try
            {
                oOpportunitiesService.Add(mOpporDoc);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }


        public string UpdateOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            SalesOpportunitiesServiceWeb.MsgHeader header = new SalesOpportunitiesServiceWeb.MsgHeader();
            header.ServiceName = SalesOpportunitiesServiceWeb.MsgHeaderServiceName.SalesOpportunitiesService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesOpportunitiesServiceWeb.SalesOpportunitiesService oOpportunitiesService = new SalesOpportunitiesServiceWeb.SalesOpportunitiesService();
            oOpportunitiesService.MsgHeaderValue = header;

            SalesOpportunitiesServiceWeb.SalesOpportunitiesParams parametros = new SalesOpportunitiesServiceWeb.SalesOpportunitiesParams();
            parametros.SequentialNo = pObject.OpprId;
            parametros.SequentialNoSpecified = true;

            SalesOpportunitiesServiceWeb.SalesOpportunities mOpporDoc = oOpportunitiesService.GetByParams(parametros);

            mOpporDoc.OpportunityName = pObject.Name;
            mOpporDoc.CardCode = pObject.CardCode;
            mOpporDoc.ContactPerson = pObject.CprCode ?? 0;
            mOpporDoc.SalesPerson = pObject.SlpCode ?? -1;
            mOpporDoc.SalesPersonSpecified = true;
            mOpporDoc.DataOwnershipfield = pObject.Owner ?? 0;
            mOpporDoc.DataOwnershipfieldSpecified = true;
            if (pObject.PredDate.HasValue)
            {
                mOpporDoc.PredictedClosingDate = pObject.PredDate.Value;
                mOpporDoc.PredictedClosingDateSpecified = true;
            }
            mOpporDoc.MaxLocalTotal = pObject.MaxSumLoc.HasValue ? Convert.ToDouble(pObject.MaxSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.MaxLocalTotalSpecified = true;
            mOpporDoc.WeightedSumLC = pObject.WtSumLoc.HasValue ? Convert.ToDouble(pObject.WtSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.WeightedSumLCSpecified = true;
            mOpporDoc.GrossProfit = pObject.PrcnProf.HasValue ? Convert.ToDouble(pObject.PrcnProf.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.GrossProfitSpecified = true;
            //mOpporDoc.GrossProfitTotalLocal = pObject.SumProfL.HasValue ? Convert.ToDouble(pObject.SumProfL.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            //mOpporDoc.GrossProfitTotalLocalSpecified = true;



            switch (pObject.Status)
            {
                case "O":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
                case "L":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Missed;
                    break;
                case "W":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Sold;
                    break;
                default:
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
            }
            mOpporDoc.StartDate = pObject.OpenDate ?? DateTime.Now;
            if (pObject.CloseDate.HasValue)
            {
                mOpporDoc.ClosingDate = pObject.CloseDate.Value;
                mOpporDoc.ClosingDateSpecified = true;
            }
            mOpporDoc.ClosingPercentage = pObject.CloPrcnt.HasValue ? Convert.ToDouble(pObject.CloPrcnt.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.ClosingPercentageSpecified = true;
            mOpporDoc.ProjectCode = pObject.PrjCode;
            mOpporDoc.Remarks = pObject.Memo;

            //Creo las lineas del documento
            SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[] newLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[pObject.OpportunitiesLines.Count];
            int positionNewLine = 0;
            foreach (OpportunitiesSAPLine portalLine in pObject.OpportunitiesLines)
            {
                SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine line = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine();

                line.LineNum = portalLine.Line;
                line.LineNumSpecified = true;

                line.StartDate = portalLine.OpenDate ?? DateTime.Now;
                line.StartDateSpecified = true;

                line.ClosingDate = portalLine.CloseDate ?? DateTime.Now;
                line.ClosingDateSpecified = true;

                line.SalesPerson = portalLine.SlpCode ?? -1;
                line.SalesPersonSpecified = true;

                line.StageKey = portalLine.Step_Id ?? 0;
                line.StageKeySpecified = true;

                line.PercentageRate = (double)(portalLine.ClosePrcnt.HasValue ? portalLine.ClosePrcnt.Value : 0);
                line.PercentageRateSpecified = true;

                line.MaxLocalTotal = (double)(portalLine.MaxSumLoc.HasValue ? portalLine.MaxSumLoc.Value : 0);
                line.MaxLocalTotalSpecified = true;

                line.WeightedAmountLocal = (double)(portalLine.WtSumLoc.HasValue ? portalLine.WtSumLoc.Value : 0);
                line.WeightedAmountLocalSpecified = true;

                switch (portalLine.Status)
                {
                    case "O":
                        line.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineStatus.so_Open;
                        break;
                    case "C":
                        line.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineStatus.so_Closed;
                        break;
                }
                line.StatusSpecified = true;

                switch (portalLine.ObjType)
                {
                    case "17":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Order;
                        break;
                    case "23":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Quotation;
                        break;
                    case "13":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Invoice;
                        break;
                    case "22":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseOrder;
                        break;
                    case "540000006":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseQutation;
                        break;
                    case "18":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseInvoice;
                        break;
                    default:
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_MinusOne;
                        break;
                }
                line.DocumentTypeSpecified = true;

                if (portalLine.DocNumber.HasValue)
                {
                    line.DocumentNumber = portalLine.DocNumber.Value;
                    line.DocumentNumberSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mOpporDoc.SalesOpportunitiesLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[newLines.Length];
            mOpporDoc.SalesOpportunitiesLines = newLines;

            try
            {
                oOpportunitiesService.Update(mOpporDoc);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }


        public List<OpportunitiesSAP> GetOpportunityListSearch(CompanyConn pCompanyParam, string pCodeBP = "", int? pSalesEmp = null, string pOpportunityName = "", int? pOwner = null, DateTime? pDate = null, string pStatus = "")
        {
            string mUrl = string.Empty;
            string mFilters = "";
            List<OpportunitiesSAP> mRestu;
            mFilters = " 1 eq 1 ";

            try
            {
                if (!string.IsNullOrEmpty(pCodeBP))
                {
                    mFilters = mFilters + " and CardCode eq '" + pCodeBP + "'";
                }

                if (pSalesEmp != null)
                {
                    mFilters = mFilters + " and SlpCode eq " + pSalesEmp + "";
                }

                if (!string.IsNullOrEmpty(pOpportunityName))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pOpportunityName + "'), tolower(Name))";
                }

                if (pOwner != null)
                {
                    mFilters = mFilters + " and Owner eq " + pOwner + "";
                }

                if (pDate != null)
                {
                    DateTime mDate = pDate ?? DateTime.Now;
                    mFilters = mFilters + " and  OpenDate eq datetime'" + mDate.ToString("yyyy-MM-dd") + "'";
                }

                if (!string.IsNullOrEmpty(pStatus))
                {
                    mFilters = mFilters + " and Status eq '" + pStatus + "'";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "OOPR", mFilters);
                mRestu = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OpportunitiesSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

            }
            catch (Exception)
            {
                return null;
            }
            return mRestu;
        }


        #endregion


        #region Projects

        public List<ProjectSAP> GetProjectsSAPList(CompanyConn pCompanyParam, string pProjectCode, string pProjectName)
        {
            try
            {
                string mUrl = string.Empty;
                string mFilters = "1 eq 1";

                if (!string.IsNullOrEmpty(pProjectCode))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pProjectCode + "'), tolower(PrjCode))";
                }

                if (!string.IsNullOrEmpty(pProjectName))
                {
                    mFilters = mFilters + " and substringof(tolower('" + pProjectName + "'), tolower(PrjName))";
                }

                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "Project", mFilters);

                List<ProjectSAP> result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectSAP>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> GetProjectsSAPList :" + ex.Message);
                return null;
            }
        }

        #endregion


        //private Dictionary<string, string> GetObjectListWithUDF(List<string> pListUDF, Newtonsoft.Json.Linq.JObject pJsonObject)
        //{
        //    Dictionary<string, string> mDictionaryAux = new Dictionary<string, string>();
        //    if (pListUDF != null)
        //    {
        //        foreach (string mUDFAux in pListUDF)
        //            mDictionaryAux.Add(mUDFAux, (string)pJsonObject[mUDFAux]);
        //    }

        //    return mDictionaryAux;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pIsUpdate"></param>
        /// <param name="pHeader"></param>
        /// <param name="pBody"></param>
        /// <returns></returns>
        private XmlDocument GetSoapStructure(bool pIsUpdate, object pHeader, object pBody)
        {
            string body = string.Empty;
            XmlSerializer mySerializer = new XmlSerializer(pBody.GetType(), @"http://www.sap.com/SBO/DIS");
            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);
            xns.Add(string.Empty, @"http://www.sap.com/SBO/DIS");

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = Encoding.GetEncoding("UTF-8")
            };

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, xmlWriterSettings))
                {
                    mySerializer.Serialize(xmlWriter, pBody, xns);
                }

                body = stringWriter.ToString();
            }


            MemoryStream streamDocs = new MemoryStream();
            mySerializer.Serialize(streamDocs, pBody, xns);
            XmlDocument mXmlDocument = new XmlDocument();
            mXmlDocument.LoadXml(@"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Header></soap:Header><soap:Body><" + (pIsUpdate ? "Update" : "Add") + @" xmlns=""" + pHeader.GetType().GetProperty("ServiceName").GetValue(pHeader, null) + @"""></" + (pIsUpdate ? "Update" : "Add") + "></soap:Body></soap:Envelope>");
            XmlElement mBodyElement = mXmlDocument.DocumentElement;

            mBodyElement.SelectSingleNode("//*[local-name()='" + (pIsUpdate ? "Update" : "Add") + "']").InnerXml = (body).Remove(0, 22);

            streamDocs = new MemoryStream();

            mySerializer = new XmlSerializer(pHeader.GetType(), @"http://www.sap.com/SBO/DIS");
            mySerializer.Serialize(streamDocs, pHeader, xns);

            mBodyElement.SelectSingleNode("//*[local-name()='Header']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            return mXmlDocument;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pXml"></param>
        /// <param name="pDictionary"></param>
        /// <returns></returns>
        private string AddUDFToXml(string pXml, Dictionary<string, string> pDictionary)
        {
            foreach (var mDictRow in pDictionary)
            {
                pXml = pXml + "<" + mDictRow.Key + ">" + mDictRow.Value + "</" + mDictRow.Key + ">";
            }

            return pXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUgpEntry"></param>
        /// <returns></returns>
        private List<UnitOfMeasure> GetUOMLists(CompanyConn pCompanyParam, int pUgpEntry)
        {
            List<UnitOfMeasure> oUnitOfMeasure = new List<UnitOfMeasure>();
            if (pUgpEntry != -1)
            {

                string mFilters = "UgpEntry eq " + pUgpEntry;
                string mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "UOMGroup", mFilters);
                oUnitOfMeasure = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UnitOfMeasure>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword));
            }
            else
            {
                oUnitOfMeasure.Add(new UnitOfMeasure() { UomEntry = -1, UomCode = "Manual", UomName = "Manual", LineNum = 1, BaseQty = 1 });
            }
            return oUnitOfMeasure;
        }

        #region Payment Means

        private bool AddPaymentByOrder(CompanyConn pCompanyParam, List<PaymentMeanOrder> listPaymentOrder, int type, int docEntry, int docnum)
        {
            string mUrl = string.Empty;
            List<PaymentMeanOrder> paymentMeanOrder;
            try
            {
                paymentMeanOrder = new List<PaymentMeanOrder>();
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMeanByOrder", $"U_DocType eq {type} and U_DocEntryOrder eq {docEntry}");
                paymentMeanOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMeanOrder>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

                //OrderByPaymentWeb.MsgHeader headerOrderByPaymentWeb = new OrderByPaymentWeb.MsgHeader();
                //headerOrderByPaymentWeb.ServiceName = OrderByPaymentWeb.MsgHeaderServiceName.ARGNS_POORDERPAYME;
                //headerOrderByPaymentWeb.ServiceNameSpecified = true;
                //headerOrderByPaymentWeb.SessionID = pCompanyParam.DSSessionId;

                //OrderByPaymentWeb.ARGNS_POORDERPAYME OrderByPaymentService = new OrderByPaymentWeb.ARGNS_POORDERPAYME();
                //OrderByPaymentService.MsgHeaderValue = headerOrderByPaymentWeb;

                foreach (var item in paymentMeanOrder)
                {
                    //OrderByPaymentService.Remove(new OrderByPaymentWeb.ARGNS_POORDERPAYMEParams { DocEntry = item.DocEntry, DocEntrySpecified = true });

                    var result = SLManager.callAPI(pCompanyParam, $"ARGNS_POORDERPAYME({item.DocEntry})", RestSharp.Method.DELETE, "");

                }

                foreach (var item in listPaymentOrder)
                {
                    var ordernewOrderByPayment = new OrderByPaymentWeb.ARGNS_POORDERPAYME1
                    {
                        U_Account = item.U_Account,
                        U_AccountSpecified = true,
                        U_Date = item.U_Date ?? System.DateTime.Now,
                        U_DateSpecified = item.U_Date == null ? false : true,
                        U_CUIT = item.U_CUIT,
                        U_CUITSpecified = true,
                        U_DueDate = item.U_DueDate ?? System.DateTime.Now,
                        U_DueDateSpecified = item.U_DueDate == null ? false : true,
                        U_Reference = item.U_Reference,
                        U_ReferenceSpecified = true,
                        U_DocNumOrder = docnum,
                        u_DocNumOrderSpecified = true,
                        U_AmountSurcharger = Convert.ToDouble(item.U_AmountSurcharger),
                        U_AmountSurchargerSpecified = true,
                        U_Amount = Convert.ToDouble(item.U_Amount),
                        U_AmountSpecified = true,
                        U_DocEntryOrder = docEntry,
                        U_DocEntryOrderSpecified = true,
                        U_DocType = type,
                        U_DocTypeSpecified = true,
                        U_PaymentCode = item.U_PaymentCode,
                        U_CreditCard = item.U_CreditCard,
                        U_CreditCardSpecified = true,
                        U_Voucher = item.U_Voucher,
                        U_VoucherSpecified = true,
                        U_Quotas = item.U_Quotas ?? 0,
                        U_QuotasSpecified = true,
                        U_Neto = Convert.ToDouble(item.U_Neto),
                        U_NetoSpecified = true,
                        U_Contract = item.U_Contract ?? 0,
                        U_ContractSpecified = true
                    };

                    item.U_DocEntryOrder = docEntry;
                    item.U_DocNumOrder = docnum;
                    item.U_DocType = type;
                    var jsonToAdd = JsonConvert.SerializeObject(item);

                    var result = SLManager.callAPI(pCompanyParam, "ARGNS_POORDERPAYME", RestSharp.Method.POST, jsonToAdd);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceSL -> AddPaymentByOrder:" + ex.Message);

                return false;
            }

            return true;
        }

        public List<PaymentMeanOrder> GetForDraft(CompanyConn pCompanyParam, int pCode)
        {
            var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];
            string select = $"\"{udfIdPayment}\"";
            string mUrl = string.Empty;
            string valueData = string.Empty;

            var xmlResult = GlobalUtil.getData(pCompanyParam.DSSessionId, "ODRF", $"\"DocEntry\" = {pCode}", select, true);

            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

            xmlDoc.LoadXml(xmlResult);
            System.Xml.XmlNodeList resultList = xmlDoc.SelectNodes("//*[local-name()='row']");

            if (null != resultList && resultList.Count > 0)
            {

                foreach (System.Xml.XmlNode node in resultList)
                {
                    valueData = node.SelectSingleNode($"*[local-name()='{udfIdPayment}']").InnerText;
                }
            }

            List<PaymentMeanOrder> paymentMeanOrder = new List<PaymentMeanOrder>();
            try
            {
                mUrl = RESTService.GetURL(pCompanyParam.UrlHana, "PaymentMeanByOrder", $"U_DocType eq {17} and U_DocEntryOrder eq {valueData}");
                paymentMeanOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PaymentMeanOrder>>(RESTService.GetRequestJson(mUrl, WebServices.UtilWeb.MethodType.GET, pCompanyParam.DbUserName, pCompanyParam.DbPassword)).ToList();

            }
            catch (Exception)
            {
                return new List<PaymentMeanOrder>();
            }

            return paymentMeanOrder;
        }

        #endregion
    }
}