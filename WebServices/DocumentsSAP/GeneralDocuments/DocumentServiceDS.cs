﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using AutoMapper;
using WebServices.Model.Tables;
using WebServices.Model;
using System.Data.Entity;
using System.Xml;
using ARGNS.Util;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Linq.Dynamic;
using static ARGNS.Util.Enums;
using ARGNS.Model.Interfaces.SAP;
using ExtendedXmlSerializer.Configuration;
using ExtendedXmlSerializer.ExtensionModel.Xml;
using System.Xml.Linq;
using System.Reflection;
using System.Web.Configuration;
using WebServices.Query;

namespace WebServices.DocumentsSAP.GeneralDocuments
{
    public class DocumentServiceDS : IDocumentService
    {
        private DataBase mDbContext;
        private B1WSHandler mB1WSHandler;

        public DocumentServiceDS()
        {
            //Database.SetInitializer<DataBase>(null);
            mDbContext = new DataBase();
            mB1WSHandler = new B1WSHandler();
        }


        public DocumentSAPCombo GetDocumentSAPCombo(CompanyConn pCompanyParam)
        {
            //Obtengo el ConnString con los Parametros de la Company y abro conexion
            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();
            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();
            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();
            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();
            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();
            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();
            Mapper.CreateMap<CountrySAP, OCRY>();
            Mapper.CreateMap<OCRY, CountrySAP>();

            DocumentSAPCombo retCb = new DocumentSAPCombo();
            try
            {
                //Buyer
                List<OSLP> mLisOSLP = mDbContext.OSLP.ToList();

                //Currency
                List<OCRN> mListOCRN = mDbContext.OCRN.ToList();

                //PaymentTerm
                List<OCTG> mListOCTG = mDbContext.OCTG.ToList();

                //PaymentMethod
                // List<OPYM> mListOPYM = mDbContext.OPYM.ToList();

                //ShippingType
                List<OSHP> mListOSHP = mDbContext.OSHP.ToList();

                //Taxt
                List<OVTG> mListOVTG = mDbContext.OVTG.ToList();

                //COUNTRY
                List<OCRY> mListOCRY = mDbContext.OCRY.ToList();

                retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

                retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

                retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

                // retCb.ListPaymentMethod = mListOPYM.Select(c => new PaymentMethod { PayMethCod = c.PayMethCod, Descript = c.Descript }).ToList(); 

                retCb.ListCountry = Mapper.Map<List<CountrySAP>>(mListOCRY);

                retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

                retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

                retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

                return retCb;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetDocumentSAPCombo :" + ex.Message);
                return null;
            }
        }


        #region Common Method

        public List<GLAccountSAP> GetGLAccountList(CompanyConn pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<OACT, GLAccountSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OACT> ListRestu = mDbContext.OACT.Where(c => c.FormatCode != null).ToList();

                return Mapper.Map<List<GLAccountSAP>>(ListRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetGLAccountList:" + ex.Message);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public string GetSystemCurrency(CompanyConn pCompanyParam)
        {
            return GetCurrency(WebServices.UtilWeb.CurrentType.SystemCurrency.ToString(), pCompanyParam.DSSessionId);
        }

        public string GetLocalCurrency(CompanyConn pCompanyParam)
        {
            return GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);
        }

        public string GetCurrencyRate(string pCurrency, DateTime pDate, CompanyConn pCompanyParam)
        {
            string mXmlResult = string.Empty;
            string ret = string.Empty;
            string pXmlString = string.Empty;
            string AddCmd = string.Empty;
            SBODI_Server.Node mServerNode = null;
            XmlDocument mResultXML = null;

            try
            {
                mResultXML = new XmlDocument();
                mServerNode = new SBODI_Server.Node();


                AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                @"<env:Body><dis:GetCurrencyRate xmlns:dis=""http://www.sap.com/SBO/DIS""><Currency>" + pCurrency + "</Currency><Date>" + pDate.ToString("yyyyMMdd") + "</Date></dis:GetCurrencyRate></env:Body></env:Envelope>";


                //Envio Solicitud al DI Server y Obtengo la Respuesta
                mXmlResult = mServerNode.Interact(AddCmd);
                mResultXML.LoadXml(mXmlResult);

                //Recupero los Valores de la Respuesta                
                return UtilWeb.GetResult(mResultXML, WebServices.UtilWeb.CurrentType.CurrencyRate.ToString());
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetCurrencyRate:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public string GetCurrency(string pCurrType, string pSession)
        {
            string mXmlResult = string.Empty;
            string ret = string.Empty;
            string pXmlString = string.Empty;
            string AddCmd = string.Empty;
            SBODI_Server.Node mServerNode = null;
            XmlDocument mResultXML = null;

            try
            {
                mResultXML = new XmlDocument();
                mServerNode = new SBODI_Server.Node();

                //Poner Enum
                if (pCurrType == WebServices.UtilWeb.CurrentType.SystemCurrency.ToString())
                {
                    AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                    @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                    "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                    @"<env:Body><dis:GetSystemCurrency xmlns:dis=""http://www.sap.com/SBO/DIS""></dis:GetSystemCurrency></env:Body></env:Envelope>";
                }
                else
                {
                    AddCmd = ret = @"<?xml version=""1.0"" ?>" +
                    @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                     "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                    @"<env:Body><dis:GetLocalCurrency xmlns:dis=""http://www.sap.com/SBO/DIS""></dis:GetLocalCurrency></env:Body></env:Envelope>";

                }


                //Envio Solicitud al DI Server y Obtengo la Respuesta
                mXmlResult = mServerNode.Interact(AddCmd);
                mResultXML.LoadXml(mXmlResult);

                //Recupero los Valores de la Respuesta                
                return UtilWeb.GetResult(mResultXML, pCurrType);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetCurrency:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        public List<SalesTaxCodesSAP> GetTaxCode(CompanyConn pCompanyParam)
        {
            List<SalesTaxCodesSAP> ret = null;
            try
            {
                Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
                Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = Mapper.Map<List<SalesTaxCodesSAP>>(mDbContext.OSTC.ToList());

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetCurrency:" + ex.Message);
            }

            return ret;
        }

        public List<BranchesSAP> GetBranchesByUser(CompanyConn pCompanyParam, string userSAP)
        {
            List<BranchesSAP> ret = null;

            try
            {

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = (from c in mDbContext.USR6 join j in mDbContext.OBPL on c.BPLId equals j.BPLId where c.UserCode == userSAP && j.Disabled == "N" select new BranchesSAP { BPLId = j.BPLId, BPLName = j.BPLName }).ToList();

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetBranchesByUser:" + ex.Message);
            }

            return ret;
        }

        public SerialSAP GetSerialsByDocument(CompanyConn pCompanyParam, int docNum, int line, string whsCode, DocumentsType docType, string itemCode, List<UDF_ARGNS> pListUDF = null, int anio = 0)
        {
            SerialSAP serialSAP = new SerialSAP();

            try
            {
                var whsUseBin = false;
                var where = string.Empty;
                var wherebySAP = string.Empty;
                var whereAnio = string.Empty;
                var whereLine = string.Empty;
                //var itemCode = string.Empty;
                serialSAP.SerialsSAP = new List<Serial>();

                string connectionString = Utils.ConnectionString(pCompanyParam);

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                XDocument docQuery = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "/Query/SQL.xml");

                //Run query
                var QueryUDFs = (from UdfQuerySerials in docQuery.Elements("Query").Elements("UdfQuerySerials").Elements("value")
                                 select UdfQuerySerials.Value).FirstOrDefault();


                if (QueryUDFs != "")
                {
                    QueryUDFs = "AND " + QueryUDFs + "  ";
                }

                if (line < 0)
                {
                    whereLine = $"T0.DocLine > { line}";
                }
                else
                {
                    whereLine = $"T0.DocLine = { line}";
                }

                if (!string.IsNullOrEmpty(whsCode))
                { whsUseBin = mDbContext.OWHS.Where(c => c.WhsCode == whsCode).FirstOrDefault().BinActivat == "Y" ? true : false; }

                where = $"WHERE T0.DocEntry = {docNum} AND T0.DocType = {(int)docType} AND {whereLine}";

                if (whsUseBin)
                {
                    wherebySAP = $"join OSBQ T3 ON T1.MdAbsEntry = T3.SnBMDAbs AND T3.WhsCode = '{whsCode}' and T3.ItemCode = '{itemCode}' and  T3.OnHandQty > 0";
                }
                else
                {
                    wherebySAP = $"WHERE T0.ItemCode = '{itemCode}' and T1.MdAbsEntry NOT IN (SELECT SnBMDAbs FROM OSBQ)";
                }

                if (docNum > 0)
                {
                    var query = string.Format(QueryResource.QueryGet("FilterAnioByLine"), docNum, line);

                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(mB1WSHandler.ExecuetQuery(pCompanyParam.DSSessionId, query));

                    var nodes = xml.SelectNodes("//*[local-name()='row']");
                    foreach (XmlNode row in nodes)
                    {

                        anio = Convert.ToInt32(row.SelectSingleNode(".//*[local-name()='U_anioSelected']").InnerText);
                    }
                }

                if (anio != 0)
                {
                    whereAnio = string.Format(QueryResource.QueryGet("FilterAnio"), anio);
                }

                serialSAP.SerialsSAP = mDbContext.Database.SqlQuery<Serial>("SELECT T2.DistNumber,T1.SysNumber,T2.AbsEntry, SUM(T1.AllocQty) AllocQty " +
                   "FROM OITL T0 " +
                   "JOIN ITL1 T1 ON T0.LogEntry = T1.LogEntry " +
                   "JOIN OSRN T2 ON T1.SysNumber = T2.SysNumber AND T0.ItemCode = T2.ItemCode " + QueryUDFs +
                   wherebySAP + whereAnio +
                   "GROUP BY  T2.DistNumber,T1.SysNumber,T2.AbsEntry HAVING SUM(T1.AllocQty)=0").ToList<Serial>();

                serialSAP.SerialsInDocument = mDbContext.Database.SqlQuery<Serial>("SELECT T0.DocNum, T0.DocLine, T2.DistNumber,T1.SysNumber,T2.AbsEntry, SUM(T1.AllocQty) AllocQty " +
                    "FROM OITL T0 " +
                    "JOIN ITL1 T1 ON T0.LogEntry = T1.LogEntry " +
                    "JOIN OSRN T2 ON T1.SysNumber = T2.SysNumber AND T0.ItemCode = T2.ItemCode " + QueryUDFs +
                    where +
                    "GROUP BY T0.DocNum, T0.DocLine, T2.DistNumber,T1.SysNumber,T2.AbsEntry HAVING SUM(T1.AllocQty)<>0").ToList<Serial>();


                foreach (var item in serialSAP.SerialsInDocument)
                {
                    string conditional = itemCode != "" ? $" and [OSRN].[ItemCode] = '{itemCode}'" : "";

                    string mWhere = $"[OSRN].[DistNumber] = '{item.DistNumber}' {conditional}  and [OSRN].[SysNumber] = {item.SysNumber}";
                    item.UDFs = UDFUtil.GetObjectListWithUDF(pListUDF, mWhere, typeof(OSRN), connectionString, "OSRN").Cast<OSRN>().FirstOrDefault().MappedUdf;

                    foreach (var itemUDF in item.UDFs.Where(c => c.RTable != null))
                    {
                        var masterData = GlobalUtil.getMasterData(pCompanyParam.DSSessionId, itemUDF.RTable, $" Code = '{itemUDF.Value}' ", false);

                        itemUDF.Value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");
                    }
                }

                foreach (var item in serialSAP.SerialsSAP)
                {
                    string mWhere = $"[OSRN].[DistNumber] = '{item.DistNumber}' and [OSRN].[ItemCode] = '{itemCode}' and [OSRN].[SysNumber] = {item.SysNumber}";
                    item.UDFs = UDFUtil.GetObjectListWithUDF(pListUDF, mWhere, typeof(OSRN), connectionString, "OSRN").Cast<OSRN>().FirstOrDefault().MappedUdf;

                    foreach (var itemUDF in item.UDFs.Where(c => c.RTable != null))
                    {
                        var masterData = GlobalUtil.getMasterData(pCompanyParam.DSSessionId, itemUDF.RTable, $" Code = '{itemUDF.Value}' ", false);

                        itemUDF.Value = (masterData.Rows.Count() > 0 ? masterData.Rows.FirstOrDefault().Name : "??");
                    }
                }



            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSerialsByDocument:" + ex.Message);
            }

            return serialSAP;
        }


        public List<PaymentMean> GetPaymentMeans(CompanyConn pCompanyParam, string branchCode)
        {
            List<PaymentMean> paymentMeans = new List<PaymentMean>();
            try
            {
                string connectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                paymentMeans = mDbContext.Database.SqlQuery<PaymentMean>("SELECT * FROM [@ARGNS_POPAYMENTMEA]").ToList<PaymentMean>();

                foreach (var item in paymentMeans)
                {
                    item.U_AmountRule = item.U_AmountRule ?? 0;
                    item.U_Surcharge = item.U_Surcharge ?? 0;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPaymentMeans:" + ex.Message);
            }

            return paymentMeans;

        }

        public List<PaymentMeanType> GetPaymentMeanType(CompanyConn pCompanyParam)
        {
            List<PaymentMeanType> paymentMeanType = new List<PaymentMeanType>();
            try
            {
                string connectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                paymentMeanType = mDbContext.Database.SqlQuery<PaymentMeanType>("SELECT * FROM [@ARGNS_POPAYMENTTYPE]").ToList<PaymentMeanType>();

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPaymentMeanType:" + ex.Message);
            }

            return paymentMeanType;

        }
        #endregion


        #region Sales Invoice

        public JsonObjectResult GetSalesInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<OINV, SalesInvoiceSAP>(); }).CreateMapper();
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                pCompanyParam.IdBranchSelect = pCompanyParam.IdBranchSelect ?? 0;

                int mSECode = Convert.ToInt32(pSECode == "" ? "0" : pSECode);
                int mOwnerCode = Convert.ToInt32(pOwnerCode == "" ? "0" : pOwnerCode);

                int mTotalRecords = mDbContext.OINV.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true) && (pCompanyParam.IdBranchSelect > 0 ? c.BPLId == pCompanyParam.IdBranchSelect : true)).Count();
                List<OINV> ListRestu = mDbContext.OINV.Where(c => (c.CardCode.Contains(pCodeVendor)) &&
                        (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) &&
                        (pDate == null ? c.DocDate != null : c.DocDate == pDate) &&
                        (pDocStatus != "" ? c.DocStatus == pDocStatus : true) &&
                        (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) &&
                        (mSECode != 0 ? c.SlpCode == mSECode : true) &&
                        (pCompanyParam.IdBranchSelect > 0 ? c.BPLId == pCompanyParam.IdBranchSelect : true)).
                        OrderBy(Utils.OrderString(pOrderColumn)).
                        Skip(pStart).
                        Take(pLength).
                        ToList();

                mJsonObjectResult.SalesInvoiceSAPList = mapper.Map<List<SalesInvoiceSAP>>(ListRestu);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());
                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesInvoiceListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public SalesInvoiceSAP GetAllbySI(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOINV = null, List<UDF_ARGNS> pListUDFINV1 = null)
        {
            SalesInvoiceSAP ret = null;
            try
            {
                Mapper.CreateMap<SalesInvoiceLineSAP, INV1>();
                Mapper.CreateMap<INV1, SalesInvoiceLineSAP>();

                Mapper.CreateMap<SalesInvoiceSAP, OINV>();
                Mapper.CreateMap<OINV, SalesInvoiceSAP>();

                Mapper.CreateMap<DocumentAddress, INV12>();
                Mapper.CreateMap<INV12, DocumentAddress>();

                string mConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.ConnectionString = mConnectionString;
                mDbContext.Database.Connection.Open();

                string mWhere = "[OINV].[DocEntry] = " + pCode;
                OINV mOINV = UDFUtil.GetObjectListWithUDF(pListUDFOINV, mWhere, typeof(OINV), mConnectionString, "OINV").Cast<OINV>().FirstOrDefault();

                if (mOINV != null)
                {

                    ret = Mapper.Map<SalesInvoiceSAP>(mOINV);
                    ret = FillSIDocumentSAP(pCompanyParam, ret, mDbContext);

                    mWhere = "[INV1].[DocEntry] = " + pCode;
                    List<INV1> mListINV1 = UDFUtil.GetObjectListWithUDF(pListUDFINV1, mWhere, typeof(INV1), mConnectionString, "INV1").Cast<INV1>().ToList();

                    ret.Lines = Mapper.Map<List<SalesInvoiceLineSAP>>(mListINV1);
                    ret.SIAddress = Mapper.Map<DocumentAddress>(mDbContext.INV12.Where(c => c.DocEntry == ret.DocEntry).FirstOrDefault());

                }
                else
                {
                    ret = new SalesInvoiceSAP();
                }
                //ret.ListDocumentSAPCombo = GetDocumentSAPCombo(pCompanyParam);



                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetAllbySI :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public SalesInvoiceSAP FillSIDocumentSAP(CompanyConn pCompanyParam, SalesInvoiceSAP pSaleOrderInvSAP, DataBase pDataBase)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();


            //Buyer
            List<OSLP> mLisOSLP = pDataBase.OSLP.ToList();

            //Currency
            List<OCRN> mListOCRN = pDataBase.OCRN.ToList();

            //PaymentMethod
            List<OCTG> mListOCTG = pDataBase.OCTG.ToList();

            //PaymentTerm
            //List<OPYM> mListOPYM = pDataBase.OPYM.ToList();

            //ShippingType
            List<OSHP> mListOSHP = pDataBase.OSHP.ToList();

            //Taxt
            List<OVTG> mListOVTG = pDataBase.OVTG.ToList();

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pSaleOrderInvSAP.CardCode))
            {
                List<PaymentMethod> mList = (from mCRD2 in mDbContext.CRD2
                                             join mOYPM in mDbContext.OPYM on mCRD2.PymCode equals mOYPM.PayMethCod
                                             where mCRD2.CardCode == pSaleOrderInvSAP.CardCode
                                             select new PaymentMethod()
                                             {
                                                 PayMethCod = mOYPM.PayMethCod,
                                                 Descript = mOYPM.Descript

                                             }).ToList();

                retCb.ListPaymentMethod.AddRange(mList);
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pSaleOrderInvSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = pDataBase.OACT.Where(c => c.FormatCode != null).ToList();
            pSaleOrderInvSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //Warehouse
            List<OWHS> mListOWHS = pDataBase.OWHS.ToList();
            pSaleOrderInvSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pSaleOrderInvSAP.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);
            pSaleOrderInvSAP.SystemCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.SystemCurrency.ToString(), pCompanyParam.DSSessionId);

            //DistrbRule
            List<OOCR> mListOOCR = pDataBase.OOCR.Where(c => c.Active == "Y").ToList();
            pSaleOrderInvSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pSaleOrderInvSAP;
        }

        #endregion


        #region Approval
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDate"></param>
        /// <param name="pDocStatus"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetPurchaseApprovalListSearch(CompanyConn pCompanyParam,
            int pUserSign,
            DateTime? pDocDate = null,
            string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<DocumentConfirmationLines> approvalList = new List<DocumentConfirmationLines>();
                approvalList = (from mWDD1 in mDbContext.WDD1
                                join mOWDD in mDbContext.OWDD on mWDD1.WddCode equals mOWDD.WddCode
                                join mODRF in mDbContext.ODRF on mOWDD.DraftEntry equals mODRF.DocEntry
                                where
                                (
                                    (mWDD1.UserID == pUserSign)
                                    && (pDocStatus == "" ? mWDD1.Status != null : mWDD1.Status == pDocStatus)
                                    && (pDocDate == null ? mWDD1.CreateDate != null : mWDD1.CreateDate == pDocDate)
                                    && (mOWDD.DraftType == "112")
                                    && (pObjectId == "" || (mODRF.ObjType == "22" ||
                                    mODRF.ObjType == "540000006" || mODRF.ObjType == "1470000113"))
                                )
                                select new DocumentConfirmationLines()
                                {
                                    CreateDate = mWDD1.CreateDate,
                                    CreateTime = mWDD1.CreateTime,
                                    Remarks = mWDD1.Remarks,
                                    Status = mWDD1.Status,
                                    StepCode = mWDD1.StepCode,
                                    UpdateDate = mWDD1.UpdateDate,
                                    UpdateTime = mWDD1.UpdateTime,
                                    UserID = mWDD1.UserID,
                                    UserSign = mWDD1.UserSign,
                                    WddCode = mWDD1.WddCode,
                                    TaxDate = mODRF.TaxDate,
                                    ReqDate = mODRF.ReqDate,
                                    RequestorRemark = mOWDD.Remarks,
                                    ObjType = mODRF.ObjType,
                                    ODRFDocEntry = mODRF.DocEntry,
                                    DocTotal = mODRF.DocTotal,
                                    DocumentComments = mODRF.Comments,
                                    CardCode = mODRF.CardCode,
                                    CardName = mODRF.CardName
                                }).ToList();

                //lista con los documentos que ya respondi su aprobacion (docentry = docnum)
                approvalList.AddRange((from mWDD1 in mDbContext.WDD1
                                       join mOWDD in mDbContext.OWDD on mWDD1.WddCode equals mOWDD.WddCode
                                       join mODRF in mDbContext.ODRF on mOWDD.DraftEntry equals mODRF.DocNum
                                       where
                                       (
                                           (mWDD1.UserID == pUserSign)
                                           && (pDocStatus == "" ? mWDD1.Status != null : mWDD1.Status == pDocStatus)
                                           && (pDocDate == null ? mWDD1.CreateDate != null : mWDD1.CreateDate == pDocDate)
                                           && (mOWDD.ObjType == mODRF.ObjType)
                                           && (mODRF.ObjType == "22" || mODRF.ObjType == "540000006" || mODRF.ObjType == "1470000113")
                                       )
                                       select new DocumentConfirmationLines()
                                       {
                                           CreateDate = mWDD1.CreateDate,
                                           CreateTime = mWDD1.CreateTime,
                                           Remarks = mWDD1.Remarks,
                                           Status = mWDD1.Status,
                                           StepCode = mWDD1.StepCode,
                                           UpdateDate = mWDD1.UpdateDate,
                                           UpdateTime = mWDD1.UpdateTime,
                                           UserID = mWDD1.UserID,
                                           UserSign = mWDD1.UserSign,
                                           WddCode = mWDD1.WddCode,
                                           TaxDate = mODRF.TaxDate,
                                           ReqDate = mODRF.ReqDate,
                                           RequestorRemark = mOWDD.Remarks,
                                           ObjType = mODRF.ObjType,
                                           ODRFDocEntry = mODRF.DocEntry,
                                           DocTotal = mODRF.DocTotal,
                                           DocumentComments = mODRF.Comments,
                                           CardCode = mODRF.CardCode,
                                           CardName = mODRF.CardName
                                       }).ToList().GroupBy(x => x.WddCode).Select(g => g.First()));

                return approvalList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseApprovalListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<DocumentConfirmationLines> GetPurchaseApprovalByOriginator(CompanyConn pCompanyParam,
            int pUserSign, DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<DocumentConfirmationLines> approvalList = new List<DocumentConfirmationLines>();

                //lista con los documentos que necesitan aprobacion y origino el usuario actual (docentry = docentry y objtype en owdd= 112)
                approvalList.AddRange(from mOWDD in mDbContext.OWDD
                                      join mODRF in mDbContext.ODRF on mOWDD.DraftEntry equals mODRF.DocEntry
                                      where
                                      (
                                          (pDocStatus == "" ? mOWDD.Status != null : mOWDD.Status == pDocStatus)
                                          && (mOWDD.UserSign == pUserSign)
                                          && (pDocDate == null ? mOWDD.CreateDate != null : mOWDD.CreateDate == pDocDate)
                                          && (mOWDD.DraftType == "112")
                                          && pObjectId == "" || (mODRF.ObjType == "22" || mODRF.ObjType == "540000006" ||
                                             mODRF.ObjType == "1470000113" || mODRF.ObjType == "18")
                                      )
                                      select new DocumentConfirmationLines()
                                      {
                                          CreateDate = mOWDD.CreateDate,
                                          CreateTime = mOWDD.CreateTime,
                                          //Remarks = mOWDD.Remarks,
                                          Status = mOWDD.Status,
                                          //StepCode = mWDD1.StepCode,
                                          //UpdateDate = mWDD1.UpdateDate,
                                          //UpdateTime = mWDD1.UpdateTime,
                                          //UserID = mWDD1.UserID,
                                          UserSign = mOWDD.UserSign,
                                          WddCode = mOWDD.WddCode,
                                          TaxDate = mODRF.TaxDate,
                                          ReqDate = mODRF.ReqDate,
                                          RequestorRemark = mOWDD.Remarks,
                                          ObjType = mODRF.ObjType,
                                          ODRFDocEntry = mODRF.DocEntry,
                                          DocTotal = mODRF.DocTotal,
                                          DocumentComments = mODRF.Comments,
                                          CardCode = mODRF.CardCode,
                                          CardName = mODRF.CardName
                                      });

                return approvalList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseApprovalByOriginator :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public List<DocumentConfirmationLines> GetSalesApprovalListSearch(CompanyConn pCompanyParam,
            int pUserSign, DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<DocumentConfirmationLines> approvalList = new List<DocumentConfirmationLines>();
                //lista con los documentos que necesitan aprobacion (docentry = docentry y objtype en owdd= 112)
                approvalList = (from mWDD1 in mDbContext.WDD1
                                join mOWDD in mDbContext.OWDD on mWDD1.WddCode equals mOWDD.WddCode
                                join mODRF in mDbContext.ODRF on mOWDD.DraftEntry equals mODRF.DocEntry
                                where
                                (
                                    (mWDD1.UserID == pUserSign)
                                    && (pDocStatus == "" ? mWDD1.Status != null : mWDD1.Status == pDocStatus)
                                    && (pDocDate == null ? mWDD1.CreateDate != null : mWDD1.CreateDate == pDocDate)
                                    && (mOWDD.DraftType == "112")
                                    && pObjectId == "" || (mODRF.ObjType == "23" ||
                                    mODRF.ObjType == "17" || mODRF.ObjType == "13")
                                )
                                select new DocumentConfirmationLines()
                                {
                                    CreateDate = mWDD1.CreateDate,
                                    CreateTime = mWDD1.CreateTime,
                                    Remarks = mWDD1.Remarks,
                                    Status = mWDD1.Status,
                                    StepCode = mWDD1.StepCode,
                                    UpdateDate = mWDD1.UpdateDate,
                                    UpdateTime = mWDD1.UpdateTime,
                                    UserID = mWDD1.UserID,
                                    UserSign = mWDD1.UserSign,
                                    WddCode = mWDD1.WddCode,
                                    TaxDate = mODRF.TaxDate,
                                    ReqDate = mODRF.ReqDate,
                                    RequestorRemark = mOWDD.Remarks,
                                    ObjType = mODRF.ObjType,
                                    ODRFDocEntry = mODRF.DocEntry,
                                    DocTotal = mODRF.DocTotal,
                                    DocumentComments = mODRF.Comments,
                                    CardCode = mODRF.CardCode,
                                    CardName = mODRF.CardName
                                }).ToList();

                //lista con los documentos que ya respondi su aprobacion (docentry = docnum)
                approvalList.AddRange((from mWDD1 in mDbContext.WDD1
                                       join mOWDD in mDbContext.OWDD on mWDD1.WddCode equals mOWDD.WddCode
                                       join mODRF in mDbContext.ODRF on mOWDD.DraftEntry equals mODRF.DocNum
                                       where
                                       (
                                           (mWDD1.UserID == pUserSign)
                                           && (pDocStatus == "" ? mWDD1.Status != null : mWDD1.Status == pDocStatus)
                                           && (pDocDate == null ? mWDD1.CreateDate != null : mWDD1.CreateDate == pDocDate)
                                           && (mOWDD.DraftType == mODRF.ObjType)
                                           && (mODRF.ObjType == "23" || mODRF.ObjType == "17")
                                       )
                                       select new DocumentConfirmationLines()
                                       {
                                           CreateDate = mWDD1.CreateDate,
                                           CreateTime = mWDD1.CreateTime,
                                           Remarks = mWDD1.Remarks,
                                           Status = mWDD1.Status,
                                           StepCode = mWDD1.StepCode,
                                           UpdateDate = mWDD1.UpdateDate,
                                           UpdateTime = mWDD1.UpdateTime,
                                           UserID = mWDD1.UserID,
                                           UserSign = mWDD1.UserSign,
                                           WddCode = mWDD1.WddCode,
                                           TaxDate = mODRF.TaxDate,
                                           ReqDate = mODRF.ReqDate,
                                           RequestorRemark = mOWDD.Remarks,
                                           ObjType = mODRF.ObjType,
                                           ODRFDocEntry = mODRF.DocEntry,
                                           DocTotal = mODRF.DocTotal,
                                           DocumentComments = mODRF.Comments,
                                           CardCode = mODRF.CardCode,
                                           CardName = mODRF.CardName
                                       }).ToList().GroupBy(x => x.WddCode).Select(g => g.First()));

                return approvalList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesApprovalListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDate"></param>
        /// <param name="pDocStatus"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetSalesApprovalByOriginator(CompanyConn pCompanyParam, int pUserSign,
            DateTime? pDocDate = null, string pDocStatus = "", string pObjectId = "")
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<DocumentConfirmationLines> approvalList = new List<DocumentConfirmationLines>();

                //lista con los documentos que necesitan aprobacion y origino el usuario actual (docentry = docentry y objtype en owdd= 112)
                approvalList.AddRange(from mOWDD in mDbContext.OWDD
                                      join mODRF in mDbContext.ODRF on mOWDD.DraftEntry equals mODRF.DocEntry
                                      where
                                      (
                                         (pDocStatus == "" ? mOWDD.Status != null : mOWDD.Status == pDocStatus)
                                          && (mOWDD.UserSign == pUserSign)
                                          && (pDocDate == null ? mOWDD.CreateDate != null : mOWDD.CreateDate == pDocDate)
                                          && (mOWDD.DraftType == "112")
                                          && (pObjectId == "" || (mODRF.ObjType == "23" || mODRF.ObjType == "17" || mODRF.ObjType == "13"))
                                      )
                                      select new DocumentConfirmationLines()
                                      {
                                          CreateDate = mOWDD.CreateDate,
                                          CreateTime = mOWDD.CreateTime,
                                          //Remarks = mOWDD.Remarks,
                                          Status = mOWDD.Status,
                                          //StepCode = mWDD1.StepCode,
                                          //UpdateDate = mWDD1.UpdateDate,
                                          //UpdateTime = mWDD1.UpdateTime,
                                          //UserID = mWDD1.UserID,
                                          UserSign = mOWDD.UserSign,
                                          WddCode = mOWDD.WddCode,
                                          TaxDate = mODRF.TaxDate,
                                          ReqDate = mODRF.ReqDate,
                                          RequestorRemark = mOWDD.Remarks,
                                          ObjType = mODRF.ObjType,
                                          ODRFDocEntry = mODRF.DocEntry,
                                          DocTotal = mODRF.DocTotal,
                                          DocumentComments = mODRF.Comments,
                                          CardCode = mODRF.CardCode,
                                          CardName = mODRF.CardName
                                      });

                return approvalList.OrderBy(c => c.WddCode).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesApprovalByOriginator :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pWddCode"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetApprovalListByID(CompanyConn pCompanyParam, int pWddCode)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<DocumentConfirmationLines, WDD1>();
                Mapper.CreateMap<WDD1, DocumentConfirmationLines>();

                List<DocumentConfirmationLines> listReturn = Mapper.Map<List<DocumentConfirmationLines>>(mDbContext.WDD1.Where(c => c.WddCode == pWddCode).ToList());

                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetApprovalListByID:" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pDocEntry"></param>
        /// <param name="pObjType"></param>
        /// <returns></returns>
        public List<DocumentConfirmationLines> GetApprovalListByDocumentId(CompanyConn pCompanyParam, int pDocEntry, string pObjType)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<DocumentConfirmationLines, WDD1>();
                Mapper.CreateMap<WDD1, DocumentConfirmationLines>();

                List<OWDD> listOWDD = mDbContext.OWDD.Where(c => c.DocEntry == pDocEntry && c.ObjType == pObjType).ToList();
                List<DocumentConfirmationLines> listReturn = new List<DocumentConfirmationLines>();
                foreach (OWDD aux in listOWDD)
                    listReturn.AddRange(Mapper.Map<List<DocumentConfirmationLines>>(mDbContext.WDD1.Where(c => c.WddCode == aux.WddCode).ToList()));

                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetApprovalListByDocumentId:" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public List<StageSAP> GetStageList(CompanyConn pCompanyParam)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<StageSAP, OWST>();
                Mapper.CreateMap<OWST, StageSAP>();

                List<StageSAP> listReturn = Mapper.Map<List<StageSAP>>(mDbContext.OWST.ToList());

                return listReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetStageList:" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owwdCode"></param>
        /// <param name="remark"></param>
        /// <param name="UserNameSAP"></param>
        /// <param name="UserPasswordSAP"></param>
        /// <param name="approvalCode"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string SaveApprovalResponse(int owwdCode, string remark, string UserNameSAP, string UserPasswordSAP, string approvalCode, CompanyConn pCompanyParam)
        {
            ApprovalRequestsServiceWeb.ApprovalRequest oApprovalRequest = new ApprovalRequestsServiceWeb.ApprovalRequest();
            ApprovalRequestsServiceWeb.ApprovalRequestParams oApprovalRequestParams = new ApprovalRequestsServiceWeb.ApprovalRequestParams();
            ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecision oApprovalRequestDecision = new ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecision();
            ApprovalRequestsServiceWeb.MsgHeader oApprovalRequestMsgHeader = new ApprovalRequestsServiceWeb.MsgHeader();

            oApprovalRequestMsgHeader.SessionID = pCompanyParam.DSSessionId;
            oApprovalRequestMsgHeader.ServiceName = ApprovalRequestsServiceWeb.MsgHeaderServiceName.ApprovalRequestsService;
            oApprovalRequestMsgHeader.ServiceNameSpecified = true;

            ApprovalRequestsServiceWeb.ApprovalRequestsService oApprovalRequestsService = new ApprovalRequestsServiceWeb.ApprovalRequestsService();

            oApprovalRequestsService.MsgHeaderValue = oApprovalRequestMsgHeader;

            //.- LINK HERE -.\\
            oApprovalRequestParams.Code = owwdCode;
            oApprovalRequestParams.CodeSpecified = true;

            //.- Get the approval request -.\\
            oApprovalRequest = oApprovalRequestsService.GetApprovalRequest(oApprovalRequestParams);

            //.- Set approval status -.\\
            switch (approvalCode)
            {
                case "W":
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardPending;
                    break;
                case "Y":
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardApproved;
                    break;
                case "N":
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardNotApproved;
                    break;
                default:
                    oApprovalRequestDecision.Status = ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecisionStatus.ardNotApproved;
                    break;

            }
            oApprovalRequestDecision.StatusSpecified = true;
            oApprovalRequestDecision.Remarks = remark;
            oApprovalRequestDecision.ApproverUserName = UserNameSAP;
            oApprovalRequestDecision.ApproverPassword = UserPasswordSAP;

            oApprovalRequest.ApprovalRequestDecisions = new ApprovalRequestsServiceWeb.ApprovalRequestApprovalRequestDecision[1];

            //.- Set the approval request statuts -.\\
            oApprovalRequest.ApprovalRequestDecisions.SetValue(oApprovalRequestDecision, 0);
            try
            {
                oApprovalRequestsService.UpdateRequest(oApprovalRequest);
                oApprovalRequest = oApprovalRequestsService.GetApprovalRequest(oApprovalRequestParams);

                if (oApprovalRequest.Status == ApprovalRequestsServiceWeb.ApprovalRequestStatus.arsApproved)
                {
                    DraftsServiceWeb.Document oDraftDocument = new DraftsServiceWeb.Document();
                    DraftsServiceWeb.DocumentParams oDraftDocumentParams = new DraftsServiceWeb.DocumentParams();
                    DraftsServiceWeb.MsgHeader oDraftDocumentMsgHeader = new DraftsServiceWeb.MsgHeader();



                    oDraftDocumentMsgHeader.ServiceName = DraftsServiceWeb.MsgHeaderServiceName.DraftsService;
                    oDraftDocumentMsgHeader.ServiceNameSpecified = true;
                    oDraftDocumentMsgHeader.SessionID = pCompanyParam.DSSessionId;

                    DraftsServiceWeb.DraftsService oDrafService = new DraftsServiceWeb.DraftsService();
                    oDrafService.MsgHeaderValue = oDraftDocumentMsgHeader;

                    //.- LINK HERE -.\\
                    oDraftDocumentParams.DocEntry = oApprovalRequest.ObjectEntry;
                    oDraftDocumentParams.DocEntrySpecified = true;

                    oDraftDocument = oDrafService.GetByParams(oDraftDocumentParams);
                    oDrafService.SaveDraftToDocument(oDraftDocument);
                }
                return "Ok";
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> SaveApprovalResponse:" + ex.Message);
                return "Error:" + ex.Message;
            }
        }

        #endregion


        #region Draft

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseUserDraftListSearch(CompanyConn pCompanyParam, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                IQueryable<DraftPortal> mDraftQueryable = null;
                List<DraftPortal> ListReturn = new List<DraftPortal>();

                if (pDocType == "" || pDocType == "22")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPOR.Where(c => c.UserSign == pUserSign && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }
                if (pDocType == "" || pDocType == "540000006")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPQT.Where(c => c.UserSign == pUserSign && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }
                if (pDocType == "" || pDocType == "1470000113")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPRQ.Where(c => c.UserSign == pUserSign && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }
                if (pDocType == "" || pDocType == "18")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPCH.Where(c => c.UserSign == pUserSign && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                ListReturn = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                int mTotalRecords = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).Count();

                mJsonObjectResult.DraftPortalList = ListReturn;
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseUserDraftListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSalesEmployee"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                IQueryable<DraftPortal> mDraftQueryable = null;
                List<DraftPortal> ListReturn = new List<DraftPortal>();

                if (pDocType == "" || pDocType == "22")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPOR.Where(c => c.SlpCode == pSalesEmployee && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "540000006")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPQT.Where(c => c.SlpCode == pSalesEmployee && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "1470000113")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPRQ.Where(c => c.SlpCode == pSalesEmployee && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "18")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPCH.Where(c => c.SlpCode == pSalesEmployee && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (!string.IsNullOrEmpty(pCodeVendor) ? c.CardCode.Contains(pCodeVendor) : true)).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                ListReturn = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                int mTotalRecords = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).Count();

                mJsonObjectResult.DraftPortalList = ListReturn;
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseSalesEmployeeDraftListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBP"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pReqDate"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, DateTime? pReqDate = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                IQueryable<DraftPortal> mDraftQueryable = null;
                List<DraftPortal> ListReturn = new List<DraftPortal>();

                if (pDocType == "" || pDocType == "22")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPOR.Where(c => c.CardCode == pBP && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "540000006")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPQT.Where(c => c.CardCode == pBP && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "1470000113")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPRQ.Where(c => c.CardCode == pBP && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "18")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OPCH.Where(c => c.CardCode == pBP && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (pReqDate == null ? true : c.ReqDate == pReqDate) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                ListReturn = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                int mTotalRecords = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).Count();

                mJsonObjectResult.DraftPortalList = ListReturn;
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseCustomerDraftListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pUserSign"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesUserDraftListSearch(CompanyConn pCompanyParam, int pUserSign, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                bool oOpenNotDelivered;

                IQueryable<DraftPortal> mDraftQueryable = null;
                List<DraftPortal> ListReturn = new List<DraftPortal>();

                if (pDocType == "" || pDocType == "17")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.ORDR.Where(c => c.UserSign == pUserSign && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "23")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OQUT.Where(c => c.UserSign == pUserSign && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "13")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OINV.Where(c => c.UserSign == pUserSign && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                ListReturn = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                int mTotalRecords = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).Count();

                mJsonObjectResult.DraftPortalList = ListReturn;
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                foreach (DraftPortal order in mJsonObjectResult.DraftPortalList)
                {
                    if (order.ObjType == "17")
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            List<RDR1> mLisRDR1 = Mapper.Map<List<RDR1>>(
                                mDbContext.RDR1.Where(c => c.DocEntry == order.DocEntry)
                                .ToList());

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && DateTime.Compare(line.ShipDate.Value.Date, DateTime.Now.Date) < 0)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }




                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesUserDraftListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSalesEmployee"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesSalesEmployeeDraftListSearch(CompanyConn pCompanyParam, int pSalesEmployee, DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null, string pDocType = "", string pDocStatus = "", string pCodeVendor = "", int? pDocNum = null, int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                bool oOpenNotDelivered;

                IQueryable<DraftPortal> mDraftQueryable = null;
                List<DraftPortal> ListReturn = new List<DraftPortal>();

                if (pDocType == "" || pDocType == "17")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.ORDR.Where(c => c.SlpCode == pSalesEmployee && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "23")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OQUT.Where(c => c.SlpCode == pSalesEmployee && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "13")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OINV.Where(c => c.SlpCode == pSalesEmployee && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                ListReturn = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                int mTotalRecords = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).Count();

                mJsonObjectResult.DraftPortalList = ListReturn;
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                foreach (DraftPortal order in mJsonObjectResult.DraftPortalList)
                {
                    if (order.ObjType == "17")
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            List<RDR1> mLisRDR1 = Mapper.Map<List<RDR1>>(
                                mDbContext.RDR1.Where(c => c.DocEntry == order.DocEntry)
                                .ToList());

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && DateTime.Compare(line.ShipDate.Value.Date, DateTime.Now.Date) < 0)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesSalesEmployeeDraftListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pBP"></param>
        /// <param name="pDocDateFrom"></param>
        /// <param name="pDocDateTo"></param>
        /// <param name="pDocType"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesCustomerDraftListSearch(CompanyConn pCompanyParam, string pBP,
            DateTime? pDocDateFrom = null, DateTime? pDocDateTo = null,
            string pDocType = "", string pDocStatus = "",
            string pCodeVendor = "", int? pDocNum = null,
            int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                bool oOpenNotDelivered;

                IQueryable<DraftPortal> mDraftQueryable = null;
                List<DraftPortal> ListReturn = new List<DraftPortal>();

                if (pDocType == "" || pDocType == "17")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.ORDR.Where(c => c.CardCode == pBP && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "23")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OQUT.Where(c => c.CardCode == pBP && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                if (pDocType == "" || pDocType == "13")
                {
                    IQueryable<DraftPortal> mQueryableAux = mDbContext.OINV.Where(c => c.CardCode == pBP && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDocDateFrom == null ? true : c.DocDate >= pDocDateFrom) && (pDocDateTo == null ? true : c.DocDate <= pDocDateTo) && (c.CardCode.Contains(pCodeVendor))).Select(c => new DraftPortal { DocEntry = c.DocEntry, DocNum = c.DocNum, DocDate = c.DocDate, ReqDate = c.ReqDate, ObjType = c.ObjType, DocStatus = c.DocStatus, PRDocStatus = string.Empty, PODocStatus = string.Empty, PQDocStatus = string.Empty, CardCode = c.CardCode, CardName = c.CardName, IsDraft = false });
                    mDraftQueryable = (mDraftQueryable == null ? mQueryableAux : mDraftQueryable.Union(mQueryableAux));
                }

                ListReturn = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                int mTotalRecords = mDraftQueryable.Where(c => (pDocStatus != "" ? c.DocStatus == pDocStatus : true)).Count();

                mJsonObjectResult.DraftPortalList = ListReturn;
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                foreach (DraftPortal order in mJsonObjectResult.DraftPortalList)
                {
                    if (order.ObjType == "17")
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            List<RDR1> mLisRDR1 = Mapper.Map<List<RDR1>>(
                                mDbContext.RDR1.Where(c => c.DocEntry == order.DocEntry)
                                .ToList());

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && DateTime.Compare(line.ShipDate.Value.Date, DateTime.Now.Date) < 0)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesCustomerDraftListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCode"></param>
        /// <returns></returns>
        public Draft GetDraftById(CompanyConn pCompanyParam, int pCode)
        {
            Draft ret = null;
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                #region Mapper
                Mapper.CreateMap<Draft, ODRF>();
                Mapper.CreateMap<ODRF, Draft>();
                Mapper.CreateMap<DraftLine, DRF1>();
                Mapper.CreateMap<DRF1, DraftLine>();
                Mapper.CreateMap<DocumentSettingsSAP, CINF>();
                Mapper.CreateMap<CINF, DocumentSettingsSAP>();
                #endregion

                ret = Mapper.Map<Draft>(mDbContext.ODRF.Where(c => c.DocEntry == pCode).SingleOrDefault());

                if (ret != null)
                {
                    ret.Lines = Mapper.Map<List<DraftLine>>(mDbContext.DRF1.Where(c => c.DocEntry == pCode).ToList());
                }
                else
                { ret = new Draft(); }

                ret.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mDbContext.CINF.FirstOrDefault());
                return ret;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetDraftById :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="docEntry"></param>
        /// <param name="pSession"></param>
        /// <returns></returns>
        public string CreateDocument(CompanyConn pCompanyParam, int docEntry, string pSession)
        {
            DraftsServiceWeb.Document oDraftDocument = new DraftsServiceWeb.Document();
            DraftsServiceWeb.DocumentParams oDraftDocumentParams = new DraftsServiceWeb.DocumentParams();
            DraftsServiceWeb.MsgHeader oDraftDocumentMsgHeader = new DraftsServiceWeb.MsgHeader();

            oDraftDocumentMsgHeader.ServiceName = DraftsServiceWeb.MsgHeaderServiceName.DraftsService;
            oDraftDocumentMsgHeader.ServiceNameSpecified = true;
            oDraftDocumentMsgHeader.SessionID = pSession;

            DraftsServiceWeb.DraftsService oDrafService = new DraftsServiceWeb.DraftsService();
            oDrafService.MsgHeaderValue = oDraftDocumentMsgHeader;

            //.- LINK HERE -.\\
            oDraftDocumentParams.DocEntry = docEntry;
            oDraftDocumentParams.DocEntrySpecified = true;

            try
            {
                oDraftDocument = oDrafService.GetByParams(oDraftDocumentParams);
                oDrafService.SaveDraftToDocument(oDraftDocument);
                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        #endregion


        #region Sales Order

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pCodeVendor"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetSalesOrderListSearch(CompanyConn pCompanyParam,
            bool pShowOpenQuantityStatus, string pCodeVendor = "",
            DateTime? pDate = null, int? pDocNum = null,
            string pDocStatus = "", string pOwnerCode = "",
            string pSECode = "", int pStart = 0, int pLength = 0,
            OrderColumn pOrderColumn = null)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<ORDR, SaleOrderSAP>(); }).CreateMapper();
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();
                bool oOpenNotDelivered;
                pCompanyParam.IdBranchSelect = pCompanyParam.IdBranchSelect ?? 0;

                int mSECode = Convert.ToInt32(pSECode == "" ? "0" : pSECode);
                int mOwnerCode = Convert.ToInt32(pOwnerCode == "" ? "0" : pOwnerCode);

                int mTotalRecords = mDbContext.ORDR.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true) && (pCompanyParam.IdBranchSelect > 0 ? c.BPLId == pCompanyParam.IdBranchSelect : true)).Count();
                List<ORDR> ListRestu = mDbContext.ORDR.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true) && (pCompanyParam.IdBranchSelect > 0 ? c.BPLId == pCompanyParam.IdBranchSelect : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();

                mJsonObjectResult.SalesOrderSAPList = mapper.Map<List<SaleOrderSAP>>(ListRestu);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                //
                if (pShowOpenQuantityStatus)
                {
                    foreach (SaleOrderSAP order in mJsonObjectResult.SalesOrderSAPList)
                    {
                        if (order.DocStatus == "C")
                        {
                            order.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            List<RDR1> mLisRDR1 = Mapper.Map<List<RDR1>>(
                                mDbContext.RDR1.Where(c => c.DocEntry == order.DocEntry)
                                .ToList());

                            if (mLisRDR1.Where(w => w.OpenQty == 0).Any())
                            {
                                order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                            }
                            else
                            {
                                oOpenNotDelivered = true;
                                foreach (var line in mLisRDR1)
                                {
                                    if (line.OpenQty > 0 && DateTime.Compare(line.ShipDate.Value.Date, DateTime.Now.Date) < 0)
                                    {
                                        order.OpenQuantityStatus = OpenQuantityStatus.Red;
                                        oOpenNotDelivered = false;
                                        break;
                                    }
                                }

                                if (oOpenNotDelivered)
                                {
                                    order.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                            }
                        }
                    }
                }

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesOredrListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult AddSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam)
        {
            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            SalesOrderServiceWeb.MsgHeader header = new SalesOrderServiceWeb.MsgHeader();
            header.ServiceName = SalesOrderServiceWeb.MsgHeaderServiceName.OrdersService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesOrderServiceWeb.OrdersService oOrderService = new SalesOrderServiceWeb.OrdersService();
            oOrderService.MsgHeaderValue = header;

            JsonObjectResult mJsonObjectResult = new JsonObjectResult();

            SaleOrderSAP mSOIni = new SaleOrderSAP();
            Mapper.CreateMap(typeof(SaleOrderSAP), typeof(SaleOrderSAP));
            Mapper.Map(pObject, mSOIni);

            SalesOrderServiceWeb.Document prDocument = new SalesOrderServiceWeb.Document();

            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;
            prDocument.ShipToCode = pObject.ShipToCode;
            prDocument.PayToCode = pObject.PayToCode;
            if (pCompanyParam.IdBranchSelect != null)
            {
                prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
                prDocument.BPL_IDAssignedToInvoiceSpecified = true;
            }

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;

            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            //prDocument.DocTotal = (double)pObject.DocTotal;

            SalesOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesOrderServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.SOAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.SOAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.SOAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.SOAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.SOAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.SOAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.SOAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.SOAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.SOAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.SOAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.SOAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.SOAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.SOAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.SOAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.SOAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.SOAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.SOAddress.CountyB;
            oDocumentAddress.BillToState = pObject.SOAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.SOAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.SOAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                SalesOrderServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense();

                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocEntry = expenseLine.BaseAbsEnt ?? 0;
                    oDocumentExpenses.BaseDocEntrySpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocLine = expenseLine.BaseLnNum ?? 0;
                    oDocumentExpenses.BaseDocLineSpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocType = expenseLine.BaseType ?? 0;
                    oDocumentExpenses.BaseDocTypeSpecified = true;
                }


                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }


                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                if (expenseLine.ExpnsCode != null)
                {
                    oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode ?? 0;
                    oDocumentExpenses.ExpenseCodeSpecified = true;
                }
                oDocumentExpenses.LineNum = expenseLine.LineNum;
                oDocumentExpenses.LineNumSpecified = true;
                oDocumentExpenses.Project = expenseLine.Project;
                oDocumentExpenses.Remarks = expenseLine.Comments;

                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            SalesOrderServiceWeb.DocumentDocumentLine[] newLines = new SalesOrderServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;
            foreach (SaleOrderSAPLine portalLine in pObject.Lines)
            {
                SalesOrderServiceWeb.DocumentDocumentLine line = new SalesOrderServiceWeb.DocumentDocumentLine();

                ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == portalLine.ItemCode).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                if (mOitm == null)
                {
                    mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                    mJsonObjectResult.ErrorMsg = "Invalid ItemCode";

                    return mJsonObjectResult;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                #region Line Freight

                line.DocumentLineAdditionalExpenses = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense[1];
                line.DocumentLineAdditionalExpenses[0] = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense();
                if (portalLine.Freight.Count > 0)
                {
                    if (portalLine.Freight[0].ExpnsCode.HasValue)
                    {
                        line.DocumentLineAdditionalExpenses[0].ExpenseCode = portalLine.Freight[0].ExpnsCode.Value;
                        line.DocumentLineAdditionalExpenses[0].ExpenseCodeSpecified = true;
                    }
                    if (portalLine.Freight[0].LineTotal.HasValue)
                    {
                        line.DocumentLineAdditionalExpenses[0].LineTotal = (double)portalLine.Freight[0].LineTotal.Value;
                        line.DocumentLineAdditionalExpenses[0].LineTotalSpecified = true;
                    }
                }

                #endregion Line Freight

                #region Line Batchs And Serials

                if (mOitm.ManBtchNum == "Y" && !string.IsNullOrEmpty(portalLine.SerialBatch))
                {
                    //line.BatchNumbers = portalLine.SerialBatch;
                    SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[] dBatchNumber = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[1];

                    dBatchNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber();

                    dBatchNumber[0].BaseLineNumber = portalLine.LineNum;

                    dBatchNumber[0].BaseLineNumberSpecified = true;

                    dBatchNumber[0].BatchNumber = portalLine.SerialBatch;

                    dBatchNumber[0].AddmisionDate = DateTime.Now;

                    dBatchNumber[0].AddmisionDateSpecified = true;

                    dBatchNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                    dBatchNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                    dBatchNumber[0].Notes = "";

                    dBatchNumber[0].Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);

                    dBatchNumber[0].QuantitySpecified = true;

                    line.BatchNumbers = dBatchNumber;

                }

                if (mOitm.ManSerNum == "Y")
                {

                    if (portalLine.Serials != null && portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count() > 0)
                    {
                        SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count()];
                        int i = 0;
                        foreach (Serial itemSerial in portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum))
                        {
                            OSRN mOSRN = mDbContext.OSRN.Where(c => c.DistNumber == itemSerial.DistNumber.Trim()).FirstOrDefault();
                            dSerialNumber[i] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();
                            dSerialNumber[i].BaseLineNumber = portalLine.LineNum;
                            dSerialNumber[i].BaseLineNumberSpecified = true;
                            dSerialNumber[i].Quantity = Convert.ToDouble(1);
                            dSerialNumber[i].QuantitySpecified = true;
                            dSerialNumber[i].SystemSerialNumber = mOSRN.SysNumber;
                            dSerialNumber[i].SystemSerialNumberSpecified = true;

                            i++;
                        }

                        line.SerialNumbers = dSerialNumber;

                    }

                }

                #endregion Line Batchs And Serials

                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.CostingCode = portalLine.OcrCode;
                line.CostingCode2 = portalLine.OcrCode2;
                line.TaxCode = portalLine.TaxCode;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }


                line.ProjectCode = portalLine.Project;
                line.FreeText = portalLine.FreeTxt;
                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.BaseEntry != null)
                {
                    line.BaseEntry = (long)portalLine.BaseEntry;
                    line.BaseEntrySpecified = true;
                }
                if (portalLine.BaseLine != null)
                {
                    line.BaseLine = (long)portalLine.BaseLine;
                    line.BaseLineSpecified = true;
                }
                if (portalLine.BaseType != null)
                {
                    line.BaseType = (long)portalLine.BaseType;
                    line.BaseTypeSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }
                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }
            prDocument.DocumentLines = new SalesOrderServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            try
            {
                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    SaleOrderSAPLine mAuxLine = mSOIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
                mJsonObjectResult.Code = mResponse == "" ? "0" : mResponse;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

                if (udfIdPayment != "")
                {
                    var idPayment = Convert.ToInt32(pObject.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

                    AddPaymentByOrder(pCompanyParam, pObject.PaymentMeanOrder, 17, Convert.ToInt32(idPayment), Convert.ToInt32(idPayment));
                }

                if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" encoding=""UTF-16""?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    SalesOrderServiceWeb.DocumentParams parametros = new SalesOrderServiceWeb.DocumentParams();
                    parametros.DocEntry = Convert.ToInt32(mJsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
                    parametros.DocEntrySpecified = true;

                    prDocument = oOrderService.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        prDocument.AttachmentEntrySpecified = true;
                    }
                    oOrderService.Update(prDocument);
                }

            }
            catch (Exception ex)
            {
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                mJsonObjectResult.ErrorMsg = ex.Message;
            }

            return mJsonObjectResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public string UpdateSaleOrder(SaleOrderSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                SalesOrderServiceWeb.MsgHeader header = new SalesOrderServiceWeb.MsgHeader();
                header.ServiceName = SalesOrderServiceWeb.MsgHeaderServiceName.OrdersService;
                header.ServiceNameSpecified = true;
                header.SessionID = pCompanyParam.DSSessionId;

                SalesOrderServiceWeb.OrdersService oSalesOrderService = new SalesOrderServiceWeb.OrdersService();
                oSalesOrderService.MsgHeaderValue = header;

                SalesOrderServiceWeb.DocumentParams parametros = new SalesOrderServiceWeb.DocumentParams();
                parametros.DocEntry = pObject.DocEntry;
                parametros.DocEntrySpecified = true;

                SaleOrderSAP mSOIni = new SaleOrderSAP();
                Mapper.CreateMap(typeof(SaleOrderSAP), typeof(SaleOrderSAP));
                Mapper.Map(pObject, mSOIni);

                SalesOrderServiceWeb.Document prDocument = oSalesOrderService.GetByParams(parametros);

                switch (pObject.DocType)
                {
                    case "I":
                        prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                        prDocument.DocTypeSpecified = true;
                        break;
                    case "S":
                        prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Service;
                        prDocument.DocTypeSpecified = true;
                        break;
                    default:
                        prDocument.DocType = SalesOrderServiceWeb.DocumentDocType.dDocument_Items;
                        prDocument.DocTypeSpecified = true;
                        break;
                }
                if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.DocDate = pObject.DocDate.Value;
                    prDocument.DocDateSpecified = true;
                }
                if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.DocDueDate = pObject.DocDueDate.Value;
                    prDocument.DocDueDateSpecified = true;
                }
                if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.TaxDate = pObject.TaxDate.Value;
                    prDocument.TaxDateSpecified = true;
                }
                if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.CancelDate = pObject.CancelDate.Value;
                    prDocument.CancelDateSpecified = true;
                }
                if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    prDocument.RequriedDate = pObject.ReqDate.Value;
                    prDocument.RequriedDateSpecified = true;
                }

                prDocument.CardCode = pObject.CardCode;
                prDocument.CardName = pObject.CardName;
                prDocument.Project = pObject.Project;
                prDocument.ShipToCode = pObject.ShipToCode;
                prDocument.PayToCode = pObject.PayToCode;


                switch (pObject.RevisionPo)
                {
                    case "Y":
                        prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tYES;
                        prDocument.RevisionPoSpecified = true;
                        break;
                    case "N":
                        prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                        prDocument.RevisionPoSpecified = true;
                        break;
                    default:
                        prDocument.RevisionPo = SalesOrderServiceWeb.DocumentRevisionPo.tNO;
                        prDocument.RevisionPoSpecified = true;
                        break;
                }
                switch (pObject.SummryType)
                {
                    case "N":
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                    case "I":
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByItems;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                    case "D":
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dByDocuments;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                    default:
                        prDocument.SummeryType = SalesOrderServiceWeb.DocumentSummeryType.dNoSummary;
                        prDocument.SummeryTypeSpecified = true;
                        break;
                }

                prDocument.NumAtCard = pObject.NumAtCard;
                prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                prDocument.SalesPersonCodeSpecified = true;

                if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
                {
                    prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                    prDocument.DocumentsOwnerSpecified = true;
                }
                prDocument.DocCurrency = pObject.DocCur;
                prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
                prDocument.DocRateSpecified = true;
                prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
                prDocument.ContactPersonCodeSpecified = true;
                prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
                prDocument.PaymentGroupCodeSpecified = true;
                prDocument.PaymentMethod = pObject.PeyMethod;

                if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
                {
                    prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                    prDocument.DiscountPercentSpecified = true;
                }
                prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
                prDocument.TransportationCodeSpecified = true;
                //prDocument.Address = pObject.Address;
                //prDocument.Address2 = pObject.Address2;
                prDocument.Comments = pObject.Comments;
                prDocument.JournalMemo = pObject.JrnlMemo;
                //prDocument.DocTotal = (double)pObject.DocTotal;

                SalesOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesOrderServiceWeb.DocumentAddressExtension();
                //Defino los campos del shipto
                oDocumentAddress.ShipToStreet = pObject.SOAddress.StreetS;
                oDocumentAddress.ShipToStreetNo = pObject.SOAddress.StreetNoS;
                oDocumentAddress.ShipToBlock = pObject.SOAddress.BlockS;
                oDocumentAddress.ShipToBuilding = pObject.SOAddress.BuildingS;
                oDocumentAddress.ShipToCity = pObject.SOAddress.CityS;
                oDocumentAddress.ShipToZipCode = pObject.SOAddress.ZipCodeS;
                oDocumentAddress.ShipToCounty = pObject.SOAddress.CountyS;
                oDocumentAddress.ShipToState = pObject.SOAddress.StateS;
                oDocumentAddress.ShipToCountry = pObject.SOAddress.CountryS;
                oDocumentAddress.ShipToGlobalLocationNumber = pObject.SOAddress.GlbLocNumS;
                //Defino los campos del billto
                oDocumentAddress.BillToStreet = pObject.SOAddress.StreetB;
                oDocumentAddress.BillToStreetNo = pObject.SOAddress.StreetNoB;
                oDocumentAddress.BillToBlock = pObject.SOAddress.BlockB;
                oDocumentAddress.BillToBuilding = pObject.SOAddress.BuildingB;
                oDocumentAddress.BillToCity = pObject.SOAddress.CityB;
                oDocumentAddress.BillToZipCode = pObject.SOAddress.ZipCodeB;
                oDocumentAddress.BillToCounty = pObject.SOAddress.CountyB;
                oDocumentAddress.BillToState = pObject.SOAddress.StateB;
                oDocumentAddress.BillToCountry = pObject.SOAddress.CountryB;
                oDocumentAddress.BillToGlobalLocationNumber = pObject.SOAddress.GlbLocNumB;

                prDocument.AddressExtension = oDocumentAddress;

                #region Document Freight
                //Agrego los Freights al documento
                SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
                int positionNewLineAdd = 0;
                foreach (Freight expenseLine in pObject.ListFreight)
                {
                    SalesOrderServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense();

                    if (expenseLine.BaseAbsEnt != null)
                    {
                        oDocumentExpenses.BaseDocEntry = expenseLine.BaseAbsEnt ?? 0;
                        oDocumentExpenses.BaseDocEntrySpecified = true;
                    }
                    if (expenseLine.BaseAbsEnt != null)
                    {
                        oDocumentExpenses.BaseDocLine = expenseLine.BaseLnNum ?? 0;
                        oDocumentExpenses.BaseDocLineSpecified = true;
                    }
                    if (expenseLine.BaseAbsEnt != null)
                    {
                        oDocumentExpenses.BaseDocType = expenseLine.BaseType ?? 0;
                        oDocumentExpenses.BaseDocTypeSpecified = true;
                    }


                    switch (expenseLine.DistrbMthd)
                    {
                        case "N":
                            oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                            oDocumentExpenses.DistributionMethodSpecified = true;
                            break;
                        case "Q":
                            oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                            oDocumentExpenses.DistributionMethodSpecified = true;
                            break;
                        case "V":
                            oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                            oDocumentExpenses.DistributionMethodSpecified = true;
                            break;
                        case "W":
                            oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                            oDocumentExpenses.DistributionMethodSpecified = true;
                            break;
                        case "E":
                            oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                            oDocumentExpenses.DistributionMethodSpecified = true;
                            break;
                        case "T":
                            oDocumentExpenses.DistributionMethod = SalesOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                            oDocumentExpenses.DistributionMethodSpecified = true;
                            break;
                    }


                    oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                    if (expenseLine.ExpnsCode != null)
                    {
                        oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode ?? 0;
                        oDocumentExpenses.ExpenseCodeSpecified = true;
                    }

                    if (expenseLine.LineTotal != null)
                    {
                        oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                        oDocumentExpenses.LineTotalSpecified = true;
                    }

                    oDocumentExpenses.LineNum = expenseLine.LineNum;
                    oDocumentExpenses.LineNumSpecified = true;
                    oDocumentExpenses.Project = expenseLine.Project;
                    oDocumentExpenses.Remarks = expenseLine.Comments;

                    oDocumentExpensesList[positionNewLineAdd] = oDocumentExpenses;
                    positionNewLineAdd += 1;

                }
                prDocument.DocumentAdditionalExpenses = new SalesOrderServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
                prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

                #endregion Document Freight

                #region Document Lines
                foreach (SalesOrderServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
                {
                    SaleOrderSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                    if (portalLine != null)
                    {
                        if (docLine.LineStatus != SalesOrderServiceWeb.DocumentDocumentLineLineStatus.bost_Close)
                        {
                            docLine.ItemCode = portalLine.ItemCode;
                            docLine.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                            docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                            docLine.QuantitySpecified = true;
                            docLine.SupplierCatNum = portalLine.SubCatNum;
                            docLine.AccountCode = portalLine.AcctCode;
                            docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                            docLine.PriceSpecified = true;
                            docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                            docLine.UnitPriceSpecified = true;
                            docLine.Currency = portalLine.Currency;
                            docLine.WarehouseCode = portalLine.WhsCode;
                            docLine.CostingCode = portalLine.OcrCode;
                            docLine.CostingCode2 = portalLine.OcrCode2;
                            docLine.TaxCode = portalLine.TaxCode;
                            docLine.ProjectCode = portalLine.Project;
                            docLine.FreeText = portalLine.FreeTxt;
                            docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                            docLine.SalesPersonCodeSpecified = true;
                            if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                            {
                                docLine.ShipDate = portalLine.ShipDate.Value;
                                docLine.ShipDateSpecified = true;
                            }
                            if (portalLine.AcctCode != null)
                            {
                                docLine.AccountCode = portalLine.AcctCode;
                            }

                            if (portalLine.DiscPrcnt.HasValue)
                            {
                                docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                                docLine.DiscountPercentSpecified = true;
                            }

                            if (portalLine.VatPrcnt.HasValue)
                            {
                                docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                                docLine.TaxPercentagePerRowSpecified = true;
                            }

                            ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == docLine.ItemCode).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                            if (mOitm == null)
                            {
                                return "Error: Invalid ItemCode";
                            }

                            if (!string.IsNullOrEmpty(portalLine.UomCode))
                            {
                                OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                                if (mOUOM != null)
                                {
                                    docLine.UoMCode = portalLine.UomCode;
                                    docLine.UoMEntry = mOUOM.UomEntry;
                                    docLine.UoMEntrySpecified = true;
                                }
                            }

                            #region Line Freight

                            if (docLine.DocumentLineAdditionalExpenses.Count() > 0)
                            {
                                if (portalLine.Freight[0].ExpnsCode.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCode = portalLine.Freight[0].ExpnsCode.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCodeSpecified = true;
                                }
                                if (portalLine.Freight[0].LineTotal.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotal = (double)portalLine.Freight[0].LineTotal.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotalSpecified = true;
                                }
                            }
                            else
                            {
                                docLine.DocumentLineAdditionalExpenses = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense[1];
                                docLine.DocumentLineAdditionalExpenses[0] = new SalesOrderServiceWeb.DocumentDocumentLineDocumentLineAdditionalExpense();
                                if (portalLine.Freight[0].ExpnsCode.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCode = portalLine.Freight[0].ExpnsCode.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].ExpenseCodeSpecified = true;
                                }
                                if (portalLine.Freight[0].LineTotal.HasValue)
                                {
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotal = (double)portalLine.Freight[0].LineTotal.Value;
                                    docLine.DocumentLineAdditionalExpenses[0].LineTotalSpecified = true;
                                }
                            }

                            #endregion Line Freight

                            #region Line Batchs And Serials

                            if (mOitm.ManBtchNum == "Y")
                            {
                                if (docLine.BatchNumbers.Count() > 0)
                                {
                                    if (!string.IsNullOrEmpty(portalLine.SerialBatch))
                                    {
                                        docLine.BatchNumbers.FirstOrDefault().BatchNumber = portalLine.SerialBatch;
                                        docLine.BatchNumbers.FirstOrDefault().InternalSerialNumber = portalLine.SerialBatch;
                                        docLine.BatchNumbers.FirstOrDefault().ManufacturerSerialNumber = portalLine.SerialBatch;
                                        docLine.BatchNumbers.FirstOrDefault().Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                                    }
                                    else
                                    {
                                        docLine.BatchNumbers = null;
                                    }

                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(portalLine.SerialBatch))
                                    {
                                        SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[] dBatchNumber = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[1];

                                        dBatchNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber();

                                        dBatchNumber[0].BaseLineNumber = portalLine.LineNum;

                                        dBatchNumber[0].BaseLineNumberSpecified = true;

                                        dBatchNumber[0].BatchNumber = portalLine.SerialBatch;

                                        dBatchNumber[0].AddmisionDate = DateTime.Now;

                                        dBatchNumber[0].AddmisionDateSpecified = true;

                                        dBatchNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                                        dBatchNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                                        dBatchNumber[0].Notes = "";

                                        dBatchNumber[0].Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);

                                        dBatchNumber[0].QuantitySpecified = true;

                                        docLine.BatchNumbers = dBatchNumber;
                                    }
                                }
                            }


                            if (mOitm.ManSerNum == "Y")
                            {

                                if (portalLine.Serials != null && portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count() > 0)
                                {
                                    SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count()];
                                    int i = 0;
                                    foreach (Serial itemSerial in portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum))
                                    {
                                        OSRN mOSRN = mDbContext.OSRN.Where(c => c.DistNumber == itemSerial.DistNumber.Trim()).FirstOrDefault();
                                        dSerialNumber[i] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();
                                        dSerialNumber[i].BaseLineNumber = portalLine.LineNum;
                                        dSerialNumber[i].BaseLineNumberSpecified = true;
                                        dSerialNumber[i].Quantity = Convert.ToDouble(1);
                                        dSerialNumber[i].QuantitySpecified = true;
                                        dSerialNumber[i].SystemSerialNumber = mOSRN.SysNumber;
                                        dSerialNumber[i].SystemSerialNumberSpecified = true;
                                        i++;
                                    }

                                    docLine.SerialNumbers = dSerialNumber;

                                }

                            }

                            #endregion Line Batchs And Serials

                            docLine.ShipToDescription = Miscellaneous.ReplaceUTF(docLine.ShipToDescription);
                        }
                        pObject.Lines.Remove(portalLine);
                    }
                    else
                    {
                        prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                    }
                }
                SalesOrderServiceWeb.DocumentDocumentLine[] newLines = new SalesOrderServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
                prDocument.DocumentLines.CopyTo(newLines, 0);
                int positionNewLine = prDocument.DocumentLines.Length;
                foreach (SaleOrderSAPLine portalLine in pObject.Lines)
                {
                    SalesOrderServiceWeb.DocumentDocumentLine line = new SalesOrderServiceWeb.DocumentDocumentLine();
                    line.LineNum = portalLine.LineNum;
                    line.LineNumSpecified = true;
                    line.ItemCode = portalLine.ItemCode;
                    //line.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                    line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    line.QuantitySpecified = true;
                    line.SupplierCatNum = portalLine.SubCatNum;
                    line.AccountCode = portalLine.AcctCode;
                    line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    line.PriceSpecified = true;
                    line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    line.UnitPriceSpecified = true;
                    line.Currency = portalLine.Currency;
                    line.WarehouseCode = portalLine.WhsCode;
                    line.CostingCode = portalLine.OcrCode;
                    line.CostingCode2 = portalLine.OcrCode2;
                    line.TaxCode = portalLine.TaxCode;
                    line.ProjectCode = portalLine.Project;
                    line.FreeText = portalLine.FreeTxt;
                    line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    line.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        line.ShipDate = portalLine.ShipDate.Value;
                        line.ShipDateSpecified = true;
                    }
                    if (portalLine.AcctCode != null)
                    {
                        line.AccountCode = portalLine.AcctCode;
                    }
                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        line.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        line.TaxPercentagePerRowSpecified = true;
                    }

                    ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == portalLine.ItemCode).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum }).FirstOrDefault();

                    if (!string.IsNullOrEmpty(portalLine.UomCode))
                    {
                        OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                        if (mOUOM != null)
                        {
                            line.UoMCode = portalLine.UomCode;
                            line.UoMEntry = mOUOM.UomEntry;
                            line.UoMEntrySpecified = true;
                        }
                    }

                    if (mOitm.ManBtchNum == "Y" && !string.IsNullOrEmpty(portalLine.SerialBatch))
                    {
                        SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[] dBatchNumber = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber[1];

                        dBatchNumber[0] = new SalesOrderServiceWeb.DocumentDocumentLineBatchNumber();

                        dBatchNumber[0].BaseLineNumber = portalLine.LineNum;

                        dBatchNumber[0].BaseLineNumberSpecified = true;

                        dBatchNumber[0].BatchNumber = portalLine.SerialBatch;

                        dBatchNumber[0].AddmisionDate = DateTime.Now;

                        dBatchNumber[0].AddmisionDateSpecified = true;

                        dBatchNumber[0].InternalSerialNumber = portalLine.SerialBatch;

                        dBatchNumber[0].ManufacturerSerialNumber = portalLine.SerialBatch;

                        dBatchNumber[0].Notes = "";

                        dBatchNumber[0].Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);

                        dBatchNumber[0].QuantitySpecified = true;

                        line.BatchNumbers = dBatchNumber;

                    }

                    if (mOitm.ManSerNum == "Y")
                    {

                        if (portalLine.Serials != null && portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count() > 0)
                        {
                            SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[] dSerialNumber = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber[portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum).Count()];
                            int i = 0;
                            foreach (Serial itemSerial in portalLine.Serials.Where(c => c.DocLine == portalLine.LineNum))
                            {
                                OSRN mOSRN = mDbContext.OSRN.Where(c => c.DistNumber == itemSerial.DistNumber.Trim()).FirstOrDefault();
                                dSerialNumber[i] = new SalesOrderServiceWeb.DocumentDocumentLineSerialNumber();
                                dSerialNumber[i].BaseLineNumber = portalLine.LineNum;
                                dSerialNumber[i].BaseLineNumberSpecified = true;
                                dSerialNumber[i].Quantity = Convert.ToDouble(1);
                                dSerialNumber[i].QuantitySpecified = true;
                                dSerialNumber[i].SystemSerialNumber = mOSRN.SysNumber;
                                dSerialNumber[i].SystemSerialNumberSpecified = true;

                                i++;
                            }

                            line.SerialNumbers = dSerialNumber;

                        }

                    }

                    newLines[positionNewLine] = line;
                    positionNewLine += 1;
                }
                prDocument.DocumentLines = new SalesOrderServiceWeb.DocumentDocumentLine[newLines.Length];
                prDocument.DocumentLines = newLines;
                #endregion Document Lines

                if (pObject.AttachmentList.Count <= 0)
                {
                    prDocument.AttachmentEntrySpecified = false;
                }

                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    SaleOrderSAPLine mAuxLine = mSOIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }


                mB1WSHandler.ProcessDoc(mXmlDoc);

                if (prDocument.AttachmentEntry != 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                }
                else
                {
                    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                    {
                        string AddCmd = string.Empty;
                        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                        XmlDocument mResultXML = new XmlDocument();
                        AddCmd = @"<?xml version=""1.0"" ?>" +
                                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                        {
                            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                        }

                        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                        prDocument = oSalesOrderService.GetByParams(parametros);
                        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                        {
                            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                            prDocument.AttachmentEntrySpecified = true;
                        }
                        oSalesOrderService.Update(prDocument);
                    }
                }

                var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

                if (udfIdPayment != "")
                {
                    var idPayment = Convert.ToInt32(pObject.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

                    AddPaymentByOrder(pCompanyParam, pObject.PaymentMeanOrder, 17, Convert.ToInt32(idPayment), Convert.ToInt32(idPayment));
                }

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pDocEntry"></param>
        /// <param name="pListUDFORDR"></param>
        /// <param name="pListUDFRDR1"></param>
        /// <param name="pListUDFCRD1"></param>
        /// <returns></returns>
        public SaleOrderSAP GetAllbySO(CompanyConn pCompanyParam, int pDocEntry,
            bool pShowOpenQuantity,
            List<UDF_ARGNS> pListUDFORDR = null,
            List<UDF_ARGNS> pListUDFRDR1 = null,
            List<UDF_ARGNS> pListUDFCRD1 = null)
        {
            Mapper.CreateMap<SaleOrderSAPLine, RDR1>();
            Mapper.CreateMap<RDR1, SaleOrderSAPLine>();

            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            Mapper.CreateMap<SaleOrderSAP, ORDR>();
            Mapper.CreateMap<ORDR, SaleOrderSAP>();

            Mapper.CreateMap<DocumentAddress, RDR12>();
            Mapper.CreateMap<RDR12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            Mapper.CreateMap<SaleOrderSAPLineFreight, RDR2>();
            Mapper.CreateMap<RDR2, SaleOrderSAPLineFreight>();

            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            SaleOrderSAP mSaleOrderSAP = new SaleOrderSAP();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();


            string mWhere = "[ORDR].[DocEntry] = " + pDocEntry;
            ORDR mORDR = UDFUtil.GetObjectListWithUDF(pListUDFORDR, mWhere, typeof(ORDR), mConnectionString, "ORDR").Cast<ORDR>().FirstOrDefault();

            if (mORDR != null)
            {

                mSaleOrderSAP = Mapper.Map<SaleOrderSAP>(mORDR);

                mSaleOrderSAP = FillSODocumentSAP(pCompanyParam, mSaleOrderSAP, mDbContext);

                //DocumentLines
                mWhere = "[RDR1].[DocEntry] = " + pDocEntry;
                List<RDR1> mListRDR1 = UDFUtil.GetObjectListWithUDF(pListUDFRDR1, mWhere, typeof(RDR1), mConnectionString, "RDR1").Cast<RDR1>().ToList();
                mSaleOrderSAP.Lines = Mapper.Map<List<SaleOrderSAPLine>>(mListRDR1);

                List<OITL> mListOITL = mDbContext.OITL.Where(c => c.DocEntry == pDocEntry && c.ApplyType == 17).ToList();

                mSaleOrderSAP.Lines = mSaleOrderSAP.Lines.Select(c => { c.GLAccount = mSaleOrderSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();

                foreach (SaleOrderSAPLine item in mSaleOrderSAP.Lines)
                {
                    ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == item.ItemCode).Select(c => new ItemMasterSAP { ManSerNum = c.ManSerNum, ManBtchNum = c.ManBtchNum, UgpEntry = c.UgpEntry }).FirstOrDefault();

                    item.Freight = Mapper.Map<List<SaleOrderSAPLineFreight>>(mDbContext.RDR2.Where(c => c.DocEntry == item.DocEntry && c.LineNum == item.LineNum && c.GroupNum == 0).ToList());

                    OUGP mOUGPAux = mDbContext.OUGP.Where(c => c.UgpEntry == mOitm.UgpEntry).FirstOrDefault();
                    if (mOUGPAux != null)
                    {
                        item.UomGroupName = mOUGPAux.UgpName;
                    }
                    item.UnitOfMeasureList = GetUOMLists(mOitm.UgpEntry);

                    int mLogEntry = mListOITL.Where(c => c.ItemCode == item.ItemCode && c.DocLine == item.LineNum).LastOrDefault() != null ? (mListOITL.Where(c => c.ItemCode == item.ItemCode && c.DocLine == item.LineNum).LastOrDefault().DocQty > 0 ? mListOITL.Where(c => c.ItemCode == item.ItemCode && c.DocLine == item.LineNum).LastOrDefault().LogEntry : 0) : 0;

                    int SysNum = 0;

                    if (mLogEntry != 0)
                    {
                        if (mDbContext.ITL1.Where(c => c.LogEntry == mLogEntry).FirstOrDefault() != null)
                        {
                            SysNum = mDbContext.ITL1.Where(c => c.LogEntry == mLogEntry).FirstOrDefault().SysNumber;

                            if (mOitm.ManBtchNum == "Y")
                            {
                                item.SerialBatch = mDbContext.OBTN.Where(c => c.SysNumber == SysNum).FirstOrDefault().DistNumber;
                            }
                            else
                            {
                                //item.SerialBatch = mDbContext.OSRN.Where(c => c.SysNumber == SysNum).FirstOrDefault().DistNumber;
                            }
                        }

                    }

                    ///Open Quantity Status
                    if (pShowOpenQuantity)
                    {
                        if (item.LineStatus == "C")
                        {
                            item.OpenQuantityStatus = OpenQuantityStatus.Green;
                        }
                        else
                        {
                            if (item.OpenQty == 0)
                            {
                                if (item.ShipDate.Value.Date < DateTime.Now.Date)
                                {
                                    item.OpenQuantityStatus = OpenQuantityStatus.Red;
                                }
                                else
                                {
                                    item.OpenQuantityStatus = OpenQuantityStatus.Green;
                                }
                            }
                            else
                            {
                                if (item.ShipDate.Value.Date < DateTime.Now.Date)
                                {
                                    item.OpenQuantityStatus = OpenQuantityStatus.Yellow;
                                }
                                else
                                {
                                    item.OpenQuantityStatus = OpenQuantityStatus.Green;
                                }
                            }
                        }
                    }

                    item.ManBtchNum = mOitm.ManBtchNum;
                    item.ManSerNum = mOitm.ManSerNum;
                }

                //DocumentAddress
                List<RDR12> mListRDR12 = mDbContext.RDR12.Where(c => c.DocEntry == pDocEntry).ToList();
                mSaleOrderSAP.SOAddress = Mapper.Map<DocumentAddress>(mListRDR12.FirstOrDefault());

                if (pListUDFCRD1 != null)
                {
                    if (!string.IsNullOrEmpty(mSaleOrderSAP.ShipToCode))
                    {
                        mWhere = "[CRD1].[Address] = '" + mSaleOrderSAP.ShipToCode + "' and [CRD1].[CardCode] = '" + mSaleOrderSAP.CardCode + "' and [CRD1].[AdresType] = 'S'";
                        CRD1 mCRD1ShipToAux = UDFUtil.GetObjectListWithUDF(pListUDFCRD1, mWhere, typeof(CRD1), mConnectionString, "CRD1").Cast<CRD1>().FirstOrDefault();
                        if (mCRD1ShipToAux != null)
                            mSaleOrderSAP.SOAddress.MappedUdfShipTo = mCRD1ShipToAux.MappedUdf;
                    }

                    if (!string.IsNullOrEmpty(mSaleOrderSAP.PayToCode))
                    {
                        mWhere = "[CRD1].[Address] = '" + mSaleOrderSAP.PayToCode + "' and [CRD1].[CardCode] = '" + mSaleOrderSAP.CardCode + "' and [CRD1].[AdresType] = 'B'";
                        CRD1 mCRD1ShipToAux = UDFUtil.GetObjectListWithUDF(pListUDFCRD1, mWhere, typeof(CRD1), mConnectionString, "CRD1").Cast<CRD1>().FirstOrDefault();
                        if (mCRD1ShipToAux != null)
                            mSaleOrderSAP.SOAddress.MappedUdfBillTo = mCRD1ShipToAux.MappedUdf;
                    }
                }

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (mORDR.OwnerCode ?? 0)).FirstOrDefault();
                mSaleOrderSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

                //Attachments
                List<ATC1> mATC1List = mDbContext.ATC1.Where(c => c.AbsEntry == mSaleOrderSAP.AtcEntry).ToList();
                mSaleOrderSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);

                mSaleOrderSAP.ListFreight = new List<Freight>();
                mSaleOrderSAP.ListFreight.Add(new Freight());
                mSaleOrderSAP.ListFreight.AddRange(Mapper.Map<List<Freight>>(mDbContext.OEXD.ToList()));
            }
            else
            {
                mSaleOrderSAP = FillSODocumentSAP(pCompanyParam, mSaleOrderSAP, mDbContext);

                //CompanyAddress
                List<ADM1> mListADM1 = mDbContext.ADM1.Where(c => c.Code == 1).ToList();
                mSaleOrderSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (0)).FirstOrDefault();
                mSaleOrderSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

                mSaleOrderSAP.ListFreight = new List<Freight>();
                mSaleOrderSAP.ListFreight.Add(new Freight());
                mSaleOrderSAP.ListFreight.AddRange(Mapper.Map<List<Freight>>(mDbContext.OEXD.ToList()));
            }

            List<PaymentMeanOrder> paymentMeanOrder = new List<PaymentMeanOrder>();

            try
            {
                var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

                if (udfIdPayment != "")
                {
                    var idPayment = Convert.ToInt32(mSaleOrderSAP.MappedUdf.Where(c => c.UDFName == udfIdPayment).FirstOrDefault().Value);

                    paymentMeanOrder = mDbContext.Database.SqlQuery<PaymentMeanOrder>($"SELECT * FROM [@ARGNS_POORDERPAYME] where U_DocType = {17} and U_DocEntryOrder = {idPayment}").ToList<PaymentMeanOrder>();
                }


            }
            catch (Exception)
            {
            }

            mSaleOrderSAP.PaymentMeanOrder = paymentMeanOrder;

            mDbContext.Dispose();

            return mSaleOrderSAP;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pSaleOrderSAP"></param>
        /// <param name="pDataBase"></param>
        /// <returns></returns>
        public SaleOrderSAP FillSODocumentSAP(CompanyConn pCompanyParam,
            SaleOrderSAP pSaleOrderSAP,
            DataBase pDataBase)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();

            Mapper.CreateMap<OCRY, CountrySAP>();
            Mapper.CreateMap<CountrySAP, OCRY>();

            List<OCRY> mListOCRY = mDbContext.OCRY.ToList();

            //Buyer
            List<OSLP> mLisOSLP = pDataBase.OSLP.ToList();

            //Currency
            List<OCRN> mListOCRN = pDataBase.OCRN.ToList();

            //PaymentMethod
            List<OCTG> mListOCTG = pDataBase.OCTG.ToList();

            //ShippingType
            List<OSHP> mListOSHP = pDataBase.OSHP.ToList();

            //Taxt
            List<OVTG> mListOVTG = pDataBase.OVTG.ToList();

            retCb.ListCountry = Mapper.Map<List<CountrySAP>>(mListOCRY);

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pSaleOrderSAP.CardCode))
            {
                List<PaymentMethod> mList = (from mCRD2 in mDbContext.CRD2
                                             join mOYPM in mDbContext.OPYM on mCRD2.PymCode equals mOYPM.PayMethCod
                                             where mCRD2.CardCode == pSaleOrderSAP.CardCode
                                             select new PaymentMethod()
                                             {
                                                 PayMethCod = mOYPM.PayMethCod,
                                                 Descript = mOYPM.Descript

                                             }).ToList();

                retCb.ListPaymentMethod.AddRange(mList);
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pSaleOrderSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = pDataBase.OACT.Where(c => c.FormatCode != null).ToList();
            pSaleOrderSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = pDataBase.CINF.ToList();
            pSaleOrderSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = pDataBase.OWHS.ToList();
            pSaleOrderSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pSaleOrderSAP.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);
            pSaleOrderSAP.SystemCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.SystemCurrency.ToString(), pCompanyParam.DSSessionId);

            //DistrbRule
            List<OOCR> mListOOCR = pDataBase.OOCR.Where(c => c.Active == "Y").ToList();
            pSaleOrderSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pSaleOrderSAP;
        }

        public List<Freight> GetSalesOrderFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            Mapper.CreateMap<OOCR, DistrRuleSAP>();
            Mapper.CreateMap<DistrRuleSAP, OOCR>();

            Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
            Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

            Mapper.CreateMap<OPRJ, ProjectSAP>();
            Mapper.CreateMap<ProjectSAP, OPRJ>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            List<RDR3> mListRDR3 = mDbContext.RDR3.ToList();

            List<OEXD> mListOEXD = mDbContext.OEXD.ToList();

            List<Freight> mListFreight = (from mRDR3 in mListRDR3
                                          join mOEXD in mListOEXD on mRDR3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mRDR3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mRDR3.DocEntry,
                                              ExpnsCode = mRDR3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mRDR3.LineTotal,
                                              TotalFrgn = mRDR3.TotalFrgn,
                                              Comments = mRDR3.Comments,
                                              ObjType = mRDR3.ObjType,
                                              DistrbMthd = mRDR3.DistrbMthd,
                                              VatSum = mRDR3.VatSum,
                                              TaxCode = mRDR3.TaxCode,
                                              LineNum = mRDR3.LineNum,
                                              Status = mRDR3.Status,
                                              OcrCode = mRDR3.OcrCode,
                                              TaxDistMtd = mRDR3.TaxDistMtd
                                          }).ToList();

            List<Freight> mListFreightOut = mListOEXD.Where(c => !(mListFreight.Select(d => d.ExpnsCode).ToArray().Contains(c.ExpnsCode))).Select(c => new Freight()
            {
                ExpnsCode = c.ExpnsCode,
                ExpnsName = c.ExpnsName,
                DistrbMthd = c.DistrbMthd,
                OcrCode = c.OcrCode,
                Status = "O"
            }).ToList();

            mListFreight = mListFreight.Union(mListFreightOut).ToList();


            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }


            List<OOCR> mListOOCR = mDbContext.OOCR.Where(c => c.Active == "Y").ToList();

            List<OSTC> mListOSTC = mDbContext.OSTC.ToList();

            List<OPRJ> mListOPRJ = mDbContext.OPRJ.ToList();

            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;

        }

        #endregion


        #region Purchase Order

        public List<Freight> GetPurchaseOrderFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            Mapper.CreateMap<OOCR, DistrRuleSAP>();
            Mapper.CreateMap<DistrRuleSAP, OOCR>();

            Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
            Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

            Mapper.CreateMap<OPRJ, ProjectSAP>();
            Mapper.CreateMap<ProjectSAP, OPRJ>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            List<POR3> mListPOR3 = mDbContext.POR3.ToList();

            List<OEXD> mListOEXD = mDbContext.OEXD.ToList();

            List<Freight> mListFreight = (from mPOR3 in mListPOR3
                                          join mOEXD in mListOEXD on mPOR3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mPOR3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mPOR3.DocEntry,
                                              ExpnsCode = mPOR3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mPOR3.LineTotal,
                                              TotalFrgn = mPOR3.TotalFrgn,
                                              Comments = mPOR3.Comments,
                                              ObjType = mPOR3.ObjType,
                                              DistrbMthd = mPOR3.DistrbMthd,
                                              VatSum = mPOR3.VatSum,
                                              TaxCode = mPOR3.TaxCode,
                                              LineNum = mPOR3.LineNum,
                                              Status = mPOR3.Status,
                                              OcrCode = mPOR3.OcrCode,
                                              TaxDistMtd = mPOR3.TaxDistMtd
                                          }).ToList();

            List<Freight> mListFreightOut = mListOEXD.Where(c => !(mListFreight.Select(d => d.ExpnsCode).ToArray().Contains(c.ExpnsCode))).Select(c => new Freight()
            {
                ExpnsCode = c.ExpnsCode,
                ExpnsName = c.ExpnsName,
                DistrbMthd = c.DistrbMthd,
                OcrCode = c.OcrCode,
                Status = "O"
            }).ToList();

            mListFreight = mListFreight.Union(mListFreightOut).ToList();

            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }


            List<OOCR> mListOOCR = mDbContext.OOCR.Where(c => c.Active == "Y").ToList();

            List<OSTC> mListOSTC = mDbContext.OSTC.ToList();

            List<OPRJ> mListOPRJ = mDbContext.OPRJ.ToList();

            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;
        }

        public PurchaseOrderSAP GetAllbyPO(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPOR = null, List<UDF_ARGNS> pListUDFPOR1 = null)
        {
            Mapper.CreateMap<PurchaseOrderSAPLine, POR1>();
            Mapper.CreateMap<POR1, PurchaseOrderSAPLine>();

            Mapper.CreateMap<PurchaseOrderSAP, OPOR>();
            Mapper.CreateMap<OPOR, PurchaseOrderSAP>();

            Mapper.CreateMap<DocumentAddress, POR12>();
            Mapper.CreateMap<POR12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            PurchaseOrderSAP mPurchaseOrderSAP = new PurchaseOrderSAP();
            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            string mWhere = "[OPOR].[DocEntry] = " + pDocEntry;
            OPOR mOPOR = UDFUtil.GetObjectListWithUDF(pListUDFOPOR, mWhere, typeof(OPOR), mConnectionString, "OPOR").Cast<OPOR>().FirstOrDefault();

            if (mOPOR != null)
            {
                mPurchaseOrderSAP = Mapper.Map<PurchaseOrderSAP>(mOPOR);

                mPurchaseOrderSAP = FillDocumentSAP(pCompanyParam, mPurchaseOrderSAP, mDbContext);

                //DocumentLines
                mWhere = "[POR1].[DocEntry] = " + pDocEntry;
                List<POR1> mLisPOR1 = UDFUtil.GetObjectListWithUDF(pListUDFPOR1, mWhere, typeof(POR1), mConnectionString, "POR1").Cast<POR1>().ToList();
                mPurchaseOrderSAP.Lines = Mapper.Map<List<PurchaseOrderSAPLine>>(mLisPOR1);

                mPurchaseOrderSAP.Lines = mPurchaseOrderSAP.Lines.Select(c => { c.GLAccount = mPurchaseOrderSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();


                foreach (PurchaseOrderSAPLine item in mPurchaseOrderSAP.Lines)
                {
                    ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == item.ItemCode).Select(c => new ItemMasterSAP { UgpEntry = c.UgpEntry }).FirstOrDefault();

                    item.UnitOfMeasureList = GetUOMLists(mOitm.UgpEntry);
                }


                //DocumentAddress
                List<POR12> mListPOR12 = mDbContext.POR12.Where(c => c.DocEntry == pDocEntry).ToList();
                mPurchaseOrderSAP.POAddress = Mapper.Map<DocumentAddress>(mListPOR12.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (mOPOR.OwnerCode ?? 0)).FirstOrDefault();
                mPurchaseOrderSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

                //Attachments
                List<ATC1> mATC1List = mDbContext.ATC1.Where(c => c.AbsEntry == mPurchaseOrderSAP.AtcEntry).ToList();
                mPurchaseOrderSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);
            }
            else
            {
                mPurchaseOrderSAP = FillDocumentSAP(pCompanyParam, mPurchaseOrderSAP, mDbContext);

                //CompanyAddress
                List<ADM1> mListADM1 = mDbContext.ADM1.Where(c => c.Code == 1).ToList();
                mPurchaseOrderSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (0)).FirstOrDefault();
                mPurchaseOrderSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

            }

            mDbContext.Dispose();

            return mPurchaseOrderSAP;

        }

        public JsonObjectResult GetPurchaseOrderListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<OPOR, PurchaseOrderSAP>(); }).CreateMapper();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                int mSECode = Convert.ToInt32(pSECode == "" ? "0" : pSECode);
                int mOwnerCode = Convert.ToInt32(pOwnerCode == "" ? "0" : pOwnerCode);
                pCompanyParam.IdBranchSelect = pCompanyParam.IdBranchSelect ?? 0;

                int mTotalRecords = mDbContext.OPOR.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true) && (pCompanyParam.IdBranchSelect > 0 ? c.BPLId == pCompanyParam.IdBranchSelect : true)).Count();
                List<OPOR> ListRestu = mDbContext.OPOR.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true) && (pCompanyParam.IdBranchSelect > 0 ? c.BPLId == pCompanyParam.IdBranchSelect : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.PurchaseOrderSAPList = mapper.Map<List<PurchaseOrderSAP>>(ListRestu);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseOrderListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public string AddPurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseOrderServiceWeb.MsgHeader header = new PurchaseOrderServiceWeb.MsgHeader();
            header.ServiceName = PurchaseOrderServiceWeb.MsgHeaderServiceName.PurchaseOrdersService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseOrderServiceWeb.PurchaseOrdersService oPurchaseOrderService = new PurchaseOrderServiceWeb.PurchaseOrdersService();
            oPurchaseOrderService.MsgHeaderValue = header;

            PurchaseOrderSAP mOPORIni = new PurchaseOrderSAP();
            Mapper.CreateMap(typeof(PurchaseOrderSAP), typeof(PurchaseOrderSAP));
            Mapper.Map(pObject, mOPORIni);

            PurchaseOrderServiceWeb.Document prDocument = new PurchaseOrderServiceWeb.Document();
            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;
            prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
            prDocument.BPL_IDAssignedToInvoiceSpecified = true;


            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseOrderServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.POAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.POAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.POAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.POAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.POAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.POAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.POAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.POAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.POAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.POAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.POAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.POAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.POAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.POAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.POAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.POAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.POAddress.CountyB;
            oDocumentAddress.BillToState = pObject.POAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.POAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.POAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense();
                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }
                oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode.Value;
                oDocumentExpenses.ExpenseCodeSpecified = true;
                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                oDocumentExpenses.Remarks = expenseLine.Comments;
                oDocumentExpenses.TaxCode = expenseLine.TaxCode;
                if (expenseLine.LineTotal != null)
                {
                    oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                    oDocumentExpenses.LineTotalSpecified = true;
                }
                oDocumentExpenses.Project = expenseLine.Project;
                //if (expenseLine.BaseAbsEnt != null)
                //{
                //    oDocumentExpenses.BaseDocType = (long)expenseLine.BaseType;
                //    oDocumentExpenses.BaseDocTypeSpecified = true;
                //    oDocumentExpenses.BaseDocLine = (long)expenseLine.BaseLnNum;
                //    oDocumentExpenses.BaseDocLineSpecified = true;
                //    oDocumentExpenses.BaseDocEntry = (long)expenseLine.BaseAbsEnt;
                //    oDocumentExpenses.BaseDocEntrySpecified = true;
                //}

                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            PurchaseOrderServiceWeb.DocumentDocumentLine[] newLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;
            foreach (PurchaseOrderSAPLine portalLine in pObject.Lines)
            {
                PurchaseOrderServiceWeb.DocumentDocumentLine line = new PurchaseOrderServiceWeb.DocumentDocumentLine();

                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;


                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.BaseEntry != null)
                {
                    line.BaseEntry = (long)portalLine.BaseEntry;
                    line.BaseEntrySpecified = true;
                }
                if (portalLine.BaseLine != null)
                {
                    line.BaseLine = (long)portalLine.BaseLine;
                    line.BaseLineSpecified = true;
                }
                if (portalLine.BaseType != null)
                {
                    line.BaseType = (long)portalLine.BaseType;
                    line.BaseTypeSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                //oPurchaseOrderService.Add(prDocument);

                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseOrderSAPLine mAuxLine = mOPORIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                mJsonObjectResult.Code = mResponse;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    PurchaseOrderServiceWeb.DocumentParams parametros = new PurchaseOrderServiceWeb.DocumentParams();
                    parametros.DocEntry = Convert.ToInt32(mJsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
                    parametros.DocEntrySpecified = true;

                    prDocument = oPurchaseOrderService.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        prDocument.AttachmentEntrySpecified = true;
                    }
                    oPurchaseOrderService.Update(prDocument);
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseOrder(PurchaseOrderSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseOrderServiceWeb.MsgHeader header = new PurchaseOrderServiceWeb.MsgHeader();
            header.ServiceName = PurchaseOrderServiceWeb.MsgHeaderServiceName.PurchaseOrdersService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseOrderServiceWeb.PurchaseOrdersService oPurchaseOrderService = new PurchaseOrderServiceWeb.PurchaseOrdersService();
            oPurchaseOrderService.MsgHeaderValue = header;

            PurchaseOrderServiceWeb.DocumentParams parametros = new PurchaseOrderServiceWeb.DocumentParams();
            parametros.DocEntry = pObject.DocEntry;
            parametros.DocEntrySpecified = true;

            PurchaseOrderServiceWeb.Document prDocument = oPurchaseOrderService.GetByParams(parametros);

            PurchaseOrderSAP mOPORIni = new PurchaseOrderSAP();
            Mapper.CreateMap(typeof(PurchaseOrderSAP), typeof(PurchaseOrderSAP));
            Mapper.Map(pObject, mOPORIni);

            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseOrderServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = pObject.CardName;
            prDocument.Project = pObject.Project;

            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseOrderServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseOrderServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseOrderServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseOrderServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.POAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.POAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.POAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.POAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.POAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.POAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.POAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.POAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.POAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.POAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.POAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.POAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.POAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.POAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.POAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.POAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.POAddress.CountyB;
            oDocumentAddress.BillToState = pObject.POAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.POAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.POAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLineAdd = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense();
                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }
                oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode.Value;
                oDocumentExpenses.ExpenseCodeSpecified = true;
                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                oDocumentExpenses.Remarks = expenseLine.Comments;
                oDocumentExpenses.TaxCode = expenseLine.TaxCode;
                if (expenseLine.LineTotal != null)
                {
                    oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                    oDocumentExpenses.LineTotalSpecified = true;
                }
                oDocumentExpenses.Project = expenseLine.Project;

                oDocumentExpensesList[positionNewLineAdd] = oDocumentExpenses;
                positionNewLineAdd += 1;

            }
            prDocument.DocumentAdditionalExpenses = new PurchaseOrderServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;


            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            foreach (PurchaseOrderServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                PurchaseOrderSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                    docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    docLine.QuantitySpecified = true;
                    docLine.SupplierCatNum = portalLine.SubCatNum;
                    docLine.AccountCode = portalLine.AcctCode;
                    docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    docLine.PriceSpecified = true;
                    docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    docLine.UnitPriceSpecified = true;
                    docLine.Currency = portalLine.Currency;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.TaxCode = portalLine.TaxCode;
                    docLine.ProjectCode = portalLine.Project;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.FreeText = portalLine.FreeTxt;
                    docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    docLine.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.ShipDate = portalLine.ShipDate.Value;
                        docLine.ShipDateSpecified = true;
                    }

                    if (!string.IsNullOrEmpty(portalLine.UomCode))
                    {
                        OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                        if (mOUOM != null)
                        {
                            docLine.UoMCode = portalLine.UomCode;
                            docLine.UoMEntry = mOUOM.UomEntry;
                            docLine.UoMEntrySpecified = true;
                        }
                    }

                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        docLine.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        docLine.TaxPercentagePerRowSpecified = true;
                    }

                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();

                }
            }
            PurchaseOrderServiceWeb.DocumentDocumentLine[] newLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (PurchaseOrderSAPLine portalLine in pObject.Lines)
            {
                PurchaseOrderServiceWeb.DocumentDocumentLine line = new PurchaseOrderServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new PurchaseOrderServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            if (pObject.AttachmentList.Count <= 0)
            {
                prDocument.AttachmentEntrySpecified = false;
            }
            try
            {
                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseOrderSAPLine mAuxLine = mOPORIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                if (prDocument.AttachmentEntry != 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                }
                else
                {
                    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                    {
                        string AddCmd = string.Empty;
                        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                        XmlDocument mResultXML = new XmlDocument();
                        AddCmd = @"<?xml version=""1.0"" ?>" +
                                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                        {
                            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                        }

                        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                        prDocument = oPurchaseOrderService.GetByParams(parametros);
                        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                        {
                            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                            prDocument.AttachmentEntrySpecified = true;
                        }
                        oPurchaseOrderService.Update(prDocument);
                    }
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }


        public PurchaseOrderSAP FillDocumentSAP(CompanyConn pCompanyParam, PurchaseOrderSAP pPurchaseOrderSAP, DataBase pDataBase)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();


            //Buyer
            List<OSLP> mLisOSLP = pDataBase.OSLP.ToList();

            //Currency
            List<OCRN> mListOCRN = pDataBase.OCRN.ToList();

            //PaymentMethod
            List<OCTG> mListOCTG = pDataBase.OCTG.ToList();

            //PaymentTerm
            // List<OPYM> mListOPYM = pDataBase.OPYM.ToList();

            //ShippingType
            List<OSHP> mListOSHP = pDataBase.OSHP.ToList();

            //Taxt
            List<OVTG> mListOVTG = pDataBase.OVTG.ToList();

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pPurchaseOrderSAP.CardCode))
            {
                List<PaymentMethod> mList = (from mCRD2 in mDbContext.CRD2
                                             join mOYPM in mDbContext.OPYM on mCRD2.PymCode equals mOYPM.PayMethCod
                                             where mCRD2.CardCode == pPurchaseOrderSAP.CardCode
                                             select new PaymentMethod()
                                             {
                                                 PayMethCod = mOYPM.PayMethCod,
                                                 Descript = mOYPM.Descript

                                             }).ToList();

                retCb.ListPaymentMethod.AddRange(mList);
            }
            //retCb.ListPaymentMethod = mListOPYM.Select(c => new PaymentMethod { PayMethCod = c.PayMethCod, Descript = c.Descript }).ToList(); 

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pPurchaseOrderSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = pDataBase.OACT.Where(c => c.FormatCode != null).ToList();
            pPurchaseOrderSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = pDataBase.CINF.ToList();
            pPurchaseOrderSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = pDataBase.OWHS.ToList();
            pPurchaseOrderSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pPurchaseOrderSAP.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);
            pPurchaseOrderSAP.SystemCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.SystemCurrency.ToString(), pCompanyParam.DSSessionId);

            //DistrbRule
            List<OOCR> mListOOCR = pDataBase.OOCR.Where(c => c.Active == "Y").ToList();
            pPurchaseOrderSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pPurchaseOrderSAP;
        }
        #endregion


        #region Purchase Quotation

        public List<Freight> GetPurchaseQuotationFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            Mapper.CreateMap<OOCR, DistrRuleSAP>();
            Mapper.CreateMap<DistrRuleSAP, OOCR>();

            Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
            Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

            Mapper.CreateMap<OPRJ, ProjectSAP>();
            Mapper.CreateMap<ProjectSAP, OPRJ>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            List<PQT3> mListPQT3 = mDbContext.PQT3.ToList();

            List<OEXD> mListOEXD = mDbContext.OEXD.ToList();

            List<Freight> mListFreight = (from mPQT3 in mListPQT3
                                          join mOEXD in mListOEXD on mPQT3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mPQT3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mPQT3.DocEntry,
                                              ExpnsCode = mPQT3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mPQT3.LineTotal,
                                              TotalFrgn = mPQT3.TotalFrgn,
                                              Comments = mPQT3.Comments,
                                              ObjType = mPQT3.ObjType,
                                              DistrbMthd = mPQT3.DistrbMthd,
                                              VatSum = mPQT3.VatSum,
                                              TaxCode = mPQT3.TaxCode,
                                              LineNum = mPQT3.LineNum,
                                              Status = mPQT3.Status,
                                              OcrCode = mPQT3.OcrCode,
                                              TaxDistMtd = mPQT3.TaxDistMtd
                                          }).ToList();

            List<Freight> mListFreightOut = mListOEXD.Where(c => !(mListFreight.Select(d => d.ExpnsCode).ToArray().Contains(c.ExpnsCode))).Select(c => new Freight()
            {
                ExpnsCode = c.ExpnsCode,
                ExpnsName = c.ExpnsName,
                DistrbMthd = c.DistrbMthd,
                OcrCode = c.OcrCode,
                Status = "O"
            }).ToList();

            mListFreight = mListFreight.Union(mListFreightOut).ToList();

            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }


            List<OOCR> mListOOCR = mDbContext.OOCR.Where(c => c.Active == "Y").ToList();

            List<OSTC> mListOSTC = mDbContext.OSTC.ToList();

            List<OPRJ> mListOPRJ = mDbContext.OPRJ.ToList();

            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;
        }

        public JsonObjectResult GetPurchaseQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                Mapper.CreateMap<OPQT, PurchaseQuotationSAP>();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                int mSECode = Convert.ToInt32(pSECode == "" ? "0" : pSECode);
                int mOwnerCode = Convert.ToInt32(pOwnerCode == "" ? "0" : pOwnerCode);

                int mTotalRecords = mDbContext.OPQT.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).Count();
                List<OPQT> ListRestu = Mapper.Map<List<OPQT>>(mDbContext.OPQT.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList());
                mJsonObjectResult.PurchaseQuotationSAPList = Mapper.Map<List<PurchaseQuotationSAP>>(ListRestu);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<PurchaseQuotationSAPLine> GetPurchaseQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {

                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<PQT1, PurchaseQuotationSAPLine>();
                Mapper.CreateMap<PurchaseQuotationSAPLine, PQT1>();

                //TODO la parte del where que hace el in la voy a hacer en C#
                List<PQT1> ListRestu = mDbContext.PQT1.Where(c => pDocuments.ToList().Contains(c.DocEntry.ToString()) && c.LineStatus == "O").ToList();

                return Mapper.Map<List<PurchaseQuotationSAPLine>>(ListRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseQuotationLinesSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public string AddPurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseQuotationServiceWeb.MsgHeader header = new PurchaseQuotationServiceWeb.MsgHeader();
            header.ServiceName = PurchaseQuotationServiceWeb.MsgHeaderServiceName.PurchaseQuotationsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseQuotationServiceWeb.PurchaseQuotationsService oPurchaseQuotationService = new PurchaseQuotationServiceWeb.PurchaseQuotationsService();
            oPurchaseQuotationService.MsgHeaderValue = header;

            PurchaseQuotationSAP mPQIni = new PurchaseQuotationSAP();
            Mapper.CreateMap(typeof(PurchaseQuotationSAP), typeof(PurchaseQuotationSAP));
            Mapper.Map(pObject, mPQIni);

            PurchaseQuotationServiceWeb.Document prDocument = new PurchaseQuotationServiceWeb.Document();
            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;

            prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
            prDocument.BPL_IDAssignedToInvoiceSpecified = true;


            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.PQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.PQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.PQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.PQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.PQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.PQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.PQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.PQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.PQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.PQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.PQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.PQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.PQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.PQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.PQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.PQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.PQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.PQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.PQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.PQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense();
                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }
                oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode.Value;
                oDocumentExpenses.ExpenseCodeSpecified = true;
                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                oDocumentExpenses.Remarks = expenseLine.Comments;
                oDocumentExpenses.TaxCode = expenseLine.TaxCode;

                if (expenseLine.LineTotal != null)
                {
                    oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                    oDocumentExpenses.LineTotalSpecified = true;
                }

                oDocumentExpenses.Project = expenseLine.Project;

                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            PurchaseQuotationServiceWeb.DocumentDocumentLine[] newLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;
            foreach (PurchaseQuotationSAPLine portalLine in pObject.Lines)
            {
                PurchaseQuotationServiceWeb.DocumentDocumentLine line = new PurchaseQuotationServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.PQTReqDate.HasValue && portalLine.PQTReqDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.RequiredDate = portalLine.PQTReqDate.Value;
                    line.RequiredDateSpecified = true;
                }
                if (portalLine.PQTReqQty.HasValue)
                {
                    line.RequiredQuantity = (double)portalLine.PQTReqQty.Value;
                    line.RequiredQuantitySpecified = true;
                }
                if (portalLine.BaseEntry != null)
                {
                    line.BaseEntry = (long)portalLine.BaseEntry;
                    line.BaseEntrySpecified = true;
                }
                if (portalLine.BaseLine != null)
                {
                    line.BaseLine = (long)portalLine.BaseLine;
                    line.BaseLineSpecified = true;
                }
                if (portalLine.BaseType != null)
                {
                    line.BaseType = (long)portalLine.BaseType;
                    line.BaseTypeSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                //oPurchaseQuotationService.Add(prDocument);

                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseQuotationSAPLine mAuxLine = mPQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                //mB1WSHandler.ProcessDoc(mXmlDoc);
                string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                mJsonObjectResult.Code = mResponse;
                mJsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    PurchaseQuotationServiceWeb.DocumentParams parametros = new PurchaseQuotationServiceWeb.DocumentParams();
                    parametros.DocEntry = Convert.ToInt32(mJsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
                    parametros.DocEntrySpecified = true;

                    prDocument = oPurchaseQuotationService.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        prDocument.AttachmentEntrySpecified = true;
                    }
                    oPurchaseQuotationService.Update(prDocument);
                }

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseQuotation(PurchaseQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseQuotationServiceWeb.MsgHeader header = new PurchaseQuotationServiceWeb.MsgHeader();
            header.ServiceName = PurchaseQuotationServiceWeb.MsgHeaderServiceName.PurchaseQuotationsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseQuotationServiceWeb.PurchaseQuotationsService oPurchaseQuotationService = new PurchaseQuotationServiceWeb.PurchaseQuotationsService();
            oPurchaseQuotationService.MsgHeaderValue = header;

            PurchaseQuotationServiceWeb.DocumentParams parametros = new PurchaseQuotationServiceWeb.DocumentParams();
            parametros.DocEntry = pObject.DocEntry;
            parametros.DocEntrySpecified = true;

            PurchaseQuotationSAP mPQIni = new PurchaseQuotationSAP();
            Mapper.CreateMap(typeof(PurchaseQuotationSAP), typeof(PurchaseQuotationSAP));
            Mapper.Map(pObject, mPQIni);

            PurchaseQuotationServiceWeb.Document prDocument = oPurchaseQuotationService.GetByParams(parametros);

            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = PurchaseQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }
            if (pObject.CancelDate.HasValue && pObject.CancelDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.CancelDate = pObject.CancelDate.Value;
                prDocument.CancelDateSpecified = true;
            }
            if (pObject.ReqDate.HasValue && pObject.ReqDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.RequriedDate = pObject.ReqDate.Value;
                prDocument.RequriedDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = pObject.CardName;
            prDocument.Project = pObject.Project;


            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = PurchaseQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = PurchaseQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            prDocument.Address = pObject.Address;
            prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            prDocument.DocTotal = (double)pObject.DocTotal;

            PurchaseQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new PurchaseQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.PQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.PQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.PQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.PQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.PQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.PQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.PQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.PQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.PQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.PQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.PQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.PQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.PQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.PQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.PQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.PQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.PQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.PQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.PQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.PQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //foreach (PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense expenseLine in prDocument.DocumentAdditionalExpenses)
            //{
            //    Freight portalExpenseLine = pObject.ListFreight.Where(c => c.LineNum == expenseLine.LineNum).FirstOrDefault();
            //    if (portalExpenseLine != null)
            //    {
            //        switch (portalExpenseLine.DistrbMthd)
            //        {
            //            case "N":
            //                expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "Q":
            //                expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "V":
            //                expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "W":
            //                expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "E":
            //                expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "T":
            //                expenseLine.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //        }
            //        expenseLine.ExpenseCode = portalExpenseLine.ExpnsCode.Value;
            //        expenseLine.ExpenseCodeSpecified = true;
            //        expenseLine.DistributionRule = portalExpenseLine.OcrCode;
            //        expenseLine.Remarks = portalExpenseLine.Comments;
            //        expenseLine.TaxCode = portalExpenseLine.TaxCode;
            //        expenseLine.LineTotal = (double)portalExpenseLine.LineTotal;
            //        expenseLine.LineTotalSpecified = true;
            //        expenseLine.Project = portalExpenseLine.Project;
            //    }

            //}

            //Agrego los Freights al documento
            PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLineAdd = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense();
                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }
                oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode.Value;
                oDocumentExpenses.ExpenseCodeSpecified = true;
                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                oDocumentExpenses.Remarks = expenseLine.Comments;
                oDocumentExpenses.TaxCode = expenseLine.TaxCode;

                if (expenseLine.LineTotal != null)
                {
                    oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                    oDocumentExpenses.LineTotalSpecified = true;
                }

                oDocumentExpenses.Project = expenseLine.Project;

                oDocumentExpensesList[positionNewLineAdd] = oDocumentExpenses;
                positionNewLineAdd += 1;

            }
            prDocument.DocumentAdditionalExpenses = new PurchaseQuotationServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            foreach (PurchaseQuotationServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                PurchaseQuotationSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                    docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    docLine.QuantitySpecified = true;
                    docLine.SupplierCatNum = portalLine.SubCatNum;
                    docLine.AccountCode = portalLine.AcctCode;
                    docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    docLine.PriceSpecified = true;
                    docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    docLine.UnitPriceSpecified = true;
                    docLine.Currency = portalLine.Currency;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.TaxCode = portalLine.TaxCode;
                    docLine.ProjectCode = portalLine.Project;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.FreeText = portalLine.FreeTxt;
                    docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    docLine.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.ShipDate = portalLine.ShipDate.Value;
                        docLine.ShipDateSpecified = true;
                    }
                    if (portalLine.PQTReqDate.HasValue && portalLine.PQTReqDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.RequiredDate = portalLine.PQTReqDate.Value;
                        docLine.RequiredDateSpecified = true;
                    }
                    if (portalLine.PQTReqQty.HasValue)
                    {
                        docLine.RequiredQuantity = (double)portalLine.PQTReqQty.Value;
                        docLine.RequiredQuantitySpecified = true;
                    }
                    if (portalLine.AcctCode != null)
                    {
                        docLine.AccountCode = portalLine.AcctCode;
                    }
                    if (!string.IsNullOrEmpty(portalLine.UomCode))
                    {
                        OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                        if (mOUOM != null)
                        {
                            docLine.UoMCode = portalLine.UomCode;
                            docLine.UoMEntry = mOUOM.UomEntry;
                            docLine.UoMEntrySpecified = true;
                        }
                    }
                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        docLine.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        docLine.TaxPercentagePerRowSpecified = true;
                    }
                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                }
            }
            PurchaseQuotationServiceWeb.DocumentDocumentLine[] newLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (PurchaseQuotationSAPLine portalLine in pObject.Lines)
            {
                PurchaseQuotationServiceWeb.DocumentDocumentLine line = new PurchaseQuotationServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.PQTReqDate.HasValue && portalLine.PQTReqDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.RequiredDate = portalLine.PQTReqDate.Value;
                    line.RequiredDateSpecified = true;
                }
                if (portalLine.PQTReqQty.HasValue)
                {
                    line.RequiredQuantity = (double)portalLine.PQTReqQty.Value;
                    line.RequiredQuantitySpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }
                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }
                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }
                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new PurchaseQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            if (pObject.AttachmentList.Count <= 0)
            {
                prDocument.AttachmentEntrySpecified = false;
            }

            try
            {
                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseQuotationSAPLine mAuxLine = mPQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                if (prDocument.AttachmentEntry != 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                }
                else
                {
                    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                    {
                        string AddCmd = string.Empty;
                        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                        XmlDocument mResultXML = new XmlDocument();
                        AddCmd = @"<?xml version=""1.0"" ?>" +
                                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                        {
                            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                        }

                        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                        prDocument = oPurchaseQuotationService.GetByParams(parametros);
                        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                        {
                            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                            prDocument.AttachmentEntrySpecified = true;
                        }
                        oPurchaseQuotationService.Update(prDocument);
                    }
                }

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public PurchaseQuotationSAP GetAllbyPQ(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPQT = null, List<UDF_ARGNS> pListUDFPQT1 = null)
        {
            Mapper.CreateMap<PurchaseQuotationSAPLine, PQT1>();
            Mapper.CreateMap<PQT1, PurchaseQuotationSAPLine>();

            Mapper.CreateMap<PurchaseQuotationSAP, OPQT>();
            Mapper.CreateMap<OPQT, PurchaseQuotationSAP>();

            Mapper.CreateMap<DocumentAddress, PQT12>();
            Mapper.CreateMap<PQT12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            PurchaseQuotationSAP mPQSAP = new PurchaseQuotationSAP();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            string mWhere = "[OPQT].[DocEntry] = " + pDocEntry;
            OPQT mOPQT = UDFUtil.GetObjectListWithUDF(pListUDFOPQT, mWhere, typeof(OPQT), mConnectionString, "OPQT").Cast<OPQT>().FirstOrDefault();

            if (mOPQT != null)
            {
                mPQSAP = Mapper.Map<PurchaseQuotationSAP>(mOPQT);

                mPQSAP = FillPQDocumentSAP(pCompanyParam, mPQSAP, mDbContext);

                //DocumentLines
                mWhere = "[PQT1].[DocEntry] = " + pDocEntry;
                List<PQT1> mListPQT1 = UDFUtil.GetObjectListWithUDF(pListUDFPQT1, mWhere, typeof(PQT1), mConnectionString, "PQT1").Cast<PQT1>().ToList();
                mPQSAP.Lines = Mapper.Map<List<PurchaseQuotationSAPLine>>(mListPQT1);

                mPQSAP.Lines = mPQSAP.Lines.Select(c => { c.GLAccount = mPQSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();
                foreach (PurchaseQuotationSAPLine item in mPQSAP.Lines)
                {
                    ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == item.ItemCode).Select(c => new ItemMasterSAP { UgpEntry = c.UgpEntry }).FirstOrDefault();

                    item.UnitOfMeasureList = GetUOMLists(mOitm.UgpEntry);
                }

                //DocumentAddress
                List<PQT12> mListPQT12 = mDbContext.PQT12.Where(c => c.DocEntry == pDocEntry).ToList();
                mPQSAP.PQAddress = Mapper.Map<DocumentAddress>(mListPQT12.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (mOPQT.OwnerCode ?? 0)).FirstOrDefault();
                mPQSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

                //Attachments
                List<ATC1> mATC1List = mDbContext.ATC1.Where(c => c.AbsEntry == mPQSAP.AtcEntry).ToList();
                mPQSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);
            }
            else
            {
                mPQSAP = FillPQDocumentSAP(pCompanyParam, mPQSAP, mDbContext);

                //CompanyAddress
                List<ADM1> mListADM1 = mDbContext.ADM1.Where(c => c.Code == 1).ToList();
                mPQSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (0)).FirstOrDefault();
                mPQSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);
            }


            mDbContext.Dispose();

            return mPQSAP;
        }

        public PurchaseQuotationSAP FillPQDocumentSAP(CompanyConn pCompanyParam, PurchaseQuotationSAP pPQSAP, DataBase pDataBase)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();


            //Buyer
            List<OSLP> mLisOSLP = pDataBase.OSLP.ToList();

            //Currency
            List<OCRN> mListOCRN = pDataBase.OCRN.ToList();

            //PaymentMethod
            List<OCTG> mListOCTG = pDataBase.OCTG.ToList();

            //PaymentTerm
            // List<OPYM> mListOPYM = pDataBase.OPYM.ToList();

            //ShippingType
            List<OSHP> mListOSHP = pDataBase.OSHP.ToList();

            //Taxt
            List<OVTG> mListOVTG = pDataBase.OVTG.ToList();

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pPQSAP.CardCode))
            {
                List<PaymentMethod> mList = (from mCRD2 in mDbContext.CRD2
                                             join mOYPM in mDbContext.OPYM on mCRD2.PymCode equals mOYPM.PayMethCod
                                             where mCRD2.CardCode == pPQSAP.CardCode
                                             select new PaymentMethod()
                                             {
                                                 PayMethCod = mOYPM.PayMethCod,
                                                 Descript = mOYPM.Descript

                                             }).ToList();

                retCb.ListPaymentMethod.AddRange(mList);
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pPQSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = pDataBase.OACT.Where(c => c.FormatCode != null).ToList();
            pPQSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = pDataBase.CINF.ToList();
            pPQSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = pDataBase.OWHS.ToList();
            pPQSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pPQSAP.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);
            pPQSAP.SystemCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.SystemCurrency.ToString(), pCompanyParam.DSSessionId);

            //DistrbRule
            List<OOCR> mListOOCR = pDataBase.OOCR.Where(c => c.Active == "Y").ToList();
            pPQSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pPQSAP;
        }

        #endregion


        #region Purchase Invoice

        public JsonObjectResult GetPurchaseInvoiceListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            Mapper.CreateMap<OPCH, PurchaseInvoiceSAP>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                int mSECode = Convert.ToInt32(pSECode == "" ? "0" : pSECode);
                int mOwnerCode = Convert.ToInt32(pOwnerCode == "" ? "0" : pOwnerCode);

                int mTotalRecords = mDbContext.OPCH.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).Count();
                List<OPCH> ListRestu = mDbContext.OPCH.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();
                mJsonObjectResult.PurchaseInvoiceSAPList = Mapper.Map<List<PurchaseInvoiceSAP>>(ListRestu);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseInvoiceListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public PurchaseInvoiceSAP GetAllbyPI(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOPCH = null, List<UDF_ARGNS> pListUDFPCH1 = null)
        {
            Mapper.CreateMap<PurchaseInvoiceLineSAP, PCH1>();
            Mapper.CreateMap<PCH1, PurchaseInvoiceLineSAP>();

            Mapper.CreateMap<PurchaseInvoiceSAP, OPCH>();
            Mapper.CreateMap<OPCH, PurchaseInvoiceSAP>();

            Mapper.CreateMap<DocumentAddress, PCH12>();
            Mapper.CreateMap<PCH12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            PurchaseInvoiceSAP mPISAP = new PurchaseInvoiceSAP();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            string mWhere = "[OPCH].[DocEntry] = " + pDocEntry;
            OPCH mOPCH = UDFUtil.GetObjectListWithUDF(pListUDFOPCH, mWhere, typeof(OPCH), mConnectionString, "OPCH").Cast<OPCH>().FirstOrDefault();

            if (mOPCH != null)
            {
                mPISAP = Mapper.Map<PurchaseInvoiceSAP>(mOPCH);

                mPISAP = FillPIDocumentSAP(pCompanyParam, mPISAP, mDbContext);

                //DocumentLines
                mWhere = "[PCH1].[DocEntry] = " + pDocEntry;
                List<PCH1> mListPCH1 = UDFUtil.GetObjectListWithUDF(pListUDFPCH1, mWhere, typeof(PCH1), mConnectionString, "PCH1").Cast<PCH1>().ToList();
                mPISAP.Lines = Mapper.Map<List<PurchaseInvoiceLineSAP>>(mListPCH1);

                //DocumentAddress
                List<PCH12> mListPCH12 = mDbContext.PCH12.Where(c => c.DocEntry == pDocEntry).ToList();
                mPISAP.PIAddress = Mapper.Map<DocumentAddress>(mListPCH12.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (mOPCH.OwnerCode ?? 0)).FirstOrDefault();
                mPISAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);
            }
            else
            {
                mPISAP = FillPIDocumentSAP(pCompanyParam, mPISAP, mDbContext);

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (0)).FirstOrDefault();
                mPISAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

            }


            mDbContext.Dispose();

            return mPISAP;
        }

        public PurchaseInvoiceSAP FillPIDocumentSAP(CompanyConn pCompanyParam, PurchaseInvoiceSAP pPISAP, DataBase pDataBase)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();


            //Buyer
            List<OSLP> mLisOSLP = pDataBase.OSLP.ToList();

            //Currency
            List<OCRN> mListOCRN = pDataBase.OCRN.ToList();

            //PaymentTerm
            List<OCTG> mListOCTG = pDataBase.OCTG.ToList();

            //PaymentMethod
            //List<OPYM> mListOPYM = pDataBase.OPYM.ToList();

            //ShippingType
            List<OSHP> mListOSHP = pDataBase.OSHP.ToList();

            //Taxt
            List<OVTG> mListOVTG = pDataBase.OVTG.ToList();

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pPISAP.CardCode))
            {
                List<PaymentMethod> mList = (from mCRD2 in mDbContext.CRD2
                                             join mOYPM in mDbContext.OPYM on mCRD2.PymCode equals mOYPM.PayMethCod
                                             where mCRD2.CardCode == pPISAP.CardCode
                                             select new PaymentMethod()
                                             {
                                                 PayMethCod = mOYPM.PayMethCod,
                                                 Descript = mOYPM.Descript

                                             }).ToList();

                retCb.ListPaymentMethod.AddRange(mList);
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pPISAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = pDataBase.OACT.Where(c => c.FormatCode != null).ToList();
            pPISAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //Warehouse
            List<OWHS> mListOWHS = pDataBase.OWHS.ToList();
            pPISAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pPISAP.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);
            pPISAP.SystemCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.SystemCurrency.ToString(), pCompanyParam.DSSessionId);

            //DistrbRule
            List<OOCR> mListOOCR = pDataBase.OOCR.Where(c => c.Active == "Y").ToList();
            pPISAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pPISAP;
        }

        #endregion


        #region Purchase Request

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pRequest"></param>
        /// <param name="pDate"></param>
        /// <param name="pDocNum"></param>
        /// <param name="pDepartment"></param>
        /// <param name="pDocStatus"></param>
        /// <param name="pOwnerCode"></param>
        /// <param name="pSECode"></param>
        /// <param name="pStart"></param>
        /// <param name="pLength"></param>
        /// <param name="pOrderColumn"></param>
        /// <returns></returns>
        public JsonObjectResult GetPurchaseRequestListSearch(CompanyConn pCompanyParam,
            string pRequest = "", DateTime? pDate = null, int? pDocNum = null,
            int? pDepartment = null, string pDocStatus = "", string pOwnerCode = "",
            string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            Mapper.CreateMap<OPRQ, PurchaseRequestSAP>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();
            try
            {
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                int mSECode = Convert.ToInt32(pSECode == "" ? "0" : pSECode);
                int mOwnerCode = Convert.ToInt32(pOwnerCode == "" ? "0" : pOwnerCode);

                int mTotalRecords = mDbContext.OPRQ.Where(c => (!string.IsNullOrEmpty(pRequest) ? c.Requester.Contains(pRequest) : true) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDepartment == null ? c.Department != null : c.Department == pDepartment) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).Count();
                List<OPRQ> ListRestu = Mapper.Map<List<OPRQ>>(mDbContext.OPRQ.Where(c => (!string.IsNullOrEmpty(pRequest) ? c.Requester.Contains(pRequest) : true) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDepartment == null ? c.Department != null : c.Department == pDepartment) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList());
                mJsonObjectResult.PurchaseRequestSAPList = Mapper.Map<List<PurchaseRequestSAP>>(ListRestu);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());

                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseRequestListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        public List<PurchaseRequestLineSAP> GetPurchaseRequestLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<PRQ1, PurchaseRequestLineSAP>();
                Mapper.CreateMap<PurchaseRequestLineSAP, PRQ1>();

                List<PRQ1> ListRestu = mDbContext.PRQ1.Where(c => pDocuments.ToList().Contains(c.DocEntry.ToString()) && c.LineStatus == "O").ToList();

                return Mapper.Map<List<PurchaseRequestLineSAP>>(ListRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseRequestLinesSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public PurchaseRequestCombo GetPurchaseRequestCombo(CompanyConn pCompanyParam)
        {
            PurchaseRequestCombo mPurchaseRequestCombo = new PurchaseRequestCombo();

            Mapper.CreateMap<OCRN, CurrencySAP>();
            Mapper.CreateMap<CurrencySAP, OCRN>();

            Mapper.CreateMap<OUDP, Department>();
            Mapper.CreateMap<Department, OUDP>();

            Mapper.CreateMap<OUBR, Branch>();
            Mapper.CreateMap<Branch, OUBR>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            List<OCRN> ListOCRN = mDbContext.OCRN.ToList();
            List<OUDP> ListOUDP = mDbContext.OUDP.ToList();
            List<OUBR> ListOUBR = mDbContext.OUBR.ToList();

            mPurchaseRequestCombo.CurrencyList = Mapper.Map<List<CurrencySAP>>(ListOCRN);
            mPurchaseRequestCombo.DepartmentList = Mapper.Map<List<Department>>(ListOUDP);
            mPurchaseRequestCombo.BranchList = Mapper.Map<List<Branch>>(ListOUBR);

            return mPurchaseRequestCombo;
        }

        public List<Freight> GetPurchaseRequestFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            try
            {
                Mapper.CreateMap<OEXD, Freight>();
                Mapper.CreateMap<Freight, OEXD>();

                Mapper.CreateMap<OOCR, DistrRuleSAP>();
                Mapper.CreateMap<DistrRuleSAP, OOCR>();

                Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
                Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

                Mapper.CreateMap<OPRJ, ProjectSAP>();
                Mapper.CreateMap<ProjectSAP, OPRJ>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<PRQ3> mListPRQ3 = mDbContext.PRQ3.ToList();

                List<OEXD> mListOEXD = mDbContext.OEXD.ToList();

                List<Freight> mListFreight = (from mPRQ3 in mListPRQ3
                                              join mOEXD in mListOEXD on mPRQ3.ExpnsCode equals mOEXD.ExpnsCode
                                              where
                                              (
                                                  mPRQ3.DocEntry == pDocEntry
                                              )
                                              select new Freight()
                                              {
                                                  DocEntry = mPRQ3.DocEntry,
                                                  ExpnsCode = mPRQ3.ExpnsCode,
                                                  ExpnsName = mOEXD.ExpnsName,
                                                  LineTotal = mPRQ3.LineTotal,
                                                  TotalFrgn = mPRQ3.TotalFrgn,
                                                  Comments = mPRQ3.Comments,
                                                  ObjType = mPRQ3.ObjType,
                                                  DistrbMthd = mPRQ3.DistrbMthd,
                                                  VatSum = mPRQ3.VatSum,
                                                  TaxCode = mPRQ3.TaxCode,
                                                  LineNum = mPRQ3.LineNum,
                                                  Status = mPRQ3.Status,
                                                  OcrCode = mPRQ3.OcrCode,
                                                  TaxDistMtd = mPRQ3.TaxDistMtd
                                              }).ToList();


                if (mListFreight.Count == 0)
                {
                    mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                    mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
                }


                List<OOCR> mListOOCR = mDbContext.OOCR.Where(c => c.Active == "Y").ToList();

                List<OSTC> mListOSTC = mDbContext.OSTC.ToList();

                List<OPRJ> mListOPRJ = mDbContext.OPRJ.ToList();

                mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

                return mListFreight;

            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetPurchaseRequestFreights :" + ex.Message);
                return null;
            }
        }

        public PurchaseRequestSAP GetAllbyPR(CompanyConn pCompanyParam, int pCode, List<UDF_ARGNS> pListUDFOPRQ = null, List<UDF_ARGNS> pListUDFPRQ1 = null)
        {
            PurchaseRequestSAP mPRSAP;

            Mapper.CreateMap<PurchaseRequestLineSAP, PRQ1>();
            Mapper.CreateMap<PRQ1, PurchaseRequestLineSAP>();

            Mapper.CreateMap<PurchaseRequestSAP, OPRQ>();
            Mapper.CreateMap<OPRQ, PurchaseRequestSAP>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            string mWhere = "[OPRQ].[DocEntry] = " + pCode;
            OPRQ mOPRQ = UDFUtil.GetObjectListWithUDF(pListUDFOPRQ, mWhere, typeof(OPRQ), mConnectionString, "OPRQ").Cast<OPRQ>().FirstOrDefault();

            //GLAccount
            List<OACT> mListOACT = mDbContext.OACT.ToList();
            List<GLAccountSAP> listAuxGLA = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            if (mOPRQ != null)
            {
                mPRSAP = Mapper.Map<PurchaseRequestSAP>(mOPRQ);

                //DocumentLines
                mWhere = "[PRQ1].[DocEntry] = " + pCode;
                List<PRQ1> mListPRQ1 = UDFUtil.GetObjectListWithUDF(pListUDFPRQ1, mWhere, typeof(PRQ1), mConnectionString, "PRQ1").Cast<PRQ1>().ToList();
                mPRSAP.Lines = Mapper.Map<List<PurchaseRequestLineSAP>>(mListPRQ1);

                mPRSAP.Lines = mPRSAP.Lines.Select(c => { c.GLAccount = listAuxGLA.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();
            }
            else
            {
                mPRSAP = new PurchaseRequestSAP();
            }

            CINF mCINF = mDbContext.CINF.FirstOrDefault();

            if (mCINF != null)
            { mPRSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mCINF); }

            return mPRSAP;
        }

        public string AddPurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseRequestServiceWeb.MsgHeader header = new PurchaseRequestServiceWeb.MsgHeader();
            header.ServiceName = PurchaseRequestServiceWeb.MsgHeaderServiceName.PurchaseRequestService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseRequestServiceWeb.PurchaseRequestService oPurchaseRequestService = new PurchaseRequestServiceWeb.PurchaseRequestService();
            oPurchaseRequestService.MsgHeaderValue = header;

            PurchaseRequestSAP mPRIni = new PurchaseRequestSAP();
            Mapper.CreateMap(typeof(PurchaseRequestSAP), typeof(PurchaseRequestSAP));
            Mapper.Map(pObject, mPRIni);

            PurchaseRequestServiceWeb.Document prDocument = new PurchaseRequestServiceWeb.Document();
            prDocument.Requester = pObject.Requester;
            prDocument.ReqType = (long)pObject.ReqType;
            prDocument.ReqTypeSpecified = true;
            prDocument.RequesterBranch = (long)pObject.Branch;
            prDocument.RequesterBranchSpecified = true;
            prDocument.RequesterDepartment = (long)pObject.Department;
            prDocument.RequesterDepartmentSpecified = true;
            prDocument.SalesPersonCode = -1;
            prDocument.SalesPersonCodeSpecified = true;
            prDocument.DocDate = pObject.DocDate.Value;
            prDocument.DocDateSpecified = true;
            prDocument.DocDueDate = pObject.DocDueDate.Value;
            prDocument.DocDueDateSpecified = true;
            prDocument.TaxDate = pObject.TaxDate.Value;
            prDocument.TaxDateSpecified = true;
            prDocument.RequriedDate = pObject.ReqDate.Value;
            prDocument.RequriedDateSpecified = true;
            prDocument.Comments = pObject.Comments;

            prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
            prDocument.BPL_IDAssignedToInvoiceSpecified = true;

            //Creo las lineas del documento
            PurchaseRequestServiceWeb.DocumentDocumentLine[] newLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            int positionNewLine = 0;

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            foreach (PurchaseRequestLineSAP portalLine in pObject.Lines)
            {
                PurchaseRequestServiceWeb.DocumentDocumentLine line = new PurchaseRequestServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                if (portalLine.Quantity != null)
                {
                    line.Quantity = (double)portalLine.Quantity;
                    line.QuantitySpecified = true;
                }
                if (portalLine.PriceBefDi != null)
                {
                    line.UnitPrice = (double)portalLine.PriceBefDi;
                    line.UnitPriceSpecified = true;
                }
                line.Currency = portalLine.Currency;
                line.RequiredDate = (portalLine.PQTReqDate == null ? prDocument.RequriedDate : portalLine.PQTReqDate.Value);
                line.RequiredDateSpecified = true;
                line.SalesPersonCode = -1;
                line.SalesPersonCodeSpecified = true;
                line.WarehouseCode = portalLine.WhsCode;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                //oPurchaseRequestService.Add(prDocument);

                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseRequestLineSAP mAuxLine = mPRIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            PurchaseRequestServiceWeb.MsgHeader header = new PurchaseRequestServiceWeb.MsgHeader();
            header.ServiceName = PurchaseRequestServiceWeb.MsgHeaderServiceName.PurchaseRequestService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            PurchaseRequestServiceWeb.PurchaseRequestService oPurchaseRequestService = new PurchaseRequestServiceWeb.PurchaseRequestService();
            oPurchaseRequestService.MsgHeaderValue = header;

            PurchaseRequestSAP mPRIni = new PurchaseRequestSAP();
            Mapper.CreateMap(typeof(PurchaseRequestSAP), typeof(PurchaseRequestSAP));
            Mapper.Map(pObject, mPRIni);

            PurchaseRequestServiceWeb.DocumentParams parametros = new PurchaseRequestServiceWeb.DocumentParams();
            parametros.DocEntry = pObject.DocEntry;
            parametros.DocEntrySpecified = true;

            PurchaseRequestServiceWeb.Document prDocument = oPurchaseRequestService.GetByParams(parametros);
            prDocument.Requester = pObject.Requester;
            prDocument.ReqType = (long)pObject.ReqType;
            prDocument.ReqTypeSpecified = true;
            prDocument.RequesterBranch = (long)pObject.Branch;
            prDocument.RequesterBranchSpecified = true;
            prDocument.RequesterDepartment = (long)pObject.Department;
            prDocument.RequesterDepartmentSpecified = true;
            prDocument.DocDate = pObject.DocDate.Value;
            prDocument.DocDateSpecified = true;
            prDocument.DocDueDate = pObject.DocDueDate.Value;
            prDocument.DocDueDateSpecified = true;
            prDocument.TaxDate = pObject.TaxDate.Value;
            prDocument.TaxDateSpecified = true;
            prDocument.RequriedDate = pObject.ReqDate.Value;
            prDocument.RequriedDateSpecified = true;
            prDocument.Comments = pObject.Comments;

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            foreach (PurchaseRequestServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                PurchaseRequestLineSAP portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                    if (portalLine.Quantity != null)
                    {
                        docLine.Quantity = (double)portalLine.Quantity;
                        docLine.QuantitySpecified = true;
                    }
                    if (portalLine.PriceBefDi != null)
                    {
                        docLine.UnitPrice = (double)portalLine.PriceBefDi;
                        docLine.UnitPriceSpecified = true;
                    }
                    docLine.Currency = portalLine.Currency;
                    docLine.RequiredDate = (portalLine.PQTReqDate == null ? prDocument.RequriedDate : portalLine.PQTReqDate.Value);
                    docLine.RequiredDateSpecified = true;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.FreeText = portalLine.FreeTxt;
                    if (portalLine.AcctCode != null)
                    {
                        docLine.AccountCode = portalLine.AcctCode;
                    }

                    if (!string.IsNullOrEmpty(portalLine.UomCode))
                    {
                        OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                        if (mOUOM != null)
                        {
                            docLine.UoMCode = portalLine.UomCode;
                            docLine.UoMEntry = mOUOM.UomEntry;
                            docLine.UoMEntrySpecified = true;
                        }
                    }

                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                }
            }
            PurchaseRequestServiceWeb.DocumentDocumentLine[] newLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (PurchaseRequestLineSAP portalLine in pObject.Lines)
            {
                PurchaseRequestServiceWeb.DocumentDocumentLine line = new PurchaseRequestServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                if (portalLine.Quantity != null)
                {
                    line.Quantity = (double)portalLine.Quantity;
                    line.QuantitySpecified = true;
                }
                if (portalLine.PriceBefDi != null)
                {
                    line.UnitPrice = (double)portalLine.PriceBefDi;
                    line.UnitPriceSpecified = true;
                }
                line.Currency = portalLine.Currency;
                line.RequiredDate = (portalLine.PQTReqDate == null ? prDocument.RequriedDate : portalLine.PQTReqDate.Value);
                line.RequiredDateSpecified = true;
                line.WarehouseCode = portalLine.WhsCode;
                line.CostingCode = portalLine.OcrCode;
                line.FreeText = portalLine.FreeTxt;
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new PurchaseRequestServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;

            try
            {
                //oPurchaseRequestService.Update(prDocument);

                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    PurchaseRequestLineSAP mAuxLine = mPRIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }
        #endregion


        #region Sales Quotation

        public List<Freight> GetSalesQuotationFreights(CompanyConn pCompanyParam, int pDocEntry)
        {
            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            Mapper.CreateMap<OOCR, DistrRuleSAP>();
            Mapper.CreateMap<DistrRuleSAP, OOCR>();

            Mapper.CreateMap<OSTC, SalesTaxCodesSAP>();
            Mapper.CreateMap<SalesTaxCodesSAP, OSTC>();

            Mapper.CreateMap<OPRJ, ProjectSAP>();
            Mapper.CreateMap<ProjectSAP, OPRJ>();

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            List<QUT3> mListQUT3 = mDbContext.QUT3.ToList();

            List<OEXD> mListOEXD = mDbContext.OEXD.ToList();

            List<Freight> mListFreight = (from mQUT3 in mListQUT3
                                          join mOEXD in mListOEXD on mQUT3.ExpnsCode equals mOEXD.ExpnsCode
                                          where
                                          (
                                              mQUT3.DocEntry == pDocEntry
                                          )
                                          select new Freight()
                                          {
                                              DocEntry = mQUT3.DocEntry,
                                              ExpnsCode = mQUT3.ExpnsCode,
                                              ExpnsName = mOEXD.ExpnsName,
                                              LineTotal = mQUT3.LineTotal,
                                              TotalFrgn = mQUT3.TotalFrgn,
                                              Comments = mQUT3.Comments,
                                              ObjType = mQUT3.ObjType,
                                              DistrbMthd = mQUT3.DistrbMthd,
                                              VatSum = mQUT3.VatSum,
                                              TaxCode = mQUT3.TaxCode,
                                              LineNum = mQUT3.LineNum,
                                              Status = mQUT3.Status,
                                              OcrCode = mQUT3.OcrCode,
                                              TaxDistMtd = mQUT3.TaxDistMtd
                                          }).ToList();

            List<Freight> mListFreightOut = mListOEXD.Where(c => !(mListFreight.Select(d => d.ExpnsCode).ToArray().Contains(c.ExpnsCode))).Select(c => new Freight()
            {
                ExpnsCode = c.ExpnsCode,
                ExpnsName = c.ExpnsName,
                DistrbMthd = c.DistrbMthd,
                OcrCode = c.OcrCode,
                Status = "O"
            }).ToList();

            mListFreight = mListFreight.Union(mListFreightOut).ToList();


            if (mListFreight.Count == 0)
            {
                mListFreight = Mapper.Map<List<Freight>>(mListOEXD);
                mListFreight.Select(c => { c.Status = "O"; return c; }).ToList();
            }


            List<OOCR> mListOOCR = mDbContext.OOCR.Where(c => c.Active == "Y").ToList();

            List<OSTC> mListOSTC = mDbContext.OSTC.ToList();

            List<OPRJ> mListOPRJ = mDbContext.OPRJ.ToList();

            mListFreight.Select(c => { c.ListDistrRule = Mapper.Map<List<DistrRuleSAP>>(mListOOCR); c.ListTax = Mapper.Map<List<SalesTaxCodesSAP>>(mListOSTC); c.ListProject = Mapper.Map<List<ProjectSAP>>(mListOPRJ); return c; }).ToList();

            return mListFreight;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pObject"></param>
        /// <param name="pCompanyParam"></param>
        /// <returns></returns>
        public JsonObjectResult AddSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {

            SalesQuotationServiceWeb.MsgHeader header = new SalesQuotationServiceWeb.MsgHeader();
            header.ServiceName = SalesQuotationServiceWeb.MsgHeaderServiceName.QuotationsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesQuotationServiceWeb.QuotationsService oSalesQuotationService = new SalesQuotationServiceWeb.QuotationsService();
            oSalesQuotationService.MsgHeaderValue = header;

            SalesQuotationSAP mSQIni = new SalesQuotationSAP();
            Mapper.CreateMap(typeof(SalesQuotationSAP), typeof(SalesQuotationSAP));
            Mapper.Map(pObject, mSQIni);

            JsonObjectResult jsonObjectResult = new JsonObjectResult();

            SalesQuotationServiceWeb.Document prDocument = new SalesQuotationServiceWeb.Document();
            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = Miscellaneous.ReplaceUTF(pObject.CardName);
            prDocument.Project = pObject.Project;
            prDocument.ShipToCode = pObject.ShipToCode;
            prDocument.PayToCode = pObject.PayToCode;
            prDocument.BPL_IDAssignedToInvoice = (long)pCompanyParam.IdBranchSelect;
            prDocument.BPL_IDAssignedToInvoiceSpecified = true;


            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            //prDocument.Address = pObject.Address;
            //prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            //prDocument.DocTotal = (double)pObject.DocTotal;

            SalesQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.SQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.SQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.SQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.SQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.SQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.SQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.SQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.SQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.SQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.SQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.SQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.SQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.SQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.SQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.SQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.SQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.SQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.SQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.SQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.SQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //Agrego los Freights al documento
            SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLine = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense();

                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocEntry = expenseLine.BaseAbsEnt ?? 0;
                    oDocumentExpenses.BaseDocEntrySpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocLine = expenseLine.BaseLnNum ?? 0;
                    oDocumentExpenses.BaseDocLineSpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocType = expenseLine.BaseType ?? 0;
                    oDocumentExpenses.BaseDocTypeSpecified = true;
                }


                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }


                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                if (expenseLine.ExpnsCode != null)
                {
                    oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode ?? 0;
                    oDocumentExpenses.ExpenseCodeSpecified = true;
                }
                oDocumentExpenses.LineNum = expenseLine.LineNum;
                oDocumentExpenses.LineNumSpecified = true;
                oDocumentExpenses.Project = expenseLine.Project;
                oDocumentExpenses.Remarks = expenseLine.Comments;


                oDocumentExpensesList[positionNewLine] = oDocumentExpenses;
                positionNewLine += 1;

            }
            prDocument.DocumentAdditionalExpenses = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            //Creo las lineas del documento
            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            SalesQuotationServiceWeb.DocumentDocumentLine[] newLines = new SalesQuotationServiceWeb.DocumentDocumentLine[pObject.Lines.Count];
            positionNewLine = 0;
            foreach (SalesQuotationSAPLine portalLine in pObject.Lines)
            {
                SalesQuotationServiceWeb.DocumentDocumentLine line = new SalesQuotationServiceWeb.DocumentDocumentLine();

                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.CostingCode2 = portalLine.OcrCode2;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {


                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }

                    //mDbContext.Dispose();
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new SalesQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                //oSalesQuotationService.Add(prDocument);

                XmlDocument mXmlDoc = GetSoapStructure(false, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    SalesQuotationSAPLine mAuxLine = mSQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                string mResponse = mB1WSHandler.ProcessDocWithError(mXmlDoc);
                jsonObjectResult.Code = mResponse;
                jsonObjectResult.ServiceAnswer = Enums.ServiceResult.Ok.ToDescriptionString();

                if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" encoding=""UTF-16""?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));

                    SalesQuotationServiceWeb.DocumentParams parametros = new SalesQuotationServiceWeb.DocumentParams();
                    parametros.DocEntry = Convert.ToInt32(jsonObjectResult.Code, System.Globalization.CultureInfo.InvariantCulture);
                    parametros.DocEntrySpecified = true;

                    prDocument = oSalesQuotationService.GetByParams(parametros);
                    foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                    {
                        prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                        prDocument.AttachmentEntrySpecified = true;
                    }
                    oSalesQuotationService.Update(prDocument);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS - > AddSalesQuotation: " + ex.Message);
                jsonObjectResult.ServiceAnswer = Enums.ServiceResult.Error.ToDescriptionString();
                jsonObjectResult.ErrorMsg = ex.Message;
            }
            return jsonObjectResult;
        }

        public string UpdateSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {

            SalesQuotationServiceWeb.MsgHeader header = new SalesQuotationServiceWeb.MsgHeader();
            header.ServiceName = SalesQuotationServiceWeb.MsgHeaderServiceName.QuotationsService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesQuotationServiceWeb.QuotationsService oSalesQuotationService = new SalesQuotationServiceWeb.QuotationsService();
            oSalesQuotationService.MsgHeaderValue = header;

            SalesQuotationServiceWeb.DocumentParams parametros = new SalesQuotationServiceWeb.DocumentParams();
            parametros.DocEntry = pObject.DocEntry;
            parametros.DocEntrySpecified = true;

            SalesQuotationSAP mSQIni = new SalesQuotationSAP();
            Mapper.CreateMap(typeof(SalesQuotationSAP), typeof(SalesQuotationSAP));
            Mapper.Map(pObject, mSQIni);

            SalesQuotationServiceWeb.Document prDocument = oSalesQuotationService.GetByParams(parametros);

            switch (pObject.DocType)
            {
                case "I":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
                case "S":
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Service;
                    prDocument.DocTypeSpecified = true;
                    break;
                default:
                    prDocument.DocType = SalesQuotationServiceWeb.DocumentDocType.dDocument_Items;
                    prDocument.DocTypeSpecified = true;
                    break;
            }
            if (pObject.DocDate.HasValue && pObject.DocDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDate = pObject.DocDate.Value;
                prDocument.DocDateSpecified = true;
            }
            if (pObject.DocDueDate.HasValue && pObject.DocDueDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.DocDueDate = pObject.DocDueDate.Value;
                prDocument.DocDueDateSpecified = true;
            }
            if (pObject.TaxDate.HasValue && pObject.TaxDate.Value.ToString("yyyyMMdd") != "00010101")
            {
                prDocument.TaxDate = pObject.TaxDate.Value;
                prDocument.TaxDateSpecified = true;
            }

            prDocument.CardCode = pObject.CardCode;
            prDocument.CardName = pObject.CardName;
            prDocument.Project = pObject.Project;
            prDocument.ShipToCode = pObject.ShipToCode;
            prDocument.PayToCode = pObject.PayToCode;



            switch (pObject.RevisionPo)
            {
                case "Y":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tYES;
                    prDocument.RevisionPoSpecified = true;
                    break;
                case "N":
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
                default:
                    prDocument.RevisionPo = SalesQuotationServiceWeb.DocumentRevisionPo.tNO;
                    prDocument.RevisionPoSpecified = true;
                    break;
            }
            switch (pObject.SummryType)
            {
                case "N":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "I":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByItems;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                case "D":
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dByDocuments;
                    prDocument.SummeryTypeSpecified = true;
                    break;
                default:
                    prDocument.SummeryType = SalesQuotationServiceWeb.DocumentSummeryType.dNoSummary;
                    prDocument.SummeryTypeSpecified = true;
                    break;
            }

            prDocument.NumAtCard = pObject.NumAtCard;
            prDocument.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
            prDocument.SalesPersonCodeSpecified = true;

            if (pObject.OwnerCode.HasValue && pObject.OwnerCode.Value != 0)
            {
                prDocument.DocumentsOwner = pObject.OwnerCode.Value;
                prDocument.DocumentsOwnerSpecified = true;
            }
            prDocument.DocCurrency = pObject.DocCur;
            prDocument.DocRate = (double)((pObject.DocRate.HasValue && pObject.DocRate.Value != 0) ? pObject.DocRate.Value : 1);
            prDocument.DocRateSpecified = true;
            prDocument.ContactPersonCode = pObject.CntctCode.HasValue ? pObject.CntctCode.Value : 0;
            prDocument.ContactPersonCodeSpecified = true;
            prDocument.PaymentGroupCode = pObject.GroupNum.HasValue ? pObject.GroupNum.Value : -1;
            prDocument.PaymentGroupCodeSpecified = true;
            prDocument.PaymentMethod = pObject.PeyMethod;

            if (pObject.DiscPrcnt.HasValue && pObject.DiscPrcnt.Value != 0)
            {
                prDocument.DiscountPercent = (double)pObject.DiscPrcnt.Value;
                prDocument.DiscountPercentSpecified = true;
            }
            prDocument.TransportationCode = (pObject.TrnspCode.HasValue && pObject.TrnspCode.Value != 0) ? pObject.TrnspCode.Value : -1;
            prDocument.TransportationCodeSpecified = true;
            //prDocument.Address = pObject.Address;
            //prDocument.Address2 = pObject.Address2;
            prDocument.Comments = pObject.Comments;
            prDocument.JournalMemo = pObject.JrnlMemo;
            //prDocument.DocTotal = (double)pObject.DocTotal;

            SalesQuotationServiceWeb.DocumentAddressExtension oDocumentAddress = new SalesQuotationServiceWeb.DocumentAddressExtension();
            //Defino los campos del shipto
            oDocumentAddress.ShipToStreet = pObject.SQAddress.StreetS;
            oDocumentAddress.ShipToStreetNo = pObject.SQAddress.StreetNoS;
            oDocumentAddress.ShipToBlock = pObject.SQAddress.BlockS;
            oDocumentAddress.ShipToBuilding = pObject.SQAddress.BuildingS;
            oDocumentAddress.ShipToCity = pObject.SQAddress.CityS;
            oDocumentAddress.ShipToZipCode = pObject.SQAddress.ZipCodeS;
            oDocumentAddress.ShipToCounty = pObject.SQAddress.CountyS;
            oDocumentAddress.ShipToState = pObject.SQAddress.StateS;
            oDocumentAddress.ShipToCountry = pObject.SQAddress.CountryS;
            oDocumentAddress.ShipToGlobalLocationNumber = pObject.SQAddress.GlbLocNumS;
            //Defino los campos del billto
            oDocumentAddress.BillToStreet = pObject.SQAddress.StreetB;
            oDocumentAddress.BillToStreetNo = pObject.SQAddress.StreetNoB;
            oDocumentAddress.BillToBlock = pObject.SQAddress.BlockB;
            oDocumentAddress.BillToBuilding = pObject.SQAddress.BuildingB;
            oDocumentAddress.BillToCity = pObject.SQAddress.CityB;
            oDocumentAddress.BillToZipCode = pObject.SQAddress.ZipCodeB;
            oDocumentAddress.BillToCounty = pObject.SQAddress.CountyB;
            oDocumentAddress.BillToState = pObject.SQAddress.StateB;
            oDocumentAddress.BillToCountry = pObject.SQAddress.CountryB;
            oDocumentAddress.BillToGlobalLocationNumber = pObject.SQAddress.GlbLocNumB;

            prDocument.AddressExtension = oDocumentAddress;

            //foreach (SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense expenseLine in prDocument.DocumentAdditionalExpenses)
            //{
            //    Freight portalExpenseLine = pObject.ListFreight.Where(c => c.LineNum == expenseLine.LineNum).FirstOrDefault();
            //    if (portalExpenseLine != null)
            //    {
            //        switch (portalExpenseLine.DistrbMthd)
            //        {
            //            case "N":
            //                expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "Q":
            //                expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "V":
            //                expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "W":
            //                expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "E":
            //                expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //            case "T":
            //                expenseLine.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
            //                expenseLine.DistributionMethodSpecified = true;
            //                break;
            //        }
            //        expenseLine.ExpenseCode = portalExpenseLine.ExpnsCode.Value;
            //        expenseLine.ExpenseCodeSpecified = true;
            //        expenseLine.DistributionRule = portalExpenseLine.OcrCode;
            //        expenseLine.Remarks = portalExpenseLine.Comments;
            //        expenseLine.TaxCode = portalExpenseLine.TaxCode;
            //        expenseLine.LineTotal = (double)portalExpenseLine.LineTotal;
            //        expenseLine.LineTotalSpecified = true;
            //        expenseLine.Project = portalExpenseLine.Project;
            //    }

            //}

            //Agrego los Freights al documento
            SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[] oDocumentExpensesList = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[pObject.ListFreight.Count];
            int positionNewLineAdd = 0;
            foreach (Freight expenseLine in pObject.ListFreight)
            {
                SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense oDocumentExpenses = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense();

                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocEntry = expenseLine.BaseAbsEnt ?? 0;
                    oDocumentExpenses.BaseDocEntrySpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocLine = expenseLine.BaseLnNum ?? 0;
                    oDocumentExpenses.BaseDocLineSpecified = true;
                }
                if (expenseLine.BaseAbsEnt != null)
                {
                    oDocumentExpenses.BaseDocType = expenseLine.BaseType ?? 0;
                    oDocumentExpenses.BaseDocTypeSpecified = true;
                }


                switch (expenseLine.DistrbMthd)
                {
                    case "N":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_None;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "Q":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Quantity;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "V":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Volume;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "W":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Weight;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "E":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_Equally;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                    case "T":
                        oDocumentExpenses.DistributionMethod = SalesQuotationServiceWeb.DocumentDocumentAdditionalExpenseDistributionMethod.aedm_RowTotal;
                        oDocumentExpenses.DistributionMethodSpecified = true;
                        break;
                }


                oDocumentExpenses.DistributionRule = expenseLine.OcrCode;
                if (expenseLine.ExpnsCode != null)
                {
                    oDocumentExpenses.ExpenseCode = expenseLine.ExpnsCode ?? 0;
                    oDocumentExpenses.ExpenseCodeSpecified = true;
                }

                if (expenseLine.LineTotal != null)
                {
                    oDocumentExpenses.LineTotal = (double)expenseLine.LineTotal;
                    oDocumentExpenses.LineTotalSpecified = true;
                }

                oDocumentExpenses.LineNum = expenseLine.LineNum;
                oDocumentExpenses.LineNumSpecified = true;
                oDocumentExpenses.Project = expenseLine.Project;
                oDocumentExpenses.Remarks = expenseLine.Comments;


                oDocumentExpensesList[positionNewLineAdd] = oDocumentExpenses;
                positionNewLineAdd += 1;

            }
            prDocument.DocumentAdditionalExpenses = new SalesQuotationServiceWeb.DocumentDocumentAdditionalExpense[oDocumentExpensesList.Length];
            prDocument.DocumentAdditionalExpenses = oDocumentExpensesList;

            mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.Open();

            foreach (SalesQuotationServiceWeb.DocumentDocumentLine docLine in prDocument.DocumentLines)
            {
                SalesQuotationSAPLine portalLine = pObject.Lines.Where(c => c.LineNum == docLine.LineNum).FirstOrDefault();

                if (portalLine != null)
                {
                    docLine.ItemCode = portalLine.ItemCode;
                    docLine.ItemDescription = Miscellaneous.ReplaceUTF(portalLine.Dscription);
                    docLine.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                    docLine.QuantitySpecified = true;
                    docLine.SupplierCatNum = portalLine.SubCatNum;
                    docLine.AccountCode = portalLine.AcctCode;
                    docLine.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                    docLine.PriceSpecified = true;
                    docLine.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                    docLine.UnitPriceSpecified = true;
                    docLine.Currency = portalLine.Currency;
                    docLine.WarehouseCode = portalLine.WhsCode;
                    docLine.TaxCode = portalLine.TaxCode;
                    docLine.ProjectCode = portalLine.Project;
                    docLine.CostingCode = portalLine.OcrCode;
                    docLine.CostingCode2 = portalLine.OcrCode2;
                    docLine.FreeText = portalLine.FreeTxt;
                    docLine.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                    docLine.SalesPersonCodeSpecified = true;

                    if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                    {
                        docLine.ShipDate = portalLine.ShipDate.Value;
                        docLine.ShipDateSpecified = true;
                    }
                    if (portalLine.AcctCode != null)
                    {
                        docLine.AccountCode = portalLine.AcctCode;
                    }

                    if (portalLine.DiscPrcnt.HasValue)
                    {
                        docLine.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                        docLine.DiscountPercentSpecified = true;
                    }

                    if (portalLine.VatPrcnt.HasValue)
                    {
                        docLine.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                        docLine.TaxPercentagePerRowSpecified = true;
                    }

                    if (!string.IsNullOrEmpty(portalLine.UomCode))
                    {
                        OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                        if (mOUOM != null)
                        {
                            docLine.UoMCode = portalLine.UomCode;
                            docLine.UoMEntry = mOUOM.UomEntry;
                            docLine.UoMEntrySpecified = true;
                        }

                    }

                    pObject.Lines.Remove(portalLine);
                }
                else
                {
                    //prDocument.DocumentLines.ToList().Remove(docLine);
                    prDocument.DocumentLines = prDocument.DocumentLines.Where(c => c.LineNum != docLine.LineNum).ToArray();
                }
            }



            SalesQuotationServiceWeb.DocumentDocumentLine[] newLines = new SalesQuotationServiceWeb.DocumentDocumentLine[prDocument.DocumentLines.Length + pObject.Lines.Count];
            prDocument.DocumentLines.CopyTo(newLines, 0);
            int positionNewLine = prDocument.DocumentLines.Length;
            foreach (SalesQuotationSAPLine portalLine in pObject.Lines)
            {
                SalesQuotationServiceWeb.DocumentDocumentLine line = new SalesQuotationServiceWeb.DocumentDocumentLine();
                line.LineNum = portalLine.LineNum;
                line.LineNumSpecified = true;
                line.ItemCode = portalLine.ItemCode;
                line.ItemDescription = portalLine.Dscription;
                line.Quantity = (double)(portalLine.Quantity.HasValue ? portalLine.Quantity.Value : 0);
                line.QuantitySpecified = true;
                line.SupplierCatNum = portalLine.SubCatNum;
                line.AccountCode = portalLine.AcctCode;
                line.Price = (double)(portalLine.Price.HasValue ? portalLine.Price.Value : 0);
                line.PriceSpecified = true;
                line.UnitPrice = (double)(portalLine.PriceBefDi.HasValue ? portalLine.PriceBefDi.Value : 0);
                line.UnitPriceSpecified = true;
                line.Currency = portalLine.Currency;
                line.WarehouseCode = portalLine.WhsCode;
                line.TaxCode = portalLine.TaxCode;
                line.ProjectCode = portalLine.Project;
                line.CostingCode = portalLine.OcrCode;
                line.CostingCode2 = portalLine.OcrCode2;
                line.FreeText = portalLine.FreeTxt;
                line.SalesPersonCode = ((pObject.SlpCode.HasValue && pObject.SlpCode.Value != 0) ? pObject.SlpCode.Value : -1);
                line.SalesPersonCodeSpecified = true;

                if (portalLine.ShipDate.HasValue && portalLine.ShipDate.Value.ToString("yyyyMMdd") != "00010101")
                {
                    line.ShipDate = portalLine.ShipDate.Value;
                    line.ShipDateSpecified = true;
                }
                if (portalLine.AcctCode != null)
                {
                    line.AccountCode = portalLine.AcctCode;
                }

                if (portalLine.DiscPrcnt.HasValue)
                {
                    line.DiscountPercent = (double)portalLine.DiscPrcnt.Value;
                    line.DiscountPercentSpecified = true;
                }

                if (portalLine.VatPrcnt.HasValue)
                {
                    line.TaxPercentagePerRow = (double)portalLine.VatPrcnt.Value;
                    line.TaxPercentagePerRowSpecified = true;
                }

                if (!string.IsNullOrEmpty(portalLine.UomCode))
                {
                    OUOM mOUOM = mDbContext.OUOM.Where(c => c.UomCode == portalLine.UomCode).FirstOrDefault();

                    if (mOUOM != null)
                    {
                        line.UoMCode = portalLine.UomCode;
                        line.UoMEntry = mOUOM.UomEntry;
                        line.UoMEntrySpecified = true;
                    }
                }
                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mDbContext.Dispose();
            prDocument.DocumentLines = new SalesQuotationServiceWeb.DocumentDocumentLine[newLines.Length];
            prDocument.DocumentLines = newLines;
            try
            {
                XmlDocument mXmlDoc = GetSoapStructure(true, header, prDocument);

                if (pObject.MappedUdf.Count > 0)
                { mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml = UDFUtil.AddUDFToXml(mXmlDoc.SelectSingleNode("//*[local-name()='Document']").InnerXml, pObject.MappedUdf); }

                XmlNodeList mAuxNodeList = mXmlDoc.SelectNodes("//*[local-name()='DocumentLine']");
                foreach (XmlNode mAuxNode in mAuxNodeList)
                {
                    SalesQuotationSAPLine mAuxLine = mSQIni.Lines.Where(c => c.LineNum.ToString() == mAuxNode["LineNum"].InnerText).FirstOrDefault();

                    if (mAuxLine != null && mAuxLine.MappedUdf.Count > 0)
                    { mAuxNode.InnerXml = UDFUtil.AddUDFToXml(mAuxNode.InnerXml, mAuxLine.MappedUdf).ToString(); }

                }

                mB1WSHandler.ProcessDoc(mXmlDoc);

                if (prDocument.AttachmentEntry != 0)
                {
                    string AddCmd = string.Empty;
                    SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                    XmlDocument mResultXML = new XmlDocument();
                    AddCmd = @"<?xml version=""1.0"" ?>" +
                            @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                            "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                            @"<env:Body><dis:UpdateObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><QueryParams><AbsoluteEntry>" + prDocument.AttachmentEntry + "</AbsoluteEntry></QueryParams><Attachments2></Attachments2><Attachments2_Lines> ";
                    foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                    {
                        AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension><Override>tYES</Override></row>";
                    }

                    AddCmd += @"</Attachments2_Lines></BO></BOM></dis:UpdateObject></env:Body></env:Envelope>";
                    mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                }
                else
                {
                    if (pObject.AttachmentList.Where(c => c.AbsEntry == 0).Count() > 0)
                    {
                        string AddCmd = string.Empty;
                        SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                        XmlDocument mResultXML = new XmlDocument();
                        AddCmd = @"<?xml version=""1.0"" ?>" +
                                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                                "<env:Header>" + "<SessionID>" + pCompanyParam.DSSessionId + "</SessionID>" + "</env:Header>" +
                                @"<env:Body><dis:AddObject xmlns:dis=""http://www.sap.com/SBO/DIS""><BOM><BO><AdmInfo><Object>oAttachments2</Object></AdmInfo><Attachments2></Attachments2><Attachments2_Lines> ";
                        foreach (AttachmentSAP mAttachment in pObject.AttachmentList)
                        {
                            AddCmd += @"<row><SourcePath>" + mAttachment.srcPath + "</SourcePath><FileName>" + mAttachment.FileName + "</FileName><FileExtension>" + mAttachment.FileExt + "</FileExtension></row>";
                        }

                        AddCmd += @"</Attachments2_Lines></BO></BOM></dis:AddObject></env:Body></env:Envelope>";
                        mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                        prDocument = oSalesQuotationService.GetByParams(parametros);
                        foreach (XmlNode node in mResultXML.GetElementsByTagName("RetKey"))
                        {
                            prDocument.AttachmentEntry = Convert.ToInt32(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);
                            prDocument.AttachmentEntrySpecified = true;
                        }
                        oSalesQuotationService.Update(prDocument);
                    }
                }

                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public JsonObjectResult GetSalesQuotationListSearch(CompanyConn pCompanyParam, string pCodeVendor = "", DateTime? pDate = null, int? pDocNum = null, string pDocStatus = "", string pOwnerCode = "", string pSECode = "", int pStart = 0, int pLength = 0, OrderColumn pOrderColumn = null)
        {
            try
            {
                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<OQUT, SalesQuotationSAP>(); }).CreateMapper();
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                JsonObjectResult mJsonObjectResult = new JsonObjectResult();

                int mSECode = Convert.ToInt32(pSECode == "" ? "0" : pSECode);
                int mOwnerCode = Convert.ToInt32(pOwnerCode == "" ? "0" : pOwnerCode);

                int mTotalRecords = mDbContext.OQUT.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).Count();
                List<OQUT> ListRestu = mDbContext.OQUT.Where(c => (c.CardCode.Contains(pCodeVendor)) && (pDocNum == null ? (c.DocNum > 0) : (c.DocNum == pDocNum)) && (pDate == null ? c.DocDate != null : c.DocDate == pDate) && (pDocStatus != "" ? c.DocStatus == pDocStatus : true) && (mOwnerCode != 0 ? c.OwnerCode == mOwnerCode : true) && (mSECode != 0 ? c.SlpCode == mSECode : true)).OrderBy(Utils.OrderString(pOrderColumn)).Skip(pStart).Take(pLength).ToList();

                mJsonObjectResult.SalesQuotationSAPList = mapper.Map<List<SalesQuotationSAP>>(ListRestu);
                mJsonObjectResult.Others.Add("TotalRecords", mTotalRecords.ToString());
                return mJsonObjectResult;
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetSalesQuotationListSearch :" + ex.Message);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pCompanyParam"></param>
        /// <param name="pDocuments"></param>
        /// <returns></returns>
        public List<SalesQuotationSAPLine> GetSalesQuotationLinesSearch(CompanyConn pCompanyParam, string[] pDocuments)
        {
            try
            {
                //Obtengo el ConnString con los Parametros de la Company y abro conexion
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();
                Mapper.CreateMap<QUT1, SalesQuotationSAPLine>();
                Mapper.CreateMap<SalesQuotationSAPLine, QUT1>();

                List<QUT1> ListRestu = mDbContext.QUT1.Where(c => pDocuments.ToList().Contains(c.DocEntry.ToString()) && c.LineStatus == "O").ToList();

                return Mapper.Map<List<SalesQuotationSAPLine>>(ListRestu);
            }
            catch (Exception ex)
            {
                Logger.WriteError("PurchaseOrderService -> GetSalesQuotationLinesSearch :" + ex.Message);
                return null;
            }
            finally
            {
                //Cierro la conexion abierta
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }

            }
        }

        public SalesQuotationSAP GetAllbySQ(CompanyConn pCompanyParam, int pDocEntry, List<UDF_ARGNS> pListUDFOQUT = null, List<UDF_ARGNS> pListUDFQUT1 = null)
        {

            Mapper.CreateMap<AttachmentSAP, ATC1>();
            Mapper.CreateMap<ATC1, AttachmentSAP>();

            Mapper.CreateMap<SalesQuotationSAPLine, QUT1>();
            Mapper.CreateMap<QUT1, SalesQuotationSAPLine>();

            Mapper.CreateMap<SalesQuotationSAP, OQUT>();
            Mapper.CreateMap<OQUT, SalesQuotationSAP>();

            Mapper.CreateMap<DocumentAddress, QUT12>();
            Mapper.CreateMap<QUT12, DocumentAddress>();

            Mapper.CreateMap<EmployeeSAP, OHEM>();
            Mapper.CreateMap<OHEM, EmployeeSAP>();

            Mapper.CreateMap<CompanyAddress, ADM1>();
            Mapper.CreateMap<ADM1, CompanyAddress>();

            Mapper.CreateMap<SalesQuotationSAPLineFreight, QUT2>();
            Mapper.CreateMap<QUT2, SalesQuotationSAPLineFreight>();

            Mapper.CreateMap<OEXD, Freight>();
            Mapper.CreateMap<Freight, OEXD>();

            SalesQuotationSAP mSaleQSAP = new SalesQuotationSAP();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            string mWhere = "[OQUT].[DocEntry] = " + pDocEntry;
            OQUT mOQUT = UDFUtil.GetObjectListWithUDF(pListUDFOQUT, mWhere, typeof(OQUT), mConnectionString, "OQUT").Cast<OQUT>().FirstOrDefault();

            if (mOQUT != null)
            {

                mSaleQSAP = Mapper.Map<SalesQuotationSAP>(mOQUT);

                mSaleQSAP = FillSQDocumentSAP(pCompanyParam, mSaleQSAP, mDbContext);

                //DocumentLines

                mWhere = "[QUT1].[DocEntry] = " + pDocEntry;
                List<QUT1> mListQUT1 = UDFUtil.GetObjectListWithUDF(pListUDFQUT1, mWhere, typeof(QUT1), mConnectionString, "QUT1").Cast<QUT1>().ToList();
                mSaleQSAP.Lines = Mapper.Map<List<SalesQuotationSAPLine>>(mListQUT1);

                foreach (SalesQuotationSAPLine item in mSaleQSAP.Lines)
                {
                    item.Freight = Mapper.Map<List<SalesQuotationSAPLineFreight>>(mDbContext.QUT2.Where(c => c.DocEntry == item.DocEntry && c.LineNum == item.LineNum && c.GroupNum == 0).ToList());

                    ItemMasterSAP mOitm = mDbContext.OITM.Where(c => c.ItemCode == item.ItemCode).Select(c => new ItemMasterSAP { UgpEntry = c.UgpEntry }).FirstOrDefault();

                    item.UnitOfMeasureList = GetUOMLists(mOitm.UgpEntry);

                }

                mSaleQSAP.Lines = mSaleQSAP.Lines.Select(c => { c.GLAccount = mSaleQSAP.GLAccountSAP.Where(j => j.AcctCode == c.AcctCode).FirstOrDefault(); return c; }).ToList();

                //DocumentAddress
                List<QUT12> mListQUT12 = mDbContext.QUT12.Where(c => c.DocEntry == pDocEntry).ToList();
                mSaleQSAP.SQAddress = Mapper.Map<DocumentAddress>(mListQUT12.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (mOQUT.OwnerCode ?? 0)).FirstOrDefault();
                mSaleQSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

                //Attachments
                List<ATC1> mATC1List = mDbContext.ATC1.Where(c => c.AbsEntry == mSaleQSAP.AtcEntry).ToList();
                mSaleQSAP.AttachmentList = Mapper.Map<List<AttachmentSAP>>(mATC1List);

                mSaleQSAP.ListFreight = new List<Freight>();
                mSaleQSAP.ListFreight.Add(new Freight());
                mSaleQSAP.ListFreight.AddRange(Mapper.Map<List<Freight>>(mDbContext.OEXD.ToList()));
            }
            else
            {
                mSaleQSAP = FillSQDocumentSAP(pCompanyParam, mSaleQSAP, mDbContext);

                //CompanyAddress
                List<ADM1> mListADM1 = mDbContext.ADM1.Where(c => c.Code == 1).ToList();
                mSaleQSAP.companyAddress = Mapper.Map<CompanyAddress>(mListADM1.FirstOrDefault());

                //Employees
                OHEM mOHEM = mDbContext.OHEM.Where(c => c.empID == (0)).FirstOrDefault();
                mSaleQSAP.Employee = Mapper.Map<EmployeeSAP>(mOHEM);

                mSaleQSAP.ListFreight = new List<Freight>();
                mSaleQSAP.ListFreight.Add(new Freight());
                mSaleQSAP.ListFreight.AddRange(Mapper.Map<List<Freight>>(mDbContext.OEXD.ToList()));
            }


            mDbContext.Dispose();

            return mSaleQSAP;
        }

        public SalesQuotationSAP FillSQDocumentSAP(CompanyConn pCompanyParam, SalesQuotationSAP pSaleQSAP, DataBase pDataBase)
        {
            DocumentSAPCombo retCb = new DocumentSAPCombo();

            Mapper.CreateMap<Buyer, OSLP>();
            Mapper.CreateMap<OSLP, Buyer>();

            Mapper.CreateMap<CurrencySAP, OCRN>();
            Mapper.CreateMap<OCRN, CurrencySAP>();

            Mapper.CreateMap<PaymentMethod, OCTG>();
            Mapper.CreateMap<OCTG, PaymentMethod>();

            Mapper.CreateMap<PaymentTerm, OPYM>();
            Mapper.CreateMap<OPYM, PaymentTerm>();

            Mapper.CreateMap<ShippingType, OSHP>();
            Mapper.CreateMap<OSHP, ShippingType>();

            Mapper.CreateMap<Taxt, OVTG>();
            Mapper.CreateMap<OVTG, Taxt>();

            Mapper.CreateMap<DistrRuleSAP, OOCR>();
            Mapper.CreateMap<OOCR, DistrRuleSAP>();

            Mapper.CreateMap<Warehouse, OWHS>();
            Mapper.CreateMap<OWHS, Warehouse>();

            Mapper.CreateMap<OACT, GLAccountSAP>();
            Mapper.CreateMap<GLAccountSAP, OACT>();

            Mapper.CreateMap<CINF, DocumentSettingsSAP>();
            Mapper.CreateMap<DocumentSettingsSAP, CINF>();

            Mapper.CreateMap<OCRY, CountrySAP>();
            Mapper.CreateMap<CountrySAP, OCRY>();


            List<OCRY> mListOCRY = mDbContext.OCRY.ToList();

            //Buyer
            List<OSLP> mLisOSLP = pDataBase.OSLP.ToList();

            //Currency
            List<OCRN> mListOCRN = pDataBase.OCRN.ToList();

            //PaymentMethod
            List<OCTG> mListOCTG = pDataBase.OCTG.ToList();

            //PaymentTerm
            // List<OPYM> mListOPYM = pDataBase.OPYM.ToList();

            //ShippingType
            List<OSHP> mListOSHP = pDataBase.OSHP.ToList();

            //Taxt
            List<OVTG> mListOVTG = pDataBase.OVTG.ToList();

            retCb.ListBuyer = Mapper.Map<List<Buyer>>(mLisOSLP);

            retCb.ListCurrency = Mapper.Map<List<CurrencySAP>>(mListOCRN);

            retCb.ListCountry = Mapper.Map<List<CountrySAP>>(mListOCRY);

            retCb.ListPaymentTerm = mListOCTG.Select(c => new PaymentTerm { PymntGroup = c.PymntGroup, GroupNum = c.GroupNum }).ToList();

            if (!string.IsNullOrEmpty(pSaleQSAP.CardCode))
            {
                List<PaymentMethod> mList = (from mCRD2 in mDbContext.CRD2
                                             join mOYPM in mDbContext.OPYM on mCRD2.PymCode equals mOYPM.PayMethCod
                                             where mCRD2.CardCode == pSaleQSAP.CardCode
                                             select new PaymentMethod()
                                             {
                                                 PayMethCod = mOYPM.PayMethCod,
                                                 Descript = mOYPM.Descript

                                             }).ToList();

                retCb.ListPaymentMethod.AddRange(mList);
            }

            retCb.ListShippingType = Mapper.Map<List<ShippingType>>(mListOSHP);

            retCb.ListTaxt = Mapper.Map<List<Taxt>>(mListOVTG);

            retCb.ListTypeCurrency = new List<TypeCurrency>() { new TypeCurrency { Description = "Local Currency", CurSource = "L" }, new TypeCurrency { Description = "System Currency", CurSource = "S" }, new TypeCurrency { Description = "BP Currency", CurSource = "C" } };

            pSaleQSAP.ListDocumentSAPCombo = retCb;

            //GLAccount
            List<OACT> mListOACT = pDataBase.OACT.Where(c => c.FormatCode != null).ToList();
            pSaleQSAP.GLAccountSAP = Mapper.Map<List<GLAccountSAP>>(mListOACT);

            //DocumentSettings
            List<CINF> mListCINF = pDataBase.CINF.ToList();
            pSaleQSAP.SAPDocSettings = Mapper.Map<DocumentSettingsSAP>(mListCINF.FirstOrDefault());

            //Warehouse
            List<OWHS> mListOWHS = pDataBase.OWHS.ToList();
            pSaleQSAP.Warehouse = Mapper.Map<List<Warehouse>>(mListOWHS);


            pSaleQSAP.LocalCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.LocalCurrency.ToString(), pCompanyParam.DSSessionId);
            pSaleQSAP.SystemCurrency = GetCurrency(WebServices.UtilWeb.CurrentType.SystemCurrency.ToString(), pCompanyParam.DSSessionId);

            //DistrbRule
            List<OOCR> mListOOCR = pDataBase.OOCR.Where(c => c.Active == "Y").ToList();
            pSaleQSAP.DistrRuleSAP = Mapper.Map<List<DistrRuleSAP>>(mListOOCR);

            return pSaleQSAP;
        }

        #endregion


        #region Warehouse

        public List<Warehouse> GetListWarehouse(CompanyConn pCompanyParam)
        {
            try
            {
                Mapper.CreateMap<Warehouse, OWHS>();
                Mapper.CreateMap<OWHS, Warehouse>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<Warehouse> ListReturn = Mapper.Map<List<Warehouse>>(mDbContext.OWHS.ToList());

                return ListReturn;
            }
            catch (Exception ex)
            {
                Logger.WriteError("GetListWarehouseDS -> GetListWarehouse :" + ex.Message);
                return null;
            }
        }

        #endregion


        #region Oportunities

        public OpportunitiesSAPLine GetOpportunityLine(CompanyConn pCompanyParam)
        {

            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                return GetLine();
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }

        }

        private OpportunitiesSAPLine GetLine()
        {
            OpportunitiesSAPLine ret = null;
            try
            {

                Mapper.CreateMap<OSLP, Buyer>();
                Mapper.CreateMap<OOST, OpportunityStage>();

                ret = new OpportunitiesSAPLine();
                //Sales Employee 
                ret.SalesEmployeeList = Mapper.Map<List<Buyer>>(mDbContext.OSLP.ToList());
                ret.SlpCode = -1;

                //Document Type
                ret.DocumentTypeList = GetDocumentType();
                //Stages
                ret.StageList = Mapper.Map<List<OpportunityStage>>(mDbContext.OOST.ToList());

                //Stage by Default
                if (ret.StageList != null)
                {
                    if (ret.StageList.Count > 0)
                    {
                        ret.Step_Id = ret.StageList.FirstOrDefault().StepId;
                        ret.ClosePrcnt = ret.StageList.FirstOrDefault().CloPrcnt;
                    }
                }

                //Date by Default
                ret.OpenDate = DateTime.Now;
                ret.CloseDate = DateTime.Now;

                //Status
                ret.Status = "O";

            }
            catch (Exception)
            {

                ret = null;
            }

            return ret;
        }

        public OpportunityStage GetStageById(CompanyConn pCompanyParam, int pId)
        {
            OpportunityStage ret = null;
            try
            {
                Mapper.CreateMap<OOST, OpportunityStage>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                ret = Mapper.Map<OpportunityStage>(mDbContext.OOST.Where(c => c.StepId == pId).SingleOrDefault());
            }
            catch (Exception)
            {
                ret = null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }

            return ret;
        }


        public OpportunitiesSAP GetOportunityById(CompanyConn pCompanyParam, int pId)
        {
            OpportunitiesSAP mRestu;
            try
            {
                Mapper.CreateMap<OOPR, OpportunitiesSAP>();
                Mapper.CreateMap<OPR1, OpportunitiesSAPLine>();
                Mapper.CreateMap<OCPR, ContacPerson>();
                Mapper.CreateMap<OSLP, Buyer>();
                Mapper.CreateMap<OOST, OpportunityStage>();
                Mapper.CreateMap<OHEM, EmployeeSAP>();
                Mapper.CreateMap<OCLG, ActivitiesSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                mRestu = Mapper.Map<OpportunitiesSAP>(mDbContext.OOPR.Where(c => c.OpprId == pId).SingleOrDefault());

                if (mRestu != null)
                {
                    //Contact Person
                    List<OCPR> mListOCPR = mDbContext.OCPR.Where(c => c.CardCode == mRestu.CardCode).ToList();
                    mRestu.ContactPersonList = Mapper.Map<List<ContacPerson>>(mListOCPR);

                    //Owner
                    mRestu.OwnerSAP = Mapper.Map<EmployeeSAP>(mDbContext.OHEM.Where(c => c.empID == mRestu.Owner).SingleOrDefault());
                    if (mRestu.OwnerSAP == null)
                    {
                        mRestu.OwnerSAP = new EmployeeSAP();
                    }
                    mRestu.OwnerSAP.completeName = mRestu.OwnerSAP.lastName + ", " + mRestu.OwnerSAP.firstName;
                    //Opportunity Lines
                    List<OPR1> mListOPR1 = mDbContext.OPR1.Where(c => c.OpprId == mRestu.OpprId).ToList();
                    mRestu.OpportunitiesLines = Mapper.Map<List<OpportunitiesSAPLine>>(mListOPR1);

                    foreach (var item in mRestu.OpportunitiesLines)
                    {
                        //Sales Employee 
                        item.SalesEmployeeList = Mapper.Map<List<Buyer>>(mDbContext.OSLP.ToList());
                        //Document Type
                        item.DocumentTypeList = GetDocumentType();

                        //Stage by Document Type
                        if (mRestu.OpprType == "R")
                        {
                            item.StageList = Mapper.Map<List<OpportunityStage>>(mDbContext.OOST.Where(c => c.SalesStage == "Y").ToList());
                        }
                        else
                        {
                            item.StageList = Mapper.Map<List<OpportunityStage>>(mDbContext.OOST.Where(c => c.PurStage == "Y").ToList());
                        }

                        item.ActivitiesList = (
                             from mOCLG in mDbContext.OCLG
                             where
                             (
                                 mOCLG.OprId == mRestu.OpprId &&
                                 mOCLG.OprLine == item.Line
                             )
                             select new ActivitiesSAP()
                             {
                                 ClgCode = mOCLG.ClgCode,
                                 CardCode = mOCLG.CardCode,
                                 Notes = mOCLG.Notes,
                                 Recontact = mOCLG.Recontact,
                                 Closed = mOCLG.Closed,
                                 CntctSbjct = mOCLG.CntctSbjct,
                                 AttendUser = mOCLG.AttendUser,
                                 AttendEmpl = mOCLG.AttendEmpl,
                                 Action = mOCLG.Action,
                                 Details = mOCLG.Details,
                                 CntctType = mOCLG.CntctType,
                                 Priority = mOCLG.Priority,
                                 endDate = mOCLG.endDate,
                                 inactive = mOCLG.inactive,
                                 OprId = mOCLG.OprId,
                                 OprLine = mOCLG.OprLine,
                                 status = mOCLG.status
                             }).ToList();
                    }

                }
                else
                {
                    mRestu = new OpportunitiesSAP();
                    mRestu.OpportunitiesLines.Add(GetLine());
                }

                mRestu.OpportunityTypeList = GetOpportunityType();

                //Sales Employee
                List<OSLP> mListSalesEmplpyee = mDbContext.OSLP.ToList();
                mRestu.SalesEmployeeList = Mapper.Map<List<Buyer>>(mListSalesEmplpyee);

            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
            return Mapper.Map<OpportunitiesSAP>(mRestu);
        }


        private List<DocumentType> GetDocumentType()
        {
            List<DocumentType> ret = null;
            try
            {
                ret = new List<DocumentType>();
                ret.Add(new DocumentType() { Code = "", Name = "" });
                ret.Add(new DocumentType() { Code = "23", Name = "Sales Quotations" });
                ret.Add(new DocumentType() { Code = "17", Name = "Sales Orders" });
                ret.Add(new DocumentType() { Code = "13", Name = "Sales Invoices" });
                //ret.Add(new DocumentType() { Code = "22", Name = "Purchase Orders" });
                //ret.Add(new DocumentType() { Code = "18", Name = "Purchase Invoices" });
                //ret.Add(new DocumentType() { Code = "540000006", Name = "Purchase Quotations" });

            }
            catch (Exception)
            {
                ret = null;
            }
            return ret;
        }

        private List<OpportunityType> GetOpportunityType()
        {
            List<OpportunityType> ret = null;
            try
            {
                ret = new List<OpportunityType>();
                ret.Add(new OpportunityType() { Code = "R", Name = "Sales" });
                //ret.Add(new OpportunityType() { Code = "P", Name = "Purchasing" });
            }
            catch (Exception)
            {
                ret = null;
            }
            return ret;
        }

        public string AddOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            SalesOpportunitiesServiceWeb.MsgHeader header = new SalesOpportunitiesServiceWeb.MsgHeader();
            header.ServiceName = SalesOpportunitiesServiceWeb.MsgHeaderServiceName.SalesOpportunitiesService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesOpportunitiesServiceWeb.SalesOpportunitiesService oOpportunitiesService = new SalesOpportunitiesServiceWeb.SalesOpportunitiesService();
            oOpportunitiesService.MsgHeaderValue = header;

            SalesOpportunitiesServiceWeb.SalesOpportunities mOpporDoc = new SalesOpportunitiesServiceWeb.SalesOpportunities();

            mOpporDoc.OpportunityName = pObject.Name;
            mOpporDoc.CardCode = pObject.CardCode;
            mOpporDoc.ContactPerson = pObject.CprCode ?? 0;
            mOpporDoc.SalesPerson = pObject.SlpCode ?? -1;
            mOpporDoc.SalesPersonSpecified = true;
            mOpporDoc.DataOwnershipfield = pObject.Owner ?? 0;
            mOpporDoc.DataOwnershipfieldSpecified = true;
            if (pObject.PredDate.HasValue)
            {
                mOpporDoc.PredictedClosingDate = pObject.PredDate.Value;
                mOpporDoc.PredictedClosingDateSpecified = true;
            }
            mOpporDoc.MaxLocalTotal = pObject.MaxSumLoc.HasValue ? Convert.ToDouble(pObject.MaxSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.MaxLocalTotalSpecified = true;
            mOpporDoc.WeightedSumLC = pObject.WtSumLoc.HasValue ? Convert.ToDouble(pObject.WtSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.WeightedSumLCSpecified = true;
            mOpporDoc.GrossProfit = pObject.PrcnProf.HasValue ? Convert.ToDouble(pObject.PrcnProf.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.GrossProfitSpecified = true;
            mOpporDoc.GrossProfitTotalLocal = pObject.SumProfL.HasValue ? Convert.ToDouble(pObject.SumProfL.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.GrossProfitTotalLocalSpecified = true;

            switch (pObject.Status)
            {
                case "O":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
                case "L":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Missed;
                    break;
                case "W":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Sold;
                    break;
                default:
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
            }
            mOpporDoc.StartDate = pObject.OpenDate ?? DateTime.Now;
            mOpporDoc.StartDateSpecified = true;
            if (pObject.CloseDate.HasValue)
            {
                mOpporDoc.ClosingDate = pObject.CloseDate.Value;
                mOpporDoc.ClosingDateSpecified = true;
            }
            mOpporDoc.ClosingPercentage = pObject.CloPrcnt.HasValue ? Convert.ToDouble(pObject.CloPrcnt.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.ClosingPercentageSpecified = true;
            mOpporDoc.ProjectCode = pObject.PrjCode;
            mOpporDoc.Remarks = pObject.Memo;




            //Creo las lineas del documento
            SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[] newLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[pObject.OpportunitiesLines.Count];
            int positionNewLine = 0;
            foreach (OpportunitiesSAPLine portalLine in pObject.OpportunitiesLines)
            {
                SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine line = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine();

                line.StartDate = portalLine.OpenDate ?? DateTime.Now;
                line.StartDateSpecified = true;

                line.ClosingDate = portalLine.CloseDate ?? DateTime.Now;
                line.ClosingDateSpecified = true;

                line.SalesPerson = portalLine.SlpCode ?? -1;
                line.SalesPersonSpecified = true;

                line.StageKey = portalLine.Step_Id ?? 0;
                line.StageKeySpecified = true;

                line.PercentageRate = (double)(portalLine.ClosePrcnt.HasValue ? portalLine.ClosePrcnt.Value : 0);
                line.PercentageRateSpecified = true;

                line.MaxLocalTotal = (double)(portalLine.MaxSumLoc.HasValue ? portalLine.MaxSumLoc.Value : 0);
                line.MaxLocalTotalSpecified = true;

                line.WeightedAmountLocal = (double)(portalLine.WtSumLoc.HasValue ? portalLine.WtSumLoc.Value : 0);
                line.WeightedAmountLocalSpecified = true;

                switch (portalLine.ObjType)
                {
                    case "17":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Order;
                        break;
                    case "23":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Quotation;
                        break;
                    case "13":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Invoice;
                        break;
                    case "22":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseOrder;
                        break;
                    case "540000006":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseQutation;
                        break;
                    case "18":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseInvoice;
                        break;
                    default:
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_MinusOne;
                        break;
                }
                line.DocumentTypeSpecified = true;

                if (pObject.DocNum.HasValue)
                {
                    line.DocumentNumber = pObject.DocNum.Value;
                    line.DocumentNumberSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mOpporDoc.SalesOpportunitiesLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[newLines.Length];
            mOpporDoc.SalesOpportunitiesLines = newLines;
            try
            {
                SalesOpportunitiesServiceWeb.SalesOpportunitiesParams pResult = oOpportunitiesService.Add(mOpporDoc);
                pObject.OpprId = (int)pResult.SequentialNo;
                UpdateActivities(pObject, pCompanyParam);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }


        public string UpdateOportunity(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            SalesOpportunitiesServiceWeb.MsgHeader header = new SalesOpportunitiesServiceWeb.MsgHeader();
            header.ServiceName = SalesOpportunitiesServiceWeb.MsgHeaderServiceName.SalesOpportunitiesService;
            header.ServiceNameSpecified = true;
            header.SessionID = pCompanyParam.DSSessionId;

            SalesOpportunitiesServiceWeb.SalesOpportunitiesService oOpportunitiesService = new SalesOpportunitiesServiceWeb.SalesOpportunitiesService();
            oOpportunitiesService.MsgHeaderValue = header;

            SalesOpportunitiesServiceWeb.SalesOpportunitiesParams parametros = new SalesOpportunitiesServiceWeb.SalesOpportunitiesParams();
            parametros.SequentialNo = pObject.OpprId;
            parametros.SequentialNoSpecified = true;

            SalesOpportunitiesServiceWeb.SalesOpportunities mOpporDoc = oOpportunitiesService.GetByParams(parametros);

            mOpporDoc.OpportunityName = pObject.Name;
            mOpporDoc.CardCode = pObject.CardCode;
            mOpporDoc.ContactPerson = pObject.CprCode ?? 0;
            mOpporDoc.SalesPerson = pObject.SlpCode ?? -1;
            mOpporDoc.SalesPersonSpecified = true;
            mOpporDoc.DataOwnershipfield = pObject.Owner ?? 0;
            mOpporDoc.DataOwnershipfieldSpecified = true;
            if (pObject.PredDate.HasValue)
            {
                mOpporDoc.PredictedClosingDate = pObject.PredDate.Value;
                mOpporDoc.PredictedClosingDateSpecified = true;
            }
            mOpporDoc.MaxLocalTotal = pObject.MaxSumLoc.HasValue ? Convert.ToDouble(pObject.MaxSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.MaxLocalTotalSpecified = true;
            mOpporDoc.WeightedSumLC = pObject.WtSumLoc.HasValue ? Convert.ToDouble(pObject.WtSumLoc.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.WeightedSumLCSpecified = true;
            mOpporDoc.GrossProfit = pObject.PrcnProf.HasValue ? Convert.ToDouble(pObject.PrcnProf.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.GrossProfitSpecified = true;
            mOpporDoc.GrossProfitTotalLocal = pObject.SumProfL.HasValue ? Convert.ToDouble(pObject.SumProfL.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.GrossProfitTotalLocalSpecified = true;

            switch (pObject.Status)
            {
                case "O":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
                case "L":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Missed;
                    break;
                case "W":
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Sold;
                    break;
                default:
                    mOpporDoc.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesStatus.sos_Open;
                    break;
            }
            mOpporDoc.StartDate = pObject.OpenDate ?? DateTime.Now;
            if (pObject.CloseDate.HasValue)
            {
                mOpporDoc.ClosingDate = pObject.CloseDate.Value;
                mOpporDoc.ClosingDateSpecified = true;
            }
            mOpporDoc.ClosingPercentage = pObject.CloPrcnt.HasValue ? Convert.ToDouble(pObject.CloPrcnt.Value, System.Globalization.CultureInfo.InstalledUICulture) : 0;
            mOpporDoc.ClosingPercentageSpecified = true;
            mOpporDoc.ProjectCode = pObject.PrjCode;
            mOpporDoc.Remarks = pObject.Memo;

            //Creo las lineas del documento
            SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[] newLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[pObject.OpportunitiesLines.Count];
            int positionNewLine = 0;
            foreach (OpportunitiesSAPLine portalLine in pObject.OpportunitiesLines)
            {
                SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine line = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine();

                line.LineNum = portalLine.Line;
                line.LineNumSpecified = true;

                line.StartDate = portalLine.OpenDate ?? DateTime.Now;
                line.StartDateSpecified = true;

                line.ClosingDate = portalLine.CloseDate ?? DateTime.Now;
                line.ClosingDateSpecified = true;

                line.SalesPerson = portalLine.SlpCode ?? -1;
                line.SalesPersonSpecified = true;

                line.StageKey = portalLine.Step_Id ?? 0;
                line.StageKeySpecified = true;

                line.PercentageRate = (double)(portalLine.ClosePrcnt.HasValue ? portalLine.ClosePrcnt.Value : 0);
                line.PercentageRateSpecified = true;

                line.MaxLocalTotal = (double)(portalLine.MaxSumLoc.HasValue ? portalLine.MaxSumLoc.Value : 0);
                line.MaxLocalTotalSpecified = true;

                line.WeightedAmountLocal = (double)(portalLine.WtSumLoc.HasValue ? portalLine.WtSumLoc.Value : 0);
                line.WeightedAmountLocalSpecified = true;

                switch (portalLine.Status)
                {
                    case "O":
                        line.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineStatus.so_Open;
                        break;
                    case "C":
                        line.Status = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineStatus.so_Closed;
                        break;
                }
                line.StatusSpecified = true;

                switch (portalLine.ObjType)
                {
                    case "17":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Order;
                        break;
                    case "23":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Quotation;
                        break;
                    case "13":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_Invoice;
                        break;
                    case "22":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseOrder;
                        break;
                    case "540000006":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseQutation;
                        break;
                    case "18":
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_PurchaseInvoice;
                        break;
                    default:
                        line.DocumentType = SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLineDocumentType.bodt_MinusOne;
                        break;
                }
                line.DocumentTypeSpecified = true;

                if (portalLine.DocNumber.HasValue)
                {
                    line.DocumentNumber = portalLine.DocNumber.Value;
                    line.DocumentNumberSpecified = true;
                }

                newLines[positionNewLine] = line;
                positionNewLine += 1;
            }

            mOpporDoc.SalesOpportunitiesLines = new SalesOpportunitiesServiceWeb.SalesOpportunitiesSalesOpportunitiesLine[newLines.Length];
            mOpporDoc.SalesOpportunitiesLines = newLines;

            try
            {
                oOpportunitiesService.Update(mOpporDoc);
                UpdateActivities(pObject, pCompanyParam);
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }


        private void UpdateActivities(OpportunitiesSAP pObject, CompanyConn pCompanyParam)
        {
            WebServices.Activities.ActivityServiceDS pActivitiesServ = new Activities.ActivityServiceDS();

            foreach (var item in pObject.OpportunitiesLines)
            {
                if (item.ActivitiesList.Count > 0)
                {
                    foreach (var mAct in item.ActivitiesList)
                    {
                        pActivitiesServ.UpdateSalesOpportunitiesActivity(mAct.ClgCode, pObject.OpprId, item.Line, pCompanyParam);
                    }
                }
            }
        }

        public List<OpportunitiesSAP> GetOpportunityListSearch(CompanyConn pCompanyParam, string pCodeBP = "", int? pSalesEmp = null, string pOpportunityName = "", int? pOwner = null, DateTime? pDate = null, string pStatus = "")
        {
            try
            {
                Mapper.CreateMap<OOPR, OpportunitiesSAP>();

                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                List<OOPR> ListRestu = Mapper.Map<List<OOPR>>(mDbContext.OOPR.Where(c => (c.CardCode.Contains(pCodeBP)) && (pSalesEmp != null ? (c.SlpCode == pSalesEmp) : true) && (pDate == null ? c.OpenDate != null : c.OpenDate >= pDate) && (pStatus != "" ? c.Status == pStatus : true) && (pOwner != null ? (c.Owner == pOwner) : true) && (!string.IsNullOrEmpty(pOpportunityName) ? c.Name.ToUpper().Contains(pOpportunityName.ToUpper()) : true)).ToList());

                return Mapper.Map<List<OpportunitiesSAP>>(ListRestu);
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }


        #endregion


        #region Projects

        public List<ProjectSAP> GetProjectsSAPList(CompanyConn pCompanyParam, string pProjectCode, string pProjectName)
        {
            try
            {
                mDbContext.Database.Connection.ConnectionString = Utils.ConnectionString(pCompanyParam);
                mDbContext.Database.Connection.Open();

                IMapper mapper = new MapperConfiguration(cfg => { cfg.CreateMap<OPRJ, ProjectSAP>(); }).CreateMapper();

                return mapper.Map<List<ProjectSAP>>(mDbContext.OPRJ.Where(c => (!string.IsNullOrEmpty(pProjectCode) ? c.PrjCode.Contains(pProjectCode) : true) && (!string.IsNullOrEmpty(pProjectName) ? c.PrjName.Contains(pProjectName) : true)).ToList());
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> GetProjectsSAPList :" + ex.Message);
                return null;
            }
            finally
            {
                if (mDbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                {
                    mDbContext.Database.Connection.Close();
                }
            }
        }

        #endregion

        //private List<object> GetObjectListWithUDF(List<string> pListUDF, string pWhere, Type pObjectType, string pConnectionString)
        //{
        //    //Dictionary<string, string> mDictionaryAux = new Dictionary<string, string>();
        //    //if (pListUDF != null)
        //    //{
        //    //    foreach (string mUDFAux in pListUDF)
        //    //        mDictionaryAux.Add(mUDFAux, null);
        //    //}
        //    //string sqlquery = UDFUtil.getSelectQuery(pObjectType, pObjectType.Name, mDictionaryAux, pWhere);
        //    //List<object> mAuxList = UDFUtil.GetDBObjectList(sqlquery, pConnectionString, mDictionaryAux, pObjectType);

        //    List<UDF_ARGNS> mListUDF = new List<UDF_ARGNS>();

        //    if (pListUDF != null)
        //    {
        //        foreach (string mUDFAux in pListUDF)
        //            mListUDF.Add(new UDF_ARGNS(mUDFAux, null));
        //    }
        //    string sqlquery = UDFUtil.getSelectQuery(pObjectType, pObjectType.Name, mListUDF, pWhere);
        //    //List<object> mAuxList = UDFUtil.GetDBObjectList(sqlquery, pConnectionString, mListUDF, pObjectType);
        //    //GetDBObjectList(string pSQLQuery, string pConnectionString, List<UDF_ARGNS> pListUDF, Type pObjType, List<UserUDF> pListUserUDF, string pTableName)

        //    //return mAuxList;
        //    return new List<object>();
        //}

        private XmlDocument GetSoapStructure(bool pIsUpdate, object pHeader, object pBody)
        {
            string body = string.Empty;
            XmlSerializer mySerializer = new XmlSerializer(pBody.GetType(), @"http://www.sap.com/SBO/DIS");
            var xns = new XmlSerializerNamespaces();
            xns.Add(string.Empty, string.Empty);
            xns.Add(string.Empty, @"http://www.sap.com/SBO/DIS");

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = Encoding.GetEncoding("UTF-8")
            };

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, xmlWriterSettings))
                {
                    mySerializer.Serialize(xmlWriter, pBody, xns);
                }

                body = stringWriter.ToString();
            }

            MemoryStream streamDocs = new MemoryStream();
            mySerializer.Serialize(streamDocs, pBody, xns);
            XmlDocument mXmlDocument = new XmlDocument();
            mXmlDocument.LoadXml(@"<soap:Envelope xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""><soap:Header></soap:Header><soap:Body><" + (pIsUpdate ? "Update" : "Add") + @" xmlns=""" + pHeader.GetType().GetProperty("ServiceName").GetValue(pHeader, null) + @"""></" + (pIsUpdate ? "Update" : "Add") + "></soap:Body></soap:Envelope>");
            XmlElement mBodyElement = mXmlDocument.DocumentElement;

            mBodyElement.SelectSingleNode("//*[local-name()='" + (pIsUpdate ? "Update" : "Add") + "']").InnerXml = (body).Remove(0, 22);

            streamDocs = new MemoryStream();

            mySerializer = new XmlSerializer(pHeader.GetType(), @"http://www.sap.com/SBO/DIS");
            mySerializer.Serialize(streamDocs, pHeader, xns);

            mBodyElement.SelectSingleNode("//*[local-name()='Header']").InnerXml = (Encoding.Default.GetString(streamDocs.ToArray())).Remove(0, 22);

            return mXmlDocument;

        }

        //private string AddUDFToXml(string pXml, Dictionary<string, string> pDictionary)
        //{
        //    foreach (var mDictRow in pDictionary)
        //    {
        //        pXml = pXml + "<" + mDictRow.Key + ">" + mDictRow.Value + "</" + mDictRow.Key + ">";
        //    }

        //    return pXml;
        //}


        private List<UnitOfMeasure> GetUOMLists(int pUgpEntry)
        {
            List<UnitOfMeasure> oUnitOfMeasure = new List<UnitOfMeasure>();
            if (pUgpEntry != -1)
            {
                oUnitOfMeasure.AddRange((from mUGP1 in mDbContext.UGP1
                                         join mUOM in mDbContext.OUOM on mUGP1.UomEntry equals mUOM.UomEntry
                                         where mUGP1.UgpEntry == pUgpEntry
                                         select new UnitOfMeasure()
                                         {
                                             UomEntry = mUOM.UomEntry,
                                             UomCode = mUOM.UomCode,
                                             UomName = mUOM.UomName,
                                             LineNum = mUGP1.LineNum,
                                             BaseQty = mUGP1.BaseQty,
                                             AltQty = mUGP1.AltQty
                                         }).ToList());
            }
            else
            {
                oUnitOfMeasure.Add(new UnitOfMeasure() { UomEntry = -1, UomCode = "Manual", UomName = "Manual", LineNum = 1, BaseQty = 1 });
            }
            return oUnitOfMeasure;
        }


        #region Payment Means

        private bool AddPaymentByOrder(CompanyConn pCompanyParam, List<PaymentMeanOrder> listPaymentOrder, int type, int docEntry, int docnum)
        {

            try
            {

                List<PaymentMeanOrder> paymentMeanOrder = mDbContext.Database.SqlQuery<PaymentMeanOrder>($"SELECT * FROM [@ARGNS_POORDERPAYME] where U_DocType = {type} and U_DocEntryOrder = {docEntry}").ToList<PaymentMeanOrder>();

                OrderByPaymentWeb.MsgHeader headerOrderByPaymentWeb = new OrderByPaymentWeb.MsgHeader();
                headerOrderByPaymentWeb.ServiceName = OrderByPaymentWeb.MsgHeaderServiceName.ARGNS_POORDERPAYME;
                headerOrderByPaymentWeb.ServiceNameSpecified = true;
                headerOrderByPaymentWeb.SessionID = pCompanyParam.DSSessionId;

                OrderByPaymentWeb.ARGNS_POORDERPAYME OrderByPaymentService = new OrderByPaymentWeb.ARGNS_POORDERPAYME();
                OrderByPaymentService.MsgHeaderValue = headerOrderByPaymentWeb;

                foreach (var item in paymentMeanOrder)
                {
                    OrderByPaymentService.Remove(new OrderByPaymentWeb.ARGNS_POORDERPAYMEParams { DocEntry = item.DocEntry, DocEntrySpecified = true });
                }

                foreach (var item in listPaymentOrder)
                {
                    OrderByPaymentService.Add(new OrderByPaymentWeb.ARGNS_POORDERPAYME1
                    {
                        U_Account = item.U_Account,
                        U_AccountSpecified = true,
                        U_Date = item.U_Date ?? System.DateTime.Now,
                        U_DateSpecified = item.U_Date == null ? false : true,
                        U_CUIT = item.U_CUIT,
                        U_CUITSpecified = true,
                        U_DueDate = item.U_DueDate ?? System.DateTime.Now,
                        U_DueDateSpecified = item.U_DueDate == null ? false : true,
                        U_Reference = item.U_Reference,
                        U_ReferenceSpecified = true,
                        U_DocNumOrder = docnum,
                        u_DocNumOrderSpecified = true,
                        U_AmountSurcharger = Convert.ToDouble(item.U_AmountSurcharger),
                        U_AmountSurchargerSpecified = true,
                        U_Amount = Convert.ToDouble(item.U_Amount),
                        U_AmountSpecified = true,
                        U_DocEntryOrder = docEntry,
                        U_DocEntryOrderSpecified = true,
                        U_DocType = type,
                        U_DocTypeSpecified = true,
                        U_PaymentCode = item.U_PaymentCode,
                        U_CreditCard = item.U_CreditCard,
                        U_CreditCardSpecified = true,
                        U_Voucher = item.U_Voucher,
                        U_VoucherSpecified = true,
                        U_Quotas = item.U_Quotas ?? 0,
                        U_QuotasSpecified = true,
                        U_Neto = Convert.ToDouble(item.U_Neto),
                        U_NetoSpecified = true,
                        U_Contract = item.U_Contract ?? 0,
                        U_ContractSpecified = true
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.WriteError("DocumentServiceDS -> AddPaymentByOrder:" + ex.Message);

                return false;
            }

            return true;
        }

        public List<PaymentMeanOrder> GetForDraft(CompanyConn pCompanyParam, int pCode)
        {
            List<PaymentMeanOrder> listRetrun = new List<PaymentMeanOrder>();

            string mConnectionString = Utils.ConnectionString(pCompanyParam);
            mDbContext.Database.Connection.ConnectionString = mConnectionString;
            mDbContext.Database.Connection.Open();

            var udfIdPayment = WebConfigurationManager.AppSettings["udfIdPayment"];

            if (udfIdPayment != "")
            {
                var idPayment = mDbContext.Database.SqlQuery<string>($"SELECT {udfIdPayment} FROM ODRF where DocEntry = {pCode}").FirstOrDefault();

                listRetrun = mDbContext.Database.SqlQuery<PaymentMeanOrder>($"SELECT * FROM [@ARGNS_POORDERPAYME] where U_DocType = {17} and U_DocEntryOrder = {idPayment}").ToList<PaymentMeanOrder>();
            }


            return listRetrun;
        }


        #endregion



    }
}