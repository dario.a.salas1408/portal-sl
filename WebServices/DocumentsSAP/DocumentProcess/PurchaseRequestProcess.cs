﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace WebServices.DocumentsSAP.DocumentProcess
{
    public class PurchaseRequestProcess
    {
        public string AddPurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return "";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdatePurchaseRequest(PurchaseRequestSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return "";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }
    }
}









































        //public string AddActivity(ActivitiesSAP pObject, string pSession, string pUser)
        //{
        //    string mXmlResult = string.Empty;
        //    string ret = string.Empty;
        //    string pXmlString = string.Empty;
        //    string AddCmd = string.Empty;
        //    SBODI_Server.Node mServerNode = null;
        //    XmlDocument mResultXML = null, pXML = null;
        //    int mTransType;
        //    try
        //    {
        //        if (GetXMLFileDIServer(pObject, out pXmlString, pUser))
        //        {
        //            mResultXML = new XmlDocument();
        //            mServerNode = new SBODI_Server.Node();

        //            //Leo el XML File Generado
        //            pXML = new System.Xml.XmlDocument();
        //            pXML.LoadXml(pXmlString);

        //            //Armo la cabecera del XML
        //            mTransType = (int)UtilWeb.TransactionType.Add;
        //            AddCmd = GetActivityHeader(pSession, pXML, mTransType, "ActivitiesService");

        //            //Envio Solicitud al DI Server y Obtengo la Respuesta
        //            mXmlResult = mServerNode.Interact(AddCmd);
        //            mResultXML.LoadXml(mXmlResult);

        //        }

        //        //Recupero los Valores de la Respuesta                
        //        return UtilWeb.GetResult(mResultXML); ;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error:" + ex.Message;
        //    }
        //    finally
        //    {
        //        Utils.ReleaseObject((object)mServerNode);
        //    }
        //}

        ////Actualizar Activity
        //public string UpdateActivity(ActivitiesSAP pObject, string pSession, string pUser)
        //{
        //    string mXmlResult = string.Empty;
        //    string ret = string.Empty;
        //    string pXmlString = string.Empty;
        //    string AddCmd = string.Empty;
        //    SBODI_Server.Node mServerNode = null;
        //    XmlDocument mResultXML = null, pXML = null;
        //    int mTransType;
        //    try
        //    {

        //        mResultXML = new XmlDocument();
        //        pXML = new System.Xml.XmlDocument();
        //        mServerNode = new SBODI_Server.Node();

        //        //GetByParams
        //        AddCmd = GetByParams(pSession, "ActivitiesService", pObject.ClgCode.ToString());
        //        mResultXML.LoadXml(mServerNode.Interact(AddCmd));               
        //        mXmlResult = mResultXML.SelectSingleNode("//*[local-name()='Activity']").OuterXml.ToString();

        //        //Configurar XML
        //        pXML = ReplaceXMLServer(pObject, mXmlResult);

        //        //Armo la cabecera del XML
        //        mTransType = (int)UtilWeb.TransactionType.Update;
        //        AddCmd = GetActivityHeader(pSession, pXML, mTransType, "ActivitiesService");               

        //        //Envio Solicitud al DI Server y Obtengo la Respuesta
        //        mXmlResult = mServerNode.Interact(AddCmd);
        //        mResultXML.LoadXml(mXmlResult);

        //        //Recupero los Valores de la Respuesta                
        //        return UtilWeb.GetResult(mResultXML);

        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error:" + ex.Message;
        //    }
        //    finally
        //    {
        //        Utils.ReleaseObject((object)mServerNode);
        //    }
        //}

        //public static string GetActivityHeader(string pSession, XmlDocument pXML, int pType, string pObjectType)
        //{
        //    string ret = string.Empty;

        //    switch (pType)
        //    {
        //        case 1:
        //            ret = @"<?xml version=""1.0"" encoding=""UTF-16"" ?>" +
        //              @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
        //              "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
        //              @"<env:Body><dis:AddActivity  xmlns:dis=""http://www.sap.com/SBO/DIS""><Service>" + pObjectType + "</Service>" + pXML.InnerXml + "</dis:AddActivity></env:Body></env:Envelope>";
        //            break;
        //        case 2:

        //            ret = @"<?xml version=""1.0"" encoding=""UTF-16"" ?>" +
        //              @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
        //              "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
        //              @"<env:Body><dis:UpdateActivity  xmlns:dis=""http://www.sap.com/SBO/DIS""><Service>" + pObjectType + "</Service>" + pXML.InnerXml + "</dis:UpdateActivity></env:Body></env:Envelope>";
        //            break;
        //    }

        //    return ret;
        //}

        //private XmlDocument ReplaceXMLServer(ActivitiesSAP pObject, string pXmlString)
        //{
        //    XmlDocument pXML;
        //    XmlDocument pXMLNuevo;

        //    try
        //    {
        //        StringBuilder mSB = new StringBuilder(pXmlString);
        //        mSB.Remove(9, 35);

        //        pXML = new XmlDocument();
        //        pXML.LoadXml(mSB.ToString());


        //        //ActivityCode                
        //        pXML.SelectSingleNode("Activity/ActivityCode").InnerText = pObject.ClgCode.ToString();

        //        //CardCode                
        //        pXML.SelectSingleNode("Activity/CardCode").InnerText = pObject.CardCode;

        //        //StartDate
        //        pXML.SelectSingleNode("Activity/StartDate").InnerText = pObject.Recontact.HasValue ? pObject.Recontact.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");

        //        //EndDuedate
        //        pXML.SelectSingleNode("Activity/EndDueDate").InnerText = pObject.endDate.HasValue ? pObject.endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");

        //        //Activity
        //        switch (pObject.Action)
        //        {
        //            case "C":
        //                pXML.SelectSingleNode("Activity/Activity").InnerText = "cn_Conversation";
        //                break;
        //            case "M":
        //                pXML.SelectSingleNode("Activity/Activity").InnerText = "cn_Meeting";
        //                break;
        //            case "T":
        //                pXML.SelectSingleNode("Activity/Activity").InnerText = "cn_Task";
        //                break;
        //            case "E":
        //                pXML.SelectSingleNode("Activity/Activity").InnerText = "cn_Note";
        //                break;
        //            case "P":
        //                pXML.SelectSingleNode("Activity/Activity").InnerText = "cn_Campaign";
        //                break;
        //            case "N":
        //                pXML.SelectSingleNode("Activity/Activity").InnerText = "cn_Other";
        //                break;
        //            default:
        //                pXML.SelectSingleNode("Activity/Activity").InnerText = "cn_Conversation";
        //                break;
        //        }

        //        //ActivityType
        //        pXML.SelectSingleNode("Activity/ActivityType").InnerText = (pObject.CntctType.HasValue && pObject.CntctType.Value != 0) ? pObject.CntctType.Value.ToString() : "-1";

        //        //Subject
        //        pXML.SelectSingleNode("Activity/Subject").InnerText = (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0) ? pObject.CntctSbjct.Value.ToString() : "-1";

        //        //Notes
        //        pXML.SelectSingleNode("Activity/Notes").InnerText = pObject.Notes;

        //        //Details
        //        pXML.SelectSingleNode("Activity/Details").InnerText = pObject.Details;

        //        //Priority
        //        switch (pObject.Priority)
        //        {
        //            case "0":
        //                pXML.SelectSingleNode("Activity/Priority").InnerText = "pr_Low";
        //                break;
        //            case "1":
        //                pXML.SelectSingleNode("Activity/Priority").InnerText = "pr_Normal";
        //                break;
        //            case "2":
        //                pXML.SelectSingleNode("Activity/Priority").InnerText = "pr_High";
        //                break;
        //            default:
        //                pXML.SelectSingleNode("Activity/Priority").InnerText = "pr_Normal";
        //                break;
        //        }


        //        //Closed
        //        pXML.SelectSingleNode("Activity/Closed").InnerText = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? "tYES" : "tNO";

        //        //Inactiveflag
        //        pXML.SelectSingleNode("Activity/InactiveFlag").InnerText = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? "tYES" : "tNO";
                               

        //        pXML.SelectSingleNode("Activity").RemoveChild(pXML.SelectSingleNode("Activity/HandledBy"));
        //        pXML.SelectSingleNode("Activity").RemoveChild(pXML.SelectSingleNode("Activity/HandledByEmployee"));        

        //        //AttendUser  
        //        XmlNode mHandle;
        //        if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
        //        {
        //            mHandle = pXML.CreateNode(XmlNodeType.Element, "HandledBy", String.Empty);
        //            mHandle.InnerText = pObject.AttendUser.Value.ToString();
        //            pXML.SelectSingleNode("Activity").AppendChild(mHandle);                   
                  
        //        }
        //        else
        //        {
        //            mHandle = pXML.CreateNode(XmlNodeType.Element, "HandledBy", String.Empty);
        //            mHandle.InnerText = string.Empty;
        //            XmlAttribute mNil = pXML.CreateAttribute("nil");
        //            mNil.Value = "true";
        //            mHandle.Attributes.Append(mNil);
        //            pXML.SelectSingleNode("Activity").AppendChild(mHandle);
        //        }

        //        //HandledByEmployee
        //        if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
        //        {
        //            mHandle = pXML.CreateNode(XmlNodeType.Element, "HandledByEmployee", String.Empty);
        //            mHandle.InnerText = pObject.AttendEmpl.Value.ToString();
        //            pXML.SelectSingleNode("Activity").AppendChild(mHandle);
        //        }
        //        else
        //        {
        //            mHandle = pXML.CreateNode(XmlNodeType.Element, "HandledByEmployee", String.Empty);
        //            mHandle.InnerText = string.Empty;
        //            XmlAttribute mNil = pXML.CreateAttribute("nil");
        //            mNil.Value = "true";
        //            mHandle.Attributes.Append(mNil);
        //            pXML.SelectSingleNode("Activity").AppendChild(mHandle);
        //        }


        //        return pXML;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        ////Obtengo GetByParams
        //private string GetByParams(string pSession, string pObjectType, string pCode)
        //{
        //    string ret = string.Empty;
        //    try
        //    {

        //        ret = @"<?xml version=""1.0"" ?>" +
        //        @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
        //        "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
        //        @"<env:Body><dis:GetActivity  xmlns:dis=""http://www.sap.com/SBO/DIS""><Service>" + pObjectType + "</Service><ActivityParams><ActivityCode>" + pCode + "</ActivityCode></ActivityParams></dis:GetActivity></env:Body></env:Envelope>";

        //        return ret;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteError("ProductDataProcess -> ConfigureXML :" + ex.Message);
        //        return ret;
        //    }

        //}

        //private bool GetXMLFileDIServer(ActivitiesSAP pObject, out string pXmlFile, string pUser)
        //{
        //    mXmlFile = "<Activity></Activity>";
        //    XmlNode xmlRow;
        //    bool ret = false;
        //    try
        //    {
        //        mXmlDocument = new XmlDocument();
        //        mXmlDocument.LoadXml(mXmlFile);

        //        xmlRow = mXmlDocument.SelectSingleNode("Activity");

        //        //ActivityCode                
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("ActivityCode")).InnerText = pObject.ClgCode.ToString();

        //        //CardCode                
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("CardCode")).InnerText = pObject.CardCode;

        //        //StartDate
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("StartDate")).InnerText = pObject.Recontact.HasValue ? pObject.Recontact.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");

        //        //EndDuedate
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("EndDueDate")).InnerText = pObject.endDate.HasValue ? pObject.endDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");

        //        //Activity
        //        switch (pObject.Action)
        //        {
        //            case "C":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Activity")).InnerText = "cn_Conversation";
        //                break;
        //            case "M":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Activity")).InnerText = "cn_Meeting";
        //                break;
        //            case "T":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Activity")).InnerText = "cn_Task";
        //                break;
        //            case "E":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Activity")).InnerText = "cn_Note";
        //                break;
        //            case "P":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Activity")).InnerText = "cn_Campaign";
        //                break;
        //            case "N":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Activity")).InnerText = "cn_Other";
        //                break;
        //            default:
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Activity")).InnerText = "cn_Conversation";
        //                break;
        //        }

        //        //ActivityType
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("ActivityType")).InnerText = (pObject.CntctType.HasValue && pObject.CntctType.Value != 0) ? pObject.CntctType.Value.ToString() : "-1";

        //        //Subject
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("Subject")).InnerText = (pObject.CntctSbjct.HasValue && pObject.CntctSbjct.Value != 0) ? pObject.CntctSbjct.Value.ToString() : "-1";

        //        //Notes
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("Notes")).InnerText = pObject.Notes;

        //        //Details
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("Details")).InnerText = pObject.Details;

        //        //Priority
        //        switch (pObject.Priority)
        //        {
        //            case "0":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Priority")).InnerText = "pr_Low";
        //                break;
        //            case "1":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Priority")).InnerText = "pr_Normal";
        //                break;
        //            case "2":
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Priority")).InnerText = "pr_High";
        //                break;
        //            default:
        //                xmlRow.AppendChild(mXmlDocument.CreateElement("Priority")).InnerText = "pr_Normal";
        //                break;
        //        }


        //        //Closed
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("Closed")).InnerText = (!string.IsNullOrEmpty(pObject.Closed) && pObject.Closed == "Y") ? "tYES" : "tNO";

        //        //Inactiveflag
        //        xmlRow.AppendChild(mXmlDocument.CreateElement("InactiveFlag")).InnerText = (!string.IsNullOrEmpty(pObject.inactive) && pObject.inactive == "Y") ? "tYES" : "tNO";


        //        //AttendUser                
        //        if (pObject.AttendUser.HasValue && pObject.AttendUser.Value != 0)
        //        {
        //            xmlRow.AppendChild(mXmlDocument.CreateElement("HandledBy")).InnerText = pObject.AttendUser.Value.ToString();
        //        }
        //        else
        //        {
        //            XmlNode mHandle = xmlRow.AppendChild(mXmlDocument.CreateElement("HandledBy"));
        //            mHandle.InnerText = string.Empty;
        //            XmlAttribute mNil = mXmlDocument.CreateAttribute("nil");
        //            mNil.Value = "true";
        //            mHandle.Attributes.Append(mNil);
        //        }


        //        //HandledByEmployee
        //        if (pObject.AttendEmpl.HasValue && pObject.AttendEmpl.Value != 0)
        //        {
        //            xmlRow.AppendChild(mXmlDocument.CreateElement("HandledByEmployee")).InnerText = pObject.AttendEmpl.Value.ToString();
        //        }
        //        else
        //        {
        //            XmlNode mHandle = xmlRow.AppendChild(mXmlDocument.CreateElement("HandledByEmployee"));
        //            mHandle.InnerText = string.Empty;
        //            XmlAttribute mNil = mXmlDocument.CreateAttribute("nil");
        //            mNil.Value = "true";
        //            mHandle.Attributes.Append(mNil);
        //        }



        //        pXmlFile = mXmlDocument.InnerXml;
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        pXmlFile = string.Empty;
        //        return ret;
        //    }
        //}

        ////Eliminar Activity
        //public string DeleteActivity(string pCode, string pSession)
        //{
        //    string mXmlResult = string.Empty;
        //    string ret = string.Empty;
        //    string AddCmd = null;
        //    SBODI_Server.Node mServerNode = null;
        //    XmlDocument mResultXML = null;
        //    XmlDocument pXML = null;

        //    try
        //    {
        //        mResultXML = new XmlDocument();
        //        mServerNode = new SBODI_Server.Node();

        //        //XML Remove
        //        string mXmlString = "<BOM><BO><AdmInfo><Object>oContacts</Object></AdmInfo><QueryParams><ContactCode>" + pCode + "</ContactCode></QueryParams></BO></BOM>";
        //        pXML = new System.Xml.XmlDocument();
        //        pXML.LoadXml(mXmlString);

        //        //Armo la cabecera del XML  
        //        AddCmd = UtilWeb.GetHeader(pSession, pXML, (int)UtilWeb.TransactionType.Delete);

        //        //Envio Solicitud al DI Server y Obtengo la Respuesta
        //        mXmlResult = mServerNode.Interact(AddCmd);
        //        mResultXML.LoadXml(mXmlResult);

        //        //Recupero los Valores de la Respuesta            
        //        return UtilWeb.GetResult(mResultXML);

        //    }
        //    catch (Exception ex)
        //    {
        //        return "Error:" + ex.Message;
        //    }
        //    finally
        //    {
        //        Utils.ReleaseObject((object)mServerNode);
        //    }
        //}