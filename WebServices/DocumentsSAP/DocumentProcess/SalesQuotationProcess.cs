﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Model.Implementations.View;
using ARGNS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WebServices.DocumentsSAP
{
    public class SalesQuotationProcess
    {
        public string AddSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string UpdateSalesQuotation(SalesQuotationSAP pObject, CompanyConn pCompanyParam)
        {
            try
            {
                return "Ok";
            }

            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }
    }
}