﻿using ARGNS.Model.Implementations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebServices.DocumentsSAP.DocumentProcess
{
    public class ApprovalProcess
    {
        #region DI Server

        public string SaveApprovalResponse(int owwdCode, string remark, string UserNameSAP, string UserPasswordSAP, string approvalCode, CompanyConn pCompanyParam)
        {
            B1IFDocumentService.UpdateApprovalType request = new B1IFDocumentService.UpdateApprovalType();
            B1IFDocumentService.UpdateApprovalResponseType response = new B1IFDocumentService.UpdateApprovalResponseType();
            B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();
            try
            {

                request.ApproverPassword = UserPasswordSAP;
                request.ApproverUserName = UserNameSAP;
                request.Code = owwdCode.ToString();
                request.DBName = pCompanyParam.CompanyDB;
                request.DbTypeNumber = pCompanyParam.ServerType.ToString();
                request.Decision = approvalCode;
                request.LicenseServer = pCompanyParam.LicenseServer;
                request.LSPort = pCompanyParam.PortNumber.ToString();
                request.PathToDll = ConfigurationManager.AppSettings["PathToDll"];
                request.Remarks = remark;    
                request.SAPPassword = pCompanyParam.Password;
                request.SAPServer = pCompanyParam.Server;
                request.SAPUser = pCompanyParam.UserName;

                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSUpdateApproval(request);

                return "Ok";
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }

        }

        #endregion
    }
}