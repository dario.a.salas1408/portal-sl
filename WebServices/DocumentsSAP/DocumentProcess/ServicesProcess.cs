﻿using ARGNS.Model.Implementations;
using ARGNS.Model.Implementations.SAP;
using ARGNS.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using WebServices.Model;
using WebServices.Query;

namespace WebServices.DocumentsSAP
{
    public class ServicesProcess
    {
        #region DI Server
        private B1WSHandler mB1WSHandler;
        public ServicesProcess()
        {
            mB1WSHandler = new B1WSHandler();
        }

        public string GetSystemCurrency(CompanyConn pCompanyParam)
        {
            try
            {
                B1IFDocumentService.GetSystemCurrencyType request = new B1IFDocumentService.GetSystemCurrencyType();
                B1IFDocumentService.GetSystemCurrencyResponseType response = new B1IFDocumentService.GetSystemCurrencyResponseType();
                B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();


                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetSystemCurrency(request);

                return response.Currency;
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string GetLocalCurrency(CompanyConn pCompanyParam)
        {

            try
            {
                B1IFDocumentService.GetLocalCurrencyType request = new B1IFDocumentService.GetLocalCurrencyType();
                B1IFDocumentService.GetLocalCurrencyResponseType response = new B1IFDocumentService.GetLocalCurrencyResponseType();
                B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();


                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                response = llamadaServicio.ARGNSGetLocalCurrency(request);

                return response.Currency;
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public string GetCurrencyRate(string pCurr, DateTime pDate, CompanyConn pCompanyParam)
        {
            try
            {
                B1IFDocumentService.GetCurrencyRateType request = new B1IFDocumentService.GetCurrencyRateType();
                B1IFDocumentService.GetCurrencyRateResponseType response = new B1IFDocumentService.GetCurrencyRateResponseType();
                B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient llamadaServicio = new B1IFDocumentService.ipostep_vP001sap0003in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient();


                llamadaServicio.ClientCredentials.UserName.UserName = pCompanyParam.SAPLanguaje + "/" + pCompanyParam.UserName + "/" + pCompanyParam.CompanyDB;
                llamadaServicio.ClientCredentials.UserName.Password = pCompanyParam.Password;
                request.RateParams = pCurr + "," + pDate.ToString("yyyy-MM-dd");
                response = llamadaServicio.ARGNSGetCurrencyRate(request);

                return response.Rate;
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public List<ItemMasterSAP> GetItemPriceInBatch(List<ItemMasterSAP> pItems, string pCardCode, string pSession, bool pOrderItems, bool isSO, string ano = "")
        {
            try
            {
                string AddCmd = string.Empty;
                int? uomDefault;
                SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                XmlDocument mResultXML = new XmlDocument();
                if (pOrderItems == true)
                {
                    pItems = pItems.OrderBy(c => c.ItemCode).ToList();
                }
                AddCmd = GetItemsPriceInBatchXML(pSession, pCardCode, pItems.Select(c => c.ItemCode).ToList());
                mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                int i = 0;

                Logger.WriteInfo("GetItemPriceInBatch");

                foreach (XmlNode node in mResultXML.GetElementsByTagName("row"))
                {
                    ItemPrices oItemPrices = new ItemPrices();
                    oItemPrices.Price = Convert.ToDecimal(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);

                    Logger.WriteInfo(" Test" + node.FirstChild.InnerText);

                    var udfExternalPrice = WebConfigurationManager.AppSettings["udfExternalPrice"];

                    var externalPrice = pItems.Where(c => c.ItemCode == pItems[i].ItemCode).FirstOrDefault().MappedUdf.Where(c => c.UDFName == udfExternalPrice).FirstOrDefault();



                    if (externalPrice != null)
                    {
                        Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice" + externalPrice.Value);
                        //Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice");
                        if (externalPrice.Value == "Y")
                        {
                            Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice SI");

                            var query = string.Format(QueryResource.QueryGet("externalPrice"), pItems[i].ItemCode, ano);

                            Logger.WriteInfo(query);

                            XmlDocument xml = new XmlDocument();
                            xml.LoadXml(mB1WSHandler.ExecuetQuery(pSession, query));



                            var nodes = xml.SelectNodes("//*[local-name()='row']");
                            foreach (XmlNode row in nodes)
                            {
                                Logger.WriteInfo("GetItemPriceInBatch" + row.SelectSingleNode(".//*[local-name()='U_importe']").InnerText);

                                oItemPrices.Price = Convert.ToDecimal(row.SelectSingleNode(".//*[local-name()='U_importe']").InnerText, System.Globalization.CultureInfo.InvariantCulture);
                                // oItemPrices.Price = Convert.ToDecimal("137024.000000");
                            }

                        }
                        else
                        {
                            Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice No");
                        }
                    }

                    oItemPrices.Currency = node.LastChild.InnerText.Trim();
                    oItemPrices.ItemCode = pItems[i].ItemCode;

                    if (isSO)
                    {
                        if (pItems[i].SUoMEntry == null)
                        {
                            uomDefault = pItems[i].PriceUnit;
                        }
                        else
                        {
                            uomDefault = pItems[i].SUoMEntry;
                        }
                    }
                    else
                    {
                        if (pItems[i].PUoMEntry == null)
                        {
                            uomDefault = pItems[i].PriceUnit;
                        }
                        else
                        {
                            uomDefault = pItems[i].PUoMEntry;
                        }
                    }

                    oItemPrices.UOMCode = pItems[i].UnitOfMeasureList.Where(c => c.UomEntry == uomDefault).FirstOrDefault().UomEntry.ToString();
                    oItemPrices.HasUOMPrice = false;
                    pItems[i].ItemPrices.Add(oItemPrices);
                    i++;
                }

                return pItems;
            }
            catch (Exception ex)
            {
                return pItems;
            }
        }

        public List<ItemMasterSAP> GetItemPriceInBatchSL(List<ItemMasterSAP> pItems, string pCardCode, string pSession, bool pOrderItems, bool isSO, string ano = "", CompanyConn pCompanyParam = null)
        {
            try
            {
                int? uomDefault;

                foreach (var item in pItems)
                {
                    ItemPrices oItemPrices = new ItemPrices();

                    var body = "{\"ItemPriceParams\": {\"CardCode\": \"" + pCardCode + "\",\"ItemCode\": \"" + item.ItemCode + "\"}}";

                    var resultAPI = SLManager.callAPI(pCompanyParam, "CompanyService_GetItemPrice", RestSharp.Method.POST, body).Content;

                    var objResult = JsonConvert.DeserializeObject<ItemPriceResult>(resultAPI);

                    oItemPrices.Price = Convert.ToDecimal(objResult.Price, System.Globalization.CultureInfo.InvariantCulture);

                    var udfExternalPrice = WebConfigurationManager.AppSettings["udfExternalPrice"];
                    var externalPrice = pItems.Where(c => c.ItemCode == item.ItemCode).FirstOrDefault().MappedUdf.Where(c => c.UDFName == udfExternalPrice).FirstOrDefault();
                    if (externalPrice != null)
                    {
                        if (externalPrice.Value == "Y")
                        {
                            var endPOint = $"listaespmotos?$filter=contains(U_articulo, '{item.ItemCode}') and contains(U_ano, '{ano}')";
                            var resultExtePrice = SLManager.callAPI(pCompanyParam, endPOint, RestSharp.Method.GET, "").Content;
                            var extPrice = JsonConvert.DeserializeObject<ItemPriceExternal.Root>(resultExtePrice).value.FirstOrDefault();

                            if(extPrice != null)
                            {
                                oItemPrices.Price = Convert.ToDecimal(extPrice.U_importe, System.Globalization.CultureInfo.InvariantCulture);
                            }
                            
                        }
                    }

                    oItemPrices.Currency = objResult.Currency;
                    oItemPrices.ItemCode = item.ItemCode;

                    if (isSO)
                    {
                        if (item.SUoMEntry == null)
                        {
                            uomDefault = item.PriceUnit;
                        }
                        else
                        {
                            uomDefault = item.SUoMEntry;
                        }
                    }
                    else
                    {
                        if (item.PUoMEntry == null)
                        {
                            uomDefault = item.PriceUnit;
                        }
                        else
                        {
                            uomDefault = item.PUoMEntry;
                        }
                    }


                    if (item.UnitOfMeasureList.Where(c => c.UomEntry == uomDefault).FirstOrDefault() != null)
                    {
                        oItemPrices.UOMCode = item.UnitOfMeasureList.Where(c => c.UomEntry == uomDefault).FirstOrDefault().UomEntry.ToString();
                    }
                    else
                    {
                        oItemPrices.UOMCode = "";
                    }

                    oItemPrices.HasUOMPrice = false;
                    item.ItemPrices.Add(oItemPrices);


                }



                //string AddCmd = string.Empty;
                //int? uomDefault;
                //SBODI_Server.Node mServerNode = new SBODI_Server.Node();
                //XmlDocument mResultXML = new XmlDocument();
                //if (pOrderItems == true)
                //{
                //    pItems = pItems.OrderBy(c => c.ItemCode).ToList();
                //}
                //AddCmd = GetItemsPriceInBatchXML(pSession, pCardCode, pItems.Select(c => c.ItemCode).ToList());
                //mResultXML.LoadXml(mServerNode.Interact(AddCmd));
                //int i = 0;

                //Logger.WriteInfo("GetItemPriceInBatch");

                //foreach (XmlNode node in mResultXML.GetElementsByTagName("row"))
                //{
                //    ItemPrices oItemPrices = new ItemPrices();
                //    oItemPrices.Price = Convert.ToDecimal(node.FirstChild.InnerText, System.Globalization.CultureInfo.InvariantCulture);

                //    Logger.WriteInfo(" Test" + node.FirstChild.InnerText);

                //    var udfExternalPrice = WebConfigurationManager.AppSettings["udfExternalPrice"];

                //    var externalPrice = pItems.Where(c => c.ItemCode == pItems[i].ItemCode).FirstOrDefault().MappedUdf.Where(c => c.UDFName == udfExternalPrice).FirstOrDefault();



                //    if (externalPrice != null)
                //    {
                //        Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice" + externalPrice.Value);
                //        //Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice");
                //        if (externalPrice.Value == "Y")
                //        {
                //            Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice SI");

                //            var query = string.Format(QueryResource.QueryGet("externalPrice"), pItems[i].ItemCode, ano);

                //            Logger.WriteInfo(query);

                //            XmlDocument xml = new XmlDocument();
                //            xml.LoadXml(mB1WSHandler.ExecuetQuery(pSession, query));



                //            var nodes = xml.SelectNodes("//*[local-name()='row']");
                //            foreach (XmlNode row in nodes)
                //            {
                //                Logger.WriteInfo("GetItemPriceInBatch" + row.SelectSingleNode(".//*[local-name()='U_importe']").InnerText);

                //                oItemPrices.Price = Convert.ToDecimal(row.SelectSingleNode(".//*[local-name()='U_importe']").InnerText, System.Globalization.CultureInfo.InvariantCulture);
                //                // oItemPrices.Price = Convert.ToDecimal("137024.000000");
                //            }

                //        }
                //        else
                //        {
                //            Logger.WriteInfo("GetItemPriceInBatch" + "externalPrice No");
                //        }
                //    }

                //    oItemPrices.Currency = node.LastChild.InnerText.Trim();
                //    oItemPrices.ItemCode = pItems[i].ItemCode;

                //    if (isSO)
                //    {
                //        if (pItems[i].SUoMEntry == null)
                //        {
                //            uomDefault = pItems[i].PriceUnit;
                //        }
                //        else
                //        {
                //            uomDefault = pItems[i].SUoMEntry;
                //        }
                //    }
                //    else
                //    {
                //        if (pItems[i].PUoMEntry == null)
                //        {
                //            uomDefault = pItems[i].PriceUnit;
                //        }
                //        else
                //        {
                //            uomDefault = pItems[i].PUoMEntry;
                //        }
                //    }

                //    oItemPrices.UOMCode = pItems[i].UnitOfMeasureList.Where(c => c.UomEntry == uomDefault).FirstOrDefault().UomEntry.ToString();
                //    oItemPrices.HasUOMPrice = false;
                //    pItems[i].ItemPrices.Add(oItemPrices);
                //    i++;
                //}

                return pItems;
            }
            catch (Exception ex)
            {
                return pItems;
            }
        }

        private string GetItemsPriceInBatchXML(string pSession, string pCardCode, List<string> pItemCodeList)
        {
            string mReturnXML = string.Empty;
            try
            {
                mReturnXML = @"<?xml version=""1.0"" ?>" +
                @"<env:Envelope xmlns:env=""http://schemas.xmlsoap.org/soap/envelope/"">" +
                "<env:Header>" + "<SessionID>" + pSession + "</SessionID>" + "</env:Header>" +
                @"<env:Body>";

                foreach (string mItem in pItemCodeList)
                {
                    mReturnXML += @"<dis:GetItemPrice xmlns:dis=""http://www.sap.com/SBO/DIS""><CardCode>" + pCardCode + "</CardCode><ItemCode>" + mItem + "</ItemCode></dis:GetItemPrice>";
                }

                mReturnXML += @"</env:Body></env:Envelope>";

                return mReturnXML;
            }
            catch (Exception ex)
            {
                Logger.WriteError("ServicesProcess -> GetItemsPriceInBatchXML :" + ex.Message);
                return mReturnXML;
            }
        }

        //public ItemMasterSAP GetItemPriceServiceLayer(ItemMasterSAP item, CompanyConn companyParam, string cardCode, DateTime? date, int priceList, object serviceLayer, bool inSP = false, string defaultUOM = "-1")
        //{
        //    try
        //    {
        //        ServiceLayerService sL = (ServiceLayerService)serviceLayer;

        //        int attempt = 2;

        //        //var listUOMs = item.ItemPrices.Where(c => c.UOMCode == defaultUOM.ToString()).Select(c => c.UOMCode).ToList();

        //        var listUOMs = new List<string>();

        //        listUOMs.Add(defaultUOM);

        //        if (item.SUoMEntry != null)
        //        {
        //            listUOMs.Add(item.SUoMEntry.ToString());
        //        }

        //        if (item.PUoMEntry != null)
        //        {
        //            listUOMs.Add(item.PUoMEntry.ToString());
        //        }

        //        var arrayUOMs = listUOMs.Select(c => c).ToArray();

        //        foreach (var itemUom in item.UnitOfMeasureList.Where(c => arrayUOMs.Contains(c.UomEntry.ToString())))
        //        {
        //            ItemPriceParams priceParam = new ItemPriceParams();
        //            priceParam.CardCode = cardCode;
        //            priceParam.ItemCode = item.ItemCode;
        //            priceParam.PriceList = priceList;

        //            if (!inSP)
        //            {
        //                //priceParam.Date = date;
        //                priceParam.UoMEntry = itemUom.UomEntry;
        //                //priceParam.UoMQuantity = 1;
        //            }

        //            var result = sL.GetItemPrice(priceParam);

        //            if (result.Price == 0)
        //            {
        //                for (int i = 0; i < attempt; i++)
        //                {

        //                    result = sL.GetItemPrice(priceParam);

        //                    Logger.WriteInfo("Get Item Price -> Item -> " + item.ItemCode + " -> Uom ->" + itemUom.UomCode + " -> attempt" + i.ToString());

        //                    if (result.Price != 0)
        //                    {
        //                        break;
        //                    }

        //                }
        //            }

        //            if (item.ItemPrices.Where(c => c.UOMCode == itemUom.UomEntry.ToString()).FirstOrDefault() != null)
        //            {
        //                item.ItemPrices.Where(c => c.UOMCode == itemUom.UomEntry.ToString()).FirstOrDefault().Discount = result.Discount ?? 0;
        //                item.ItemPrices.Where(c => c.UOMCode == itemUom.UomEntry.ToString()).FirstOrDefault().Price = Convert.ToDecimal(result.Price);
        //                item.ItemPrices.Where(c => c.UOMCode == itemUom.UomEntry.ToString()).FirstOrDefault().Currency = result.Currency;
        //            }
        //            else
        //            {
        //                item.ItemPrices.Add(new ItemPrices
        //                {
        //                    Currency = result.Currency,
        //                    ItemCode = item.ItemCode,
        //                    Price = (decimal)result.Price,
        //                    PriceList = (short)priceList,
        //                    HasUOMPrice = false,
        //                    UOMCode = item.UnitOfMeasureList.Where(c => c.UomEntry == itemUom.UomEntry).FirstOrDefault().UomEntry.ToString(),
        //                    Discount = result.Discount ?? 0
        //                });

        //            }

        //        }

        //        return item;
        //    }
        //    catch (Exception ex)
        //    {
        //        return item;
        //    }
        //}

        #endregion
    }

    internal class ItemPriceResult
    {
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public string Currency { get; set; }

    }

    internal class ItemPriceExternal
    {
        public class Value
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public int DocEntry { get; set; }
            public string Canceled { get; set; }
            public string Object { get; set; }
            public object LogInst { get; set; }
            public int UserSign { get; set; }
            public string Transfered { get; set; }
            public string CreateDate { get; set; }
            public string CreateTime { get; set; }
            public string UpdateDate { get; set; }
            public string UpdateTime { get; set; }
            public string DataSource { get; set; }
            public int U_ano { get; set; }
            public double U_importe { get; set; }
            public string U_articulo { get; set; }
        }
        public class Root
        {
            [JsonProperty("odata.metadata")]
            public string OdataMetadata { get; set; }
            public List<Value> value { get; set; }
        }

    }
}