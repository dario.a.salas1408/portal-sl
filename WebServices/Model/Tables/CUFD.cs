﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("CUFD")]
    public partial class CUFD
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string TableID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short FieldID { get; set; }

        [Required]
        [StringLength(18)]
        public string AliasID { get; set; }

        [StringLength(30)]
        public string Descr { get; set; }

        [StringLength(1)]
        public string TypeID { get; set; }

        [StringLength(1)]
        public string EditType { get; set; }

        public short? SizeID { get; set; }

        public short? EditSize { get; set; }

        [StringLength(254)]
        public string Dflt { get; set; }

        [StringLength(1)]
        public string NotNull { get; set; }

        [StringLength(1)]
        public string Sys { get; set; }

        [StringLength(20)]
        public string RTable { get; set; }

    }
}