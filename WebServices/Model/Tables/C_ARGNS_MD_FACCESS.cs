﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MD_FACCESS")]
    public partial class C_ARGNS_MD_FACCESS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_SLaces { get; set; }

        [StringLength(254)]
        public string U_Eyelets { get; set; }

        [StringLength(254)]
        public string U_Buckles { get; set; }

        [StringLength(254)]
        public string U_Enclosures { get; set; }

        [StringLength(254)]
        public string U_Labelling { get; set; }

        [StringLength(254)]
        public string U_Others { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}