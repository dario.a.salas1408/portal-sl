﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_FBOOKD")]
    public partial class C_ARGNS_FBOOKD
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        [StringLength(254)]
        public string U_ModDesc { get; set; }

        [StringLength(30)]
        public string U_Season { get; set; }

        [StringLength(50)]
        public string U_Collection { get; set; }

        [StringLength(50)]
        public string U_Brand { get; set; }

        [StringLength(50)]
        public string U_Group { get; set; }

        [StringLength(50)]
        public string U_Division { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_PriceR { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_PriceW { get; set; }

        public DateTime? U_DateR { get; set; }

        [StringLength(100)]
        public string U_Remark { get; set; }
    }
}