﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OCLA")]
    public partial class OCLA
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int statusID { get; set; }

        [StringLength(30)]
        public string name { get; set; }

        [StringLength(254)]
        public string descriptio { get; set; }

        [StringLength(1)]
        public string Locked { get; set; }
    }
}