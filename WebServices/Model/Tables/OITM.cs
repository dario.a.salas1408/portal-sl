namespace WebServices.Model.Tables
{
    using ARGNS.Model.Implementations.View;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OITM")]
    public partial class OITM
    {
        [Key]
        [StringLength(20)]
        public string ItemCode { get; set; }

        [StringLength(100)]
        public string ItemName { get; set; }

        [StringLength(1)]
        public string PrchseItem { get; set; }

        [StringLength(1)]
        public string SellItem { get; set; }

        [StringLength(1)]
        public string InvntItem { get; set; }

        [StringLength(16)]
        public string CodeBars { get; set; }

        [StringLength(1)]
        public string ManBtchNum { get; set; }

        [StringLength(1)]
        public string ManSerNum { get; set; }
        public short? ItmsGrpCod { get; set; }

        //TODO validar que est� instalado apparell antes de agregar los siguisntes campos (udf de argentis) a esta tabla

        [StringLength(254)]
        public string U_ARGNS_COL { get; set; }

        [StringLength(254)]
        public string U_ARGNS_MOD { get; set; }


        [StringLength(254)]
        public string U_ARGNS_SIZE { get; set; }

        public short? U_ARGNS_SIZEVO { get; set; }

        [StringLength(254)]
        public string U_ARGNS_SCL { get; set; }

        [StringLength(254)]
        public string U_ARGNS_VAR { get; set; }
        [StringLength(15)]

        public string CardCode { get; set; }
        [StringLength(200)]
        public string PicturName { get; set; }

        [Column(TypeName = "ntext")]
        public string UserText { get; set; }

        [StringLength(1)]
        public string ItemType { get; set; }

        public int? AtcEntry { get; set; }

        public int UgpEntry { get; set; }

        [StringLength(8)]
        public string DfltWH { get; set; }

        [StringLength(1)]
        public string U_ARGNS_ITYPE { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
        //Agregados para Quick Order
        [NotMapped]
        public decimal? CostPrice { get; set; }
        [NotMapped]
        public string CostPriceCurrency { get; set; }
        [NotMapped]
        public string CardName { get; set; }
        [NotMapped]
        public decimal? OnHand { get; set; }
        [NotMapped]
        public decimal? IsCommited { get; set; }
        [NotMapped]
        public decimal? OnOrder { get; set; }
        //Agregados para BP Catalogue Number
        [NotMapped]
        public string Substitute { get; set; }

        //Added to Argentina Localization
        [StringLength(8)]
        public string TaxCodeAR { get; set; }

        [StringLength(8)]
        public string TaxCodeAP { get; set; }

        [StringLength(1)]
        public string validFor { get; set; }
        public DateTime? validFrom { get; set; }
        public DateTime? validTo { get; set; }

        [StringLength(1)]
        public string frozenFor { get; set; }
        public DateTime? frozenFrom { get; set; }
        public DateTime? frozenTo { get; set; }
        public int? PUoMEntry { get; set; }
        public int? SUoMEntry { get; set; }

        public int PriceUnit { get; set; }
    }
}
