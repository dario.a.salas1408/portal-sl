﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class QUT2
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNum { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GroupNum { get; set; }

        public int? ExpnsCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? LineTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalFrgn { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalSumSy { get; set; }
    }
}