﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("OBPL")]
    public class OBPL
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BPLId { get; set; }

        [Required]
        [StringLength(100)]
        public string BPLName { get; set; }

        [StringLength(100)]
        public string BPLFrName { get; set; }

        [StringLength(12)]
        public string VATRegNum { get; set; }

        [StringLength(15)]
        public string RepName { get; set; }

        [StringLength(20)]
        public string Industry { get; set; }

        [StringLength(20)]
        public string Business { get; set; }

        [StringLength(254)]
        public string Address { get; set; }

        [StringLength(254)]
        public string AddressFr { get; set; }

        [StringLength(1)]
        public string MainBPL { get; set; }

        [StringLength(3)]
        public string TxOffcNo { get; set; }

        [StringLength(1)]
        public string Disabled { get; set; }

        public int? LogInstanc { get; set; }

        public short? UserSign2 { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(15)]
        public string DflCust { get; set; }

        [StringLength(15)]
        public string DflVendor { get; set; }

        [StringLength(8)]
        public string DflWhs { get; set; }

        [StringLength(8)]
        public string DflTaxCode { get; set; }

        [StringLength(100)]
        public string RevOffice { get; set; }

        [StringLength(32)]
        public string TaxIdNum { get; set; }

        [StringLength(32)]
        public string TaxIdNum2 { get; set; }

        [StringLength(32)]
        public string TaxIdNum3 { get; set; }

        [StringLength(32)]
        public string AddtnlId { get; set; }

        public int? CompNature { get; set; }

        public int? EconActT { get; set; }

        [StringLength(2)]
        public string CredCOrig { get; set; }

        [StringLength(2)]
        public string IPIPeriod { get; set; }

        public int? CoopAssocT { get; set; }

        [StringLength(3)]
        public string PrefState { get; set; }

        public int? ProfTax { get; set; }

        public int? CompQualif { get; set; }

        public int? DeclType { get; set; }

        [StringLength(100)]
        public string AddrType { get; set; }

        [StringLength(100)]
        public string Street { get; set; }

        [StringLength(100)]
        public string StreetNo { get; set; }

        [StringLength(100)]
        public string Building { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        [StringLength(100)]
        public string Block { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(3)]
        public string State { get; set; }

        [StringLength(100)]
        public string County { get; set; }

        [StringLength(3)]
        public string Country { get; set; }

        [StringLength(15)]
        public string PmtClrAct { get; set; }

        [StringLength(60)]
        public string CommerReg { get; set; }

        public DateTime? DateOfInc { get; set; }

        [StringLength(2)]
        public string SPEDProf { get; set; }

        public int? EnvTypeNFe { get; set; }

        [StringLength(1)]
        public string Opt4ICMS { get; set; }

        [Column(TypeName = "ntext")]
        public string AliasName { get; set; }

        [StringLength(50)]
        public string GlblLocNum { get; set; }

        public DateTime? TaxRptFrm { get; set; }

        [StringLength(100)]
        public string Suframa { get; set; }

        [StringLength(8)]
        public string DfltResWhs { get; set; }

        public int? SnapshotId { get; set; }
    }
}