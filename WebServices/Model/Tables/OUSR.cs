﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OUSR")]
    public partial class OUSR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short USERID { get; set; }

        [StringLength(254)]
        public string PASSWORD { get; set; }

        [Required]
        [StringLength(8)]
        public string USER_CODE { get; set; }

        [StringLength(155)]
        public string U_NAME { get; set; }

        [StringLength(8)]
        public string DfltsGroup { get; set; }

    }
}