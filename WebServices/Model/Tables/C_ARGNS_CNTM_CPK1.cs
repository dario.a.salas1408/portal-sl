﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_CNTM_CPK1")]
    public partial class C_ARGNS_CNTM_CPK1
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        public short? U_CntnerNum { get; set; }

        [StringLength(50)]
        public string U_ItemCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Quantity { get; set; }

        public int? U_UomEntry { get; set; }

        [StringLength(100)]
        public string U_unitMsr { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_NumPerMsr { get; set; }

        [StringLength(10)]
        public string U_BaseEntry { get; set; }

        [StringLength(10)]
        public string U_BaseRef { get; set; }

        [StringLength(10)]
        public string U_BaseLine { get; set; }
    }
}