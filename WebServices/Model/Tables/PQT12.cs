﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class PQT12
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [StringLength(100)]
        public string StreetS { get; set; }

        [StringLength(100)]
        public string BlockS { get; set; }

        [Column(TypeName = "ntext")]
        public string BuildingS { get; set; }

        [StringLength(100)]
        public string CityS { get; set; }

        [StringLength(20)]
        public string ZipCodeS { get; set; }

        [StringLength(100)]
        public string CountyS { get; set; }

        [StringLength(3)]
        public string StateS { get; set; }

        [StringLength(3)]
        public string CountryS { get; set; }

        [StringLength(100)]
        public string AddrTypeS { get; set; }

        [StringLength(100)]
        public string StreetNoS { get; set; }

        [StringLength(100)]
        public string StreetB { get; set; }

        [StringLength(100)]
        public string BlockB { get; set; }

        [Column(TypeName = "ntext")]
        public string BuildingB { get; set; }

        [StringLength(100)]
        public string CityB { get; set; }

        [StringLength(20)]
        public string ZipCodeB { get; set; }

        [StringLength(100)]
        public string CountyB { get; set; }

        [StringLength(3)]
        public string StateB { get; set; }

        [StringLength(3)]
        public string CountryB { get; set; }

        [StringLength(100)]
        public string AddrTypeB { get; set; }

        [StringLength(100)]
        public string StreetNoB { get; set; }

        [StringLength(50)]
        public string GlbLocNumS { get; set; }

        [StringLength(50)]
        public string GlbLocNumB { get; set; }
    }
}