namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OSCL")]
    public partial class OSCL
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int callID { get; set; }

        [StringLength(254)]
        public string subject { get; set; }

        [StringLength(15)]
        public string customer { get; set; }

        [StringLength(100)]
        public string custmrName { get; set; }

        public int? contctCode { get; set; }

        [StringLength(36)]
        public string manufSN { get; set; }

        [StringLength(36)]
        public string internalSN { get; set; }

        public int? contractID { get; set; }

        public DateTime? cntrctDate { get; set; }

        public DateTime? resolDate { get; set; }

        public short? resolTime { get; set; }

        [StringLength(1)]
        public string free_1 { get; set; }

        public DateTime? free_2 { get; set; }

        public short? origin { get; set; }

        [StringLength(20)]
        public string itemCode { get; set; }

        [StringLength(100)]
        public string itemName { get; set; }

        public short? itemGroup { get; set; }

        public short? status { get; set; }

        [StringLength(1)]
        public string priority { get; set; }

        public short? callType { get; set; }

        public short? problemTyp { get; set; }

        public short? assignee { get; set; }

        [Column(TypeName = "ntext")]
        public string descrption { get; set; }

        [StringLength(20)]
        public string objType { get; set; }

        public int? logInstanc { get; set; }

        public short? userSign { get; set; }

        public DateTime? createDate { get; set; }

        public short? createTime { get; set; }

        public DateTime? closeDate { get; set; }

        public short? closeTime { get; set; }

        public short? userSign2 { get; set; }

        public DateTime? updateDate { get; set; }

        public int? SCL1Count { get; set; }

        public int? SCL2Count { get; set; }

        [StringLength(1)]
        public string isEntitled { get; set; }

        public int? insID { get; set; }

        public int? technician { get; set; }

        [Column(TypeName = "ntext")]
        public string resolution { get; set; }

        public int? Scl1NxtLn { get; set; }

        public int? Scl2NxtLn { get; set; }

        public int? Scl3NxtLn { get; set; }

        public int? Scl4NxtLn { get; set; }

        public int? Scl5NxtLn { get; set; }

        [StringLength(1)]
        public string isQueue { get; set; }

        [StringLength(20)]
        public string Queue { get; set; }

        public DateTime? resolOnDat { get; set; }

        public short? resolOnTim { get; set; }

        public DateTime? respByDate { get; set; }

        public short? respByTime { get; set; }

        public DateTime? respOnDate { get; set; }


        public short? respOnTime { get; set; }

        public short? respAssign { get; set; }

        public DateTime? AssignDate { get; set; }

        public short? AssignTime { get; set; }

        public short? UpdateTime { get; set; }

        public short? responder { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public short Instance { get; set; }

        public int DocNum { get; set; }

        public int? Series { get; set; }

        [StringLength(1)]
        public string Handwrtten { get; set; }

        [Required]
        [StringLength(10)]
        public string PIndicator { get; set; }

        public DateTime? StartDate { get; set; }

        public int? StartTime { get; set; }

        public DateTime? EndDate { get; set; }

        public int? EndTime { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Duration { get; set; }

        [StringLength(1)]
        public string DurType { get; set; }

        [StringLength(1)]
        public string Reminder { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RemQty { get; set; }

        [StringLength(1)]
        public string RemType { get; set; }

        public DateTime? RemDate { get; set; }


        [StringLength(1)]
        public string RemSent { get; set; }

        public short? RemTime { get; set; }

        public short? Location { get; set; }

        [StringLength(50)]
        public string AddrName { get; set; }

        [StringLength(1)]
        public string AddrType { get; set; }

        [StringLength(100)]
        public string Street { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string Room { get; set; }

        [StringLength(3)]
        public string State { get; set; }

        [StringLength(3)]
        public string Country { get; set; }

        [StringLength(1)]
        public string DisplInCal { get; set; }

        [StringLength(254)]
        public string SupplCode { get; set; }

        [Column(TypeName = "ntext")]
        public string Attachment { get; set; }

        public int? AtcEntry { get; set; }

        [StringLength(100)]
        public string NumAtCard { get; set; }

        public short? ProSubType { get; set; }
    }
}
