﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SAMPLESLINES")]
    public partial class C_ARGNS_SAMPLESLINES
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(254)]
        public string U_SclCode { get; set; }

        [StringLength(30)]
        public string U_POM { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(254)]
        public string U_SizeCode { get; set; }

        [StringLength(254)]
        public string U_SizeDesc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Target { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Actual { get; set; }

        [StringLength(254)]
        public string U_Revision { get; set; }
    }
}