﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class PCH1
    {
        public PCH1()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNum { get; set; }

        public int? TargetType { get; set; }

        public int? TrgetEntry { get; set; }

        [StringLength(16)]
        public string BaseRef { get; set; }

        public int? BaseType { get; set; }

        public int? BaseEntry { get; set; }

        public int? BaseLine { get; set; }

        [StringLength(1)]
        public string LineStatus { get; set; }

        [StringLength(20)]
        public string ItemCode { get; set; }

        [StringLength(100)]
        public string Dscription { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Quantity { get; set; }

        public DateTime? ShipDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? OpenQty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Price { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Rate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DiscPrcnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? LineTotal { get; set; }

        [StringLength(8)]
        public string WhsCode { get; set; }

        public int? SlpCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PriceBefDi { get; set; }

        [StringLength(8)]
        public string OcrCode { get; set; }

        [StringLength(100)]
        public string FreeTxt { get; set; }

        [StringLength(15)]
        public string LineVendor { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}