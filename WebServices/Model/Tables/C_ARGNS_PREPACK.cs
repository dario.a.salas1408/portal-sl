﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_PREPACK")]
    public partial class C_ARGNS_PREPACK
    {
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        public int DocEntry { get; set; }

        [StringLength(254)]
        public string U_PpCode { get; set; }

        [StringLength(1)]
        public string U_PpType { get; set; }

        [StringLength(254)]
        public string U_PpDesc { get; set; }

        [StringLength(254)]
        public string U_SclCode { get; set; }

        [StringLength(254)]
        public string U_BPCode { get; set; }

        [StringLength(50)]
        public string U_NRFCode { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(30)]
        public string U_AFSeg { get; set; }

        [StringLength(30)]
        public string U_ModGrp { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        [StringLength(100)]
        public string U_Shipto { get; set; }

        [StringLength(254)]
        public string U_Sruncode { get; set; }

        [StringLength(1)]
        public string U_Comb { get; set; }

        [StringLength(30)]
        public string U_PriceList { get; set; }

        [StringLength(30)]
        public string U_WhsCode { get; set; }
    }
}