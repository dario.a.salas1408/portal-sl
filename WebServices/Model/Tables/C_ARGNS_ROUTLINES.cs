﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_ROUTLINES")]
    public partial class C_ARGNS_ROUTLINES
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(30)]
        public string U_Workflow { get; set; }

        [StringLength(254)]
        public string U_WorkfDes { get; set; }

        [StringLength(20)]
        public string U_Depart { get; set; }

        public short? U_LeadTime { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(20)]
        public string U_Role { get; set; }

        [StringLength(100)]
        public string U_Manager { get; set; }

        [StringLength(30)]
        public string U_User { get; set; }

        public short? U_LTime { get; set; }

        [StringLength(160)]
        public string U_Usrname { get; set; }
    }
}