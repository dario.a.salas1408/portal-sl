﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OACT")]
    public partial class OACT
    {
        [Key]
        [StringLength(15)]
        public string AcctCode { get; set; }

        [StringLength(100)]
        public string AcctName { get; set; }

        [StringLength(210)]
        public string FormatCode { get; set; }
    }
}