namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_APPTEXGRPS")]
    public partial class C_ARGNS_APPTEXGRPS
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }
        [StringLength(1)]
        public string U_UseMod { get; set; }

        [StringLength(1)]
        public string U_UseCol { get; set; }       

        [StringLength(1)]
        public string U_UseScl { get; set; }

        [StringLength(1)]
        public string U_UseVar { get; set; }
        [StringLength(1)]
        public string U_Active { get; set; }
        public short? U_ModLen { get; set; }

        public short? U_ColLen { get; set; }

        public short? U_SizeLen { get; set; }

        public short? U_VarLen { get; set; }

    }
}
