﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OITB")]
    public partial class OITB
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ItmsGrpCod { get; set; }

        [Required]
        [StringLength(20)]
        public string ItmsGrpNam { get; set; }

    }
}