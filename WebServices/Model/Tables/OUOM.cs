namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OUOM")]
    public partial class OUOM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UomEntry { get; set; }

        [Required]
        [StringLength(20)]
        public string UomCode { get; set; }

        [StringLength(100)]
        public string UomName { get; set; }

        [StringLength(1)]
        public string Locked { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public short? UserSign { get; set; }

        public short? LogInstanc { get; set; }

        public short? UserSign2 { get; set; }

        public DateTime? UpdateDate { get; set; }

        public DateTime? CreateDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Length1 { get; set; }

        public short? Len1Unit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? length2 { get; set; }

        public short? Len2Unit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Width1 { get; set; }

        public short? Wdth1Unit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Width2 { get; set; }

        public short? Wdth2Unit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Height1 { get; set; }

        public short? Hght1Unit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Height2 { get; set; }

        public short? Hght2Unit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Volume { get; set; }

        public short? VolUnit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Weight1 { get; set; }

        public short? WghtUnit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Weight2 { get; set; }

        public short? Wght2Unit { get; set; }

        [StringLength(20)]
        public string IntSymbol { get; set; }
    }
}
