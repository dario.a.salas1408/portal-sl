﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MODVENDOR")]
    public partial class C_ARGNS_MODVENDOR
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        [StringLength(254)]
        public string U_CardCode { get; set; }

        [StringLength(5)]
        public string U_CardType { get; set; }
    }
}