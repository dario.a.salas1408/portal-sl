namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_MODLIST")]
    public partial class C_ARGNS_MODLIST
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        public short? U_ListNum { get; set; }

        [StringLength(32)]
        public string U_ListName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Price { get; set; }

        [StringLength(1)]
        public string U_DefList { get; set; }

        [StringLength(32)]
        public string U_BasList { get; set; }

        public short? U_BASE_NUM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Factor { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_BasPrice { get; set; }

        [StringLength(1)]
        public string U_Manual { get; set; }

        [StringLength(3)]
        public string U_Currency { get; set; }
    }
}
