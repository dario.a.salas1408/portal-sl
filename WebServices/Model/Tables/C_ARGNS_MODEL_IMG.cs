﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MODEL_IMG")]
    public partial class C_ARGNS_MODEL_IMG
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        [StringLength(254)]
        public string U_File { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Path { get; set; }

        [StringLength(254)]
        public string U_FType { get; set; }
    }
}