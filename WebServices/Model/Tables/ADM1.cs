﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace WebServices.Model.Tables
{
    public partial class ADM1
    {
        
        [StringLength(100)]
        public string Street { get; set; }

        [StringLength(100)]
        public string Block { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        [StringLength(100)]
        public string County { get; set; }

        [StringLength(3)]
        public string State { get; set; }

        [StringLength(3)]
        public string Country { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Code { get; set; }

        [StringLength(100)]
        public string AddrType { get; set; }

        [StringLength(100)]
        public string StreetNo { get; set; }

        [StringLength(100)]
        public string Building { get; set; }

        [StringLength(50)]
        public string GlblLocNum { get; set; }
    }
}