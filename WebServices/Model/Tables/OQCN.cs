﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("OQCN")]
    public partial class OQCN
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CategoryId { get; set; }

        [StringLength(50)]
        public string CatName { get; set; }

        [StringLength(20)]
        public string PermMask { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public short? UserSign { get; set; }
    }
}