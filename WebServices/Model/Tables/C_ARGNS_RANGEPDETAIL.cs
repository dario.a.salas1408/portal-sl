﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_RANGEPDETAIL")]
    public partial class C_ARGNS_RANGEPDETAIL
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(50)]
        public string U_ModCode { get; set; }

        [StringLength(254)]
        public string U_ModDesc { get; set; }

        [StringLength(20)]
        public string U_Season { get; set; }

        [StringLength(20)]
        public string U_Coll { get; set; }

        [StringLength(100)]
        public string U_ProdLine { get; set; }

        [StringLength(30)]
        public string U_ProdGrou { get; set; }

        [StringLength(30)]
        public string U_Brand { get; set; }

        [StringLength(254)]
        public string U_ModPic { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_ModPrice { get; set; }

        [StringLength(254)]
        public string U_ModCol { get; set; }

        [StringLength(254)]
        public string U_Page { get; set; }

        [StringLength(254)]
        public string U_PicCode { get; set; }

        [StringLength(254)]
        public string U_PicSize { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Comments { get; set; }
    }
}