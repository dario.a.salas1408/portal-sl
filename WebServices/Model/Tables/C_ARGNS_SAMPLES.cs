﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SAMPLES")]
    public partial class C_ARGNS_SAMPLES
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(100)]
        public string U_ModCode { get; set; }

        [StringLength(100)]
        public string U_Type { get; set; }

        [StringLength(50)]
        public string U_SampCode { get; set; }

        [StringLength(254)]
        public string U_Status { get; set; }

        [StringLength(254)]
        public string U_SclCode { get; set; }

        [StringLength(254)]
        public string U_POM { get; set; }

        public DateTime? U_Date { get; set; }

        [StringLength(50)]
        public string U_User { get; set; }

        public DateTime? U_ApproDate { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(254)]
        public string U_Comments { get; set; }

        [StringLength(254)]
        public string U_BPCustomer { get; set; }

        [StringLength(254)]
        public string U_BPSupp { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Picture { get; set; }
    }
}