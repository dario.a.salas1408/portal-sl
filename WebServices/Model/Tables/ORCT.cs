﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("ORCT")]
    public partial class ORCT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int DocNum { get; set; }

        [StringLength(1)]
        public string DocType { get; set; }

        [StringLength(1)]
        public string PayNoDoc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NoDocSum { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NoDocSumFC { get; set; }
    }
}