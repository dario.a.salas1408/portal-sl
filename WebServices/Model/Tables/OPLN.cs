﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OPLN")]
    public partial class OPLN
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short ListNum { get; set; }

        [Required]
        [StringLength(32)]
        public string ListName { get; set; }

        public short? BASE_NUM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Factor { get; set; }

        public short? RoundSys { get; set; }

        public short? GroupCode { get; set; }
    }
}