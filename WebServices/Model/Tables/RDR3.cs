﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class RDR3
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? ExpnsCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? LineTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalFrgn { get; set; }

        [StringLength(100)]
        public string Comments { get; set; }

        [StringLength(20)]
        public string ObjType { get; set; }

        [StringLength(1)]
        public string DistrbMthd { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VatSum { get; set; }

        [StringLength(8)]
        public string TaxCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNum { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [StringLength(8)]
        public string OcrCode { get; set; }

        [StringLength(1)]
        public string TaxDistMtd { get; set; }
    }
}