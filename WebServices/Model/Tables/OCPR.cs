namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OCPR")]
    public partial class OCPR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CntctCode { get; set; }

        [Required]
        [StringLength(15)]
        public string CardCode { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}
