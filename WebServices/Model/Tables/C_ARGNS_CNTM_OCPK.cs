﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_CNTM_OCPK")]
    public partial class C_ARGNS_CNTM_OCPK
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        public short? U_CntnerNum { get; set; }

        [StringLength(30)]
        public string U_CntnerTyp { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Weight { get; set; }

        public short? U_WeightUnit { get; set; }
    }
}