﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class CRD1
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Address { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(15)]
        public string CardCode { get; set; }

        [StringLength(100)]
        public string Street { get; set; }

        [StringLength(100)]
        public string Block { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(100)]
        public string County { get; set; }

        [StringLength(3)]
        public string Country { get; set; }

        [StringLength(3)]
        public string State { get; set; }

        public int? LineNum { get; set; }

        [Column(TypeName = "ntext")]
        public string Building { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string AdresType { get; set; }

        [StringLength(100)]
        public string StreetNo { get; set; }

        [StringLength(50)]
        public string GlblLocNum { get; set; }

        [StringLength(32)]
        public string LicTradNum { get; set; }

        [StringLength(8)]
        public string TaxCode { get; set; }

        //Added by Portal
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}