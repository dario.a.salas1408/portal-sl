namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OOPR")]
    public partial class OOPR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OpprId { get; set; }

        [StringLength(15)]
        public string CardCode { get; set; }

        public int? SlpCode { get; set; }

        public int? CprCode { get; set; }

        public int? Source { get; set; }

        public int? IntCat1 { get; set; }

        public int? IntCat2 { get; set; }

        public int? IntCat3 { get; set; }

        public int? IntRate { get; set; }

        public DateTime? OpenDate { get; set; }

        [StringLength(1)]
        public string DifType { get; set; }

        public DateTime? PredDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MaxSumLoc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MaxSumSys { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? WtSumLoc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? WtSumSys { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PrcnProf { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SumProfL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SumProfS { get; set; }

        [Column(TypeName = "ntext")]
        public string Memo { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [StringLength(30)]
        public string StatusRem { get; set; }

        public int? Reason { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RealSumLoc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RealSumSys { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RealProfL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RealProfS { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? CloPrcnt { get; set; }

        public short? StepLast { get; set; }

        public short? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public short? Instance { get; set; }

        [StringLength(100)]
        public string CardName { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? LastSlp { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int? Territory { get; set; }

        public int? Industry { get; set; }

        [StringLength(15)]
        public string ChnCrdCode { get; set; }

        [StringLength(100)]
        public string ChnCrdName { get; set; }

        [StringLength(20)]
        public string PrjCode { get; set; }

        public short? CardGroup { get; set; }

        public int? ChnCrdCon { get; set; }

        public int? Owner { get; set; }

        [Column(TypeName = "ntext")]
        public string attachment { get; set; }

        [StringLength(9)]
        public string DocType { get; set; }

        public int? DocNum { get; set; }

        public int? DocEntry { get; set; }

        [StringLength(1)]
        public string DocChkbox { get; set; }

        public int? AtcEntry { get; set; }

        public int? LogInstanc { get; set; }

        public short? UserSign2 { get; set; }

        public DateTime? UpdateDate { get; set; }

        [StringLength(1)]
        public string OpprType { get; set; }
    }
}
