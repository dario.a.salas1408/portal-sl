﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("USR6")]
    public class USR6
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(25)]
        public string UserCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BPLId { get; set; }

        [Column(TypeName = "ntext")]
        public string DigCrtPath { get; set; }

        [StringLength(1)]
        public string AcsDsbldBP { get; set; }
    }
}