﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("OHTY")]
    public partial class OHTY
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int typeID { get; set; }

        [StringLength(20)]
        public string name { get; set; }

        [Column(TypeName = "ntext")]
        public string descriptio { get; set; }

        [StringLength(1)]
        public string locked { get; set; }
    }
}