﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_COLOR")]
    public partial class C_ARGNS_COLOR
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(254)]
        public string U_ColCode { get; set; }

        [StringLength(254)]
        public string U_ColDesc { get; set; }

        [StringLength(50)]
        public string U_Shade { get; set; }

        [StringLength(50)]
        public string U_NRFCode { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(50)]
        public string U_Argb { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Pic { get; set; }

        [StringLength(254)]
        public string U_Attrib1 { get; set; }

        [StringLength(254)]
        public string U_Attrib2 { get; set; }
    }
}