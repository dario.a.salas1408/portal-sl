namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OPR1
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OpprId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Line { get; set; }

        public int? SlpCode { get; set; }

        public int? CntctCode { get; set; }

        public DateTime? OpenDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? Step_Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ClosePrcnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MaxSumLoc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MaxSumSys { get; set; }

        [Column(TypeName = "ntext")]
        public string Memo { get; set; }

        public int? DocId { get; set; }

        [StringLength(9)]
        public string ObjType { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [StringLength(1)]
        public string Linked { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? WtSumLoc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? WtSumSys { get; set; }

        public short? UserSign { get; set; }

        [StringLength(15)]
        public string ChnCrdCode { get; set; }

        [StringLength(100)]
        public string ChnCrdName { get; set; }

        public int? ChnCrdCon { get; set; }

        [StringLength(1)]
        public string DocChkbox { get; set; }

        public int? Owner { get; set; }

        public int? DocNumber { get; set; }
    }
}
