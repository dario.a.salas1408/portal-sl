﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("OPRJ")]
    public partial class OPRJ
    {
        [Key]
        [StringLength(20)]
        public string PrjCode { get; set; }

        [StringLength(100)]
        public string PrjName { get; set; }
    }
}