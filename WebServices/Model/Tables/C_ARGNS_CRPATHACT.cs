namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_CRPATHACT")]
    public partial class C_ARGNS_CRPATHACT
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(50)]
        public string U_PhaseId { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(20)]
        public string U_Depart { get; set; }

        [StringLength(20)]
        public string U_Role { get; set; }

        [StringLength(100)]
        public string U_Manager { get; set; }

        [StringLength(30)]
        public string U_User { get; set; }

        [StringLength(15)]
        public string U_Bp { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_DelivDay { get; set; }

        public DateTime? U_SDate { get; set; }

        public DateTime? U_CDate { get; set; }

        [StringLength(254)]
        public string U_ActSAP { get; set; }

        [StringLength(254)]
        public string U_Priority { get; set; }

        [StringLength(254)]
        public string U_Status { get; set; }

        [StringLength(254)]
        public string U_Comment { get; set; }

        [StringLength(1)]
        public string U_CreateAc { get; set; }

        [StringLength(1)]
        public string U_Updated { get; set; }

        public DateTime? U_PlSDate { get; set; }

        public DateTime? U_PlCDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_LTime { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_PlanLead { get; set; }

        public short? U_JobId { get; set; }

        [StringLength(20)]
        public string U_JobNum { get; set; }
    }
}
