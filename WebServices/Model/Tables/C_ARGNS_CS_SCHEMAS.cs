namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_CS_SCHEMAS")]
    public partial class C_ARGNS_CS_SCHEMAS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string U_ItemCode { get; set; }

        [StringLength(254)]
        public string U_ItemName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Percent { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Amount { get; set; }

        [StringLength(3)]
        public string U_Currency { get; set; }

        [StringLength(1)]
        public string U_AplicTo { get; set; }

        public short? U_PList { get; set; }
    }
}
