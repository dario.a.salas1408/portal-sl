﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_RANGEP")]
    public partial class C_ARGNS_RANGEP
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        public int? UserSign { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(100)]
        public string U_RangeCode { get; set; }

        [StringLength(254)]
        public string U_RangeDesc { get; set; }

        [StringLength(254)]
        public string U_Coll { get; set; }

        [StringLength(100)]
        public string U_Season { get; set; }

        public DateTime? U_SDate { get; set; }

        [StringLength(20)]
        public string U_RPEmpl { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        public DateTime? U_DTo { get; set; }

        [StringLength(20)]
        public string U_PrjCode { get; set; }
    }
}