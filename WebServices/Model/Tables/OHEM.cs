namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OHEM")]
    public partial class OHEM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int empID { get; set; }
        [StringLength(50)]
        public string lastName { get; set; }
        [StringLength(50)]
        public string firstName { get; set; }
        public int? userId { get; set; }
        public short? dept { get; set; }
        public int? salesPrson { get; set; }
    }
}
