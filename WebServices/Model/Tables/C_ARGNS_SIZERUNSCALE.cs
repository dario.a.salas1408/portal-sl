﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SIZERUNSCALE")]
    public partial class C_ARGNS_SIZERUNSCALE
    {
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        public int DocEntry { get; set; }

        [StringLength(254)]
        public string U_SizeRunCode { get; set; }

        [StringLength(254)]
        public string U_SizeRunDesc { get; set; }

        [StringLength(254)]
        public string U_SclCode { get; set; }

        [StringLength(15)]
        public string U_CustCode { get; set; }

        [StringLength(100)]
        public string U_Country { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(100)]
        public string U_shipto { get; set; }
    }
}