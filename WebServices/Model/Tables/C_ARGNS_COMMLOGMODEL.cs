namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_COMMLOGMODEL")]
    public partial class C_ARGNS_COMMLOGMODEL
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(254)]
        public string U_MODCODE { get; set; }

        public DateTime? U_DATE { get; set; }

        [StringLength(254)]
        public string U_USER { get; set; }

        [StringLength(254)]
        public string U_COMMENT { get; set; }

        [StringLength(254)]
        public string U_FILE { get; set; }

        [StringLength(254)]
        public string U_FILEPATH { get; set; }
    }
}
