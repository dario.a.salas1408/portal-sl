﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OWDD")]
    public partial class OWDD
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WddCode { get; set; }

        public int? OwnerID { get; set; }

        public int? DocEntry { get; set; }

        [StringLength(20)]
        public string ObjType { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [StringLength(100)]
        public string Remarks { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public short? UserSign { get; set; }

        public int? DraftEntry { get; set; }
		public string DraftType { get; set; }
	}
}