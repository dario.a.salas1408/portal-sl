namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OSCP")]
    public partial class OSCP
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short prblmTypID { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        [Column(TypeName = "ntext")]
        public string Descriptio { get; set; }
    }
}
