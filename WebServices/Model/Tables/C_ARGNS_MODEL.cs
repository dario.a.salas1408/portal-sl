﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MODEL")]
    public partial class C_ARGNS_MODEL
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(30)]
        public string U_ATGrp { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        [StringLength(254)]
        public string U_ModDesc { get; set; }

        public DateTime? U_SSDate { get; set; }

        public DateTime? U_SCDate { get; set; }

        [StringLength(30)]
        public string U_Year { get; set; }

        [StringLength(100)]
        public string U_COO { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Pic { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        public short? U_SapGrp { get; set; }

        public short? U_PList { get; set; }

        [StringLength(30)]
        public string U_Division { get; set; }

        [StringLength(30)]
        public string U_Season { get; set; }

        [StringLength(254)]
        public string U_FrgnDesc { get; set; }

        [StringLength(30)]
        public string U_ModGrp { get; set; }

        [StringLength(254)]
        public string U_SclCode { get; set; }

        [StringLength(50)]
        public string U_LineCode { get; set; }

        [StringLength(15)]
        public string U_Vendor { get; set; }

        [StringLength(8)]
        public string U_MainWhs { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Comments { get; set; }

        [StringLength(254)]
        public string U_Owner { get; set; }

        [StringLength(1)]
        public string U_Approved { get; set; }

        [Column(TypeName = "ntext")]
        public string U_PicR { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Price { get; set; }

        [StringLength(254)]
        public string U_WhsNewI { get; set; }

        [StringLength(100)]
        public string U_CollCode { get; set; }

        [StringLength(100)]
        public string U_AmbCode { get; set; }

        [StringLength(100)]
        public string U_CompCode { get; set; }

        [StringLength(8)]
        public string U_Brand { get; set; }

        [StringLength(30)]
        public string U_ChartCod { get; set; }

        [StringLength(254)]
        public string U_Designer { get; set; }

        [StringLength(15)]
        public string U_Customer { get; set; }

        [StringLength(254)]
        public string U_GrpSCod { get; set; }

        [StringLength(3)]
        public string U_Currency { get; set; }

        [StringLength(30)]
        public string U_Status { get; set; }

        [StringLength(1)]
        public string U_RMaterial { get; set; }

        [StringLength(10)]
        public string U_DocNumb { get; set; }

        [StringLength(254)]
        public string U_SclPOM { get; set; }

        [StringLength(50)]
        public string U_CodePOM { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}