﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("ODPI")]
    public class ODPI
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int DocNum { get; set; }

        [StringLength(1)]
        public string DocType { get; set; }
        
        [StringLength(1)]
        public string DocStatus { get; set; }

        [StringLength(20)]
        public string ObjType { get; set; }

        public int? FolNumFrom { get; set; }

        public int? FolNumTo { get; set; }

    }
}