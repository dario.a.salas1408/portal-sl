﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OPKG")]
    public partial class OPKG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PkgCode { get; set; }

        [Required]
        [StringLength(30)]
        public string PkgType { get; set; }
    }
}