﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_FASEMP")]
    public partial class C_ARGNS_FASEMP
    {
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(254)]
        public string U_FasCode { get; set; }

        [StringLength(254)]
        public string U_empId { get; set; }

        [StringLength(254)]
        public string U_empName { get; set; }
    }
}