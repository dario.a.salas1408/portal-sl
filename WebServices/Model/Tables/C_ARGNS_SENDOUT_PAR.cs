﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SENDOUT_PAR")]
    public partial class C_ARGNS_SENDOUT_PAR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(20)]
        public string U_StgCode { get; set; }

        [StringLength(20)]
        public string U_ItemCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_PlanQty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_IssQty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_RecQty { get; set; }

        [StringLength(150)]
        public string U_Comments { get; set; }

        [StringLength(200)]
        public string U_BarCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_BadQty { get; set; }

        public int? U_ProdOrder { get; set; }

        [StringLength(1)]
        public string U_Rework { get; set; }
    }
}