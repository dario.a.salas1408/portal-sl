﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("OUQR")]
    public partial class OUQR
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IntrnalKey { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int QCategory { get; set; }

        [Required]
        [StringLength(100)]
        public string QName { get; set; }

        [Column(TypeName = "ntext")]
        public string QString { get; set; }

        [StringLength(1)]
        public string QType { get; set; }

        [StringLength(100)]
        public string ColumnSize { get; set; }

        public int? DBType { get; set; }
    }
}