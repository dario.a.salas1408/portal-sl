namespace WebServices.Model.Tables
{
    using ARGNS.Model.Implementations.View;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("OCRD")]
    public partial class OCRD
    {
        [Key]
        [StringLength(15)]
        public string CardCode { get; set; }

        [StringLength(100)]
        public string CardName { get; set; }

        [StringLength(1)]
        public string CardType { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [StringLength(20)]
        public string ZipCode { get; set; }

        [StringLength(100)]
        public string MailAddres { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        [StringLength(50)]
        public string ShipToDef { get; set; }

        [StringLength(50)]
        public string BillToDef { get; set; }
        
        public short? ListNum { get; set; }

        public short? ShipType { get; set; }

        [StringLength(100)]
        public string Notes { get; set; }

        public int? SlpCode { get; set; }

        [StringLength(1)]
        public string QryGroup1 { get; set; }

        [StringLength(1)]
        public string QryGroup2 { get; set; }

        [StringLength(1)]
        public string QryGroup3 { get; set; }

        [StringLength(1)]
        public string QryGroup4 { get; set; }

        [StringLength(1)]
        public string QryGroup5 { get; set; }

        [StringLength(1)]
        public string QryGroup6 { get; set; }

        [StringLength(1)]
        public string QryGroup7 { get; set; }

        [StringLength(1)]
        public string QryGroup8 { get; set; }

        [StringLength(1)]
        public string QryGroup9 { get; set; }

        [StringLength(1)]
        public string QryGroup10 { get; set; }

        [StringLength(1)]
        public string QryGroup11 { get; set; }

        [StringLength(1)]
        public string QryGroup12 { get; set; }

        [StringLength(1)]
        public string QryGroup13 { get; set; }

        [StringLength(1)]
        public string QryGroup14 { get; set; }

        [StringLength(1)]
        public string QryGroup15 { get; set; }

        [StringLength(1)]
        public string QryGroup16 { get; set; }

        [StringLength(1)]
        public string QryGroup17 { get; set; }

        [StringLength(1)]
        public string QryGroup18 { get; set; }

        [StringLength(1)]
        public string QryGroup19 { get; set; }

        [StringLength(1)]
        public string QryGroup20 { get; set; }

        [StringLength(1)]
        public string QryGroup21 { get; set; }

        [StringLength(1)]
        public string QryGroup22 { get; set; }

        [StringLength(1)]
        public string QryGroup23 { get; set; }

        [StringLength(1)]
        public string QryGroup24 { get; set; }

        [StringLength(1)]
        public string QryGroup25 { get; set; }

        [StringLength(1)]
        public string QryGroup26 { get; set; }

        [StringLength(1)]
        public string QryGroup27 { get; set; }

        [StringLength(1)]
        public string QryGroup28 { get; set; }

        [StringLength(1)]
        public string QryGroup29 { get; set; }

        [StringLength(1)]
        public string QryGroup30 { get; set; }

        [StringLength(1)]
        public string QryGroup31 { get; set; }

        [StringLength(1)]
        public string QryGroup32 { get; set; }

        [StringLength(1)]
        public string QryGroup33 { get; set; }

        [StringLength(1)]
        public string QryGroup34 { get; set; }

        [StringLength(1)]
        public string QryGroup35 { get; set; }

        [StringLength(1)]
        public string QryGroup36 { get; set; }

        [StringLength(1)]
        public string QryGroup37 { get; set; }

        [StringLength(1)]
        public string QryGroup38 { get; set; }

        [StringLength(1)]
        public string QryGroup39 { get; set; }

        [StringLength(1)]
        public string QryGroup40 { get; set; }

        [StringLength(1)]
        public string QryGroup41 { get; set; }

        [StringLength(1)]
        public string QryGroup42 { get; set; }

        [StringLength(1)]
        public string QryGroup43 { get; set; }

        [StringLength(1)]
        public string QryGroup44 { get; set; }

        [StringLength(1)]
        public string QryGroup45 { get; set; }

        [StringLength(1)]
        public string QryGroup46 { get; set; }

        [StringLength(1)]
        public string QryGroup47 { get; set; }

        [StringLength(1)]
        public string QryGroup48 { get; set; }

        [StringLength(1)]
        public string QryGroup49 { get; set; }

        [StringLength(1)]
        public string QryGroup50 { get; set; }

        [StringLength(1)]
        public string QryGroup51 { get; set; }

        [StringLength(1)]
        public string QryGroup52 { get; set; }

        [StringLength(1)]
        public string QryGroup53 { get; set; }

        [StringLength(1)]
        public string QryGroup54 { get; set; }

        [StringLength(1)]
        public string QryGroup55 { get; set; }

        [StringLength(1)]
        public string QryGroup56 { get; set; }

        [StringLength(1)]
        public string QryGroup57 { get; set; }

        [StringLength(1)]
        public string QryGroup58 { get; set; }

        [StringLength(1)]
        public string QryGroup59 { get; set; }

        [StringLength(1)]
        public string QryGroup60 { get; set; }

        [StringLength(1)]
        public string QryGroup61 { get; set; }

        [StringLength(1)]
        public string QryGroup62 { get; set; }

        [StringLength(1)]
        public string QryGroup63 { get; set; }

        [StringLength(1)]
        public string QryGroup64 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Balance { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DNotesBal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? OrdersBal { get; set; }

        public int? OprCount { get; set; }

        [StringLength(32)]
        public string LicTradNum { get; set; }

        public Int16 GroupNum { get; set; }

        [StringLength(15)]
        public string PymCode { get; set; }

        [StringLength(1)]
        public string VatStatus { get; set; }


        [Column(TypeName = "numeric")]
        public decimal? Discount { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(20)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Cellular { get; set; }

        [StringLength(100)]
        public string E_Mail { get; set; }

        [StringLength(100)]
        public string IntrntSite { get; set; }

        [NotMapped]
        [StringLength(3)]
        public string U_B1SYS_FiscIdType { get; set; }

        [StringLength(1)]
        public string frozenFor { get; set; }
        //Added by Portal
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}
