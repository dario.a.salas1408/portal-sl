﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OPCH")]
    public partial class OPCH
    {
        public OPCH()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        [StringLength(1)]
        public string DocType { get; set; }

        [StringLength(1)]
        public string DocStatus { get; set; }

        [StringLength(20)]
        public string ObjType { get; set; }

        public DateTime? DocDate { get; set; }

        public DateTime? DocDueDate { get; set; }

        [StringLength(15)]
        public string CardCode { get; set; }

        [StringLength(100)]
        public string CardName { get; set; }

        [StringLength(254)]
        public string Address { get; set; }

        [StringLength(100)]
        public string NumAtCard { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VatSum { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DiscPrcnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DiscSum { get; set; }

        [StringLength(3)]
        public string DocCur { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DocRate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DocTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PaidToDate { get; set; }

        [StringLength(11)]
        public string Ref1 { get; set; }

        [StringLength(11)]
        public string Ref2 { get; set; }

        [StringLength(254)]
        public string Comments { get; set; }

        [StringLength(50)]
        public string JrnlMemo { get; set; }

        public int? SlpCode { get; set; }

        public short? TrnspCode { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? TaxDate { get; set; }

        public short? UserSign { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TotalExpns { get; set; }

        public DateTime? ReqDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public int? OwnerCode { get; set; }

        [StringLength(8)]
        public string Requester { get; set; }

        [StringLength(155)]
        public string ReqName { get; set; }

        public short? Branch { get; set; }

        public short? Department { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(1)]
        public string Notify { get; set; }

        public short? GroupNum { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DpmAmnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RoundDif { get; set; }

        [StringLength(15)]
        public string PeyMethod { get; set; }

        [StringLength(1)]
        public string Confirmed { get; set; }
        public int? draftKey { get; set; }
        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}