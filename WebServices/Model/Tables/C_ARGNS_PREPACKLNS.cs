﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_PREPACKLNS")]
    public partial class C_ARGNS_PREPACKLNS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_SizeCode { get; set; }

        [StringLength(254)]
        public string U_SizeDesc { get; set; }

        [StringLength(254)]
        public string U_ColCode { get; set; }

        [StringLength(254)]
        public string U_ColDesc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Value { get; set; }

        [StringLength(254)]
        public string U_VarCode { get; set; }

        [StringLength(254)]
        public string U_VarDesc { get; set; }

        [StringLength(254)]
        public string U_SclCode { get; set; }

        [StringLength(254)]
        public string U_Sruncode { get; set; }
    }
}