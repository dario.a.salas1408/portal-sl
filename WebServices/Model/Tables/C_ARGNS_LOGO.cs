namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_LOGO")]
    public partial class C_ARGNS_LOGO
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(100)]
        public string U_CodeLogo { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(100)]
        public string U_LType { get; set; }

        public DateTime? U_LDate { get; set; }

        [StringLength(1)]
        public string U_Approved { get; set; }

        [Column(TypeName = "ntext")]
        public string U_LImagen { get; set; }

        [Column(TypeName = "ntext")]
        public string U_LArchive { get; set; }

        [StringLength(100)]
        public string U_StichCount { get; set; }

        [StringLength(100)]
        public string U_HoopSize { get; set; }

        [StringLength(100)]
        public string U_LWidth { get; set; }

        [StringLength(100)]
        public string U_LHeight { get; set; }

        [StringLength(150)]
        public string U_Comments { get; set; }
    }
}
