﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{

    [Table("OCLG")]
    public partial class OCLG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ClgCode { get; set; }
        [StringLength(15)]
        public string CardCode { get; set; }
        [Column(TypeName = "ntext")]
        public string Notes { get; set; }
        public DateTime? Recontact { get; set; }
        [StringLength(1)]
        public string Closed { get; set; }
        public short? CntctSbjct { get; set; }
        [StringLength(20)]
        public string DocType { get; set; }
        [StringLength(20)]
        public string DocNum { get; set; }
        [StringLength(20)]
        public string DocEntry { get; set; }
        public short? AttendUser { get; set; }
        public int? AttendEmpl { get; set; }
        [StringLength(1)]
        public string Action { get; set; }
        [StringLength(100)]
        public string Details { get; set; }
        public short? CntctType { get; set; }
        [StringLength(1)]
        public string Priority { get; set; }
        public DateTime? endDate { get; set; }
        [StringLength(1)]
        public string inactive { get; set; }
        [StringLength(254)]
        public string U_ARGNS_PRDCODE { get; set; }
        [StringLength(254)]
        public string U_ARGNS_PROYECT { get; set; }
        public int? OprId { get; set; }
        public short? OprLine { get; set; }
        public int? status { get; set; }
        public int? BeginTime { get; set; }
        public int? ENDTime { get; set; }
        [Column(TypeName = "numeric")]
        public decimal? Duration { get; set; }
    }
}