﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OCRY")]
    public partial class OCRY
    {
        [Key]
        [StringLength(3)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }
    }
}