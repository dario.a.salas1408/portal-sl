namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_MODEL_CS")]
    public partial class C_ARGNS_MODEL_CS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(100)]
        public string U_CSCode { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(1)]
        public string U_Selected { get; set; }
    }
}
