namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OPYM")]
    public partial class OPYM
    {
        [Key]
        [StringLength(15)]
        public string PayMethCod { get; set; }

        [StringLength(100)]
        public string Descript { get; set; }

        [StringLength(1)]
        public string Type { get; set; }
    }
}
