namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OCTG")]
    public partial class OCTG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short GroupNum { get; set; }

        [Required]
        [StringLength(100)]
        public string PymntGroup { get; set; }

    }
}
