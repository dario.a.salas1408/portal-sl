﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OALR")]
    public class OALR
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Code { get; set; }

        [StringLength(1)]
        public string Type { get; set; }

        [StringLength(1)]
        public string Priority { get; set; }

        [StringLength(254)]
        public string Subject { get; set; }

        [Column(TypeName = "ntext")]
        public string UserText { get; set; }

        public int? DataCols { get; set; }

        [Column(TypeName = "ntext")]
        public string DataParams { get; set; }

        [Column(TypeName = "ntext")]
        public string MsgData { get; set; }

        public short? UserSign { get; set; }

    }
}