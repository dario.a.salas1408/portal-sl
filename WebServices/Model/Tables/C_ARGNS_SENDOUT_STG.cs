﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SENDOUT_STG")]
    public partial class C_ARGNS_SENDOUT_STG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public int? Period { get; set; }

        public short? Instance { get; set; }

        public int? Series { get; set; }

        [StringLength(1)]
        public string Handwrtten { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(1)]
        public string RequestStatus { get; set; }

        [StringLength(8)]
        public string Creator { get; set; }

        [Column(TypeName = "ntext")]
        public string Remark { get; set; }

        public short? U_CutTic { get; set; }

        [StringLength(20)]
        public string U_StgCode { get; set; }

        [StringLength(20)]
        public string U_Oper { get; set; }

        [StringLength(100)]
        public string U_OperDesc { get; set; }

        [StringLength(15)]
        public string U_CtrCode { get; set; }

        [StringLength(100)]
        public string U_CtrName { get; set; }

        [StringLength(20)]
        public string U_Status { get; set; }

        [StringLength(8)]
        public string U_UserCode { get; set; }

        [StringLength(30)]
        public string U_UserName { get; set; }

        [StringLength(150)]
        public string U_Comments { get; set; }
    }
}