namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ITL1
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LogEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(20)]
        public string ItemCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SysNumber { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Quantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? AllocQty { get; set; }

        public int? MdAbsEntry { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ReleaseQty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PickedQty { get; set; }
    }
}
