namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OITL")]
    public partial class OITL
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LogEntry { get; set; }

        public int? TransId { get; set; }

        [StringLength(20)]
        public string ItemCode { get; set; }

        [StringLength(100)]
        public string ItemName { get; set; }

        public int? ManagedBy { get; set; }

        public int? DocEntry { get; set; }

        public int? DocLine { get; set; }

        public int? DocType { get; set; }

        public int? DocNum { get; set; }

        public int? BaseEntry { get; set; }

        public int? BaseLine { get; set; }

        public int? BaseType { get; set; }

        public int? ApplyEntry { get; set; }

        public int? ApplyLine { get; set; }

        public int? ApplyType { get; set; }

        public DateTime? DocDate { get; set; }

        [StringLength(15)]
        public string CardCode { get; set; }

        [StringLength(100)]
        public string CardName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DocQty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? StockQty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DefinedQty { get; set; }

        public int? StockEff { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? LocType { get; set; }

        [StringLength(8)]
        public string LocCode { get; set; }

        public int? AppDocNum { get; set; }

        [StringLength(11)]
        public string VersionNum { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public short? Instance { get; set; }

        public int? SubLineNum { get; set; }

        public int? BSubLineNo { get; set; }

        public int? AppSubLine { get; set; }

        public int? ActBaseTp { get; set; }

        public int? ActBaseEnt { get; set; }

        public int? ActBaseLn { get; set; }

        public int? ActBasSubL { get; set; }

        public int? AllocateTp { get; set; }

        public int? AllocatEnt { get; set; }

        public int? AllocateLn { get; set; }

        public short? CreateTime { get; set; }
    }
}
