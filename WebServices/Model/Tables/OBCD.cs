﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OBCD")]
    public partial class OBCD
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BcdEntry { get; set; }

        [Required]
        [StringLength(16)]
        public string BcdCode { get; set; }

        [Required]
        [StringLength(20)]
        public string ItemCode { get; set; }

        public int? UomEntry { get; set; }
    }
}