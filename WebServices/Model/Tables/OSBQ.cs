﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("OSBQ")]
    public partial class OSBQ
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AbsEntry { get; set; }

        [StringLength(50)]
        public string ItemCode { get; set; }

        public int SnBMDAbs { get; set; }

        public int BinAbs { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? OnHandQty { get; set; }

        [StringLength(8)]
        public string WhsCode { get; set; }
    }
}