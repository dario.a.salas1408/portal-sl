﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MD_FOOTWEAR")]
    public partial class C_ARGNS_MD_FOOTWEAR
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_Sfabric { get; set; }

        [StringLength(254)]
        public string U_Lining { get; set; }

        [StringLength(254)]
        public string U_Insole { get; set; }

        [StringLength(254)]
        public string U_Outsole { get; set; }

        [StringLength(254)]
        public string U_Heel { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}