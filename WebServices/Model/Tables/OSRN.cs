namespace WebServices.Model.Tables
{
    using ARGNS.Model.Implementations.View;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OSRN")]
    public partial class OSRN
    {
        public OSRN()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        [Required]
        [StringLength(20)]
        public string ItemCode { get; set; }

        public int SysNumber { get; set; }

        [StringLength(36)]
        public string DistNumber { get; set; }

        [StringLength(36)]
        public string MnfSerial { get; set; }

        [StringLength(36)]
        public string LotNumber { get; set; }

        public DateTime? ExpDate { get; set; }

        public DateTime? MnfDate { get; set; }

        public DateTime? InDate { get; set; }

        public DateTime? GrntStart { get; set; }

        public DateTime? GrntExp { get; set; }

        public DateTime? CreateDate { get; set; }

        [StringLength(100)]
        public string Location { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "ntext")]
        public string Notes { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public short? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public short? Instance { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AbsEntry { get; set; }

        [StringLength(20)]
        public string ObjType { get; set; }

        [StringLength(100)]
        public string itemName { get; set; }

        public int? LogInstanc { get; set; }

        public short? UserSign2 { get; set; }

        public DateTime? UpdateDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? CostTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Quantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? QuantOut { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PriceDiff { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Balance { get; set; }

        public int? TrackingNt { get; set; }

        public int? TrackiNtLn { get; set; }

        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}
