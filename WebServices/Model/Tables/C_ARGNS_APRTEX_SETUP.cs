﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_APRTEX_SETUP")]
    public partial class C_ARGNS_APRTEX_SETUP
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(1)]
        public string U_SBODec { get; set; }

        public short? U_BOMDSPL { get; set; }

        [StringLength(15)]
        public string U_BPDef { get; set; }
    }
}