﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("OITT")]
    public partial class OITT
    {
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(254)]
        public string U_ARGNS_PPCode { get; set; }
    }
}