namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OSLP")]
    public partial class OSLP
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SlpCode { get; set; }

        [Required]
        [StringLength(155)]
        public string SlpName { get; set; }
    }
}
