﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public class RCT2
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocNum { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InvoiceId { get; set; }

        public int? DocEntry { get; set; }

        public int? DocTransId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SumApplied { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? AppliedFC { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? AppliedSys { get; set; }
    }
}