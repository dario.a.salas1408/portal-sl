﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class UFD1
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string TableID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short FieldID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short IndexID { get; set; }

        [StringLength(254)]
        public string FldValue { get; set; }

        [StringLength(254)]
        public string Descr { get; set; }
    }
}