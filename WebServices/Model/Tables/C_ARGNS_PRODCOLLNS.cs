﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_PRODCOLLNS")]
    public partial class C_ARGNS_PRODCOLLNS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(16)]
        public string U_CodeBars { get; set; }

        public short? U_ProdOrd { get; set; }

        [StringLength(254)]
        public string U_Model { get; set; }

        [StringLength(254)]
        public string U_Color { get; set; }

        [StringLength(254)]
        public string U_Scale { get; set; }

        [StringLength(254)]
        public string U_Size { get; set; }

        [StringLength(254)]
        public string U_Variable { get; set; }

        [StringLength(20)]
        public string U_WorkCtr { get; set; }

        [StringLength(50)]
        public string U_ResourID { get; set; }

        [StringLength(254)]
        public string U_ResName { get; set; }

        [StringLength(254)]
        public string U_OperCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Qty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_BadQty { get; set; }

        public short? U_Stage { get; set; }
    }
}