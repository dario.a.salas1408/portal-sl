﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SZRUNSCALELN")]
    public partial class C_ARGNS_SZRUNSCALELN
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_SizeRunCode { get; set; }

        [StringLength(254)]
        public string U_SizeCode { get; set; }

        public short? U_Percent { get; set; }

        public short? U_Qty { get; set; }
    }
}