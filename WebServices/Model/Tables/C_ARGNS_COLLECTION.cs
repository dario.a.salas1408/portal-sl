﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_COLLECTION")]
    public partial class C_ARGNS_COLLECTION
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(100)]
        public string U_CollCode { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(30)]
        public string U_Season { get; set; }

    }
}