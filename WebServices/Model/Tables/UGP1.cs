﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class UGP1
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UgpEntry { get; set; }

        public int UomEntry { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? AltQty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? BaseQty { get; set; }

        public int? LogInstanc { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNum { get; set; }

        public short? WghtFactor { get; set; }

        public int? UdfFactor { get; set; }
    }
}