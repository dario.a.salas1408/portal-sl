﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_FBOOK")]
    public partial class C_ARGNS_FBOOK
    {
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        public int DocEntry { get; set; }

        [StringLength(50)]
        public string U_Season { get; set; }

        public DateTime? U_DateF { get; set; }

        public DateTime? U_DateT { get; set; }

        [StringLength(8)]
        public string U_SalesP { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        public string Name { get; set; }
    }
}