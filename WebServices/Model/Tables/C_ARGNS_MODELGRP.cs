﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MODELGRP")]
    public partial class C_ARGNS_MODELGRP
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(100)]
        public string U_NFRCode { get; set; }

        [StringLength(30)]
        public string U_ProdLine { get; set; }
    }
}