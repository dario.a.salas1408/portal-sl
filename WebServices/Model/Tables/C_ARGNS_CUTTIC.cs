﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_CUTTIC")]
    public partial class C_ARGNS_CUTTIC
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        public int? Period { get; set; }

        public short? Instance { get; set; }

        public int? Series { get; set; }

        [StringLength(1)]
        public string Handwrtten { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(1)]
        public string RequestStatus { get; set; }

        [StringLength(8)]
        public string Creator { get; set; }

        [Column(TypeName = "ntext")]
        public string Remark { get; set; }

        [StringLength(15)]
        public string U_CardCode { get; set; }

        public DateTime? U_PostDate { get; set; }

        public DateTime? U_DueDate { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Comments { get; set; }

        [StringLength(20)]
        public string U_OperNo { get; set; }

        public short? U_SegmentCC { get; set; }

        [StringLength(50)]
        public string U_DocNumb { get; set; }

        [StringLength(50)]
        public string U_CutCode { get; set; }

        [StringLength(30)]
        public string U_PriCode { get; set; }
    }
}