namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ATC1
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AbsEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Line { get; set; }

        [Column(TypeName = "ntext")]
        public string srcPath { get; set; }

        [Column(TypeName = "ntext")]
        public string trgtPath { get; set; }

        [StringLength(254)]
        public string FileName { get; set; }

        [StringLength(8)]
        public string FileExt { get; set; }

        public DateTime? Date { get; set; }

        public int? UsrID { get; set; }

        [StringLength(1)]
        public string Copied { get; set; }

        [StringLength(1)]
        public string Override { get; set; }

        [StringLength(254)]
        public string subPath { get; set; }
    }
}
