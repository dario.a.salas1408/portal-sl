﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_CNTM")]
    public partial class C_ARGNS_CNTM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int? DocNum { get; set; }

        [StringLength(50)]
        public string U_Shipment { get; set; }

        [StringLength(30)]
        public string U_Broker { get; set; }

        [StringLength(50)]
        public string U_Status { get; set; }

        [StringLength(8)]
        public string U_ShipVia { get; set; }

        public DateTime? U_ESD { get; set; }

        public DateTime? U_EDA { get; set; }

        public DateTime? U_EDW { get; set; }

        public DateTime? U_ASD { get; set; }

        public DateTime? U_ADA { get; set; }

        public DateTime? U_ADW { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Quantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Amount { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Weight { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Volume { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Carton { get; set; }

        [StringLength(50)]
        public string U_POE { get; set; }

        [StringLength(100)]
        public string U_UserTxt1 { get; set; }

        [StringLength(100)]
        public string U_UserTxt2 { get; set; }

        [StringLength(100)]
        public string U_UserTxt3 { get; set; }

        [StringLength(100)]
        public string U_UserTxt4 { get; set; }

        [StringLength(50)]
        public string U_ObjType { get; set; }
    }
}