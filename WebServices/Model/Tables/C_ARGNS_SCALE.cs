﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SCALE")]
    public partial class C_ARGNS_SCALE
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(254)]
        public string U_SclCode { get; set; }

        [StringLength(254)]
        public string U_SclDesc { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        public short? U_VOrder { get; set; }
    }
}