﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_AMBIENT")]
    public partial class C_ARGNS_AMBIENT
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string U_AmbCode { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [Column(TypeName = "ntext")]
        public string U_DetDesc { get; set; }

        [StringLength(100)]
        public string U_Collect { get; set; }
    }
}