﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OITW")]
    public partial class OITW
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string ItemCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(8)]
        public string WhsCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? OnHand { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? IsCommited { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? OnOrder { get; set; }
    }
}