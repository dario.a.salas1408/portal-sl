﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_CS_PATTERNS")]
    public partial class C_ARGNS_CS_PATTERNS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(100)]
        public string U_PattCode { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(20)]
        public string U_ItemCode { get; set; }

        [StringLength(254)]
        public string U_ItemName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Quantity { get; set; }

        [StringLength(5)]
        public string U_UoM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_PattQty { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }
    }
}