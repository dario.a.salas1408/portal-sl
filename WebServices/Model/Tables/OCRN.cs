namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OCRN")]
    public partial class OCRN
    {
        [Key]
        [StringLength(3)]
        public string CurrCode { get; set; }

        [StringLength(20)]
        public string CurrName { get; set; }
    }
}
