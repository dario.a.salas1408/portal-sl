namespace WebServices.Model.Tables
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("@ARGNS_CRPATH")]
    public partial class C_ARGNS_CRPATH
    {
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(100)]
        public string U_ProyCode { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(254)]
        public string U_Model { get; set; }

        [StringLength(100)]
        public string U_Workflow { get; set; }

        [StringLength(100)]
        public string U_Status { get; set; }

        [StringLength(100)]
        public string U_Calendar { get; set; }

        [StringLength(100)]
        public string U_Planning { get; set; }

        public DateTime? U_SDate { get; set; }

        public DateTime? U_CDate { get; set; }

        [StringLength(100)]
        public string U_SalesO { get; set; }

        [StringLength(10)]
        public string U_DocType { get; set; }

    }
}
