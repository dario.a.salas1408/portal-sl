﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("OPOR")]
    public partial class OPOR
    {
        public OPOR()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        public int DocNum { get; set; }

        [StringLength(1)]
        public string DocType { get; set; }

        [StringLength(1)]
        public string DocStatus { get; set; }

        [StringLength(20)]
        public string ObjType { get; set; }

        public DateTime? DocDate { get; set; }

        public DateTime? DocDueDate { get; set; }

        [StringLength(15)]
        public string CardCode { get; set; }

        [StringLength(100)]
        public string CardName { get; set; }

        [StringLength(254)]
        public string Address { get; set; }

        [StringLength(100)]
        public string NumAtCard { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DiscPrcnt { get; set; }

        [StringLength(3)]
        public string DocCur { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DocRate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DocTotal { get; set; }

        [StringLength(254)]
        public string Comments { get; set; }

        [StringLength(50)]
        public string JrnlMemo { get; set; }

        public short? GroupNum { get; set; }

        public int? SlpCode { get; set; }

        public short? TrnspCode { get; set; }

        public DateTime? TaxDate { get; set; }

        public DateTime? CancelDate { get; set; }

        public DateTime? ReqDate { get; set; }

        [StringLength(20)]
        public string Project { get; set; }

        [StringLength(1)]
        public string RevisionPo { get; set; }

        [StringLength(1)]
        public string SummryType { get; set; }

        public int? OwnerCode { get; set; }

        public int? CntctCode { get; set; }

        [StringLength(15)]
        public string PeyMethod { get; set; }

        [StringLength(254)]
        public string Address2 { get; set; }

        [StringLength(1)]
        public string FatherType { get; set; }

        [StringLength(50)]
        public string ShipToCode { get; set; }

        [StringLength(1)]
        public string CurSource { get; set; }

        public int? draftKey { get; set; }

        public short? UserSign { get; set; }
        
        [Column(TypeName = "numeric")]
        public decimal? TotalExpns { get; set; }

        [StringLength(1)]
        public string Confirmed { get; set; }

        public int? AtcEntry { get; set; }

        public int? BPLId { get; set; }
        public string BPLName { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}