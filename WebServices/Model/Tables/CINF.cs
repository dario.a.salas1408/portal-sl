﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("CINF")]
    public partial class CINF
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Version { get; set; }

        [StringLength(1)]
        public string EnblExpns { get; set; }

        [StringLength(3)]
        public string LawsSet { get; set; }

    }
}