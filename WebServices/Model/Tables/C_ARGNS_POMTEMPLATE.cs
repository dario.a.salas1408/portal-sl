﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_POMTEMPLATE")]
    public partial class C_ARGNS_POMTEMPLATE
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        public int DocEntry { get; set; }

        [StringLength(100)]
        public string U_CodeTmpl { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(30)]
        public string U_ProdGrp { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(254)]
        public string U_Scale { get; set; }

        [StringLength(254)]
        public string U_SSize { get; set; }

        [StringLength(254)]
        public string U_File { get; set; }

        [StringLength(254)]
        public string U_FType { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Path { get; set; }

        [StringLength(1)]
        public string U_MenType { get; set; }
    }
}