﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MD_CARE_INST")]
    public partial class C_ARGNS_MD_CARE_INST
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_CareText { get; set; }

        [StringLength(254)]
        public string U_CareCode { get; set; }

        [StringLength(2)]
        public string U_CareLbl { get; set; }

        [StringLength(254)]
        public string U_ColCode { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}