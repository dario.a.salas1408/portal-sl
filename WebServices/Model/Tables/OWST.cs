﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OWST")]
    public partial class OWST
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int WstCode { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }
    }
}