﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class JDT1
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TransId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Line_ID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Debit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Credit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SYSCred { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? SYSDeb { get; set; }

        [StringLength(3)]
        public string FCCurrency { get; set; }

        public DateTime? DueDate { get; set; }

        [StringLength(15)]
        public string ShortName { get; set; }

        public int? IntrnMatch { get; set; }

        [StringLength(20)]
        public string TransType { get; set; }

        public DateTime? RefDate { get; set; }

        [StringLength(100)]
        public string Ref1 { get; set; }

        [StringLength(100)]
        public string Ref2 { get; set; }

        public DateTime? TaxDate { get; set; }

        [StringLength(1)]
        public string DebCred { get; set; }
        public int? SourceID { get; set; }
        public short? SourceLine { get; set; }
    }
}