﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OAIB")]
    public class OAIB
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AlertCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short UserSign { get; set; }

        [StringLength(1)]
        public string Opened { get; set; }

        public DateTime? RecDate { get; set; }

        [StringLength(1)]
        public string WasRead { get; set; }

        [StringLength(1)]
        public string Deleted { get; set; }

        [StringLength(1)]
        public string Failed { get; set; }
    }
}