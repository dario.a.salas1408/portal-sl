namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_DOCNUMBER_LIN")]
    public partial class C_ARGNS_DOCNUMBER_LIN
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(100)]
        public string U_Desc { get; set; }

        public short? U_firstno { get; set; }

        public short? U_nextno { get; set; }

        public short? U_lastno { get; set; }

        [StringLength(100)]
        public string U_prefix { get; set; }

        [StringLength(100)]
        public string U_sufix { get; set; }

        public short? U_size { get; set; }

        [StringLength(1)]
        public string U_lock { get; set; }

        [StringLength(1)]
        public string U_updated { get; set; }
    }
}
