﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OSCN")]
    public partial class OSCN
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string ItemCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(15)]
        public string CardCode { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Substitute { get; set; }

        [StringLength(1)]
        public string ShowSCN { get; set; }
    }
}