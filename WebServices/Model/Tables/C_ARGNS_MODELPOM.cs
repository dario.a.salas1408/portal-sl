﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MODELPOM")]
    public class C_ARGNS_MODELPOM
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_Desc { get; set; }

        [StringLength(254)]
        public string U_SizeCode { get; set; }

        [StringLength(254)]
        public string U_SizeDesc { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Value { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_TolPosit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_TolNeg { get; set; }

        [StringLength(1)]
        public string U_QAPoint { get; set; }

        [StringLength(30)]
        public string U_POM { get; set; }

        [StringLength(254)]
        public string U_SclPom { get; set; }

        [StringLength(254)]
        public string U_PomCode { get; set; }
    }
}