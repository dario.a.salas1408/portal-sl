namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CRD2
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(15)]
        public string CardCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNum { get; set; }

        [StringLength(15)]
        public string PymCode { get; set; }

        public int? LogInstanc { get; set; }

        public short? ObjType { get; set; }
    }
}
