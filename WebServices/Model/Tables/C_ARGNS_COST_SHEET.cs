namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_COST_SHEET")]
    public partial class C_ARGNS_COST_SHEET
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        [StringLength(3)]
        public string U_Currency { get; set; }

        [StringLength(254)]
        public string U_Description { get; set; }

        [StringLength(20)]
        public string U_OTCode { get; set; }

        [StringLength(20)]
        public string U_SchCode { get; set; }

        [StringLength(100)]
        public string U_CodeTmpl { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_PurchPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_TMaterials { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_TOperations { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_IndrCost { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_TCost { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_CostPercent { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_SlsPercent { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_StrFactor { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_FPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Duty { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_IntTransp { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Freight { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Margin { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_DDP { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_TMargin { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_CIF { get; set; }

        [StringLength(254)]
        public string U_UserText1 { get; set; }

        [StringLength(254)]
        public string U_UserText2 { get; set; }

        [StringLength(254)]
        public string U_UserText3 { get; set; }

    }
}
