﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OADM")]
    public partial class OADM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Code { get; set; }
        [StringLength(1)]
        public string TimeFormat { get; set; }
        [StringLength(1)]
        public string DateFormat { get; set; }
        [StringLength(1)]
        public string DateSep { get; set; }
        public short? SumDec { get; set; }
        public short? PriceDec { get; set; }
        public short? RateDec { get; set; }
        public short? QtyDec { get; set; }
        public short? PercentDec { get; set; }
        public short? MeasureDec { get; set; }
        public short? QueryDec { get; set; }
        [StringLength(1)]
        public string DecSep { get; set; }
        [StringLength(100)]
        public string CompnyName { get; set; }
        [StringLength(3)]
        public string Country { get; set; }
        [StringLength(100)]
        public string DfltWhs { get; set; }

    }
}