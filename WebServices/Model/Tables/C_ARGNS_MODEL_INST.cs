namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_MODEL_INST")]
    public partial class C_ARGNS_MODEL_INST
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(100)]
        public string U_Sector { get; set; }

        [StringLength(254)]
        public string U_ModCode { get; set; }

        [StringLength(254)]
        public string U_InstCod { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Instuct { get; set; }

        [StringLength(100)]
        public string U_Descrip { get; set; }

        [StringLength(100)]
        public string U_Imag { get; set; }

        [StringLength(100)]
        public string U_Version { get; set; }

        [StringLength(50)]
        public string U_Active { get; set; }
    }
}
