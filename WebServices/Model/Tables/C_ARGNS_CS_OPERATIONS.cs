namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_CS_OPERATIONS")]
    public partial class C_ARGNS_CS_OPERATIONS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string U_ItemCode { get; set; }

        [StringLength(254)]
        public string U_ItemName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Quantity { get; set; }

        [StringLength(8)]
        public string U_UoM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Price { get; set; }

        [StringLength(3)]
        public string U_Currency { get; set; }

        public short? U_PList { get; set; }
        
    }
}
