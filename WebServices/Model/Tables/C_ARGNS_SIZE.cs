﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_SIZE")]
    public partial class C_ARGNS_SIZE
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(254)]
        public string U_SizeCode { get; set; }

        [StringLength(254)]
        public string U_SizeDesc { get; set; }

        public short? U_VOrder { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }
    }
}