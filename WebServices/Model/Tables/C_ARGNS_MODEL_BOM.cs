namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_MODEL_BOM")]
    public partial class C_ARGNS_MODEL_BOM
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(254)]
        public string U_ItemCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Quantity { get; set; }

        [StringLength(5)]
        public string U_UOM { get; set; }

        [StringLength(8)]
        public string U_Whse { get; set; }

        [StringLength(50)]
        public string U_IssueMth { get; set; }

        public short? U_PList { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_UPrice { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Total { get; set; }

        [StringLength(150)]
        public string U_Comments { get; set; }

        [StringLength(50)]
        public string U_TreeType { get; set; }

        [StringLength(150)]
        public string U_PVendor { get; set; }
    }
}
