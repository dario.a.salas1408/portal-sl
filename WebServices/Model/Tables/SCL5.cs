namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCL5
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SrvcCallId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short Line { get; set; }

        public int? ClgID { get; set; }

        [StringLength(20)]
        public string ObjectType { get; set; }

        public int? LogInstanc { get; set; }

        public short? UserSign { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? UserSign2 { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? VisOrder { get; set; }
    }
}
