using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace WebServices.Model.Tables
{
    public partial class ITM1
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(20)]
        public string ItemCode { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short PriceList { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Price { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

    }
}
