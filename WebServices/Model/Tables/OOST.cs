namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OOST")]
    public partial class OOST
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Num { get; set; }

        [StringLength(30)]
        public string Descript { get; set; }

        public short StepId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? CloPrcnt { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        public short? UserSign { get; set; }

        [StringLength(1)]
        public string SalesStage { get; set; }

        [StringLength(1)]
        public string PurStage { get; set; }
    }
}
