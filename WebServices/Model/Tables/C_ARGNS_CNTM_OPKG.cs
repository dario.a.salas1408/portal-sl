﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_CNTM_OPKG")]
    public partial class C_ARGNS_CNTM_OPKG
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        public short? U_PackageNum { get; set; }

        [StringLength(30)]
        public string U_PackageTyp { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Weight { get; set; }

        [StringLength(10)]
        public string U_WeightUnit { get; set; }

        [StringLength(20)]
        public string U_ARGNS_UCC128 { get; set; }

        [StringLength(30)]
        public string U_ARGNS_TRACKN { get; set; }

        public short? U_CntnerNum { get; set; }
    }
}