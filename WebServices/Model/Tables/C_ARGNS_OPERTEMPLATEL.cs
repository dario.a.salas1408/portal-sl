﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_OPERTEMPLATEL")]
    public partial class C_ARGNS_OPERTEMPLATEL
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(20)]
        public string U_ItemCode { get; set; }

        [StringLength(254)]
        public string U_ItemName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Quantity { get; set; }

        [StringLength(8)]
        public string U_UoM { get; set; }

        public short? U_PList { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Price { get; set; }

        [StringLength(1)]
        public string U_EndProd { get; set; }

        [StringLength(20)]
        public string U_WorkCtr { get; set; }

        [StringLength(50)]
        public string U_ResourID { get; set; }

        [StringLength(254)]
        public string U_ResName { get; set; }

        [StringLength(3)]
        public string U_Currency { get; set; }

        public short? U_TRAPlatz { get; set; }

        public short? U_TR2APlat { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_TEAPlatz { get; set; }

        public short? U_menge_je { get; set; }

        public short? U_menge_ze { get; set; }

        public short? U_nutzen { get; set; }

        public short? U_anzahl { get; set; }

        [StringLength(1)]
        public string U_bde { get; set; }

        public short? U_SAM { get; set; }

        [StringLength(15)]
        public string U_ResCode { get; set; }

        public short? U_LeadTime { get; set; }

        public int? U_Seconds { get; set; }

        [StringLength(20)]
        public string U_SOTCode { get; set; }
    }
}