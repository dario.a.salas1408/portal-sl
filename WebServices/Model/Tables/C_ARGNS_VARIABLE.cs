﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_VARIABLE")]
    public partial class C_ARGNS_VARIABLE
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(254)]
        public string U_VarCode { get; set; }

        [StringLength(254)]
        public string U_VarDesc { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(30)]
        public string U_ATGrp { get; set; }
    }
}