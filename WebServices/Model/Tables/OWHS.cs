using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    public partial class OWHS
    {
        [Key]
        [StringLength(8)]
        public string WhsCode { get; set; }

        [StringLength(100)]
        public string WhsName { get; set; }

        public int? BPLid { get; set; }

        [StringLength(1)]
        public string BinActivat { get; set; }
    }
}
