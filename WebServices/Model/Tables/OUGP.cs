﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebServices.Model.Tables
{
    [Table("OUGP")]
    public partial class OUGP
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UgpEntry { get; set; }

        [Required]
        [StringLength(20)]
        public string UgpCode { get; set; }

        [StringLength(100)]
        public string UgpName { get; set; }

        public int? BaseUom { get; set; }

    }
}