﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_MD_FDETAILS")]
    public partial class C_ARGNS_MD_FDETAILS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(254)]
        public string U_HHeight { get; set; }

        [StringLength(254)]
        public string U_BHeight { get; set; }

        [StringLength(254)]
        public string U_BWidth { get; set; }

        [StringLength(254)]
        public string U_Strap { get; set; }

        [StringLength(254)]
        public string U_Seams { get; set; }

        [StringLength(254)]
        public string U_Others { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }
    }
}