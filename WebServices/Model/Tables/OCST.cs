﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OCST")]
    public partial class OCST
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(3)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(3)]
        public string Country { get; set; }

        [StringLength(100)]
        public string Name { get; set; }
    }
}