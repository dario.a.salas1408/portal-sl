﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OOCR")]
    public partial class OOCR
    {
        [Key]
        [StringLength(8)]
        public string OcrCode { get; set; }

        [StringLength(30)]
        public string OcrName { get; set; }

        [StringLength(1)]
        public string Locked { get; set; }

        [StringLength(1)]
        public string Active { get; set; }

    }
}