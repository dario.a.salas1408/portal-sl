﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OEXD")]
    public partial class OEXD
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ExpnsCode { get; set; }

        [Required]
        [StringLength(20)]
        public string ExpnsName { get; set; }

        [StringLength(15)]
        public string RevAcct { get; set; }

        [StringLength(15)]
        public string ExpnsAcct { get; set; }

        [StringLength(1)]
        public string TaxLiable { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RevFixSum { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ExpFixSum { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public short? UserSign { get; set; }

        [StringLength(8)]
        public string VatGroupI { get; set; }

        [StringLength(8)]
        public string VatGroupO { get; set; }

        [StringLength(1)]
        public string DistrbMthd { get; set; }

        [StringLength(1)]
        public string In1099 { get; set; }

        [StringLength(15)]
        public string ExpOfstAct { get; set; }

        [StringLength(1)]
        public string WTLiable { get; set; }

        [StringLength(1)]
        public string BaseMethod { get; set; }

        [StringLength(1)]
        public string Stock { get; set; }

        [StringLength(1)]
        public string LstPchPrce { get; set; }

        [StringLength(1)]
        public string SalseRpt { get; set; }

        [StringLength(1)]
        public string PchRpt { get; set; }

        [StringLength(15)]
        public string RevExmAcct { get; set; }

        [StringLength(15)]
        public string ExpnsExAct { get; set; }

        [StringLength(15)]
        public string RevRetAct { get; set; }

        [StringLength(1)]
        public string ExpnsType { get; set; }

        [StringLength(8)]
        public string OcrCode { get; set; }

        [StringLength(1)]
        public string TaxDisMthd { get; set; }

        [StringLength(8)]
        public string OcrCode2 { get; set; }

        [StringLength(8)]
        public string OcrCode3 { get; set; }

        [StringLength(8)]
        public string OcrCode4 { get; set; }

        [StringLength(8)]
        public string OcrCode5 { get; set; }

        [StringLength(100)]
        public string OcrCodeX { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UserSign2 { get; set; }

        [StringLength(20)]
        public string Project { get; set; }

        [StringLength(1)]
        public string Intrastat { get; set; }
    }
}