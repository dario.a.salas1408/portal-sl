namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OINS")]
    public partial class OIN
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int insID { get; set; }

        [StringLength(15)]
        public string customer { get; set; }

        [StringLength(100)]
        public string custmrName { get; set; }

        public int? contactCod { get; set; }     

        [StringLength(36)]
        public string manufSN { get; set; }

        [StringLength(36)]
        public string internalSN { get; set; } 

        [StringLength(20)]
        public string itemCode { get; set; }

        [StringLength(100)]
        public string itemName { get; set; }

        public short? itemGroup { get; set; }      
    }
}
