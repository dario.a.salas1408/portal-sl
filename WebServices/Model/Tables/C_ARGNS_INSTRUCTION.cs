﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_INSTRUCTION")]
    public partial class C_ARGNS_INSTRUCTION
    {
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(254)]
        public string U_InsCode { get; set; }

        [StringLength(254)]
        public string U_InsDesc { get; set; }

        [StringLength(254)]
        public string U_Instruction { get; set; }

        [StringLength(254)]
        public string U_Section { get; set; }

        [StringLength(254)]
        public string U_Version { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }
    }
}