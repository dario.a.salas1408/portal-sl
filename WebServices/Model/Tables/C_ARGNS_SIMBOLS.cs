namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_SIMBOLS")]
    public partial class C_ARGNS_SIMBOLS
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [StringLength(254)]
        public string U_Description { get; set; }

        [StringLength(254)]
        public string U_File { get; set; }

        [StringLength(254)]
        public string U_FType { get; set; }

        [Column(TypeName = "ntext")]
        public string U_Path { get; set; }

        [StringLength(1)]
        public string U_Washfont { get; set; }
    }
}
