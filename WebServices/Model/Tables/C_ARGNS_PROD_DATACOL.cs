﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_PROD_DATACOL")]
    public partial class C_ARGNS_PROD_DATACOL
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public DateTime? U_Date { get; set; }

        public short? U_empID { get; set; }

        [StringLength(20)]
        public string U_ShiftCode { get; set; }

        public short? U_MinPres { get; set; }

        public short? U_UnprodMins { get; set; }

        [StringLength(100)]
        public string U_Vendor { get; set; }

        [StringLength(20)]
        public string U_Branch { get; set; }
    }
}