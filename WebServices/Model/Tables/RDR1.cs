﻿using ARGNS.Model.Implementations.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;


namespace WebServices.Model.Tables
{
    public partial class RDR1
    {
        public RDR1()
        {
            this.MappedUdf = new List<UDF_ARGNS>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNum { get; set; }

        public int? TargetType { get; set; }

        public int? TrgetEntry { get; set; }

        [StringLength(16)]
        public string BaseRef { get; set; }

        public int? BaseType { get; set; }

        public int? BaseEntry { get; set; }

        public int? BaseLine { get; set; }

        [StringLength(20)]
        public string ItemCode { get; set; }

        [StringLength(100)]
        public string Dscription { get; set; }

        [StringLength(20)]
        public string SubCatNum { get; set; }

        [StringLength(15)]
        public string AcctCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Quantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Price { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PriceBefDi { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        [StringLength(8)]
        public string WhsCode { get; set; }

        [StringLength(8)]
        public string TaxCode { get; set; }

        [StringLength(20)]
        public string Project { get; set; }

        [StringLength(8)]
        public string OcrCode { get; set; }

        [StringLength(100)]
        public string FreeTxt { get; set; }

        public DateTime? ShipDate { get; set; }

        [StringLength(20)]
        public string UomCode { get; set; }

        public int? UomEntry { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VatPrcnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PriceAfVAT { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VatSum { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? GTotal { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DiscPrcnt { get; set; }

        [StringLength(1)]
        public string LineStatus { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DelivrdQty { get; set; }

        [StringLength(50)]
        public string ShipToCode { get; set; }

        [StringLength(1)]
        public string SpecPrice { get; set; }

        [StringLength(8)]
        public string OcrCode2 { get; set; }

        //Contenedor de udfs
        [NotMapped]
        public List<UDF_ARGNS> MappedUdf { get; set; }

        public decimal OpenQty { get; set; }

        public string TreeType { get; set; }
    }
}