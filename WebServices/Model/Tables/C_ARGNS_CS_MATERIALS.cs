namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_CS_MATERIALS")]
    public partial class C_ARGNS_CS_MATERIALS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string U_ItemCode { get; set; }

        [StringLength(254)]
        public string U_ItemName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Quantity { get; set; }

        [StringLength(5)]
        public string U_UoM { get; set; }

        [StringLength(8)]
        public string U_Whse { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_UPrice { get; set; }

        [StringLength(50)]
        public string U_TreeType { get; set; }

        [StringLength(150)]
        public string U_PVendor { get; set; }

        [StringLength(8)]
        public string U_OcrCode { get; set; }

        [StringLength(3)]
        public string U_Currency { get; set; }

        [StringLength(1)]
        public string U_CxT { get; set; }

        [StringLength(1)]
        public string U_CxS { get; set; }

        [StringLength(1)]
        public string U_CxC { get; set; }

        [StringLength(1)]
        public string U_CxGT { get; set; }

        [StringLength(254)]
        public string U_ItmLinkO { get; set; }

        [StringLength(50)]
        public string U_IssueMth { get; set; }

        public short? U_PList { get; set; }

        [StringLength(150)]
        public string U_Comments { get; set; }
       
    }
}
