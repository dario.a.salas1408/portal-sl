﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("DRF1")]
    public partial class DRF1
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineNum { get; set; }

        [StringLength(20)]
        public string ItemCode { get; set; }

        [StringLength(100)]
        public string Dscription { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Quantity { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Price { get; set; }

        [StringLength(15)]
        public string LineVendor { get; set; }

        public DateTime? PQTReqDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? DiscPrcnt { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PriceBefDi { get; set; }

        [StringLength(3)]
        public string Currency { get; set; }

        public DateTime? ShipDate { get; set; }

        [StringLength(100)]
        public string FreeTxt { get; set; }

        [StringLength(8)]
        public string OcrCode { get; set; }

        [StringLength(20)]
        public string Project { get; set; }

        [StringLength(8)]
        public string TaxCode { get; set; }

        [StringLength(1)]
        public string TaxRelev { get; set; }

        [StringLength(20)]
        public string SubCatNum { get; set; }

        [StringLength(8)]
        public string WhsCode { get; set; }

        [StringLength(15)]
        public string AcctCode { get; set; }
    }
}