﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("OCQG")]
    public partial class OCQG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short GroupCode { get; set; }

        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }

        public short? UserSign { get; set; }

        [StringLength(10)]
        public string Filler { get; set; }
    }
}