namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("@ARGNS_MODEL_LOGOS")]
    public partial class C_ARGNS_MODEL_LOGOS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(30)]
        public string Code { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        [StringLength(100)]
        public string U_CodeLogo { get; set; }

        [StringLength(254)]
        public string U_LogDesc { get; set; }

        [StringLength(100)]
        public string U_Location { get; set; }

        [StringLength(100)]
        public string U_Position { get; set; }

        [StringLength(254)]
        public string U_ModColor { get; set; }

        [StringLength(254)]
        public string U_CodColow { get; set; }

        [StringLength(254)]
        public string U_Colorway { get; set; }

        [StringLength(254)]
        public string U_Variable { get; set; }
    }
}
