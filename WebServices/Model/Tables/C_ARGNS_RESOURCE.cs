﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_RESOURCE")]
    public partial class C_ARGNS_RESOURCE
    {
        [Key]
        [StringLength(30)]
        public string Code { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int DocEntry { get; set; }

        [StringLength(1)]
        public string Canceled { get; set; }

        [StringLength(20)]
        public string Object { get; set; }

        public int? LogInst { get; set; }

        public int? UserSign { get; set; }

        [StringLength(1)]
        public string Transfered { get; set; }

        public DateTime? CreateDate { get; set; }

        public short? CreateTime { get; set; }

        public DateTime? UpdateDate { get; set; }

        public short? UpdateTime { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        [StringLength(254)]
        public string U_ResCode { get; set; }

        [StringLength(254)]
        public string U_ResDesc { get; set; }

        [StringLength(30)]
        public string U_WorkCePrCode { get; set; }

        [StringLength(1)]
        public string U_Active { get; set; }

        [StringLength(30)]
        public string U_TypeRCode { get; set; }

        [StringLength(20)]
        public string U_HldCode { get; set; }

        public short? U_CapaPla { get; set; }

        public short? U_CapaThe { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Cost { get; set; }

        [StringLength(254)]
        public string U_Photo { get; set; }

        public short? U_Capacity { get; set; }

        public short? U_Count { get; set; }

        [StringLength(254)]
        public string U_ResName { get; set; }

        [StringLength(1)]
        public string U_External { get; set; }

        [StringLength(1)]
        public string U_Include { get; set; }

        public short? U_CountPart { get; set; }

        [StringLength(15)]
        public string U_Provider { get; set; }
    }
}