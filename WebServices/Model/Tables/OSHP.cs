namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OSHP")]
    public partial class OSHP
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short TrnspCode { get; set; }

        [Required]
        [StringLength(40)]
        public string TrnspName { get; set; }

        [StringLength(1)]
        public string DataSource { get; set; }

        public short? UserSign { get; set; }

        [StringLength(50)]
        public string WebSite { get; set; }
    }
}
