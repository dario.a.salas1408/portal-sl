namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OSCS")]
    public partial class OSC
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short statusID { get; set; }

        [StringLength(20)]
        public string Name { get; set; }

        [Column(TypeName = "ntext")]
        public string Descriptio { get; set; }

        [StringLength(1)]
        public string Locked { get; set; }
    }
}
