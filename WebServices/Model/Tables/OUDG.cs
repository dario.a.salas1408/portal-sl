namespace WebServices.Model.Tables
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OUDG")]
    public partial class OUDG
    {
        [Key]
        [StringLength(8)]
        public string Code { get; set; }

        [StringLength(20)]
        public string Name { get; set; }

        [StringLength(8)]
        public string Warehouse { get; set; }      
    }
}
