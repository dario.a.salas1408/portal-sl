﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WebServices.Model.Tables
{
    [Table("@ARGNS_CNTM_LINE")]
    public partial class C_ARGNS_CNTM_LINE
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DocEntry { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LineId { get; set; }

        public int? VisOrder { get; set; }

        [StringLength(20)]
        public string U_BaseEntry { get; set; }

        [StringLength(20)]
        public string U_BaseDocNo { get; set; }

        public short? U_BaseLine { get; set; }

        [StringLength(50)]
        public string U_ItemCode { get; set; }

        [StringLength(100)]
        public string U_ItemName { get; set; }

        [StringLength(8)]
        public string U_WhsCode { get; set; }

        [StringLength(20)]
        public string U_UoM { get; set; }

        public DateTime? U_ESD { get; set; }

        public DateTime? U_EDA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? U_Price { get; set; }

        [StringLength(2)]
        public string U_ObjType { get; set; }

        [StringLength(2)]
        public string U_LineStatus { get; set; }

        [StringLength(15)]
        public string U_CardCode { get; set; }

        [StringLength(100)]
        public string U_CardName { get; set; }

        [StringLength(254)]
        public string U_ARGNS_MOD { get; set; }

        [StringLength(254)]
        public string U_ARGNS_COL { get; set; }

        [StringLength(254)]
        public string U_ARGNS_SIZE { get; set; }

        [StringLength(20)]
        public string U_ItemGroup { get; set; }

        [StringLength(30)]
        public string U_ARGNS_SEASON { get; set; }

        [StringLength(100)]
        public string U_UserTxt1 { get; set; }

        [StringLength(100)]
        public string U_UserTxt2 { get; set; }

        [StringLength(100)]
        public string U_UserTxt3 { get; set; }

        [StringLength(100)]
        public string U_UserTxt4 { get; set; }
    }
}