﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveDrafToDocumentARGNS
{
    public static class SaveDraftToDocumentClass
    {
        public static string error;

        public static string SaveDraftToDocumentMethod(string userName, string userPassword, string company, string licenseServer, string server, string port, int dbType, int approvalKey, string approvalCode, string remarks, string ApproverUserName, string ApproverUserPassword)
        {
            SAPbobsCOM.Company oCompany = new SAPbobsCOM.Company();
            oCompany.UserName = userName;
            oCompany.Password = userPassword;
            oCompany.CompanyDB = company;
            oCompany.Server = server;
            oCompany.LicenseServer = licenseServer + ":" + port;
            switch (dbType)
            {
                case 6:
                    oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                    break;
                case 7:
                    oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                    break;
                case 8:
                    oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                    break;
                case 9:
                    oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                    break;
            }
            try
            {
                int ErrorCompany = oCompany.Connect();

                error = oCompany.GetLastErrorDescription();

                if (ErrorCompany == 0)
                {

                    SAPbobsCOM.ApprovalRequestsService oService = (SAPbobsCOM.ApprovalRequestsService)oCompany.GetCompanyService().GetBusinessService(SAPbobsCOM.ServiceTypes.ApprovalRequestsService);
                    SAPbobsCOM.ApprovalRequest oApprovalRequest = (SAPbobsCOM.ApprovalRequest)oService.GetDataInterface(SAPbobsCOM.ApprovalRequestsServiceDataInterfaces.arsApprovalRequest);
                    SAPbobsCOM.ApprovalRequestParams oApprovalRequestParams = (SAPbobsCOM.ApprovalRequestParams)oService.GetDataInterface(SAPbobsCOM.ApprovalRequestsServiceDataInterfaces.arsApprovalRequestParams);
                    oApprovalRequestParams.Code = approvalKey;
                    oApprovalRequest = oService.GetApprovalRequest(oApprovalRequestParams);

                    SAPbobsCOM.ApprovalRequestDecision oApprovalRequestDecision;

                    oApprovalRequestDecision = oApprovalRequest.ApprovalRequestDecisions.Add();
                    switch (approvalCode)
                    {
                        case "W":
                            oApprovalRequestDecision.Status = SAPbobsCOM.BoApprovalRequestDecisionEnum.ardPending;
                            break;
                        case "Y":
                            oApprovalRequestDecision.Status = SAPbobsCOM.BoApprovalRequestDecisionEnum.ardApproved;
                            break;
                        case "N":
                            oApprovalRequestDecision.Status = SAPbobsCOM.BoApprovalRequestDecisionEnum.ardNotApproved;
                            break;
                        default:
                            oApprovalRequestDecision.Status = SAPbobsCOM.BoApprovalRequestDecisionEnum.ardNotApproved;
                            break;
                    }
                    oApprovalRequestDecision.Remarks = remarks;
                    oApprovalRequestDecision.ApproverUserName = ApproverUserName;
                    oApprovalRequestDecision.ApproverPassword = ApproverUserPassword;
                    oService.UpdateRequest(oApprovalRequest);

                    oApprovalRequest = oService.GetApprovalRequest(oApprovalRequestParams);
                    if (oApprovalRequest.Status == SAPbobsCOM.BoApprovalRequestStatusEnum.arsApproved)
                    {
                        SAPbobsCOM.Documents oDraft = (SAPbobsCOM.Documents)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                        oDraft.GetByKey(oApprovalRequest.ObjectEntry);
                        int result = oDraft.SaveDraftToDocument();
                        if (result != 0)
                        {
                            oCompany.Disconnect();
                            return "Error:" + oCompany.GetLastErrorDescription();
                        }
                        oCompany.Disconnect();
                        return "Ok";
                    }
                    else
                    {

                        oCompany.Disconnect();
                        return "Draft was not approved, it's not possible to create the document before it";
                    }
                }

                return error;
            }
            catch (Exception ex)
            {
                oCompany.Disconnect();
                return "Error in try/catch:" + ex.Message + " \n Error de oCompany connect: " + error;
            }
        }
    }
}
